using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraEditors.Controls;
using DevExpress.ExpressApp.Win.SystemModule;

namespace WinAisogdMapInfo.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class LookupPropertyEditorExtension : ViewController
    {
        public LookupPropertyEditorExtension()
        {
            InitializeComponent();
            this.TargetViewType = ViewType.DetailView;
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
DetailView v = (DetailView)this.View;
            foreach (ViewItem vi in v.Items)
            {
                try
                {
                    if (vi is DevExpress.ExpressApp.Win.Editors.LookupPropertyEditor)
                    {
                        LookupPropertyEditor pe = (LookupPropertyEditor)vi;
                        LookupEdit ped = (LookupEdit)pe.Control;
                        EditorButton eb = new EditorButton();
                        eb.Caption = "...";
                        eb.Click += new EventHandler(LinkButtonClicked);
                        //ped.Properties.Buttons.Add(eb);
                        ped.Properties.Buttons.Insert(1, eb);
                        ped.Properties.ActionButtonIndex = 2;
                    }
                }
                catch { }
            }
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

        void LinkButtonClicked(object sender, EventArgs e)
        {
            OpenObjectController _openLink = Frame.GetController<OpenObjectController>();
            if (_openLink != null)
            {
                if (_openLink.OpenObjectAction != null && _openLink.OpenObjectAction.Active && _openLink.OpenObjectAction.Enabled)
                {
                    _openLink.OpenObjectAction.DoExecute();
                }
            }
        }

    }
}
