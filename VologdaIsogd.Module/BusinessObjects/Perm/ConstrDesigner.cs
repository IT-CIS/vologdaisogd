﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.Enums;
using AISOGD.Perm;
using AISOGD.Constr;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.Constr
{


    [ModelDefault("Caption", "Проектировщик"), System.ComponentModel.DefaultProperty("RequesterString")]
    public class ConstrDesigner : BaseObjectXAF
    {
        public ConstrDesigner(Session session) : base(session) { }

        private string i_requesterString;
        private string i_requesterAddress;
        private string i_requesterPhones;
        private Org i_org;
        private AISOGD.Subject.Person i_person;
        private eSubjectType i_requesterType;
        private string i_LicenseNo;
        private DateTime i_LicenseDate;
        private string _code;

        [Size(255), DisplayName("Код")]
        public string code
        {
            get
            {
                return _code;
            }
            set { try { _code = value; } catch { } }
        }

        [DisplayName("Тип проектировщика")]
        [ImmediatePostData]
        public eSubjectType RequesterType
        {
            get { return i_requesterType; }
            set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        }
        [DisplayName("Проектировщик")]
        [Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'физ'", Context = "DetailView")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [DisplayName("Проектировщик")]
        [Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'юр'", Context = "DetailView")]
        public AISOGD.Subject.Person RequesterPerson
        {
            get { return i_person; }
            set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Наименование проектировщика")]
        public string RequesterString
        {
            get {
                 try
                 {
                     if (String.IsNullOrEmpty(i_requesterString))
                         i_requesterString = GetRequesterInfo();
                 }
                 catch { }
                 return i_requesterString; }
             set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Адрес проектировщика")]
        public string RequesterAddress
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterAddress))
                        i_requesterAddress = GetRequesterAddress();
                }
                catch { }
                return i_requesterAddress;
            }
            set { SetPropertyValue("RequesterAddress", ref i_requesterAddress, value); }
        }

        [Size(500), DisplayName("Телефоны")]
        public string RequesterPhones
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterPhones))
                        i_requesterPhones = GetRequesterPhones();
                }
                catch { }
                return i_requesterPhones;
            }
            set { SetPropertyValue("RequesterPhones", ref i_requesterPhones, value); }
        }

        [Size(255), DisplayName("Номер лицензии")]
        public string LicenseNo
        {
            get { return i_LicenseNo; }
            set { SetPropertyValue("LicenseNo", ref i_LicenseNo, value); }
        }
        [DisplayName("Дата выдачи лицензии")]
        public DateTime LicenseDate
        {
            get { return i_LicenseDate; }
            set { SetPropertyValue("LicenseDate", ref i_LicenseDate, value); }
        }

        private string _requesterFax;
        [Size(255), DisplayName("Факс")]
        public string RequesterFax
        {
            get
            {
                return _requesterFax;
            }
            set { _requesterFax = value; }
        }

        private string _FIO;
        [Size(SizeAttribute.Unlimited), DisplayName("ФИО")]
        public string fio
        {
            get
            {
                return _FIO;
            }
            set { _FIO = value; }
        }


        private string _executor;
        [Size(255), DisplayName("Исполнитель")]
        public string executor
        {
            get { return _executor; }
            set { SetPropertyValue("executor", ref _executor, value); }
        }

        private string _status;
        [DisplayName("Статус")]
        public string status
        {
            get { return _status; }
            set { try { _status = value; } catch { } }
        }


        public string GetRequesterInfo()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.FullName;
            }
            if (i_person != null)
            {
                result = i_person.FullName;
            }
            return result;
        }
        public string GetRequesterAddress()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.address;
            }
            if (i_person != null)
            {
                result = i_person.address;
            }
            return result;
        }
        public string GetRequesterPhones()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.phones;
            }
            if (i_person != null)
            {
                result = i_person.phones;
            }
            return result;
        }
        [Association, DisplayName("Разрешения на строительство")]
        public XPCollection<ConstrPerm> ConstrPerm
        {
            get
            {
                return GetCollection<ConstrPerm>("ConstrPerm");
            }
        }

        [Association, DisplayName("Разрешения на ввод объектов в эксплуатацию")]
        public XPCollection<UsePerm> UsePerm
        {
            get
            {
                return GetCollection<UsePerm>("UsePerm");
            }
        }
        
    }


}