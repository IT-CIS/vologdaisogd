using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.DocFlow;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.Constr;
using VologdaIsogd.Module.Controllers.Perm;
using System.Linq;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace AISOGD.Perm
{
    [ModelDefault("Caption", "��������� ���������� �� �������������"),
    System.ComponentModel.DefaultProperty("FIO")]
    public class PermProlongue : BaseObjectXAF
    {
        public PermProlongue(Session session) : base(session) { }


        private ConstrPerm i_ConstrPerm;
        [Association, DisplayName("���������� �� �������������"), System.ComponentModel.Browsable(false)]
        public ConstrPerm ConstrPerm
        {
            get { return i_ConstrPerm; }
            set { SetPropertyValue("ConstrPerm", ref i_ConstrPerm, value); }
        }

        private PermNoticeIZD i_PermNoticeIZD;
        [Association, DisplayName("����������� �� �������������"), System.ComponentModel.Browsable(false)]
        public PermNoticeIZD PermNoticeIZD
        {
            get { return i_PermNoticeIZD; }
            set { SetPropertyValue("PermNoticeIZD", ref i_PermNoticeIZD, value); }
        }


        private DateTime i_DateProlongue;
        [DisplayName("���� ��������� ����������")]
        public DateTime DateProlongue
        {
            get { return i_DateProlongue; }
            set { SetPropertyValue("DateProlongue", ref i_DateProlongue, value); }
        }
        private DateTime i_ValidDateProlongue;
        [DisplayName("���� ��������� ����������")]
        public DateTime ValidDateProlongue
        {
            get { return i_ValidDateProlongue; }
            set { SetPropertyValue("ValidDateProlongue", ref i_ValidDateProlongue, value); }
        }

        private Signer i_SignerProlongue;
        [DisplayName("�� �������� (���������)")]
        public Signer SignerProlongue
        {
            get { return i_SignerProlongue; }
            set { SetPropertyValue("SignerProlongue", ref i_SignerProlongue, value); }
        }

        protected override void OnSaving()
        {
            base.OnSaving();

            SetDateProlongue();
        }
        public void SetDateProlongue()
        {
            // ��������� ���������
            // �� ������� ����� �������� �� ����������� ������� ��
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            Connect connect = Connect.FromSession(Session);
            var spatial = new SpatialController<GisLayer>(connect);

            try
            {
                ConstrPerm constrPerm = this.ConstrPerm;
                foreach (ConstrStage stage in constrPerm.ConstrStages)
                {
                    SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", stage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                    if (objLink != null)
                    {
                        try
                        {
                            m_MapInfo.Do("Select * From " + objLink.SpatialLayerId + " Where MI_PRINX = " + objLink.SpatialObjectId);
                            m_MapInfo.Do("Update Selection Set ����_������ = \"" + ValidDateProlongue.ToShortDateString() + "\" Commit table " + objLink.SpatialLayerId);
                            //SynhronizePermDataController.SetConstrStageSemantic(m_MapInfo, stage, constrPerm, objLink.SpatialLayerId, objLink.SpatialObjectId);
                        }
                        catch { }
                    }
                }
            }
            catch { }
        }

    }
}