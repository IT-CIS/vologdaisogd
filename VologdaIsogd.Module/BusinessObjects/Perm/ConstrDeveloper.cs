﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.Enums;
using AISOGD.Perm;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.Isogd;

namespace AISOGD.Constr
{


    [ModelDefault("Caption", "Застройщик"), System.ComponentModel.DefaultProperty("RequesterString")]
    [NavigationItem("Общие реестры")]
    public class ConstrDeveloper : BaseObjectXAF
    {
        public ConstrDeveloper(Session session) : base(session) { }

        private string i_requesterString;
        private string i_requesterAddress;
        private string i_requesterPhones;
        private Org i_org;
        private AISOGD.Subject.Person i_person;
        private eSubjectType i_requesterType;
        private string i_LicenseNo;
        private DateTime i_LicenseDate;
        private string i_LicenseIssued;
        private string i_LicenseSeries;
        private string _code;

        //[Size(255), DisplayName("Код")]
        //public string code
        //{
        //    get
        //    {
        //        return _code;
        //    }
        //    set { try { _code = value; } catch { } }
        //}

        [DisplayName("Тип застройщика")]
        [ImmediatePostData]
        public eSubjectType RequesterType
        {
            get { return i_requesterType; }
            set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        }
        [DisplayName("Застройщик")]
        [Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'физ'", Context = "DetailView")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [DisplayName("Застройщик")]
        [Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'юр'", Context = "DetailView")]
        public AISOGD.Subject.Person RequesterPerson
        {
            get { return i_person; }
            set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Наименование застройщика")]
        public string RequesterString
        {
            get {
                 try
                 {
                     if (String.IsNullOrEmpty(i_requesterString))
                         i_requesterString = GetRequesterInfo();
                 }
                 catch { }
                 return i_requesterString; }
             set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        }

        private string i_RequesterStringDat;
        [Size(SizeAttribute.Unlimited), DisplayName("Наименование застройщика (дат. падеж)")]
        public string RequesterStringDat
        {
            get {
                 try
                 {
                     if (String.IsNullOrEmpty(i_RequesterStringDat))
                         i_RequesterStringDat = GetRequesterInfoDat();
                 }
                 catch { }
                 return i_RequesterStringDat;
            }
            set { SetPropertyValue("RequesterStringDat", ref i_RequesterStringDat, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Адрес застройщика")]
        public string RequesterAddress
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterAddress))
                        i_requesterAddress = GetRequesterAddress();
                }
                catch { }
                return i_requesterAddress;
            }
            set { SetPropertyValue("RequesterAddress", ref i_requesterAddress, value); }
        }

        [Size(255), DisplayName("Телефоны")]
        public string RequesterPhones
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterPhones))
                        i_requesterPhones = GetRequesterPhones();
                }
                catch { }
                return i_requesterPhones;
            }
            set { SetPropertyValue("RequesterPhones", ref i_requesterPhones, value); }
        }
        private string i_RequesterEmail;
        [Size(255), DisplayName("Email")]
        public string RequesterEmail
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_RequesterEmail))
                        i_RequesterEmail = GetRequesterEmail();
                }
                catch { }
                return i_RequesterEmail;
            }
            set { SetPropertyValue("RequesterEmail", ref i_RequesterEmail, value); }
        }
        [Size(255), DisplayName("Номер лицензии")]
        public string LicenseNo
        {
            get { return i_LicenseNo; }
            set { SetPropertyValue("LicenseNo", ref i_LicenseNo, value); }
        }
        [Size(255), DisplayName("Серия лицензии")]
        public string LicenseSeries
        {
            get { return i_LicenseNo; }
            set { SetPropertyValue("LicenseSeries", ref i_LicenseSeries, value); }
        }

        [Size(255), DisplayName("Кем выдано")]
        public string LicenseIssued
        {
            get { return i_LicenseNo; }
            set { SetPropertyValue("LicenseIssued", ref i_LicenseIssued, value); }
        }
        [DisplayName("Дата выдачи лицензии")]
        public DateTime LicenseDate
        {
            get { return i_LicenseDate; }
            set { SetPropertyValue("LicenseDate", ref i_LicenseDate, value); }
        }

        private string _requesterFax;
        [Size(255), DisplayName("Факс")]
        public string RequesterFax
        {
            get
            {
                return _requesterFax;
            }
            set { _requesterFax = value; }
        }

        //private string _FIO;
        //[Size(255), DisplayName("ФИО")]
        //public string fio
        //{
        //    get
        //    {
        //        return _FIO;
        //    }
        //    set { _FIO = value; }
        //}


        //private string _executor;
        //[Size(255), DisplayName("Исполнитель")]
        //public string executor
        //{
        //    get { return _executor; }
        //    set { SetPropertyValue("executor", ref _executor, value); }
        //}

        //private string _status;
        //[DisplayName("Статус")]
        //public string status
        //{
        //    get { return _status; }
        //    set { try { _status = value; } catch { } }
        //}

        
        public string GetRequesterInfoDat()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.BriefNameDat;
            }
            if (i_person != null)
            {
                result = i_person.NameTo;
            }
            return result;
        }
        public string GetRequesterInfo()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.FullName;
            }
            if (i_person != null)
            {
                result = i_person.FullName;
            }
            return result;
        }
        public string GetRequesterAddress()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.address;
            }
            if (i_person != null)
            {
                result = i_person.address;
            }
            return result;
        }
        public string GetRequesterPhones()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.phones;
            }
            if (i_person != null)
            {
                result = i_person.phones;
            }
            return result;
        }
        public string GetRequesterEmail()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.ContactMainEmail;
            }
            if (i_person != null)
            {
                result = i_person.ContactMainEmail;
            }
            return result;
        }

        [Association, DisplayName("Разрешения на строительство")]
        public XPCollection<ConstrPerm> ConstrPerm
        {
            get
            {
                return GetCollection<ConstrPerm>("ConstrPerm");
            }
        }
        [Association, DisplayName("Разрешения на ввод объектов в эксплуатацию")]
        public XPCollection<UsePerm> UsePerm
        {
            get
            {
                return GetCollection<UsePerm>("UsePerm");
            }
        }

        [Association, DisplayName("ДЗУ")]
        public XPCollection<IsogdPhysStorageBook> PhysStorageBooks
        {
            get { return GetCollection<IsogdPhysStorageBook>("PhysStorageBooks"); }
        }
    }


}