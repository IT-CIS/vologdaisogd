﻿using AISOGD.Address;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Автор
    /// </summary>
    [Custom("Caption", "Автор"), System.ComponentModel.DefaultProperty("name")]
    public class Avtor : BaseObject
    {
        public Avtor(Session session)
            : base(session) { }

        private string _IDField;

        [Size(255), DisplayName("IDField")]
        public string IDField
        {
            get
            {
                return _IDField;
            }
            set { try { _IDField = value; } catch { } }
        }

        private string _code;

        [Size(255), DisplayName("Код")]
        public string code
        {
            get
            {
                return _code;
            }
            set { try { _code = value; } catch { } }
        }


        private string _adres;

        [Size(255), DisplayName("Адрес")]
        public string adres
        {
            get
            {
                return _adres;
            }
            set { try { _adres = value; } catch { } }
        }

        private DateTime _contractDate;

        [DisplayName("Дата договора")]
        public DateTime contractDate
        {
            get { return _contractDate; }
            set { try { _contractDate = value; } catch { } }
        }

        private string _name;

        [Size(1000), DisplayName("Наименование")]
        public string name
        {
            get { return _name; }
            set { try { _name = value; } catch { } }
        }

        private string _contract;
        [DisplayName("Номер договора")]
        public string contract
        {
            get { return _contract; }
            set { try { _contract = value; } catch { } }
        }


        private string _note;
        [DisplayName("Примечание")]
        public string note
        {
            get { return _note; }
            set { try { _note = value; } catch { } }
        }

        private string _phone;
        [DisplayName("Телефон")]
        public string phone
        {
            get { return _phone; }
            set { try { _phone = value; } catch { } }
        }

        private string _status;
        [DisplayName("Статус")]
        public string status
        {
            get { return _status; }
            set { try { _status = value; } catch { } }
        }
    }



}
