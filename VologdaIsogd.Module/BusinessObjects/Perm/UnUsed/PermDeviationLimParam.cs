﻿using AISOGD.Address;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Разрешение на отклонение от предельных параметров разрешенного строительства, реконструкции ОКС
    /// </summary>
    [Custom("Caption", "Разрешение на отклонение от предельных параметров разрешенного строительства, реконструкции ОКС"), NavigationItem("Подготовка документов"), System.ComponentModel.DefaultProperty("descript")]
    public class PermDeviationLimParam : GeneralDocBase
    {
        public PermDeviationLimParam(Session session)
            : base(session) { }

        private DateTime _letterDate;
        private string _letterNo;
        private string _ObjectName;
        private string _AddressObjectName;
       
        private CityRegion _CityRegion;
       
        //private string descript;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Разрешение на отклонение от предельных параметров разрешенного строительства, реконструкции ОКС № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }


        [Size(32),DisplayName("Номер заявки")]
        public string LetterNo
        {
            get {string res = String.Empty;
                try
                {
                    if (InLetter.Count > 0)
                        res = InLetter[0].RegNo;
                }
                catch { }
                return res;}
            set { SetPropertyValue("LetterNo", ref _letterNo, value); }
        }
        [DisplayName("Дата заявки")]
        public DateTime LetterDate
        {
            get
            {
                _letterDate = DateTime.MinValue;
                if (InLetter.Count > 0)
                    _letterDate = InLetter[0].RegDate;
                return _letterDate;
            }
            set { SetPropertyValue("LetterDate", ref _letterDate, value); }
        }

        private string i_LetterSubject;
        [Size(255), DisplayName("Описание заявителя")]
        public string LetterSubject
        {
            get
            {
                if (i_LetterSubject == null || i_LetterSubject == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (InLetter.Count > 0)
                            res = InLetter[0].Subject;
                    }
                    catch { }
                    i_LetterSubject = res;
                }
                return i_LetterSubject;
            }
            set { SetPropertyValue("LetterSubject", ref i_LetterSubject, value); }
        }

        private WorkKind i_WorkKind;
        [DisplayName("Вид работ")]
        public WorkKind WorkKind
        {
            get { return i_WorkKind; }
            set { try { SetPropertyValue("WorkKind", ref i_WorkKind, value); } catch { } }
        }

        private string i_LotCadNo;
        [Size(255), DisplayName("Кадастровый номер земельного участка")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get
            {
                if (i_LotCadNo == null || i_LotCadNo == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].CadNo != null)
                                res = Lots[0].CadNo;
                    }
                    catch { }
                    i_LotCadNo = res;
                }
                return i_LotCadNo;
            }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }

        private Municipality i_Municipality;
        [DisplayName("Муниципальное образование")]
        [ImmediatePostData]
        public Municipality Municipality
        {
            get
            {
                try { i_Municipality = GetMunicipality(); }
                catch { }
                return i_Municipality;
            }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        [DisplayName("Район города")]
        [ImmediatePostData]
        public CityRegion CityRegion
        {
            get { return _CityRegion; }
            set { try { SetPropertyValue("CityRegion", ref _CityRegion, value); } catch { } }
        }
        private string i_LotAddress;
        [Size(1000), DisplayName("Адрес земельного участка")]
        public string LotAddress
        {
            get
            {
                if (i_LotAddress == null || i_LotAddress == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].Adress != null)
                                res = Lots[0].Adress;
                    }
                    catch { }
                    try
                    {
                        if (res == String.Empty)
                        {
                            res = Municipality.Name + ", " + CityRegion.Name;
                        }
                    }
                    catch { }
                    i_LotAddress = res;
                }
                return i_LotAddress;
            }
            set { SetPropertyValue("LotAddress", ref i_LotAddress, value); }
        }

        private string i_LotSquare;
        [Size(255), DisplayName("Площадь земельного участка, кв.м.")]
        public string LotSquare
        {
            get
            {
                if (i_LotSquare == null || i_LotSquare == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].LotSquare != null)
                                res = Lots[0].LotSquare;
                    }
                    catch { }
                    i_LotSquare = res;
                }
                return i_LotSquare;
            }
            set { SetPropertyValue("LotSquare", ref i_LotSquare, value); }
        }

        private string i_CadastralOrder;
        [Size(255), DisplayName("На основании кадастрового паспорта земельного участка")]
        public string CadastralOrder
        {
            get { return i_CadastralOrder; }
            set { SetPropertyValue("CadastralOrder", ref i_CadastralOrder, value); }
        }

        private string i_LawDocument;
        [Size(510), DisplayName("На основании документа правообладания")]
        public string LawDocument
        {
            get
            {
                if (i_LawDocument == null || i_LawDocument == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].LawDocument != null)
                                res = Lots[0].LawDocument;
                    }
                    catch { }
                    i_LawDocument = res;
                }
                return i_LawDocument;
            }
            set { SetPropertyValue("LawDocument", ref i_LawDocument, value); }
        }
        private string i_TerritoryPlanDocString;
        [Size(1000), DisplayName("На основании проекта планировки территории")]
        public string TerritoryPlanDocString
        {
            get
            {
                return i_TerritoryPlanDocString;
            }
            set { SetPropertyValue("TerritoryPlanDocString", ref i_TerritoryPlanDocString, value); }
        }

        private string i_TerrZoneString;
        [Size(510), DisplayName("Территориальная зона")]
        public string TerrZoneString
        {
            get
            {
                if (i_TerrZoneString == null || i_TerrZoneString == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].TerrZoneString != null)
                                res = Lots[0].TerrZoneString;
                    }
                    catch { }
                    i_TerrZoneString = res;
                }
                return i_TerrZoneString;
            }
            set { SetPropertyValue("TerrZoneString", ref i_TerrZoneString, value); }
        }
        private UsingWay i_UsingWay;
        [DisplayName("Использование земельного участка")]
        public UsingWay UsingWay
        {
            get { return i_UsingWay; }
            set { SetPropertyValue("UsingWay", ref i_UsingWay, value); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Разрешение на отклонение от предельных параметров разрешенного строительства, реконструкции ОКС"));
            if (this.signer == null)
                this.signer = Connect.FromSession(Session).FindFirstObject<Signer>(mc => mc.capacity.Contains("Глава администрации") == true);
            Session.CommitTransaction();
        }

    }

}
