﻿using AISOGD.DocFlow;
using AISOGD.General;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Бегунок (разрешение на строительство)
    /// </summary>
    [ModelDefault("Caption", "Бегунок (разрешение на строительство)"), NavigationItem("Подготовка документов")]
    public class ConstrPermComplectDoc: AttachBase
    {
        public ConstrPermComplectDoc(Session session)
            : base(session) { }

        private bool _ComplectElem1;
        [DisplayName("Правоустанавливающие документы на земельный участок"), VisibleInListView(false)]
        public bool complectElem1
        {
            get { return _ComplectElem1; }
            set { try { _ComplectElem1 = value; } catch { } }
        }
        private bool _ComplectElem2;
        [DisplayName("Градостроительный план земельного участка"), VisibleInListView(false)]
        public bool complectElem2
        {
            get { return _ComplectElem2; }
            set { try { _ComplectElem2 = value; } catch { } }
        }
        private bool _ComplectElem3;
        [DisplayName("Пояснительная записка"), VisibleInListView(false)]
        public bool complectElem3
        {
            get { return _ComplectElem3; }
            set { try { _ComplectElem3 = value; } catch { } }
        }
        private bool _ComplectElem4;
        [DisplayName("Схемы, отображающие архитектурные решения"), VisibleInListView(false)]
        public bool complectElem4
        {
            get { return _ComplectElem4; }
            set { try { _ComplectElem4 = value; } catch { } }
        }
        private bool _ComplectElem5;
        [DisplayName("Сведения об инженерном оборудовании, сводный план сетей инженерно-технического обеспечения"), VisibleInListView(false)]
        public bool complectElem5
        {
            get { return _ComplectElem5; }
            set { try { _ComplectElem5 = value; } catch { } }
        }
        private bool _ComplectElem6;
        [DisplayName("Проект организации строительства объекта капитального строительства"), VisibleInListView(false)]
        public bool complectElem6
        {
            get { return _ComplectElem6; }
            set { try { _ComplectElem6 = value; } catch { } }
        }
        private bool _ComplectElem7;
        [DisplayName("Положительное заключение негосударственной экспертизы проектной документации"), VisibleInListView(false)]
        public bool complectElem7
        {
            get { return _ComplectElem7; }
            set { try { _ComplectElem7 = value; } catch { } }
        }
        private bool _ComplectElem8;
        [DisplayName("Положительное заключение экспертизы проектной документации и результатов инженерных изысканий"), VisibleInListView(false)]
        public bool complectElem8
        {
            get { return _ComplectElem8; }
            set { try { _ComplectElem8 = value; } catch { } }
        }
        private bool _ComplectElem9;
        [DisplayName("Проект организации работ по сносу или демонтажу объектов капитального строительства, их частей (при наличии)"), VisibleInListView(false)]
        public bool complectElem9
        {
            get { return _ComplectElem9; }
            set { try { _ComplectElem9 = value; } catch { } }
        }
        private bool _ComplectElem10;
        [DisplayName("Согласие всех правообладателей объекта капитального строительства, в случае реконструкции такого объекта"), VisibleInListView(false)]
        public bool complectElem10
        {
            get { return _ComplectElem10; }
            set { try { _ComplectElem10 = value; } catch { } }
        }
        private bool _ComplectElem11;
        [DisplayName("Схема планировочной организации земельного участка, выполненная в соответствии с градостроительным планом земельного участка"), VisibleInListView(false)]
        public bool complectElem11
        {
            get { return _ComplectElem11; }
            set { try { _ComplectElem11 = value; } catch { } }
        }
        private bool _ComplectElem12;
        [DisplayName("Схема планировочной организации земельного участка, подтверждающая расположение линейного объекта в пределах красных линий"), VisibleInListView(false)]
        public bool complectElem12
        {
            get { return _ComplectElem12; }
            set { try { _ComplectElem12 = value; } catch { } }
        }
        private bool _ComplectElem13;
        [DisplayName("Схема планировочной организации земельного участка с обозначением места размещения объекта индивидуального жилищного строительства"), VisibleInListView(false)]
        public bool complectElem13
        {
            get { return _ComplectElem13; }
            set { try { _ComplectElem13 = value; } catch { } }
        }
        private bool _ComplectElem0;
        [DisplayName("Разрешение на отклонение от предельных параметров разрешенного строительства, реконструкции"), VisibleInListView(false)]
        public bool complectElem0
        {
            get { return _ComplectElem0; }
            set { try { _ComplectElem0 = value; } catch { } }
        }
        private bool _CopySvedAkr;
        [DisplayName("Копия свидетельства об аккредитации юридического лица, выдавшего положительное заключение негосударственной экспертизы проектной документации"), VisibleInListView(false)]
        public bool copySvedAkr
        {
            get { return _CopySvedAkr; }
            set { try { _CopySvedAkr = value; } catch { } }
        }
        private bool _Complete;
        [DisplayName("Бегунок заполнен")]
        public bool complete
        {
            get { return _Complete; }
            set { try { _Complete = value; } catch { } }
        }

        //private ConstrPerm _ConstrPerm;
        //[DisplayName("Разрешение на строительство")]
        //[Association("ConstrPerm-ConstrPermComplectDoc")]
        //public ConstrPerm constrPerm
        //{
        //    get { return _ConstrPerm; }
        //    set { SetPropertyValue("constrPerm", ref _ConstrPerm, value); }
        //}
    }
}
