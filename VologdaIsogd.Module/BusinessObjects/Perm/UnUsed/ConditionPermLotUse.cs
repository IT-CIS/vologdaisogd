﻿using AISOGD.Address;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Разрешение на условно-разрешенный вид использования земельного участка
    /// </summary>
    [Custom("Caption", "Разрешение на условно-разрешенный вид использования земельного участка"), NavigationItem("Подготовка документов"), System.ComponentModel.DefaultProperty("descript")]
    public class ConditionPermLotUse : GeneralDocBase
    {
        public ConditionPermLotUse(Session session)
            : base(session) { }


        private DateTime _letterDate;
        private string _letterNo;
       
        private CityRegion _CityRegion;
       
        //private string descript;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Разрешение на условно-разрешенный вид использования земельного участка № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }


        [Size(32),DisplayName("Номер заявки")]
        public string LetterNo
        {
            get {string res = String.Empty;
                try
                {
                    if (InLetter.Count > 0)
                        res = InLetter[0].RegNo;
                }
                catch { }
                return res;}
            set { SetPropertyValue("LetterNo", ref _letterNo, value); }
        }
        [DisplayName("Дата заявки")]
        public DateTime LetterDate
        {
            get
            {
                _letterDate = DateTime.MinValue;
                if (InLetter.Count > 0)
                    _letterDate = InLetter[0].RegDate;
                return _letterDate;
            }
            set { SetPropertyValue("LetterDate", ref _letterDate, value); }
        }

        private string i_LetterSubject;
        [Size(255), DisplayName("Описание заявителя")]
        public string LetterSubject
        {
            get
            {
                if (i_LetterSubject == null || i_LetterSubject == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (InLetter.Count > 0)
                            res = InLetter[0].Subject;
                    }
                    catch { }
                    i_LetterSubject = res;
                }
                return i_LetterSubject;
            }
            set { SetPropertyValue("LetterSubject", ref i_LetterSubject, value); }
        }

        private string i_LotCadNo;
        [Size(255), DisplayName("Кадастровый номер земельного участка")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get {
                if (i_LotCadNo == null || i_LotCadNo == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].CadNo != null)
                                res = Lots[0].CadNo;
                    }
                    catch { }
                    i_LotCadNo = res;
                }
                return i_LotCadNo; }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }
        private Municipality i_Municipality;
        [DisplayName("Муниципальное образование")]
        [ImmediatePostData]
        public Municipality Municipality
        {
            get
            {
                try { i_Municipality = GetMunicipality(); }
                catch { }
                return i_Municipality;
            }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }

        [DisplayName("Район города")]
        [ImmediatePostData]
        public CityRegion CityRegion
        {
            get { return _CityRegion; }
            set { try { SetPropertyValue("CityRegion", ref _CityRegion, value); } catch { } }
        }

        private string i_LotAddress;
        [Size(1000), DisplayName("Адрес земельного участка")]
        public string LotAddress
        {
            get
            {
                if (i_LotAddress == null || i_LotAddress == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].Adress != null)
                                res = Lots[0].Adress;
                    }
                    catch { }
                    try
                    {
                        if (res == String.Empty)
                        {
                            res = Municipality.Name + ", " + CityRegion.Name;
                        }
                    }
                    catch { }
                    i_LotAddress = res;
                }
                return i_LotAddress;
            }
            set { SetPropertyValue("LotAddress", ref i_LotAddress, value); }
        }

        private string i_LotSquare;
        [Size(255), DisplayName("Площадь земельного участка, кв.м.")]
        public string LotSquare
        {
            get {
                if (i_LotSquare == null || i_LotSquare == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].LotSquare != null)
                                res = Lots[0].LotSquare;
                    }
                    catch { }
                    i_LotSquare = res;
                }
                return i_LotSquare; }
            set { SetPropertyValue("LotSquare", ref i_LotSquare, value); }
        }

        private string i_CadastralOrder;
        [Size(255), DisplayName("Кадастровая выписка о земельном участке")]
        public string CadastralOrder
        {
            get
            {
                if (i_CadastralOrder == null || i_CadastralOrder == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].CadastralOrder != null)
                                res = Lots[0].CadastralOrder;
                    }
                    catch { }
                    i_CadastralOrder = res;
                }
                return i_CadastralOrder;
            }
            set { SetPropertyValue("CadastralOrder", ref i_CadastralOrder, value); }
        }

        private DateTime i_PublicListensDate;
        [DisplayName("Дата публичных слушаний")]
        public DateTime PublicListensDate
        {
            get { return i_PublicListensDate; }
            set { SetPropertyValue("PublicListensDate", ref i_PublicListensDate, value); }
        }

        private string i_TerrZoneString;
        [Size(510), DisplayName("Территориальная зона")]
        public string TerrZoneString
        {
            get {
                if (i_TerrZoneString == null || i_TerrZoneString == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].TerrZoneString != null)
                                res = Lots[0].TerrZoneString;
                    }
                    catch { }
                    i_TerrZoneString = res;
                }
                return i_TerrZoneString; }
            set { SetPropertyValue("TerrZoneString", ref i_TerrZoneString, value); }
        }
        private UsingWay i_UsingWay;
        [DisplayName("Использование земельного участка")]
        public UsingWay UsingWay
        {
            get { return i_UsingWay; }
            set { SetPropertyValue("UsingWay", ref i_UsingWay, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Разрешение на условно-разрешенный вид использования земельного участка"));
            if (this.signer == null)
                this.signer = Connect.FromSession(Session).FindFirstObject<Signer>(mc => mc.capacity.Contains("Глава администрации") == true);
            Session.CommitTransaction();
        }

    }



}
