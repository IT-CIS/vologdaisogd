﻿using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.General;
using AISOGD.Land;
using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Технический план
    /// </summary>
    [Custom("Caption", "Технический план"), System.ComponentModel.DefaultProperty("TechPlanInfo")]
    public class TechPlan : AttachBase
    {
        public TechPlan(Session session)
            : base(session) { }

        //private string i_TechPlanInfo;
        //[Size(255), DisplayName("Описание")]
        //public string TechPlanInfo
        //{
        //    get
        //    {
        //        return i_TechPlanInfo;
        //    }
        //    //set { SetPropertyValue("TechPlanInfo", ref i_TechPlanInfo, value); }
        //}

        private string i_TechPlanNo;
        [Size(32), DisplayName("Номер техплана")]
        public string TechPlanNo
        {
            get
            {
                return i_TechPlanNo;
            }
            set { SetPropertyValue("TechPlanNo", ref i_TechPlanNo, value); }
        }
        private DateTime i_TechPlanDate;
        [DisplayName("Дата техплана")]
        public DateTime TechPlanDate
        {
            get
            {
                return i_TechPlanDate;
            }
            set { SetPropertyValue("TechPlanDate", ref i_TechPlanDate, value); }
        }
        private CadEngineer i_CadEngineer;
        [DisplayName("Кадастровый инженер")]
        public CadEngineer CadEngineer
        {
            get { return i_CadEngineer; }
            set { SetPropertyValue("CadEngineer", ref i_CadEngineer, value); }
        }
        private string i_TechPlanInfo;
        [Size(500), DisplayName("Описание")]
        public string TechPlanInfo
        {
            get
            {
                string res = "";
                try{
                    res = "№ " + TechPlanNo;
                }
                catch { }
                try
                {
                    res += " от " + TechPlanDate.ToShortDateString();
                }
                catch { }
                try
                {
                    res += ", " + CadEngineer.FIO;
                }
                catch { }
                i_TechPlanInfo = res.Trim();
                return i_TechPlanInfo;
            }
            //set { SetPropertyValue("TechPlanInfo", ref i_TechPlanInfo, value); }
        }
    }
}
