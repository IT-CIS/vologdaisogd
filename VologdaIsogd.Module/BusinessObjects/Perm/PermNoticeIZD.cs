﻿using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.Perm;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Уведомление на строительство
    /// </summary>
    [ModelDefault("Caption", "Уведомление на строительство"), NavigationItem("Подготовка разрешений"), System.ComponentModel.DefaultProperty("descript")]
    public class PermNoticeIZD : GeneralDocBase
    {
        public PermNoticeIZD(): base() { }
        public PermNoticeIZD(Session session): base(session) { }

        private const string descriptFormat = "Уведомление на строительство № {DocNo} от {DocDate}";
        [Size(255), DisplayName("Описание")]//System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Уведомление на строительство № {0} от {1}", (DocNo?? ""), DocDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        private string i_letterNo;
        [Size(32), DisplayName("Номер заявки (входящий)")]
        public string LetterNo
        {
            get
            {
                try
                {
                    if (i_letterNo == null || i_letterNo == "")
                    {
                        string res = String.Empty;
                        try
                        {
                            if (InLetter.Count > 0)
                                res = InLetter[0].RegNo;
                        }
                        catch { }
                        i_letterNo = res;
                    }
                }
                catch { }
                return i_letterNo;
            }
            set { SetPropertyValue("LetterNo", ref i_letterNo, value); }
        }
        private DateTime i_letterDate;
        [DisplayName("Дата заявки (входящяя)")]
        public DateTime LetterDate
        {
            get
            {
                try
                {
                    if (i_letterDate == DateTime.MinValue)
                    {
                        i_letterDate = DateTime.MinValue;
                        if (InLetter.Count > 0)
                            i_letterDate = InLetter[0].RegDate;
                    }
                }
                catch { }
                return i_letterDate;
            }
            set { SetPropertyValue("LetterDate", ref i_letterDate, value); }
        }
        private ePermKind i_PermKind;
        [DisplayName("Вид разрешения на строительство")]
        public ePermKind PermKind
        {
            get { return i_PermKind; }
            set { SetPropertyValue("PermKind", ref i_PermKind, value); }
        }

        private eIZDAim i_IZDAim;
        [DisplayName("Цель подачи")]
        public eIZDAim IZDAim
        {
            get { return i_IZDAim; }
            set { try { SetPropertyValue("IZDAim", ref i_IZDAim, value); } catch { } }
        }

        private string i_MapNo;
        [DisplayName("Номер в карте")]
        public string MapNo
        {
            get { return i_MapNo; }
            set { SetPropertyValue("MapNo", ref i_MapNo, value); }
        }
        
        private eIZDKind i_IZDKind;
        [DisplayName("Вид разрешенного использования")]
        [ImmediatePostData]
        public eIZDKind IZDKind
        {
            get { return i_IZDKind; }
            set { SetPropertyValue("IZDKind", ref i_IZDKind, value); }
        }
        //private eBuildingCategory i_BuildingCategory;
        //[DisplayName("Назначение объекта строительства"), VisibleInListView(false)]
        //public eBuildingCategory BuildingCategory
        //{
        //    get { return i_BuildingCategory; }
        //    set { try { SetPropertyValue("BuildingCategory", ref i_BuildingCategory, value); } catch { } }
        //}

        private string i_LotCadNo;
        [Size(255), DisplayName("Кадастровый номер земельного участка(ов)")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get { return i_LotCadNo; }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }

        

        private string i_ObjectAddress;
        [Size(1000), DisplayName("Адрес (местоположение) участка")]
        public string ObjectAddress
        {
            get { return i_ObjectAddress; }
            set { try { SetPropertyValue("ObjectAddress", ref i_ObjectAddress, value); } catch { } }
        }

        private DateTime i_ValidDate;
        [DisplayName("Срок действия уведомления")]
        public DateTime ValidDate
        {
            get { return i_ValidDate; }
            set
            {
                SetPropertyValue("ValidDate", ref i_ValidDate, value);
                //OnChanged("ValidDate");
            }
        }
        private DateTime i_ValidDateTotal;
        [DisplayName("Срок действия уведомления (с продлениями)")]
        public DateTime ValidDateTotal
        {
            get
            {
                try
                {
                    if (PermProlongues.Count > 0)
                    {
                        SortProperty sort = new SortProperty("ValidDateProlongue", DevExpress.Xpo.DB.SortingDirection.Descending);
                        PermProlongues.Sorting.Add(sort);
                        i_ValidDateTotal = PermProlongues[0].ValidDateProlongue;
                    }
                    else
                        i_ValidDateTotal = ValidDate;
                }
                catch { }
                return i_ValidDateTotal;
            }
            set
            {
                SetPropertyValue("ValidDateTotal", ref i_ValidDateTotal, value);

                //SetConstrPlaningYearInUse();
                //OnChanged("ValidDateTotal");
            }
        }


        // застройщик
        private string i_ConstrDeveloperString;
        [Size(500), DisplayName("Застройщик")]
        public string ConstrDeveloperString
        {
            get
            {
                return i_ConstrDeveloperString;
            }
            set { try { SetPropertyValue("ConstrDeveloperString", ref i_ConstrDeveloperString, value); } catch { } }
        }
        private string i_ConstrDeveloperStringDat;
        [Size(500), DisplayName("Застройщик (дат.падеж)")]
        public string ConstrDeveloperStringDat
        {
            get
            {
                return i_ConstrDeveloperStringDat;
            }
            set { try { SetPropertyValue("ConstrDeveloperString", ref i_ConstrDeveloperStringDat, value); } catch { } }
        }
        private string i_ConstrDeveloperContactInfo;
        [Size(500), DisplayName("Реквизиты застройщика")]
        public string ConstrDeveloperContactInfo
        {
            get
            {
                return i_ConstrDeveloperContactInfo;
            }
            set { SetPropertyValue("ConstrDeveloperContactInfo", ref i_ConstrDeveloperContactInfo, value); }
        }
        private string i_ConstrDeveloperINN;
        [Size(255), DisplayName("ИНН застройщика")]
        public string ConstrDeveloperINN
        {
            get
            {
                return i_ConstrDeveloperINN;
            }
            set { try { SetPropertyValue("ConstrDeveloperINN", ref i_ConstrDeveloperINN, value); } catch { } }
        }
        private string i_ConstrDeveloperEmail;
        [Size(255), DisplayName("Email застройщика")]
        public string ConstrDeveloperEmail
        {
            get
            {
                return i_ConstrDeveloperEmail;
            }
            set { try { SetPropertyValue("ConstrDeveloperEmail", ref i_ConstrDeveloperEmail, value); } catch { } }
        }

        // параметры строительства
        private string i_FloorCount;
        [Size(255), DisplayName("Количество надземных этажей (шт.)")]
        public string FloorCount
        {
            get { return i_FloorCount; }
            set { SetPropertyValue("FloorCount", ref i_FloorCount, value); }
        }

        private string i_Height;
        [Size(255), DisplayName("Высота (м.)")]
        public string Height
        {
            get { return i_Height; }
            set { SetPropertyValue("Height", ref i_Height, value); }
        }

        private string i_BuildSquare;
        [Size(255), DisplayName("Площадь застройки (кв.м)"), VisibleInListView(false)]
        public string BuildSquare
        {
            get { return i_BuildSquare; }
            set { SetPropertyValue("BuildSquare", ref i_BuildSquare, value); }
        }

        private string i_DeviationDecision;
        [Size(SizeAttribute.Unlimited), DisplayName("Сведения о предоставлении разрешения на отклонение")]
        public string DeviationDecision
        {
            get { return i_DeviationDecision; }
            set { SetPropertyValue("DeviationDecision", ref i_DeviationDecision, value); }
        }

        private string i_TypedArchInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("Сведения о типовом архитектурном решении")]
        public string TypedArchInfo
        {
            get { return i_TypedArchInfo; }
            set { SetPropertyValue("TypedArchInfo", ref i_TypedArchInfo, value); }
        }

        /// <summary>
        /// не соответствие
        /// </summary>
        private string i_RefuseReasonParam;
        [Size(SizeAttribute.Unlimited), DisplayName("1. Несоответствие параметров")]
        public string RefuseReasonParam
        {
            get { return i_RefuseReasonParam; }
            set { SetPropertyValue("RefuseReasonParam", ref i_RefuseReasonParam, value); }
        }
        private string i_RefuseReasonPlacement;
        [Size(SizeAttribute.Unlimited), DisplayName("2. Недопустимость размещения")]
        public string RefuseReasonPlacement
        {
            get { return i_RefuseReasonPlacement; }
            set { SetPropertyValue("RefuseReasonPlacement", ref i_RefuseReasonPlacement, value); }
        }
        private string i_RefuseReasonRights;
        [Size(SizeAttribute.Unlimited), DisplayName("3. Отсутствие прав на ЗУ")]
        public string RefuseReasonRights
        {
            get { return i_RefuseReasonRights; }
            set { SetPropertyValue("RefuseReasonRights", ref i_RefuseReasonRights, value); }
        }
        private string i_RefuseReasonAppearance;
        [Size(SizeAttribute.Unlimited), DisplayName("4. Несоответствие внешнего облика")]
        public string RefuseReasonAppearance
        {
            get { return i_RefuseReasonAppearance; }
            set { SetPropertyValue("RefuseReasonAppearance", ref i_RefuseReasonAppearance, value); }
        }

        [Association, DisplayName("Застройщики")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }

        [Association, DisplayName("Продления уведомления")]
        public XPCollection<PermProlongue> PermProlongues
        {
            get
            {
                return GetCollection<PermProlongue>("PermProlongues");
            }
        }
        [Association, DisplayName("Стадии строительства")]
        public XPCollection<ConstrStage> ConstrStages
        {
            get
            {
                return GetCollection<ConstrStage>("ConstrStages");
            }
        }
        [Association, DisplayName("Уведомления на ввод")]
        public XPCollection<UsePermNoticeIZD> UsePermNoticeIZDs
        {
            get
            {
                return GetCollection<UsePermNoticeIZD>("UsePermNoticeIZDs");
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            Connect connect = Connect.FromSession(Session);
            this.DocKind = Session.FindObject<dDocKind>(new BinaryOperator("Name", "Уведомление о строительстве объекта индивидуального жилищного строительства"));

            if (Empl == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    Empl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            if (signer == null)
            {
                signer = connect.FindFirstObject<Signer>(x => x.DefaultSignerFlag == true);
            }

            if(String.IsNullOrEmpty(TypedArchInfo))
            {
                TypedArchInfo = "нет";
            }
            Session.CommitTransaction();

        }

        protected override void OnSaving()
        {
            base.OnSaving();
            //Connect connect = Connect.FromSession(Session);
            //if (this.CapitalStructureBase.Count > 0)
            //{
            //    foreach(CapitalStructureBase constrObj in this.CapitalStructureBase)
            //    {
            //        try
            //        {
            //            constrObj.FloorCountProject = Convert.ToInt16(this.FloorCount);
            //        }
            //        catch { }
            //        try
            //        {
            //            constrObj.HeightProject = Convert.ToDouble(this.Height);
            //        }
            //        catch { }
            //        try
            //        {
            //            constrObj.BuildSquareProject = Convert.ToDouble(this.BuildSquare);
            //        }
            //        catch { }

            //        try
            //        {
            //            if (this.IZDAim == AISOGD.Enums.eIZDAim.Строительство)
            //                constrObj.WorkKind = AISOGD.Enums.eWorkKind.Строительство;
            //            else
            //                constrObj.WorkKind = AISOGD.Enums.eWorkKind.Реконструкция;
            //        }
            //        catch { }

            //        try {
            //            constrObj.Address = this.ObjectAddress ?? "";
            //        }
            //        catch { }

            //        try
            //        {
            //            if (this.IZDKind == AISOGD.Enums.eIZDKind.ИЖД)
            //                constrObj.ConstructionType = connect.FindFirstObject<dConstructionType>(x=> x.Name == "индивидуальный жилой дом");
            //            else
            //                constrObj.ConstructionType = connect.FindFirstObject<dConstructionType>(x => x.Name == "дачный дом");
            //        }
            //        catch { }
            //        constrObj.Save();
            //    }
            //}

        }

        #region Логика при изменении XPCollections
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //изменилась коллекция входящих обращений
            if (property.Name == "InLetter")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(InLetters_CollectionChanged);
            }
            //изменилась коллекция застройщиков
            if (property.Name == "DocSubjects")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(DocSubjects_CollectionChanged);
            }
            //изменилась коллекция земельных участков
            if (property.Name == "Parcels")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            }
            return result;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции входящих обращений - заполнение листа по застройщикам
        ///// </summary>
        private void InLetters_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                InLetter newObj = (InLetter)e.ChangedObject;
                if (newObj.DocSubjects.Count > 0)
                {
                    foreach (GeneralSubject DocSubject in newObj.DocSubjects)
                    {
                        DocSubjects.Add(DocSubject);

                        foreach (CapitalStructureBase constrObj in CapitalStructureBase)
                        {
                            constrObj.DocSubjects.Add(DocSubject);
                        }
                    }

                }

            }
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции застройщиков - заполнение поля по застройщикам
        ///// </summary>
        private void DocSubjects_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetDevelopers();
            }
        }
        /// <summary>
        /// Заполенение поля Застройщик (string) по листу застройщиков (XPCollection)
        /// </summary>
        private void SetDevelopers()
        {
            string dev = "";
            string devdat = "";
            string contact = "";
            string inn = "";
            string email = "";
            foreach (GeneralSubject subj in DocSubjects)
            {
                // именительный падеж
                if (dev == "")
                {
                    dev = subj.FullName;
                }
                else
                    if (!dev.Contains(subj.FullName))
                    dev += ", " + subj.FullName;

                //  дательный падеж застройщика
                string devdattemp = "";
                if (subj.NameDat != null && subj.NameDat != "")
                {
                    devdattemp = subj.NameDat;
                }
                else
                    devdattemp = subj.FullName;
                if (devdat == "")
                {
                    devdat = devdattemp;
                }
                else
                    if (!devdat.Contains(devdattemp))
                    devdat += ", " + devdattemp;

                // контактная информация
                try
                {
                    if (contact == "")
                    {
                        contact = subj.FullContactInfo;
                    }
                    else
                        if (!contact.Contains(subj.FullContactInfo))
                        contact += ", " + subj.FullContactInfo;
                }
                catch { }
                // ИНН
                try
                {
                    if (!String.IsNullOrEmpty(subj.INN))
                    {
                        if (inn == "")
                        {
                            inn = subj.INN;
                        }
                        else
                          if (!inn.Contains(subj.INN))
                            inn += ", " + subj.INN;
                    }
                }
                catch { }
                // Email
                
                try
                {
                    if (!String.IsNullOrEmpty(subj.ContactMainEmail))
                    {
                        if (email == "")
                        {
                            email = subj.ContactMainEmail;
                        }
                        else
                          if (!email.Contains(subj.ContactMainEmail))
                            email += ", " + subj.ContactMainEmail;
                    }
                }
                catch { }
                
            }
            if (dev.Length > 500)
                dev = dev.Remove(499, dev.Length);
            ConstrDeveloperString = dev;

            if (devdat.Length > 500)
                devdat = devdat.Remove(499, devdat.Length);
            ConstrDeveloperStringDat = devdat;

            if (contact.Length > 500)
                contact = contact.Remove(499, contact.Length);
            ConstrDeveloperContactInfo = contact;

            if (inn.Length > 255)
                inn = inn.Remove(254, inn.Length);
            ConstrDeveloperINN = inn;

            if (email.Length > 255)
                email = email.Remove(254, email.Length);
            ConstrDeveloperEmail = email;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции земельных участков - заполнение поля кадастровых номеров
        ///// </summary>
        private void Parcels_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetCadNoms();
                SetAddress();
            }
        }

        /// <summary>
        /// Заполенение поля кадастровых номеров(string) по листу земельных участков (XPCollection)
        /// </summary>
        private void SetCadNoms()
        {
            string res = "";
            foreach (Parcel obj in Parcels)
            {
                if (res == "")
                    res = obj.CadastralNumber;
                else
                    if (!res.Contains(obj.CadastralNumber))
                    res += ", " + obj.CadastralNumber;
            }
            if (res.Length > 255)
                res = res.Remove(254, res.Length);
            LotCadNo = res;
        }
        private void SetAddress()
        {
            string res = "";
            foreach (Parcel obj in Parcels)
            {
                if (res == "")
                    res = obj.Adress;
                else
                    if (!res.Contains(obj.Adress))
                    res += ", " + obj.Adress;
            }
            ObjectAddress = res;
        }
        #endregion
    }
}
