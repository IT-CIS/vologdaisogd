﻿//using AISOGD.Address;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Perm
{
    /// <summary>
    /// Разрешение на строительство
    /// </summary>
    [ModelDefault("Caption", "Разрешение на строительство"), NavigationItem("Подготовка разрешений"), System.ComponentModel.DefaultProperty("descript")]
    public class ConstrPerm : GeneralDocBase
    {
        public ConstrPerm()
            : base() { }
        public ConstrPerm(Session session)
            : base(session) { }
        
        //private string descript;

        private const string descriptFormat = "Разрешение на строительство № {DocNo} от {DocDate}";
        [Size(255), DisplayName("Описание")]//System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Разрешение на строительство № {0} от {1}", DocNo, DocDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        private string i_letterNo;
        [Size(32), DisplayName("Номер заявки (входящий)")]
        public string LetterNo
        {
            get
            {
                try
                {
                    if (i_letterNo == null || i_letterNo == "")
                    {
                        string res = String.Empty;
                        try
                        {
                            if (InLetter.Count > 0)
                                res = InLetter[0].RegNo;
                        }
                        catch { }
                        i_letterNo = res;
                    }
                }
                catch { }
                return i_letterNo;
            }
            set { SetPropertyValue("LetterNo", ref i_letterNo, value); }
        }
        private DateTime i_letterDate;
        [DisplayName("Дата заявки (входящяя)")]
        public DateTime LetterDate
        {
            get
            {
                try
                {
                    if (i_letterDate == DateTime.MinValue)
                    {
                        i_letterDate = DateTime.MinValue;
                        if (InLetter.Count > 0)
                            i_letterDate = InLetter[0].RegDate;
                    }
                }
                catch { }
                return i_letterDate;
            }
            set { SetPropertyValue("LetterDate", ref i_letterDate, value); }
        }
        private ePermKind i_PermKind;
        [DisplayName("Вид разрешения на строительство")]
        public ePermKind PermKind
        {
            get { return i_PermKind; }
            set { SetPropertyValue("PermKind", ref i_PermKind, value); }
        }

        private eWorkKind i_WorkKind;
        [DisplayName("Вид работ")]
        public eWorkKind WorkKind
        {
            get { return i_WorkKind; }
            set { try { SetPropertyValue("WorkKind", ref i_WorkKind, value); } catch { } }
        }

        private ConstrPerm i_ConstrPermInstead;
        [DisplayName("Разрешение взамен")]
        public ConstrPerm ConstrPermInstead
        {
            get { return i_ConstrPermInstead; }
            set { try { SetPropertyValue("ConstrPermInstead", ref i_ConstrPermInstead, value); } catch { } }
        }
        private string i_MapNo;
        [DisplayName("Номер в карте")]
        public string MapNo
        {
            get { return i_MapNo; }
            set { SetPropertyValue("MapNo", ref i_MapNo, value); }
        }
        private dConstructionType i_ConstructionType;
        [DisplayName("Тип объекта(справочник)")]
        public dConstructionType ConstructionType
        {
            get { return i_ConstructionType; }
            set
            {
                SetPropertyValue("ConstructionType", ref i_ConstructionType, value);
                try
                {
                    if (i_ConstructionType.ParentConstructionUsing != null)
                    {
                        if (i_ConstructionType.ConstructionTypes.Count == 0)
                            ConstructionCategory = i_ConstructionType.ParentConstructionUsing;
                        else
                            ConstructionCategory = i_ConstructionType;
                    }
                }
                catch { }
                OnChanged("ConstructionType");
            }
        }
        private dConstructionType i_ConstructionCategory;
        [DisplayName("Категория объекта")]
        public dConstructionType ConstructionCategory
        {
            get { return i_ConstructionCategory; }
            set
            {
                SetPropertyValue("ConstructionCategory", ref i_ConstructionCategory, value);
            }
        }
        private eBuildingKind i_BuildingKind;
        [DisplayName("Вид объекта")]
        [ImmediatePostData]
        public eBuildingKind BuildingKind
        {
            get { return i_BuildingKind; }
            set { SetPropertyValue("BuildingKind", ref i_BuildingKind, value); }
        }
        private eBuildingCategory i_BuildingCategory;
        [DisplayName("Назначение объекта строительства"), VisibleInListView(false)]
        public eBuildingCategory BuildingCategory
        {
            get { return i_BuildingCategory; }
            set { try { SetPropertyValue("BuildingCategory", ref i_BuildingCategory, value); } catch { } }
        }
        //private dConstructionType i_ConstructionType;
        //[DisplayName("Тип объекта(справочник)")]
        //[ImmediatePostData]
        //public dConstructionType ConstructionType
        //{
        //    get { return i_ConstructionType; }
        //    set { SetPropertyValue("ConstructionType", ref i_ConstructionType, value); }
        //}
        //private string _WorkKindText;
        //[DisplayName("Вид работ (текст)"), VisibleInListView(false)]
        //public string WorkKindText
        //{
        //    get { return _WorkKindText; }
        //    set { try { SetPropertyValue("WorkKindText", ref _WorkKindText, value); } catch { } }
        //}
        private string i_ObjectName;
        [Size(4000), DisplayName("Наименование объекта по проекту")]
        public string ObjectName
        {
            get { return i_ObjectName; }
            set { try { SetPropertyValue("ObjectName", ref i_ObjectName, value); } catch { } }
        }

        private string i_OrgExpertName;
        [Size(1000), DisplayName("Наименование организации, выдавшей заключение экспертизы и реквизиты приказа об утверждении")]
        public string OrgExpertName
        {
            get { return i_OrgExpertName; }
            set { try { SetPropertyValue("OrgExpertName", ref i_OrgExpertName, value); } catch { } }
        }

        private string i_ExpertInfo;
        [Size(1000), DisplayName("Сведения о заключении экспертизы и реквизиты приказа об утверждении")]
        public string ExpertInfo
        {
            get { return i_ExpertInfo; }
            set { try { SetPropertyValue("ExpertInfo", ref i_ExpertInfo, value); } catch { } }
        }

        private string i_LotCadNo;
        [Size(255), DisplayName("Кадастровый номер земельного участка(ов)")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get { return i_LotCadNo; }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }

        private string i_CadKvartalNo;
        [Size(255), DisplayName("Номер кадастрового квартала")]
        //[ImmediatePostData]
        public string CadKvartalNo
        {
            get
            {
                if (string.IsNullOrWhiteSpace(i_CadKvartalNo) && !string.IsNullOrWhiteSpace(i_LotCadNo))
                    i_CadKvartalNo = GetCadKvartalNo();
                return i_CadKvartalNo;
            }
            set { SetPropertyValue("CadKvartalNo", ref i_CadKvartalNo, value); }
        }

        private string GetCadKvartalNo()
        {
            string res = "";
            List<string> cadNoList = new List<string>();
            try
            {
                foreach(string cadNo in i_LotCadNo.Split(','))
                {
                    string c = cadNo.Remove(cadNo.LastIndexOf(':')).Trim();
                    if (res == "")
                        res = c;
                    else
                    if (!res.Contains(c))
                        res += ", " + c;
                }
                res = res.Trim().TrimEnd(',').Trim();
                //res = i_LotCadNo.Split(',')[0].Remove(i_LotCadNo.Split(',')[0].LastIndexOf(':'));
            }
            catch { }
            return res;
        }

        private string i_ObjChangeCadNo;
        [Size(255), DisplayName("Кадастровый номер реконструированного ОКС")]
        //[ImmediatePostData]
        public string ObjChangeCadNo
        {
            get { return i_ObjChangeCadNo; }
            set { SetPropertyValue("ObjChangeCadNo", ref i_ObjChangeCadNo, value); }
        }

        private string i_GPZU;
        [Size(255), DisplayName("Сведения о градостроительном плане земельного участка")]
        public string GPZU
        {
            get { return i_GPZU; }
            set { SetPropertyValue("GPZU", ref i_GPZU, value); }
        }

        private string i_PlanningDocInfo;
        [Size(255), DisplayName("Сведения о проекте планировки и проекте межевания")]
        public string PlanningDocInfo
        {
            get { return i_PlanningDocInfo; }
            set { SetPropertyValue("PlanningDocInfo", ref i_PlanningDocInfo, value); }
        }

        private string i_ProjectDocInfo;
        [Size(255), DisplayName("Сведения о проектной документации")]
        public string ProjectDocInfo
        {
            get { return i_ProjectDocInfo; }
            set { SetPropertyValue("ProjectDocInfo", ref i_ProjectDocInfo, value); }
        }

        private int i_StageCount;
        [DisplayName("Количество этапов"), VisibleInListView(false)]
        public int StageCount
        {
            get { return i_StageCount; }
            set { SetPropertyValue("StageCount", ref i_StageCount, value); }
        }

        private string i_ObjectEstateComplexName;
        [Size(1000), DisplayName("Наименование ОКС, входящего в состав имущественного комплекса")]
        public string ObjectEstateComplexName
        {
            get { return i_ObjectEstateComplexName; }
            set { SetPropertyValue("ObjectEstateComplexName", ref i_ObjectEstateComplexName, value); }
        }

        //private CityRegion i_CityRegion;
        //[DisplayName("Район города")]
        //public CityRegion CityRegion
        //{
        //    get { return i_CityRegion; }
        //    set { try { SetPropertyValue("CityRegion", ref i_CityRegion, value); } catch { } }
        //}

        private string i_ObjectAddress;
        [Size(1000), DisplayName("Адрес (местоположение) объекта")]
        public string ObjectAddress
        {
            get { return i_ObjectAddress; }
            set { try { SetPropertyValue("ObjectAddress", ref i_ObjectAddress, value); } catch { } }
        }

        private DateTime i_ValidDate;
        [DisplayName("Срок действия разрешения")]
        public DateTime ValidDate
        {
            get { return i_ValidDate; }
            set { SetPropertyValue("ValidDate", ref i_ValidDate, value);
                //OnChanged("ValidDate");
            }
        }
        private DateTime i_ValidDateTotal;
        [DisplayName("Срок действия разрешения (с продлениями)")]
        public DateTime ValidDateTotal
        {
            get {
                try
                {
                    if (PermProlongues.Count > 0)
                    {
                        SortProperty sort = new SortProperty("ValidDateProlongue", DevExpress.Xpo.DB.SortingDirection.Descending);
                        PermProlongues.Sorting.Add(sort);
                        i_ValidDateTotal = PermProlongues[0].ValidDateProlongue;
                    }
                    else
                        i_ValidDateTotal = ValidDate;
                }
                catch { }
                return i_ValidDateTotal; }
            set
            {
                SetPropertyValue("ValidDateTotal", ref i_ValidDateTotal, value);
                
                //SetConstrPlaningYearInUse();
                //OnChanged("ValidDateTotal");
            }
        }
        private void SetConstrPlaningYearInUse()
        {
            //if (!Session.IsObjectsLoading)
            {
                foreach (CapitalStructureBase constrObj in this.CapitalStructureBase)
                {
                    try
                    {
                        constrObj.PlaningYearInUse = i_ValidDateTotal.Year;
                        constrObj.Save();
                    }
                    catch { }
                }
                foreach (ConstrStage constrStage in this.ConstrStages)
                {
                    //try
                    //{
                        constrStage.PlaningYearInUse = i_ValidDateTotal.Year;
                        constrStage.Save();
                    //}
                    //catch { }
                }
            }
        }

        private DateTime i_ProlongDateLast;
        [DisplayName("Дата последнего продления")]
        public DateTime ProlongDateLast
        {
            get
            {
                try
                {
                    if (PermProlongues.Count > 0)
                    {
                        SortProperty sort = new SortProperty("DateProlongue", DevExpress.Xpo.DB.SortingDirection.Descending);
                        PermProlongues.Sorting.Add(sort);
                        i_ProlongDateLast = PermProlongues[0].DateProlongue;
                    }
                    else
                        i_ProlongDateLast = DateTime.MinValue;
                }
                catch { }
                return i_ProlongDateLast;
            }
            set { SetPropertyValue("ProlongDateLast", ref i_ProlongDateLast, value); }
        }
        //private string i_ValidDateAccording;
        //[Size(1000),DisplayName("в соответствии с")]
        //public string ValidDateAccording
        //{
        //    get { return i_ValidDateAccording; }
        //    set { SetPropertyValue("ValidDateAccording", ref i_ValidDateAccording, value); }
        //}

        private string i_ConstrDeveloperString;
        [Size(500), DisplayName("Застройщик")]
        public string ConstrDeveloperString
        {
            get
            {
                //string res = String.Empty;
                //if (!String.IsNullOrEmpty(i_ConstrDeveloperString))
                //    return i_ConstrDeveloperString;
                //else
                //{
                    return i_ConstrDeveloperString;
                //}
            }
            set { try { SetPropertyValue("ConstrDeveloperString", ref i_ConstrDeveloperString, value); } catch { } }
        }
        private string i_ConstrDeveloperStringDat;
        [Size(500), DisplayName("Застройщик (дат.падеж)")]
        public string ConstrDeveloperStringDat
        {
            get
            {
                return i_ConstrDeveloperStringDat;
            }
            set { try { SetPropertyValue("ConstrDeveloperString", ref i_ConstrDeveloperStringDat, value); } catch { } }
        }
        private string i_ConstrDeveloperContactInfo;
        [Size(500), DisplayName("Реквизиты застройщика")]
        public string ConstrDeveloperContactInfo
        {
            get
            {
                return i_ConstrDeveloperContactInfo;
            }
            set { SetPropertyValue("ConstrDeveloperContactInfo", ref i_ConstrDeveloperContactInfo, value); }
        }
        private string i_ConstrDeveloperINN;
        [Size(255), DisplayName("ИНН застройщика")]
        public string ConstrDeveloperINN
        {
            get
            {
                return i_ConstrDeveloperINN;
            }
            set { try { SetPropertyValue("ConstrDeveloperINN", ref i_ConstrDeveloperINN, value); } catch { } }
        }
        //private ConstrDeveloper i_ConstrDeveloper;
        //[Association, DisplayName("Застройщик"), VisibleInListView(false)]
        //public ConstrDeveloper ConstrDeveloper
        //{
        //    get {
        //        try
        //        {
        //            Connect connect = Connect.FromSession(Session);
        //            ConstrDeveloper constrDeveloper = connect.FindFirstObject<ConstrDeveloper>(mc => mc.RequesterString == this.InLetter[0].Subject);
        //            if (constrDeveloper != null)
        //                i_ConstrDeveloper = constrDeveloper;
        //        }
        //        catch { }
        //        return i_ConstrDeveloper; }
        //    set { try { SetPropertyValue("ConstrDeveloper", ref i_ConstrDeveloper, value); } catch { } }
        //}


        private string i_TotalBuildSquareProject;
        [Size(500),DisplayName("Общая площадь (кв.м)"), VisibleInListView(false)]
        public string TotalBuildSquareProject
        {
            get { return i_TotalBuildSquareProject; }
            set { SetPropertyValue("TotalBuildSquareProject", ref i_TotalBuildSquareProject, value); }
        }

        private string i_LotSquare;
        [Size(500), DisplayName("Площадь участка (кв.м)"), VisibleInListView(false)]
        public string LotSquare
        {
            get { return i_LotSquare; }
            set { SetPropertyValue("LotSquare", ref i_LotSquare, value); }
        }

        private string i_BuildingSizeProject;
        [Size(500), DisplayName("Объем (куб.м)"), VisibleInListView(false)]
        public string BuildingSizeProject
        {
            get { return i_BuildingSizeProject; }
            set { SetPropertyValue("BuildingSizeProject", ref i_BuildingSizeProject, value); }
        }

        private string i_BuildingSizeUnderGroundPartProject;
        [Size(500), DisplayName("Объем подземной части (куб.м)"), VisibleInListView(false)]
        public string BuildingSizeUnderGroundPartProject
        {
            get { return i_BuildingSizeUnderGroundPartProject; }
            set { SetPropertyValue("BuildingSizeUnderGroundPartProject", ref i_BuildingSizeUnderGroundPartProject, value); }
        }

        private string i_Height;
        [Size(500), DisplayName("Высота (м.)")]
        public string Height
        {
            get { return i_Height; }
            set { SetPropertyValue("Height", ref i_Height, value); }
        }

        // Этажность ----------------------------
        private string i_FloorCount;
        [Size(500), DisplayName("Количество этажей (шт.)")]
        public string FloorCount
        {
            get { return i_FloorCount; }
            set { SetPropertyValue("FloorCount", ref i_FloorCount, value); }
        }

        private string i_UnderGroundFloorCount;
        [Size(500), DisplayName("Количество подземных этажей (шт.)")]
        public string UnderGroundFloorCount
        {
            get { return i_UnderGroundFloorCount; }
            set { SetPropertyValue("UnderGroundFloorCount", ref i_UnderGroundFloorCount, value); }
        }
        //private int i_FloorCountAbove;
        //[DisplayName("Этажность")]
        //public int FloorCountAbove
        //{
        //    get { return i_FloorCountAbove; }
        //    set { SetPropertyValue("FloorCountAbove", ref i_FloorCountAbove, value); }
        //}
        //private string i_FloorsNote;
        //[DisplayName("Примечание по этажности")]
        //public string FloorsNote
        //{
        //    get { return i_FloorsNote; }
        //    set { SetPropertyValue("FloorsNote", ref i_FloorsNote, value); }
        //}
        // ------------------------------------
        private string i_Capacity;
        [Size(500), DisplayName("Вместимость (чел.)"), VisibleInListView(false)]
        public string Capacity
        {
            get { return i_Capacity; }
            set { SetPropertyValue("Capacity", ref i_Capacity, value); }
        }

        private string i_BuildSquare;
        [Size(500), DisplayName("Площадь застройки (кв.м)"), VisibleInListView(false)]
        public string BuildSquare
        {
            get { return i_BuildSquare; }
            set { SetPropertyValue("BuildSquare", ref i_BuildSquare, value); }
        }
        private string i_TotalLivingSpaceProject;
        [Size(500), DisplayName("Площадь жилых помещений с балконами (кв.м)")]
        public string TotalLivingSpaceProject
        {
            get
            {
                return i_TotalLivingSpaceProject;
            }
            set { SetPropertyValue("TotalLivingSpaceProject", ref i_TotalLivingSpaceProject, value); }
        }

        private string i_OKSOtherIndicators;
        [Size(500), DisplayName("Иные показатели"), VisibleInListView(false)]
        public string OKSOtherIndicators
        {
            get { return i_OKSOtherIndicators; }
            set { SetPropertyValue("OKSOtherIndicators", ref i_OKSOtherIndicators, value); }
        }

        /// <summary>
        /// Линейные объекты
        /// </summary>
        /// LineObjectClass,LengthProject,PowerLineProject,PipesInfoProject,ElectricLinesInfoProject,ConstructiveElementsInfoProject,OtherIndicatorsLineProject
        private LineObjectClass i_LineObjectClass;
        [DisplayName("Категория (класс) линейного объекта"), VisibleInListView(false)]
        public LineObjectClass LineObjectClass
        {
            get { return i_LineObjectClass; }
            set { SetPropertyValue("LineObjectClass", ref i_LineObjectClass, value); }
        }

        private string i_LengthProject;
        [DisplayName("Протяженность"), VisibleInListView(false)]
        public string LengthProject
        {
            get { return i_LengthProject; }
            set { SetPropertyValue("LengthProject", ref i_LengthProject, value); }
        }

        private string i_PowerLineProject;
        [DisplayName("Мощность"), VisibleInListView(false)]
        public string PowerLineProject
        {
            get { return i_PowerLineProject; }
            set { SetPropertyValue("PowerLineProject", ref i_PowerLineProject, value); }
        }

        //private string i_PipesInfoProject;
        //[Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб"), VisibleInListView(false)]
        //public string PipesInfoProject
        //{
        //    get { return i_PipesInfoProject; }
        //    set { SetPropertyValue("PipesInfoProject", ref i_PipesInfoProject, value); }
        //}

        private string i_ElectricLinesInfoProject;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи"), VisibleInListView(false)]
        public string ElectricLinesInfoProject
        {
            get { return i_ElectricLinesInfoProject; }
            set { SetPropertyValue("ElectricLinesInfoProject", ref i_ElectricLinesInfoProject, value); }
        }

        private string i_ConstructiveElementsInfoProject;
        [Size(1000), DisplayName("Перечень конструктивных элементов"), VisibleInListView(false)]
        public string ConstructiveElementsInfoProject
        {
            get { return i_ConstructiveElementsInfoProject; }
            set { SetPropertyValue("ConstructiveElementsInfoProject", ref i_ConstructiveElementsInfoProject, value); }
        }

        private string i_OtherIndicatorsLineProject;
        [DisplayName("Иные показатели"), VisibleInListView(false)]
        public string OtherIndicatorsLineProject
        {
            get { return i_OtherIndicatorsLineProject; }
            set { SetPropertyValue("OtherIndicatorsLineProject", ref i_OtherIndicatorsLineProject, value); }
        }


        //[Size(255), DisplayName("Краткие характеристики объекта"), VisibleInListView(false)]
        //public string shortObjectDescription
        //{
        //    get { return _ShortObjectDescription; }
        //    set { try { _ShortObjectDescription = value; } catch { } }
        //}

        //[DisplayName("Со сносом"), VisibleInListView(false)]
        //public bool removeFlag
        //{
        //    get { return _RemoveFlag; }
        //    set { try { _RemoveFlag = value; } catch { } }
        //}
        //[DisplayName("С экспертизой"), VisibleInListView(false)]
        //public bool examinationFlag
        //{
        //    get { return _ExaminationFlag; }
        //    set { try { _ExaminationFlag = value; } catch { } }
        //}


        #region //Информационный лист (согласования)
        private string i_LetterInfo;
        [Size(500), DisplayName("Основание для рассмотрения"), VisibleInListView(false)]
        public string LetterInfo
        {
            get { return i_LetterInfo; }
            set { SetPropertyValue("LetterInfo", ref i_LetterInfo, value); }
        }
        private string i_PermInfo;
        [Size(255), DisplayName("Выдача разрешения"), VisibleInListView(false)]
        public string PermInfo
        {
            get { return i_PermInfo; }
            set { SetPropertyValue("PermInfo", ref i_PermInfo, value); }
        }
        private string i_PermInfoR;
        [Size(255), DisplayName("Отказ в выдаче разрешения"), VisibleInListView(false)]
        public string PermInfoR
        {
            get { return i_PermInfoR; }
            set { SetPropertyValue("PermInfoR", ref i_PermInfoR, value); }
        }
        private Employee i_Decision1;
        [DisplayName("Решение 1"), VisibleInListView(false)]
        public Employee Decision1
        {
            get { return i_Decision1; }
            set { SetPropertyValue("Decision1", ref i_Decision1, value); }
        }
        private Employee i_Decision2;
        [DisplayName("Решение 2"), VisibleInListView(false)]
        public Employee Decision2
        {
            get { return i_Decision2; }
            set { SetPropertyValue("Decision2", ref i_Decision2, value); }
        }
        private Employee i_AgreementEmploye1;
        [DisplayName("Согласующий 1"), VisibleInListView(false)]
        public Employee AgreementEmploye1
        {
            get { return i_AgreementEmploye1; }
            set { SetPropertyValue("AgreementEmploye1", ref i_AgreementEmploye1, value); }
        }
        private Employee i_AgreementEmploye2;
        [DisplayName("Согласующий 2"), VisibleInListView(false)]
        public Employee AgreementEmploye2
        {
            get { return i_AgreementEmploye2; }
            set { SetPropertyValue("AgreementEmploye2", ref i_AgreementEmploye2, value); }
        }
        private Employee i_AgreementEmploye3;
        [DisplayName("Согласующий 3"), VisibleInListView(false)]
        public Employee AgreementEmploye3
        {
            get { return i_AgreementEmploye3; }
            set { SetPropertyValue("AgreementEmploye3", ref i_AgreementEmploye3, value); }
        }
        private Employee i_AgreementEmploye4;
        [DisplayName("Согласующий 4"), VisibleInListView(false)]
        public Employee AgreementEmploye4
        {
            get { return i_AgreementEmploye4; }
            set { SetPropertyValue("AgreementEmploye4", ref i_AgreementEmploye4, value); }
        }
        private Employee i_AgreementEmploye5;
        [DisplayName("Согласующий 5"), VisibleInListView(false)]
        public Employee AgreementEmploye5
        {
            get { return i_AgreementEmploye5; }
            set { SetPropertyValue("AgreementEmploye5", ref i_AgreementEmploye5, value); }
        }
        private Employee i_AgreementEmploye6;
        [DisplayName("Согласующий 6"), VisibleInListView(false)]
        public Employee AgreementEmploye6
        {
            get { return i_AgreementEmploye6; }
            set { SetPropertyValue("AgreementEmploye6", ref i_AgreementEmploye6, value); }
        }
        private Employee i_AgreementEmploye7;
        [DisplayName("Согласующий 7"), VisibleInListView(false)]
        public Employee AgreementEmploye7
        {
            get { return i_AgreementEmploye7; }
            set { SetPropertyValue("AgreementEmploye7", ref i_AgreementEmploye7, value); }
        }
        #endregion

        [Association, DisplayName("Застройщики")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }
        //[Association, DisplayName("Сведения об ОКС")]
        //public XPCollection<ObjectConstractionInfo> ObjectConstractionInfo
        //{
        //    get
        //    {
        //        return GetCollection<ObjectConstractionInfo>("ObjectConstractionInfo");
        //    }
        //}
        [Association, DisplayName("Стадии строительства")]
        public XPCollection<ConstrStage> ConstrStages
        {
            get
            {
                return GetCollection<ConstrStage>("ConstrStages");
            }
        }
        [Association, DisplayName("Разрешение на ввод объекта в эксплуатацию")]
        public XPCollection<UsePerm> UsePerm
        {
            get
            {
                return GetCollection<UsePerm>("UsePerm");
            }
        }
        [Association, DisplayName("Продления разрешения")]
        public XPCollection<PermProlongue> PermProlongues
        {
            get
            {
                return GetCollection<PermProlongue>("PermProlongues");
            }
        }

        //public void CheckPermDates()
        //{
        //    if(Date)
        //}
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            Connect connect = Connect.FromSession(Session);
            this.DocKind = Session.FindObject<dDocKind>(new BinaryOperator("Name", "Разрешение на строительство"));
            //if (this.ObjectAddress == String.Empty || this.ObjectAddress == null)
            //{
            //    this.ObjectAddress = "";
            //}
            //this.DocNo = "35-32357000-         -" + DateTime.Now.Year.ToString();

            if (Empl == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    Empl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            if(signer == null)
            {
                signer = connect.FindFirstObject<Signer>(x => x.DefaultSignerFlag == true);
            }
            Session.CommitTransaction();

        }

        protected override void OnSaved()
        {
            if (!IsDeleted)
            {
                //SetConstrPlaningYearInUse();
                //try
                //{
                //    if (this.DocNo == String.Empty || this.DocNo == null)
                //    {
                //        double InitialRegNo = 0;
                //        try
                //        {
                //            InitialRegNo = this.DocKind.InitialRegNo;
                //        }
                //        catch { }
                //        if (DateTime.Now.Year.ToString() != "2016")
                //            InitialRegNo = 0;
                //        this.DocNo = Convert.ToString(InitialRegNo + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + DateTime.Now.Year.ToString(),
                //            string.Empty));
                //        this.DocNo = "35-32357000" + this.DocNo + "-" + DateTime.Now.Year.ToString();
                //    }
                //}
                //catch { }
                //this.DocDate = DateTime.Now.Date;
            }
            base.OnSaved();
        }

        #region Логика при изменении XPCollections
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //изменилась коллекция входящих обращений
            if (property.Name == "InLetter")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(InLetters_CollectionChanged);
            }
            //изменилась коллекция застройщиков
            if (property.Name == "DocSubjects")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(DocSubjects_CollectionChanged);
            }
            //изменилась коллекция земельных участков
            if (property.Name == "Parcels")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            }
            return result;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции входящих обращений - заполнение листа по застройщикам
        ///// </summary>
        private void InLetters_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                InLetter newObj = (InLetter)e.ChangedObject;
                if (newObj.DocSubjects.Count > 0)
                {
                    foreach (GeneralSubject DocSubject in newObj.DocSubjects)
                    {
                        DocSubjects.Add(DocSubject);

                        foreach(CapitalStructureBase constrObj in CapitalStructureBase)
                        {
                            constrObj.DocSubjects.Add(DocSubject);
                        }
                        foreach (ConstrStage constrObj in ConstrStages)
                        {
                            constrObj.DocSubjects.Add(DocSubject);
                        }
                    }

                }
                
            }
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции застройщиков - заполнение поля по застройщикам
        ///// </summary>
        private void DocSubjects_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetDevelopers();
            }
        }
        /// <summary>
        /// Заполенение поля Застройщик (string) по листу застройщиков (XPCollection)
        /// </summary>
        private void SetDevelopers()
        {
            string dev = "";
            string devdat = "";
            string contact = "";
            string inn = "";
            foreach (GeneralSubject subj in DocSubjects)
            {
                // именительный падеж
                if (dev == "")
                {
                    dev = subj.FullName;
                }
                else
                    if (!dev.Contains(subj.FullName))
                        dev += ", " + subj.FullName;

                //  дательный падеж застройщика
                string devdattemp = "";
                if(subj.NameDat != null && subj.NameDat != "")
                {
                    devdattemp = subj.NameDat;
                }
                else
                    devdattemp = subj.FullName;
                if (devdat == "")
                {
                    devdat = devdattemp;
                }
                else
                    if (!devdat.Contains(devdattemp))
                    devdat += ", " + devdattemp;

                // контактная информация
                try
                {
                    if (contact == "")
                    {
                        contact = subj.FullContactInfo;
                    }
                    else
                        if (!contact.Contains(subj.FullContactInfo))
                        contact += ", " + subj.FullContactInfo;
                }
                catch { }
                // ИНН
                try
                {
                    if (!String.IsNullOrEmpty(subj.INN))
                    {
                        if (inn == "")
                        {
                            inn = subj.INN;
                        }
                        else
                          if (!inn.Contains(subj.INN))
                            inn += ", " + subj.INN;
                    }
                }
                catch { }
            }
            if (dev.Length > 500)
                dev = dev.Remove(499, dev.Length);
            ConstrDeveloperString = dev;

            if (devdat.Length > 500)
                devdat = devdat.Remove(499, devdat.Length);
            ConstrDeveloperStringDat = devdat;

            if (contact.Length > 500)
                contact = contact.Remove(499, contact.Length);
            ConstrDeveloperContactInfo = contact;

            if (inn.Length > 255)
                inn = inn.Remove(254, inn.Length);
            ConstrDeveloperINN = inn;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции земельных участков - заполнение поля кадастровых номеров
        ///// </summary>
        private void Parcels_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetCadNoms();
            }
        }
        
        /// <summary>
        /// Заполенение поля кадастровых номеров(string) по листу земельных участков (XPCollection)
        /// </summary>
        private void SetCadNoms()
        {
            string res = "";
            foreach (Parcel obj in Parcels)
            {
                if (res == "")
                    res = obj.CadastralNumber;
                else
                    if (!res.Contains(obj.CadastralNumber))
                    res += ", " + obj.CadastralNumber;
            }
            if (res.Length > 255)
                res = res.Remove(254, res.Length);
            LotCadNo = res;
        }
        #endregion


    }
}
