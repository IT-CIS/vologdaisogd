using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.DocFlow;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.Perm
{
    [ModelDefault("Caption", "����������� �������"),
    System.ComponentModel.DefaultProperty("FIO")]
    public class CadEngineer: BaseObjectXAF
    {
        public CadEngineer(Session session) : base(session) { }

        /// <summary>
        /// ����������� �������
        /// </summary>
        private string i_FIO;
        [Size(255), DisplayName("���")]
        public string FIO
        {
            get { return i_FIO; }
            set { SetPropertyValue("FIO", ref i_FIO, value); }
        }

        private string i_NCertificate;
        [Size(50), DisplayName("����� ����������������� ��������� ������������ ��������")]
        public string NCertificate
        {
            get { return i_NCertificate; }
            set { SetPropertyValue("NCertificate", ref i_NCertificate, value); }
        }

        private DateTime i_NCertificateDate;
        [DisplayName("���� ������ ���������")]
        public DateTime NCertificateDate
        {
            get
            {
                return i_NCertificateDate;
            }
            set { SetPropertyValue("NCertificateDate", ref i_NCertificateDate, value); }
        }
        private string i_Authority;
        [Size(255), DisplayName("����� ������, �������� ��������")]
        public string Authority
        {
            get { return i_Authority; }
            set { SetPropertyValue("Authority", ref i_Authority, value); }
        }
        private DateTime i_EntryDataDate;
        [DisplayName("���� �������� �������� � ������ ���������")]
        public DateTime EntryDataDate
        {
            get
            {
                return i_EntryDataDate;
            }
            set { SetPropertyValue("EntryDataDate", ref i_EntryDataDate, value); }
        }
        
    }
}