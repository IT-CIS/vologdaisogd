﻿using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Land;
using DevExpress.ExpressApp.Model;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace AISOGD.Perm
{
    /// <summary>
    /// Разрешение на строительство
    /// </summary>
    [Appearance("isLivingUsePerm", AppearanceItemType = "ViewItem", Priority = 1,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,Жилой#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"PlacesCountProject,PlacesCountFakt,RoomCountProject,RoomCountFakt,
ProductPurposeObjectType,PowerProject,PowerFakt,ProductivityProject,ProductivityFakt,
LineObjectClass,LengthProject,LengthFakt,PowerLineProject,PowerLineFakt,PipesInfoProject,PipesInfoFakt,ElectricLinesInfoProject,ElectricLinesInfoFakt,
ConstructiveElementsInfoProject,ConstructiveElementsInfoFakt")]
    [Appearance("isNotLivingUsePerm", AppearanceItemType = "ViewItem", Priority = 2,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,Нежилой#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"LivingSpaceProject,LivingSpaceFakt,TotalLivingSpaceProject,TotalLivingSpaceFakt,NotLivingSpaceProject,NotLivingSpaceFakt,SectionCountProject,
SectionCountFakt,PorchCountProject,PorchCountFakt,AppartmentsCountProject,AppartmentsCountFakt,StudioCountProject,StudioSpaceProject,OneRoomFlatCountProject,
OneRoomFlatSpaceProject,TwoRoomFlatCountProject,TwoRoomFlatSpaceProject,ThreeRoomFlatCountProject,ThreeRoomFlatSpaceProject,FourRoomFlatCountProject,
FourRoomSpaceProject,MoreRoomFlatCountProject,MoreRoomSpaceProject,StudioCountFakt,StudioSpaceFakt,OneRoomFlatCountFakt,OneRoomFlatSpaceFakt,TwoRoomFlatCountFakt,
TwoRoomFlatSpaceFakt,ThreeRoomFlatCountFakt,ThreeRoomFlatSpaceFakt,FourRoomFlatCountFakt,FourRoomSpaceFakt,MoreRoomFlatCountFakt,MoreRoomSpaceFakt,
ElitAppartmentsCountProject,ElitAppartmentsCountFakt,ElitAppartmentsSpaceProject,ElitAppartmentsSpaceFakt,BalconySpaceProject,BalconySpaceFakt,
ProductPurposeObjectType,PowerProject,PowerFakt,ProductivityProject,ProductivityFakt,
LineObjectClass,LengthProject,LengthFakt,PowerLineProject,PowerLineFakt,PipesInfoProject,PipesInfoFakt,ElectricLinesInfoProject,ElectricLinesInfoFakt,
ConstructiveElementsInfoProject,ConstructiveElementsInfoFakt")]
    [Appearance("isProduktObjectUsePerm", AppearanceItemType = "ViewItem", Priority = 3,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,ПроизводственногоНазначения#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"PlacesCountProject,PlacesCountFakt,RoomCountProject,RoomCountFakt,
LivingSpaceProject,LivingSpaceFakt,TotalLivingSpaceProject,TotalLivingSpaceFakt,NotLivingSpaceProject,NotLivingSpaceFakt,SectionCountProject,
SectionCountFakt,PorchCountProject,PorchCountFakt,AppartmentsCountProject,AppartmentsCountFakt,StudioCountProject,StudioSpaceProject,OneRoomFlatCountProject,
OneRoomFlatSpaceProject,TwoRoomFlatCountProject,TwoRoomFlatSpaceProject,ThreeRoomFlatCountProject,ThreeRoomFlatSpaceProject,FourRoomFlatCountProject,
FourRoomSpaceProject,MoreRoomFlatCountProject,MoreRoomSpaceProject,StudioCountFakt,StudioSpaceFakt,OneRoomFlatCountFakt,OneRoomFlatSpaceFakt,TwoRoomFlatCountFakt,
TwoRoomFlatSpaceFakt,ThreeRoomFlatCountFakt,ThreeRoomFlatSpaceFakt,FourRoomFlatCountFakt,FourRoomSpaceFakt,MoreRoomFlatCountFakt,MoreRoomSpaceFakt,
ElitAppartmentsCountProject,ElitAppartmentsCountFakt,ElitAppartmentsSpaceProject,ElitAppartmentsSpaceFakt,BalconySpaceProject,BalconySpaceFakt,
LineObjectClass,LengthProject,LengthFakt,PowerLineProject,PowerLineFakt,PipesInfoProject,PipesInfoFakt,ElectricLinesInfoProject,ElectricLinesInfoFakt,
ConstructiveElementsInfoProject,ConstructiveElementsInfoFakt")]
    [Appearance("isLineObjectUsePerm", AppearanceItemType = "ViewItem", Priority = 4,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,Линейные#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"PlacesCountProject,PlacesCountFakt,RoomCountProject,RoomCountFakt,CapacityProject,CapacityFakt,
LivingSpaceProject,LivingSpaceFakt,TotalLivingSpaceProject,TotalLivingSpaceFakt,NotLivingSpaceProject,NotLivingSpaceFakt,SectionCountProject,
SectionCountFakt,PorchCountProject,PorchCountFakt,AppartmentsCountProject,AppartmentsCountFakt,StudioCountProject,StudioSpaceProject,OneRoomFlatCountProject,
OneRoomFlatSpaceProject,TwoRoomFlatCountProject,TwoRoomFlatSpaceProject,ThreeRoomFlatCountProject,ThreeRoomFlatSpaceProject,FourRoomFlatCountProject,
FourRoomSpaceProject,MoreRoomFlatCountProject,MoreRoomSpaceProject,StudioCountFakt,StudioSpaceFakt,OneRoomFlatCountFakt,OneRoomFlatSpaceFakt,TwoRoomFlatCountFakt,
TwoRoomFlatSpaceFakt,ThreeRoomFlatCountFakt,ThreeRoomFlatSpaceFakt,FourRoomFlatCountFakt,FourRoomSpaceFakt,MoreRoomFlatCountFakt,MoreRoomSpaceFakt,
ElitAppartmentsCountProject,ElitAppartmentsCountFakt,ElitAppartmentsSpaceProject,ElitAppartmentsSpaceFakt,BalconySpaceProject,BalconySpaceFakt,
ProductPurposeObjectType,PowerProject,PowerFakt,ProductivityProject,ProductivityFakt,
FloorCountProject,FloorCountFakt,UnderGroundFloorCounProject,UnderGroundFloorCountFakt,FloorCountAboveProject,FloorCountAboveFakt,FloorCountDiffProject,
FloorCountDiffFakt,FloorsNoteProject,FloorsNoteFakt,
ElevatorsCountProject,ElevatorsCountFakt,EscalatorsCountProject,EscalatorsCountFakt,InvalidLiftsCountProject,InvalidLiftsCountFakt,
FundMaterialProject,FundMaterialFakt,WallMaterialProject,WallMaterialFakt,BorderMaterialProject,BorderMaterialFakt,RoofMaterialProject,RoofMaterialFakt,
EnergyEfficiencyClassProject,EnergyEfficiencyClassFakt,HeatConsumptionProject,HeatConsumptionFakt,OutdoorIsolationMaterialProject,OutdoorIsolationMaterialFakt,
SkylightsFillingProject,SkylightsFillingFakt,
ElectroProject,ElectroFakt,HeatProject,HeatFakt,WaterProject,WaterFakt,GazProject,GazFakt,HouseSeverageProject,HouseSeverageFakt,PhonesProject,PhonesFakt,
TVProject,TVFakt,RadioProject,RadioFakt,SeverageProject,SeverageFakt")]
    [ModelDefault("Caption", "Разрешение на ввод объекта в эксплуатацию"), NavigationItem("Подготовка документов"), System.ComponentModel.DefaultProperty("descript")]
    public class UsePerm : GeneralDocBase
    {
        public UsePerm(Session session)
            : base(session) { }


        private const string descriptFormat = "Разрешение на ввод объекта в эксплуатацию № {DocNo} от {DocDate}";
        [Size(255), DisplayName("Описание")]
        //[Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Разрешение на ввод объекта в эксплуатацию № {0} от {1}", DocNo, DocDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        private string i_letterNo;
        [Size(32), DisplayName("Номер заявки (входящий)")]
        public string LetterNo
        {
            get
            {
                if (i_letterNo == null || i_letterNo == "")
                {
                    string res = String.Empty;
                    try
                    {
                        if (InLetter.Count > 0)
                            res = InLetter[0].RegNo;
                    }
                    catch { }
                    i_letterNo = res;
                }
                return i_letterNo;
            }
            set { SetPropertyValue("LetterNo", ref i_letterNo, value); }
        }
        private DateTime i_letterDate;
        [DisplayName("Дата заявки (входящяя)")]
        public DateTime LetterDate
        {
            get
            {
                if (i_letterDate == DateTime.MinValue)
                {
                    try
                    {
                        i_letterDate = DateTime.MinValue;
                        if (InLetter.Count > 0)
                            i_letterDate = InLetter[0].RegDate;
                    }
                    catch { }
                }
                return i_letterDate;
            }
            set { SetPropertyValue("LetterDate", ref i_letterDate, value); }
        }

        private eWorkKind i_WorkKind;
        [DisplayName("Вид работ")]
        public eWorkKind WorkKind
        {
            get { return i_WorkKind; }
            set { try { SetPropertyValue("WorkKind", ref i_WorkKind, value); } catch { } }
        }
        private string i_MapNo;
        [DisplayName("Номер в карте")]
        public string MapNo
        {
            get { return i_MapNo; }
            set { SetPropertyValue("MapNo", ref i_MapNo, value); }
        }
        private dConstructionType i_ConstructionType;
        [DisplayName("Тип объекта(справочник)")]
        public dConstructionType ConstructionType
        {
            get { return i_ConstructionType; }
            set
            {
                SetPropertyValue("ConstructionType", ref i_ConstructionType, value);
                try
                {
                    if (i_ConstructionType.ParentConstructionUsing != null)
                    {
                        if (i_ConstructionType.ConstructionTypes.Count == 0)
                            ConstructionCategory = i_ConstructionType.ParentConstructionUsing;
                        else
                            ConstructionCategory = i_ConstructionType;
                    }
                }
                catch { }
                OnChanged("ConstructionType");
            }
        }
        private dConstructionType i_ConstructionCategory;
        [DisplayName("Категория объекта")]
        public dConstructionType ConstructionCategory
        {
            get { return i_ConstructionCategory; }
            set
            {
                SetPropertyValue("ConstructionCategory", ref i_ConstructionCategory, value);
            }
        }
        private string i_ConstrPermInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("Реквизиты РС")]
        public string ConstrPermInfo
        {
            get { return i_ConstrPermInfo; }
            set { SetPropertyValue("ConstrPermInfo", ref i_ConstrPermInfo, value); }
        }
        private eBuildingCategory i_BuildingCategory;
        [DisplayName("Назначение объекта строительства"), VisibleInListView(false)]
        public eBuildingCategory BuildingCategory
        {
            get { return i_BuildingCategory; }
            set { try { SetPropertyValue("BuildingCategory", ref i_BuildingCategory, value); } catch { } }
        }

        
        private string i_ObjectName;
        [Size(4000), DisplayName("Наименование объекта")]
        public string ObjectName
        {
            get {
                //if (i_ObjectName == null || i_ObjectName == String.Empty)
                //{
                //    if (ConstrPerm != null)
                //        if (this.ConstrPermLink.ObjectName != null)
                //            i_ObjectName = this.ConstrPermLink.ObjectName;
                //}
                return i_ObjectName;
            }
            set { try { SetPropertyValue("ObjectName", ref i_ObjectName, value); } catch { } }
        }

        private string i_ObjChangeCadNo;
        [Size(255), DisplayName("Кадастровый номер объекта")]
        //[ImmediatePostData]
        public string ObjChangeCadNo
        {
            get {
                //if (i_ObjChangeCadNo == null || i_ObjChangeCadNo == String.Empty)
                //{
                //    if (this.ConstrPermLink != null)
                //        if (this.ConstrPermLink.ObjChangeCadNo != null)
                //            i_ObjChangeCadNo = this.ConstrPermLink.ObjChangeCadNo;
                //}
                return i_ObjChangeCadNo; }
            set { SetPropertyValue("ObjChangeCadNo", ref i_ObjChangeCadNo, value); }
        }

        private string i_AddressObjectName;
        [Size(SizeAttribute.Unlimited), DisplayName("Адресная привязка объекта")]
        public string AddressObjectName
        {
            get {
                //if (i_AddressObjectName == null || i_AddressObjectName == String.Empty)
                //{
                //    if (this.ConstrPermLink != null)
                //        if (this.ConstrPermLink.ObjectAddress != null)
                //            i_AddressObjectName = this.ConstrPermLink.ObjectAddress;
                //}
                return i_AddressObjectName; }
            set { SetPropertyValue("AddressObjectName", ref i_AddressObjectName, value); }
        }

        private string i_ConstrAddress;
        [Size(SizeAttribute.Unlimited), DisplayName("Строительный адрес")]
        public string ConstrAddress
        {
            get
            {
                //if (i_AddressObjectName == null || i_AddressObjectName == String.Empty)
                //{
                //    if (this.ConstrPermLink != null)
                //        if (this.ConstrPermLink.ObjectAddress != null)
                //            i_AddressObjectName = this.ConstrPermLink.ObjectAddress;
                //}
                return i_ConstrAddress;
            }
            set { SetPropertyValue("ConstrAddress", ref i_ConstrAddress, value); }
        }
        //private string i_AddressLot;
        //[Size(1000), DisplayName("Строительный адрес")]
        //public string AddressLot
        //{
        //    get { return i_AddressLot; }
        //    set { SetPropertyValue("AddressLot", ref i_AddressLot, value); }
        //}
        private string i_AddressDocInfo;
        [Size(255), DisplayName("Реквизиты документа об адресе")]
        public string AddressDocInfo
        {
            get { return i_AddressDocInfo; }
            set { SetPropertyValue("AddressDocInfo", ref i_AddressDocInfo, value); }
        }

        private string i_LotCadNo;
        [Size(255), DisplayName("Кадастровый номер земельного участка")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get {
                //if (i_LotCadNo == null || i_LotCadNo == String.Empty)
                //{
                //    if (this.ConstrPermLink != null)
                //        if (this.ConstrPermLink.LotCadNo != null)
                //            i_LotCadNo = this.ConstrPermLink.LotCadNo;
                //}
                return i_LotCadNo; }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }
        
        //[DisplayName("Район города")]
        //public CityRegion CityRegion
        //{
        //    get { return _CityRegion; }
        //    set { try { SetPropertyValue("CityRegion", ref _CityRegion, value); } catch { } }
        //}

        //private TechPlan i_TechPlan;
        //[DisplayName("Технический план")]
        //public TechPlan TechPlan
        //{
        //    get { return i_TechPlan; }
        //    set { SetPropertyValue("TechPlan", ref i_TechPlan, value);
        //        i_TechPlansInfo = GetTechPlanInfo();
        //        OnChanged("TechPlan");
        //    }
        //}
        private string i_TechPlanNo;
        [Size(32), DisplayName("Номер техплана")]
        public string TechPlanNo
        {
            get
            {
                return i_TechPlanNo;
            }
            set { SetPropertyValue("TechPlanNo", ref i_TechPlanNo, value); }
        }
        private DateTime i_TechPlanDate;
        [DisplayName("Дата техплана")]
        public DateTime TechPlanDate
        {
            get
            {
                return i_TechPlanDate;
            }
            set { SetPropertyValue("TechPlanDate", ref i_TechPlanDate, value);
                try {
                    i_TechPlansInfo = GetTechPlanInfo();
                    OnChanged("TechPlanDate");
                }
                catch { }
            }
        }
        private CadEngineer i_CadEngineer;
        [DisplayName("Кадастровый инженер")]
        public CadEngineer CadEngineer
        {
            get { return i_CadEngineer; }
            set { SetPropertyValue("CadEngineer", ref i_CadEngineer, value);
                try {
                    i_TechPlansInfo = GetTechPlanInfo();
                    OnChanged("CadEngineer");
                }
                catch { } }
        }
        private string i_TechPlansInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("Сведения о техническом плане(текст)")]
        public string TechPlansInfo
        {
            get { return i_TechPlansInfo; }
            set { SetPropertyValue("TechPlansInfo", ref i_TechPlansInfo, value); }
        }
        private string GetTechPlanInfo()
        {
            string res = "";
            try { res = String.Format("дата подготовки технического плана {0} года, ", TechPlanDate.ToShortDateString()); }
            catch { }
            try { res += String.Format("кадастровый инженер {0}, ", CadEngineer.FIO); }
            catch { }
            try { res += String.Format("№ квалификационного аттестата кадастрового инженера {0}", CadEngineer.NCertificate); }
            catch { }
            res = res.Trim();
            res = res.TrimEnd(',');
            return res;
        }

        private string i_ConstrDeveloperString;
        [Size(500), DisplayName("Застройщик")]
        public string ConstrDeveloperString
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(i_ConstrDeveloperString))
                    return i_ConstrDeveloperString;
                else
                {
                    //try
                    //{
                    //    foreach (GeneralSubject docsubj in DocSubjects)
                    //        if (docsubj.FullName != String.Empty)
                    //        {
                    //            if (res != String.Empty)
                    //                res += ", ";
                    //            res += docsubj.FullName;
                    //        }
                    //}
                    //catch { }
                    //i_ConstrDeveloperString = res;
                    return i_ConstrDeveloperString;
                }
            }
            set { try { SetPropertyValue("ConstrDeveloperString", ref i_ConstrDeveloperString, value); } catch { } }
        }
        private string i_ConstrDeveloperContactInfo;
        [Size(500), DisplayName("Реквизиты застройщика")]
        public string ConstrDeveloperContactInfo
        {
            get
            {
                return i_ConstrDeveloperContactInfo;
            }
            set { SetPropertyValue("ConstrDeveloperContactInfo", ref i_ConstrDeveloperContactInfo, value); }
        }

        private string i_ConstrDeveloperINN;
        [Size(255), DisplayName("ИНН застройщика")]
        public string ConstrDeveloperINN
        {
            get
            {
                return i_ConstrDeveloperINN;
            }
            set { try { SetPropertyValue("ConstrDeveloperINN", ref i_ConstrDeveloperINN, value); } catch { } }
        }

        #region // краткие характеристики РВ
        //private double i_BuildingSizeFakt;
        //[DisplayName("Объем (куб.м)")]
        //public double BuildingSizeFakt
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_BuildingSizeFakt == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_BuildingSizeFakt = ObjectConstractionInfo[0].BuildingSizeFakt;
        //        //    }
        //        //}
        //        //catch { }
        //        return i_BuildingSizeFakt; }
        //    set { SetPropertyValue("BuildingSizeFakt", ref i_BuildingSizeFakt, value); }
        //}
        //private double i_TotalBuildSquareFakt;
        //[DisplayName("Общая площадь (кв.м)")]
        //public double TotalBuildSquareFakt
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_TotalBuildSquareFakt == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_TotalBuildSquareFakt = ObjectConstractionInfo[0].TotalBuildSquareFakt;
        //        //    }
        //        //}
        //        //catch { }
        //        return i_TotalBuildSquareFakt; }
        //    set { SetPropertyValue("TotalBuildSquareFakt", ref i_TotalBuildSquareFakt, value); }
        //}
        //private int i_FlatCount;
        //[DisplayName("Количество квартир")]
        //public int FlatCount
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_FlatCount == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_FlatCount = ObjectConstractionInfo[0].FloorCountLivingFakt;
        //        //    }
        //        //}
        //        //catch { }
        //        return i_FlatCount; }
        //    set { SetPropertyValue("FlatCount", ref i_FlatCount, value); }
        //}
        //private double i_TotalLivingSpaceFakt;
        //[DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.)(кв.м)")]
        //public double TotalLivingSpaceFakt
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_TotalLivingSpaceFakt == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_TotalLivingSpaceFakt = ObjectConstractionInfo[0].TotalLivingSpaceFakt;
        //        //    }
        //        //}
        //        //catch { }

        //        return i_TotalLivingSpaceFakt; }
        //    set { SetPropertyValue("TotalLivingSpaceFakt", ref i_TotalLivingSpaceFakt, value); }
        //}
        //private double i_LivingSpaceFakt;
        //[DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.)(кв.м)")]
        //public double LivingSpaceFakt
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_LivingSpaceFakt == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_LivingSpaceFakt = ObjectConstractionInfo[0].LivingSpaceFakt;
        //        //    }
        //        //}
        //        //catch { }
        //        return i_LivingSpaceFakt; }
        //    set { SetPropertyValue("LivingSpaceFakt", ref i_LivingSpaceFakt, value); }
        //}

        //private int i_FloorCount;
        //[DisplayName("Этажность")]
        //public int FloorCount
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_FloorCount == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_FloorCount = ObjectConstractionInfo[0].FloorCountLivingFakt;
        //        //    }
        //        //}
        //        //catch { }
        //        return i_FloorCount; }
        //    set { SetPropertyValue("FloorCount", ref i_FloorCount, value); }
        //}
        //private double i_ConstrCost;
        //[DisplayName("Стоимость строительства")]
        //public double ContsrCost
        //{
        //    get
        //    {
        //        //try
        //        //{
        //        //    if (i_LivingSpaceFakt == 0)
        //        //    {
        //        //        if (ObjectConstractionInfo.Count > 0)
        //        //            i_LivingSpaceFakt = ObjectConstractionInfo[0].LivingSpaceFakt;
        //        //    }
        //        //}
        //        //catch { }
        //        return i_ConstrCost;
        //    }
        //    set { SetPropertyValue("ConstrCost", ref i_ConstrCost, value); }
        //}
        //private string i_PowerFakt;
        //[DisplayName("Мощность"), VisibleInListView(false)]
        //public string PowerFakt
        //{
        //    get { return i_PowerFakt; }
        //    set { SetPropertyValue("PowerFakt", ref i_PowerFakt, value); }
        //}

        //private Material i_ConstrMaterial;
        //[DisplayName("Строительный материал")]
        //public Material ConstrMaterial
        //{
        //    get {
        //        //try
        //        //{
        //        //    if (i_ConstrMaterial == null)
        //        //        if (CapitalStructureBase.Count > 0)
        //        //            if (CapitalStructureBase[0].ConstrMaterial != null)
        //        //            {
        //        //                Connect connect = Connect.FromSession(Session);
        //        //                i_ConstrMaterial = connect.FindFirstObject<Material>(x => x.Oid.ToString() == CapitalStructureBase[0].ConstrMaterial.Oid.ToString());
        //        //            }
        //        //}
        //        //catch { }
        //        return i_ConstrMaterial; }
        //    set { SetPropertyValue("ConstrMaterial", ref i_ConstrMaterial, value); }
        //}
        #endregion

        private eBuildingKind i_BuildingKind;
        [DisplayName("Вид объекта")]
        [ImmediatePostData]
        public eBuildingKind BuildingKind
        {
            get { return i_BuildingKind; }
            set { SetPropertyValue("BuildingKind", ref i_BuildingKind, value); }
        }

        /// <summary>
        /// 1. Общие показатели объекта
        /// </summary>
        #region 1. Общие показатели объекта
        private double i_BuildingSizeProject;
        [DisplayName("Строительный объем - всего по проекту (куб.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double BuildingSizeProject
        {
            get { return i_BuildingSizeProject; }
            set { SetPropertyValue("BuildingSizeProject", ref i_BuildingSizeProject, value); }
        }
        private double i_BuildingSizeFakt;
        [DisplayName("Строительный объем - всего фактически (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeFakt
        {
            get { return i_BuildingSizeFakt; }
            set { SetPropertyValue("BuildingSizeFakt", ref i_BuildingSizeFakt, value); }
        }

        private double i_BuildingSizeOverGroundPartProject;
        [DisplayName("Строительный объем надземной части по проекту (куб.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double BuildingSizeOverGroundPartProject
        {
            get { return i_BuildingSizeOverGroundPartProject; }
            set { SetPropertyValue("BuildingSizeOverGroundPartProject", ref i_BuildingSizeOverGroundPartProject, value); }
        }

        private double i_BuildingSizeOverGroundPartFakt;
        [DisplayName("Строительный объем надземной части фактически (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeOverGroundPartFakt
        {
            get { return i_BuildingSizeOverGroundPartFakt; }
            set { SetPropertyValue("BuildingSizeOverGroundPartFakt", ref i_BuildingSizeOverGroundPartFakt, value); }
        }

        private double i_BuildingSizeUnderGroundPartProject;
        [DisplayName("Строительный объем подземной части по проекту (куб.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double BuildingSizeUnderGroundPartProject
        {
            get { return i_BuildingSizeUnderGroundPartProject; }
            set { SetPropertyValue("BuildingSizeUnderGroundPartProject", ref i_BuildingSizeUnderGroundPartProject, value); }
        }

        private double i_BuildingSizeUnderGroundPartFakt;
        [DisplayName("Строительный объем подземной части фактически (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeUnderGroundPartFakt
        {
            get { return i_BuildingSizeUnderGroundPartFakt; }
            set { SetPropertyValue("BuildingSizeUnderGroundPartFakt", ref i_BuildingSizeUnderGroundPartFakt, value); }
        }

        private double i_TotalBuildSquareProject;
        [DisplayName("Общая площадь по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double TotalBuildSquareProject
        {
            get { return i_TotalBuildSquareProject; }
            set { SetPropertyValue("TotalBuildSquareProject", ref i_TotalBuildSquareProject, value); }
        }
        private double i_TotalBuildSquareFakt;
        [DisplayName("Общая площадь фактически (кв.м)")]
        public double TotalBuildSquareFakt
        {
            get { return i_TotalBuildSquareFakt; }
            set { SetPropertyValue("TotalBuildSquareFakt", ref i_TotalBuildSquareFakt, value); }
        }
        private double i_BuildSquareProject;
        [DisplayName("Площадь застройки по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double BuildSquareProject
        {
            get { return i_BuildSquareProject; }
            set { SetPropertyValue("BuildSquareProject", ref i_BuildSquareProject, value); }
        }
        private double i_BuildSquareFakt;
        [DisplayName("Площадь застройки по фактически (кв.м)"), VisibleInListView(false)]
        public double BuildSquareFakt
        {
            get { return i_BuildSquareFakt; }
            set { SetPropertyValue("BuildSquareFakt", ref i_BuildSquareFakt, value); }
        }
        private double i_HeightProject;
        [DisplayName("Высота по проекту (м.)")]
        [ModelDefault("AllowEdit", "False")]
        public double HeightProject
        {
            get { return i_HeightProject; }
            set { SetPropertyValue("HeightProject", ref i_HeightProject, value); }
        }
        private double i_HeightFakt;
        [DisplayName("Высота по фактически (м.)")]
        public double HeightFakt
        {
            get { return i_HeightFakt; }
            set { SetPropertyValue("HeightProject", ref i_HeightFakt, value); }
        }
        private double i_NotLivingSquareProject;
        [DisplayName("Площадь нежилых помещений по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double NotLivingSquareProject
        {
            get { return i_NotLivingSquareProject; }
            set { SetPropertyValue("NotLivingSquareProject", ref i_NotLivingSquareProject, value); }
        }

        private double i_NotLivingSquareFakt;
        [DisplayName("Площадь нежилых помещений фактически (кв.м)"), VisibleInListView(false)]
        public double NotLivingSquareFakt
        {
            get { return i_NotLivingSquareFakt; }
            set { SetPropertyValue("NotLivingSquareFakt", ref i_NotLivingSquareFakt, value); }
        }

        private double i_OutBuildingSquareProject;
        [DisplayName("Площадь встроенно-пристроенных помещений по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double OutBuildingSquareProject
        {
            get { return i_OutBuildingSquareProject; }
            set { SetPropertyValue("OutBuildingSquareProject", ref i_OutBuildingSquareProject, value); }
        }

        private double i_OutBuildingSquareFakt;
        [DisplayName("Площадь встроенно-пристроенных помещений фактически (кв.м)"), VisibleInListView(false)]
        public double OutBuildingSquareFakt
        {
            get { return i_OutBuildingSquareFakt; }
            set { SetPropertyValue("OutBuildingSquareFakt", ref i_OutBuildingSquareFakt, value); }
        }

        private int i_BuildingCountProject;
        [DisplayName("Количество зданий, сооружений по проекту (шт.)")]
        [ModelDefault("AllowEdit", "False")]
        public int BuildingCountProject
        {
            get { return i_BuildingCountProject; }
            set { SetPropertyValue("BuildingCountProject", ref i_BuildingCountProject, value); }
        }

        private int i_BuildingCountFakt;
        [DisplayName("Количество зданий, сооружений фактически (шт.)")]
        public int BuildingCountFakt
        {
            get { return i_BuildingCountFakt; }
            set { SetPropertyValue("BuildingCountFakt", ref i_BuildingCountFakt, value); }
        }
        #endregion

        #region Общие показатели по этажности для нежилых, жилых и производственных (ОКС)
        private int i_FloorCountProject;
        [DisplayName("Количество этажей по проекту"), VisibleInListView(false)]
        //[ModelDefault("AllowEdit", "False")]
        public int FloorCountProject
        {
            get { return i_FloorCountProject; }
            set { SetPropertyValue("FloorCountProject", ref i_FloorCountProject, value); }
        }

        private int i_FloorCountFakt;
        [DisplayName("Количество этажей фактически"), VisibleInListView(false)]
        public int FloorCountFakt
        {
            get { return i_FloorCountFakt; }
            set { SetPropertyValue("FloorCountFakt", ref i_FloorCountFakt, value); }
        }

        private int i_UnderGroundFloorCounProject;
        [DisplayName("Количество этажей подземной части по проекту"), VisibleInListView(false)]
        //[ModelDefault("AllowEdit", "False")]
        public int UnderGroundFloorCounProject
        {
            get { return i_UnderGroundFloorCounProject; }
            set { SetPropertyValue("UnderGroundFloorCounProject", ref i_UnderGroundFloorCounProject, value); }
        }

        private int i_UnderGroundFloorCountFakt;
        [DisplayName("Количество этажей подземной части фактически"), VisibleInListView(false)]
        public int UnderGroundFloorCountFakt
        {
            get { return i_UnderGroundFloorCountFakt; }
            set { SetPropertyValue("UnderGroundFloorCountFakt", ref i_UnderGroundFloorCountFakt, value); }
        }

        private int i_FloorCountAboveProject;
        [DisplayName("Этажность по проекту")]
        //[ModelDefault("AllowEdit", "False")]
        public int FloorCountAboveProject
        {
            get { return i_FloorCountAboveProject; }
            set { SetPropertyValue("FloorCountAboveProject", ref i_FloorCountAboveProject, value); }
        }
        private int i_FloorCountAboveFakt;
        [DisplayName("Этажность фактически")]
        public int FloorCountAboveFakt
        {
            get { return i_FloorCountAboveFakt; }
            set { SetPropertyValue("FloorCountAboveFakt", ref i_FloorCountAboveFakt, value); }
        }
        private string i_FloorCountDiffProject;
        [DisplayName("Разноэтажность по проекту")]
        //[ModelDefault("AllowEdit", "False")]
        public string FloorCountDiffProject
        {
            get { return i_FloorCountDiffProject; }
            set { SetPropertyValue("FloorCountDiffProject", ref i_FloorCountDiffProject, value); }
        }
        private string i_FloorCountDiffFakt;
        [DisplayName("Разноэтажность фактически")]
        public string FloorCountDiffFakt
        {
            get { return i_FloorCountDiffFakt; }
            set { SetPropertyValue("FloorCountDiffFakt", ref i_FloorCountDiffFakt, value); }
        }
        private string i_FloorsNoteProject;
        [DisplayName("Примечание по этажности по проекту")]
        public string FloorsNoteProject
        {
            get { return i_FloorsNoteProject; }
            set { SetPropertyValue("FloorsNoteProject", ref i_FloorsNoteProject, value); }
        }
        private string i_FloorsNoteFakt;
        [DisplayName("Примечание по этажности фактически")]
        public string FloorsNoteFakt
        {
            get { return i_FloorsNoteFakt; }
            set { SetPropertyValue("FloorsNoteFakt", ref i_FloorsNoteFakt, value); }
        }
        #endregion

        #region Лифты, Эскалаторы, подъемники для ОКС
        private int i_ElevatorsCountProject;
        [DisplayName("Лифты по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int ElevatorsCountProject
        {
            get { return i_ElevatorsCountProject; }
            set { SetPropertyValue("ElevatorsCountProject", ref i_ElevatorsCountProject, value); }
        }
        private int i_ElevatorsCountFakt;
        [DisplayName("Лифты фактически (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountFakt
        {
            get { return i_ElevatorsCountFakt; }
            set { SetPropertyValue("ElevatorsCountFakt", ref i_ElevatorsCountFakt, value); }
        }

        private int i_EscalatorsCountProject;
        [DisplayName("Эскалаторы по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int EscalatorsCountProject
        {
            get { return i_EscalatorsCountProject; }
            set { SetPropertyValue("EscalatorsCountProject", ref i_EscalatorsCountProject, value); }
        }
        private int i_EscalatorsCountFakt;
        [DisplayName("Эскалаторы фактически (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountFakt
        {
            get { return i_EscalatorsCountFakt; }
            set { SetPropertyValue("EscalatorsCountFakt", ref i_EscalatorsCountFakt, value); }
        }
        private int i_InvalidLiftsCountProject;
        [DisplayName("Инвалидные подъемники по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int InvalidLiftsCountProject
        {
            get { return i_InvalidLiftsCountProject; }
            set { SetPropertyValue("InvalidLiftsCountProject", ref i_InvalidLiftsCountProject, value); }
        }

        private int i_InvalidLiftsCountFakt;
        [DisplayName("Инвалидные подъемники фактически (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountFakt
        {
            get { return i_InvalidLiftsCountFakt; }
            set { SetPropertyValue("InvalidLiftsCountFakt", ref i_InvalidLiftsCountFakt, value); }
        }
        #endregion

        #region Материалы
        private Material i_FundMaterialProject;
        [DisplayName("Материалы фундаментов по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public Material FundMaterialProject
        {
            get { return i_FundMaterialProject; }
            set { SetPropertyValue("FundMaterialProject", ref i_FundMaterialProject, value); }
        }

        private Material i_FundMaterialFakt;
        [DisplayName("Материалы фундаментов фактически"), VisibleInListView(false)]
        public Material FundMaterialFakt
        {
            get { return i_FundMaterialFakt; }
            set { SetPropertyValue("FundMaterialFakt", ref i_FundMaterialFakt, value); }
        }

        private Material i_WallMaterialProject;
        [DisplayName("Материалы стен по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public Material WallMaterialProject
        {
            get { return i_WallMaterialProject; }
            set { SetPropertyValue("WallMaterialProject", ref i_WallMaterialProject, value); }
        }

        private Material i_WallMaterialFakt;
        [DisplayName("Материалы стен фактически"), VisibleInListView(false)]
        public Material WallMaterialFakt
        {
            get { return i_WallMaterialFakt; }
            set { SetPropertyValue("WallMaterialFakt", ref i_WallMaterialFakt, value); }
        }

        private Material i_BorderMaterialProject;
        [DisplayName("Материалы перекрытий по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public Material BorderMaterialProject
        {
            get { return i_BorderMaterialProject; }
            set { SetPropertyValue("BorderMaterialProject", ref i_BorderMaterialProject, value); }
        }

        private Material i_BorderMaterialFakt;
        [DisplayName("Материалы перекрытий фактически"), VisibleInListView(false)]
        public Material BorderMaterialFakt
        {
            get { return i_BorderMaterialFakt; }
            set { SetPropertyValue("BorderMaterialFakt", ref i_BorderMaterialFakt, value); }
        }

        private Material i_RoofMaterialProject;
        [DisplayName("Материалы кровли по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public Material RoofMaterialProject
        {
            get { return i_RoofMaterialProject; }
            set { SetPropertyValue("RoofMaterialProject", ref i_RoofMaterialProject, value); }
        }

        private Material i_RoofMaterialFakt;
        [DisplayName("Материалы кровли фактически"), VisibleInListView(false)]
        public Material RoofMaterialFakt
        {
            get { return i_RoofMaterialFakt; }
            set { SetPropertyValue("RoofMaterialFakt", ref i_RoofMaterialFakt, value); }
        }
        #endregion

        #region Иные показатели
        private string i_OtherIndicatorsProject;
        [Size(4000), DisplayName("Иные показатели по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string OtherIndicatorsProject
        {
            get { return i_OtherIndicatorsProject; }
            set { SetPropertyValue("OtherIndicatorsProject", ref i_OtherIndicatorsProject, value); }
        }
        private string i_OtherIndicatorsFakt;
        [Size(4000), DisplayName("Иные показатели фактически"), VisibleInListView(false)]
        public string OtherIndicatorsFakt
        {
            get { return i_OtherIndicatorsFakt; }
            set { SetPropertyValue("OtherIndicatorsFakt", ref i_OtherIndicatorsFakt, value); }
        }
        #endregion

        /// <summary>
        /// 2.1. Нежилые объекты
        /// </summary>
        #region 2.1. Нежилые объекты
        private int i_PlacesCountProject;
        [DisplayName("Количество мест по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int PlacesCountProject
        {
            get { return i_PlacesCountProject; }
            set { SetPropertyValue("PlacesCountProject", ref i_PlacesCountProject, value); }
        }

        private int i_PlacesCountFakt;
        [DisplayName("Количество мест фактически"), VisibleInListView(false)]
        public int PlacesCountFakt
        {
            get { return i_PlacesCountFakt; }
            set { SetPropertyValue("PlacesCountFakt", ref i_PlacesCountFakt, value); }
        }

        private int i_RoomCountProject;
        [DisplayName("Количество помещений по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int RoomCountProject
        {
            get { return i_RoomCountProject; }
            set { SetPropertyValue("RoomCountProject", ref i_RoomCountProject, value); }
        }

        private int i_RoomCountFakt;
        [DisplayName("Количество помещений фактически"), VisibleInListView(false)]
        public int RoomCountFakt
        {
            get { return i_RoomCountFakt; }
            set { SetPropertyValue("RoomCountFakt", ref i_RoomCountFakt, value); }
        }

        private int i_CapacityProject;
        [DisplayName("Вместимость по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int CapacityProject
        {
            get { return i_CapacityProject; }
            set { SetPropertyValue("CapacityProject", ref i_CapacityProject, value); }
        }

        private int i_CapacityFakt;
        [DisplayName("Вместимость фактически"), VisibleInListView(false)]
        public int CapacityFakt
        {
            get { return i_CapacityFakt; }
            set { SetPropertyValue("CapacityFakt", ref i_CapacityFakt, value); }
        }

        //private string i_OtherIndicatorsNotLivingProject;
        //[DisplayName("Иные показатели по проекту(нежилые)"), VisibleInListView(false)]
        //public string OtherIndicatorsNotLivingProject
        //{
        //    get { return i_OtherIndicatorsNotLivingProject; }
        //    set { SetPropertyValue("OtherIndicatorsNotLivingProject", ref i_OtherIndicatorsNotLivingProject, value); }
        //}

        //private string i_OtherIndicatorsNotLivingFakt;
        //[DisplayName("Иные показатели фактически(нежилые)"), VisibleInListView(false)]
        //public string OtherIndicatorsNotLivingFakt
        //{
        //    get { return i_OtherIndicatorsNotLivingFakt; }
        //    set { SetPropertyValue("OtherIndicatorsNotLivingFakt", ref i_OtherIndicatorsNotLivingFakt, value); }
        //}
        #endregion

        /// <summary>
        /// 2.2. Объекты жилищного фонда
        /// </summary>
        #region 2.2. Объекты жилищного фонда
        private double i_LivingSpaceProject;
        [DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.) по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double LivingSpaceProject
        {
            get { return i_LivingSpaceProject; }
            set { SetPropertyValue("LivingSpaceProject", ref i_LivingSpaceProject, value); }
        }

        private double i_LivingSpaceFakt;
        [DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.) фактически (кв.м)"), VisibleInListView(false)]
        public double LivingSpaceFakt
        {
            get { return i_LivingSpaceFakt; }
            set { SetPropertyValue("LivingSpaceFakt", ref i_LivingSpaceFakt, value); }
        }
        private double i_TotalLivingSpaceProject;
        [DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.) по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double TotalLivingSpaceProject
        {
            get { return i_TotalLivingSpaceProject; }
            set { SetPropertyValue("TotalLivingSpaceProject", ref i_TotalLivingSpaceProject, value); }
        }

        private double i_TotalLivingSpaceFakt;
        [DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.) фактически (кв.м)"), VisibleInListView(false)]
        public double TotalLivingSpaceFakt
        {
            get { return i_TotalLivingSpaceFakt; }
            set { SetPropertyValue("TotalLivingSpaceFakt", ref i_TotalLivingSpaceFakt, value); }
        }
        private double i_BalconySpaceProject;
        [DisplayName("Площадь балконов с поправочным коэффициентом по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double BalconySpaceProject
        {
            get { return i_BalconySpaceProject; }
            set { SetPropertyValue("BalconySpaceProject", ref i_BalconySpaceProject, value); }
        }
        
        private double i_BalconySpaceFakt;
        [DisplayName("Площадь балконов с поправочным коэффициентом фактически (кв.м)"), VisibleInListView(false)]
        public double BalconySpaceFakt
        {
            get { return i_BalconySpaceFakt; }
            set { SetPropertyValue("BalconySpaceFakt", ref i_BalconySpaceFakt, value); }
        }
        private double i_NotLivingSpaceProject;
        [DisplayName("Общая площадь нежилых помещений по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double NotLivingSpaceProject
        {
            get { return i_NotLivingSpaceProject; }
            set { SetPropertyValue("NotLivingSpaceProject", ref i_NotLivingSpaceProject, value); }
        }

        private double i_NotLivingSpaceFakt;
        [DisplayName("Общая площадь нежилых помещений фактически (кв.м)"), VisibleInListView(false)]
        public double NotLivingSpaceFakt
        {
            get { return i_NotLivingSpaceFakt; }
            set { SetPropertyValue("NotLivingSpaceFakt", ref i_NotLivingSpaceFakt, value); }
        }

        //private int i_FloorCountLivingProject;
        //[DisplayName("Количество этажей по проекту(жилые)"), VisibleInListView(false)]
        //public int FloorCountLivingProject
        //{
        //    get { return i_FloorCountLivingProject; }
        //    set { SetPropertyValue("FloorCountLivingProject", ref i_FloorCountLivingProject, value); }
        //}

        //private int i_FloorCountLivingFakt;
        //[DisplayName("Количество этажей фактически(жилые)"), VisibleInListView(false)]
        //public int FloorCountLivingFakt
        //{
        //    get { return i_FloorCountLivingFakt; }
        //    set { SetPropertyValue("FloorCountLivingFakt", ref i_FloorCountLivingFakt, value); }
        //}

        //private int i_UnderGroundFloorCountLivingProject;
        //[DisplayName("Количество этажей подземной части по проекту(жилые)"), VisibleInListView(false)]
        //public int UnderGroundFloorCountLivingProject
        //{
        //    get { return i_UnderGroundFloorCountLivingProject; }
        //    set { SetPropertyValue("UnderGroundFloorCountLivingProject", ref i_UnderGroundFloorCountLivingProject, value); }
        //}

        //private int i_UnderGroundFloorCountLivingFakt;
        //[DisplayName("Количество этажей подземной части фактически(жилые)"), VisibleInListView(false)]
        //public int UnderGroundFloorCountLivingFakt
        //{
        //    get { return i_UnderGroundFloorCountLivingFakt; }
        //    set { SetPropertyValue("UnderGroundFloorCountLivingFakt", ref i_UnderGroundFloorCountLivingFakt, value); }
        //}

        private int i_SectionCountProject;
        [DisplayName("Количество секций по проекту(жилые)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int SectionCountProject
        {
            get { return i_SectionCountProject; }
            set { SetPropertyValue("SectionCountProject", ref i_SectionCountProject, value); }
        }

        private int i_SectionCountFakt;
        [DisplayName("Количество секций фактически(жилые)"), VisibleInListView(false)]
        public int SectionCountFakt
        {
            get { return i_SectionCountFakt; }
            set { SetPropertyValue("SectionCountFakt", ref i_SectionCountFakt, value); }
        }
        private int i_PorchCountProject;
        [DisplayName("Количество подъездов по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int PorchCountProject
        {
            get { return i_PorchCountProject; }
            set { SetPropertyValue("PorchCountProject", ref i_PorchCountProject, value); }
        }

        private int i_PorchCountFakt;
        [DisplayName("Количество подъездов фактически"), VisibleInListView(false)]
        public int PorchCountFakt
        {
            get { return i_PorchCountFakt; }
            set { SetPropertyValue("PorchCountFakt", ref i_PorchCountFakt, value); }
        }
        private int i_AppartmentsCountProject;
        [DisplayName("Количество квартир всего по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int AppartmentsCountProject
        {
            get { return i_AppartmentsCountProject; }
            set { SetPropertyValue("AppartmentsCountProject", ref i_AppartmentsCountProject, value); }
        }

        private int i_AppartmentsCountFakt;
        [DisplayName("Количество квартир всего фактически (шт.)"), VisibleInListView(false)]
        public int AppartmentsCountFakt
        {
            get { return i_AppartmentsCountFakt; }
            set { SetPropertyValue("AppartmentsCountFakt", ref i_AppartmentsCountFakt, value); }
        }
        private int i_StudioCountProject;
        [DisplayName("Количество студий по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int StudioCountProject
        {
            get { return i_StudioCountProject; }
            set { SetPropertyValue("StudioCountProject", ref i_StudioCountProject, value); }
        }
        private double i_StudioSpaceProject;
        [DisplayName("Площадь студий по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double StudioSpaceProject
        {
            get { return i_StudioSpaceProject; }
            set { SetPropertyValue("StudioSpaceProject", ref i_StudioSpaceProject, value); }
        }
        private int i_StudioCountFakt;
        [DisplayName("Количество студии фактически (шт.)"), VisibleInListView(false)]
        public int StudioCountFakt
        {
            get { return i_StudioCountFakt; }
            set { SetPropertyValue("StudioCountFakt", ref i_StudioCountFakt, value); }
        }
        private double i_StudioSpaceFakt;
        [DisplayName("Площадь студий фактически (кв.м)"), VisibleInListView(false)]
        public double StudioSpaceFakt
        {
            get { return i_StudioSpaceFakt; }
            set { SetPropertyValue("StudioSpaceFakt", ref i_StudioSpaceFakt, value); }
        }

        private int i_OneRoomFlatCountProject;
        [DisplayName("Количество 1-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int OneRoomFlatCountProject
        {
            get { return i_OneRoomFlatCountProject; }
            set { SetPropertyValue("OneRoomFlatCountProject", ref i_OneRoomFlatCountProject, value); }
        }
        private double i_OneRoomFlatSpaceProject;
        [DisplayName("Площадь 1-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double OneRoomFlatSpaceProject
        {
            get { return i_OneRoomFlatSpaceProject; }
            set { SetPropertyValue("OneRoomFlatSpaceProject", ref i_OneRoomFlatSpaceProject, value); }
        }

        private int i_OneRoomFlatCountFakt;
        [DisplayName("Количество 1-комнатных квартир фактически (шт.)"), VisibleInListView(false)]
        public int OneRoomFlatCountFakt
        {
            get { return i_OneRoomFlatCountFakt; }
            set { SetPropertyValue("OneRoomFlatCountFakt", ref i_OneRoomFlatCountFakt, value); }
        }
        private double i_OneRoomFlatSpaceFakt;
        [DisplayName("Площадь 1-комнатных квартир фактически (кв.м)"), VisibleInListView(false)]
        public double OneRoomFlatSpaceFakt
        {
            get { return i_OneRoomFlatSpaceProject; }
            set { SetPropertyValue("OneRoomFlatSpaceFakt", ref i_OneRoomFlatSpaceProject, value); }
        }


        private int i_TwoRoomFlatCountProject;
        [DisplayName("Количество 2-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int TwoRoomFlatCountProject
        {
            get { return i_TwoRoomFlatCountProject; }
            set { SetPropertyValue("TwoRoomFlatCountProject", ref i_TwoRoomFlatCountProject, value); }
        }
        private double i_TwoRoomFlatSpaceProject;
        [DisplayName("Площадь 2-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double TwoRoomFlatSpaceProject
        {
            get { return i_TwoRoomFlatSpaceProject; }
            set { SetPropertyValue("TwoRoomFlatSpaceProject", ref i_TwoRoomFlatSpaceProject, value); }
        }

        private int i_TwoRoomFlatCountFakt;
        [DisplayName("Количество 2-комнатных квартир фактически (шт.)"), VisibleInListView(false)]
        public int TwoRoomFlatCountFakt
        {
            get { return i_TwoRoomFlatCountFakt; }
            set { SetPropertyValue("TwoRoomFlatCountFakt", ref i_TwoRoomFlatCountFakt, value); }
        }
        private double i_TwoRoomFlatSpaceFakt;
        [DisplayName("Площадь 2-комнатных квартир фактически (кв.м)"), VisibleInListView(false)]
        public double TwoRoomFlatSpaceFakt
        {
            get { return i_TwoRoomFlatSpaceFakt; }
            set { SetPropertyValue("TwoRoomFlatSpaceFakt", ref i_TwoRoomFlatSpaceFakt, value); }
        }


        private int i_ThreeRoomFlatCountProject;
        [DisplayName("Количество 3-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int ThreeRoomFlatCountProject
        {
            get { return i_ThreeRoomFlatCountProject; }
            set { SetPropertyValue("ThreeRoomFlatCountProject", ref i_ThreeRoomFlatCountProject, value); }
        }
        private double i_ThreeRoomFlatSpaceProject;
        [DisplayName("Площадь 3-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double ThreeRoomFlatSpaceProject
        {
            get { return i_ThreeRoomFlatSpaceProject; }
            set { SetPropertyValue("ThreeRoomFlatSpaceProject", ref i_ThreeRoomFlatSpaceProject, value); }
        }

        private int i_ThreeRoomFlatCountFakt;
        [DisplayName("Количество 3-комнатных квартир фактически (шт.)"), VisibleInListView(false)]
        public int ThreeRoomFlatCountFakt
        {
            get { return i_ThreeRoomFlatCountFakt; }
            set { SetPropertyValue("ThreeRoomFlatCountFakt", ref i_ThreeRoomFlatCountFakt, value); }
        }
        private double i_ThreeRoomFlatSpaceFakt;
        [DisplayName("Площадь 3-комнатных квартир фактически (кв.м)"), VisibleInListView(false)]
        public double ThreeRoomFlatSpaceFakt
        {
            get { return i_ThreeRoomFlatSpaceFakt; }
            set { SetPropertyValue("ThreeRoomFlatSpaceFakt", ref i_ThreeRoomFlatSpaceFakt, value); }
        }


        private int i_FourRoomFlatCountProject;
        [DisplayName("Количество 4-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int FourRoomFlatCountProject
        {
            get { return i_FourRoomFlatCountProject; }
            set { SetPropertyValue("FourRoomFlatCountProject", ref i_FourRoomFlatCountProject, value); }
        }
        private double i_FourRoomSpaceProject;
        [DisplayName("Площадь 4-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double FourRoomSpaceProject
        {
            get { return i_FourRoomSpaceProject; }
            set { SetPropertyValue("FourRoomSpaceProject", ref i_FourRoomSpaceProject, value); }
        }

        private int i_FourRoomFlatCountFakt;
        [DisplayName("Количество 4-комнатных квартир фактически (шт.)"), VisibleInListView(false)]
        public int FourRoomFlatCountFakt
        {
            get { return i_FourRoomFlatCountFakt; }
            set { SetPropertyValue("FourRoomFlatCountFakt", ref i_FourRoomFlatCountFakt, value); }
        }
        private double i_FourRoomSpaceFakt;
        [DisplayName("Площадь 4-комнатных квартир фактически (кв.м)"), VisibleInListView(false)]
        public double FourRoomSpaceFakt
        {
            get { return i_FourRoomSpaceFakt; }
            set { SetPropertyValue("FourRoomSpaceFakt", ref i_FourRoomSpaceFakt, value); }
        }


        private int i_MoreRoomFlatCountProject;
        [DisplayName("Количество квартир > 4 комнат по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int MoreRoomFlatCountProject
        {
            get { return i_MoreRoomFlatCountProject; }
            set { SetPropertyValue("MoreRoomFlatCountProject", ref i_MoreRoomFlatCountProject, value); }
        }
        private double i_MoreRoomSpaceProject;
        [DisplayName("Площадь квартир > 4 комнат по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double MoreRoomSpaceProject
        {
            get { return i_MoreRoomSpaceProject; }
            set { SetPropertyValue("MoreRoomSpaceProject", ref i_MoreRoomSpaceProject, value); }
        }

        private int i_MoreRoomFlatCountFakt;
        [DisplayName("Количество квартир > 4 комнат фактически (шт.)"), VisibleInListView(false)]
        public int MoreRoomFlatCountFakt
        {
            get { return i_MoreRoomFlatCountFakt; }
            set { SetPropertyValue("MoreRoomFlatCountFakt", ref i_MoreRoomFlatCountFakt, value); }
        }
        private double i_MoreRoomSpaceFakt;
        [DisplayName("Площадь квартир > 4 комнат фактически (кв.м)"), VisibleInListView(false)]
        public double MoreRoomSpaceFakt
        {
            get { return i_MoreRoomSpaceFakt; }
            set { SetPropertyValue("MoreRoomSpaceFakt", ref i_MoreRoomSpaceFakt, value); }
        }
        private int i_ElitAppartmentsCountProject;
        [DisplayName("Количество квартир > 150м2 по проекту (шт.)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public int ElitAppartmentsCountProject
        {
            get { return i_ElitAppartmentsCountProject; }
            set { SetPropertyValue("ElitAppartmentsCountProject", ref i_ElitAppartmentsCountProject, value); }
        }

        private int i_ElitAppartmentsCountFakt;
        [DisplayName("Количество квартир > 150м2 фактически (шт.)"), VisibleInListView(false)]
        public int ElitAppartmentsCountFakt
        {
            get { return i_ElitAppartmentsCountFakt; }
            set { SetPropertyValue("ElitAppartmentsCountFakt", ref i_ElitAppartmentsCountFakt, value); }
        }
        private double i_ElitAppartmentsSpaceProject;
        [DisplayName("Площадь квартир > 150м2 по проекту (кв.м)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double ElitAppartmentsSpaceProject
        {
            get { return i_ElitAppartmentsSpaceProject; }
            set { SetPropertyValue("ElitAppartmentsSpaceProject", ref i_ElitAppartmentsSpaceProject, value); }
        }

        private double i_ElitAppartmentsSpaceFakt;
        [DisplayName("Площадь квартир > 150м2 фактически (кв.м)"), VisibleInListView(false)]
        public double ElitAppartmentsSpaceFakt
        {
            get { return i_ElitAppartmentsSpaceFakt; }
            set { SetPropertyValue("ElitAppartmentsSpaceFakt", ref i_ElitAppartmentsSpaceFakt, value); }
        }
        //private int i_ElevatorsCountLivingProject;
        //[DisplayName("Лифты по проекту (шт.)(жилые)"), VisibleInListView(false)]
        //public int ElevatorsCountLivingProject
        //{
        //    get { return i_ElevatorsCountLivingProject; }
        //    set { SetPropertyValue("ElevatorsCountLivingProject", ref i_ElevatorsCountLivingProject, value); }
        //}

        //private int i_ElevatorsCountLivingFakt;
        //[DisplayName("Лифты фактически (шт.)(жилые)"), VisibleInListView(false)]
        //public int ElevatorsCountLivingFakt
        //{
        //    get { return i_ElevatorsCountLivingFakt; }
        //    set { SetPropertyValue("ElevatorsCountLivingFakt", ref i_ElevatorsCountLivingFakt, value); }
        //}

        //private int i_EscalatorsCountLivingProject;
        //[DisplayName("Эскалаторы по проекту (шт.)(жилые)"), VisibleInListView(false)]
        //public int EscalatorsCountLivingProject
        //{
        //    get { return i_EscalatorsCountLivingProject; }
        //    set { SetPropertyValue("EscalatorsCountLivingProject", ref i_EscalatorsCountLivingProject, value); }
        //}

        //private int i_EscalatorsCountLivingFakt;
        //[DisplayName("Эскалаторы фактически (шт.)(жилые)"), VisibleInListView(false)]
        //public int EscalatorsCountLivingFakt
        //{
        //    get { return i_EscalatorsCountLivingFakt; }
        //    set { SetPropertyValue("EscalatorsCountLivingFakt", ref i_EscalatorsCountLivingFakt, value); }
        //}

        //private int i_InvalidLiftsCountLivingProject;
        //[DisplayName("Инвалидные подъемники по проекту (шт.)(жилые)"), VisibleInListView(false)]
        //public int InvalidLiftsCountLivingProject
        //{
        //    get { return i_InvalidLiftsCountLivingProject; }
        //    set { SetPropertyValue("InvalidLiftsCountLivingProject", ref i_InvalidLiftsCountLivingProject, value); }
        //}

        //private int i_InvalidLiftsCountLivingFakt;
        //[DisplayName("Инвалидные подъемники фактически (шт.)(жилые)"), VisibleInListView(false)]
        //public int InvalidLiftsCountLivingFakt
        //{
        //    get { return i_InvalidLiftsCountLivingFakt; }
        //    set { SetPropertyValue("InvalidLiftsCountLivingFakt", ref i_InvalidLiftsCountLivingFakt, value); }
        //}

        //private Material i_FundMaterialLivingProject;
        //[DisplayName("Материалы фундаментов по проекту(жилые)"), VisibleInListView(false)]
        //public Material FundMaterialLivingProject
        //{
        //    get { return i_FundMaterialLivingProject; }
        //    set { SetPropertyValue("FundMaterialLivingProject", ref i_FundMaterialLivingProject, value); }
        //}

        //private Material i_FundMaterialLivingFakt;
        //[DisplayName("Материалы фундаментов фактически(жилые)"), VisibleInListView(false)]
        //public Material FundMaterialLivingFakt
        //{
        //    get { return i_FundMaterialLivingFakt; }
        //    set { SetPropertyValue("FundMaterialLivingFakt", ref i_FundMaterialLivingFakt, value); }
        //}

        //private Material i_WallMaterialLivingProject;
        //[DisplayName("Материалы стен по проекту(жилые)"), VisibleInListView(false)]
        //public Material WallMaterialLivingProject
        //{
        //    get { return i_WallMaterialLivingProject; }
        //    set { SetPropertyValue("WallMaterialLivingProject", ref i_WallMaterialLivingProject, value); }
        //}

        //private Material i_WallMaterialLivingFakt;
        //[DisplayName("Материалы стен фактически(жилые)"), VisibleInListView(false)]
        //public Material WallMaterialLivingFakt
        //{
        //    get { return i_WallMaterialLivingFakt; }
        //    set { SetPropertyValue("WallMaterialLivingFakt", ref i_WallMaterialLivingFakt, value); }
        //}
        //private Material i_BorderMaterialLivingProject;
        //[DisplayName("Материалы перекрытий по проекту(жилые)"), VisibleInListView(false)]
        //public Material BorderMaterialLivingProject
        //{
        //    get { return i_BorderMaterialLivingProject; }
        //    set { SetPropertyValue("BorderMaterialLivingProject", ref i_BorderMaterialLivingProject, value); }
        //}

        //private Material i_BorderMaterialLivingFakt;
        //[DisplayName("Материалы перекрытий фактически(жилые)"), VisibleInListView(false)]
        //public Material BorderMaterialLivingFakt
        //{
        //    get { return i_BorderMaterialLivingFakt; }
        //    set { SetPropertyValue("BorderMaterialLivingFakt", ref i_BorderMaterialLivingFakt, value); }
        //}

        //private Material i_RoofMaterialLivingProject;
        //[DisplayName("Материалы кровли по проекту(жилые)"), VisibleInListView(false)]
        //public Material RoofMaterialLivingProject
        //{
        //    get { return i_RoofMaterialLivingProject; }
        //    set { SetPropertyValue("RoofMaterialLivingProject", ref i_RoofMaterialLivingProject, value); }
        //}

        //private Material i_RoofMaterialLivingFakt;
        //[DisplayName("Материалы кровли фактически(жилые)"), VisibleInListView(false)]
        //public Material RoofMaterialLivingFakt
        //{
        //    get { return i_RoofMaterialLivingFakt; }
        //    set { SetPropertyValue("RoofMaterialLivingFakt", ref i_RoofMaterialLivingFakt, value); }
        //}

        //private string i_OtherIndicatorsLivingProject;
        //[DisplayName("Иные показатели по проекту(жилые)"), VisibleInListView(false)]
        //public string OtherIndicatorsLivingProject
        //{
        //    get { return i_OtherIndicatorsLivingProject; }
        //    set { SetPropertyValue("OtherIndicatorsLivingProject", ref i_OtherIndicatorsLivingProject, value); }
        //}

        //private string i_OtherIndicatorsLivingFakt;
        //[DisplayName("Иные показатели фактически(жилые)"), VisibleInListView(false)]
        //public string OtherIndicatorsLivingFakt
        //{
        //    get { return i_OtherIndicatorsLivingFakt; }
        //    set { SetPropertyValue("OtherIndicatorsLivingFakt", ref i_OtherIndicatorsLivingFakt, value); }
        //}
        #endregion

        #region Стоимость строительства
        private double _ConstructionCost;
        private double _SpecificCost;
        private double _ConstrWorkCost;
        private double _Cost;
        /// <summary>
        [DisplayName("Стоимость строительства объекта (тыс.руб)")]
        public double constructionCost
        {
            get { return _ConstructionCost; }
            set { try { _ConstructionCost = value; } catch { } }
        }
        [DisplayName("Удельная стоимость 1 кв.м. (тыс.руб)")]
        public double specificCost
        {
            get { return _SpecificCost; }
            set { try { _SpecificCost = value; } catch { } }
        }
        [DisplayName("Стоимость строительно-монтажных работ (тыс.руб)"), VisibleInListView(false)]
        public double constrWorkCost
        {
            get { return _ConstrWorkCost; }
            set { try { _ConstrWorkCost = value; } catch { } }
        }
        [DisplayName("Стоимость введенных в действие основных фондов (тыс.руб)"), VisibleInListView(false)]
        public double cost
        {
            get { return _Cost; }
            set { try { _Cost = value; } catch { } }
        }
        #endregion

        /// <summary>
        /// 3. Объекты производственного назначения
        /// </summary>
        #region 3. Объекты производственного назначения
        private ProductPurposeObjectType i_ProductPurposeObjectType;
        [DisplayName("Тип объекта производственного назначения")]
        public ProductPurposeObjectType ProductPurposeObjectType
        {
            get { return i_ProductPurposeObjectType; }
            set { SetPropertyValue("ProductPurposeObjectType", ref i_ProductPurposeObjectType, value); }
        }

        private string i_PowerProject;
        [DisplayName("Мощность по проекту(производство)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string PowerProject
        {
            get { return i_PowerProject; }
            set { SetPropertyValue("PowerProject", ref i_PowerProject, value); }
        }
        private string i_PowerFakt;
        [DisplayName("Мощность фактически(производство)"), VisibleInListView(false)]
        public string PowerFakt
        {
            get { return i_PowerFakt; }
            set { SetPropertyValue("PowerFakt", ref i_PowerFakt, value); }
        }

        private string i_ProductivityProject;
        [DisplayName("Производительность по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string ProductivityProject
        {
            get { return i_ProductivityProject; }
            set { SetPropertyValue("ProductivityProject", ref i_ProductivityProject, value); }
        }

        private string i_ProductivityFakt;
        [DisplayName("Производительность фактически"), VisibleInListView(false)]
        public string ProductivityFakt
        {
            get { return i_ProductivityFakt; }
            set { SetPropertyValue("ProductivityFakt", ref i_ProductivityFakt, value); }
        }

        //private int i_ElevatorsCountProductProject;
        //[DisplayName("Лифты по проекту (шт.)(производство)"), VisibleInListView(false)]
        //public int ElevatorsCountProductProject
        //{
        //    get { return i_ElevatorsCountProductProject; }
        //    set { SetPropertyValue("ElevatorsCountProductProject", ref i_ElevatorsCountProductProject, value); }
        //}

        //private int i_ElevatorsCountProductFakt;
        //[DisplayName("Лифты фактически (шт.)(производство)"), VisibleInListView(false)]
        //public int ElevatorsCountProductFakt
        //{
        //    get { return i_ElevatorsCountProductFakt; }
        //    set { SetPropertyValue("ElevatorsCountProductFakt", ref i_ElevatorsCountProductFakt, value); }
        //}

        //private int i_EscalatorsCountProductProject;
        //[DisplayName("Эскалаторы по проекту (шт.)(производство)"), VisibleInListView(false)]
        //public int EscalatorsCountProductProject
        //{
        //    get { return i_EscalatorsCountProductProject; }
        //    set { SetPropertyValue("EscalatorsCountProductProject", ref i_EscalatorsCountProductProject, value); }
        //}

        //private int i_EscalatorsCountProductFakt;
        //[DisplayName("Эскалаторы фактически (шт.)(производство)"), VisibleInListView(false)]
        //public int EscalatorsCountProductFakt
        //{
        //    get { return i_EscalatorsCountProductFakt; }
        //    set { SetPropertyValue("EscalatorsCountProductFakt", ref i_EscalatorsCountProductFakt, value); }
        //}

        //private int i_InvalidLiftsCountProductProject;
        //[DisplayName("Инвалидные подъемники по проекту (шт.)(производство)"), VisibleInListView(false)]
        //public int InvalidLiftsCountProductProject
        //{
        //    get { return i_InvalidLiftsCountProductProject; }
        //    set { SetPropertyValue("InvalidLiftsCountProductProject", ref i_InvalidLiftsCountProductProject, value); }
        //}

        //private int i_InvalidLiftsCountProductFakt;
        //[DisplayName("Инвалидные подъемники фактически (шт.)(производство)"), VisibleInListView(false)]
        //public int InvalidLiftsCountProductFakt
        //{
        //    get { return i_InvalidLiftsCountProductFakt; }
        //    set { SetPropertyValue("InvalidLiftsCountProductFakt", ref i_InvalidLiftsCountProductFakt, value); }
        //}

        //private Material i_FundMaterialProductProject;
        //[DisplayName("Материалы фундаментов по проекту(производство)"), VisibleInListView(false)]
        //public Material FundMaterialProductProject
        //{
        //    get { return i_FundMaterialProductProject; }
        //    set { SetPropertyValue("FundMaterialProductProject", ref i_FundMaterialProductProject, value); }
        //}

        //private Material i_FundMaterialProductFakt;
        //[DisplayName("Материалы фундаментов фактически(производство)"), VisibleInListView(false)]
        //public Material FundMaterialProductFakt
        //{
        //    get { return i_FundMaterialProductFakt; }
        //    set { SetPropertyValue("FundMaterialProductFakt", ref i_FundMaterialProductFakt, value); }
        //}

        //private Material i_WallMaterialProductProject;
        //[DisplayName("Материалы стен по проекту(производство)"), VisibleInListView(false)]
        //public Material WallMaterialProductProject
        //{
        //    get { return i_WallMaterialProductProject; }
        //    set { SetPropertyValue("WallMaterialProductProject", ref i_WallMaterialProductProject, value); }
        //}

        //private Material i_WallMaterialProductFakt;
        //[DisplayName("Материалы стен фактически(производство)"), VisibleInListView(false)]
        //public Material WallMaterialProductFakt
        //{
        //    get { return i_WallMaterialProductFakt; }
        //    set { SetPropertyValue("WallMaterialProductFakt", ref i_WallMaterialProductFakt, value); }
        //}
        //private Material i_BorderMaterialProductProject;
        //[DisplayName("Материалы перекрытий по проекту(производство)"), VisibleInListView(false)]
        //public Material BorderMaterialProductProject
        //{
        //    get { return i_BorderMaterialProductProject; }
        //    set { SetPropertyValue("BorderMaterialProductProject", ref i_BorderMaterialProductProject, value); }
        //}

        //private Material i_BorderMaterialProductFakt;
        //[DisplayName("Материалы перекрытий фактически(производство)"), VisibleInListView(false)]
        //public Material BorderMaterialProductFakt
        //{
        //    get { return i_BorderMaterialProductFakt; }
        //    set { SetPropertyValue("BorderMaterialProductFakt", ref i_BorderMaterialProductFakt, value); }
        //}

        //private Material i_RoofMaterialProductProject;
        //[DisplayName("Материалы кровли по проекту(производство)"), VisibleInListView(false)]
        //public Material RoofMaterialProductProject
        //{
        //    get { return i_RoofMaterialProductProject; }
        //    set { SetPropertyValue("RoofMaterialProductProject", ref i_RoofMaterialProductProject, value); }
        //}

        //private Material i_RoofMaterialProductFakt;
        //[DisplayName("Материалы кровли фактически(производство)"), VisibleInListView(false)]
        //public Material RoofMaterialProductFakt
        //{
        //    get { return i_RoofMaterialProductFakt; }
        //    set { SetPropertyValue("RoofMaterialProductFakt", ref i_RoofMaterialProductFakt, value); }
        //}

        //private string i_OtherIndicatorsProductProject;
        //[DisplayName("Иные показатели по проекту(производство)"), VisibleInListView(false)]
        //public string OtherIndicatorsProductProject
        //{
        //    get { return i_OtherIndicatorsProductProject; }
        //    set { SetPropertyValue("OtherIndicatorsProductProject", ref i_OtherIndicatorsProductProject, value); }
        //}

        //private string i_OtherIndicatorsProductFakt;
        //[DisplayName("Иные показатели фактически(производство)"), VisibleInListView(false)]
        //public string OtherIndicatorsProductFakt
        //{
        //    get { return i_OtherIndicatorsProductFakt; }
        //    set { SetPropertyValue("OtherIndicatorsProductFakt", ref i_OtherIndicatorsProductFakt, value); }
        //}
        #endregion

        /// <summary>
        /// 4. Линейные объекты
        /// </summary>
        #region 4. Линейные объекты
        private LineObjectClass i_LineObjectClass;
        [DisplayName("Категория (класс) линейного объекта"), VisibleInListView(false)]
        public LineObjectClass LineObjectClass
        {
            get { return i_LineObjectClass; }
            set { SetPropertyValue("LineObjectClass", ref i_LineObjectClass, value); }
        }

        private double i_LengthProject;
        [DisplayName("Протяженность по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double LengthProject
        {
            get { return i_LengthProject; }
            set { SetPropertyValue("LengthProject", ref i_LengthProject, value); }
        }

        private double i_LengthFakt;
        [DisplayName("Протяженность фактически"), VisibleInListView(false)]
        public double LengthFakt
        {
            get { return i_LengthFakt; }
            set { SetPropertyValue("LengthFakt", ref i_LengthFakt, value); }
        }

        private string i_PowerLineProject;
        [DisplayName("Мощность по проекту(линейный)"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string PowerLineProject
        {
            get { return i_PowerLineProject; }
            set { SetPropertyValue("PowerLineProject", ref i_PowerLineProject, value); }
        }
        private string i_PowerLineFakt;
        [DisplayName("Мощность фактически(линейный)"), VisibleInListView(false)]
        public string PowerLineFakt
        {
            get { return i_PowerLineFakt; }
            set { SetPropertyValue("PowerLineFakt", ref i_PowerLineFakt, value); }
        }

        private string i_PipesInfoProject;
        [Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string PipesInfoProject
        {
            get { return i_PipesInfoProject; }
            set { SetPropertyValue("PipesInfoProject", ref i_PipesInfoProject, value); }
        }
        private string i_PipesInfoFakt;
        [Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб фактически"), VisibleInListView(false)]
        public string PipesInfoFakt
        {
            get { return i_PipesInfoFakt; }
            set { SetPropertyValue("PipesInfoFakt", ref i_PipesInfoFakt, value); }
        }

        private string i_ElectricLinesInfoProject;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string ElectricLinesInfoProject
        {
            get { return i_ElectricLinesInfoProject; }
            set { SetPropertyValue("ElectricLinesInfoProject", ref i_ElectricLinesInfoProject, value); }
        }

        private string i_ElectricLinesInfoFakt;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи фактически"), VisibleInListView(false)]
        public string ElectricLinesInfoFakt
        {
            get { return i_ElectricLinesInfoFakt; }
            set { SetPropertyValue("ElectricLinesInfoFakt", ref i_ElectricLinesInfoFakt, value); }
        }

        private string i_ConstructiveElementsInfoProject;
        [Size(1000), DisplayName("Перечень конструктивных элементов по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string ConstructiveElementsInfoProject
        {
            get { return i_ConstructiveElementsInfoProject; }
            set { SetPropertyValue("ConstructiveElementsInfoProject", ref i_ConstructiveElementsInfoProject, value); }
        }

        private string i_ConstructiveElementsInfoFakt;
        [Size(1000), DisplayName("Перечень конструктивных элементов фактически"), VisibleInListView(false)]
        public string ConstructiveElementsInfoFakt
        {
            get { return i_ConstructiveElementsInfoFakt; }
            set { SetPropertyValue("ConstructiveElementsInfoFakt", ref i_ConstructiveElementsInfoFakt, value); }
        }

        //private string i_OtherIndicatorsLineProject;
        //[DisplayName("Иные показатели по проекту(линейный)"), VisibleInListView(false)]
        //public string OtherIndicatorsLineProject
        //{
        //    get { return i_OtherIndicatorsLineProject; }
        //    set { SetPropertyValue("OtherIndicatorsLineProject", ref i_OtherIndicatorsLineProject, value); }
        //}

        //private string i_OtherIndicatorsLineFakt;
        //[DisplayName("Иные показатели фактически(линейный)"), VisibleInListView(false)]
        //public string OtherIndicatorsLineFakt
        //{
        //    get { return i_OtherIndicatorsLineFakt; }
        //    set { SetPropertyValue("OtherIndicatorsLineFakt", ref i_OtherIndicatorsLineFakt, value); }
        //}
        #endregion

        /// <summary>
        /// 5. Соответствие требованиям энергетической эффективности
        /// </summary>
        #region 5. Соответствие требованиям энергетической эффективности
        private EnergyEfficiencyClass i_EnergyEfficiencyClassProject;
        [DisplayName("Класс энергоэффективности по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public EnergyEfficiencyClass EnergyEfficiencyClassProject
        {
            get { return i_EnergyEfficiencyClassProject; }
            set { SetPropertyValue("EnergyEfficiencyClassProject", ref i_EnergyEfficiencyClassProject, value); }
        }

        private EnergyEfficiencyClass i_EnergyEfficiencyClassFakt;
        [DisplayName("Класс энергоэффективности фактически"), VisibleInListView(false)]
        public EnergyEfficiencyClass EnergyEfficiencyClassFakt
        {
            get { return i_EnergyEfficiencyClassFakt; }
            set { SetPropertyValue("EnergyEfficiencyClassFakt", ref i_EnergyEfficiencyClassFakt, value); }
        }
        private dHeatUnit i_HeatUnit;
        [DisplayName("Единица расхода тепловой энергии"), VisibleInListView(false)]
        public dHeatUnit HeatUnit
        {
            get { return i_HeatUnit; }
            set { SetPropertyValue("HeatUnit", ref i_HeatUnit, value); }
        }
        private double i_HeatConsumptionProject;
        [DisplayName("Удельный расход тепловой энергии на 1 кв.м. площади по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public double HeatConsumptionProject
        {
            get { return i_HeatConsumptionProject; }
            set { SetPropertyValue("HeatConsumptionProject", ref i_HeatConsumptionProject, value); }
        }

        private double i_HeatConsumptionFakt;
        [DisplayName("Удельный расход тепловой энергии на 1 кв.м. площади фактически"), VisibleInListView(false)]
        public double HeatConsumptionFakt
        {
            get { return i_HeatConsumptionFakt; }
            set { SetPropertyValue("HeatConsumptionFakt", ref i_HeatConsumptionFakt, value); }
        }

        private string i_OutdoorIsolationMaterialProject;
        [Size(1000), DisplayName("Материалы утепления наружных ограждающих конструкций по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string OutdoorIsolationMaterialProject
        {
            get { return i_OutdoorIsolationMaterialProject; }
            set { SetPropertyValue("OutdoorIsolationMaterialProject", ref i_OutdoorIsolationMaterialProject, value); }
        }

        private string i_OutdoorIsolationMaterialFakt;
        [Size(1000), DisplayName("Материалы утепления наружных ограждающих конструкций фактически"), VisibleInListView(false)]
        public string OutdoorIsolationMaterialFakt
        {
            get { return i_OutdoorIsolationMaterialFakt; }
            set { SetPropertyValue("OutdoorIsolationMaterialFakt", ref i_OutdoorIsolationMaterialFakt, value); }
        }

        private string i_SkylightsFillingProject;
        [Size(1000), DisplayName("Заполнение световых проемов по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public string SkylightsFillingProject
        {
            get { return i_SkylightsFillingProject; }
            set { SetPropertyValue("SkylightsFillingProject", ref i_SkylightsFillingProject, value); }
        }

        private string i_SkylightsFillingFakt;
        [Size(1000), DisplayName("Заполнение световых проемов фактически"), VisibleInListView(false)]
        public string SkylightsFillingFakt
        {
            get { return i_SkylightsFillingFakt; }
            set { SetPropertyValue("SkylightsFillingFakt", ref i_SkylightsFillingFakt, value); }
        }


        #endregion
        //[Association, DisplayName("Сети и системы инженерно технического обслуживания")]
        //public XPCollection<IngeneeringSystems> IngeneeringSystems
        //{
        //    get { return GetCollection<IngeneeringSystems>("IngeneeringSystems"); }
        //}



        #region Сети и системы инженерно технического обслуживания
        private bool i_ElectroProject;
        [DisplayName("Электроснабжение по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool ElectroProject
        {
            get { return i_ElectroProject; }
            set { SetPropertyValue("ElectroProject", ref i_ElectroProject, value); }
        }
        private bool i_ElectroFakt;
        [DisplayName("Электроснабжение фактически"), VisibleInListView(false)]
        public bool ElectroFakt
        {
            get { return i_ElectroFakt; }
            set { SetPropertyValue("ElectroFakt", ref i_ElectroFakt, value); }
        }
        private bool i_HeatProject;
        [DisplayName("Теплоснабжение по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool HeatProject
        {
            get { return i_HeatProject; }
            set { SetPropertyValue("HeatProject", ref i_HeatProject, value); }
        }
        private bool i_HeatFakt;
        [DisplayName("Теплоснабжение фактически"), VisibleInListView(false)]
        public bool HeatFakt
        {
            get { return i_HeatFakt; }
            set { SetPropertyValue("HeatFakt", ref i_HeatFakt, value); }
        }
        private bool i_WaterProject;
        [DisplayName("Водоснабжение по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool WaterProject
        {
            get { return i_WaterProject; }
            set { SetPropertyValue("WaterProject", ref i_WaterProject, value); }
        }
        private bool i_WaterFakt;
        [DisplayName("Водоснабжение фактически"), VisibleInListView(false)]
        public bool WaterFakt
        {
            get { return i_WaterFakt; }
            set { SetPropertyValue("WaterFakt", ref i_WaterFakt, value); }
        }
        private bool i_GazProject;
        [DisplayName("Газофикация по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool GazProject
        {
            get { return i_GazProject; }
            set { SetPropertyValue("GazProject", ref i_GazProject, value); }
        }
        private bool i_GazFakt;
        [DisplayName("Газофикация фактически"), VisibleInListView(false)]
        public bool GazFakt
        {
            get { return i_GazFakt; }
            set { SetPropertyValue("GazFakt", ref i_GazFakt, value); }
        }
        private bool i_HouseSeverageProject;
        [DisplayName("Водоотведение проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool HouseSeverageProject
        {
            get { return i_HouseSeverageProject; }
            set { SetPropertyValue("HouseSeverageProject", ref i_HouseSeverageProject, value); }
        }
        private bool i_HouseSeverageFakt;
        [DisplayName("Водоотведение фактически"), VisibleInListView(false)]
        public bool HouseSeverageFakt
        {
            get { return i_HouseSeverageFakt; }
            set { SetPropertyValue("HouseSeverageFakt", ref i_HouseSeverageFakt, value); }
        }
        private bool i_PhonesProject;
        [DisplayName("Телефонизация по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool PhonesProject
        {
            get { return i_PhonesProject; }
            set { SetPropertyValue("PhonesProject", ref i_PhonesProject, value); }
        }
        private bool i_PhonesFakt;
        [DisplayName("Телефонизация фактически"), VisibleInListView(false)]
        public bool PhonesFakt
        {
            get { return i_PhonesFakt; }
            set { SetPropertyValue("PhonesFakt", ref i_PhonesFakt, value); }
        }
        private bool i_TVProject;
        [DisplayName("Телевидение по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool TVProject
        {
            get { return i_TVProject; }
            set { SetPropertyValue("TVProject", ref i_TVProject, value); }
        }
        private bool i_TVFakt;
        [DisplayName("Телевидение фактически"), VisibleInListView(false)]
        public bool TVFakt
        {
            get { return i_TVFakt; }
            set { SetPropertyValue("TVFakt", ref i_TVFakt, value); }
        }
        private bool i_RadioProject;
        [DisplayName("Радиофикация по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool RadioProject
        {
            get { return i_RadioProject; }
            set { SetPropertyValue("RadioProject", ref i_RadioProject, value); }
        }
        private bool i_RadioFakt;
        [DisplayName("Радиофикация фактически"), VisibleInListView(false)]
        public bool RadioFakt
        {
            get { return i_RadioFakt; }
            set { SetPropertyValue("RadioFakt", ref i_RadioFakt, value); }
        }
        private bool i_SeverageProject;
        [DisplayName("Ливневая канализация по проекту"), VisibleInListView(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool SeverageProject
        {
            get { return i_SeverageProject; }
            set { SetPropertyValue("SeverageProject", ref i_SeverageProject, value); }
        }
        private bool i_SeverageFakt;
        [DisplayName("Ливневая канализация фактически"), VisibleInListView(false)]
        public bool SeverageFakt
        {
            get { return i_SeverageFakt; }
            set { SetPropertyValue("SeverageFakt", ref i_SeverageFakt, value); }
        }
        #endregion

        #region //Информационный лист (согласования)
        private string i_LetterInfo;
        [Size(500), DisplayName("Основание для рассмотрения"), VisibleInListView(false)]
        public string LetterInfo
        {
            get { return i_LetterInfo; }
            set { SetPropertyValue("LetterInfo", ref i_LetterInfo, value); }
        }
        private string i_PermInfo;
        [Size(255), DisplayName("Выдача разрешения"), VisibleInListView(false)]
        public string PermInfo
        {
            get { return i_PermInfo; }
            set { SetPropertyValue("PermInfo", ref i_PermInfo, value); }
        }
        private string i_PermInfoR;
        [Size(255), DisplayName("Отказ в выдаче разрешения"), VisibleInListView(false)]
        public string PermInfoR
        {
            get { return i_PermInfoR; }
            set { SetPropertyValue("PermInfoR", ref i_PermInfoR, value); }
        }
        private Employee i_Decision1;
        [DisplayName("Решение 1"), VisibleInListView(false)]
        public Employee Decision1
        {
            get { return i_Decision1; }
            set { SetPropertyValue("Decision1", ref i_Decision1, value); }
        }
        private Employee i_Decision2;
        [DisplayName("Решение 2"), VisibleInListView(false)]
        public Employee Decision2
        {
            get { return i_Decision2; }
            set { SetPropertyValue("Decision2", ref i_Decision2, value); }
        }
        private Employee i_AgreementEmploye1;
        [DisplayName("Согласующий 1"), VisibleInListView(false)]
        public Employee AgreementEmploye1
        {
            get { return i_AgreementEmploye1; }
            set { SetPropertyValue("AgreementEmploye1", ref i_AgreementEmploye1, value); }
        }
        private Employee i_AgreementEmploye2;
        [DisplayName("Согласующий 2"), VisibleInListView(false)]
        public Employee AgreementEmploye2
        {
            get { return i_AgreementEmploye2; }
            set { SetPropertyValue("AgreementEmploye2", ref i_AgreementEmploye2, value); }
        }
        private Employee i_AgreementEmploye3;
        [DisplayName("Согласующий 3"), VisibleInListView(false)]
        public Employee AgreementEmploye3
        {
            get { return i_AgreementEmploye3; }
            set { SetPropertyValue("AgreementEmploye3", ref i_AgreementEmploye3, value); }
        }
        private Employee i_AgreementEmploye4;
        [DisplayName("Согласующий 4"), VisibleInListView(false)]
        public Employee AgreementEmploye4
        {
            get { return i_AgreementEmploye4; }
            set { SetPropertyValue("AgreementEmploye4", ref i_AgreementEmploye4, value); }
        }
        private Employee i_AgreementEmploye5;
        [DisplayName("Согласующий 5"), VisibleInListView(false)]
        public Employee AgreementEmploye5
        {
            get { return i_AgreementEmploye5; }
            set { SetPropertyValue("AgreementEmploye5", ref i_AgreementEmploye5, value); }
        }
        private Employee i_AgreementEmploye6;
        [DisplayName("Согласующий 6"), VisibleInListView(false)]
        public Employee AgreementEmploye6
        {
            get { return i_AgreementEmploye6; }
            set { SetPropertyValue("AgreementEmploye6", ref i_AgreementEmploye6, value); }
        }
        private Employee i_AgreementEmploye7;
        [DisplayName("Согласующий 7"), VisibleInListView(false)]
        public Employee AgreementEmploye7
        {
            get { return i_AgreementEmploye7; }
            set { SetPropertyValue("AgreementEmploye7", ref i_AgreementEmploye7, value); }
        }
        #endregion

        [Association, DisplayName("Застройщики")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }
        [Association, DisplayName("Разрешения на строительство")]
        public XPCollection<ConstrPerm> ConstrPerm
        {
            get
            {
                //if (ConstrPermLink != null)
                //    ConstrPerm.Add(ConstrPermLink);
                return GetCollection<ConstrPerm>("ConstrPerm");
            }
        }
        [Association, DisplayName("Стадии строительства")]
        public XPCollection<ConstrStage> ConstrStages
        {
            get
            {
                return GetCollection<ConstrStage>("ConstrStages");
            }
        }
        //[Association, DisplayName("Сведения об объекте капитального строительства"), VisibleInListView(false)]
        //public XPCollection<ObjectConstractionInfo> ObjectConstractionInfo
        //{
        //    get
        //    {
        //        //if (ConstrPermLink != null)
        //        //    ObjectConstractionInfo.Add(ConstrPermLink.ObjectConstractionInfo.First());
        //        return GetCollection<ObjectConstractionInfo>("ObjectConstractionInfo");
        //    }
        //}
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            //if (DocKind == null)
            this.DocKind = Session.FindObject<dDocKind>(new BinaryOperator("Name", "Разрешение на ввод объекта в эксплуатацию"));
            //if (this.AddressObjectName == String.Empty || this.AddressObjectName == null)
            //{
            //    this.AddressObjectName = "Вологодская область, г. Вологда, ";
            //}
            //if (this.AddressLot == String.Empty || this.AddressLot == null)
            //{
            //    this.AddressLot = "Вологодская область, г. Вологда,  ";
            //}
            //this.DocNo = "35-32357000-         -" + DateTime.Now.Year.ToString();

            if (Empl == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    Empl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            if (signer == null)
            {
                signer = connect.FindFirstObject<Signer>(x => x.DefaultSignerFlag == true);
            }
            Session.CommitTransaction();
        }


        protected override void OnSaved()
        {
            //if (!IsDeleted)
            //{
            //    try
            //    {
            //        if (this.DocNo == String.Empty || this.DocNo == null)
            //        {
            //            double InitialRegNo = 0;
            //            try
            //            {
            //                InitialRegNo = this.DocKind.InitialRegNo;
            //            }
            //            catch { }
            //            if (DateTime.Now.Year.ToString() != "2016")
            //                InitialRegNo = 0;
            //            this.DocNo = Convert.ToString(InitialRegNo + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + DateTime.Now.Year.ToString(),
            //                string.Empty));
            //            this.DocNo = "35-32357000" + this.DocNo + "-" + DateTime.Now.Year.ToString();
            //        }
            //    }
            //    catch { }
            //    this.DocDate = DateTime.Now.Date;
            //}
            base.OnSaved();
        }

        #region Логика при изменении XPCollections
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //изменилась коллекция стадий
            if (property.Name == "ConstrStages")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(ConstrStages_CollectionChanged);
            }
            //изменилась коллекция разрешений на строительство
            if (property.Name == "ConstrPerm")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(ConstrPerm_CollectionChanged);
            }
            //изменилась коллекция входящих обращений
            if (property.Name == "InLetter")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(InLetters_CollectionChanged);
            }
            //изменилась коллекция застройщиков
            if (property.Name == "DocSubjects")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(DocSubjects_CollectionChanged);
            }
            //изменилась коллекция земельных участков
            if (property.Name == "Parcels")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            }
            return result;
        }

        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции стадий - заполнении коллекции разрешений на строительство, земельных участков, объекта строительства
        ///// </summary>
        private void ConstrStages_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                ConstrStage newObj = (ConstrStage)e.ChangedObject;
                foreach (ConstrPerm constrPerm in newObj.ConstrPerms)
                {
                    if (constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.Утвержден)
                        this.ConstrPerm.Add(constrPerm);
                }
                try
                {
                    foreach (Parcel parcel in newObj.Parcels)
                    {
                        this.Parcels.Add(parcel);
                    }
                }
                catch { }
                try
                {
                    CapitalStructureBase constrObj = newObj.CapitalStructureBase;
                    this.CapitalStructureBase.Add(constrObj);
                }
                catch { }
                //try
                //{
                //    i_ConstrAddress = "";
                //    if (!i_ConstrAddress.Contains(newObj.ConstrAddress))
                //        i_ConstrAddress += newObj.ConstrAddress + "; ";
                //    i_ConstrAddress = i_ConstrAddress.Trim();
                //    i_ConstrAddress = i_ConstrAddress.TrimEnd(';');
                //}
                //catch { }
            }
        }

        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции разрешений на строительство - заполнении коллекции  застройщиков, данных по разрешению
        ///// </summary>
        private void ConstrPerm_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                ConstrPerm newObj = (ConstrPerm)e.ChangedObject;


                this.WorkKind = newObj.WorkKind;
                this.BuildingCategory = newObj.BuildingCategory;
                try
                {
                    i_ObjectName = "";
                    if (!i_ObjectName.Contains(newObj.ObjectName))
                        i_ObjectName += newObj.ObjectName + "; ";
                    i_ObjectName = i_ObjectName.Trim();
                    i_ObjectName = i_ObjectName.TrimEnd(';');
                }
                catch { }
                try
                {
                    i_AddressObjectName = "";
                    if (!i_AddressObjectName.Contains(newObj.ObjectAddress))
                        i_AddressObjectName += newObj.ObjectAddress + "; ";
                    i_AddressObjectName = i_AddressObjectName.Trim();
                    i_AddressObjectName = i_AddressObjectName.TrimEnd(';');
                }
                catch { }

                foreach (GeneralSubject DocSubject in newObj.DocSubjects)
                {
                    this.DocSubjects.Add(DocSubject);
                }
                try
                {
                    if(i_ConstrPermInfo == null)
                        i_ConstrPermInfo = "";
                    string docInfo = "";
                    docInfo = String.Format($"от {newObj.DocDate.ToShortDateString()} № {newObj.DocNo}"); 
                    if (!i_ConstrPermInfo.Contains(docInfo))
                        i_ConstrPermInfo += docInfo + "; ";
                    i_ConstrPermInfo = i_ConstrPermInfo.Trim();
                    i_ConstrPermInfo = i_ConstrPermInfo.TrimEnd(';');
                }
                catch { }

                try {
                    if (i_ConstrDeveloperString == null)
                        i_ConstrDeveloperString = "";
                    string constrDeveloperString = "";
                    constrDeveloperString = newObj.ConstrDeveloperString;
                    if (!i_ConstrDeveloperString.Contains(constrDeveloperString))
                        i_ConstrDeveloperString += constrDeveloperString + ", ";
                    i_ConstrDeveloperString = i_ConstrDeveloperString.Trim();
                    i_ConstrDeveloperString = i_ConstrDeveloperString.TrimEnd(',');
                }
                catch { }
                try
                {
                    if (i_ConstrDeveloperContactInfo == null)
                        i_ConstrDeveloperContactInfo = "";
                    string constrDeveloperContactInfo = "";
                    constrDeveloperContactInfo = newObj.ConstrDeveloperContactInfo;
                    if (!i_ConstrDeveloperContactInfo.Contains(constrDeveloperContactInfo))
                        i_ConstrDeveloperContactInfo += constrDeveloperContactInfo + ", ";
                    i_ConstrDeveloperContactInfo = i_ConstrDeveloperContactInfo.Trim();
                    i_ConstrDeveloperContactInfo = i_ConstrDeveloperContactInfo.TrimEnd(',');
                }
                catch { }
                try
                {
                    if (i_ConstrDeveloperINN == null)
                        i_ConstrDeveloperINN = "";
                    string constrDeveloperINN = "";
                    constrDeveloperINN = newObj.ConstrDeveloperINN;
                    if (!i_ConstrDeveloperINN.Contains(constrDeveloperINN))
                        i_ConstrDeveloperINN += constrDeveloperINN + ", ";
                    i_ConstrDeveloperINN = i_ConstrDeveloperINN.Trim();
                    i_ConstrDeveloperINN = i_ConstrDeveloperINN.TrimEnd(',');
                }
                catch { }
            }
        }

        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции входящих обращений - заполнение листа по застройщикам
        ///// </summary>
        private void InLetters_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                InLetter newObj = (InLetter)e.ChangedObject;
                if (newObj.DocSubjects.Count > 0)
                {
                    foreach (GeneralSubject DocSubject in newObj.DocSubjects)
                    {
                        DocSubjects.Add(DocSubject);

                        foreach (CapitalStructureBase constrObj in CapitalStructureBase)
                        {
                            constrObj.DocSubjects.Add(DocSubject);
                        }
                        foreach (ConstrStage constrObj in ConstrStages)
                        {
                            constrObj.DocSubjects.Add(DocSubject);
                        }
                    }

                }
            }
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции застройщиков - заполнение поля по застройщикам
        ///// </summary>
        private void DocSubjects_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetDevelopers();
            }
        }
        /// <summary>
        /// Заполенение поля Застройщик (string) по листу застройщиков (XPCollection)
        /// </summary>
        private void SetDevelopers()
        {
            string dev = "";
            string contact = "";
            string inn = "";
            foreach (GeneralSubject subj in DocSubjects)
            {
                if (!String.IsNullOrEmpty(subj.FullName))
                {
                    if (dev == "")
                    {
                        dev = subj.FullName;
                    }
                    else
                      if (!dev.Contains(subj.FullName))
                        dev += ", " + subj.FullName;
                }
                if (!String.IsNullOrEmpty(subj.FullContactInfo))
                {
                    if (contact == "")
                    {
                        contact = subj.FullContactInfo;
                    }
                    else
                      if (!contact.Contains(subj.FullContactInfo))
                        contact += ", " + subj.FullContactInfo;
                }
                // ИНН
                if (!String.IsNullOrEmpty(subj.INN))
                {
                    if (inn == "")
                    {
                        inn = subj.INN;
                    }
                    else
                      if (!inn.Contains(subj.INN))
                        inn += ", " + subj.INN;
                }
            }
            if (dev.Length > 500)
                dev = dev.Remove(499, dev.Length);
            ConstrDeveloperString = dev;
            if (contact.Length > 500)
                contact = contact.Remove(499, contact.Length);
            ConstrDeveloperContactInfo = contact;
            if (inn.Length > 255)
                inn = inn.Remove(254, inn.Length);
            ConstrDeveloperINN = inn;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции земельных участков - заполнение поля кадастровых номеров
        ///// </summary>
        private void Parcels_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetCadNoms();
            }
        }

        /// <summary>
        /// Заполенение поля кадастровых номеров(string) по листу земельных участков (XPCollection)
        /// </summary>
        private void SetCadNoms()
        {
            string res = "";
            foreach (Parcel obj in Parcels)
            {
                if (res == "")
                    res = obj.CadastralNumber;
                else
                    if (!res.Contains(obj.CadastralNumber))
                    res += ", " + obj.CadastralNumber;
            }
            if (res.Length > 255)
                res = res.Remove(254, res.Length);
            LotCadNo = res;
        }
        #endregion

    }
}
