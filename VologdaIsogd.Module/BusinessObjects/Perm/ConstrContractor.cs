﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.Enums;
using AISOGD.Perm;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.Constr
{
    [ModelDefault("Caption", "Подрядчик"), System.ComponentModel.DefaultProperty("RequesterString")]
    public class ConstrContractor : BaseObjectXAF
    {
        public ConstrContractor(Session session) : base(session) { }
        private string i_requesterString;
        private string i_requesterAddress;
        private string i_requesterPhones;
        private Org i_org;

        private string i_LicenseNo;
        private DateTime i_LicenseDate;
        private string i_LicenseIssued;
        private string i_LicenseSeries;

        private string _code;
        [Size(255), DisplayName("Код")]
        public string code
        {
            get
            {
                return _code;
            }
            set { try { _code = value; } catch { } }
        }

        [DisplayName("Подрядчик")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Наименование подрядчика")]
        public string RequesterString
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterString))
                        i_requesterString = GetRequesterInfo();
                }
                catch { }
                return i_requesterString;
            }
            set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Адрес подрядчика")]
        public string RequesterAddress
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterAddress))
                        i_requesterAddress = GetRequesterAddress();
                }
                catch { }
                return i_requesterAddress;
            }
            set { SetPropertyValue("RequesterAddress", ref i_requesterAddress, value); }
        }

        [Size(500), DisplayName("Телефоны")]
        public string RequesterPhones
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_requesterPhones))
                        i_requesterPhones = GetRequesterPhones();
                }
                catch { }
                return i_requesterPhones;
            }
            set { SetPropertyValue("RequesterPhones", ref i_requesterPhones, value); }
        }


        private string _requesterFax;
        [Size(255), DisplayName("Факс")]
        public string RequesterFax
        {
            get
            {
                return _requesterFax;
            }
            set { _requesterFax = value; }
        }

        private string _FIO;
        [Size(SizeAttribute.Unlimited), DisplayName("ФИО")]
        public string fio
        {
            get
            {
                return _FIO;
            }
            set { _FIO = value; }
        }

        [Size(255), DisplayName("Номер лицензии")]
        public string LicenseNo
        {
            get { return i_LicenseNo; }
            set { SetPropertyValue("LicenseNo", ref i_LicenseNo, value); }
        }


        private DateTime _dateZak;
        [DisplayName("DATE_L_ZAK")]
        public DateTime dateZak
        {
            get { return _dateZak; }
            set { try { _dateZak = value; } catch { } }
        }

        [Size(255), DisplayName("Серия лицензии")]
        public string LicenseSeries
        {
            get { return i_LicenseSeries; }
            set { SetPropertyValue("LicenseSeries", ref i_LicenseSeries, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Кем выдано")]
        public string LicenseIssued
        {
            get { return i_LicenseIssued; }
            set { SetPropertyValue("LicenseIssued", ref i_LicenseIssued, value); }
        }
        [DisplayName("Дата выдачи лицензии")]
        public DateTime LicenseDate
        {
            get { return i_LicenseDate; }
            set { SetPropertyValue("LicenseDate", ref i_LicenseDate, value); }
        }

        private string _note;
        [Size(SizeAttribute.Unlimited), DisplayName("Примечание")]
        public string note
        {
            get { return _note; }
            set { try { _note = value; } catch { } }
        }

        private string _status;
        [DisplayName("Статус")]
        public string status
        {
            get { return _status; }
            set { try { _status = value; } catch { } }
        }

        public string GetRequesterInfo()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.FullName;
            }
            return result;
        }
        public string GetRequesterAddress()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.address;
            }
            return result;
        }
        public string GetRequesterPhones()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.phones;
            }
            return result;
        }
        [Association, DisplayName("Разрешения на ввод объектов в эксплуатацию")]
        public XPCollection<UsePerm> UsePerm
        {
            get
            {
                return GetCollection<UsePerm>("UsePerm");
            }
        }
    }
}
