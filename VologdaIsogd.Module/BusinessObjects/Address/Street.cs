﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Улица"), NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class Street : BaseAO
    {
        public Street(Session session) : base(session) { }

        private dStreetKind _streetKind;
        private CityRegion _cityRegion;
        private City _city;
        private string _codeOKATO;
        private string _codeKLADR;
        private string _name;

        //public void testFlow()
        //{
        //    _name = "Техпроцесс отработал";
        //}
        
        [Association, DisplayName("Район/Муниципальное образование")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }
        [Association, DisplayName("Район города")]
        public CityRegion CityRegion
        {
            get { return _cityRegion; }
            set
            {
                SetPropertyValue("CityRegion", ref _cityRegion, value);
            }
        }
        [DisplayName("Вид улицы")]
        public dStreetKind StreetKind
        {
            get { return _streetKind; }
            set {
                try {
                    _streetKind = value;
                    OnChanged("StreetKind");
                } catch { }
            }
        }
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnChanged("Name");
            }
        }
        [Size(32), DisplayName("Код ОКАТО")]
        public string Code {
            get { return _codeOKATO; }
            set {
                _codeOKATO = value;
                OnChanged("Code");
            }
        }

        [Size(32), DisplayName("Код КЛАДР")]
        public string CodeKLADR
        {
            get { return _codeKLADR; }
            set
            {
                _codeKLADR = value;
                OnChanged("CodeKLADR");
            }
        }


        [Size(510), DisplayName("Полное наименование")]
        public string FullName {
            get {
                if(_name != null)
                    try
                    {
                        return _streetKind.Format.Replace("{Name}", _name);
                    }
                    catch { return _name; }
                else
                    return String.Empty;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
