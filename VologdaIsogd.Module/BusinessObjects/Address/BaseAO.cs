﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Адресообразующий элемент")]//, NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class BaseAO : BaseObjectXAF
    {
        public BaseAO(Session session) : base(session) { }


        //Глобальный уникальный идентификатор адресного объекта 
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [DisplayName("Глобальный идентификатор объекта")]
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string AOGUID { get; set; }


        //[DisplayName("Записи ФИАС")]
        //[Association("AOBaseObjectXAF-AOFias")]
        //public XPCollection<AISOGD.Address.FIAS.Object> FIASObj
        //{
        //    get
        //    {
        //        return GetCollection<AISOGD.Address.FIAS.Object>("FIASObj");
        //    }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }

}
