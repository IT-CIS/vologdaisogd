﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Район города/Сельсовет"), NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class CityRegion : BaseAO
    {
        public CityRegion(Session session) : base(session) { }

        private string _name;

        private City i_City;
        [Association, DisplayName("Муниципальное образование")]
        public City City
        {
            get { return i_City; }
            set { SetPropertyValue("City", ref i_City, value); }
        }

        private dCityRegionKind _regionKind;
        [DisplayName("Тип")]
        public dCityRegionKind cityRegionKind
        {
            get { return _regionKind; }
            set { SetPropertyValue("CityRegionKind", ref _regionKind, value); }
        }


        [Size(255), DisplayName("Наименование")]
        public string Name {
            get { return _name; }
            set {
                _name = value;
                OnChanged("Name");
            }
        }



        [Size(500), DisplayName("Полное наименование")]
        public string FullName
        {
            get
            {
                if (_name != null)
                    return Name;
                   // if (_regionKind != null)
                       
                  //  else
                   //     return String.Empty;
                else
                    return String.Empty;
            }
        }
        [Association, DisplayName("Улица")]
        public XPCollection<Street> Streets
        {
            get
            {
                return GetCollection<Street>("Streets");
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
    
}
