﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Муниципальное образование"), NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class City : BaseAO
    {
        public City(Session session) : base(session) { }


        private string i_Country;
        [Size(255), DisplayName("Страна")]
        public string Country
        {
            get { return i_Country; }
            set { SetPropertyValue("Country", ref i_Country, value); }
        }

        private Region i_Region;
        [Association, DisplayName("Субъект Федерации")]
        public Region Region
        {
            get { return i_Region; }
            set { SetPropertyValue("Region", ref i_Region, value); }
        }
        
        private dCityKind i_CityKind;
        [DisplayName("Тип муниципального образования")]
        public dCityKind CityKind
        {
            get { return i_CityKind; }
            set { SetPropertyValue("CityKind", ref i_CityKind, value); }
        }

        private string i_Code;
        [Size(32), DisplayName("Код")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        [Size(510), DisplayName("Полное наименование")]
        public string FullName
        {
            get
            {
                if (i_Name != null)
                    return CityKind.Name + " " + i_Name;
                else
                    return String.Empty;
            }
        }
        [Association, DisplayName("Районы города/Сельсоветы/Населённые пункты")]
        public XPCollection<CityRegion> CityRegions
        {
            get
            {
                return GetCollection<CityRegion>("CityRegions");
            }
        }
        [Association, DisplayName("Улица")]
        public XPCollection<Street> Streets
        {
            get
            {
                return GetCollection<Street>("Streets");
            }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            i_Country = "Российская Федерация";
        }
    }
}
