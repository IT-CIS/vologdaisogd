﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using WinAisogdIngeo.Module.SystemDir;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Адресообразующий элемент"), NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class BaseAO : BaseObjectXAF
    {
        public BaseAO(Session session) : base(session) { }

        //[VisibleInDetailView(false), VisibleInListView(false)]
        public string AOGUID
        {
            get;
            set;
        }


        [DisplayName("Записи ФИАС")]
        [Association("AOBaseObjectXAF-AOFias")]
        public XPCollection<AISOGD.Address.FIAS.Object> FIASObj
        {
            get
            {
                return GetCollection<AISOGD.Address.FIAS.Object>("FIASObj");
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }

}
