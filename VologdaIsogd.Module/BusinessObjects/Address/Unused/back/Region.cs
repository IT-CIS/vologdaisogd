﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using WinAisogdIngeo.Module.SystemDir;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Субъект Федерации"), NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class Region : BaseAO
    {
        public Region(Session session) : base(session) { }

        private string i_country;
        [DisplayName("Страна")]
        public string Country
        {
            get { return i_country; }
            set { SetPropertyValue("Country", ref i_country, value); }
        }

        private RegionKind i_regionKind;
        [DisplayName("Тип региона")]
        public RegionKind RegionKind
        {
            get { return i_regionKind; }
            set { SetPropertyValue("RegionKind", ref i_regionKind, value); }
        }

        private string i_code;
        [Size(32), DisplayName("Код")]
        public string Code
        {
            get { return i_code; }
            set { SetPropertyValue("Code", ref i_code, value); }
        }

        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        [Size(500), DisplayName("Полное наименование")]
        public string FullName {
            get {
                if (i_Name != null)
                    return i_regionKind.Format.Replace("{Name}", i_Name);
                else
                    return String.Empty;
            }
        }
        [Association, DisplayName("Муниципальные образования")]
        public XPCollection<City> Citys
        {
            get
            {
                return GetCollection<City>("Citys");
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            i_country = "Российская Федерация";
        }
    }
}
