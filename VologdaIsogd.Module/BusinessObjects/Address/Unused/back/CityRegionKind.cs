﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Тип района МО"), NavigationItem("Адресный реестр")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class CityRegionKind : BaseObject
    {
        public CityRegionKind(Session session) : base(session) { }

        private string _name;
        private string _code;

        [Size(255), DisplayName("Код")]
        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                OnChanged("Code");
            }
        }

        [Size(255), DisplayName("Наименование")]
        public string Name {
            get { return _name; }
            set {
                _name = value;
                OnChanged("Name");
            }
        }

        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
