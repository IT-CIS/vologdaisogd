﻿using AISOGD.Address;
//using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Ingeo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AISOGD.AdressDoc
{
    /// <summary>
    /// Документ об адресе
    /// </summary>
    [ModelDefault("Caption", "Документ об адресе"), NavigationItem("Подготовка документов"), System.ComponentModel.DefaultProperty("descript")]
    public class AdressDoc : GeneralDocBase
    {
        public AdressDoc(Session session)
            : base(session) { }

        public AdressDoc(Session session, string aObjectClass, string gisObjectID)
            : base(session) { }

        private DateTime _letterDate;
        private string _letterNo;
        private DateTime i_ValidDateFrom;
        private string i_ObjectName;
        private string i_StreetName;
        private string i_addressName;
        private string i_reasonDoc;
        private AddressDocType i_AddressDocType;
        private string i_adressDecription;
        string i_cadNo = "";

        private const string descriptFormat = "Документ об адресе № {DocNo} от {DocDate}";
        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Документ об адресе № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        [DisplayName("Тип документа об адресе")]
        [ImmediatePostData]
        public AddressDocType addressDocType
        {
            get { return i_AddressDocType; }
            set { SetPropertyValue("addressDocType", ref i_AddressDocType, value); }
        }

        //[DisplayName("Присвоен адрес")]
        //[ImmediatePostData]
        ////[Appearance("isChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType= 'Аннулирование'", Context = "DetailView")]
        //public string AddressName
        //{
        //    get
        //    {
        //        return i_addressName;
        //    }
        //    set
        //    {
        //        SetPropertyValue("AddressName", ref i_addressName, value);
        //    }
        //}

        [DisplayName("На основании")]
        [Size(1000)]
        public string reasonDoc
        {
            get
            {
                return i_reasonDoc;
            }
            set
            {
                SetPropertyValue("reasonDoc", ref i_reasonDoc, value);
            }
        }

        private DateTime i_reasonDocDate;
        [Appearance("adressDocsDateChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        [DisplayName("На основании (дата)")]
        public DateTime reasonDocDate
        {
            get
            {
                return i_reasonDocDate;
            }
            set
            {
                SetPropertyValue("reasonDocDate", ref i_reasonDocDate, value);
            }
        }


        [DisplayName("Здание")]
        public string objectName
        {
            get
            {
                return i_ObjectName;
            }
            set
            {
                SetPropertyValue("objectName", ref i_ObjectName, value);
            }
        }



        [DisplayName("Кадастровый номер здания")]
        public string cadNo
        {
            get
            {
                return i_cadNo;
            }
            set
            {
                SetPropertyValue("cadNo", ref i_cadNo, value);
            }
        }

        private string i_lotCadNo;
        [DisplayName("Кадастровый номер земельного участка")]
        public string lotCadNo
        {
            get
            {
                return i_lotCadNo;
            }
            set
            {
                SetPropertyValue("lotCadNo", ref i_lotCadNo, value);
            }
        }




        private string i_lotUsing;
        [DisplayName("Разрешенное использование ЗУ")]
        public string lotUsing
        {
            get
            {
                return i_lotUsing;
            }
            set
            {
                SetPropertyValue("lotUsing", ref i_lotUsing, value);
            }
        }

        private string i_oldStreet;

        [Appearance("oldStreetChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        [DisplayName("Улица (старый адрес)")]
        public string oldStreet
        {
            get
            {
                return i_oldStreet;
            }
            set
            {
                SetPropertyValue("oldStreet", ref i_oldStreet, value);
            }
        }

        private string i_oldHouseNo;
        [Appearance("oldHouseNoChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        [DisplayName("Номер дома (старый адрес)")]
        public string oldHouseNo
        {
            get
            {
                return i_oldHouseNo;
            }
            set
            {
                SetPropertyValue("oldHouseNo", ref i_oldHouseNo, value);
            }
        }

        [DisplayName("Улица")]
        public string streetName
        {
            get
            {
                return i_StreetName;
            }
            set
            {
                SetPropertyValue("streetName", ref i_StreetName, value);
            }
        }


        private string i_houseNo;
        [DisplayName("Номер дома")]
        public string  houseNo
        {
            get
            {
                return i_houseNo;
            }
            set
            {
                SetPropertyValue("houseNo", ref i_houseNo, value);
            }
        }

        private string i_GUP;
        [DisplayName("Справка ГУП")]
        [Appearance("GUPChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        [Size(255)]
        public string GUP
        {
            get
            {
                return i_GUP;
            }
            set
            {
                SetPropertyValue("GUP", ref i_GUP, value);
            }
        }

        private DateTime i_DateGUP;
        [DisplayName("Справка ГУП (дата)")]
        [Appearance("DateGUPChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        [Size(255)]
        public DateTime DateGUP
        {
            get
            {
                return i_DateGUP;
            }
            set
            {
                SetPropertyValue("DateGUP", ref i_DateGUP, value);
            }
        }



        [ImmediatePostData]
        [DisplayName("Примечание")]
        [Size(SizeAttribute.Unlimited)]
        public string adressDecription
        {
            get
            {
                return i_adressDecription;
            }
            set
            {
                SetPropertyValue("adressDecription", ref i_adressDecription, value);
            }
        }


        [ImmediatePostData]
        [DisplayName("Дата утверждения")]
        public DateTime ValidDateFrom
        {
            get { return i_ValidDateFrom; }
            set { SetPropertyValue("ValidDateFrom", ref i_ValidDateFrom, value); }
        }

        private string _Requester = "";

        [ImmediatePostData]
        [Size(32), DisplayName("Заявитель")]
        public string Requester
        {
            get
            {
                try
                {
                    if (InLetter.Count > 0)
                        if (InLetter[0].DocSubjects.Count > 0)
                            _Requester = InLetter[0].DocSubjects[0].RequesterString;
                }
                catch { }
                return _Requester;
            }
            set { SetPropertyValue("Requester", ref _Requester, value); }
        }

        [ImmediatePostData]
        [Size(32), DisplayName("Номер заявления")]
        public string LetterNo
        {
            get
            {
                try
                {
                    if (InLetter.Count > 0)
                        _letterNo = InLetter[0].RegNo;
                }
                catch { }
                return _letterNo;
            }
            set { SetPropertyValue("LetterNo", ref _letterNo, value); }
        }
        private string i_GISID;

        [Size(32), DisplayName("Идентификатор ГИС"), VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public string GISID
        {
            get
            {
                return i_GISID;
            }
            set { SetPropertyValue("GISID", ref i_GISID, value); }
        }

        private string i_objectClass;

        [Size(128), DisplayName("Класс объекта"), VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public string objectClass
        {
            get
            {
                return i_objectClass;
            }
            set { SetPropertyValue("objectClass", ref i_objectClass, value); }
        }

        [ImmediatePostData]
        [DisplayName("Дата заявления")]
        public DateTime LetterDate
        {
            get
            {
                _letterDate = DateTime.Now.Date;
                if (InLetter.Count > 0)
                    _letterDate = InLetter[0].RegDate;
                return _letterDate;
            }
            set { SetPropertyValue("LetterDate", ref _letterDate, value); }
        }

        private Image image;
        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("Схема")]
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Image
        {
            get { return image; }
            set { SetPropertyValue("Image", ref image, value); }
        }




        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Документ об адресе"));
            Session.CommitTransaction();
            i_ValidDateFrom = DateTime.Now.Date;

            addressDocType = AddressDocType.Присвоение;



        }


    }
}
