﻿using AISOGD.Address;
//using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Ingeo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AISOGD.AdressDoc
{
    /// <summary>
    /// Документ об адресе
    /// </summary>
    [ModelDefault("Caption", "Документ об адресе"), NavigationItem("Подготовка документов"), System.ComponentModel.DefaultProperty("descript")]
    public class AdressDoc : GeneralDocBase
    {
        public AdressDoc(Session session)
            : base(session) { }

        public AdressDoc(Session session, string aObjectClass, string gisObjectID)
            : base(session) { }

        private DateTime _letterDate;
        private string _letterNo;
        private DateTime i_ValidDateFrom;
        private string i_ObjectName;
        private string i_StreetName;
        private string i_addressName;
        private string i_reasonDoc;
        private AddressDocType i_AddressDocType;
        private string i_adressDecription;
        string i_cadNo = "";

        private const string descriptFormat = "Документ об адресе № {DocNo} от {DocDate}";
        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Документ об адресе № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        [DisplayName("Тип документа об адресе")]
        [ImmediatePostData]
        public AddressDocType addressDocType
        {
            get { return i_AddressDocType; }
            set { SetPropertyValue("addressDocType", ref i_AddressDocType, value); }
        }

        private AddressKind i_AddressKind;
        [DisplayName("Вид адреса")]
        public AddressKind AddressKind
        {
            get { return i_AddressKind; }
            set { SetPropertyValue("AddressKind", ref i_AddressKind, value); }
        }

        private string i_ZipPostal;
        [Size(50), DisplayName("Почтовый индекс")]
        public string ZipPostal
        {
            get { return i_ZipPostal; }
            set { SetPropertyValue("ZipPostal", ref i_ZipPostal, value); }
        }
        private string i_Country;
        [Size(255), DisplayName("Страна")]
        public string Country
        {
            get { return i_Country; }
            set { SetPropertyValue("Country", ref i_Country, value); }
        }

        private AISOGD.Address.Region i_Region;
        [DisplayName("Субъект Федерации")]
        public AISOGD.Address.Region Region
        {
            get { return i_Region; }
            set { SetPropertyValue("Region", ref i_Region, value); }
        }
        private City i_City;
        [DisplayName("Муниципальное образование")]
        public City City
        {
            get { return i_City; }
            set { SetPropertyValue("City", ref i_City, value); }
        }
        private CityRegion i_CityRegion;
        [DisplayName("Район города/Сельсовет")]
        public CityRegion CityRegion
        {
            get { return i_CityRegion; }
            set { SetPropertyValue("CityRegion", ref i_CityRegion, value); }
        }
        private Street i_Street;
        [DisplayName("Улица")]
        public Street Street
        {
            get { return i_Street; }
            set { SetPropertyValue("Street", ref i_Street, value); }
        }


        private string i_House;
        [Size(100), DisplayName("Дом")]
        public string House
        {
            get { return i_House; }
            set { SetPropertyValue("House", ref i_House, value); }
        }
        private string i_Building;
        [Size(100), DisplayName("Строение")]
        public string Building
        {
            get { return i_Building; }
            set { SetPropertyValue("Building", ref i_Building, value); }
        }
        private string i_Corpus;
        [Size(100), DisplayName("Корпус")]
        public string Corpus
        {
            get { return i_Corpus; }
            set { SetPropertyValue("Corpus", ref i_Corpus, value); }
        }
        private string i_Structure;
        [Size(50), DisplayName("Сооружение")]
        public string Structure
        {
            get { return i_Structure; }
            set { SetPropertyValue("Structure", ref i_Structure, value); }
        }
        private string i_Description;
        [Size(510), DisplayName("Место расположения")]
        public string Description
        {
            get { return i_Description; }
            set { SetPropertyValue("Description", ref i_Description, value); }
        }
        private string i_Other;
        [Size(2500), DisplayName("Иное")]
        public string Other
        {
            get { return i_Other; }
            set { SetPropertyValue("Other", ref i_Other, value); }
        }
        private string i_CodeOKATO;
        [Size(50), DisplayName("Код ОКАТО")]
        public string CodeOKATO
        {
            get { return i_CodeOKATO; }
            set { SetPropertyValue("CodeOKATO", ref i_CodeOKATO, value); }
        }
        private string i_CodeKLADR;
        [Size(32), DisplayName("Код КЛАДР")]
        public string CodeKLADR
        {
            get { return i_CodeKLADR; }
            set { SetPropertyValue("CodeKLADR", ref i_CodeKLADR, value); }
        }
        private string i_codeOKTMO;
        [Size(32), DisplayName("Код ОКТМО")]
        public string CodeOKTMO
        {
            get { return i_codeOKTMO; }
            set { SetPropertyValue("CodeOKTMO", ref i_codeOKTMO, value); }
        }
        private string i_CancelReason;
        [Size(500), DisplayName("Причина аннулирования")]
        public string CancelReason
        {
            get { return i_CancelReason; }
            set { SetPropertyValue("CancelReason", ref i_CancelReason, value); }
        }
        //private string i_CancelReason;
        //[Size(500), DisplayName("Причина аннулирования")]
        //public string CancelReason
        //{
        //    get { return i_CancelReason; }
        //    set { SetPropertyValue("CancelReason", ref i_CancelReason, value); }
        //}
        //private string i_GUP;
        //[DisplayName("Справка ГУП")]
        //[Appearance("GUPChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        //[Size(255)]
        //public string GUP
        //{
        //    get
        //    {
        //        return i_GUP;
        //    }
        //    set
        //    {
        //        SetPropertyValue("GUP", ref i_GUP, value);
        //    }
        //}

        //private DateTime i_DateGUP;
        //[DisplayName("Справка ГУП (дата)")]
        //[Appearance("DateGUPChange", Visibility = ViewItemVisibility.Hide, Criteria = "addressDocType != 'Аннулирование'", Context = "DetailView")]
        //[Size(255)]
        //public DateTime DateGUP
        //{
        //    get
        //    {
        //        return i_DateGUP;
        //    }
        //    set
        //    {
        //        SetPropertyValue("DateGUP", ref i_DateGUP, value);
        //    }
        //}



        //[ImmediatePostData]
        [DisplayName("Примечание")]
        [Size(SizeAttribute.Unlimited)]
        public string adressDecription
        {
            get
            {
                return i_adressDecription;
            }
            set
            {
                SetPropertyValue("adressDecription", ref i_adressDecription, value);
            }
        }


        [ImmediatePostData]
        [DisplayName("Дата утверждения")]
        public DateTime ValidDateFrom
        {
            get { return i_ValidDateFrom; }
            set { SetPropertyValue("ValidDateFrom", ref i_ValidDateFrom, value); }
        }

        private string _Requester = "";

        [ImmediatePostData]
        [Size(32), DisplayName("Заявитель")]
        public string Requester
        {
            get
            {
                try
                {
                    if (InLetter.Count > 0)
                        if (InLetter[0].DocSubjects.Count > 0)
                            _Requester = InLetter[0].DocSubjects[0].RequesterString;
                }
                catch { }
                return _Requester;
            }
            set { SetPropertyValue("Requester", ref _Requester, value); }
        }

        [ImmediatePostData]
        [Size(32), DisplayName("Номер заявления")]
        public string LetterNo
        {
            get
            {
                try
                {
                    if (InLetter.Count > 0)
                        _letterNo = InLetter[0].RegNo;
                }
                catch { }
                return _letterNo;
            }
            set { SetPropertyValue("LetterNo", ref _letterNo, value); }
        }
        private string i_GISID;

        //[Size(32), DisplayName("Идентификатор ГИС"), VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        //public string GISID
        //{
        //    get
        //    {
        //        return i_GISID;
        //    }
        //    set { SetPropertyValue("GISID", ref i_GISID, value); }
        //}

        //private string i_objectClass;

        //[Size(128), DisplayName("Класс объекта"), VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        //public string objectClass
        //{
        //    get
        //    {
        //        return i_objectClass;
        //    }
        //    set { SetPropertyValue("objectClass", ref i_objectClass, value); }
        //}

        [ImmediatePostData]
        [DisplayName("Дата заявления")]
        public DateTime LetterDate
        {
            get
            {
                _letterDate = DateTime.Now.Date;
                if (InLetter.Count > 0)
                    _letterDate = InLetter[0].RegDate;
                return _letterDate;
            }
            set { SetPropertyValue("LetterDate", ref _letterDate, value); }
        }

        private Image image;
        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("Схема")]
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Image
        {
            get { return image; }
            set { SetPropertyValue("Image", ref image, value); }
        }

        [Association, DisplayName("Адреса объектов")]
        public XPCollection<AISOGD.Address.Address> Adresses
        {
            get
            {
                return GetCollection<AISOGD.Address.Address>("Adresses");
            }
        }
        [Association, DisplayName("Адреса помещений")]
        public XPCollection<AISOGD.Address.ApartmentAddress> ApartmentAdresses
        {
            get
            {
                return GetCollection<AISOGD.Address.ApartmentAddress>("ApartmentAdresses");
            }
        }
        [Association, DisplayName("Документы основания")]
        public XPCollection<GeneralDocBase> GeneralDocsReasons
        {
            get
            {
                return GetCollection<GeneralDocBase>("GeneralDocsReasons");
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Документ об адресе"));
            Connect connect = Connect.FromSession(Session);
            Session.CommitTransaction();
            i_ValidDateFrom = DateTime.Now.Date;

            addressDocType = AddressDocType.Присвоение;
            i_Country = "Российская Федерация";
            try { i_Region = connect.FindFirstObject<AISOGD.Address.Region>(mc => mc.Name == "Камчатский"); }//Settings.RegionName); }
            catch { }
            try { i_City = connect.FindFirstObject<AISOGD.Address.City>(mc => mc.Name == "Петропавловск-Камчатский"); }
            catch { }
        }


    }
}
