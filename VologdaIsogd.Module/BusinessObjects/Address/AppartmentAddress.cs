﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using AISOGD.Subject;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.General;
using DevExpress.ExpressApp.Model;
using AISOGD.Enums;
using AISOGD.SystemDir;
//using AISOGD.Constr;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{
    [ModelDefault("Caption", "Адрес помещения"), System.ComponentModel.DefaultProperty("FullAddress"), ImageName("BO_Address")]
    [NavigationItem("Адресный реестр")]
    public class ApartmentAddress : BaseObjectXAF//, IAddressObject
    {
        public ApartmentAddress(Session session) : base(session) { }

        private const string defaultFullAddressFormat = "{Country}, {ZipPostal}, {Region.FullName}, {City.FullName}, {Street.FullName}, д.{House}," +
            " стр.{Building}, корп.{Corpus}, соор.{Structure}, {ApartmentType.Name} {Flat}";
        private const string shortAddressFormat = "{City.FullName}, {Street.FullName}, д.{House}," +
            " стр.{Building}, корп.{Corpus}, соор.{Structure}, {ApartmentType.Name} {Flat}";

        private static string fullAddressFormat = defaultFullAddressFormat;
        public static string FullAddressFormat
        {
            get { return fullAddressFormat; }
            set
            {
                fullAddressFormat = value;
                if (string.IsNullOrEmpty(fullAddressFormat))
                {
                    fullAddressFormat = defaultFullAddressFormat;
                }
            }
        }

        private SubjectBase _subject;
        private dAddressKind _addressKind;
        private Street _street;
        private City _city;
        private Region _region;
        private CityRegion _cityRegion;
        private string _zipPostal;
        private string _country;
        private string _house;
        private string _building;
        private string _corpus;
        private string _structure;
        private string _flat;
        private string _description;
        private string _codeOKATO;
        private string _codeKLADR;


        private eAddressStatus _AddressStatus;
        [DisplayName("Статус адреса")]
        public eAddressStatus AddressStatus
        {
            get { return _AddressStatus; }
            set { SetPropertyValue("AddressStatus", ref _AddressStatus, value); }
        }

        [DisplayName("Вид адреса")]
        public dAddressKind AddressKind
        {
            get { return _addressKind; }
            set
            {
                try
                {
                    _addressKind = value;
                    OnChanged("AddressKind");
                }
                catch { }
            }
        }
        //private Apartment i_Apartment;
        //[Association, DisplayName("Помещение")]
        //public Apartment Apartment
        //{
        //    get { return i_Apartment; }
        //    set { SetPropertyValue("Apartment", ref i_Apartment, value); }
        //}
        [Size(50), DisplayName("Почтовый индекс")]
        public string ZipPostal
        {
            get { return _zipPostal; }
            set
            {
                _zipPostal = value;
                OnChanged("ZipPostal");
            }
        }
        [Size(255), DisplayName("Страна")]
        public string Country
        {
            get { return _country; }
            set
            {
                try
                {
                    _country = value;
                    OnChanged("Country");
                }
                catch { }
            }
        }

        [DisplayName("Субъект Федерации")]
        public Region Region
        {
            get { return _region; }
            set
            {
                try
                {
                    _region = value;
                    OnChanged("Region");
                }
                catch { }
            }
        }
        [DisplayName("Муниципальное образование")]
        public City City
        {
            get { return _city; }
            set
            {
                _city = value;
                OnChanged("City");
            }
        }
        [DisplayName("Район города/Сельсовет")]
        public CityRegion CityRegion
        {
            get { return _cityRegion; }
            set
            {
                try
                {
                    _cityRegion = value;
                    OnChanged("CityRegion");
                }
                catch { }
            }
        }

        [DisplayName("Улица")]
        public Street Street
        {
            get { return _street; }
            set
            {
                _street = value;
                OnChanged("Street");
            }
        }



        [Size(100), DisplayName("Дом")]
        public string House
        {
            get { return _house; }
            set
            {
                _house = value;
                OnChanged("House");
            }
        }

        [Size(100), DisplayName("Строение")]
        public string Building
        {
            get { return _building; }
            set
            {
                _building = value;
                OnChanged("Building");
            }
        }
        [Size(100), DisplayName("Корпус")]
        public string Corpus
        {
            get { return _corpus; }
            set
            {
                _corpus = value;
                OnChanged("Corpus");
            }
        }
        [Size(50), DisplayName("Сооружение")]
        public string Structure
        {
            get { return _structure; }
            set
            {
                _structure = value;
                OnChanged("Structure");
            }
        }
        private dAppartmentType i_ApartmentType;
        [DisplayName("Тип помещения")]
        public dAppartmentType ApartmentType
        {
            get { return i_ApartmentType; }
            set { SetPropertyValue("ApartmentType", ref i_ApartmentType, value); }
        }

        [Size(50), DisplayName("Номер помещения")]
        public string Flat
        {
            get { return _flat; }
            set
            {
                _flat = value;
                OnChanged("Flat");
            }
        }
        [Size(510), DisplayName("Место расположения")]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnChanged("Description");
            }
        }
        private string i_Other;
        [Size(2500), DisplayName("Иное")]
        public string Other
        {
            get { return i_Other; }
            set { SetPropertyValue("Other", ref i_Other, value); }
        }

        [Size(50), DisplayName("Код ОКАТО")]
        public string CodeOKATO
        {
            get { return _codeOKATO; }
            set
            {
                _codeOKATO = value;
                OnChanged("CodeOKATO");
            }
        }
        [Size(32), DisplayName("Код КЛАДР")]
        public string CodeKLADR
        {
            get { return _codeKLADR; }
            set
            {
                _codeKLADR = value;
                OnChanged("CodeKLADR");
            }
        }
        private string i_codeOKTMO;
        [Size(32), DisplayName("Код ОКТМО")]
        public string CodeOKTMO
        {
            get { return i_codeOKTMO; }
            set { SetPropertyValue("CodeOKTMO", ref i_codeOKTMO, value); }
        }
        //[Association]
        //[DisplayName("Записи ФИАС")]
        //public XPCollection<AISOGD.Address.FIAS.House> FIASHouse
        //{
        //    get { return GetCollection<AISOGD.Address.FIAS.House>("FIASHouse"); }
        //}
        //private string i_CancelReason;
        //[Size(500), DisplayName("Причина аннулирования")]
        //public string CancelReason
        //{
        //    get { return i_CancelReason; }
        //    set { SetPropertyValue("CancelReason", ref i_CancelReason, value); }
        //}

        //private AISOGD.Address.FIAS.House _FIASHouse;

        //[DisplayName("Данные ФИАС")]
        //public AISOGD.Address.FIAS.House FIASHouse
        //{
        //    get { return _FIASHouse; }
        //    set
        //    {
        //        _FIASHouse = value;
        //        OnChanged("FIASHouse");
        //    }
        //}


        //[Association("SubjectBase-Address", typeof(SubjectBase))]
        ////[System.ComponentModel.Browsable(false)]
        //[DisplayName("Субъект")]
        //public SubjectBase Subject
        //{
        //    get { return _subject; }
        //    set { try { SetPropertyValue("Subject", ref _subject, value); } catch { } }
        //}
        //[Association("SubjectBase-Address", typeof(SubjectBase))]
        [Association, DisplayName("Субъект")]
        public XPCollection<SubjectBase> SubjectBase
        {
            get { return GetCollection<SubjectBase>("SubjectBase"); }
        }
        

        //[Aggregated, Association, DisplayName("Электронные файлы")]
        //[FileTypeFilter("Файлы", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        //[FileTypeFilter("Изображения", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        //public XPCollection<AttachmentFiles> AttachmentFiles
        //{
        //    get { return GetCollection<AttachmentFiles>("AttachmentFiles"); }
        //}

        [Size(SizeAttribute.Unlimited), System.ComponentModel.Browsable(false)]
        public string FullAddress
        {
            get
            {
                return ObjectFormatter.Format(fullAddressFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }


        [Size(510), DisplayName("Адресное описание")]
        public string ShortAddress
        {
            get
            {
                return ObjectFormatter.Format(shortAddressFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _country = "Российская Федерация";
            //_addressKind = Session.FindObject<AddressKind>(new BinaryOperator("Name", "Почтовый"));
        }
        //[Association, DisplayName("Документы об адресе")]
        //public XPCollection<AdressDoc.AdressDoc> AdressDocs
        //{
        //    get
        //    {
        //        return GetCollection<AISOGD.AdressDoc.AdressDoc>("AdressDocs");
        //    }
        //}

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }
    }
}