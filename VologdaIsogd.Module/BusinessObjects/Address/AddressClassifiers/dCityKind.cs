﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Address
{

    [ModelDefault("Caption", "Тип муниципального образования"), NavigationItem("Справочники адресного реестра")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dCityKind : dBaseClassifierAddress
    {
        public dCityKind(Session session) : base(session) { }
        
        private string _format = "{Name}";

        [Size(32), DisplayName("Формат представления")]
        public string Format
        {
            get { return _format; }
            set { SetPropertyValue("Format", ref _format, value); }
        }
        

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }
    }
}
