﻿using AISOGD.Constr;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.GPZU
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Существующие объкты капитального строительства для ГПЗУ"), System.ComponentModel.DefaultProperty("descript")]
    public class GpzuOKS : BaseObjectXAF
    {
        public GpzuOKS(Session session) : base(session) { }

        private string i_no;
        private string i_character;
        private string i_name;
        private GradPlan i_GPZU;

        private const string shortAddressFormat = "{No} " + " {Name}";

        public string descript
        {
            get
            {
                return ObjectFormatter.Format(shortAddressFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(4),DisplayName("Номер, согласно чертежу")]
        public string No
        {
            get { return i_no; }
            set { SetPropertyValue("No", ref i_no, value); }
        }

        [Size(255),DisplayName("Назначение")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        [Size(64), DisplayName("Литера")]
        public string Сharacter
        {
            get { return i_character; }
            set { SetPropertyValue("Сharacter", ref i_character, value); }
        }

        [Association, DisplayName("ГПЗУ")]
        public GradPlan GPZU
        {
            get { return i_GPZU; }
            set { SetPropertyValue("GPZU", ref i_GPZU, value); }
        }

        private CapitalStructureBase i_capitalStructureBase;
        [DisplayName("Объект строительства")]
        public CapitalStructureBase CapitalStructureBase
        {
            get { return i_capitalStructureBase; }
            set { SetPropertyValue("CapitalStructureBase", ref i_capitalStructureBase, value); }
        }
    }
}
