﻿using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.GPZU
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Технические  условия подключения объекта к сетям инженерно-технического обеспечения"), System.ComponentModel.DefaultProperty("descript")]
    public class TechSpecifications : BaseObjectXAF
    {
        public TechSpecifications(Session session) : base(session) { }


        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                string res = "";
                try { res = EngineeringSupportType.Name; }
                catch { }
                try
                {
                    if (res == "")
                        res = "№ " + TechSpecificationsNo;
                    else
                        res += ", № " + TechSpecificationsNo;
                }
                catch { }
                try
                {
                    if (res == "")
                        res = "от " + TechSpecificationsDate;
                    else
                        res += " от " + TechSpecificationsDate;
                }
                catch { }

                return res;
            }
        }

        private dEngineeringSupportType i_EngineeringSupportType;
        [DisplayName("Тип инженерно-технического обеспечения")]
        public dEngineeringSupportType EngineeringSupportType
        {
            get { return i_EngineeringSupportType; }
            set { SetPropertyValue("EngineeringSupportType", ref i_EngineeringSupportType, value); }
        }

        private string i_TechSpecificationsNo;
        [Size(255),DisplayName("Номер ТУ")]
        public string TechSpecificationsNo
        {
            get { return i_TechSpecificationsNo; }
            set { SetPropertyValue("TechSpecificationsNo", ref i_TechSpecificationsNo, value); }
        }
        private DateTime i_TechSpecificationsDate;
        [DisplayName("Дата ТУ")]
        public DateTime TechSpecificationsDate
        {
            get { return i_TechSpecificationsDate; }
            set { SetPropertyValue("TechSpecificationsDate", ref i_TechSpecificationsDate, value); }
        }


        private TechSpecificationsOrg i_TechSpecificationsOrg;
        [DisplayName("Организация, выдавшая техусловия")]
        public TechSpecificationsOrg TechSpecificationsOrg
        {
            get { return i_TechSpecificationsOrg; }
            set { SetPropertyValue("TechSpecificationsOrg", ref i_TechSpecificationsOrg, value); }
        }

        private GradPlan i_GPZU;
        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public GradPlan GPZU
        {
            get { return i_GPZU; }
            set { SetPropertyValue("GPZU", ref i_GPZU, value); }
        }
    }
}
