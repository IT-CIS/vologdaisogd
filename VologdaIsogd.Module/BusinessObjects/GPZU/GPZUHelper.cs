﻿using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.GPZU
{
    [ModelDefault("Caption", "Настройки для ГПЗУ"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GPZUHelper : BaseObjectXAF
    {
        public GPZUHelper() : base() { }
        public GPZUHelper(Session session) : base(session) { }


        private string i_name;
        [DevExpress.Xpo.DisplayName("Наименование настройки"), VisibleInListView(true), Size(255)]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        private string i_BuildingSpotLayerName;
        [DevExpress.Xpo.DisplayName("Наименование таблицы пятна застройки"), VisibleInListView(true), Size(255)]
        public string BuildingSpotLayerName
        {
            get { return i_BuildingSpotLayerName; }
            set { SetPropertyValue("BuildingSpotLayerName", ref i_BuildingSpotLayerName, value); }
        }
        private double i_BufferDistance;
        [DevExpress.Xpo.DisplayName("Отступ для буферной зоны"), VisibleInListView(true)]
        public double BufferDistance
        {
            get { return i_BufferDistance; }
            set { SetPropertyValue("BufferDistance", ref i_BufferDistance, value); }
        }
        private string i_NumberObjectLayerName;
        [DevExpress.Xpo.DisplayName("Наименование таблицы номера застройки"), VisibleInListView(true), Size(255)]
        public string NumberObjectLayerName
        {
            get { return i_NumberObjectLayerName; }
            set { SetPropertyValue("NumberObjectLayerName", ref i_NumberObjectLayerName, value); }
        }
        [Association, DevExpress.Xpo.DisplayName("Соответствие зон для создания рабочего набора")]
        public XPCollection<ZoneLayerAccording> ZoneLayerAccordings
        {
            get { return GetCollection<ZoneLayerAccording>("ZoneLayerAccordings"); }
        }
       
    }

    public class ZoneLayerAccording : BaseObjectXAF
    {
        public ZoneLayerAccording() : base() { }
        public ZoneLayerAccording(Session session) : base(session) { }


        private GisLayer _spatialLayerIn;

        [DevExpress.Xpo.DisplayName("Таблица основной зоны")]
        //[Indexed(Unique = true)]
        public GisLayer SpatialLayerIn
        {
            get { return _spatialLayerIn; }
            set
            {
                SetPropertyValue("SpatialLayerIn", ref _spatialLayerIn, value);
            }
        }
        private GisLayer _spatialLayerOut;

        [DevExpress.Xpo.DisplayName("Таблица зоны набора ГПЗУ")]
        //[Indexed(Unique = true)]
        public GisLayer SpatialLayerOut
        {
            get { return _spatialLayerOut; }
            set
            {
                SetPropertyValue("SpatialLayerOut", ref _spatialLayerOut, value);
            }
        }

        private GPZUHelper i_GPZUHelper;
        [Association, Browsable(false)]
        public GPZUHelper GPZUHelper
        {
            get { return i_GPZUHelper; }
            set { SetPropertyValue("GPZUHelper", ref i_GPZUHelper, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}