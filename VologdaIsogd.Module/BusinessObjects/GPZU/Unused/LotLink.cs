﻿using AISOGD.Land;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Module.BusinessObjects.GPZU
{
    
    //[NavigationItem("ГПЗУ")]
    [Custom("Caption", "Связь земельного участка и ГПЗУ"), System.ComponentModel.DefaultProperty("Name")]
    public class LotLink : BaseObject
    {
        public LotLink(Session session) : base(session) { }

        [DisplayName("Номер")]
        public string Nom { get; set; }

        [DisplayName("Максимальный процент застройки")]
        public string MaxProcentBuilding { get; set; }


        [DisplayName("Иные показатели")]
        public string AnotherItems { get; set; }

        private Lot _Lot;
        //[Association, DisplayName("Земельный участок")]
        //public Lot lot
        //{
        //    get { return _Lot; }
        //    set { SetPropertyValue("lot", ref _Lot, value); }
        //}

        //[Association, DisplayName("Места допустимого размещения")]
        //public XPCollection<UnderConstructionPlace> underConstructionPlace
        //{
        //    get { return GetCollection<UnderConstructionPlace>("underConstructionPlace"); }
        //}
    }
}
