using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System.Drawing;
using WinAisogdIngeo.Module.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.GPZU
{

    /// <summary>
    /// ����� ��� ������������ �������� ����
    /// </summary>
    [ModelDefault("Caption", "���������� ���������"), System.ComponentModel.DefaultProperty("Name")]
    public class GpzuImage : BaseObjectXAF
    {
        public GpzuImage(Session session) : base(session) { }

        [Size(255), DisplayName("��������")]
        public string Name { get; set; }
    
    private Image image;
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("����� ����")]
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Image
        {
            get { return image; }
            set { SetPropertyValue("Image", ref image, value);}
        }

        [Association, DisplayName("����")]
        [System.ComponentModel.Browsable(false)]
        public GradPlan GradPlan { get; set; }

        [Association, DisplayName("�������")]
        public XPCollection<GpzuImageLegend> GpzuImageLegend
        {
            get
            {
                return GetCollection<GpzuImageLegend>("GpzuImageLegend");
            }
        }
    }
}