﻿using AISOGD.Subject;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.GPZU
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Организация, выдающая технические условия"), System.ComponentModel.DefaultProperty("Name")]
    public class TechSpecificationsOrg : BaseObjectXAF
    {
        public TechSpecificationsOrg(Session session) : base(session) { }

        private Org i_TechOrg;
        [DisplayName("Организация")]
        public Org TechOrg
        {
            get { return i_TechOrg; }
            set { SetPropertyValue("TechOrg", ref i_TechOrg, value); }
        }

        private string i_name;
        [Size(255),DisplayName("Наименование организации")]
        public string Name
        {
            get {
                try
                {
                    if (String.IsNullOrEmpty(i_name))
                        i_name = GetTechOrgInfo();
                }
                catch { }
                return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }


        private string i_LicenseNo;
        private DateTime i_LicenseDate;
        private string i_LicenseIssued;
        private string i_LicenseSeries;


        private string i_TechOrgAddress;
        [Size(255), DisplayName("Адрес организации")]
        public string TechOrgAddress
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_TechOrgAddress))
                        i_TechOrgAddress = GetTechOrgAddress();
                }
                catch { }
                return i_TechOrgAddress;
            }
            set { SetPropertyValue("TechOrgAddress", ref i_TechOrgAddress, value); }
        }

        private string i_TechOrgPhones;
        [Size(255), DisplayName("Телефоны")]
        public string TechOrgPhones
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(i_TechOrgPhones))
                        i_TechOrgPhones = GetTechOrgPhones();
                }
                catch { }
                return i_TechOrgPhones;
            }
            set { SetPropertyValue("TechOrgPhones", ref i_TechOrgPhones, value); }
        }


        

        private string _note;
        [Size(SizeAttribute.Unlimited), DisplayName("Примечание")]
        public string note
        {
            get { return _note; }
            set { try { _note = value; } catch { } }
        }



        public string GetTechOrgInfo()
        {
            string result = "";
            if (i_TechOrg != null)
            {
                result = i_TechOrg.FullName;
            }
            return result;
        }
        public string GetTechOrgAddress()
        {
            string result = "";
            if (i_TechOrg != null)
            {
                result = i_TechOrg.address;
            }
            return result;
        }
        public string GetTechOrgPhones()
        {
            string result = "";
            if (i_TechOrg != null)
            {
                result = i_TechOrg.phones;
            }
            return result;
        }
        
    }
}
