﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.GPZU
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Тип инженерно-технического обеспечения"), System.ComponentModel.DefaultProperty("Name")]
    public class EngineeringSupportType : BaseObjectXAF
    {
        public EngineeringSupportType(Session session) : base(session) { }

        private string i_name;
        [Size(255),DisplayName("Наименование типа")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        
    }
}
