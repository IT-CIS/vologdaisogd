using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System.Drawing;
using WinAisogdIngeo.Module.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.GPZU
{

    /// <summary>
    /// ����� ��� ������������ �������� ����
    /// </summary>
    [ModelDefault("Caption", "���������� ���������"), System.ComponentModel.DefaultProperty("Name")]
    public class GpzuImageLegend : BaseObjectXAF
    {
        public GpzuImageLegend(Session session) : base(session) { }

        [Size(255), DisplayName("��������")]
        public string Name { get; set; }

        private System.Drawing.Image image;
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("�������")]
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Image
        {
            get { return image; }
            set { SetPropertyValue("Image", ref image, value); }
        }

        [Association, DisplayName("GpzuImage")]
        [System.ComponentModel.Browsable(false)]
        public GpzuImage GpzuImage { get; set; }


        public override void AfterConstruction()
        {

        }
    }
}