﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.GPZU
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Место допустимого размещения"), System.ComponentModel.DefaultProperty("descript")]
    public class UnderConstructionPlace : BaseObjectXAF
    {
        public UnderConstructionPlace(Session session) : base(session) { }

        private string i_no;
        private string i_name;
        private GradPlan i_GPZU;

        private const string format = "№ на участке: {No} " + ", назначение: {Name}";

        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return ObjectFormatter.Format(format, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(4),DisplayName("Номер, согласно чертежу")]
        public string No
        {
            get { return i_no; }
            set { SetPropertyValue("No", ref i_no, value); }
        }

        [Size(255),DisplayName("Назначение")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        
        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public GradPlan GPZU
        {
            get { return i_GPZU; }
            set { SetPropertyValue("GPZU", ref i_GPZU, value); }
        }
       
    }
}
