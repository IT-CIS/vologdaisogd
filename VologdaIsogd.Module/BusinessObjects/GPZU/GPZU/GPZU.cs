﻿using AISOGD.Address;
using AISOGD.DocFlow;
using AISOGD.Land;
using AISOGD.Monuments;
using AISOGD.OrgStructure;
using AISOGD.Reglament;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.GPZU
{

    [NavigationItem("Подготовка документов")]
    [ModelDefault("Caption", "Градостроительный план земельного участка"), System.ComponentModel.DefaultProperty("descript")]
    public class GradPlan : GeneralDocBase
    {
        public GradPlan(Session session) : base(session) { }


        private string i_LotCadNo;
        private string i_LotAddress;
        private string i_ObjectAddress;
        private string i_MinSize;
        private string i_MaxSize;
        private string i_Length;
        private string i_Width;
        private string i_Roadway;
        private string i_LotSquare;
        private string i_SzzText;
        private string i_ObjectSquare;
        private string i_MaxFloorCount;
        private string i_MaxHeightCount;
        private string i_MaxProcentBuilding;
        private string i_AnotherItems;
        private string i_LotDivisionInfo;



        [Size(255), DisplayName("Описание")]
        public string descript
        {
            get
            {
                return String.Format("Градостроительный план № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        private string i_GpzuReason;
        [Size(255), DisplayName("ГПЗУ готовится на основании обращения")]
        public string GpzuReason
        {
            get
            {
                if (i_GpzuReason == null || i_GpzuReason == "")
                {
                    try
                    {
                        string res = "";
                        if (InLetter.Count > 0)
                        {
                            //res = "обращения ";
                            try
                            {
                            res += InLetter[0].Subject;
                            }
                            catch { }
                            try
                            {
                                res += " от " + InLetter[0].RegDate.ToShortDateString();
                            }
                            catch { }
                        }
                        i_GpzuReason = res;
                    }
                    catch { }
                    
                }
                return i_GpzuReason;
            }
            set { SetPropertyValue("GpzuReason", ref i_GpzuReason, value); }
        }
        private string SetGPZUReason()
        {
            string res = "обращения ";

            if (InLetter.Count > 0)
            {
                //try
                //{
                    res += InLetter[0].Subject;
                //}
                //catch { }
                //try
                //{
                    res += " от " + InLetter[0].RegDate;
                //}
                //catch { }
            }
            return res;
        }
        //private string i_letterNo;
        //[Size(32), DisplayName("Номер заявки")]
        //public string LetterNo
        //{
        //    get
        //    {
        //        if (i_letterNo == null || i_letterNo == "")
        //        {
        //            string res = String.Empty;
        //            //try
        //            //{
        //                if (InLetter.Count > 0)
        //                    res = InLetter[0].RegNo;
        //            //}
        //            //catch { }
        //            i_letterNo = res;
        //        }
        //        return i_letterNo;
        //    }
        //    set { SetPropertyValue("LetterNo", ref i_letterNo, value); }
        //}

        //private DateTime i_letterDate;
        //[DisplayName("Дата заявки")]
        //public DateTime LetterDate
        //{
        //    get
        //    {
        //        if (i_letterDate == DateTime.MinValue)
        //        {
        //            i_letterDate = DateTime.MinValue;
        //            if (InLetter.Count > 0)
        //                i_letterDate = InLetter[0].RegDate;
        //        }
        //        return i_letterDate;
        //    }
        //    set { SetPropertyValue("LetterDate", ref i_letterDate, value); }
        //}
        private string i_Subject;
        [Size(255), DisplayName("Заказчик(описание)")]
        public string Subject
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(i_Subject))
                    return i_Subject;
                else
                {
                    try
                    {
                        if (InLetter.Count > 0)
                            i_Subject = InLetter[0].Subject;
                    }
                    catch { }
                    //i_Subject = res;
                    return i_Subject;
                }
            }
            set { SetPropertyValue("Subject", ref i_Subject, value); }
        }
        [Size(255), DisplayName("Кадастровый(е) номер(а) земельного(ых) участка(ов)")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get
            {
                if (i_LotCadNo == null || i_LotCadNo == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            foreach (Lot lt in Lots)
                            {
                                if (lt.CadastralNumber != null || lt.CadastralNumber != String.Empty)
                                    if (res == String.Empty)
                                        res = lt.CadastralNumber;
                                    else
                                        res = res + "," + lt.CadastralNumber;
                            }
                    }
                    catch { }
                    i_LotCadNo = res;
                }
                return i_LotCadNo;
            }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }

        private CityRegion _CityRegion;
        [DisplayName("Район города")]
        [ImmediatePostData]
        public CityRegion CityRegion
        {
            get { return _CityRegion; }
            set
            {
                SetPropertyValue("CityRegion", ref _CityRegion, value);

                //try
                //{
                //    i_LotAddress = "Астраханская область, г.Астрахань1, " + _CityRegion.FullName;
                //}
                //catch { }
                try
                {
                    i_LotAddress = GetLotAddress();
                }
                catch { }
                OnChanged("CityRegion");
            }
        }
        private Street i_Street;
        [DisplayName("Улица")]
        [ImmediatePostData]
        public Street Street
        {
            get { return i_Street; }
            set
            {
                SetPropertyValue("Street", ref i_Street, value);
                try
                {
                    i_LotAddress = GetLotAddress();
                }
                catch { }
                OnChanged("Street");
            }
        }
        private string GetLotAddress()
        {
            string res = "";
            string txt = "Астраханская область, г.Астрахань";
            string reg = "";
            string str = "";
            try { reg = ", " + _CityRegion.FullName; }
            catch { }
            try
            {
                str += ", " + Street.FullName;
                //XtraMessageBox.Show(String.Format("Street.Name - {0}", Street.Name));
            }
            catch { }
            res = txt + reg + str;
            return res;
        }

        [Size(1000),DisplayName("Местонахождение земельного участка")]
        public string LotAddress
        {
            get
            {
                //if (i_LotAddress == null || i_LotAddress == String.Empty)
                //{
                //    string res = String.Empty;
                //    try
                //    {
                //        if (Lots.Count > 0)
                //            foreach (Lot lt in Lots)
                //            {
                //                if (lt.Adress != null || lt.Adress != String.Empty)
                //                    if (res == String.Empty)
                //                        res = lt.Adress;
                //                    else
                //                        res = res + ";" + lt.Adress;
                //            }
                //    }
                //    catch { }
                //    try
                //    {
                //        if (res == String.Empty)
                //        {
                //            res = Municipality.Name + ", " + CityRegion.Name;
                //        }
                //    }
                //    catch { }
                //    i_LotAddress = res;
                //}
                return i_LotAddress;
            }
            set { SetPropertyValue("LotAddress", ref i_LotAddress, value); }
        }
        private string i_LotAddressInfo;
        [Size(1000), DisplayName("Описание местоположения границ земельного участка")]
        public string LotAddressInfo
        {
            get
            {
                return i_LotAddressInfo;
            }
            set { SetPropertyValue("LotAddressInfo", ref i_LotAddressInfo, value); }
        }
        
        private string i_ObjectName;
        [Size(255), DisplayName("Описание проектируемого объекта")]
        public string ObjectName
        {
            get { return i_ObjectName; }
            set { SetPropertyValue("ObjectName", ref i_ObjectName, value); }
        }
        [Size(1000), DisplayName("Описание местоположения проектируемого объекта")]
        public string ObjectAddress
        {
            get { return i_ObjectAddress; }
            set { SetPropertyValue("ObjectAddress", ref i_ObjectAddress, value); }
        }

        private Municipality i_Municipality;
        [DisplayName("Муниципальное образование")]
        public Municipality Municipality
        {
            get {
                try { i_Municipality = GetMunicipality(); }
                catch { }
                return i_Municipality; }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        

        [Size(255), DisplayName("Длина (м)")]
        public string Length
        {
            get { return i_Length; }
            set { SetPropertyValue("Length", ref i_Length, value); }
        }

        [Size(255), DisplayName("Ширина (м)")]
        public string Width
        {
            get { return i_Width; }
            set { SetPropertyValue("Width", ref i_Width, value); }
        }

        [Size(255), DisplayName("Зоны действия публичных сервитутов")]
        public string Roadway
        {
            get { return i_Roadway; }
            set { SetPropertyValue("Roadway", ref i_Roadway, value); }
        }

        [Size(255), DisplayName("Зоны с особями условиями использования")]
        public string SzzText
        {
            get { return i_SzzText; }
            set { SetPropertyValue("SzzText", ref i_SzzText, value); }
        }

        [Size(255), DisplayName("Площадь земельного участка(кв.м.)")]
        public string LotSquare
        {
            get
            {
                if (i_LotSquare == null || i_LotSquare == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Lots.Count > 0)
                            if (Lots[0].Area != 0)
                            {
                                if (Lots[0].Unit.Name.ToLower().Contains("га"))
                                    res = (Lots[0].Area*10000).ToString();
                                if (Lots[0].Unit.Name.ToLower().Contains("кв.м") || Lots[0].Unit.Name.ToLower().Contains("м2"))
                                    res = Lots[0].Area.ToString();
                            }
                    }
                    catch { }
                    i_LotSquare = res;
                }
                return i_LotSquare;
            }
            set { SetPropertyValue("LotSquare", ref i_LotSquare, value); }
        }

        [Size(255), DisplayName("Минимальный размер участка (кв.м)")]
        public string MinSize
        {
            get
            {
                try
                {
                    if (i_MinSize == null || i_MinSize == "")
                        if (TerrZoneKindLink != null)
                        {
                            i_MinSize = TerrZoneKindLink.MinSizeLiving;
                        }
                }
                catch { }
                return i_MinSize;
            }
            set { SetPropertyValue("MinSize", ref i_MinSize, value); }
        }

        [Size(255), DisplayName("Максимальный размер участка (кв.м)")]
        public string MaxSize
        {
            get
            {
                try
                {
                    if (i_MaxSize == null || i_MaxSize == "")
                        if (TerrZoneKindLink != null)
                        {
                            i_MaxSize = TerrZoneKindLink.MaxSizeLiving;
                        }
                }
                catch { }
                return i_MaxSize;
            }
            set { SetPropertyValue("MaxSize", ref i_MaxSize, value); }
        }

        [Size(255), DisplayName("Площадь объекта капитального строительства (га)")]
        public string ObjectSquare
        {
            get { return i_ObjectSquare; }
            set { SetPropertyValue("ObjectSquare", ref i_ObjectSquare, value); }
        }

        [Size(255), DisplayName("Предельное количество этажей")]
        public string MaxFloorCount
        {
            get
            {
                try
                {
                    if (i_MaxFloorCount == null || i_MaxFloorCount == "")
                        if (TerrZoneKindLink != null)
                        {
                            i_MaxFloorCount = TerrZoneKindLink.MaxFloorCountMax;
                        }
                }
                catch { }
                return i_MaxFloorCount;
            }
            set { SetPropertyValue("MaxFloorCount", ref i_MaxFloorCount, value); }
        }

        [Size(255), DisplayName("Предельная высота (м)")]
        public string MaxHeightCount
        {
            get
            {

                return i_MaxHeightCount;
            }
            set { SetPropertyValue("MaxHeightCount", ref i_MaxHeightCount, value); }
        }

        [Size(255), DisplayName("Максимальный процент застройки")]
        public string MaxProcentBuilding
        {
            get
            {
                try
                {
                    if (i_MaxProcentBuilding == null || i_MaxProcentBuilding == "")
                        if (TerrZoneKindLink != null)
                        {
                            i_MaxProcentBuilding = TerrZoneKindLink.MaxProcentBuilding;
                        }
                }
                catch { }
                return i_MaxProcentBuilding;
            }
            set { SetPropertyValue("MaxProcentBuilding", ref i_MaxProcentBuilding, value); }
        }
        // ссылка на территориальную зону, которая пойдет в формирование ГПЗУ
        // проставляется автоматом, если в XPCollection<TerrZoneKind> только одна зона
        // если там несколько типов зон, то возможность выбора только из них
        // если там нет типов зон, то выбор из всех типов зон
        private TerrZoneKind i_TerrZoneKindLink;
        [DisplayName("Территориальная зона")]
        [DataSourceProperty("AvailableTerrZones")]
        [ImmediatePostData]
        public TerrZoneKind TerrZoneKindLink
        {
            get { return i_TerrZoneKindLink; }
            set { try { SetPropertyValue("TerrZoneKindLink", ref i_TerrZoneKindLink, value); } catch { } }
        }

        //----------------------------------------------------
        // Получаем возможные для выбора территориальные зоны
        private XPCollection<TerrZoneKind> availableTerrZones;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<TerrZoneKind> AvailableTerrZones
        {
            get
            {
                if (availableTerrZones == null)
                {
                    // Retrieve all Terminals objects 
                    availableTerrZones = new XPCollection<TerrZoneKind>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableTerrZones();
                }
                return availableTerrZones;
            }
        }
        private void RefreshAvailableTerrZones()
        {
            if (availableTerrZones == null)
                return;
            if (TerrZoneKind.Count >= 1)
            {
                availableTerrZones = TerrZoneKind;
                if (TerrZoneKind.Count == 1)
                    TerrZoneKindLink = TerrZoneKind.FirstOrDefault();//[0];
            }
            else
            {
                availableTerrZones = new XPCollection<TerrZoneKind>(Session);
            }
            TerrZoneKindLink = null;
        }

        [Size(1000), DisplayName("Информация о разделении земельного участка")]
        public string LotDivisionInfo
        {
            get { return i_LotDivisionInfo; }
            set { SetPropertyValue("LotDivisionInfo", ref i_LotDivisionInfo, value); }
        }
        private string i_ZonesOKSStateNeedsInfo;
        [Size(4000), DisplayName("Информация о зонах размещения ОКС для гос.нужд")]
        public string ZonesOKSStateNeedsInfo
        {
            get { return i_ZonesOKSStateNeedsInfo; }
            set { SetPropertyValue("ZonesOKSStateNeedsInfo", ref i_ZonesOKSStateNeedsInfo, value); }
        }
        private string i_AnotherInfo;
        [Size(4000), DisplayName("Иная информация")]
        public string AnotherInfo
        {
            get { return i_AnotherInfo; }
            set { SetPropertyValue("AnotherInfo", ref i_AnotherInfo, value); }
        }
        private string i_PlanPrepared;
        [Size(4000), DisplayName("План подготовлен")]
        public string PlanPrepaped
        {
            get { return i_PlanPrepared; }
            set { SetPropertyValue("PlanPrepaped", ref i_PlanPrepared, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("Координатное описание")]
        public string Coords { get; set; }

        [Size(1000), DisplayName("Примечания")]
        public string Notes { get; set; }

        private GPZUSubmit i_GPZUSubmit;
        [DisplayName("Приказ об утверждении ГПЗУ")]
        [ImmediatePostData]
        public GPZUSubmit GPZUSubmit
        {
            get { return i_GPZUSubmit; }
            set { try { SetPropertyValue("GPZUSubmit", ref i_GPZUSubmit, value); } catch { } }
        }

        [Association, DisplayName("Места допустимого размещения")]
        public XPCollection<UnderConstructionPlace> underConstructionPlace
        {
            get { return GetCollection<UnderConstructionPlace>("underConstructionPlace"); }
        }
        [Association, DisplayName("Существующие ОКС на участке")]
        public XPCollection<GpzuOKS> GpzuOKS
        {
            get { return GetCollection<GpzuOKS>("GpzuOKS"); }
        }
        [Association, DisplayName("Территориальные зоны")]
        [ImmediatePostData]
        public XPCollection<TerrZoneKind> TerrZoneKind
        {
            get { return GetCollection<TerrZoneKind>("TerrZoneKind"); }
        }
        [Association, DisplayName("Зоны с особыми условиями использования")]
        public XPCollection<SpecialZoneKind> SpecialZoneKind
        {
            get { return GetCollection<SpecialZoneKind>("SpecialZoneKind"); }
        }
        [Association, DisplayName("Зоны объектов культурного наследия")]
        public XPCollection<MonumentZoneKind> MonumentZoneKind
        {
            get { return GetCollection<MonumentZoneKind>("MonumentZoneKind"); }
        }
        [Association, DisplayName("Объекты культурного наследия")]
        public XPCollection<GpzuMonument> Monuments
        {
            get { return GetCollection<GpzuMonument>("Monuments"); }
        }
        //[Association, DisplayName("Постановление об утверждении")]
        //public XPCollection<GPZUSubmit> GPZUSubmit
        //{
        //    get { return GetCollection<GPZUSubmit>("GPZUSubmit"); }
        //}
        [Association, DisplayName("Техусловия подключения объекта к инженерным сетям")]
        public XPCollection<TechSpecifications> TechSpecifications
        {
            get { return GetCollection<TechSpecifications>("TechSpecifications"); }
        }

        [Association, DisplayName("Графика по градплану")]
        public XPCollection<GpzuImage> GpzuImage
        {
            get
            {
                return GetCollection<GpzuImage>("GpzuImage");
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Градостроительный план земельного участка"));
            if (i_LotAddressInfo == null || i_LotAddressInfo == "")
                i_LotAddressInfo = "по координатам в соответствии с данными кадастровой выписки о земельном участке от ______ №______";
            if (i_ObjectAddress == null || i_ObjectAddress == "")
                i_ObjectAddress = "см. чертеж градостроительного плана земельного участка и линий градостроительного регулирования";
            if (i_PlanPrepared == null || i_PlanPrepared == "")
                i_PlanPrepared = "отделом выдачи градостроительных планов управления по строительству, " + 
                    "архитектуре и градостроительству администрации МО «Город Астрахань», начальник отдела – Магомедов Шамиль Абдурахманович";
            Session.CommitTransaction();
        }

        protected override void OnSaved()
        {
            if (!IsDeleted)
            {
                try
                {
                    if (this.DocNo == String.Empty || this.DocNo == null)
                    {
                        double InitialRegNo = 0;
                        try{
                            InitialRegNo = this.DocKind.InitialRegNo;
                        }
                        catch{}
                        if (DateTime.Now.Year.ToString() != "2016")
                            InitialRegNo = 0;
                        this.DocNo = Convert.ToString(InitialRegNo + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + DateTime.Now.Year.ToString(),
                            string.Empty));
                        this.DocNo = "RU30301000-" + this.DocNo;
                    }
                    //if (this.DocNo.EndsWith("-"))
                    //{
                    //    if (GPZUSubmit != null)
                    //        if (GPZUSubmit.DocNo != null)
                    //            this.DocNo = this.DocNo + GPZUSubmit.DocNo;
                    //}
                }
                catch { }
                this.DocDate = DateTime.Now.Date;
                try { this.DocNo = GPZUSubmit.GradPlanNo; }
                catch { }

            }
            base.OnSaved();
        }

        protected override void OnSaving()
        {
            try { this.DocNo = GPZUSubmit.GradPlanNo; }
            catch { }
            //if (!IsDeleted)
            //{
            //    //try
            //    //{
            //    if (this.LotCadNo == null || this.LotCadNo == String.Empty)
            //    {
            //        // сплитим по кадастровыми номерам участков
            //        char[] sep = { ',', ';' };
            //        string[] cadNoms = this.LotCadNo.Split(sep);

            //        foreach (string cadNo in cadNoms)
            //        {
            //            cadNo.Trim();
            //            // ищем ЗУс указанным кадастровым номером
            //            // если не находим - создаем и агрегируем на документ
            //            // если находим - проверям, есть ли он у документа, если нет - агрегируем
            //            Lot i_lot = Session.FindObject<Lot>(new BinaryOperator("CadNo", cadNo));
            //            if (i_lot == null)
            //            {
            //                i_lot = new Lot(Session);
            //                i_lot.CadNo = cadNo;
            //            }
            //            this.Lots.Add(i_lot);
            //        }
            //    }
            //    //}
            //    //catch { }
            //}
            //base.OnSaving();
        }
    }
}
