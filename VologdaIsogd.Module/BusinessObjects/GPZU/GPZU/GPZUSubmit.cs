﻿using AISOGD.Address;
using AISOGD.DocFlow;
using AISOGD.Land;
using AISOGD.Monuments;
using AISOGD.OrgStructure;
using AISOGD.Reglament;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.GPZU
{

    [NavigationItem("Подготовка документов")]
    [ModelDefault("Caption", "Приказ об утверждении градплана"), System.ComponentModel.DefaultProperty("descript")]
    public class GPZUSubmit : GeneralDocBase
    {
        public GPZUSubmit(Session session) : base(session) { }


        private string i_LotCadNo;
        private string i_LotAddress;
        private DateTime _letterDate;
        private string _letterNo;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Приказ об утверждении градплана № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        private string i_GpzuReason;
        [Size(255), DisplayName("ГПЗУ готовится по обращению")]
        public string GpzuReason
        {
            get
            {
                return i_GpzuReason;
            }
            set { SetPropertyValue("GpzuReason", ref i_GpzuReason, value); }
        }
        //[Size(32), DisplayName("Номер заявки")]
        //public string LetterNo
        //{
        //    get
        //    {
        //        string res = String.Empty;
        //        try
        //        {
        //           res = GradPlan.LetterNo;
        //        }
        //        catch { }
        //        _letterNo = res;
        //        return _letterNo;
        //    }
        //    set { SetPropertyValue("LetterNo", ref _letterNo, value); }
        //}
        //[DisplayName("Дата заявки")]
        //public DateTime LetterDate
        //{
        //    get
        //    {
        //        DateTime res = DateTime.MinValue;
        //        try
        //        {
        //            res = GradPlan.LetterDate;
        //        }
        //        catch { }
        //        _letterDate = res;
        //        return _letterDate;
        //    }
        //    set { SetPropertyValue("LetterDate", ref _letterDate, value); }
        //}
        //private string i_LetterSubject;
        //[Size(255), DisplayName("Описание заявителя")]
        //public string LetterSubject
        //{
        //    get
        //    {
        //        if (i_LetterSubject == null || i_LetterSubject == String.Empty)
        //        {
        //            string res = String.Empty;
        //            try
        //            {
        //                res = GradPlan.Subject;
        //            }
        //            catch { }
        //            i_LetterSubject = res;
        //        }
        //        return i_LetterSubject;
        //    }
        //    set { SetPropertyValue("LetterSubject", ref i_LetterSubject, value); }
        //}
        private string i_GradPlanNo;
        [Size(32), DisplayName("Присвоить номер градплану")]
        public string GradPlanNo
        {
            get
            {
                return i_GradPlanNo = SetGradPlanNo();
            }
            set { SetPropertyValue("GradPlanNo", ref i_GradPlanNo, value); }
        }
        private string SetGradPlanNo()
        {
            string res = "";
            try
            {
                if (GradPlan.DocNo.EndsWith("-") == true)
                    res = GradPlan.DocNo + this.DocNo;
            }
            catch { }
            if(res == "")
            {
                try { res = GradPlan.DocNo; }
            catch { }
            }
            return res;
        }

        [Size(255), DisplayName("Кадастровый номер земельного участка")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get
            {
                if (i_LotCadNo == null || i_LotCadNo == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        res = GradPlan.LotCadNo;
                    }
                    catch { }
                    i_LotCadNo = res;
                }
                return i_LotCadNo;
            }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }
        [Size(1000), DisplayName("Местонахождение земельного участка")]
        public string LotAddress
        {
            get
            {
                if (i_LotAddress == null || i_LotAddress == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        res = "г.Астрахань, " + GradPlan.LotAddress;
                    }
                    catch { }
                    ////try
                    ////{
                    ////    if (res == String.Empty)
                    ////    {
                    ////        res = Municipality.Name + ", " + CityRegion.Name;
                    ////    }
                    ////}
                    ////catch { }
                    i_LotAddress = res;
                }
                return i_LotAddress;
            }
            set { SetPropertyValue("LotAddress", ref i_LotAddress, value); }
        }

        private Municipality i_Municipality;
        [DisplayName("Муниципальное образование")]
        [ImmediatePostData]
        public Municipality Municipality
        {
            get {
                try { i_Municipality = GetMunicipality(); }
                catch { }
                return i_Municipality; }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        private CityRegion _CityRegion;
        [DisplayName("Район города")]
        [ImmediatePostData]
        public CityRegion CityRegion
        {
            get {
                try
                {
                    _CityRegion = GradPlan.CityRegion;
                }
                catch { }
                return _CityRegion; }
            set { try { SetPropertyValue("CityRegion", ref _CityRegion, value); } catch { } }
        }

        private GradPlan i_GradPlan;
        [DisplayName("Градостроительный план земельного участка")]
        [ImmediatePostData]
        public GradPlan GradPlan
        {
            get { return i_GradPlan; }
            set { try { SetPropertyValue("GradPlan", ref i_GradPlan, value); } catch { } }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Приказ об утверждении градостроительного плана"));
            try { this.DocNo = GradPlan.DocNo.Split('-')[1].Trim(); }
            catch { }
            Session.CommitTransaction();
        }
        
        protected override void OnSaved()
        {
            base.OnSaved();
            try
            {
                GradPlan.DocNo = this.GradPlanNo;
                GradPlan.Save();
            }
            catch { }
           
        }
    }
}
