﻿using AISOGD.Constr;
using AISOGD.Monuments;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.GPZU
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Существующие объкты культурного наследия в границах ГПЗУ"), System.ComponentModel.DefaultProperty("descript")]
    public class GpzuMonument : BaseObjectXAF
    {
        public GpzuMonument(Session session) : base(session) { }

        private string i_no;
        private string i_character;
        private string i_name;
        private GradPlan i_GPZU;
        private string i_regNo;
        private string i_RegOrg;
        private DateTime i_regDate;

        private const string shortFormat = "{No} " + " {Name}";

        public string descript
        {
            get
            {
                return ObjectFormatter.Format(shortFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(4),DisplayName("Номер, согласно чертежу")]
        public string No
        {
            get { return i_no; }
            set { SetPropertyValue("No", ref i_no, value); }
        }

        [Size(255), DisplayName("Назначение")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        [Size(255), DisplayName("Регистрационный номер в реестре")]
        public string RegNo
        {
            get { return i_regNo; }
            set { SetPropertyValue("RegNo", ref i_regNo, value); }
        }
        [DisplayName("Дата регистрации")]
        public DateTime RegDate
        {
            get { return i_regDate; }
            set { SetPropertyValue("RegDate", ref i_regDate, value); }
        }
        [DisplayName("Наименование органа, принявшего решение о включении объекта в реестр")]
        public string RegOrg
        {
            get { return i_RegOrg; }
            set { try { SetPropertyValue("RegOrg", ref i_RegOrg, value); } catch { } }
        }

        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public GradPlan GPZU
        {
            get { return i_GPZU; }
            set { SetPropertyValue("GPZU", ref i_GPZU, value); }
        }
        private Monument i_Monument;
        [DisplayName("Объект культурного наследия")]
        public Monument Monument
        {
            get { return i_Monument; }
            set { SetPropertyValue("Monument", ref i_Monument, value); }
        }
    }
}
