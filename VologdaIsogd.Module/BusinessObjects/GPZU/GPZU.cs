﻿using AISOGD.Address;
using AISOGD.Coordinates;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.Monuments;
using AISOGD.OrgStructure;
using AISOGD.Reglament;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.GPZU
{

    [NavigationItem("Подготовка документов")]
    [ModelDefault("Caption", "Градостроительный план земельного участка"), System.ComponentModel.DefaultProperty("descript")]
    public class GradPlan : GeneralDocBase
    {
        public GradPlan(Session session) : base(session) { }


        private string i_LotCadNo;
        private string i_LotAddress;
        private string i_ObjectAddress;
        private string i_MinSize;
        private string i_MaxSize;
        private string i_Length;
        private string i_Width;
        private string i_Roadway;
        private string i_LotSquare;
        private string i_SzzText;
        
        private string i_AnotherItems;
        private string i_LotDivisionInfo;



        [Size(255), DisplayName("Описание")]
        public string descript
        {
            get
            {
                return String.Format("Градостроительный план № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        //private string i_GpzuReason;
        //[Size(255), DisplayName("ГПЗУ готовится на основании обращения")]
        //public string GpzuReason
        //{
        //    get
        //    {
        //        if (i_GpzuReason == null || i_GpzuReason == "")
        //        {
        //            try
        //            {
        //                string res = "";
        //                if (InLetter.Count > 0)
        //                {
        //                    //res = "обращения ";
        //                    try
        //                    {
        //                    res += InLetter[0].Subject;
        //                    }
        //                    catch { }
        //                    try
        //                    {
        //                        res += " от " + InLetter[0].RegDate.ToShortDateString();
        //                    }
        //                    catch { }
        //                }
        //                i_GpzuReason = res;
        //            }
        //            catch { }
                    
        //        }
        //        return i_GpzuReason;
        //    }
        //    set { SetPropertyValue("GpzuReason", ref i_GpzuReason, value); }
        //}
        private string SetGPZUReason()
        {
            string res = "обращения ";

            if (InLetter.Count > 0)
            {
                //try
                //{
                    res += InLetter[0].Subject;
                //}
                //catch { }
                //try
                //{
                    res += " от " + InLetter[0].RegDate;
                //}
                //catch { }
            }
            return res;
        }
        private string i_letterNo;
        [Size(32), DisplayName("Номер заявки")]
        public string LetterNo
        {
            get
            {
                try
                {
                    if (i_letterNo == null || i_letterNo == "")
                    {
                        string res = String.Empty;
                        try
                        {
                            if (InLetter.Count > 0)
                                res = InLetter[0].RegNo;
                        }
                        catch { }
                        i_letterNo = res;
                    }
                }
                catch { }
                return i_letterNo;
            }
            set { SetPropertyValue("LetterNo", ref i_letterNo, value); }
        }

        private DateTime i_letterDate;
        [DisplayName("Дата заявки")]
        public DateTime LetterDate
        {
            get
            {
                try
                {
                    if (i_letterDate == DateTime.MinValue)
                    {
                        i_letterDate = DateTime.MinValue;
                        if (InLetter.Count > 0)
                            i_letterDate = InLetter[0].RegDate;
                    }
                }
                catch { }
                return i_letterDate;
            }
            set { SetPropertyValue("LetterDate", ref i_letterDate, value); }
        }
        private string i_GPZUSubmitDocNo;
        [Size(64), DisplayName("Номер постановления об утверждении")]
        public string GPZUSubmitDocNo
        {
            get
            {
                return i_GPZUSubmitDocNo;
            }
            set { SetPropertyValue("GPZUSubmitDocNo", ref i_GPZUSubmitDocNo, value); }
        }
        private DateTime i_GPZUSubmitDocDate;
        [DisplayName("Дата постановления об утверждении")]
        public DateTime GPZUSubmitDocDate
        {
            get
            {
                return i_GPZUSubmitDocDate;
            }
            set { SetPropertyValue("GPZUSubmitDocDate", ref i_GPZUSubmitDocDate, value); }
        }
        private string i_Subject;
        [Size(500), DisplayName("Застройщик (род.падеж)")]
        public string Subject
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(i_Subject))
                    return i_Subject;
                else
                {
                    //try
                    //{
                    //    if (InLetter.Count > 0)
                    //        i_Subject = InLetter[0].Subject;
                    //}
                    //catch { }
                    //i_Subject = res;
                    return i_Subject;
                }
            }
            set { SetPropertyValue("Subject", ref i_Subject, value); }
        }
        private string i_ConstrDeveloperContactInfo;
        [Size(500), DisplayName("Реквизиты застройщика")]
        public string ConstrDeveloperContactInfo
        {
            get
            {
                return i_ConstrDeveloperContactInfo;
            }
            set { SetPropertyValue("ConstrDeveloperContactInfo", ref i_ConstrDeveloperContactInfo, value); }
        }
        [Size(255), DisplayName("Кадастровый номер земельного участка")]
        [ImmediatePostData]
        public string LotCadNo
        {
            get
            {
                if (i_LotCadNo == null || i_LotCadNo == String.Empty)
                {
                    string res = String.Empty;
                    try
                    {
                        if (Parcels.Count > 0)
                            foreach (Parcel lt in Parcels)
                            {
                                if (lt.CadastralNumber != null || lt.CadastralNumber != String.Empty)
                                    if (res == String.Empty)
                                        res = lt.CadastralNumber;
                                    else
                                        res = res + "," + lt.CadastralNumber;
                            }
                    }
                    catch { }
                    i_LotCadNo = res;
                }
                return i_LotCadNo;
            }
            set { SetPropertyValue("LotCadNo", ref i_LotCadNo, value); }
        }

        private CityRegion i_CityRegion;
        [DisplayName("Район города")]
        public CityRegion CityRegion
        {
            get { return i_CityRegion; }
            set
            {
                SetPropertyValue("CityRegion", ref i_CityRegion, value);
                try
                {
                    i_LotAddressInfo = i_CityRegion.FullName;
                }
                catch { }
                OnChanged("CityRegion");
            }
        }

        private Street i_Street;
        [DisplayName("Улица")]
        public Street Street
        {
            get { return i_Street; }
            set
            {
                SetPropertyValue("Street", ref i_Street, value);

                try
                {
                    i_LotAddressInfo = i_Street.FullName;
                    if (CityRegion != null)
                        if (i_CityRegion.FullName != null && i_CityRegion.FullName != "")
                        {
                            i_LotAddressInfo = i_CityRegion.FullName + ", " + i_Street.FullName;
                        }
                }
                catch { }
                OnChanged("Street");
            }
        }

        //[Size(1000),DisplayName("Местонахождение земельного участка")]
        //public string LotAddress
        //{
        //    get
        //    {
        //        //if (i_LotAddress == null || i_LotAddress == String.Empty)
        //        //{
        //        //    string res = String.Empty;
        //        //    try
        //        //    {
        //        //        if (Lots.Count > 0)
        //        //            foreach (Lot lt in Lots)
        //        //            {
        //        //                if (lt.Adress != null || lt.Adress != String.Empty)
        //        //                    if (res == String.Empty)
        //        //                        res = lt.Adress;
        //        //                    else
        //        //                        res = res + ";" + lt.Adress;
        //        //            }
        //        //    }
        //        //    catch { }
        //        //    try
        //        //    {
        //        //        if (res == String.Empty)
        //        //        {
        //        //            res = Municipality.Name + ", " + CityRegion.Name;
        //        //        }
        //        //    }
        //        //    catch { }
        //        //    i_LotAddress = res;
        //        //}
        //        return i_LotAddress;
        //    }
        //    set { SetPropertyValue("LotAddress", ref i_LotAddress, value); }
        //}
        private string i_LotAddressInfo;
        [Size(1000), DisplayName("Местоположение границ участка")]
        public string LotAddressInfo
        {
            get
            {
                if (i_LotAddressInfo == null || i_LotAddressInfo == "")
                {
                    string res = String.Empty;
                    try
                    {
                        if (Parcels.Count > 0)
                            foreach (Parcel lt in Parcels)
                            {
                                if (lt.Adress != null || lt.Adress != "")
                                    if (res == String.Empty)
                                        res = lt.Adress;
                                    else
                                        res = res + "," + lt.Adress;
                            }
                    }
                    catch { }
                    i_LotAddressInfo = res;
                }
                return i_LotAddressInfo;
            }
            set { SetPropertyValue("LotAddressInfo", ref i_LotAddressInfo, value); }
        }
        [Size(255), DisplayName("Площадь земельного участка(кв.м.)")]
        public string LotSquare
        {
            get
            {
                if (i_LotSquare == null || i_LotSquare == "")
                {
                    string res = "";
                    try
                    {
                        if (Parcels.Count > 0)
                            foreach (Parcel lt in Parcels)
                            {
                                if (lt.Area != 0)
                                {
                                    //try
                                    //{
                                    //    if (lt.Unit.Name.ToLower().Contains("га"))
                                    //        res = (lt.Area * 10000).ToString();
                                    //    if (lt.Unit.Name.ToLower().Contains("кв.м") || lt.Unit.Name.ToLower().Contains("м2"))
                                    //        res = Parcels[0].Area.ToString();
                                    //}
                                    //catch { }
                                    if (res == "")
                                        res = lt.Area.ToString();
                                    else
                                        res = res + "," + lt.Area.ToString();
                                }
                            }
                    }
                    catch { }
                    i_LotSquare = res;
                }
                return i_LotSquare;
            }
            set { SetPropertyValue("LotSquare", ref i_LotSquare, value); }
        }
        private dFunctionalUsingKind i_functionalUsingKind;
        [DisplayName("Назначение проектируемого объекта(справочник)")]
        [DataSourceProperty("AvailableFuncUsingKind")]
        [ImmediatePostData]
        public dFunctionalUsingKind FunctionalUsingKind
        {
            get { return i_functionalUsingKind; }
            set { SetPropertyValue("FunctionalUsingKind", ref i_functionalUsingKind, value);
                try
                {
                    i_ObjectName = i_functionalUsingKind.Name;
                    i_ObjectNameRod = i_functionalUsingKind.Name.ToLower();
                }
                catch { }
                SetGPZUSizes();
                OnChanged("FunctionalUsingKind");
            }
        }
        //----------------------------------------------------
        // Получаем возможные для выбора виды использования, исходя из указанной террзоны
        private XPCollection<dFunctionalUsingKind> availableFuncUsingKind;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<dFunctionalUsingKind> AvailableFuncUsingKind
        {
            get
            {
                if (availableFuncUsingKind == null)
                {
                    // Retrieve all Terminals objects 
                    availableFuncUsingKind = new XPCollection<dFunctionalUsingKind>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableFuncUsingKind();
                }
                return availableFuncUsingKind;
            }
        }
        private void RefreshAvailableFuncUsingKind()
        {
            if (availableFuncUsingKind == null)
                return;
            if (TerrZoneKindLink != null)
            {
                availableFuncUsingKind.Criteria = new InOperator("Oid", GetFunkUsingKinds());
                //Connect connect = Connect.FromSession(Session);
                //foreach (MainZoneFunctionalUsingKind fuk in TerrZoneKindLink.MainZoneFunctionalUsingKind)
                //{
                //    try
                //    {
                //        availableFuncUsingKind.Add(fuk.FunctionalUsingKind);
                //    }
                //    catch { }
                //}
                //foreach (AddZoneFunctionalUsingKind fuk in TerrZoneKindLink.AddZoneFunctionalUsingKind)
                //{
                //    try {
                //        availableFuncUsingKind.Add(fuk.FunctionalUsingKind);
                //    }
                //    catch { }
                //}
                
                //availableFuncUsingKind.Criteria = new BinaryOperator("IsogdDocumentClass.Code", IsogdDocumentClass.Code);
                //availableDocNames = connect.FindObjects<dIsogdDocumentNameKind>(mc => mc.IsogdDocumentClass.Code == IsogdDocumentClass.Code) as XPCollection<dIsogdDocumentNameKind>;
            }
            else
            {
                availableFuncUsingKind = new XPCollection<dFunctionalUsingKind>(Session);
            }
            FunctionalUsingKind = null;
        }
        private List<string> GetFunkUsingKinds()
        {
            List<string> FunctionalUsingKindList = new List<string>();
            foreach (MainZoneFunctionalUsingKind fuk in TerrZoneKindLink.MainZoneFunctionalUsingKind)
            {
                try
                {
                    FunctionalUsingKindList.Add(fuk.FunctionalUsingKind.Oid.ToString());
                }
                catch { }
            }
            foreach (AddZoneFunctionalUsingKind fuk in TerrZoneKindLink.AddZoneFunctionalUsingKind)
            {
                try
                {
                    FunctionalUsingKindList.Add(fuk.FunctionalUsingKind.Oid.ToString());
                }
                catch { }
            }
            foreach (CondZoneFunctionalUsingKind fuk in TerrZoneKindLink.CondZoneFunctionalUsingKind)
            {
                try
                {
                    FunctionalUsingKindList.Add(fuk.FunctionalUsingKind.Oid.ToString());
                }
                catch { }
            }
            return FunctionalUsingKindList;
        }

        //----------------------------------------------------------
        private string i_ObjectName;
        [Size(255), DisplayName("Назначение проектируемого объекта(текст)")]
        public string ObjectName
        {
            get { return i_ObjectName; }
            set { SetPropertyValue("ObjectName", ref i_ObjectName, value); }
        }
        private string i_ObjectNameRod;
        [Size(255), DisplayName("Назначение объекта(род. падеж)")]
        public string ObjectNameRod
        {
            get { return i_ObjectNameRod; }
            set { SetPropertyValue("ObjectNameRod", ref i_ObjectNameRod, value); }
        }
        private eGPZUBuildingKind i_GPZUBuildingKind;
        [DisplayName("Вид проектируемого объекта")]
        public eGPZUBuildingKind GPZUBuildingKind
        {
            get { return i_GPZUBuildingKind; }
            set { SetPropertyValue("GPZUBuildingKind", ref i_GPZUBuildingKind, value);
                SetGPZUSizes();
                OnChanged("GPZUBuildingKind");
            }
        }
        
        private eGPZUWorkKind i_GPZUWorkKind;
        [DisplayName("Вид работ")]
        public eGPZUWorkKind GPZUWorkKind
        {
            get { return i_GPZUWorkKind; }
            set { SetPropertyValue("GPZUWorkKind", ref i_GPZUWorkKind, value); }
        }
        [Size(1000), DisplayName("Местоположение проектируемого объекта")]
        public string ObjectAddress
        {
            get { return i_ObjectAddress; }
            set { SetPropertyValue("ObjectAddress", ref i_ObjectAddress, value); }
        }

        //private Municipality i_Municipality;
        //[DisplayName("Муниципальное образование")]
        //public Municipality Municipality
        //{
        //    get {
        //        try { i_Municipality = GetMunicipality(); }
        //        catch { }
        //        return i_Municipality; }
        //    set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        //}

        //public Municipality GetMunicipality()
        //{
        //    Municipality mun = null;
        //    if (Empl != null)
        //        if (Empl.Departament != null)
        //            if (Empl.Departament.MainDepartment != null)
        //                if (Empl.Departament.MainDepartment.Municipality != null)
        //                    mun = Empl.Departament.MainDepartment.Municipality;
        //    return mun;
        //}

        private string i_ObjectSquare;
        
        
        

        private string i_MaxFloorCount;
        [Size(12),DisplayName("Макс. кол-во этажей")]
        public string MaxFloorCount
        {
            get { return i_MaxFloorCount; }
            set { SetPropertyValue("MaxFloorCount", ref i_MaxFloorCount, value); }
        }
        private string i_MaxHeight;
        [Size(12), DisplayName("Макс. высота (м)")]
        public string MaxHeight
        {
            get { return i_MaxHeight; }
            set { SetPropertyValue("MaxHeight", ref i_MaxHeight, value); }
        }
        private string i_MaxProcentBuilding;
        [Size(12), DisplayName("Макс. % застройки")]
        public string MaxProcentBuilding
        {
            get { return i_MaxProcentBuilding; }
            set { SetPropertyValue("MaxProcentBuilding", ref i_MaxProcentBuilding, value); }
        }
        private void SetGPZUSizes()
        {
            // получаем нашу террзону, и ищем по выбранному назначению объекта его параметры
            bool isFound = false;
            bool isOKNSizes = false;
            
            if (!isOKNSizes)
            {
                try
                {
                    try
                    {
                        foreach (MainZoneFunctionalUsingKind fuk in TerrZoneKindLink.MainZoneFunctionalUsingKind)
                        {
                            if (fuk.FunctionalUsingKind.Oid.ToString() == FunctionalUsingKind.Oid.ToString())
                            {
                                try
                                {
                                    if (MaxFloorCount == null || MaxFloorCount == "" || MaxFloorCount == "-")
                                        MaxFloorCount = fuk.MaxFloorCountMax;
                                }
                                catch { }
                                try
                                {
                                    if (MaxHeight == null || MaxHeight == "" || MaxHeight == "-")
                                        MaxHeight = fuk.MaxHeight;
                                }
                                catch { }
                                try
                                {
                                    if (MaxProcentBuilding == null || MaxProcentBuilding == "" || MaxProcentBuilding == "-")
                                        MaxProcentBuilding = fuk.MaxProcentBuilding;
                                }
                                catch { }
                                isFound = true;
                                break;
                            }
                        }
                    }
                    catch { }
                    try
                    {
                        if (!isFound)
                        {
                            foreach (AddZoneFunctionalUsingKind fuk in TerrZoneKindLink.AddZoneFunctionalUsingKind)
                            {
                                try
                                {
                                    if (MaxFloorCount == null || MaxFloorCount == "" || MaxFloorCount == "-")
                                        MaxFloorCount = fuk.MaxFloorCountMax;
                                }
                                catch { }
                                try
                                {
                                    if (MaxHeight == null || MaxHeight == "" || MaxHeight == "-")
                                        MaxHeight = fuk.MaxHeight;
                                }
                                catch { }
                                try
                                {
                                    if (MaxProcentBuilding == null || MaxProcentBuilding == "" || MaxProcentBuilding == "-")
                                        MaxProcentBuilding = fuk.MaxProcentBuilding;
                                }
                                catch { }
                                isFound = true;
                                break;
                            }
                        }
                    }
                    catch { }
                    try
                    {
                        if (!isFound)
                        {
                            foreach (CondZoneFunctionalUsingKind fuk in TerrZoneKindLink.CondZoneFunctionalUsingKind)
                            {
                                try
                                {
                                    if (MaxFloorCount == null || MaxFloorCount == "" || MaxFloorCount == "-")
                                        MaxFloorCount = fuk.MaxFloorCountMax;
                                }
                                catch { }
                                try
                                {
                                    if (MaxHeight == null || MaxHeight == "" || MaxHeight == "-")
                                        MaxHeight = fuk.MaxHeight;
                                }
                                catch { }
                                try
                                {
                                    if (MaxProcentBuilding == null || MaxProcentBuilding == "" || MaxProcentBuilding == "-")
                                        MaxProcentBuilding = fuk.MaxProcentBuilding;
                                }
                                catch { }
                                isFound = true;
                                break;
                            }
                        }
                    }
                    catch { }
                }
                catch { }
            }
            if (GPZUBuildingKind != eGPZUBuildingKind.ЛинейныйОбъект)
            {
                try
                {
                    if (MonumentZoneKind.Count > 0)
                    {
                        foreach (MonumentZoneKind monumentZoneKind in MonumentZoneKind)
                        {
                            if (monumentZoneKind.NameSmall == "И-2" || monumentZoneKind.NameSmall == "И-3")
                                MaxFloorCount = "2";
                            if (monumentZoneKind.NameSmall == "И-4")
                                MaxFloorCount = "3";
                            if (monumentZoneKind.NameSmall == "И-5")
                            {
                                MaxFloorCount = "5";
                                if (GPZUBuildingKind == eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                                    MaxFloorCount = "3";
                                try
                                {
                                    if (TerrZoneKindLink.NameSmall == "Ж-1")
                                        MaxFloorCount = "3";
                                }
                                catch { }
                            }
                        }
                    }
                }
                catch { }
            }
            if (MaxFloorCount == null || MaxFloorCount == "")
                MaxFloorCount = "-";

            if (MaxHeight == null || MaxHeight == "")
                MaxHeight = "-";

            if (MaxProcentBuilding == null || MaxProcentBuilding == "")
                MaxProcentBuilding = "-";

        }

        private bool i_R1OKNZoneFlag;
        [DisplayName("Часть участка в Р-1")]
        public bool R1OKNZoneFlag
        {
            get { return i_R1OKNZoneFlag; }
            set { SetPropertyValue("R1OKNZoneFlag", ref i_R1OKNZoneFlag, value); }
        }
        private bool i_I1OKNZoneFlag;
        [DisplayName("Часть участка в И-1")]
        public bool I1OKNZoneFlag
        {
            get { return i_I1OKNZoneFlag; }
            set { SetPropertyValue("I1OKNZoneFlag", ref i_I1OKNZoneFlag, value); }
        }
        private bool i_CommonUseZoneFlag;
        [DisplayName("Часть участка в территории общего пользования")]
        public bool CommonUseZoneFlag
        {
            get { return i_CommonUseZoneFlag; }
            set { SetPropertyValue("CommonUseZoneFlag", ref i_CommonUseZoneFlag, value); }
        }
        //private bool i_TopoExistFlag;
        //[DisplayName("Есть топосъемка")]
        //public bool TopoExistFlag
        //{
        //    get { return i_TopoExistFlag; }
        //    set { SetPropertyValue("TopoExistFlag", ref i_TopoExistFlag, value); }
        //}
        




        // ссылка на территориальную зону, которая пойдет в формирование ГПЗУ
        // проставляется автоматом, если в XPCollection<TerrZoneKind> только одна зона
        // если там несколько типов зон, то возможность выбора только из них
        // если там нет типов зон, то выбор из всех типов зон
        private TerrZoneKind i_TerrZoneKindLink;
        [DisplayName("Территориальная зона")]
        [DataSourceProperty("AvailableTerrZones")]
        [ImmediatePostData]
        public TerrZoneKind TerrZoneKindLink
        {
            get { return i_TerrZoneKindLink; }
            set { try { SetPropertyValue("TerrZoneKindLink", ref i_TerrZoneKindLink, value);
                    RefreshAvailableFuncUsingKind();
                    OnChanged("TerrZoneKindLink");
                } catch { } }
        }

        //----------------------------------------------------
        // Получаем возможные для выбора территориальные зоны
        private XPCollection<TerrZoneKind> availableTerrZones;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<TerrZoneKind> AvailableTerrZones
        {
            get
            {
                if (availableTerrZones == null)
                {
                    // Retrieve all Terminals objects 
                    availableTerrZones = new XPCollection<TerrZoneKind>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableTerrZones();
                }
                return availableTerrZones;
            }
        }
        private void RefreshAvailableTerrZones()
        {
            if (availableTerrZones == null)
                return;
            if (TerrZoneKind.Count >= 1)
            {
                availableTerrZones = TerrZoneKind;
                if (TerrZoneKind.Count == 1)
                    TerrZoneKindLink = TerrZoneKind.FirstOrDefault();//[0];
            }
            else
            {
                availableTerrZones = new XPCollection<TerrZoneKind>(Session);
            }
            TerrZoneKindLink = null;
        }

        [Size(1000), DisplayName("Информация о разделении земельного участка")]
        public string LotDivisionInfo
        {
            get { return i_LotDivisionInfo; }
            set { SetPropertyValue("LotDivisionInfo", ref i_LotDivisionInfo, value); }
        }
        private string i_ZonesOKSStateNeedsInfo;
        [Size(4000), DisplayName("Информация о зонах размещения ОКС для гос.нужд")]
        public string ZonesOKSStateNeedsInfo
        {
            get { return i_ZonesOKSStateNeedsInfo; }
            set { SetPropertyValue("ZonesOKSStateNeedsInfo", ref i_ZonesOKSStateNeedsInfo, value); }
        }
        //private string i_AnotherInfo;
        //[Size(4000), DisplayName("Иная информация")]
        //public string AnotherInfo
        //{
        //    get { return i_AnotherInfo; }
        //    set { SetPropertyValue("AnotherInfo", ref i_AnotherInfo, value); }
        //}
        //private string i_PlanPrepared;
        //[Size(4000), DisplayName("План подготовлен")]
        //public string PlanPrepaped
        //{
        //    get { return i_PlanPrepared; }
        //    set { SetPropertyValue("PlanPrepaped", ref i_PlanPrepared, value); }
        //}

        [Size(1000), DisplayName("Примечания")]
        public string Notes { get; set; }

        private string i_GPZUSubmitText;
        [Size(1000), DisplayName("Реквизиты утверждающего документа")]
        public string GPZUSubmitText
        {
            get { return i_GPZUSubmitText; }
            set { try { SetPropertyValue("GPZUSubmitText", ref i_GPZUSubmitText, value); } catch { } }
        }

        private GPZUSubmit i_GPZUSubmit;
        [DisplayName("Приказ об утверждении ГПЗУ")]
        [ImmediatePostData]
        public GPZUSubmit GPZUSubmit
        {
            get { return i_GPZUSubmit; }
            set { try { SetPropertyValue("GPZUSubmit", ref i_GPZUSubmit, value); } catch { } }
        }

        /// <summary>
        /// текст для иных показателей
        /// </summary>

        // 1. Инженерные изыскания
        private string i_TopoInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("1. Инженерные изыскания")]
        public string TopoInfo
        {
            get { return i_TopoInfo; }
            set { SetPropertyValue("TopoInfo", ref i_TopoInfo, value); }
        }

        // 2. Планировочная организация земельного участка
        private string i_ParcelPlaningInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("2. Планировочная организация земельного участка")]
        public string ParcelPlaningInfo
        {
            get { return i_ParcelPlaningInfo; }
            set { SetPropertyValue("ParcelPlaningInfo", ref i_ParcelPlaningInfo, value); }
        }

        // 3. Архитектурные решения
        private string i_ArchSolutionsInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("3. Архитектурные решения")]
        public string ArchSolutionsInfo
        {
            get { return i_ArchSolutionsInfo; }
            set { SetPropertyValue("ArchSolutionsInfo", ref i_ArchSolutionsInfo, value); }
        }

        // 4. Остальные пункты
        private string i_AnotherInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("Проектом строительства предусмотреть")]
        public string AnotherInfo
        {
            get { return i_AnotherInfo; }
            set { SetPropertyValue("AnotherInfo", ref i_AnotherInfo, value); }
        }

        /// <summary>
        /// Данные по существующим ОКС на участке
        /// </summary>
        
        private bool i_isGpzuOKSExist;
        [DisplayName("ОКС на участке")]
        public bool isGpzuOKSExist
        {
            get { return i_isGpzuOKSExist; }
            set { SetPropertyValue("isGpzuOKSExist", ref i_isGpzuOKSExist, value); }
        }
        private int i_GpzuOKSCount;
        [DisplayName("Кол-во ОКС на участке")]
        public int GpzuOKSCount
        {
            get { return i_GpzuOKSCount; }
            set { SetPropertyValue("GpzuOKSCount", ref i_GpzuOKSCount, value); }
        }

        [Association, DisplayName("Существующие ОКС на участке")]
        public XPCollection<GpzuOKS> GpzuOKS
        {
            get { return GetCollection<GpzuOKS>("GpzuOKS"); }
        }

        /// <summary>
        /// Проект планировки и проект межевания
        /// </summary>

        private bool i_isProjectPlanExist;
        [DisplayName("Проект планировки утвержден")]
        public bool isProjectPlanExist
        {
            get { return i_isProjectPlanExist; }
            set { SetPropertyValue("isProjectPlanExist", ref i_isProjectPlanExist, value); }
        }
        private string i_ProjectPlanInfo;
        [Size(SizeAttribute.Unlimited),DisplayName("Реквизиты проекта планировки")]
        public string ProjectPlanInfo
        {
            get { return i_ProjectPlanInfo; }
            set { SetPropertyValue("ProjectPlanInfo", ref i_ProjectPlanInfo, value); }
        }

        private bool i_isProjectLandSurveyingExist;
        [DisplayName("Проект межевания утвержден")]
        public bool isProjectLandSurveyingExist
        {
            get { return i_isProjectLandSurveyingExist; }
            set { SetPropertyValue("isProjectLandSurveyingExist", ref i_isProjectLandSurveyingExist, value); }
        }
        private string i_ProjectLandSurveyingInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("Реквизиты проекта межевания")]
        public string ProjectLandSurveyingInfo
        {
            get { return i_ProjectLandSurveyingInfo; }
            set { SetPropertyValue("ProjectLandSurveyingInfo", ref i_ProjectLandSurveyingInfo, value); }
        }


        [Association, DisplayName("Застройщики")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }
        [Association, DisplayName("Места застройки")]
        public XPCollection<UnderConstructionPlace> underConstructionPlace
        {
            get { return GetCollection<UnderConstructionPlace>("underConstructionPlace"); }
        }
        
        [Association, DisplayName("Терр. зоны")]
        [ImmediatePostData]
        public XPCollection<TerrZoneKind> TerrZoneKind
        {
            get { return GetCollection<TerrZoneKind>("TerrZoneKind"); }
        }
        [Association, DisplayName("Зоны ограничений")]
        public XPCollection<SpecialZoneKind> SpecialZoneKind
        {
            get { return GetCollection<SpecialZoneKind>("SpecialZoneKind"); }
        }
        [Association, DisplayName("Зоны ОКН")]
        public XPCollection<MonumentZoneKind> MonumentZoneKind
        {
            get { return GetCollection<MonumentZoneKind>("MonumentZoneKind"); }
        }
        [Association, DisplayName("ОКН на участке")]
        public XPCollection<GpzuMonument> GpzuMonuments
        {
            get { return GetCollection<GpzuMonument>("GpzuMonuments"); }
        }
        //[Association, DisplayName("Постановление об утверждении")]
        //public XPCollection<GPZUSubmit> GPZUSubmit
        //{
        //    get { return GetCollection<GPZUSubmit>("GPZUSubmit"); }
        //}
        [Association, DisplayName("Техусловия")]
        public XPCollection<TechSpecifications> TechSpecifications
        {
            get { return GetCollection<TechSpecifications>("TechSpecifications"); }
        }
        [Association, DisplayName("Координаты")]
        public XPCollection<Coordinate> Coordinates
        {
            get { return GetCollection<Coordinate>("Coordinates"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (DocKind == null)
            this.DocKind = Session.FindObject<dDocKind>(new BinaryOperator("Name", "Градостроительный план земельного участка"));
            if (i_ZonesOKSStateNeedsInfo == null || i_ZonesOKSStateNeedsInfo == "")
                i_ZonesOKSStateNeedsInfo = "нет";
            if (i_LotDivisionInfo == null || i_LotDivisionInfo == "")
                i_LotDivisionInfo = "СП 42.13330.2011  «Градостроительство. Планировка и застройка городских и сельских поселений», " + 
                    "действующие нормативы градостроительного проектирования муниципального образования «Город Вологда»";
            Session.CommitTransaction();
        }

        protected override void OnSaved()
        {
            if (!IsDeleted)
            {
            }
            base.OnSaved();
        }

        protected override void OnSaving()
        {
        }
        #region Логика при изменении XPCollections
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //изменилась коллекция входящих обращений
            if (property.Name == "InLetter")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(InLetters_CollectionChanged);
            }
            //изменилась коллекция застройщиков
            if (property.Name == "DocSubjects")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(DocSubjects_CollectionChanged);
            }
            //изменилась коллекция зон ОКН
            if (property.Name == "MonumentZoneKind")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(MonumentZoneKind_CollectionChanged);
            }
            return result;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции входящих обращений - заполнение листа по застройщикам
        ///// </summary>
        private void InLetters_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                InLetter newObj = (InLetter)e.ChangedObject;
                if (newObj.DocSubjects.Count > 0)
                {
                    foreach (GeneralSubject DocSubject in newObj.DocSubjects)
                    {
                        DocSubjects.Add(DocSubject);
                    }

                }
            }
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции застройщиков - заполнение поля по застройщикам
        ///// </summary>
        private void DocSubjects_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetDevelopers();
            }
        }
        /// <summary>
        /// Заполенение поля Застройщик (string) по листу застройщиков (XPCollection)
        /// </summary>
        private void SetDevelopers()
        {
            string dev = "";
            string contact = "";
            foreach (GeneralSubject subj in DocSubjects)
            {
                if (dev == "")
                {
                    dev = subj.FullName;
                }
                else
                    if (!dev.Contains(subj.FullName))
                    dev += ", " + subj.FullName;
                if (contact == "")
                {
                    contact = subj.FullContactInfo;
                }
                else
                    if (!contact.Contains(subj.FullContactInfo))
                    contact += ", " + subj.FullContactInfo;

            }
            if (dev.Length > 500)
                dev = dev.Remove(499, dev.Length);
            Subject = dev;
            if (contact.Length > 500)
                contact = contact.Remove(499, contact.Length);
            ConstrDeveloperContactInfo = contact;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции земельных участков - заполнение поля кадастровых номеров
        ///// </summary>
        private void MonumentZoneKind_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                MonumentZoneKind newObj = (MonumentZoneKind)e.ChangedObject;
                if (newObj.NameSmall.ToLower() == "р-1")
                {
                    this.R1OKNZoneFlag = true;
                }
                if (newObj.NameSmall.ToLower() == "и-1")
                {
                    this.I1OKNZoneFlag = true;
                }
                SetGPZUSizes();
            }
        }

        /// <summary>
        /// Заполенение поля кадастровых номеров(string) по листу земельных участков (XPCollection)
        /// </summary>
        private void SetCadNoms()
        {
            string res = "";
            foreach (Parcel obj in Parcels)
            {
                if (res == "")
                    res = obj.CadastralNumber;
                else
                    if (!res.Contains(obj.CadastralNumber))
                    res += ", " + obj.CadastralNumber;
            }
            if (res.Length > 255)
                res = res.Remove(254, res.Length);
            LotCadNo = res;
        }
        #endregion
    }
}
