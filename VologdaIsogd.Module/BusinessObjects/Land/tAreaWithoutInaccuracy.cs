using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "�������� ������� (� ����������� �� 0,01 ��. �) ��� ����������� �����������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class tAreaWithoutInaccuracy : BaseObjectXAF
    {
        public tAreaWithoutInaccuracy(Session session) : base(session) { }


        private double i_Area;
        [Size(20), DisplayName("�������� �������")]
        public double Area
        {
            get { return i_Area; }
            set { SetPropertyValue("Area", ref i_Area, value); }
        }

        private dUnit i_Unit;
        [DisplayName("������� ���������")]
        public dUnit Unit
        {
            get { return i_Unit; }
            set { SetPropertyValue("Unit", ref i_Unit, value); }
        }
    }
}