using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using AISOGD.Dictonaries;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "������� ������������� �� �����"),
    System.ComponentModel.DefaultProperty("Name")]
    public class dBaseClassifierLand : BaseObjectXAF
    {
        public dBaseClassifierLand(Session session) : base(session) { }


        private string i_Code;
        [Size(64), DisplayName("���")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }

        private string i_Name;
        [Size(4000), DisplayName("��������")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }


    [ModelDefault("Caption", "���� ����� ������������ ������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]//, NavigationItem("�������� �����������")]
    public class dOldNumberType : dBaseClassifierLand
    {
        public dOldNumberType(Session session) : base(session) { }

    }

    [ModelDefault("Caption", "��������� ������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dCategories : dBaseClassifierLand
    {
        public dCategories(Session session) : base(session) { }

    }

    [ModelDefault("Caption", "������ ����������� ���������� �������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]//, NavigationItem("�������� �����������")]
    public class dMethod : dBaseClassifierLand
    {
        public dMethod(Session session) : base(session) { }

    }

    [ModelDefault("Caption", "������� ���������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dUnit : dBaseClassifierLand
    {
        public dUnit(Session session) : base(session) { }

    }

    [ModelDefault("Caption", "��� ���������� �������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dParcels : dBaseClassifierLand
    {
        public dParcels(Session session) : base(session) { }

    }

    [ModelDefault("Caption", "������ ������� ������������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dStates : dBaseClassifierLand
    {
        public dStates(Session session) : base(session) { }

    }

    [ModelDefault("Caption", "������������� ����� ������������� ������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dUtilizations : dBaseClassifierLand
    {
        public dUtilizations(Session session) : base(session) { }

    }
    [ModelDefault("Caption", "��� ���������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]//, NavigationItem("�������� �����������")]
    public class dAllDocuments  : dBaseClassifierLand
    {
        public dAllDocuments(Session session) : base(session) { }

    }
    [ModelDefault("Caption", "��� ���������� �������"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]//, NavigationItem("�������� �����������")]
    public class dNaturalObjects  : dBaseClassifierLand
    {
        public dNaturalObjects(Session session) : base(session) { }

    }
    [ModelDefault("Caption", "������� ���������� �����"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]//, NavigationItem("�������� �����������")]
    public class dForestUse  : dBaseClassifierLand
    {
        public dForestUse(Session session) : base(session) { }

    }
    [ModelDefault("Caption", "����������� (�����������) ����"),
    System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dEncumbrances  : dBaseClassifierLand
    {
        public dEncumbrances(Session session) : base(session) { }

    }
}