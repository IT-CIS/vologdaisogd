using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.Isogd;
//using AISOGD.DocFlow;
using AISOGD.OrgStructure;
//using AISOGD.TempObj;
//using AISOGD.Constr;
//using AISOGD.Reglament;
using System.Drawing;
//using AISOGD.Constr;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [NavigationItem("����� �������")]
    [ModelDefault("Caption", "��������� �������"), System.ComponentModel.DefaultProperty("descript")]
    public class Lot : AISOGD.Rosreestr.Parcel
    {
        public Lot(Session session) : base(session) { }

        private const string descriptFormat = "��������� ������� ʹ {CadNo}, {Name}";

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("��������� ������� ʹ {0}, {1}", CadastralNumber, Name);
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(255), DisplayName("�������� ��������")]
        public string Adress { get; set; }


        //private string i_RightHoldersText;
        //[Size(510), DisplayName("���������������")]
        //public string RightHoldersText
        //{
        //    get
        //    {
        //        string res = String.Empty;
        //        try
        //        {
        //            foreach (RightHolder rh in RightHolders)
        //            {
        //                if (res != String.Empty)
        //                    res += ", ";
        //                res += rh.RequesterString;
        //            }
        //        }
        //        catch { }
        //        return res;
        //    }
        //    set { SetPropertyValue("RightHoldersText", ref i_RightHoldersText, value); }
        //}

        //private string i_LawDocument;
        //[Size(510), DisplayName("�������� ��������������")]
        //public string LawDocument
        //{
        //    get
        //    {
        //        string res = String.Empty;
        //        try
        //        {
        //            foreach (RightHolder rh in RightHolders)
        //            {
        //                if (res != String.Empty)
        //                    res += ", ";
        //                res += rh.LawDocument;
        //            }
        //        }
        //        catch { }
        //        i_LawDocument = res;
        //        return i_LawDocument;
        //    }
        //    set { SetPropertyValue("LawDocument", ref i_LawDocument, value); }
        //}

        private string i_CadastralOrder;
        [Size(255), DisplayName("����������� �������/ ������� ���������� �������")]
        public string CadastralOrder
        {
            get { return i_CadastralOrder; }
            set { SetPropertyValue("CadastralOrder", ref i_CadastralOrder, value); }
        }

        private Municipality _Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get { return _Municipality; }
            set { try { SetPropertyValue("Municipality", ref _Municipality, value); } catch { } }
        }

        //private string i_TerrZoneString;
        //[Size(510), DisplayName("��������������� ����")]
        //[System.ComponentModel.Browsable(false)]
        //public string TerrZoneString
        //{
        //    get
        //    {
        //        if (i_TerrZoneString == null || i_TerrZoneString == String.Empty)
        //        {
        //            string res = String.Empty;
        //            try
        //            {
        //                if (TerrZoneKind.Count > 0)
        //                    if (TerrZoneKind[0].Name != null)
        //                        res = TerrZoneKind[0].Name;
        //            }
        //            catch { }
        //            i_TerrZoneString = res;
        //        }
        //        return i_TerrZoneString;
        //    }
        //    set { SetPropertyValue("TerrZoneString", ref i_TerrZoneString, value); }
        //}

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }



        [Association, DisplayName("��������������")]
        public XPCollection<Right> Rights
        {
            get
            {
                return GetCollection<Right>("Rights");
            }
        }

        [Association, DisplayName("��������� �����, ��������� � ��������")]
        //[System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdDocument> IsogdDocuments
        {
            get { return GetCollection<IsogdDocument>("IsogdDocuments"); }
        }

        [Association, DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBooks
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBooks"); }
        }
        [Association, DisplayName("���")]
        public XPCollection<PhysStorageBook> PhysStorageBooks
        {
            get { return GetCollection<PhysStorageBook>("PhysStorageBooks"); }
        }

        //[Association, DisplayName("��������� �� ���������� �������")]
        //[System.ComponentModel.Browsable(true)]
        //public XPCollection<GeneralDocBase> generalDocBase
        //{
        //    get
        //    {
        //        return GetCollection<GeneralDocBase>("generalDocBase");
        //    }
        //}


        //[Association, DisplayName("������� ������������� �� ��������� �������")]
        //[System.ComponentModel.Browsable(true)]
        //public XPCollection<CapitalStructureBase> CapitalStructureBase
        //{
        //    get
        //    {
        //        return GetCollection<CapitalStructureBase>("CapitalStructureBase");
        //    }
        //}

        //[Association, DisplayName("��������������� ���� �������")]
        //[System.ComponentModel.Browsable(false)]
        //public XPCollection<TerrZoneKind> TerrZoneKind
        //{
        //    get
        //    {
        //        return GetCollection<TerrZoneKind>("TerrZoneKind");
        //    }
        //}

        //[Association, DisplayName("�����")]
        //public XPCollection<Address.Address> Addresses
        //{
        //    get
        //    {
        //        return GetCollection<Address.Address>("Addresses");
        //    }
        //}

        private CadastralBlock i_CadastralBlockLink;
        [Association, DisplayName("����������� �������")]
        public CadastralBlock CadastralBlockLink
        {
            get { return i_CadastralBlockLink; }
            set { SetPropertyValue("CadastralBlockLink", ref i_CadastralBlockLink, value); }
        }

        [Association("CadastreCostOnDate-Lot"), DisplayName("����������� ���������")]
        public XPCollection<CadastreCostOnDate> �adastreCostOnDate
        {
            get
            {
                return GetCollection<CadastreCostOnDate>("�adastreCostOnDate");
            }
        }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }
    }
}