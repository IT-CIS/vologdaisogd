using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.Isogd;
//using AISOGD.DocFlow;
using AISOGD.OrgStructure;
//using AISOGD.TempObj;
//using AISOGD.Constr;
//using AISOGD.Reglament;
using System.Drawing;
//using AISOGD.Constr;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;
using AISOGD.General;
using AISOGD.DocFlow;
using AISOGD.Constr;
using AISOGD.SystemDir;

namespace AISOGD.Land
{
    [NavigationItem("����� �������")]
    [ModelDefault("Caption", "��������� �������"), System.ComponentModel.DefaultProperty("descript")]
    public class Parcel : AttachBase
    {
        public Parcel(Session session) : base(session) { }

        [Size(255), DisplayName("�������� �������")]
        public string descript
        {
            get
            {
                string name = "";
                if (Name != null && Name.Name != "")
                    name = ", " + Name.Name;
                return String.Format("��������� ������� ʹ {0}{1}", CadastralNumber, name);
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        
        private string i_CadastralNumber;
        [Size(255), DisplayName("����������� �����")]
        [ModelDefault("EditMask", "35:2[4-5]{1}:[0-9]{7}:[1-9]{1}[0-9]{0,4}")]
        [ModelDefault("EditMaskType", "RegEx")]
        public string CadastralNumber
        {
            get { return i_CadastralNumber; }
            set { SetPropertyValue("CadastralNumber", ref i_CadastralNumber, value); }
        }

        [Size(255), DisplayName("�������� ��������")]
        public string Adress { get; set; }

        private DateTime i_DateExpiry;
        [DisplayName("���� ��������� ���������� ��������� �������� � ��")]
        public DateTime DateExpiry
        {
            get { return i_DateExpiry; }
            set { SetPropertyValue("DateExpiry", ref i_DateExpiry, value); }
        }

        private DateTime i_DateCreated;
        [DisplayName("���� ���������� �� ���� (���� �������� ������ � ���)")]
        public DateTime DateCreated
        {
            get { return i_DateCreated; }
            set { SetPropertyValue("DateCreated", ref i_DateCreated, value); }
        }

        private DateTime i_DateRemoved;
        [DisplayName("���� ����������� ������������� (���� ������ � ������������ �����)")]
        public DateTime DateRemoved
        {
            get { return i_DateRemoved; }
            set { SetPropertyValue("DateRemoved", ref i_DateRemoved, value); }
        }

        private DateTime i_DateCreatedDoc;
        [DisplayName("���� ���������� �� ���� �� ��������� (��� ����� �������� ��������)")]
        public DateTime DateCreatedDoc
        {
            get { return i_DateCreatedDoc; }
            set { SetPropertyValue("DateCreatedDoc", ref i_DateCreatedDoc, value); }
        }
        private dStates i_State;
        [DisplayName("������ ���������� �������")]
        public dStates State
        {
            get { return i_State; }
            set { SetPropertyValue("State", ref i_State, value); }
        }

        private string i_CadastralBlock;
        [Size(40), DisplayName("����� ������������ ��������")]
        public string CadastralBlock
        {
            get { return i_CadastralBlock; }
            set { SetPropertyValue("CadastralBlock", ref i_CadastralBlock, value); }
        }

        private dParcels i_Name;
        [DisplayName("������������ �������")]
        //[ImmediatePostData]
        public dParcels Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private dMethod i_Method;
        [DisplayName("������ ����������� ���������� �������")]
        public dMethod Method 
        {
            get { return i_Method; }
            set { SetPropertyValue("Method", ref i_Method, value); }
        }

        private string i_CadastralOrder;
        [Size(255), DisplayName("����������� �������/ ������� ���������� �������")]
        public string CadastralOrder
        {
            get { return i_CadastralOrder; }
            set { SetPropertyValue("CadastralOrder", ref i_CadastralOrder, value); }
        }

        //private Municipality _Municipality;
        //[DisplayName("������������� �����������")]
        //public Municipality Municipality
        //{
        //    get { return _Municipality; }
        //    set { try { SetPropertyValue("Municipality", ref _Municipality, value); } catch { } }
        //}

        #region
        //private string i_tOldNumbers;
        //[Size(500), DisplayName("����� ����������� ��������������� ������� ������")]
        //public string tOldNumbers
        //{
        //    get { return i_tOldNumbers; }
        //    set { SetPropertyValue("tOldNumbers", ref i_tOldNumbers, value); }
        //}

        //private string i_PrevCadastralNumbers;
        //[Size(500), DisplayName("����������� ������ ��������� ��������, �� ������� ��������� ������ �������")]
        //public string PrevCadastralNumbers
        //{
        //    get { return i_PrevCadastralNumbers; }
        //    set { SetPropertyValue("PrevCadastralNumbers", ref i_PrevCadastralNumbers, value); }
        //}

        //private string i_InnerCadastralNumbers;
        //[Size(1000), DisplayName("����������� ������ ������, ����������, �������� �������������� �������������, ������������� �� ��������� �������")]
        //public string InnerCadastralNumbers
        //{
        //    get { return i_InnerCadastralNumbers; }
        //    set { SetPropertyValue("InnerCadastralNumbers", ref i_InnerCadastralNumbers, value); }
        //}
        #endregion

        private dCategories i_Categories;
        [DisplayName("��������� ������")]
        public dCategories Categories
        {
            get { return i_Categories; }
            set { SetPropertyValue("Categories", ref i_Categories, value); }
        }


        /// <summary>
        /// ����������� �������������
        /// </summary>
        private dUtilizations i_Utilization;
        [DisplayName("��� ������������ ������������� �� ��������������")]
        public dUtilizations Utilization
        {
            get { return i_Utilization; }
            set { SetPropertyValue("Utilization", ref i_Utilization, value); }
        }

        private string i_ByDoc;
        [Size(4000), DisplayName("��� ������������� ������� �� ���������")]
        public string ByDoc
        {
            get { return i_ByDoc; }
            set { SetPropertyValue("ByDoc", ref i_ByDoc, value); }
        }

        
        private eSquareType i_SquareType;
        [DisplayName("��� �������")]
        public eSquareType SquareType
        {
            get { return i_SquareType; }
            set { SetPropertyValue("SquareType", ref i_SquareType, value); }
        }
        /// <summary>
        /// ������� � ����������� �� 1 ��. � � ����������� ����������� �������
        /// </summary>
        private double i_Area;
        [Size(20), DisplayName("�������� �������")]
        public double Area
        {
            get { return i_Area; }
            set { SetPropertyValue("Area", ref i_Area, value); }
        }

        private dUnit i_Unit;
        [DisplayName("������� ���������")]
        public dUnit Unit
        {
            get { return i_Unit; }
            set { SetPropertyValue("Unit", ref i_Unit, value); }
        }

        private double i_Inaccuracy;
        [Size(20), DisplayName("����������� ����������")]
        public double Inaccuracy
        {
            get { return i_Inaccuracy; }
            set { SetPropertyValue("Inaccuracy", ref i_Inaccuracy, value); }
        }
        //private IsogdStorageBook i_IsogdStorageBook;
        //[DisplayName("����� �������� ��������� �����")]
        //[ImmediatePostData]
        //public IsogdStorageBook IsogdStorageBook
        //{
        //    get {
        //        try
        //        {
        //            i_IsogdStorageBook = GetIsogdStorageBook();
        //        }
        //        catch { }
        //        return i_IsogdStorageBook; }
        //    set
        //    {
        //        SetPropertyValue("IsogdStorageBook", ref i_IsogdStorageBook, value);
        //        if (i_IsogdStorageBook != null)
        //        {
        //            if(i_IsogdStorageBook.Parcel == null)
        //                i_IsogdStorageBook.Parcel = this;
        //        }
        //    }
        //}
        private IsogdStorageBook GetIsogdStorageBook()
        {
            IsogdStorageBook res = null;
            Connect connect = Connect.FromSession(Session);
            res = connect.FindFirstObject<IsogdStorageBook>(mc => mc.Code.Trim() == this.CadastralNumber.Trim());
            return res;
        }
        [Association, DisplayName("��������������")]
        public XPCollection<Right> Rights
        {
            get
            {
                return GetCollection<Right>("Rights");
            }
        }

        [Association, DisplayName("��������� �����, ��������� � ��������")]
        //[System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdDocument> IsogdDocuments
        {
            get { return GetCollection<IsogdDocument>("IsogdDocuments"); }
        }
        [Association, DisplayName("�����")]
        public XPCollection<Address.Address> Addresses
        {
            get
            {
                return GetCollection<Address.Address>("Addresses");
            }
        }
        [Association, DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBooks
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBooks"); }
        }
        [Association, DisplayName("���")]
        public XPCollection<IsogdPhysStorageBook> PhysStorageBooks
        {
            get { return GetCollection<IsogdPhysStorageBook>("PhysStorageBooks"); }
        }

        private string i_SpecialNote;
        [Size(SizeAttribute.Unlimited), DisplayName("������ �������")]
        public string SpecialNote
        {
            get { return i_SpecialNote; }
            set { SetPropertyValue("SpecialNote", ref i_SpecialNote, value); }
        }

        private CadastralBlock i_CadastralBlockLink;
        [Association, DisplayName("����������� �������")]
        public CadastralBlock CadastralBlockLink
        {
            get { return i_CadastralBlockLink; }
            set { SetPropertyValue("CadastralBlockLink", ref i_CadastralBlockLink, value); }
        }

        [Association(), DisplayName("����������� ���������")]
        public XPCollection<CadastreCostOnDate> �adastreCostOnDate
        {
            get
            {
                return GetCollection<CadastreCostOnDate>("�adastreCostOnDate");
            }
        }

        [Association, DisplayName("����������� � ���� ������")]
        public XPCollection<CadastralNumbers> CadastralNumbers
        {
            get
            {
                return GetCollection<CadastralNumbers>("CadastralNumbers");
            }
        }
        [Association, DisplayName("������� ������������� �� ��������� �������")]
        //[System.ComponentModel.Browsable(true)]
        public XPCollection<CapitalStructureBase> CapitalStructureBase
        {
            get
            {
                return GetCollection<CapitalStructureBase>("CapitalStructureBase");
            }
        }
        [Association, DisplayName("����� ������������� �� ��������� �������")]
        public XPCollection<ConstrStage> ConstrStages
        {
            get
            {
                return GetCollection<ConstrStage>("ConstrStages");
            }
        }
        [Association, DisplayName("������� ��������������� �������")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<Contour> Contours
        {
            get
            {
                return GetCollection<Contour>("Contours");
            }
        }
        [Association, DisplayName("����� �������")]
        public XPCollection<tSubParcel> SubParcels
        {
            get
            {
                return GetCollection<tSubParcel>("SubParcels");
            }
        }
        [Association, DisplayName("��������� �����������������")]
        public XPCollection<Location> Location
        {
            get
            {
                return GetCollection<Location>("Location");
            }
        }
        [Association, DisplayName("��������� �������")]
        public XPCollection<NaturalObject> NaturalObjects
        {
            get
            {
                return GetCollection<NaturalObject>("NaturalObjects");
            }
        }
        [Association, DisplayName("�������� � ������� ��������� ��������")]
        public XPCollection<ParcelNeighbour> ParcelNeighbours
        {
            get
            {
                return GetCollection<ParcelNeighbour>("ParcelNeighbours");
            }
        }
        [Association, DisplayName("���������")]
        public XPCollection<GeneralDocBase> generalDocBase
        {
            get
            {
                return GetCollection<GeneralDocBase>("generalDocBase");
            }
        }
        [Association, DisplayName("����������� (�����������) �����")]
        public XPCollection<tEncumbrance> Encumbrance
        {
            get
            {
                return GetCollection<tEncumbrance>("Encumbrance");
            }
        }


        #region ���������� ������ ��� ��������� ��������� ���������� �����, ���� ��������, ���
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ��� �������� ������ �� �������� � ����� �������� � ���
        /// </summary>
        private void IsogdDocuments_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ��������
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdDocument newIsogdDocument = (IsogdDocument)e.ChangedObject;
                    // ���������, ���� �� ����� �������� � �� � ���, ���� ��� - ���������
                    foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    {
                        bool isExist = false;
                        foreach (IsogdDocument isogdDocument in isogdPhysStorageBook.IsogdDocuments)
                        {
                            if (isogdDocument == newIsogdDocument)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            isogdPhysStorageBook.IsogdDocuments.Add(newIsogdDocument);
                            isogdPhysStorageBook.Save();
                        }
                    }
                    //if (IsogdStorageBook != null)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument isogdDocument in IsogdStorageBook.IsogdDocuments)
                    //    {
                    //        if (isogdDocument == newIsogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        IsogdStorageBook.IsogdDocuments.Add(newIsogdDocument);
                    //        IsogdStorageBook.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ��������
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdDocument newIsogdDocument = (IsogdDocument)e.ChangedObject;
                    // ���������, ���� �� ����� �������� � �� � ���, ���� ���� - �������
                    foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    {
                        bool isExist = false;
                        foreach (IsogdDocument isogdDocument in isogdPhysStorageBook.IsogdDocuments)
                        {
                            if (isogdDocument == newIsogdDocument)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            isogdPhysStorageBook.IsogdDocuments.Remove(newIsogdDocument);
                            isogdPhysStorageBook.Save();
                        }
                    }
                    //if (IsogdStorageBook != null)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument isogdDocument in IsogdStorageBook.IsogdDocuments)
                    //    {
                    //        if (isogdDocument == newIsogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        IsogdStorageBook.IsogdDocuments.Remove(newIsogdDocument);
                    //        IsogdStorageBook.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ������ �� ��� � ����� �������� � ���������
        /// </summary>
        private void PhysStorageBooks_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ���
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdPhysStorageBook newObj = (IsogdPhysStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� ��� � ���������� � ��, ���� ��� - ���������
                    foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    {
                        bool isExist = false;
                        foreach (IsogdPhysStorageBook isogdPhysStorageBook in isogdDocument.PhysStorageBooks)
                        {
                            if (isogdPhysStorageBook == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            isogdDocument.PhysStorageBooks.Add(newObj);
                            isogdDocument.Save();
                        }
                    }
                    //if (IsogdStorageBook != null)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdPhysStorageBook isogdPhysStorageBook in IsogdStorageBook.PhysStorageBooks)
                    //    {
                    //        if (isogdPhysStorageBook == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        IsogdStorageBook.PhysStorageBooks.Add(newObj);
                    //        IsogdStorageBook.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ���
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdPhysStorageBook newObj = (IsogdPhysStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� ��� � ���������� � ��, ���� ���� - �������
                    foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    {
                        bool isExist = false;
                        foreach (IsogdPhysStorageBook isogdPhysStorageBook in isogdDocument.PhysStorageBooks)
                        {
                            if (isogdPhysStorageBook == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            isogdDocument.PhysStorageBooks.Remove(newObj);
                            isogdDocument.Save();
                        }
                    }
                    //if (IsogdStorageBook != null)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdPhysStorageBook isogdPhysStorageBook in IsogdStorageBook.PhysStorageBooks)
                    //    {
                    //        if (isogdPhysStorageBook == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        IsogdStorageBook.PhysStorageBooks.Remove(newObj);
                    //        IsogdStorageBook.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }
        ///// <summary>
        ///// ������� �������� �, ��� �������������, ���������� ������ �� ����� �������� c �� � ���������
        ///// </summary>
        //private void IsogdStorageBooks_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        //{
        //    if (!IsLoading)
        //    {
        //        #region ���� � ��������� �������� ���
        //        if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
        //        {
        //            IsogdStorageBook newObj = (IsogdStorageBook)e.ChangedObject;
        //            // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
        //            foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
        //            {
        //                bool isExist = false;
        //                foreach (IsogdStorageBook isogdStorageBook in isogdDocument.IsogdStorageBooks)
        //                {
        //                    if (isogdStorageBook == newObj)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (!isExist)
        //                {
        //                    isogdDocument.IsogdStorageBooks.Add(newObj);
        //                    isogdDocument.Save();
        //                }
        //            }
        //            foreach (IsogdPhysStorageBook physStorageBook in this.PhysStorageBooks)
        //            {
        //                bool isExist = false;
        //                foreach (IsogdStorageBook isogdStorageBook in physStorageBook.IsogdStorageBooks)
        //                {
        //                    if (isogdStorageBook == newObj)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (!isExist)
        //                {
        //                    physStorageBook.IsogdStorageBooks.Add(newObj);
        //                    physStorageBook.Save();
        //                }
        //            }
        //        }
        //        #endregion

        //        #region ���� �� ��������� ������� ���
        //        if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
        //        {
        //            IsogdStorageBook newObj = (IsogdStorageBook)e.ChangedObject;
        //            // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
        //            foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
        //            {
        //                bool isExist = false;
        //                foreach (IsogdStorageBook isogdStorageBook in isogdDocument.IsogdStorageBooks)
        //                {
        //                    if (isogdStorageBook == newObj)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist)
        //                {
        //                    isogdDocument.IsogdStorageBooks.Remove(newObj);
        //                    isogdDocument.Save();
        //                }
        //            }
        //            foreach (IsogdPhysStorageBook physStorageBook in this.PhysStorageBooks)
        //            {
        //                bool isExist = false;
        //                foreach (IsogdStorageBook isogdStorageBook in physStorageBook.IsogdStorageBooks)
        //                {
        //                    if (isogdStorageBook == newObj)
        //                    {
        //                        isExist = true;
        //                        break;
        //                    }
        //                }
        //                if (isExist)
        //                {
        //                    physStorageBook.IsogdStorageBooks.Remove(newObj);
        //                    physStorageBook.Save();
        //                }
        //            }
        //        }
        //        #endregion
        //    }
        //}


        //protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        //{
        //    XPCollection<T> result = base.CreateCollection<T>(property);
        //    // ���������� ��������� ���������� �����
        //    if (property.Name == "IsogdDocuments")
        //    {
        //        result.CollectionChanged += new XPCollectionChangedEventHandler(IsogdDocuments_CollectionChanged);
        //    }
        //    //���������� ��������� ���
        //    if (property.Name == "PhysStorageBooks")
        //    {
        //        result.CollectionChanged += new XPCollectionChangedEventHandler(PhysStorageBooks_CollectionChanged);
        //    }
        //    ////���������� ��������� ���
        //    //if (property.Name == "IsogdStorageBooks")
        //    //{
        //    //    result.CollectionChanged += new XPCollectionChangedEventHandler(IsogdStorageBooks_CollectionChanged);
        //    //}
        //    return result;
        //}
        #endregion

        /// <summary>
        /// ������� ���������� ���������� ����� � ��� �:
        /// 1. ����� �������� �� �������� ���������� �������
        /// 2. � ������� ��������� ������� �� ����� ��������
        /// </summary>
        //private void GetDataOnSetParecel()
        //{
        //    if (IsogdStorageBook != null)
        //    {
        //        // ��������� ��������� �� ����� �������� � ���� ��������� �������
        //        foreach (IsogdDocument isogdDocument in IsogdStorageBook.IsogdDocuments)
        //        {
        //            bool isExist = false;
        //            foreach (IsogdDocument isogdDocumentThis in IsogdDocuments)
        //            {
        //                if (isogdDocument == isogdDocumentThis)
        //                {
        //                    isExist = true;
        //                    break;
        //                }
        //            }
        //            if (!isExist)
        //                IsogdDocuments.Add(isogdDocument);
        //        }
        //        // ��������� ��������� �� ����� �� � ��
        //        foreach (IsogdDocument isogdDocumentThis in IsogdDocuments)
        //        {
        //            bool isExist = false;
        //            foreach (IsogdDocument isogdDocument in IsogdStorageBook.IsogdDocuments)
        //            {
        //                if (isogdDocument == isogdDocumentThis)
        //                {
        //                    isExist = true;
        //                    break;
        //                }
        //            }
        //            if (!isExist)
        //            {
        //                IsogdStorageBook.IsogdDocuments.Add(isogdDocumentThis);
        //                IsogdStorageBook.Save();
        //            }
        //        }
        //        // ��������� ��� �� �� � ���� �������
        //        foreach (IsogdPhysStorageBook isogdPhysStorageBook in IsogdStorageBook.PhysStorageBooks)
        //        {
        //            bool isExist = false;
        //            foreach (IsogdPhysStorageBook isogdPhysStorageBookThis in PhysStorageBooks)
        //            {
        //                if (isogdPhysStorageBook == isogdPhysStorageBookThis)
        //                {
        //                    isExist = true;
        //                    break;
        //                }
        //            }
        //            if (!isExist)
        //                PhysStorageBooks.Add(isogdPhysStorageBook);
        //        }
        //        // ��������� ��� �� ����� �� � ��
        //        foreach (IsogdPhysStorageBook isogdPhysStorageBookThis in PhysStorageBooks)
        //        {
        //            bool isExist = false;
        //            foreach (IsogdPhysStorageBook isogdPhysStorageBook in IsogdStorageBook.PhysStorageBooks)
        //            {
        //                if (isogdPhysStorageBook == isogdPhysStorageBookThis)
        //                {
        //                    isExist = true;
        //                    break;
        //                }
        //            }
        //            if (!isExist)
        //            {
        //                IsogdStorageBook.PhysStorageBooks.Add(isogdPhysStorageBookThis);
        //                IsogdStorageBook.Save();
        //            }
        //        }
        //    }
        //}

        protected override void OnSaving()
        {
            //GetDataOnSetParecel();
        }
    }
}