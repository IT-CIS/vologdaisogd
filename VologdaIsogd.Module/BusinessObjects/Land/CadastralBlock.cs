using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.SystemDir;
//using AISOGD.Rosreestr;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    /// <summary>
    /// ����������� �������
    /// </summary>
    [ModelDefault("Caption", "����������� �������"), System.ComponentModel.DefaultProperty("CadastralNumber")]
    public class CadastralBlock : tAreaWithoutInaccuracy
    {
        public CadastralBlock(Session session) : base(session) { }

        [Size(255), DisplayName("����������� ����� ��������")]
        public string CadastralNumber { get; set; }

        [Size(255), DisplayName("���� ���������� ������������ ������")]
        public DateTime CadastralNumberDate { get; set; }

        [Size(510), DisplayName("��������������� ������������ ��������")]
        public string AddressString { get; set; }

        [Association, DisplayName("��������� �������")]
        public XPCollection<Parcel> Parcels
        {
            get
            {
                return GetCollection<Parcel>("Parcels");
            }
        }
    }
}