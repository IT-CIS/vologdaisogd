using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "������� � ����������� �� 1 ��. � � ����������� ����������� �������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class tAreaOut : BaseObjectXAF
    {
        public tAreaOut(Session session) : base(session) { }


        private double i_Area;
        [Size(20), DisplayName("�������� �������")]
        public double Area
        {
            get { return i_Area; }
            set { SetPropertyValue("Area", ref i_Area, value); }
        }

        private dUnit i_Unit;
        [DisplayName("������� ���������")]
        public dUnit Unit
        {
            get { return i_Unit; }
            set { SetPropertyValue("Unit", ref i_Unit, value); }
        }

        private double i_Inaccuracy;
        [Size(20), DisplayName("����������� ����������")]
        public double Inaccuracy
        {
            get { return i_Inaccuracy; }
            set { SetPropertyValue("Inaccuracy", ref i_Inaccuracy, value); }
        }

    }
}