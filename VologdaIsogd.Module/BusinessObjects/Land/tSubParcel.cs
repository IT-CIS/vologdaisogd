using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "����� �������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class tSubParcel : tAreaWithoutInaccuracy
    {
        public tSubParcel(Session session) : base(session) { }

        private string i_NumberRecord;
        [Size(40), DisplayName("������� ����� �����")]
        public string NumberRecord
        {
            get { return i_NumberRecord; }
            set { SetPropertyValue("NumberRecord", ref i_NumberRecord, value); }
        }
        private bool i_Full;
        [DisplayName("���� �������")]
        public bool Full
        {
            get { return i_Full; }
            set { SetPropertyValue("Full", ref i_Full, value); }
        }
        private dStates i_State;
        [DisplayName("������ ������")]
        public dStates State
        {
            get { return i_State; }
            set { SetPropertyValue("State", ref i_State, value); }
        }
        private DateTime i_DateExpiry;
        [DisplayName("���� ��������� ���������� ��������� �������� � �����")]
        public DateTime DateExpiry
        {
            get { return i_DateExpiry; }
            set { SetPropertyValue("DateExpiry", ref i_DateExpiry, value); }
        }

        private Parcel i_Parcel;
        [Association, DisplayName("��������� �������")]
        [System.ComponentModel.Browsable(false)]
        public Parcel Parcel
        {
            get { return i_Parcel; }
            set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
        [Association, DisplayName("����������� (�����������) �����")]
        public XPCollection<tEncumbrance> Encumbrance
        {
            get
            {
                return GetCollection<tEncumbrance>("Encumbrance");
            }
        }
    }
}