using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    /// <summary>
    /// ����������� �������
    /// </summary>
    [ModelDefault("Caption", "����������� ��������� (�� ����)"), System.ComponentModel.DefaultProperty("CostValue")]
    public class CadastreCostOnDate  : BaseObjectXAF
    {
        public CadastreCostOnDate(Session session) : base(session) { }

        [Size(255), DisplayName("C��������")]
        public double CostValue { get; set; }

        [Size(255), DisplayName("����")]
        public DateTime CostDate { get; set; }

        private Parcel i_Parcel;
        [Association, DisplayName("��������� ������")]
        public Parcel Parcel
        {
            get { return i_Parcel; }
            set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
    }
}