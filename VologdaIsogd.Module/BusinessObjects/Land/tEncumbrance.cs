using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using AISOGD.General;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "����������� (�����������) �����"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class tEncumbrance : BaseObjectXAF
    {
        public tEncumbrance(Session session) : base(session) { }


        
        private string i_Name;
        [Size(4000), DisplayName("���������� ����������� (�����������) ����")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        private dEncumbrances i_Type;
        [DisplayName("��� �����������")]
        public dEncumbrances Type
        {
            get { return i_Type; }
            set { SetPropertyValue("Type", ref i_Type, value); }
        }
        private string i_AccountNumber;
        [Size(255), DisplayName("������� ����� ����")]
        public string AccountNumber
        {
            get { return i_AccountNumber; }
            set { SetPropertyValue("AccountNumber", ref i_AccountNumber, value); }
        }
        private string i_CadastralNumberRestriction;
        [Size(40), DisplayName("����������� ����� ��, � ������ �������� ���������� ��������")]
        public string CadastralNumberRestriction
        {
            get { return i_CadastralNumberRestriction; }
            set { SetPropertyValue("CadastralNumberRestriction", ref i_CadastralNumberRestriction, value); }
        }
        private DateTime i_Started;
        [DisplayName("���� ������������� ����������� (�����������) ����")]
        public DateTime Started
        {
            get { return i_Started; }
            set { SetPropertyValue("Started", ref i_Started, value); }
        }
        private DateTime i_Stopped;
        [DisplayName("���� ����������� ����������� (�����������) ����")]
        public DateTime Stopped
        {
            get { return i_Stopped; }
            set { SetPropertyValue("Stopped", ref i_Stopped, value); }
        }
        private string i_Term;
        [Size(100), DisplayName("�����������������")]
        public string Term
        {
            get { return i_Term; }
            set { SetPropertyValue("Term", ref i_Term, value); }
        }
        private DateTime i_RegDate;
        [DisplayName("���� ��������������� �����������")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        private string i_RegNumber;
        [Size(100), DisplayName("����� ��������������� �����������")]
        public string RegNumber
        {
            get { return i_RegNumber; }
            set { SetPropertyValue("RegNumber", ref i_RegNumber, value); }
        }

        [Association, DisplayName("����, � ������ ������� �������������� (������������) �����")]
        public XPCollection<Owner> Owners
        {
            get
            {
                return GetCollection<Owner>("Owners");
            }
        }
        //[Association, DisplayName("��������� ���������, �� ��������� �������� �������� ����������� (�����������) �����")]
        //public XPCollection<DocumentWithoutAppliedFile> Documents
        //{
        //    get
        //    {
        //        return GetCollection<DocumentWithoutAppliedFile>("Documents");
        //    }
        //}
        private Parcel i_Parcel;
        [Association, DisplayName("��������� �������")]
        public Parcel Parcel
        {
           get { return i_Parcel; }
           set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
        private tSubParcel i_SubParcel;
        [Association, DisplayName("����� ���������� �������")]
        public tSubParcel SubParcel
        {
            get { return i_SubParcel; }
            set { SetPropertyValue("SubParcel", ref i_SubParcel, value); }
        }
    }
}