using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "����������� �������������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class tUtilization : XPObject
    {
        public tUtilization(Session session) : base(session) { }

        private dUtilizations i_Utilization;
        [DisplayName("��� ������������ ������������� �� ��������������")]
        public dUtilizations Utilization
        {
            get { return i_Utilization; }
            set { SetPropertyValue("Utilization", ref i_Utilization, value); }
        }

        private string i_ByDoc;
        [Size(4000), DisplayName("��� ������������� ������� �� ���������")]
        public string ByDoc
        {
            get { return i_ByDoc; }
            set { SetPropertyValue("ByDoc", ref i_ByDoc, value); }
        }

        //[Association("ParcelUtilization")]
        //public Parcel Parcel;
    }
}