using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "������ ��������������� �������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class Contour : tAreaWithoutInaccuracy
    {
        public Contour(Session session) : base(session) { }

        

        private double i_NumberRecord;
        [Size(10), DisplayName("������� ����� �������")]
        public double NumberRecord
        {
            get { return i_NumberRecord; }
            set { SetPropertyValue("NumberRecord", ref i_NumberRecord, value); }
        }

        private string i_Coords;
        [DisplayName("������������ �������� �������")]
        public string Coords
        {
            get { return i_Coords; }
            set { SetPropertyValue("Coords", ref i_Coords, value); }
        }


        private Parcel i_Parcel;
        [Association, DisplayName("��������� �������")]
        public Parcel Parcel
        {
            get { return i_Parcel; }
            set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
    }
}