using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "��������� �����������������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class Location : BaseObjectXAF
    {
        public Location(Session session) : base(session) { }


        private inBounds i_inBounds;
        [Size(10), DisplayName("� ��������")]
        public inBounds inBounds
        {
            get { return i_inBounds; }
            set { SetPropertyValue("inBounds", ref i_inBounds, value); }
        }
        private string i_Placed;
        [Size(10), DisplayName("��������� �� ���")]
        public string Placed
        {
            get { return i_Placed; }
            set { SetPropertyValue("Placed", ref i_Placed, value); }
        }

        [Association, DisplayName("��������� ��������������")]
        public XPCollection<ElaborationLocation> Elaboration
        {
            get
            {
                return GetCollection<ElaborationLocation>("Elaboration");
            }
        }
        private Parcel i_Parcel;
        [Association, DisplayName("��������� �������")]
        public Parcel Parcel
        {
           get { return i_Parcel; }
            set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
    }
}