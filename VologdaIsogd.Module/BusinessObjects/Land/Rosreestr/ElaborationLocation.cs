using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "��������� ��������������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class ElaborationLocation : BaseObjectXAF
    {
        public ElaborationLocation(Session session) : base(session) { }


        private string i_ReferenceMark;
        [Size(255), DisplayName("������������ ���������")]
        public string ReferenceMark
        {
            get { return i_ReferenceMark; }
            set { SetPropertyValue("ReferenceMark", ref i_ReferenceMark, value); }
        }
        private string i_Distance;
        [Size(255), DisplayName("����������")]
        public string Distance
        {
            get { return i_Distance; }
            set { SetPropertyValue("Distance", ref i_Distance, value); }
        }
        private string i_Direction;
        [Size(255), DisplayName("�����������")]
        public string Direction
        {
            get { return i_Direction; }
            set { SetPropertyValue("Direction", ref i_Direction, value); }
        }
        private Location i_Location;
        [Association, DisplayName("��������� �����������������")]
        public Location Location
        {
            get { return i_Location; }
            set { SetPropertyValue("Location", ref i_Location, value); }
        }
    }
}