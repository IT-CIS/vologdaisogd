using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "�������� � ������� ��������� �������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class ParcelNeighbour : BaseObjectXAF
    {
        public ParcelNeighbour(Session session) : base(session) { }

        private string i_CadastralNumber;
        [Size(40), DisplayName("����������� ����� �������� �������")]
        public string CadastralNumber
        {
            get { return i_CadastralNumber; }
            set { SetPropertyValue("CadastralNumber", ref i_CadastralNumber, value); }
        }
        private string i_ContactAddress;
        [Size(4000), DisplayName("�������� ����� ���������������")]
        public string ContactAddress
        {
            get { return i_ContactAddress; }
            set { SetPropertyValue("ContactAddress", ref i_ContactAddress, value); }
        }
        private string i_Email;
        [Size(100), DisplayName("����� ����������� �����")]
        public string Email
        {
            get { return i_Email; }
            set { SetPropertyValue("Email", ref i_Email, value); }
        }
        
        private Parcel i_Parcel;
        [Association, DisplayName("��������� �������")]
        public Parcel Parcel
        {
           get { return i_Parcel; }
            set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
    }
}