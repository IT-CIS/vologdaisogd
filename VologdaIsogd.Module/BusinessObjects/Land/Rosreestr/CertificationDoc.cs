using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Dictonaries;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "�������� �� ������������� ���������"), System.ComponentModel.DefaultProperty("Name")]
    //[NavigationItem("�������� ����������")]
    public class CertificationDoc : BaseObject//GeneralDocBase
    {
        public CertificationDoc(Session session) : base(session) { }
        
        
        [DisplayName("��� ���������")]
        public CertificationDocType CertificationDocType { get; set; }

        private DateTime i_Date;
        [DisplayName("���� ���������")]
        public DateTime Date
        {
            get { return i_Date; }
            set { SetPropertyValue("Date", ref i_Date, value); }
        }

        private string i_Number;
        [Size(100), DisplayName("����� ���������")]
        public string Number
        {
            get { return i_Number; }
            set { SetPropertyValue("Number", ref i_Number, value); }
        }

        private string i_Organization;
        [Size(500), DisplayName("������������ ������ ������������ �����")]
        public string Organization
        {
            get { return i_Organization; }
            set { SetPropertyValue("Organization", ref i_Organization, value); }
        }

        [Association, DisplayName("����������� ����")]
        public XPCollection<Official> Officials
        {
            get
            {
                return GetCollection<Official>("Officials");
            }
        }

        [Association, DisplayName("�������� � ����������� ���������")]
        public XPCollection<Contractor> Contractors
        {
            get
            {
                return GetCollection<Contractor>("Contractors");
            }
        }

        //[Association, DisplayName("�������� ������ ���������")]
        //public XPCollection<CoordSys> CoordSystems
        //{
        //    get
        //    {
        //        return GetCollection<CoordSys>("CoordSystems");
        //    }
        //}

    }
}