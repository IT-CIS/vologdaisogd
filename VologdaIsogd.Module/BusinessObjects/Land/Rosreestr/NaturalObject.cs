using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "�������� � ��������� ��������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class NaturalObject : BaseObjectXAF
    {
        public NaturalObject(Session session) : base(session) { }


        private dNaturalObjects i_Kind;
        [DisplayName("��� �������")]
        public dNaturalObjects Kind 
        {
            get { return i_Kind; }
            set { SetPropertyValue("Kind", ref i_Kind, value); }
        }
        private dForestUse i_ForestUse;
        [DisplayName("������� ���������� �����")]
        public dForestUse ForestUse
        {
            get { return i_ForestUse; }
            set { SetPropertyValue("ForestUse", ref i_ForestUse, value); }
        }
        private string i_ProtectiveForest;
        [Size(255), DisplayName("��������� �������� �����")]
        public string ProtectiveForest
        {
            get { return i_ProtectiveForest; }
            set { SetPropertyValue("ProtectiveForest", ref i_ProtectiveForest, value); }
        }

        private string i_WaterObject;
        [Size(255), DisplayName("��� ������� �������")]
        public string WaterObject
        {
            get { return i_WaterObject; }
            set { SetPropertyValue("WaterObject", ref i_WaterObject, value); }
        }

        private string i_NameOther;
        [Size(255), DisplayName("������������ ������� ������� � ����� ���������� �������")]
        public string NameOther
        {
            get { return i_NameOther; }
            set { SetPropertyValue("NameOther", ref i_NameOther, value); }
        }
        private string i_CharOther;
        [Size(255), DisplayName("�������������� ����� ���������� �������")]
        public string CharOther
        {
            get { return i_CharOther; }
            set { SetPropertyValue("CharOther", ref i_CharOther, value); }
        }
        [Association, DisplayName("��������� �������")]
        public XPCollection<Parcel> Parcel
        {
            get
            {
                return GetCollection<Parcel>("Parcel");
            }
        }
        
    }
}