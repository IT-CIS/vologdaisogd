using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "����������� ����"), System.ComponentModel.DefaultProperty("Name")]
    public class Official : BaseObjectXAF
    {
        public Official(Session session) : base(session) { }


        private string i_Appointment;
        [Size(255), DisplayName("��������� ���������������")]
        public string Appointment
        {
            get { return i_Appointment; }
            set { SetPropertyValue("Appointment", ref i_Appointment, value); }
        }

        private string i_FamilyName;
        [Size(100), DisplayName("�������")]
        public string FamilyName
        {
            get { return i_FamilyName; }
            set { SetPropertyValue("FamilyName", ref i_FamilyName, value); }
        }

        private string i_FirstName;
        [Size(100), DisplayName("���")]
        public string FirstName
        {
            get { return i_FirstName; }
            set { SetPropertyValue("FirstName", ref i_FirstName, value); }
        }

        private string i_Patronymic;
        [Size(100), DisplayName("��������")]
        public string Patronymic
        {
            get { return i_Patronymic; }
            set { SetPropertyValue("Patronymic", ref i_Patronymic, value); }
        }

        [Association, DisplayName("�������� ����������")]
        public XPCollection<CertificationDoc> CertificationDocs
        {
            get
            {
                return GetCollection<CertificationDoc>("CertificationDocs");
            }
        }
    }
}