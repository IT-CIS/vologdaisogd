﻿//using AISOGD.Constr;
//using AISOGD.Constr;
using AISOGD.General;
using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AISOGD.SystemDir;

namespace AISOGD.Land
{
    public class DocumentWithoutAppliedFile : BaseObjectXAF
    {
        public DocumentWithoutAppliedFile(Session session) : base(session) { }


        private dAllDocuments i_CodeDocument;
        [DisplayName("Код документа")]
        public dAllDocuments CodeDocument
        {
            get { return i_CodeDocument; }
            set { SetPropertyValue("CodeDocument", ref i_CodeDocument, value); }
        }
        private string i_Name;
        [Size(500), DisplayName("Наименование документа")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        private string i_Series;
        [Size(255), DisplayName("Серия документа")]
        public string Series
        {
            get { return i_Series; }
            set { SetPropertyValue("Series", ref i_Series, value); }
        }
        private string i_Number;
        [Size(255), DisplayName("Номер документа")]
        public string Number
        {
            get { return i_Number; }
            set { SetPropertyValue("Number", ref i_Number, value); }
        }
        private DateTime i_Date;
        [DisplayName("Дата выдачи (подписания) документа")]
        public DateTime Date
        {
            get { return i_Date; }
            set { SetPropertyValue("Date", ref i_Date, value); }
        }

        private string i_IssueOrgan;
        [Size(500), DisplayName("Организация, выдавшая документ. Автор документа")]
        public string IssueOrgan
        {
            get { return i_IssueOrgan; }
            set { SetPropertyValue("IssueOrgan", ref i_IssueOrgan, value); }
        }

        private string i_Desc;
        [Size(1000), DisplayName("Особые отметки")]
        public string Desc
        {
            get { return i_Desc; }
            set { SetPropertyValue("Desc", ref i_Desc, value); }
        }

        //private Right i_Right;
        //[Association, DisplayName("Правобладание")]
        //public Right Right
        //{
        //    get { return i_Right; }
        //    set { SetPropertyValue("Right", ref i_Right, value); }
        //}

        //private tEncumbrance i_tEncumbrance;
        //[Association, DisplayName("Ограничение (обременение) права")]
        //public tEncumbrance tEncumbrance
        //{
        //    get { return i_tEncumbrance; }
        //    set { SetPropertyValue("tEncumbrance", ref i_tEncumbrance, value); }
        //}

        //[Association, DisplayName("Земельные участки")]
        //public XPCollection<Lot> Lots
        //{
        //    get
        //    {
        //        return GetCollection<Lot>("Lots");
        //    }
        //}
        

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}
