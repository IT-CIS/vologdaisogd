using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;
using AISOGD.General;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "�������� � ����������� ���������, ����������� ����������� ������ "),
    System.ComponentModel.DefaultProperty("NCertificate")]
    //[NavigationItem("�������� ����������")]
    public class Contractor : AttachBase
    {
        public Contractor(Session session) : base(session) { }

        private DateTime i_Date;
        [DisplayName("���� ���������� ����������� �����")]
        public DateTime Date
        {
            get { return i_Date; }
            set { SetPropertyValue("Date", ref i_Date, value); }
        }

        /// <summary>
        /// ����������� �������
        /// </summary>
        private string i_FamilyName;
        [Size(100), DisplayName("�������")]
        public string FamilyName
        {
            get { return i_FamilyName; }
            set { SetPropertyValue("FamilyName", ref i_FamilyName, value); }
        }

        private string i_FirstName;
        [Size(100), DisplayName("���")]
        public string FirstName
        {
            get { return i_FirstName; }
            set { SetPropertyValue("FirstName", ref i_FirstName, value); }
        }

        private string i_Patronymic;
        [Size(100), DisplayName("��������")]
        public string Patronymic
        {
            get { return i_Patronymic; }
            set { SetPropertyValue("Patronymic", ref i_Patronymic, value); }
        }


        private string i_NCertificate;
        [Size(50), DisplayName("����� ����������������� ��������� ������������ ��������")]
        public string NCertificate
        {
            get { return i_NCertificate; }
            set { SetPropertyValue("NCertificate", ref i_NCertificate, value); }
        }


        private string i_Organization;
        [Size(255), DisplayName("����������� ����")]
        public string Organization
        {
            get { return i_Organization; }
            set { SetPropertyValue("Organization", ref i_Organization, value); }
        }

        [Association, DisplayName("�������� ����������")]
        public XPCollection<CertificationDoc> CertificationDocs
        {
            get
            {
                return GetCollection<CertificationDoc>("CertificationDocs");
            }
        }
    }
}