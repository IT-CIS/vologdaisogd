using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
//using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Land
{
    [ModelDefault("Caption", "����������� ������"), 
    System.ComponentModel.DefaultProperty("Name")]
    public class CadastralNumbers : BaseObjectXAF
    {
        public CadastralNumbers(Session session) : base(session) { }

        private CadastralNumbersType i_CadastralNumbersType;
        [DisplayName("��� ������������ ������")]
        public CadastralNumbersType CadastralNumbersType
        {
            get { return i_CadastralNumbersType; }
            set { SetPropertyValue("CadastralNumbersType", ref i_CadastralNumbersType, value); }
        }

        private dOldNumberType i_OldNumberType;
        [DisplayName("��� ����� ������������ ������")]
        public dOldNumberType OldNumberType
        {
            get { return i_OldNumberType; }
            set { SetPropertyValue("OldNumberType", ref i_OldNumberType, value); }
        }

        private string i_CadastralNumber;
        [Size(500), DisplayName("�����")]
        public string CadastralNumber
        {
            get { return i_CadastralNumber; }
            set { SetPropertyValue("CadastralNumber", ref i_CadastralNumber, value); }
        }
        private Parcel i_Parcel;
        [Association, DisplayName("��������� �������")]
        public Parcel Parcel
        {
            get { return i_Parcel; }
            set { SetPropertyValue("Parcel", ref i_Parcel, value); }
        }
    }
}