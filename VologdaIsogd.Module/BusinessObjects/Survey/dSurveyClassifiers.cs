using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Surveys
{

    /// <summary>
    /// ������� ����� ������������ ���������
    /// </summary>
    [ModelDefault("Caption", "������� ����� ������������ ���������")]
    public class dSurveyClassifiers : BaseObjectXAF
    {
        public dSurveyClassifiers(Session session) : base(session) { }

        [Size(8), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }

    /// <summary>
    /// ��� ���������
    /// </summary>
    [NavigationItem("���������� ���������")]
    [ModelDefault("Caption", "��� ���������")]
    public class dSurveyKind : dSurveyClassifiers
    {
        public dSurveyKind(Session session) : base(session) { }

        //[Size(8), DisplayName("���")]
        //public string Code { get; set; }

        //[Size(255), DisplayName("������������")]
        //public string Name { get; set; }
    }

    /// <summary>
    /// ��� �������������� �����
    /// </summary>
    [NavigationItem("���������� ���������")]
    [ModelDefault("Caption", "��� �������������� �����")]
    public class dSurveyingJobType : dSurveyClassifiers
    {
        public dSurveyingJobType(Session session) : base(session) { }

        //[Size(8), DisplayName("���")]
        //public string Code { get; set; }

        //[Size(255), DisplayName("������������")]
        //public string Name { get; set; }
    }

    /// <summary>
    /// ����� ������������� ���������������� ����������
    /// </summary>
    [NavigationItem("���������� ���������")]
    [ModelDefault("Caption", "����� ������������� ���������������� ����������")]
    public class dMapMaterialForm : dSurveyClassifiers
    {
        public dMapMaterialForm(Session session) : base(session) { }

        //[Size(8), DisplayName("���")]
        //public string Code { get; set; }

        //[Size(255), DisplayName("������������")]
        //public string Name { get; set; }
    }
}