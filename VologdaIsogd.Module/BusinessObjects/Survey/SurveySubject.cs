using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.General;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using AISOGD.DocFlow;

namespace AISOGD.Surveys
{
    [System.ComponentModel.DefaultProperty("Name")]
    [NavigationItem("���������� ���������")]
    [ModelDefault("Caption", "�������������� �����������")]
    public class SurveySubject : GeneralSubject
    {
        public SurveySubject(Session session) : base(session) { }


        [Association, DisplayName("�������������")]
        public XPCollection<SurveyCertificate> SurveyCertificates
        {
            get { return GetCollection<SurveyCertificate>("SurveyCertificates"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}