using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.General;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.Isogd;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;
//using AISOGD.DocFlow;

namespace AISOGD.Surveys
{
    [System.ComponentModel.DefaultProperty("Name")]
    [NavigationItem("���������� ���������")]
    [ModelDefault("Caption", "���������� ���������")]
    public class Survey : AttachBase
    {
        public Survey(Session session) : base(session) { }

        
        private eSurveyStatus i_SurveyStatus;
        [DisplayName("������ ���������")]
        public eSurveyStatus SurveyStatus
        {
            get { return i_SurveyStatus; }
            set { SetPropertyValue("SurveyStatus", ref i_SurveyStatus, value); }
        }

        private string i_ArhNo;
        [Size(16), DisplayName("�������� �����")]
        public string ArhNo
        {
            get { return i_ArhNo; }
            set { SetPropertyValue("ArhNo", ref i_ArhNo, value); }
        }
        private string i_InvNo;
        [Size(16), DisplayName("����������� �����")]
        public string InvNo
        {
            get { return i_InvNo; }
            set { SetPropertyValue("InvNo", ref i_InvNo, value); }
        }

        private dSurveyKind i_SurveyKind;
        [DisplayName("��� ���������")]
        public dSurveyKind SurveyKind
        {
            get { return i_SurveyKind; }
            set { SetPropertyValue("SurveyKind", ref i_SurveyKind, value); }
        }

        private string i_SurveyName;
        [Size(255), DisplayName("������������ ������")]
        public string SurveyName
        {
            get { return i_SurveyName; }
            set { SetPropertyValue("SurveyName", ref i_SurveyName, value); }
        }

        private string i_ObjectAddress;
        [Size(255), DisplayName("��������������")]
        public string ObjectAddress
        {
            get { return i_ObjectAddress; }
            set { SetPropertyValue("ObjectAddress", ref i_ObjectAddress, value); }
        }

        private string i_SurveyObject;
        [Size(255), DisplayName("������ ���������")]
        public string SurveyObject
        {
            get { return i_SurveyObject; }
            set { SetPropertyValue("SurveyObject", ref i_SurveyObject, value); }
        }

        private dSurveyingJobType i_SurveyingJobType;
        [DisplayName("��� (����) �������������� �����")]
        public dSurveyingJobType SurveyingJobType
        {
            get { return i_SurveyingJobType; }
            set { SetPropertyValue("SurveyingJobType", ref i_SurveyingJobType, value); }
        }

        private string i_RequesterString;
        [Size(255), DisplayName("���������(�����)")]
        public string RequesterString
        {
            get {
                try {
                    string res = "";
                    if (GeneralSubjects.Count > 0)
                        foreach (GeneralSubject subj in this.GeneralSubjects)
                        {
                            if (res == null || res == "")
                                res = subj.FullName;
                            else
                                res += ", " + subj.FullName;
                        }
                    i_RequesterString = res;
                }
                catch { }
                return i_RequesterString; }
            set { SetPropertyValue("RequesterString", ref i_RequesterString, value); }
        }

        private SurveySubject i_SurveySubject;
        [DisplayName("�����������")]
        public SurveySubject SurveySubject
        {
            get { return i_SurveySubject; }
            set { SetPropertyValue("SurveySubject", ref i_SurveySubject, value); }
        }

        private int i_SurveyYear;
        [DisplayName("��� ���������� ���������")]
        public int SurveyYear
        {
            get { return i_SurveyYear; }
            set { SetPropertyValue("SurveyYear", ref i_SurveyYear, value); }
        }

        private string i_SurveyDocs;
        [Size(500), DisplayName("���������������� ��������")]
        public string SurveyDocs
        {
            get { return i_SurveyDocs; }
            set { SetPropertyValue("SurveyDocs", ref i_SurveyDocs, value); }
        }

        private DateTime i_RegDate;
        [DisplayName("���� �������� � ����")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }

        // ��������

        private double i_Elevation;
        [DisplayName("���������� �������, �")]
        public double Elevation
        {
            get { return i_Elevation; }
            set { SetPropertyValue("Elevation", ref i_Elevation, value); }
        }

        private double i_DrillingDepth;
        [DisplayName("������� �������, �")]
        public double DrillingDepth
        {
            get { return i_DrillingDepth; }
            set { SetPropertyValue("DrillingDepth", ref i_DrillingDepth, value); }
        }

        private double i_MoundPower;
        [DisplayName("�������� ������, �")]
        public double MoundPower
        {
            get { return i_MoundPower; }
            set { SetPropertyValue("MoundPower", ref i_MoundPower, value); }
        }

        private double i_PeatPower;
        [DisplayName("�������� �����, �")]
        public double PeatPower
        {
            get { return i_PeatPower; }
            set { SetPropertyValue("PeatPower", ref i_PeatPower, value); }
        }

        private double i_MoraineDepth;
        [DisplayName("������� ������, �")]
        public double MoraineDepth
        {
            get { return i_MoraineDepth; }
            set { SetPropertyValue("MoraineDepth", ref i_MoraineDepth, value); }
        }

        private double i_MoraineElevation;
        [DisplayName("���������� ������� ������, �")]
        public double MoraineElevation
        {
            get { return i_MoraineElevation; }
            set { SetPropertyValue("MoraineElevation", ref i_MoraineElevation, value); }
        }

        private double i_WaterDepth;
        [DisplayName("������� ����, �")]
        public double WaterDepth
        {
            get { return i_WaterDepth; }
            set { SetPropertyValue("WaterDepth", ref i_WaterDepth, value); }
        }

        private string i_DZUProjectText;
        [Size(255), DisplayName("������ ���")]
        public string DZUProjectText
        {
            get {
                try
                {
                    string res = "";
                    if (IsogdPhysStorageBooks.Count > 0)
                        foreach (IsogdPhysStorageBook book in this.IsogdPhysStorageBooks)
                        {
                            if (res == null || res == "")
                                res = book.BookNo;
                            else
                                res += ", " + book.BookNo;
                        }
                    i_DZUProjectText = res;
                }
                catch { }
                return i_DZUProjectText; }
            set { SetPropertyValue("DZUProjectText", ref i_DZUProjectText, value); }
        }

        private string i_Notes;
        [Size(1000), DisplayName("����������")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }

        private string i_SurveyLocation;
        [Size(1000), DisplayName("���������������")]
        public string SurveyLocation
        {
            get { return i_SurveyLocation; }
            set { SetPropertyValue("SurveyLocation", ref i_SurveyLocation, value); }
        }

        private Employee i_Employee;
        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        
        private dMapMaterialForm i_MapMaterialForm;
        [DisplayName("������ ��������������� ����������")]
        public dMapMaterialForm MapMaterialForm
        {
            get { return i_MapMaterialForm; }
            set { SetPropertyValue("MapMaterialForm", ref i_MapMaterialForm, value); }
        }
        //AutoCadFlag: boolean ��������� � ��������
        //ElectronicFormFlag: boolean ����� � ����������� ����
        //FullInfoFlag: boolean �������� �������� ���������

        [Association, DisplayName("���������")]
        public XPCollection<GeneralSubject> GeneralSubjects
        {
            get { return GetCollection<GeneralSubject>("GeneralSubjects"); }
        }
        [Association, DisplayName("���")]
        public XPCollection<IsogdPhysStorageBook> IsogdPhysStorageBooks
        {
            get { return GetCollection<IsogdPhysStorageBook>("IsogdPhysStorageBooks"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);

            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            RegDate = DateTime.Now.Date;
            this.SurveyStatus = eSurveyStatus.�����;
             
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsDeleted)
            {
                //''
                try
                {
                    if (ArhNo == "" || ArhNo == null)
                    {
                        int initialNo = 0;
                        if (DateTime.Now.Year == 2016)
                            initialNo = 14;
                        ArhNo = Convert.ToString(initialNo + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + DateTime.Now.Year.ToString(), string.Empty));
                        if (ArhNo.Length == 1)
                            ArhNo = DateTime.Now.Year.ToString() + "00" + ArhNo;
                        if (ArhNo.Length == 2)
                            ArhNo = DateTime.Now.Year.ToString() + "0" + ArhNo;
                        //else
                        //    ArhNo = DateTime.Now.Year.ToString() + ArhNo;
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// ������� ���������� ��������� � ��� ������� ��������� �� ����������� �������
        /// </summary>
        public void FillSurveySemantic(IGISApplication GisApp, string aLayer, string mapObjectId)
        {
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_�����", ArhNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_������", SurveyKind.Name);
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_�����", SurveyKind.Name);
            GisApp.SetFieldValue(aLayer, mapObjectId, "�����_��_����������", SurveyName);
            GisApp.SetFieldValue(aLayer, mapObjectId, "�����", SurveyName);
            GisApp.SetFieldValue(aLayer, mapObjectId, "��������������", ObjectAddress);
            GisApp.SetFieldValue(aLayer, mapObjectId, "������", SurveyObject);
            GisApp.SetFieldValue(aLayer, mapObjectId, "������_�����", SurveyObject);
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_�����", SurveyingJobType.Name);
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_�����", SurveyingJobType.Name);
            GisApp.SetFieldValue(aLayer, mapObjectId, "��������", RequesterString);
            GisApp.SetFieldValue(aLayer, mapObjectId, "�����������", SurveySubject.FullName);
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_���������", SurveyYear.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_����_�_����", RegDate.ToShortDateString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "����������", Notes);

            //��������
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_���", Elevation.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_�������", DrillingDepth.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_������", MoundPower.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_�����", PeatPower.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "��_������", MoraineDepth.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_���_������", MoraineElevation.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_����", WaterDepth.ToString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "������_���", DZUProjectText);

            GisApp.CommitLayer(aLayer);

        }
    }
}