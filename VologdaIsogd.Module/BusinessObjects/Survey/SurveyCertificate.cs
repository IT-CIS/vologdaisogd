using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.General;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using AISOGD.DocFlow;

namespace AISOGD.Surveys
{
    [System.ComponentModel.DefaultProperty("Name")]
    //[NavigationItem("���������� ���������")]
    [ModelDefault("Caption", "�������������")]
    public class SurveyCertificate : AttachBase
    {
        public SurveyCertificate(Session session) : base(session) { }

       

        private SurveySubject i_SurveySubject;
        [Association, DisplayName("�����������")]
        public SurveySubject SurveySubject
        {
            get { return i_SurveySubject; }
            set { SetPropertyValue("SurveySubject", ref i_SurveySubject, value); }
        }

        private string i_CertificateNo;
        [DisplayName("�����")]
        public string CertificateNo
        {
            get { return i_CertificateNo; }
            set { SetPropertyValue("CertificateNo", ref i_CertificateNo, value); }
        }

        private DateTime i_CertificateDate;
        [DisplayName("���� ������")]
        public DateTime CertificateDate
        {
            get { return i_CertificateDate; }
            set { SetPropertyValue("CertificateDate", ref i_CertificateDate, value); }
        }

        private string i_CertificateDateString;
        [Size(255), DisplayName("���� ��������")]
        public string CertificateDateString
        {
            get
            {
                return i_CertificateDateString;
            }
            set { SetPropertyValue("CertificateDateString", ref i_CertificateDateString, value); }
        }

        private string i_SROName;
        [Size(255), DisplayName("������������ ���, �����")]
        public string SROName
        {
            get
            {
                return i_SROName;
            }
            set { SetPropertyValue("SROName", ref i_SROName, value); }
        }

        private string i_SROSite;
        [Size(255), DisplayName("���� ���")]
        public string SROSite
        {
            get
            {
                return i_SROSite;
            }
            set { SetPropertyValue("SROSite", ref i_SROSite, value); }
        }

        private string i_SROAddress;
        [Size(500), DisplayName("�����, �������� ���")]
        public string SROAddress
        {
            get
            {
                return i_SROAddress;
            }
            set { SetPropertyValue("SROAddress", ref i_SROAddress, value); }
        }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}