﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{


    [ModelDefault("Caption", "Вид входящего обращения"), System.ComponentModel.DefaultProperty("RequesterString")]
    public class DocSubject : BaseObjectXAF
    {
        public DocSubject(Session session) : base(session) { }

        private string i_requesterString;
        private Org i_org;
        private AISOGD.Subject.Person i_person;
        private eSubjectType i_requesterType;

        [DisplayName("Тип заказчика")]
        [ImmediatePostData]
        public eSubjectType RequesterType
        {
            get { return i_requesterType; }
            set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        }
        [DisplayName("Заказчик")]
        [ImmediatePostData]
        [Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'физ'", Context = "DetailView")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [DisplayName("Заказчик")]
        [ImmediatePostData]
        [Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'юр'", Context = "DetailView")]
        public AISOGD.Subject.Person RequesterPerson
        {
            get { return i_person; }
            set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        }

        [Size(255), DisplayName("Описание заказчика")]
        public string RequesterString
        {
            get
            {
                //try
                //{
                i_requesterString = GetRequesterInfo();
                //}
                //catch { }
                return i_requesterString;
            }
            set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        }
        private string i_RequesterAddress;
        [Size(255), DisplayName("Адрес заказчика")]
        public string RequesterAddress
        {
            get
            {
                //try
                //{
                i_RequesterAddress = GetRequesterAddress();
                //}
                //catch { }
                return i_RequesterAddress;
            }
            set { SetPropertyValue("RequesterAddress", ref i_RequesterAddress, value); }
        }
        public string GetRequesterInfo()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.FullName;
                //if (i_org.Info.Length > 0)
                //    result += ", " + i_org.Info;
            }
            if (i_person != null)
            {
                result = i_person.FullName;
                //if (i_person.Info.Length > 0)
                //    result += ", " + i_person.Info;
            }
            return result;
        }
        public string GetRequesterAddress()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.ContactAddress;
                //if (i_org.ContactAddress.Length > 0)
                //    result += ", " + i_org.Info;
            }
            if (i_person != null)
            {
                result = i_person.ContactAddress;
                //if (i_person.Info.Length > 0)
                //    result += ", " + i_person.Info;
            }
            return result;
        }
        [Association, DisplayName("Входящее обращение")]
        [System.ComponentModel.Browsable(false)]
        public InLetter InLetter { get; set; }

        [Association, System.ComponentModel.Browsable(false)]
        public OutLetter OutLetter { get; set; }
    }


}