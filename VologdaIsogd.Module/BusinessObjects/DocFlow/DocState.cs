﻿using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Состояние документа"), System.ComponentModel.DefaultProperty("docStateKind")]
    public class GeneralDocState : BaseObjectXAF
    {
        public GeneralDocState(Session session)
            : base(session) { }

        private DocStateKind _DocStateKind;
        private DateTime _StateDateChange;
        private Employee _Executor;
        private string _Description;
        [DisplayName("Стадия")]
        public DocStateKind docStateKind
        {
            get { return _DocStateKind; }
            set { try { SetPropertyValue("docStateKind", ref _DocStateKind, value); } catch { } }
        }
        [DisplayName("Дата изменения состояния")]
        public DateTime stateDateChange
        {
            get { return _StateDateChange; }
            set { SetPropertyValue("stateDateChange", ref _StateDateChange, value); }
        }
        [DisplayName("Исполнитель")]
        public Employee executor
        {
            get { return _Executor; }
            set { SetPropertyValue("executor", ref _Executor, value); }
        }
        [Size(4000), DevExpress.Xpo.DisplayName("Примечание")]
        public string description
        {
            get { return _Description; }
            set { SetPropertyValue("description", ref _Description, value); }
        }

        private GeneralDocBase _generalDocBase;
        [Association, DisplayName("Документ")]
        public GeneralDocBase generalDocBase
        {
            get { return _generalDocBase; }
            set { SetPropertyValue("generalDocBase", ref _generalDocBase, value); }
        }

    }

    public enum DocStateKind
    {
        ПроверкаКомплектности = 0, Принято = 1, Отклонено = 2, Отозван = 3, Отказ = 4,
        ЧастичноПодготовлен = 5, ОтправленНаДоработку = 6, Исправлен = 7, Подготовлен = 8, Согласование = 9,
        Согласован = 10, НаПодписи = 11, Утвержден = 12, Подписан = 13, Выдано = 14
    };
}
