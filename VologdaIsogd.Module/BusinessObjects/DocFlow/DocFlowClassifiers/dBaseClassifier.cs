using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{

    /// <summary>
    /// ������� ����� ������������
    /// </summary>
    [ModelDefault("Caption", "������� ����� ������������"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]
    public class dBaseClassifierDocFlow : BaseObjectXAF
    {
        public dBaseClassifierDocFlow(Session session) : base(session) { }

        [Size(64), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
    

}