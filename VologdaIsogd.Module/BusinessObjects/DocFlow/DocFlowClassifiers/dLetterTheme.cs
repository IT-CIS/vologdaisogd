﻿using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Предмет обращения"), NavigationItem("Документооборот"), System.ComponentModel.DefaultProperty("Name")]
    public class dLetterTheme : dBaseClassifierDocFlow
    {
        public dLetterTheme(Session session)
            : base(session) { }
        private string _Name;

        private int i_ExecuteDays;
        [DisplayName("Дней на исполнение")]
        public int ExecuteDays
        {
            get { return i_ExecuteDays; }
            set { SetPropertyValue("ExecuteDays", ref i_ExecuteDays, value); }
        }
    }
}
