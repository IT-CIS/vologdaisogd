﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Вид документа"), NavigationItem("Документооборот"), System.ComponentModel.DefaultProperty("Name")]
    public class dDocKind : dBaseClassifierDocFlow
    {
        public dDocKind(Session session)
            : base(session) { }
        private string _Name;

        
        private int i_InitialRegNo;
        [DisplayName("Начало нумерации номеров")]
        public int InitialRegNo
        {
            get
            {
                return i_InitialRegNo;
            }
            set { SetPropertyValue("InitialRegNo", ref i_InitialRegNo, value); }

        }

        private string i_Prefix;
        [DisplayName("Префикс номера")]
        public string Prefix
        {
            get
            {
                return i_Prefix;
            }
            set { SetPropertyValue("Prefix", ref i_Prefix, value); }
        }
        private string i_Suffix;
        [DisplayName("Суффикс номера")]
        public string Suffix
        {
            get
            {
                return i_Suffix;
            }
            set { SetPropertyValue("Suffix", ref i_Suffix, value); }
        }

    }
}
