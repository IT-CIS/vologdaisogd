﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{


    [ModelDefault("Caption", "Вид исходящего"), System.ComponentModel.DefaultProperty("Name")]
    [NavigationItem("Документооборот")]
    public class dOutLetterKind : dBaseClassifierDocFlow
    {
        public dOutLetterKind(Session session) : base(session) { }
    }


}