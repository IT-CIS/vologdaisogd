﻿using AISOGD.Constr;
using AISOGD.Enums;
using AISOGD.General;
//using AISOGD.Constr;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.DocFlow
{
    [NavigationItem("Документооборот")]
    public class GeneralDocBase : AttachBase
    {
        public GeneralDocBase() : base() { }
        public GeneralDocBase(Session session) : base(session) { }

        private DateTime i_DocDate;
        private Employee i_Employee;
        private Signer _Signer;
        private dDocKind i_DocKind;

        [DisplayName("Вид документа")]
        public dDocKind DocKind
        {
            get { return i_DocKind; }
            set { SetPropertyValue("DocKind", ref i_DocKind, value); }
        }

        private eDocStatus i_EDocStatus;
        [DisplayName("Статус документа")]
        public eDocStatus EDocStatus
        {
            get { return i_EDocStatus; }
            set { SetPropertyValue("EDocStatus", ref i_EDocStatus, value); }
        }
        private string i_DocNo;
        [Size(64), DisplayName("Номер документа")]
        public string DocNo
        {
            get { return i_DocNo; }
            set { SetPropertyValue("DocNo", ref i_DocNo, value); }
        }
        private string i_FullDocNo;
        [Size(255), DisplayName("Полный номер документа")]
        public string FullDocNo
        {
            get { return i_FullDocNo; }
            set { SetPropertyValue("FullDocNo", ref i_FullDocNo, value); }
        }

        [DisplayName("Дата документа")]
        public DateTime DocDate
        {
            get { return i_DocDate; }
            set { SetPropertyValue("DocDate", ref i_DocDate, value); }
        }

        private string i_content;
        [Size(1000), DisplayName("Содержание документа")]
        public string Content
        {
            get { return i_content; }
            set { SetPropertyValue("Content", ref i_content, value); }
        }
        [DisplayName("Сотрудник, подготовивший документ")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [DisplayName("За подписью"), VisibleInListView(false)]
        public Signer signer
        {
            get { return _Signer; }
            set { try { SetPropertyValue("Signer", ref _Signer, value); } catch { } }
        }

        [Association, DisplayName("Заявление")]
        [DataSourceProperty("AvailableLetters")]
        public XPCollection<InLetter> InLetter
        {
            get { return GetCollection<InLetter>("InLetter"); }
        }

        private XPCollection<InLetter> availableLetters;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<InLetter> AvailableLetters
        {
            get
            {
                if (availableLetters == null)
                {
                    // Retrieve all Terminals objects 
                    availableLetters = new XPCollection<InLetter>(Session);
                    if(DocKind!=null)
                    {
                        Connect connect = Connect.FromSession(Session);
                        if (DocKind.Name != null && DocKind.Name != "")
                        {
                            if (DocKind.Name == "Разрешение на строительство")
                            {
                                //foreach (InLetter lt in connect.FindObjects<InLetter>(x => true))
                                //{
                                //    if (lt.InLetterStatus == eInLetterStatus.Новое)
                                //    {
                                //        if (lt.letterTheme.Name == "Заявление на подготовку разрешения на строительство" ||
                                //            lt.letterTheme.Name == "Заявление на продление разрешения на строительство" ||
                                //            lt.letterTheme.Name == "Заявление на прекращение делопроизводства по разрешению на строительство")
                                //            availableLetters.Add(lt);
                                //    }
                                //    else
                                //        if (lt.GeneralDocBase.Count > 0)
                                //    {
                                //        if (lt.GeneralDocBase[0] == this)
                                //            availableLetters.Add(lt);
                                //    }
                                //}
                                availableLetters.Criteria = new GroupOperator(GroupOperatorType.And,
                                    // new GroupOperator(GroupOperatorType.Or,
                                    //new BinaryOperator("InLetterStatus", 0),
                                    //new BinaryOperator("GeneralDocBase[0]", this)
                                    //),
                                    new BinaryOperator("RegDate", DateTime.Now.AddMonths(-2).Date, BinaryOperatorType.GreaterOrEqual),
                                    new GroupOperator(GroupOperatorType.Or, new BinaryOperator("InLetterStatus", 0), new BinaryOperator("InLetterStatus", 2)),
                                    //new BinaryOperator("InLetterStatus", 0),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("letterTheme.Name", "Заявка на разрешение на строительство (обр. граждан, ДГ)"), 
                                    new BinaryOperator("letterTheme.Name", "Заявка вх. на разрешение на строительство (ДГ)"),
                                    new BinaryOperator("letterTheme.Name", "Заявка о внесении измен. в разреш. на строит (обр.гр., ДГ)"),
                                    new BinaryOperator("letterTheme.Name", "Заявка вх. о внесении изменений на разреш. на строит-во (ДГ)"), 
                                    new BinaryOperator("letterTheme.Name", "Заявление на прекращение делопроизводства по разрешению на строительство"), 
                                    new BinaryOperator("letterTheme.Name", "Заявка на продл. срока действ. разр.на строит (обр.гр., ДГ)"),
                                    new BinaryOperator("letterTheme.Name", "Заявка вх. на продл.срока действия разреш. на строительсво (ДГ)")
                                    
                                    ));
                            }
                            else
                                  if (DocKind.Name == "Разрешение на ввод объекта в эксплуатацию")
                            {
                                availableLetters.Criteria = new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("RegDate", DateTime.Now.AddMonths(-2).Date, BinaryOperatorType.GreaterOrEqual),
                                    new GroupOperator(GroupOperatorType.Or, new BinaryOperator("InLetterStatus", 0), new BinaryOperator("InLetterStatus", 2)),
                                    //new BinaryOperator("InLetterStatus", 0),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("letterTheme.Name", "Заявка на разрешение на ввод в эксплуат (обр. граждан, ДГ)"),
                                    new BinaryOperator("letterTheme.Name", "Заявка вх. на разрешение на ввод в эксплуатац. объекта (ДГ)")
                                    ));
                            }
                            else
                                  if (DocKind.Name == "Уведомление о строительстве объекта индивидуального жилищного строительства")
                            {
                                availableLetters.Criteria = new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("RegDate", DateTime.Now.AddMonths(-2).Date, BinaryOperatorType.GreaterOrEqual),
                                    new GroupOperator(GroupOperatorType.Or, new BinaryOperator("InLetterStatus", 0), new BinaryOperator("InLetterStatus", 2)),
                                    //new BinaryOperator("InLetterStatus", 0),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("letterTheme.Name", "Уведомление о планируемом строит./реконструц.объекта (обр.гр,ДГ)")
                                    ));
                            }
                            else
                                  if (DocKind.Name == "Уведомление о вводе объекта индивидуального жилищного строительства")
                            {
                                availableLetters.Criteria = new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("RegDate", DateTime.Now.AddMonths(-2).Date, BinaryOperatorType.GreaterOrEqual),
                                    new GroupOperator(GroupOperatorType.Or, new BinaryOperator("InLetterStatus", 0), new BinaryOperator("InLetterStatus", 2)),
                                    //new BinaryOperator("InLetterStatus", 0),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("letterTheme.Name", "Уведомление о постр./реконстр. постр. объекте (обр.гр,ДГ)")
                                    ));
                            }
                            else
                                  if (DocKind.Name == "Градостроительный план земельного участка" || DocKind.Name == "Приказ об утверждении градостроительного плана")
                            {
                                availableLetters.Criteria = new GroupOperator(GroupOperatorType.And,
                                     new BinaryOperator("InLetterStatus", 0),
                                     //new GroupOperator(GroupOperatorType.Or, new BinaryOperator("InLetterStatus", 0), new BinaryOperator("InLetterStatus", 2)),
                                    new GroupOperator(GroupOperatorType.Or,
                                    new BinaryOperator("letterTheme.Name", "Заявка на выдачу градоплана земельного участка(обр.гр.ДГ)"),
                                    new BinaryOperator("letterTheme.Name", "Заявка вх. на выдачу градоплана земельного участка (ДГ)"))
                                    );
                            }
                        }
                    }
                    
                    // Filter the retrieved collection according to current conditions 
                    //RefreshAvailableDocNames();
                }
                return availableLetters;
            }
        }

        //[Association, DisplayName("Исходящее")]
        //public XPCollection<OutLetter> OutLetters
        //{
        //    get { return GetCollection<OutLetter>("OutLetters"); }
        //}
        //[Association, DisplayName("Правобладание")]
        //public XPCollection<Right> Rights
        //{
        //    get { return GetCollection<Right>("Rights"); }
        //}

        //private CapitalStructureBase _capitalStructureBase;
        ////[Association("CapitalStructureBase-GeneralDocBase"),DisplayName("Объект")]
        ////public CapitalStructureBase capitalStructureBase
        ////{
        ////    get { return _capitalStructureBase; }
        ////    set { SetPropertyValue("capitalStructureBase", ref _capitalStructureBase, value); }
        ////}
        [Association, DisplayName("Объекты строительства")]
        public XPCollection<CapitalStructureBase> CapitalStructureBase
        {
            get
            {
                return GetCollection<CapitalStructureBase>("CapitalStructureBase");
            }
        }
        //[Association, DisplayName("Помещения")]
        //public XPCollection<Apartment> Apartments
        //{
        //    get
        //    {
        //        return GetCollection<Apartment>("Apartments");
        //    }
        //}

        [Association, DisplayName("Земельные участки")]
        public XPCollection<Parcel> Parcels
        {
            get
            {
                return GetCollection<Parcel>("Parcels");
            }
        }
        //[Association, DisplayName("Согласования документа")]
        //public XPCollection<DocAgreement> DocAgreement
        //{
        //    get
        //    {
        //        return GetCollection<DocAgreement>("DocAgreement");
        //    }
        //}
        [Association, DisplayName("Стадия подготовки")]
        public XPCollection<GeneralDocState> generalDocState
        {
            get
            {
                return GetCollection<GeneralDocState>("generalDocState");
            }
        }

        //[Association, DisplayName("Документы об адресе")]
        //[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        //public XPCollection<AdressDoc.AdressDoc> AdressDocs
        //{
        //    get
        //    {
        //        return GetCollection<AdressDoc.AdressDoc>("AdressDocs");
        //    }
        //}

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            //if (SecuritySystem.CurrentUser != null)
            //{
            //    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //    if (currentEmpl != null)
            //        i_Employee = currentEmpl;
            //}
            if (DocDate == DateTime.MinValue)
                i_DocDate = DateTime.Now.Date;
        }
    }
}
