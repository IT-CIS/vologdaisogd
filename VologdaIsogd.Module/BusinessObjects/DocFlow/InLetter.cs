using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.OrgStructure;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Model;
using AISOGD.General;
using AISOGD.Subject;
using AISOGD.Enums;
//using AISOGD.Perm;

namespace AISOGD.DocFlow
{
    /// <summary>
    /// �������� ���������
    /// </summary>
    [NavigationItem("���������������")]
    [ModelDefault("Caption", "�������� ���������")]
    public class InLetter : AttachBase
    {
        public InLetter(Session session) : base(session) { }

        private Employee i_Employee;
        private DateTime i_RegDate;
        private bool _EndRegistrFlag = false;
        private dLetterTheme _LetterTheme;
        private int i_DaysControl;
        private DateTime i_ExecutionDate;
        private DateTime i_ExecutionFactDate;
        private string i_Subject;
        //[DisplayName("��� ���������")]
        //public LetterKind LetterKind { get; set; }

        private eInLetterStatus i_InLetterStatus;
        [DisplayName("������ ���������")]
        //[ImmediatePostData]
        public eInLetterStatus InLetterStatus
        {
            get { return i_InLetterStatus; }
            set { try { SetPropertyValue("InLetterStatus", ref i_InLetterStatus, value); } catch { } }
        }
        [DisplayName("������� ���������")]
        [ImmediatePostData]
        [RuleRequiredField("������� ����������", DefaultContexts.Save, "���� '������� ���������' �� ����� ���� ������")]
        public dLetterTheme letterTheme
        {
            get { return _LetterTheme; }
            set { try { SetPropertyValue("letterTheme", ref _LetterTheme, value); } catch { } }
        }

        //private Employee i_Official;
        //[DisplayName("����������� ����")]
        //public Employee Official
        //{
        //    get { return i_Official; }
        //    set { SetPropertyValue("Official", ref i_Official, value); }
        ////}
        //[Size(255), DisplayName("��������� ����� ��������� (�� ���������)")]
        //public string LetterSendNo { get; set; }

        //[DisplayName("���� ����������� (�� ���������)")]
        //public DateTime LetterSendDate { get; set; }

        private string i_RegNo;
        [Size(255), DisplayName("����� ������")]
        public string RegNo
        {
            get { return i_RegNo; }
            set { SetPropertyValue("RegNo", ref i_RegNo, value); }
        }

        [DisplayName("���� ������")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }

        private DateTime i_ImportDate;
        [DisplayName("���� �������")]
        public DateTime ImportDate
        {
            get { return i_ImportDate; }
            set { SetPropertyValue("ImportDate", ref i_ImportDate, value); }
        }

        private string i_CardDeloLink;
        [EditorAlias("HyperLinkStringPropertyEditor")]
        [DisplayName("������ �� �� ��� ����")]
        public string CardDeloLink
        {
            get { return i_CardDeloLink; }
            set { SetPropertyValue("CardDeloLink", ref i_CardDeloLink, value); }
        }
        private string i_RegNoCardDelo;
        [DisplayName("Id �� ��� ����")]
        public string RegNoCardDelo
        {
            get { return i_RegNoCardDelo; }
            set { SetPropertyValue("RegNoCardDelo", ref i_RegNoCardDelo, value); }
        }

        [Size(4000), DisplayName("����������")]
        public string Content { get; set; }


        //[DisplayName("�������� ����")]
        //[ImmediatePostData]
        //public int DaysControl
        //{
        //    get
        //    {
        //        try
        //        {
        //            i_DaysControl = letterTheme.ExecuteDays;
        //        }
        //        catch { }
        //        return i_DaysControl;
        //    }
        //    set
        //    {
        //        SetPropertyValue("DaysControl", ref i_DaysControl, value);
        //        OnChanged("ExecutionDate", i_RegDate, i_RegDate.AddDays(i_DaysControl).AddDays(3));
        //    }
        //}

        //[DisplayName("���� ����������")]
        //public DateTime ExecutionDate
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (i_RegDate != DateTime.MinValue)
        //                i_ExecutionDate = i_RegDate.AddDays(DaysControl);
        //        }
        //        catch { }
        //        return i_ExecutionDate;
        //    }
        //    set { SetPropertyValue("ExecutionDate", ref i_ExecutionDate, value); }
        //}

        //[DisplayName("���� ���������� ����� �� ������")]
        //public DateTime ExecutionFactDate
        //{
        //    get { return i_ExecutionFactDate; }
        //    set
        //    {
        //        SetPropertyValue("ExecutionFactDate", ref i_ExecutionFactDate, value);
        //        //OnChanged(CriteriaOperator
        //    }
        //}

        [Size(255), DisplayName("��������(��������)")]
        public string Subject
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(i_Subject))
                    return i_Subject;
                else
                {
                    try
                    {
                        foreach (GeneralSubject docsubj in DocSubjects)
                            if (docsubj.FullName != String.Empty)
                            {
                                if (res != String.Empty)
                                    res += ", ";
                                res += docsubj.FullName;
                            }
                    }
                    catch { }
                    i_Subject = res;
                    return i_Subject;
                }
            }
            set { SetPropertyValue("Subject", ref i_Subject, value); }
        }

        private string i_SubjectAdress;
        [Size(255), DisplayName("��������� ���������(��������)")]
        public string SubjectAdress
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(i_SubjectAdress))
                    return i_SubjectAdress;
                else
                {
                    try
                    {
                        foreach (GeneralSubject docsubj in DocSubjects)
                        {
                            if (docsubj.FullContactInfo != String.Empty)
                            {
                                if (res != String.Empty)
                                    res += ", ";
                                res += docsubj.FullContactInfo;
                            }
                        }
                    }
                    catch { }
                    i_SubjectAdress = res;
                    return i_SubjectAdress;
                }
            }
            set { SetPropertyValue("SubjectAdress", ref i_SubjectAdress, value); }
        }



        //[DisplayName("���������, ������������������ ���������")]
        //public Employee Empl
        //{
        //    get { return i_Employee; }
        //    set { SetPropertyValue("Empl", ref i_Employee, value); }
        //}


        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        //[Association, DisplayName("���������")]
        //public XPCollection<Resolution> Resolutions
        //{
        //    get { return GetCollection<Resolution>("Resolutions"); }
        //}
        //[Association, DisplayName("�������� ����� �� ������")]
        //public XPCollection<DocControl> DocControl
        //{
        //    get { return GetCollection<DocControl>("DocControl"); }
        //}

        [Association, DisplayName("���������")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }
        //[Association, DisplayName("��������� ����")]
        //public XPCollection<DocSubjectTest> DocSubjectTests
        //{
        //    get { return GetCollection<DocSubjectTest>("DocSubjectTests"); }
        //}
        //[Association, DisplayName("���������")]
        //public XPCollection<OutLetter> OutLetter
        //{
        //    get { return GetCollection<OutLetter>("OutLetter"); }
        //}
        [Association, DisplayName("���������")]
        public XPCollection<GeneralDocBase> GeneralDocBase
        {
            get { return GetCollection<GeneralDocBase>("GeneralDocBase"); }
        }
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //���������� ��������� ������������� ������
            if (property.Name == "GeneralDocBase")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(GeneralDocBase_CollectionChanged);
            }
            return result;
        }
        private void GeneralDocBase_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    if (InLetterStatus == eInLetterStatus.�����)
                    { InLetterStatus = eInLetterStatus.�������; }
                }
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    if(GeneralDocBase.Count == 0)
                        InLetterStatus = eInLetterStatus.�����;
                }
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (SecuritySystem.CurrentUser != null)
            //{
            //    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //    if (currentEmpl != null)
            //        i_Employee = currentEmpl;
            //}
            //i_RegDate = DateTime.Now.Date;
        }
    }

}