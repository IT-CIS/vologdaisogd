using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp.ConditionalAppearance;
using AISOGD.Subject;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using AISOGD.General;

namespace AISOGD.DocFlow
{
    /// <summary>
    /// ��������� ������
    /// </summary>
    //[NavigationItem("���������������")]
    [ModelDefault("Caption", "���������")]
    public class OutLetter : AttachBase
    {
        public OutLetter(Session session) : base(session) { }

        private Employee i_Employee;
        private DateTime i_OutLetterDate;

        private dOutLetterKind i_OutLetterKind;
        [DisplayName("��� ����������")]
        public dOutLetterKind OutLetterKind
        {
            get { return i_OutLetterKind; }
            set { SetPropertyValue("OutLetterKind", ref i_OutLetterKind, value); }
        }

        [Size(32), DisplayName("��������������� �����")]
        public string OutLetterNo { get; set; }

        [DisplayName("���� �����������")]
        public DateTime OutLetterDate
        {
            get { return i_OutLetterDate; }
            set { SetPropertyValue("OutLetterDate", ref i_OutLetterDate, value); }
        }
        //private Addressee i_Addressee;
        //[DisplayName("�������")]
        //public Addressee Addressee
        //{
        //    get { return i_Addressee; }
        //    set { SetPropertyValue("Addressee", ref i_Addressee, value); }
        //}
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Content { get; set; }

        [Size(255), DisplayName("����� ���������")]
        public string InLetterNo { get; set; }

        [DisplayName("���� ���������")]
        public DateTime InLetterDate { get; set; }
        
        [DisplayName("�����������")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        private Signer i_Signer;
        [DisplayName("�� ��������")]
        public Signer Signer
        {
            get { return i_Signer; }
            set { SetPropertyValue("Signer", ref i_Signer, value); }
        }
        //[Size(255), DisplayName("����� �������������")]
        //public string DocNo { get; set; }

        //[DisplayName("���� �������������")]
        //public DateTime DocDate { get; set; }

        [Size(255), DisplayName("��������� ���������")]
        public string InLetterStr { get; set; }


        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        //[Association, DisplayName("�����������")]
        //public XPCollection<Employee> Employees
        //{
        //    get { return GetCollection<Employee>("Employees"); }
        //}
        [Association, DisplayName("������������")]
        public XPCollection<Signer> Signers
        {
            get { return GetCollection<Signer>("Signers"); }
        }
        [Association, DisplayName("���������")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }


        [Association, DisplayName("��������")]
        public XPCollection<InLetter> InLetter
        {
            get { return GetCollection<InLetter>("InLetter"); }
        }

        [Association, DisplayName("���������")]
        public XPCollection<GeneralDocBase> GeneralDocBases
        {
            get { return GetCollection<GeneralDocBase>("GeneralDocBases"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Employee = currentEmpl;
            }
            i_OutLetterDate = DateTime.Now.Date;
        }

    }

}