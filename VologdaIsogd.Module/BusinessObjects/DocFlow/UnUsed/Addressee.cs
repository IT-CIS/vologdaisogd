﻿//using AISOGD.Constr;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AISOGD.OrgStructure;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{
    [NavigationItem("Документооборот")]
    [ModelDefault("Caption", "Адресат"), System.ComponentModel.DefaultProperty("descript")]
    public class Addressee : BaseObjectXAF
    {
        public Addressee(Session session) : base(session) { }

        private string _Name;
        private string _Capacity;
        private const string format = "{capacity} " + " {name}";

        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format(capacity + " " + name);
                //return ObjectFormatter.Format(format, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(255), DisplayName("ФИО")]
        public string name
        {
            get { return _Name; }
            set { try { _Name = value; } catch { } }
        }
        private string _NameDat;
        [Size(255), DisplayName("ФИО (дат. падеж)")]
        public string NameDat
        {
            get { return _NameDat; }
            set { try { _NameDat = value; } catch { } }
        }
        [Size(255), DisplayName("Должность")]
        public string capacity
        {
            get { return _Capacity; }
            set { try { _Capacity = value; } catch { } }
        }
        private string _CapacityDat;
        [Size(255), DisplayName("Должность (дат. падеж)")]
        public string capacityDat
        {
            get { return _CapacityDat; }
            set { try { _CapacityDat = value; } catch { } }
        }
        private string _Address;
        [Size(255), DisplayName("Адрес")]
        public string Address
        {
            get { return _Address; }
            set { try { _Address = value; } catch { } }
        }
        //[Association, System.ComponentModel.Browsable(false)]
        //public OutLetter OutLetter { get; set; }
    }

}
