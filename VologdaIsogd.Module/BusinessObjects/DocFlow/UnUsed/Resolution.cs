﻿using AISOGD.OrgStructure;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Резолюция"), NavigationItem("Документооборот"), System.ComponentModel.DefaultProperty("Name")]
    public class Resolution : TreeNodeAbstract
    {
        public Resolution(Session session)
            : base(session) { }
        

        private InLetter i_InLetter;
        [Association, DisplayName("Обращение")]
        public InLetter InLetter
        {
            get { return i_InLetter; }
            set { SetPropertyValue("InLetter", ref i_InLetter, value); }
        }
        private Employee i_Author;
        [DisplayName("Автор"), LookupEditorMode(LookupEditorMode.AllItemsWithSearch)]
        public Employee Author
        {
            get { return i_Author; }
            set
            {
                SetPropertyValue("Author", ref i_Author, value);
                //RefreshAvailableEmployees(); 
            }
        }
        private DateTime i_Date;
        [DisplayName("Дата")]
        public DateTime Date
        {
            get { return i_Date; }
            set { SetPropertyValue("Date", ref i_Date, value); }
        }
        private string i_Text;
        [Size(4000), DisplayName("Текст")]
        public string Text
        {
            get { return i_Text; }
            set { SetPropertyValue("Text", ref i_Text, value); }
        }
        [Association, DisplayName("Поручения")]
        public XPCollection<Commission> Commissions
        {
            get { return GetCollection<Commission>("Commissions"); }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return null;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return Commissions;
            }
        }
    }
}

