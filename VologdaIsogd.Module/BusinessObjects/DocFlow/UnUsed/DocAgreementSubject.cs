﻿using AISOGD.OrgStructure;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{
    /// <summary>
    /// Согласующее лицо
    /// </summary>
    [ModelDefault("Caption", "Согласующее лицо"), System.ComponentModel.DefaultProperty("DocAggrementSubjectText")]
    public class DocAgreementSubject : BaseObjectXAF
    {
        public DocAgreementSubject(Session session)
            : base(session) { }



        private Employee i_Employee;
        private string _Description;

        private string i_DocAggrementSubjectText;
        [Size(510),DisplayName("Согласующее лицо (описание)")]
        public string DocAggrementSubjectText
        {
            get {
                if (i_DocAggrementSubjectText == null || i_DocAggrementSubjectText == String.Empty)
                {
                    string res = String.Empty;
                    if (Employee != null)
                        if (Employee.BriefName != null)
                            res = Employee.BriefName;
                    i_DocAggrementSubjectText = res;
                }
                return i_DocAggrementSubjectText; }
            set { SetPropertyValue("DocAggrementSubjectText", ref i_DocAggrementSubjectText, value); }
        }


        [DisplayName("Согласующее лицо")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        [Size(4000), DevExpress.Xpo.DisplayName("Примечание")]
        public string description
        {
            get { return _Description; }
            set { SetPropertyValue("description", ref _Description, value); }
        }

        
    }
}
