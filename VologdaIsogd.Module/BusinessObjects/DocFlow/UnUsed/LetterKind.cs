﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{


    [ModelDefault("Caption", "Вид входящего обращения"), System.ComponentModel.DefaultProperty("Name")]
    [NavigationItem("Документооборот")]
    public class LetterKind : BaseObjectXAF
    {
        private string _name;

        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public LetterKind(Session session) : base(session) { }
    }


}