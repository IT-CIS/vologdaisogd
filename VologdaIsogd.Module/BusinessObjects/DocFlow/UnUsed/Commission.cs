﻿using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Поручение"), NavigationItem("Документооборот"), System.ComponentModel.DefaultProperty("Name")]
    public class Commission : TreeNodeAbstract
    {
        public Commission(Session session)
            : base(session) { }


        private Resolution i_Resolution;
        [Association, DisplayName("Резолюция")]
        [ImmediatePostData]
        public Resolution Resolution
        {
            get { return i_Resolution; }
            set { SetPropertyValue("Resolution", ref i_Resolution, value); }
        }
        private InLetter i_InLetter;
        [DisplayName("Обращение")]
        public InLetter InLetter
        {
            get { return i_InLetter; }
            set { SetPropertyValue("InLetter", ref i_InLetter, value); }
        }
        private Commission i_Commission;
        [Association, DisplayName("Вышестоящее поручение")]
        [System.ComponentModel.Browsable(false)]
        public Commission ParentCommission
        {
            get { return i_Commission; }
            set { SetPropertyValue("Commission", ref i_Commission, value); }
        }
        private Employee i_Author;
        [DisplayName("Автор"), LookupEditorMode(LookupEditorMode.AllItemsWithSearch)]
        public Employee Author
        {
            get { return i_Author; }
            set
            {
                SetPropertyValue("Author", ref i_Author, value);
                //RefreshAvailableEmployees(); 
            }
        }
        private DateTime i_Date;
        [DisplayName("Дата")]
        public DateTime Date
        {
            get { return i_Date; }
            set { SetPropertyValue("Date", ref i_Date, value); }
        }
        private string i_Text;
        [Size(4000), DisplayName("Текст")]
        public string Text
        {
            get { return i_Text; }
            set { SetPropertyValue("Text", ref i_Text, value); }
        }
        private Employee i_Employee;
        [DisplayName("Назначенный исполнитель"), LookupEditorMode(LookupEditorMode.AllItemsWithSearch)]
        public Employee Employee
        {
            get { return i_Employee; }
            set
            {
                SetPropertyValue("Employee", ref i_Employee, value);
            }
        }
        private DateTime i_ExecutionDate;
        [DisplayName("Срок исполнения")]
        public DateTime ExecutionDate
        {
            get { return i_ExecutionDate; }
            set { SetPropertyValue("ExecutionDate", ref i_ExecutionDate, value); }
        }
        private DateTime i_ExecutionFactDate;
        [DisplayName("Фактическое исполнение")]
        public DateTime ExecutionFactDate
        {
            get { return i_ExecutionFactDate; }
            set{SetPropertyValue("ExecutionFactDate", ref i_ExecutionFactDate, value);}
        }
        [Association, DisplayName("Поручения")]
        public XPCollection<Commission> Commissions
        {
            get { return GetCollection<Commission>("Commissions"); }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return ParentCommission;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return Commissions;
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            //if (SecuritySystem.CurrentUser != null)
            //{
            //    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //    if (currentEmpl != null)
            //        i_Employee = currentEmpl;
            //}
            if (Resolution != null)
            {
                InLetter = Resolution.InLetter;
            }
            i_Date = DateTime.Now.Date;
        }
    }
}

