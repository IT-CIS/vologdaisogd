﻿using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Проектная документация"), NavigationItem("Документооборот")]
    [System.ComponentModel.DefaultProperty("descript")]
    public class ConstrProjectDoc : GeneralDocBase
    {
        public ConstrProjectDoc(Session session)
            : base(session) { }
        private string i_name;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Проектная документация № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        [Size(255), DisplayName("Описание")]
        public string Name
        {
            get
            {
                return i_name;
            }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        private Municipality i_Municipality;
        [DisplayName("Муниципальное образование")]
        public Municipality Municipality
        {
            get
            {
                try { i_Municipality = GetMunicipality(); }
                catch { }
                return i_Municipality;
            }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.DocKind = Session.FindObject<DocKind>(new BinaryOperator("Name", "Проектная документация"));
            Session.CommitTransaction();

        }
    }
}
