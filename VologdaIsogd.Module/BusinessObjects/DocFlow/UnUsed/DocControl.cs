using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.OrgStructure;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{
    /// <summary>
    /// �������� ����� �� �������� ����������
    /// </summary>
    [NavigationItem("���������������")]
    public class DocControl : BaseObjectXAF
    {
        public DocControl(Session session) : base(session) { }

        private Employee i_Employee;
        private DateTime i_LetterOutDate;
        private DateTime i_LetterInDate;
        private double i_Days;
        private bool i_LetterComebackFlag;

        [Association, DisplayName("�������� ���������")]
        [System.ComponentModel.Browsable(false)]
        public InLetter InLetter { get; set; }

        [DisplayName("���������, �������� �������� ������")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [DisplayName("���� �������� ������ ����������")]
        public DateTime LetterOutDate
        {
            get { return i_LetterOutDate; }
            set { SetPropertyValue("LetterOutDate", ref i_LetterOutDate, value); }
        }

        [DisplayName("���� �������� ������ �����������")]
        public DateTime LetterInDate
        {
            get { return i_LetterInDate; }
            set { SetPropertyValue("LetterInDate", ref i_LetterInDate, value); }
        }

        [DisplayName("��������� ���� �� ������� �� ������")]
        public double Days
        {
            get { return i_Days; }
            set { SetPropertyValue("Days", ref i_Days, value); }
        }

        [Size(4000), DisplayName("����������")]
        public string Content { get; set; }

        [DisplayName("������ ���������� �����������")]
        public bool LetterComebackFlag
        {
            get { return i_LetterComebackFlag; }
            set { SetPropertyValue("LetterComebackFlag", ref i_LetterComebackFlag, value); }
        }

        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            i_LetterOutDate = DateTime.Now.Date;
        }

    }

}