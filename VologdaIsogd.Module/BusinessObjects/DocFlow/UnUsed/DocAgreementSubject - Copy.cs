﻿using AISOGD.OrgStructure;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{
    /// <summary>
    /// Согласующее лицо
    /// </summary>
    [ModelDefault("Caption", "Согласующее лицо"), System.ComponentModel.DefaultProperty("Employee.BrifName")]
    public class DocAgreementSubjectTest : BaseObjectXAF
    {
        public DocAgreementSubjectTest(Session session)
            : base(session) { }



        private Employee i_Employee;
        [DisplayName("Согласующее лицо")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        
    }
}
