﻿using AISOGD.OrgStructure;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Нормативно правовая документация"), NavigationItem("Документооборот")]
    [System.ComponentModel.DefaultProperty("descript")]
    public class NormativeLegalAct : GeneralDocBase
    {
        public NormativeLegalAct(Session session)
            : base(session) { }
        private string i_name;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("Нормативно правовой документ № {0} от {1}", DocNo, DocDate.ToShortDateString());
            }
        }

        [Size(255), DisplayName("Описание")]
        public string Name
        {
            get
            {
                return i_name;
            }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        [Association, DisplayName("Согласования документа")]
        public XPCollection<DocAgreementSubjectTest> DocAgreementSubjectTests
        {
            get
            {
                return GetCollection<DocAgreementSubjectTest>("DocAgreementSubjectTests");
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Session.CommitTransaction();

        }
    }
}
