﻿using AISOGD.OrgStructure;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{
    /// <summary>
    /// Согласование
    /// </summary>
    [ModelDefault("Caption", "Согласование")]
    [System.ComponentModel.DefaultProperty("descript")]
    public class DocAgreement : BaseObjectXAF
    {
        public DocAgreement(Session session)
            : base(session) { }

        
        
        private Employee _Executor;
        private string _Description;

        [Size(510), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("{0} : {1}", DocAggrementSubjectText, DocAggrementKind);
            }
        }

        private string i_DocAggrementSubjectText;
        [Size(510),DisplayName("Согласующее лицо (описание)")]
        public string DocAggrementSubjectText
        {
            get
            {
                if (i_DocAggrementSubjectText == null || i_DocAggrementSubjectText == String.Empty)
                {
                    string res = String.Empty;
                    if (DocAgreementSubject != null)
                        if (DocAgreementSubject.DocAggrementSubjectText != null)
                            res = DocAgreementSubject.DocAggrementSubjectText;
                    i_DocAggrementSubjectText = res;
                }
                return i_DocAggrementSubjectText;
            }
            set { SetPropertyValue("DocAggrementSubjectText", ref i_DocAggrementSubjectText, value); }
        }

        private DateTime i_DocAggrementShippDate;
        [DisplayName("Дата отправки на согласование")]
        public DateTime DocAggrementShippDate
        {
            get { return i_DocAggrementShippDate; }
            set { SetPropertyValue("DocAggrementShippDate", ref i_DocAggrementShippDate, value); }
        }
        private DateTime i_DocAggrementDate;
        [DisplayName("Дата согласования")]
        public DateTime DocAggrementDate
        {
            get { return i_DocAggrementDate; }
            set { SetPropertyValue("DocAggrementDate", ref i_DocAggrementDate, value); }
        }

        private DocAggrementKind i_DocAggrementKind;
        [DisplayName("Вид согласования")]
        public DocAggrementKind DocAggrementKind
        {
            get { return i_DocAggrementKind; }
            set { SetPropertyValue("DocAggrementKind", ref i_DocAggrementKind, value); }
        }

        private string i_DocAggrementText;
        [Size(510), DisplayName("Результат согласования")]
        public string DocAggrementText
        {
            get { return i_DocAggrementText; }
            set { SetPropertyValue("DocAggrementText", ref i_DocAggrementText, value); }
        }

        private DocAgreementSubject i_DocAgreementSubject;
        [DisplayName("Согласующее лицо")]
        public DocAgreementSubject DocAgreementSubject
        {
            get { return i_DocAgreementSubject; }
            set { SetPropertyValue("DocAgreementSubject", ref i_DocAgreementSubject, value); }
        }

        [Size(4000), DevExpress.Xpo.DisplayName("Примечание")]
        public string description
        {
            get { return _Description; }
            set { SetPropertyValue("description", ref _Description, value); }
        }

        private GeneralDocBase _generalDocBase;
        [Association, DisplayName("Документ")]
        public GeneralDocBase generalDocBase
        {
            get { return _generalDocBase; }
            set { SetPropertyValue("generalDocBase", ref _generalDocBase, value); }
        }

    }

    public enum DocAggrementKind
    {
        Согласовано = 0, НеСогласовано = 1, Иное = 2
    };
}
