﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{
    [ModelDefault("Caption", "Орган документа"), NavigationItem("Документооборот"), System.ComponentModel.DefaultProperty("Name")]
    public class DocOrg : BaseObjectXAF
    {
        public DocOrg(Session session)
            : base(session) { }
        private string _Name;

        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
    }
}
