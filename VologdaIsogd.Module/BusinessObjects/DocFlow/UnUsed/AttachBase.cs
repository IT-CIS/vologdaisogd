using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DomainComponents.Common;
using AISOGD.General;
using WinAisogdIngeo.Module.SystemDir;

namespace AISOGD.DocFlow
{

    public class AttachBase : BaseObjectXAF
    {
        public AttachBase() : base() { }
        public AttachBase(Session session) : base(session) { }

        [Aggregated, Association("AttachBase-AttachmentFiles"), DisplayName("����������� �����")]
        [FileTypeFilter("�����", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        [FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        public XPCollection<AttachmentFiles> AttachmentFiles
        {
            get { return GetCollection<AttachmentFiles>("AttachmentFiles"); }
        }
     
    }

}