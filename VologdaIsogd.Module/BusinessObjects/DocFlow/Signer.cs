﻿//using AISOGD.Constr;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AISOGD.OrgStructure;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.DocFlow
{
    [NavigationItem("Документооборот")]
    [ModelDefault("Caption", "Подписывающее лицо"), System.ComponentModel.DefaultProperty("descript")]
    public class Signer : BaseObjectXAF
    {
        public Signer(Session session) : base(session) { }

        private string _Name;
        private string _Capacity;
        private const string format = "{capacity} " + " {name}";

        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format(capacity + " " + name);
                //return ObjectFormatter.Format(format, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(255), DisplayName("ФИО")]
        public string name
        {
            get { return _Name; }
            set { try { _Name = value; } catch { } }
        }

        [Size(255), DisplayName("Должность")]
        public string capacity
        {
            get { return _Capacity; }
            set { try { _Capacity = value; } catch { } }
        }
        private bool i_DefaultSignerFlag;
        [DisplayName("Подписывающее лицо по умолчанию")]
        [ImmediatePostData]
        public bool DefaultSignerFlag
        {
            get { return i_DefaultSignerFlag; }
            set { try { i_DefaultSignerFlag = value; } catch { } }
        }

        //[Association, System.ComponentModel.Browsable(false)]
        //public OutLetter OutLetter { get; set; }

        protected override void OnSaved()
        {
            if (!IsDeleted)
            {
                if (DefaultSignerFlag)
                {
                    Connect connect = Connect.FromSession(Session);

                    foreach(Signer signer in connect.FindObjects<Signer>(mc => mc.Oid != null))
                    {
                        if (signer != this)
                            signer.DefaultSignerFlag = false;
                    }
                }
            }
        }
    }

}
