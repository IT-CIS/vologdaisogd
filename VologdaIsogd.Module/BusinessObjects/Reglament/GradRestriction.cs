﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Ограничения на виды использования для видов зон
    /// </summary>
    [ModelDefault("Caption", "Ограничения")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GradRestriction : BaseObjectXAF
    {
        public GradRestriction(Session session) : base(session) { }

        private dGradRestrictionKind i_GradRestrictionKind;
        private string i_name;
        private GradZoneKindBase i_GradZoneKindBase;
        //private TerrZoneKind i_TerrZoneKind;
        //private SpecialZoneKind i_SpecialZoneKind;
        //private MonumentZoneKind i_MonumentZoneKind;

        [DisplayName("Вид ограничения")]
        public dGradRestrictionKind GradRestrictionKind
        {
            get { return i_GradRestrictionKind; }
            set { SetPropertyValue("GradRestrictionKind", ref i_GradRestrictionKind, value); }
        }

        [Size(1000), DisplayName("Ограничение")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        [Association, DisplayName("Тип зоны")]
        [System.ComponentModel.Browsable(false)]
        public GradZoneKindBase GradZoneKindBase
        {
            get { return i_GradZoneKindBase; }
            set { SetPropertyValue("GradZoneKindBase", ref i_GradZoneKindBase, value); }
        }

        //[Association, DisplayName("Тип террзоны")]
        //[System.ComponentModel.Browsable(false)]
        //public TerrZoneKind TerrZoneKind
        //{
        //    get { return i_TerrZoneKind; }
        //    set { SetPropertyValue("TerrZoneKind", ref i_TerrZoneKind, value); }
        //}

        //[Association, DisplayName("Тип зоны с особыми условиями использования")]
        //[System.ComponentModel.Browsable(false)]
        //public SpecialZoneKind SpecialZoneKind
        //{
        //    get { return i_SpecialZoneKind; }
        //    set { SetPropertyValue("SpecialZoneKind", ref i_SpecialZoneKind, value); }
        //}
        //[Association, DisplayName("Тип зоны с особыми условиями использования")]
        //[System.ComponentModel.Browsable(false)]
        //public MonumentZoneKind MonumentZoneKind
        //{
        //    get { return i_MonumentZoneKind; }
        //    set { SetPropertyValue("MonumentZoneKind", ref i_MonumentZoneKind, value); }
        //}
        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
