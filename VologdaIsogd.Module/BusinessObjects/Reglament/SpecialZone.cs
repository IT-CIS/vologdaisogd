﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Зона с особыми условиями использования
    /// </summary>
    [ModelDefault("Caption", "Зона с особыми условиями использования")]//, NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class SpecialZone : BaseObjectXAF
    {
        public SpecialZone(Session session) : base(session) { }

        private SpecialZoneKind i_SpecialZoneKind;
        private string i_zoneNo;
        private string i_name;

        [Size(32), DisplayName("Номер зоны")]
        public string ZoneNo
        {
            get { return i_zoneNo; }
            set { SetPropertyValue("ZoneNo", ref i_zoneNo, value); }
        }

        [Size(255), DisplayName("Наименование зоны")]
        public string Name 
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        [Association, DisplayName("Тип зоны")]
        public SpecialZoneKind SpecialZoneKind
        {
            get { return i_SpecialZoneKind; }
            set { SetPropertyValue("SpecialZoneKind", ref i_SpecialZoneKind, value); }
        }

        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
