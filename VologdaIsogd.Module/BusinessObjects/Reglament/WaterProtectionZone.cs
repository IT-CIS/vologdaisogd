﻿
using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.OrgStructure;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Водоохранная зона
    /// </summary>
    [ModelDefault("Caption", "Водоохранная зона"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class WaterProtectionZone : SpecialZone
    {
        public WaterProtectionZone(Session session) : base(session) { }

        private double i_ZoneWidth;
        [DisplayName("Ширина зоны, м")]
        public double ZoneWidth
        {
            get { return i_ZoneWidth; }
            set { SetPropertyValue("ZoneWidth", ref i_ZoneWidth, value); }
        }
        private double i_RiverLength;
        [DisplayName("Длина реки, км")]
        public double RiverLength
        {
            get { return i_RiverLength; }
            set { SetPropertyValue("RiverLength", ref i_RiverLength, value); }
        }
        private string i_ZoneLimit;
        [Size(255), DisplayName("Режим ограничения")]
        public string ZoneLimit
        {
            get { return i_ZoneLimit; }
            set { SetPropertyValue("ZoneLimit", ref i_ZoneLimit, value); }
        }
        private string i_Kind;
        [Size(255), DisplayName("Вид")]
        public string Kind
        {
            get { return i_Kind; }
            set { SetPropertyValue("Kind", ref i_Kind, value); }
        }
        private string i_ZoneDocs;
        [Size(255), DisplayName("Документы")]
        public string ZoneDocs
        {
            get { return i_ZoneDocs; }
            set { SetPropertyValue("ZoneDocs", ref i_ZoneDocs, value); }
        }
        private DateTime i_RegDate;
        [DisplayName("Дата внсения")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
