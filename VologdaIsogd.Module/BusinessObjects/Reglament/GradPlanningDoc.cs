﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using AISOGD.Subject;
using AISOGD.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.DocFlow;
using AISOGD.OrgStructure;
using AISOGD.Isogd;
using DevExpress.ExpressApp.Model;
using AISOGD.General;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Градостроительная документация
    /// </summary>
    [ModelDefault("Caption", "Градостроительная документация"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GradPlanningDoc : AttachBase
    {
        public GradPlanningDoc(Session session) : base(session) { }

        private dGradPlanningDocKind i_GradPlanningDocKind;
        private string i_InventoryNo;
        private string i_name;
        //private Org i_org;
        //private AISOGD.Subject.Person i_person;
        //private eSubjectType i_requesterType;
        //private Org i_ExecutorOrg;
        //private AISOGD.Subject.Person i_ExecutorPerson;
        //private eSubjectType i_ExecutorKind;
        private string i_ExecutionYear;
        private string i_StatementDocText;
        private GeneralDocBase i_StatementDoc;
        private eDocumentStatus i_DocumentStatus;
        private string i_Note;
        private string i_Publication;
        private DateTime i_RegDate;
        private Employee i_Recorder;
        private Municipality i_Municipality;

        [DisplayName("Муниципальное образование")]
        public Municipality Municipality
        {
            get { return i_Municipality; }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }
        //        RegisterNo: string[64] Реестровый номер 
        [Size(1000), DisplayName("Наименование")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        [DisplayName("Вид документации")]
        public dGradPlanningDocKind GradPlanningDocKind
        {
            get { return i_GradPlanningDocKind; }
            set { SetPropertyValue("GradPlanningDocKind", ref i_GradPlanningDocKind, value); }
        }

        [Size(32), DisplayName("Инвентарный номер")]
        public string InventoryNo
        {
            get { return i_InventoryNo; }
            set { SetPropertyValue("InventoryNo", ref i_InventoryNo, value); }
        }
        private GeneralSubject i_Requester;
        [DisplayName("Заказчик")]
        public GeneralSubject Requester
        {
            get { return i_Requester; }
            set { SetPropertyValue("Requester", ref i_Requester, value); }
        }

        //[DisplayName("Тип заказчика")]
        //[ImmediatePostData]
        //public eSubjectType RequesterType
        //{
        //    get { return i_requesterType; }
        //    set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        //}
        //[DisplayName("Заказчик")]
        //[Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'физ'", Context = "DetailView")]
        //public Org RequesterOrg
        //{
        //    get { return i_org; }
        //    set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        //}

        //[DisplayName("Заказчик")]
        //[Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'юр'", Context = "DetailView")]
        //public AISOGD.Subject.Person RequesterPerson
        //{
        //    get { return i_person; }
        //    set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        //}
        private GeneralSubject i_Executor;
        [DisplayName("Исполнитель")]
        public GeneralSubject Executor
        {
            get { return i_Executor; }
            set { SetPropertyValue("Executor", ref i_Executor, value); }
        }
        //[DisplayName("Тип исполнителя")]
        //[ImmediatePostData]
        //public eSubjectType ExecutorKind
        //{
        //    get { return i_ExecutorKind; }
        //    set { SetPropertyValue("ExecutorKind", ref i_ExecutorKind, value); }
        //}
        //[DisplayName("Исполнитель")]
        //[Appearance("isExecutorOrg", Visibility = ViewItemVisibility.Hide, Criteria = "ExecutorKind= 'физ'", Context = "DetailView")]
        //public Org ExecutorOrg
        //{
        //    get { return i_ExecutorOrg; }
        //    set { SetPropertyValue("ExecutorOrg", ref i_ExecutorOrg, value); }
        //}

        //[DisplayName("Исполнитель")]
        //[Appearance("isExecutorPerson", Visibility = ViewItemVisibility.Hide, Criteria = "ExecutorKind= 'юр'", Context = "DetailView")]
        //public AISOGD.Subject.Person ExecutorPerson
        //{
        //    get { return i_ExecutorPerson; }
        //    set { SetPropertyValue("ExecutorPerson", ref i_ExecutorPerson, value); }
        //}

        [Size(4), DisplayName("Год исполнения")]
        public string ExecutionYear
        {
            get { return i_ExecutionYear; }
            set { SetPropertyValue("ExecutionYear", ref i_ExecutionYear, value); }
        }

        [DisplayName("Документ, утвердивший документацию")]
        public GeneralDocBase StatementDoc
        {
            get { return i_StatementDoc; }
            set { SetPropertyValue("StatementDoc", ref i_StatementDoc, value); }
        }

        [Size(4000), DisplayName("Документ, утвердивший документацию(Текст)")]
        public string StatementDocText
        {
            get { return i_StatementDocText; }
            set { SetPropertyValue("StatementDocText", ref i_StatementDocText, value); }
        }

        [DisplayName("Статус документации")]
        public eDocumentStatus DocumentStatus
        {
            get { return i_DocumentStatus; }
            set { SetPropertyValue("DocumentStatus", ref i_DocumentStatus, value); }
        }

        [Size(255), DisplayName("Публикация в СМИ")]
        public string Publication
        {
            get { return i_Publication; }
            set { SetPropertyValue("Publication", ref i_Publication, value); }
        }
        [DisplayName("Регистратор")]
        public Employee Recorder
        {
            get { return i_Recorder; }
            set { SetPropertyValue("Recorder", ref i_Recorder, value); }
        }
        [DisplayName("Дата регистрации")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        [Size(4000), DisplayName("Примечание")]
        public string Note
        {
            get { return i_Note; }
            set { SetPropertyValue("Note", ref i_Note, value); }
        }

        //[Association, DisplayName("Ссылка на документы ИСОГД")]
        //public XPCollection<IsogdDocument> IsogdDocument
        //{
        //    get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        //}
        //[ Association("TerrZone-TerrZoneKind", typeof(TerrZone)), DisplayName("Тип зоны")]

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
