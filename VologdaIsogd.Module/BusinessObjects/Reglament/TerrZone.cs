﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Территориальная зона"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TerrZone : BaseObjectXAF
    {
        public TerrZone(Session session) : base(session) { }

        private TerrZoneKind _terrZoneKind;
        private string _zoneNo;
        private string _name;
        private string _nameSmall;

        [Size(32), DisplayName("Номер зоны")]
        public string ZoneNo
        {
            get { return _zoneNo; }
            set { SetPropertyValue("ZoneNo", ref _zoneNo, value); }
        }

        [Size(255), DisplayName("Наименование зоны")]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(255), DisplayName("Наименование зоны (сокращенно)")]
        public string NameSmall
        {
            get { return _nameSmall; }
            set { SetPropertyValue("NameSmall", ref _nameSmall, value); }
        }

        private int i_YearChange;
        [DisplayName("Год изменения")]
        public int YearChange
        {
            get { return i_YearChange; }
            set { SetPropertyValue("YearChange", ref i_YearChange, value); }
        }
        private string i_OldZone;
        [Size(255), DisplayName("Бывшая зона")]
        public string OldZone
        {
            get { return i_OldZone; }
            set { SetPropertyValue("OldZone", ref i_OldZone, value); }
        }
        private string i_TerrZoneDoc;
        [Size(255), DisplayName("Документ")]
        public string TerrZoneDoc
        {
            get { return i_TerrZoneDoc; }
            set { SetPropertyValue("TerrZoneDoc", ref i_TerrZoneDoc, value); }
        }
        //[ Association("TerrZone-TerrZoneKind", typeof(TerrZone)), DisplayName("Тип зоны")]
        [Association, DisplayName("Тип зоны")]
        public TerrZoneKind TerrZoneKind
        {
            get { return _terrZoneKind; }
            set { SetPropertyValue("TerrZoneKind", ref _terrZoneKind, value); }
        }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }





        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
