﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Справочник категорий видов функционального использования")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class FunctionalUsingKindCategory : BaseObjectXAF, IHCategory
    {
        public FunctionalUsingKindCategory(Session session) : base(session) { }

        private string _note;
        private string _name;

        

        [Size(1000),DisplayName("Наименование")]
        public string Name 
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("Примечание")]
        public string Note
        {
            get { return _note; }
            set { SetPropertyValue("Note", ref _note, value); }
        }

        [Association("FunctionalUsingKindCategoryParent-FunctionalUsingKindCategoryChild")]
        [DisplayName("Подкатегория")]
        public XPCollection<FunctionalUsingKindCategory> Children
        {
            get { return GetCollection<FunctionalUsingKindCategory>("Children"); }
        }


        System.ComponentModel.IBindingList ITreeNode.Children
        {
            get { return Children as System.ComponentModel.IBindingList; }
        }

        private FunctionalUsingKindCategory parent;

        [Persistent, Association("FunctionalUsingKindCategoryParent-FunctionalUsingKindCategoryChild")]
        [DisplayName("Надкатегория")]
        public FunctionalUsingKindCategory Parent
        {
            get { return parent; }
            set
            {
                try
                {
                    parent = value;
                    OnChanged("Parent");
                }
                catch { }
            }
        }
        ITreeNode IHCategory.Parent
        {
            get { return Parent as IHCategory; }
            set { Parent = value as FunctionalUsingKindCategory; }
        }
        ITreeNode ITreeNode.Parent
        {
            get { return Parent as ITreeNode; }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
