﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Виды разрешенного использования")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class ZoneFunctionalUsingKind : BaseObjectXAF
    {
        public ZoneFunctionalUsingKind(Session session) : base(session) { }

        private dFunctionalUsingKind i_functionalUsingKind;
        [DisplayName("Вид функционального использования")]
        public dFunctionalUsingKind FunctionalUsingKind
        {
            get { return i_functionalUsingKind; }
            set { SetPropertyValue("FunctionalUsingKind", ref i_functionalUsingKind, value); }
        }

        private string i_MinSizeLiving;
        [Size(255), DisplayName("Минимальный размер участка (кв.м)")]
        public string MinSizeLiving
        {
            get { return i_MinSizeLiving; }
            set { SetPropertyValue("MinSizeLiving", ref i_MinSizeLiving, value); }
        }
        private string i_MinSizeLivingOKS;
        [Size(255), DisplayName("Минимальный размер участка при сущ. ОКС (кв.м)")]
        public string MinSizeLivingOKS
        {
            get { return i_MinSizeLivingOKS; }
            set { SetPropertyValue("MinSizeLivingOKS", ref i_MinSizeLivingOKS, value); }
        }

        private string i_MaxSizeLiving;
        [Size(255), DisplayName("Максимальный размер участка (кв.м)")]
        public string MaxSizeLiving
        {
            get { return i_MaxSizeLiving; }
            set { SetPropertyValue("MaxSizeLiving", ref i_MaxSizeLiving, value); }
        }

        //private string i_MinSizeOther;
        //[Size(255), DisplayName("Минимальный размер участка (кв.м)/иное назначение/")]
        //public string MinSizeOther
        //{
        //    get { return i_MinSizeOther; }
        //    set { SetPropertyValue("MinSizeOther", ref i_MinSizeOther, value); }
        //}

        //private string i_MaxSizeOther;
        //[Size(255), DisplayName("Максимальный размер участка (кв.м)/иное назначение/")]
        //public string MaxSizeOther
        //{
        //    get { return i_MaxSizeOther; }
        //    set { SetPropertyValue("MaxSizeOther", ref i_MaxSizeOther, value); }
        //}

        private string i_MaxFloorCountMin;
        [Size(255), DisplayName("Предельное количество этажей (минимум)")]
        public string MaxFloorCountMin
        {
            get { return i_MaxFloorCountMin; }
            set { SetPropertyValue("MaxFloorCountMin", ref i_MaxFloorCountMin, value); }
        }

        private string i_MaxFloorCountMax;
        [Size(255), DisplayName("Предельное количество этажей (максимум)")]
        public string MaxFloorCountMax
        {
            get { return i_MaxFloorCountMax; }
            set { SetPropertyValue("MaxFloorCountMax", ref i_MaxFloorCountMax, value); }
        }
        //private string i_MinIndentBorder;
        //[Size(255), DisplayName("Минимальные отступы от границ ЗУ (м)")]
        //public string MinIndentBorder
        //{
        //    get { return i_MinIndentBorder; }
        //    set { SetPropertyValue("MinIndentBorder", ref i_MinIndentBorder, value); }
        //}
        private string i_MinProcentBuilding;
        [Size(255), DisplayName("Минимальный процент застройки")]
        public string MinProcentBuilding
        {
            get { return i_MinProcentBuilding; }
            set { SetPropertyValue("MinProcentBuilding", ref i_MinProcentBuilding, value); }
        }
        private string i_MaxProcentBuilding;
        [Size(255), DisplayName("Максимальный процент застройки")]
        public string MaxProcentBuilding
        {
            get { return i_MaxProcentBuilding; }
            set { SetPropertyValue("MaxProcentBuilding", ref i_MaxProcentBuilding, value); }
        }
        private string i_MaxHeight;
        [Size(255), DisplayName("Максимальная высота")]
        public string MaxHeight
        {
            get { return i_MaxHeight; }
            set { SetPropertyValue("MaxHeight", ref i_MaxHeight, value); }
        }
        private string i_LimitionsText;
        [Size(SizeAttribute.Unlimited), DisplayName("Предельные размеры и параметры (текст)")]
        public string LimitionsText
        {
            get { return i_LimitionsText; }
            set { SetPropertyValue("LimitionsText", ref i_LimitionsText, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
