﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Виды разрешенного использования")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class AddZoneFunctionalUsingKind : ZoneFunctionalUsingKind
    {
        public AddZoneFunctionalUsingKind(Session session) : base(session) { }

        private TerrZoneKind _TerrZoneKind;

        [Association]
        [DisplayName("Вид территориальной зоны")]
        [System.ComponentModel.Browsable(false)]
        public TerrZoneKind TerrZoneKind
        {
            get { return _TerrZoneKind; }
            set { SetPropertyValue("TerrZoneKind", ref _TerrZoneKind, value); }
        }
        //[Association("AddZoneFunctionalUsingKind-TerrZoneKind", typeof(TerrZoneKind))] 
        //[DisplayName("Вид территориальной зоны")]
        //[System.ComponentModel.Browsable(false)]
        //public TerrZoneKind TerrZoneKind
        //{
        //    get { return _TerrZoneKind; }
        //    set { SetPropertyValue("TerrZoneKind", ref _TerrZoneKind, value); }
        //}
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
