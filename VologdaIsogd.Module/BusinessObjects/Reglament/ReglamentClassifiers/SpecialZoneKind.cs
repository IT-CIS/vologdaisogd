﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using AISOGD.GPZU;
using DevExpress.ExpressApp.Model;
//using AISOGD.GPZU;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Вид зоны с особыми условиями использования
    /// </summary>
    [ModelDefault("Caption", "Вид зоны с особыми условиями использования"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class SpecialZoneKind : GradZoneKindBase
    {
        public SpecialZoneKind(Session session) : base(session) { }

        [Association, DisplayName("Запрещенные виды использования")]
        public XPCollection<dProhibitedUsingSpecialZone> ProhibitedUsingSpecialZones
        {
            get { return GetCollection<dProhibitedUsingSpecialZone>("ProhibitedUsingSpecialZones"); }
        }

        [Association, DisplayName("Разрешенные виды использования")]
        public XPCollection<dPermitedUsingSpecialZone> PermitedUsingSpecialZones
        {
            get { return GetCollection<dPermitedUsingSpecialZone>("PermitedUsingSpecialZones"); }
        }
        

        [Association, DisplayName("Зоны с особыми условиями использования")]
        public XPCollection<SpecialZone> SpecialZone
        {
            get { return GetCollection<SpecialZone>("SpecialZone"); }
        }
        [Association, DisplayName("Зоны арт. скважин")]
        //[System.ComponentModel.Browsable(false)]
        public XPCollection<ArtWellZone> ArtWellZones
        {
            get { return GetCollection<ArtWellZone>("ArtWellZones"); }
        }
        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<GradPlan> GradPlan
        {
            get { return GetCollection<GradPlan>("GradPlan"); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        private void Save1()
        {
            base.Save();
        }
    }
}
