﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
//using AISOGD.GPZU;
using AISOGD.Land;
using AISOGD.GPZU;
using DevExpress.ExpressApp.Model;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Вид территориальной зоны"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TerrZoneKind : GradZoneKindBase
    {
        public TerrZoneKind(Session session) : base(session) { }

        private dTerrZoneCategory i_terrZoneCategory;


        [DisplayName("Категория территориальной зоны")]
        public dTerrZoneCategory TerrZoneCategory
        {
            get { return i_terrZoneCategory; }
            set { SetPropertyValue("TerrZoneCategory", ref i_terrZoneCategory, value); }
        }

        

        


        [Aggregated, Association, DisplayName("Основные виды разрешенного использования")]
        public XPCollection<MainZoneFunctionalUsingKind> MainZoneFunctionalUsingKind
        {
            get { return GetCollection<MainZoneFunctionalUsingKind>("MainZoneFunctionalUsingKind"); }
        }

        [Aggregated, Association, DisplayName("Условно разрешенные виды использования")]
        public XPCollection<CondZoneFunctionalUsingKind> CondZoneFunctionalUsingKind
        {
            get { return GetCollection<CondZoneFunctionalUsingKind>("CondZoneFunctionalUsingKind"); }
        }


        [Aggregated, Association, DisplayName("Вспомогательные виды разрешенного использования")]
        public XPCollection<AddZoneFunctionalUsingKind> AddZoneFunctionalUsingKind
        {
            get { return GetCollection<AddZoneFunctionalUsingKind>("AddZoneFunctionalUsingKind"); }
        }


        [Association, DisplayName("Территориальные зоны")]
        public XPCollection<TerrZone> TerrZone
        {
            get { return GetCollection<TerrZone>("TerrZone"); }
        }
        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<GradPlan> GradPlan
        {
            get { return GetCollection<GradPlan>("GradPlan"); }
        }
        //[Association, DisplayName("Земельный участок")]
        //[System.ComponentModel.Browsable(false)]
        //public XPCollection<Parcel> Parcels
        //{
        //    get { return GetCollection<Parcel>("Parcels"); }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        private void Save1()
        {
            base.Save();
        }
    }
}
