﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Вид градостроительной документации
    /// </summary>
    [ModelDefault("Caption", "Вид градостроительной документации"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dGradPlanningDocKind : dBaseClassifierReglament
    {
        public dGradPlanningDocKind(Session session) : base(session) { }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
