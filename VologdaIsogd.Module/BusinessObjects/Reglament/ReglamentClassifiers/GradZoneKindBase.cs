﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using AISOGD.OrgStructure;
using AISOGD.GPZU;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using AISOGD.GPZU;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Базовый вид зоны
    /// </summary>
    [ModelDefault("Caption", "Вид зоны")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GradZoneKindBase : BaseObjectXAF
    {
        public GradZoneKindBase(Session session) : base(session) { }

        private string i_purpose;
        private string i_name;
        private string i_nameSmall;
        private string i_article;
        private string i_GradRestrictionText;
        private GradPlanningDoc i_GradPlanningDoc;

        [Size(4000), DisplayName("Назначение зоны")]
        public string Purpose
        {
            get { return i_purpose; }
            set { SetPropertyValue("Purpose", ref i_purpose, value); }
        }

        [Size(255), DisplayName("Название зоны")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        [Size(255), DisplayName("Название зоны (сокращенно)")]
        public string NameSmall
        {
            get { return i_nameSmall; }
            set { SetPropertyValue("NameSmall", ref i_nameSmall, value); }
        }
        private string i_Reglament;
        [Size(500), DisplayName("Регламентируется")]
        public string Reglament
        {
            get { return i_Reglament; }
            set { SetPropertyValue("Reglament", ref i_Reglament, value); }
        }
        //[Size(4000), DisplayName("Ограничения")]
        //public string GradRestrictionText
        //{
        //    get { return i_GradRestrictionText; }
        //    set { SetPropertyValue("GradRestrictionText", ref i_GradRestrictionText, value); }
        //}
        [DisplayName("Градостроительная документация")]
        public GradPlanningDoc GradPlanningDoc
        {
            get { return i_GradPlanningDoc; }
            set { SetPropertyValue("GradPlanningDoc", ref i_GradPlanningDoc, value); }
        }

        private string i_Notes;
        [Size(SizeAttribute.Unlimited), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }

        //[DisplayName("Статья ПЗиЗ")]
        //public string Article
        //{
        //    get { return i_article; }
        //    set { SetPropertyValue("Article", ref i_article, value); }
        //}
        //private Municipality i_Municipality;
        //[DisplayName("Муниципальное образование")]
        //public Municipality Municipality
        //{
        //    get { return i_Municipality; }
        //    set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        //}

        [Association, DisplayName("Ограничения")]
        public XPCollection<GradRestriction> GradRestriction
        {
            get { return GetCollection<GradRestriction>("GradRestriction"); }
        }
        
        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        private void Save1()
        {
            base.Save();
        }
    }
}
