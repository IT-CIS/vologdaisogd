﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using AISOGD.GPZU;
using DevExpress.ExpressApp.Model;
//using AISOGD.GPZU;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Вид зоны объекта культурного наследия
    /// </summary>
    [ModelDefault("Caption", "Вид зоны охраны объекта культурного наследия"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class MonumentZoneKind : GradZoneKindBase
    {
        public MonumentZoneKind(Session session) : base(session) { }


        [Association, DisplayName("Разрешенные виды использования")]
        public XPCollection<dPermitedUsingMonumentZone> PermitedUsingMonumentZones
        {
            get { return GetCollection<dPermitedUsingMonumentZone>("PermitedUsingMonumentZones"); }
        }
        [Association, DisplayName("Запрещенные виды использования")]
        public XPCollection<dProhibitedUsingMonumentZone> ProhibitedUsingMonumentZones
        {
            get { return GetCollection<dProhibitedUsingMonumentZone>("ProhibitedUsingMonumentZones"); }
        }

        [Association, DisplayName("Зоны объектов культурного наследия")]
        public XPCollection<MonumentZone> MonumentZone
        {
            get { return GetCollection<MonumentZone>("MonumentZone"); }
        }
        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<GradPlan> GradPlan
        {
            get { return GetCollection<GradPlan>("GradPlan"); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
        private void Save1()
        {
            base.Save();
        }
    }
}
