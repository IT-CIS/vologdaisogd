﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Запрещенные виды использования ОКН")]//, NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dProhibitedUsingMonumentZone : BaseObjectXAF
    {
        public dProhibitedUsingMonumentZone(Session session) : base(session) { }

        [Size(SizeAttribute.Unlimited), DisplayName("Вид использования")]
        public string Name { get; set; }

        [Association, DisplayName("Виды зон ОКН")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<MonumentZoneKind> MonumentZoneKinds
        {
            get { return GetCollection<MonumentZoneKind>("MonumentZoneKinds"); }
        }
        //private MonumentZoneKind i_MonumentZoneKind;
        //[Association]
        //[DisplayName("Вид зоны объекта культурного наследия")]
        //[System.ComponentModel.Browsable(false)]
        //public MonumentZoneKind MonumentZoneKind
        //{
        //    get { return i_MonumentZoneKind; }
        //    set { SetPropertyValue("MonumentZoneKind", ref i_MonumentZoneKind, value); }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
