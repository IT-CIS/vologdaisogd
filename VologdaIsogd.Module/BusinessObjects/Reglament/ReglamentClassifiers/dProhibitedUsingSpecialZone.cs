﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{

    [ModelDefault("Caption", "Запрещенное использование ЗСУИ")]//, NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dProhibitedUsingSpecialZone : BaseObjectXAF
    {
        public dProhibitedUsingSpecialZone(Session session) : base(session) { }

        [Size(SizeAttribute.Unlimited), DisplayName("Запрещенное использование")]
        public string Name { get; set; }

        [Association, DisplayName("Виды зон с особыми условиями использования")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<SpecialZoneKind> SpecialZoneKinds
        {
            get { return GetCollection<SpecialZoneKind>("SpecialZoneKinds"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
