﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.OrgStructure;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Зона арт. скважин
    /// </summary>
    [ModelDefault("Caption", "Зона арт. скважин"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class ArtWellZone : SpecialZone
    {
        public ArtWellZone(Session session) : base(session) { }

        
        private string i_ArtWell;
        [Size(255), DisplayName("Артскважина")]
        public string ArtWell
        {
            get { return i_ArtWell; }
            set { SetPropertyValue("ArtWell", ref i_ArtWell, value); }
        }

        private string i_WellZoneNo;
        [Size(255), DisplayName("ЗСО")]
        public string WellZoneNo
        {
            get { return i_WellZoneNo; }
            set { SetPropertyValue("WellZoneNo", ref i_WellZoneNo, value); }
        }

        private string i_ZSOParam;
        [Size(255), DisplayName("Параметры ЗСО")]
        public string ZSOParam
        {
            get { return i_ZSOParam; }
            set { SetPropertyValue("ZSOParam", ref i_ZSOParam, value); }
        }

        private string i_InfoSource;
        [Size(255), DisplayName("Источник информации")]
        public string InfoSource
        {
            get { return i_InfoSource; }
            set { SetPropertyValue("InfoSource", ref i_InfoSource, value); }
        }

        private string i_Developer;
        [Size(255), DisplayName("Разработчик проекта")]
        public string Developer
        {
            get { return i_Developer; }
            set { SetPropertyValue("Developer", ref i_Developer, value); }
        }

        private string i_ZSODoc;
        [Size(255), DisplayName("Документ")]
        public string ZSODoc
        {
            get { return i_ZSODoc; }
            set { SetPropertyValue("ZSODoc", ref i_ZSODoc, value); }
        }

        private DateTime i_RegDate;
        [DisplayName("Дата внсения")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }

        private Employee i_Employee;
        [DisplayName("Регистратор")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }

        [Association, DisplayName("Типы зон с особыми условиями использования")]
        public XPCollection<SpecialZoneKind> SpecialZoneKinds
        {
            get { return GetCollection<SpecialZoneKind>("SpecialZoneKinds"); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
