﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using AISOGD.Monuments;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Зона объекта культурного наследия
    /// </summary>
    [ModelDefault("Caption", "Зона объекта культурного наследия"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class MonumentZone : BaseObjectXAF
    {
        public MonumentZone(Session session) : base(session) { }

        private MonumentZoneKind i_MonumentZoneKind;
        private string i_zoneNo;
        private string i_name;

        [Size(32), DisplayName("Номер зоны")]
        public string ZoneNo
        {
            get { return i_zoneNo; }
            set { SetPropertyValue("ZoneNo", ref i_zoneNo, value); }
        }
        private string i_ZoneCode;
        [Size(32), DisplayName("Код зоны")]
        public string ZoneCode
        {
            get { return i_ZoneCode; }
            set { SetPropertyValue("ZoneCode", ref i_ZoneCode, value); }
        }
        [Size(255), DisplayName("Наименование зоны")]
        public string Name 
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        private string i_MonumentNumbers;
        [Size(255), DisplayName("Номера памятников")]
        public string MonumentNumbers
        {
            get { return i_MonumentNumbers; }
            set { SetPropertyValue("MonumentNumbers", ref i_MonumentNumbers, value); }
        }
        private string i_Kvartal;
        [Size(255), DisplayName("Квартал")]
        public string Kvartal
        {
            get { return i_Kvartal; }
            set { SetPropertyValue("Kvartal", ref i_Kvartal, value); }
        }
        private string i_Square;
        [Size(255), DisplayName("Площадь")]
        public string Square
        {
            get { return i_Square; }
            set { SetPropertyValue("Square", ref i_Square, value); }
        }
        private string i_Note;
        [Size(1000), DisplayName("Примечание")]
        public string Note
        {
            get { return i_Note; }
            set { SetPropertyValue("Note", ref i_Note, value); }
        }

        [Association, DisplayName("Тип зоны")]
        public MonumentZoneKind MonumentZoneKind
        {
            get { return i_MonumentZoneKind; }
            set { SetPropertyValue("MonumentZoneKind", ref i_MonumentZoneKind, value); }
        }
        [Association, DisplayName("Объекты культурного наследия")]
        public XPCollection<Monument> Monuments
        {
            get { return GetCollection<Monument>("Monuments"); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
