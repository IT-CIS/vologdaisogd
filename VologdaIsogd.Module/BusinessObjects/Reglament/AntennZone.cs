﻿
using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.OrgStructure;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Зона антенн
    /// </summary>
    [ModelDefault("Caption", "Зона антенн"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class AntennZone : SpecialZone
    {
        public AntennZone(Session session) : base(session) { }


        private string i_Address;
        [Size(255), DisplayName("Местоположение")]
        public string Address
        {
            get { return i_Address; }
            set { SetPropertyValue("Address", ref i_Address, value); }
        }

        private double i_InfluenceRadius;
        [DisplayName("Радиус влияния")]
        public double InfluenceRadius
        {
            get { return i_InfluenceRadius; }
            set { SetPropertyValue("InfluenceRadius", ref i_InfluenceRadius, value); }
        }

        private double i_MaxBuildHight;
        [DisplayName("Ограничения застройки, м")]
        public double MaxBuildHight
        {
            get { return i_MaxBuildHight; }
            set { SetPropertyValue("MaxBuildHight", ref i_MaxBuildHight, value); }
        }

        private double i_InfluenceAngle;
        [DisplayName("Угол влияния")]
        public double InfluenceAngle
        {
            get { return i_InfluenceAngle; }
            set { SetPropertyValue("InfluenceAngle", ref i_InfluenceAngle, value); }
        }

        private string i_ZSODoc;
        [Size(255), DisplayName("Документы")]
        public string ZSODoc
        {
            get { return i_ZSODoc; }
            set { SetPropertyValue("ZSODoc", ref i_ZSODoc, value); }
        }

        private int i_SEZYear;
        [DisplayName("Год СЭЗ")]
        public int SEZYear
        {
            get { return i_SEZYear; }
            set { SetPropertyValue("SEZYear", ref i_SEZYear, value); }
        }

        private DateTime i_RegDate;
        [DisplayName("Дата внсения")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }

        private Employee i_Employee;
        [DisplayName("Регистратор")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        private string i_Operators;
        [Size(255), DisplayName("Операторы")]
        public string Operators
        {
            get { return i_Operators; }
            set { SetPropertyValue("Operators", ref i_Operators, value); }
        }

        private string i_DocStorage;
        [Size(255), DisplayName("Номер папки хранения")]
        public string DocStorage
        {
            get { return i_DocStorage; }
            set { SetPropertyValue("DocStorage", ref i_DocStorage, value); }
        }

        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
