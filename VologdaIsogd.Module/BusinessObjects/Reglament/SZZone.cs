﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Санитарно-защитная зона
    /// </summary>
    [ModelDefault("Caption", "Санитарно-защитная зона"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class SZZone : SpecialZone
    {
        public SZZone(Session session) : base(session) { }

        private SpecialZoneKind i_SpecialZoneKind;
        
        private string i_Polluter;
        [Size(255), DisplayName("Источник загрязнения")]
        public string Polluter
        {
            get { return i_Polluter; }
            set { SetPropertyValue("Polluter", ref i_Polluter, value); }
        }

        private string i_Address;
        [Size(255), DisplayName("Адрес")]
        public string Address
        {
            get { return i_Address; }
            set { SetPropertyValue("Address", ref i_Address, value); }
        }

        private string i_DangerClass;
        [Size(255), DisplayName("Класс опасности")]
        public string DangerClass
        {
            get { return i_DangerClass; }
            set { SetPropertyValue("DangerClass", ref i_DangerClass, value); }
        }

        private string i_ZoneMark;
        [Size(255), DisplayName("Ориентир СЗЗ")]
        public string ZoneMark
        {
            get { return i_ZoneMark; }
            set { SetPropertyValue("ZoneMark", ref i_ZoneMark, value); }
        }

        private string i_CalcParam;
        [Size(255), DisplayName("Параметры расчета")]
        public string CalcParam
        {
            get { return i_CalcParam; }
            set { SetPropertyValue("CalcParam", ref i_CalcParam, value); }
        }
        private string i_Finished;
        [Size(255), DisplayName("Уст_оконч")]
        public string Finished
        {
            get { return i_Finished; }
            set { SetPropertyValue("Finished", ref i_Finished, value); }
        }
        private string i_SanitaryConclusion;
        [Size(255), DisplayName("Сан.эпидем заключение")]
        public string SanitaryConclusion
        {
            get { return i_SanitaryConclusion; }
            set { SetPropertyValue("SanitaryConclusion", ref i_SanitaryConclusion, value); }
        }
        private string i_GSVSolution;
        [Size(255), DisplayName("Решение ГСВ")]
        public string GSVSolution
        {
            get { return i_GSVSolution; }
            set { SetPropertyValue("GSVSolution", ref i_GSVSolution, value); }
        }
        private int i_SZZYear;
        [DisplayName("Год установки СЗЗ")]
        public int SZZYear
        {
            get { return i_SZZYear; }
            set { SetPropertyValue("SZZYear", ref i_SZZYear, value); }
        }
        private DateTime i_RegDate;
        [DisplayName("Дата внсения")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        private string i_InfoSource;
        [Size(255), DisplayName("Источник информации")]
        public string InfoSource
        {
            get { return i_InfoSource; }
            set { SetPropertyValue("InfoSource", ref i_InfoSource, value); }
        }
        private string i_DocStorage;
        [Size(255), DisplayName("Дело хранения документа")]
        public string DocStorage
        {
            get { return i_DocStorage; }
            set { SetPropertyValue("DocStorage", ref i_DocStorage, value); }
        }
        private string i_SZZKindString;
        [Size(255), DisplayName("Вид СЗЗ")]
        public string SZZKindString
        {
            get { return i_SZZKindString; }
            set { SetPropertyValue("SZZKindString", ref i_SZZKindString, value); }
        }
        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }
        private string i_ZoneCode;
        [Size(255), DisplayName("Код зоны")]
        public string ZoneCode
        {
            get { return i_ZoneCode; }
            set { SetPropertyValue("ZoneCode", ref i_ZoneCode, value); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
