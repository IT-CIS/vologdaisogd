﻿
using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.OrgStructure;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Reglament
{
    /// <summary>
    /// Зона инженерных сетей
    /// </summary>
    [ModelDefault("Caption", "Зона инженерных сетей"), NavigationItem("Градостроительная документация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class EngeneeringZone : SpecialZone
    {
        public EngeneeringZone(Session session) : base(session) { }


        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
