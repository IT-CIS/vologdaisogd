﻿using AISOGD.Dictonaries;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Constr
{
    /// <summary>
    /// Иные показатели
    /// </summary>
    [ModelDefault("Caption", "Иные показатели"), System.ComponentModel.DefaultProperty("descript")]
    public class OtherIndicators : BaseObjectXAF
    {
        public OtherIndicators (Session session) : base(session) { }

        private string _PassportNo = String.Empty;
        //private DateTime _PassportDate;
        private string _InventaryNo = String.Empty;
        

        private const string shortFormat = "№ тех. паспорта {PassportNo} от {PassportDate}";
        //[System.ComponentModel.Browsable(false)]
        [Size(255), DisplayName("Описание")]
        public string descript
        {
            get
            {
                string res = "";
                try { if (OtherIndicatorType != null)
                        res = OtherIndicatorType.Name + ": ";
                }
                catch { }
                try
                {
                        res += OtherIndicatorValue + " ";
                }
                catch { }
                try
                {
                    if (Unit != null)
                        res += Unit.Name;
                }
                catch { }
                res.Trim().Trim();
                return res;
                //return ObjectFormatter.Format(shortFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        //private ConstrStage i_ConstrStage;
        //[Association, DisplayName("Этап строительства")]
        //[System.ComponentModel.Browsable(false)]
        //public ConstrStage ConstrStage
        //{
        //    get { return i_ConstrStage; }
        //    set { SetPropertyValue("ConstrStage", ref i_ConstrStage, value); }
        //}

        private dOtherIndicatorType i_OtherIndicatorType;
        [DisplayName("Показатель")]
        public dOtherIndicatorType OtherIndicatorType
        {
            get { return i_OtherIndicatorType; }
            set { SetPropertyValue("OtherIndicatorType", ref i_OtherIndicatorType, value);  }
        }
        private string i_OtherIndicatorValue;
        [Size(255), DisplayName("Значение показателя")]
        public string OtherIndicatorValue
        {
            get { return i_OtherIndicatorValue; }
            set { SetPropertyValue("OtherIndicatorValue", ref i_OtherIndicatorValue, value); }
        }
        private dUnitGeneral i_Unit;
        [DisplayName("Единица измерения")]
        public dUnitGeneral Unit
        {
            get { return i_Unit; }
            set { SetPropertyValue("Unit", ref i_Unit, value); }
        }
        
    }
}
