﻿using AISOGD.SystemDir;
using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Constr
{
    public static class ConstrStageHelper
    {
        public static string GetArchiveLayerName(IObjectSpace aObjectSpace)
        {
            Connect connect = Connect.FromObjectSpace(aObjectSpace);
            SpatialConfig SyrveyConfig = connect.FindFirstObject<SpatialConfig>(x => x.IsogdClassName == typeof(ConstrStage));
            if (SyrveyConfig == null)
                throw new Exception("Не обнаружены настройки ГИС связи для 'Стадий строительства'");
            foreach (SpatialLayerItem layer in SyrveyConfig.Layers)
            {
                string LayerId = layer?.SpatialLayer?.LayerId;
                if (string.IsNullOrWhiteSpace(LayerId))
                    throw new Exception("В настройках 'Стадий строительства' содержится пустой слой");
                if (LayerId.IndexOf("архив") != -1)
                    return LayerId;
            }
            throw new Exception("Не обнаружены настройки для архивного слоя");
        }
    }
}
