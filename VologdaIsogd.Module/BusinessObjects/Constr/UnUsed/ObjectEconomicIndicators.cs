﻿using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Perm;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Constr
{
    /// <summary>
    /// Проектные технико-экономические показатели
    /// </summary>
    public class ProjectTAEI : TechnicalAndEconomicIndicators
    {
        public ProjectTAEI(Session session) : base(session) { }

        private CapitalStructureBase _CapitalStructureBase;
        private ConstrDesigner _ConstrDesigner;
        private ConstrPerm _ConstrPerm;

        [Association("CapitalStructureBase-ProjectTAEI", typeof(CapitalStructureBase))]
        [DisplayName("Объект")]
        public CapitalStructureBase capitalStructureBase
        {
            get { return _CapitalStructureBase; }
            set { try { SetPropertyValue("capitalStructureBase", ref _CapitalStructureBase, value); } catch { } }
        }
        [DisplayName("Проектировщик")]
        public ConstrDesigner ConstrDesigner
        {
            get { return _ConstrDesigner; }
            set { try { SetPropertyValue("ConstrDesigner", ref _ConstrDesigner, value); } catch { } }
        }
        [Association, DisplayName("Разрешение на строительство")]
        public ConstrPerm ConstrPerm
        {
            get { return _ConstrPerm; }
            set { try { SetPropertyValue("ConstrPerm", ref _ConstrPerm, value); } catch { } }
        }
    }
    /// <summary>
    /// Фактические технико-экономические показатели
    /// </summary>
    public class FactTAEI : TechnicalAndEconomicIndicators
    {
        public FactTAEI(Session session) : base(session) { }

        private CapitalStructureBase _CapitalStructureBase;
        private UsePerm _UsePerm;

        [Association("CapitalStructureBase-FactTAEI", typeof(CapitalStructureBase))]
        [DisplayName("Объект")]
        public CapitalStructureBase capitalStructureBase
        {
            get { return _CapitalStructureBase; }
            set { try { SetPropertyValue("capitalStructureBase", ref _CapitalStructureBase, value); } catch { } }
        }
        [Association, DisplayName("Разрешение на ввод объекта в эксплуатацию")]
        public UsePerm UsePerm
        {
            get { return _UsePerm; }
            set { try { SetPropertyValue("UsePerm", ref _UsePerm, value); } catch { } }
        }
    }
    /// <summary>
    /// Технико-экономические показатели
    /// </summary>
    public class TechnicalAndEconomicIndicators : BaseObject
    {
        public TechnicalAndEconomicIndicators(Session session) : base(session) { }

        private int _BuildingCount;
        private double _Height;
        private double _BuildingSize;
        private double _BuildingSizeUnderGroundPart;
        private double _BuildingSizeOverGroundPart;
        private double _TotalBuildSquare;
        private double _BuildSquare;
        private double _LotSquare;
        private double _TotalSpace;
        private double _LivingSpace;
        private double _TotalLivingSpace;
        private int _FloorCount;
        private int _UnderGroundPart;
        private int _SectionCount;
        private int _ApartmentCount;
        private int _OneRoomFlatCount;
        private double _OneRoomFlatSq;
        private double _OneRoomFlatLivingSq;
        private int _TwoRoomFlatCount;
        private double _TwoRoomFlatSq;
        private double _TwoRoomFlatLivingSq;
        private int _ThreeRoomFlatCount;
        private double _ThreeRoomFlatSq;
        private double _ThreeRoomFlatLivingSq;
        private int _RoomCount;
        private double _ConstructionCost;
        private double _SpecificCost;
        private double _ConstrWorkCost;
        private double _Cost;
        private double _Power;
        private double _Productivity;
        private double _Length;
        private int _Capacity;
        private int _PlacesCount;
        private Material _FundMaterial;
        private Material _WallMaterial;
        private Material _RoofMaterial;
        private Material _BorderMaterial;
        private int _FoorRoomFlatCount;
        private double _FoorRoomFlatSq;
        private double _FoorRoomFlatLivingSq;
        private int _MoreFoorRoomFlatCount;
        private double _MoreFoorRoomFlatSq;
        private double _MoreFoorRoomFlatLivingSq;

        private BuildingKind i_BuildingKind;
        [DisplayName("Вид объекта"), VisibleInListView(false)]
        public BuildingKind BuildingKind
        {
            get { return i_BuildingKind; }
            set { try { SetPropertyValue("BuildingKind", ref i_BuildingKind, value); } catch { } }
        }

        private OKSKind i_OKSKind;
        [DisplayName("Тип объекта капитального строительства"), VisibleInListView(false)]
        public OKSKind OKSKind
        {
            get { return i_OKSKind; }
            set { try { SetPropertyValue("OKSKind", ref i_OKSKind, value); } catch { } }
        }


        [DisplayName("Площадь участка (кв.м)"), VisibleInListView(false)]
        public double lotSquare
        {
            get { return _LotSquare; }
            set { try { _LotSquare = value; } catch { } }
        }

        [DisplayName("Высота (м.)")]
        public double height
        {
            get { return _Height; }
            set { try { _Height = value; } catch { } }
        }

        [DisplayName("Площадь застройки (кв.м)"), VisibleInListView(false)]
        public double buildSquare
        {
            get { return _BuildSquare; }
            set { try { _BuildSquare = value; } catch { } }
        }

        private string i_OKSOtherIndicators;
        [DisplayName("Иные показатели"), VisibleInListView(false)]
        public string OKSOtherIndicators
        {
            get { return i_OKSOtherIndicators; }
            set { SetPropertyValue("OKSOtherIndicators", ref i_OKSOtherIndicators, value); }
        }


        /// <summary>
        /// Общие показатели объекта
        /// </summary>
        [DisplayName("Строительный объем - всего (куб.м)"), VisibleInListView(false)]
        public double buildingSize
        {
            get { return _BuildingSize; }
            set { try { _BuildingSize = value; } catch { } }
        }

        [DisplayName("Строительный объем надземной части (куб.м)"), VisibleInListView(false)]
        public double buildingSizeOverGroundPart
        {
            get { return _BuildingSizeOverGroundPart; }
            set { try { _BuildingSizeOverGroundPart = value; } catch { } }
        }

        [DisplayName("Строительный объем подземной части (куб.м)"), VisibleInListView(false)]
        public double buildingSizeUnderGroundPart
        {
            get { return _BuildingSizeUnderGroundPart; }
            set { try { _BuildingSizeUnderGroundPart = value; } catch { } }
        }

        [DisplayName("Общая площадь (кв.м)"), VisibleInListView(false)]
        public double totalBuildSquare
        {
            get { return _TotalBuildSquare; }
            set { try { _TotalBuildSquare = value; } catch { } }
        }

        private double i_NotLivingSquare;
        [DisplayName("Площадь нежилых помещений (кв.м)"), VisibleInListView(false)]
        public double NotLivingSquare
        {
            get { return i_NotLivingSquare; }
            set { SetPropertyValue("NotLivingSquare", ref i_NotLivingSquare, value); }
        }

        private double i_OutBuildingSquare;
        [DisplayName("Площадь встроенно-пристроенных помещений (кв.м)"), VisibleInListView(false)]
        public double OutBuildingSquare
        {
            get { return i_OutBuildingSquare; }
            set { SetPropertyValue("OutBuildingSquare", ref i_OutBuildingSquare, value); }
        }

        [DisplayName("Количество зданий, сооружений (ед.)")]
        public int buildingCount
        {
            get { return _BuildingCount; }
            set { try { _BuildingCount = value; } catch { } }
        }

        /// <summary>
        /// Нежилые объекты
        /// </summary>

        [DisplayName("Количество мест"), VisibleInListView(false)]
        public int placesCount
        {
            get { return _PlacesCount; }
            set { try { _PlacesCount = value; } catch { } }
        }

        [DisplayName("Количество помещений (ед.)"), VisibleInListView(false)]
        public int roomCount
        {
            get { return _RoomCount; }
            set { try { _RoomCount = value; } catch { } }
        }

        [DisplayName("Вместимость (чел.)"), VisibleInListView(false)]
        public int capacity
        {
            get { return _Capacity; }
            set { try { _Capacity = value; } catch { } }
        }

        private string i_FloorCount;
        [DisplayName("Количество этажей (шт.)")]
        public string FloorCount
        {
            get { return i_FloorCount; }
            set { SetPropertyValue("FloorCount", ref i_FloorCount, value); }
        }

        [DisplayName("Количество этажей подземной части (шт.)")]
        public int underGroundPart
        {
            get { return _UnderGroundPart; }
            set { try { _UnderGroundPart = value; } catch { } }
        }

        private int i_ElevatorsCount;
        [DisplayName("Лифты (шт.)"), VisibleInListView(false)]
        public int ElevatorsCount
        {
            get { return i_ElevatorsCount; }
            set { SetPropertyValue("ElevatorsCount", ref i_ElevatorsCount, value); }
        }

        private int i_EscalatorsCount;
        [DisplayName("Эскалаторы (шт.)"), VisibleInListView(false)]
        public int EscalatorsCount
        {
            get { return i_EscalatorsCount; }
            set { SetPropertyValue("EscalatorsCount", ref i_EscalatorsCount, value); }
        }

        private int i_InvalidLiftsCount;
        [DisplayName("Инвалидные подъемники (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCount
        {
            get { return i_InvalidLiftsCount; }
            set { SetPropertyValue("InvalidLiftsCount", ref i_InvalidLiftsCount, value); }
        }

        [DisplayName("Материалы фундаментов"), VisibleInListView(false)]
        public Material fundMaterial
        {
            get { return _FundMaterial; }
            set { try { SetPropertyValue("fundMaterial", ref _FundMaterial, value); } catch { } }
        }
        [DisplayName("Материалы стен"), VisibleInListView(false)]
        public Material wallMaterial
        {
            get { return _WallMaterial; }
            set { try { SetPropertyValue("wallMaterial", ref _WallMaterial, value); } catch { } }
        }

        [DisplayName("Материалы кровли"), VisibleInListView(false)]
        public Material roofMaterial
        {
            get { return _RoofMaterial; }
            set { try { SetPropertyValue("roofMaterial", ref _RoofMaterial, value); } catch { } }
        }
        [DisplayName("Материалы перекрытий"), VisibleInListView(false)]
        public Material borderMaterial
        {
            get { return _BorderMaterial; }
            set { try { SetPropertyValue("borderMaterial", ref _BorderMaterial, value); } catch { } }
        }



        [DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.) (кв.м)"), VisibleInListView(false)]
        public double livingSpace
        {
            get { return _LivingSpace; }
            set { try { _LivingSpace = value; } catch { } }
        }

        private double i_NotLivingSpace;
        [DisplayName("Общая площадь нежилых помещений(кв.м)"), VisibleInListView(false)]
        public double NotLivingSpace
        {
            get { return i_NotLivingSpace; }
            set { SetPropertyValue("NotLivingSpace", ref i_NotLivingSpace, value); }
        }

        [DisplayName("Количество секций")]
        public int sectionCount
        {
            get { return _SectionCount; }
            set { try { _SectionCount = value; } catch { } }
        }

        private int i_AppartmentsCount;
        [DisplayName("Количество квартир, всего (шт.)")]
        public int AppartmentsCount
        {
            get { return i_AppartmentsCount; }
            set { SetPropertyValue("AppartmentsCount", ref i_AppartmentsCount, value); }
        }

        private double i_AppartmentsSpace;
        [DisplayName("Общая площадь квартир, всего (кв.м)")]
        public double AppartmentsSpace
        {
            get { return i_AppartmentsSpace; }
            set { SetPropertyValue("AppartmentsSpace", ref i_AppartmentsSpace, value); }
        }

        [DisplayName("Количество однокомнатных квартир (шт.)"), VisibleInListView(false)]
        public int oneRoomFlatCount
        {
            get { return _OneRoomFlatCount; }
            set { try { _OneRoomFlatCount = value; } catch { } }
        }
        [DisplayName("Площадь однокомнатных квартир (кв.м)"), VisibleInListView(false)]
        public double oneRoomFlatSq
        {
            get { return _OneRoomFlatSq; }
            set { try { _OneRoomFlatSq = value; } catch { } }
        }
        //[DisplayName("Площадь однокомнатных квартир (жилая) (кв.м)"), VisibleInListView(false)]
        //public double oneRoomFlatLivingSq
        //{
        //    get { return _OneRoomFlatLivingSq; }
        //    set { try { _OneRoomFlatLivingSq = value; } catch { } }
        //}
        [DisplayName("Количество двухкомнатных квартир (шт.)"), VisibleInListView(false)]
        public int twoRoomFlatCount
        {
            get { return _TwoRoomFlatCount; }
            set { try { _TwoRoomFlatCount = value; } catch { } }
        }
        [DisplayName("Площадь двухкомнатных квартир (кв.м)"), VisibleInListView(false)]
        public double twoRoomFlatSq
        {
            get { return _TwoRoomFlatSq; }
            set { try { _TwoRoomFlatSq = value; } catch { } }
        }
        //[DisplayName("Площадь двухкомнатных квартир (жилая) (кв.м)"), VisibleInListView(false)]
        //public double twoRoomFlatLivingSq
        //{
        //    get { return _TwoRoomFlatLivingSq; }
        //    set { try { _TwoRoomFlatLivingSq = value; } catch { } }
        //}
        [DisplayName("Количество трехкомнатных квартир (шт.)"), VisibleInListView(false)]
        public int threeRoomFlatCount
        {
            get { return _ThreeRoomFlatCount; }
            set { try { _ThreeRoomFlatCount = value; } catch { } }
        }
        [DisplayName("Площадь трехкомнатных квартир (кв.м)"), VisibleInListView(false)]
        public double threeRoomFlatSq
        {
            get { return _ThreeRoomFlatSq; }
            set { try { _ThreeRoomFlatSq = value; } catch { } }
        }
        //[DisplayName("Площадь трехкомнатных квартир (жилая) (кв.м)"), VisibleInListView(false)]
        //public double threeRoomFlatLivingSq
        //{
        //    get { return _ThreeRoomFlatLivingSq; }
        //    set { try { _ThreeRoomFlatLivingSq = value; } catch { } }
        //}

        [DisplayName("Количество четырехкомнатных квартир (шт.)"), VisibleInListView(false)]
        public int foorRoomFlatCount
        {
            get { return _FoorRoomFlatCount; }
            set { try { _FoorRoomFlatCount = value; } catch { } }
        }
        [DisplayName("Площадь четырехкомнатных квартир (кв.м)"), VisibleInListView(false)]
        public double foorRoomFlatSq
        {
            get { return _FoorRoomFlatSq; }
            set { try { _FoorRoomFlatSq = value; } catch { } }
        }
        //[DisplayName("Площадь четырехкомнатных квартир (жилая) (кв.м)"), VisibleInListView(false)]
        //public double foorRoomFlatLivingSq
        //{
        //    get { return _FoorRoomFlatLivingSq; }
        //    set { try { _FoorRoomFlatLivingSq = value; } catch { } }
        //}


        [DisplayName("Количество более чем четырехкомнатных квартир (шт.)"), VisibleInListView(false)]
        public int moreFoorRoomFlatCount
        {
            get { return _MoreFoorRoomFlatCount; }
            set { try { _MoreFoorRoomFlatCount = value; } catch { } }
        }
        [DisplayName("Площадь более чем четырехкомнатных квартир (кв.м)"), VisibleInListView(false)]
        public double moreFoorRoomFlatSq
        {
            get { return _MoreFoorRoomFlatSq; }
            set { try { _MoreFoorRoomFlatSq = value; } catch { } }
        }
        //[DisplayName("Площадь более чем четырехкомнатных квартир (жилая) (кв.м)"), VisibleInListView(false)]
        //public double moreFoorRoomFlatLivingSq
        //{
        //    get { return _MoreFoorRoomFlatLivingSq; }
        //    set { try { _MoreFoorRoomFlatLivingSq = value; } catch { } }
        //}

        [DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.) (кв.м)"), VisibleInListView(false)]
        public double totalLivingSpace
        {
            get { return _TotalLivingSpace; }
            set { try { _TotalLivingSpace = value; } catch { } }
        }


        /// <summary>
        [DisplayName("Стоимость строительства объекта (тыс.руб)")]
        public double constructionCost
        {
            get { return _ConstructionCost; }
            set { try { _ConstructionCost = value; } catch { } }
        }
        [DisplayName("Удельная стоимость 1 кв.м. (тыс.руб)")]
        public double specificCost
        {
            get { return _SpecificCost; }
            set { try { _SpecificCost = value; } catch { } }
        }
        [DisplayName("Стоимость строительно-монтажных работ (тыс.руб)"), VisibleInListView(false)]
        public double constrWorkCost
        {
            get { return _ConstrWorkCost; }
            set { try { _ConstrWorkCost = value; } catch { } }
        }
        [DisplayName("Стоимость введенных в действие основных фондов (тыс.руб)"), VisibleInListView(false)]
        public double cost
        {
            get { return _Cost; }
            set { try { _Cost = value; } catch { } }
        }
        /// </summary>

        private ProductPurposeObjectType i_ProductPurposeObjectType;
        [DisplayName("Тип объекта производственного назначения")]
        public ProductPurposeObjectType ProductPurposeObjectType
        {
            get { return i_ProductPurposeObjectType; }
            set { SetPropertyValue("ProductPurposeObjectType", ref i_ProductPurposeObjectType, value); }
        }

        private string i_Power;
        [DisplayName("Мощность"), VisibleInListView(false)]
        public string Power
        {
            get { return i_Power; }
            set { SetPropertyValue("Power", ref i_Power, value); }
        }
        private string i_Productivity;
        [DisplayName("Производительность"), VisibleInListView(false)]
        public string productivity
        {
            get { return i_Productivity; }
            set { SetPropertyValue("Productivity", ref i_Productivity, value); }
        }


        /// <summary>
        /// Линейные объекты
        /// </summary>
        private LineObjectClass i_LineObjectClass;
        [DisplayName("Категория (класс) линейного объекта"), VisibleInListView(false)]
        public LineObjectClass LineObjectClass
        {
            get { return i_LineObjectClass; }
            set { SetPropertyValue("LineObjectClass", ref i_LineObjectClass, value); }
        }
        [DisplayName("Протяженность"), VisibleInListView(false)]
        public double length
        {
            get { return _Length; }
            set { try { _Length = value; } catch { } }
        }
        private string i_PipesInfo;
        [Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб"), VisibleInListView(false)]
        public string PipesInfo
        {
            get { return i_PipesInfo; }
            set { SetPropertyValue("PipesInfo", ref i_PipesInfo, value); }
        }
        private string i_ElectricLinesInfo;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи"), VisibleInListView(false)]
        public string ElectricLinesInfo
        {
            get { return i_ElectricLinesInfo; }
            set { SetPropertyValue("ElectricLinesInfo", ref i_ElectricLinesInfo, value); }
        }
        private string i_ConstructiveElementsInfo;
        [Size(1000), DisplayName("Перечень конструктивных элементов"), VisibleInListView(false)]
        public string ConstructiveElementsInfo
        {
            get { return i_ConstructiveElementsInfo; }
            set { SetPropertyValue("ConstructiveElementsInfo", ref i_ConstructiveElementsInfo, value); }
        }


        /// <summary>
        /// 5. Соответствие требованиям энергетической эффективности
        /// </summary>
        private EnergyEfficiencyClass i_EnergyEfficiencyClass;
        [DisplayName("Класс энергоэффективности"), VisibleInListView(false)]
        public EnergyEfficiencyClass EnergyEfficiencyClass
        {
            get { return i_EnergyEfficiencyClass; }
            set { SetPropertyValue("EnergyEfficiencyClass", ref i_EnergyEfficiencyClass, value); }
        }

        private double i_HeatConsumption;
        [DisplayName("Удельный расход тепловой энергии на 1 кв.м. площади (кВт*ч/м2)"), VisibleInListView(false)]
        public double HeatConsumption
        {
            get { return i_HeatConsumption; }
            set { SetPropertyValue("HeatConsumption", ref i_HeatConsumption, value); }
        }

        private string i_OutdoorIsolationMaterial;
        [Size(1000), DisplayName("Материалы утепления наружных ограждающих конструкций"), VisibleInListView(false)]
        public string OutdoorIsolationMaterial
        {
            get { return i_OutdoorIsolationMaterial; }
            set { SetPropertyValue("OutdoorIsolationMaterial", ref i_OutdoorIsolationMaterial, value); }
        }

        private string i_SkylightsFilling;
        [Size(1000), DisplayName("Заполнение световых проемов"), VisibleInListView(false)]
        public string SkylightsFilling
        {
            get { return i_SkylightsFilling; }
            set { SetPropertyValue("SkylightsFilling", ref i_SkylightsFilling, value); }
        }

        [Association, DisplayName("Сети и системы инженерно технического обслуживания")]
        public XPCollection<IngeneeringSystems> IngeneeringSystems
        {
            get { return GetCollection<IngeneeringSystems>("IngeneeringSystems"); }
        }

        
    }


       /// <summary>
       /// Справочник материалов
       /// </summary>
    public class Material : BaseObject
    {
        public Material(Session session) : base(session) { }
        
        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

    /// <summary>
    /// Справочник типов объектов производственного назначения
    /// </summary>
    public class ProductPurposeObjectType : BaseObject
    {
        public ProductPurposeObjectType(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Тип объекта")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

    /// <summary>
    /// Сети и системы инженерно технического обслуживания
    /// </summary>
    public class IngeneeringSystems : BaseObject
    {
        public IngeneeringSystems(Session session) : base(session) { }

        private IngeneeringSystemsType i_IngeneeringSystemsType;
        [DisplayName("Сети или системы инженерно технического обслуживания")]
        public IngeneeringSystemsType IngeneeringSystemsType
        {
            get { return i_IngeneeringSystemsType; }
            set { SetPropertyValue("IngeneeringSystemsType", ref i_IngeneeringSystemsType, value); }
        }

        private dUnit i_Unit;
        [DisplayName("Единица измерения")]
        public dUnit Unit
        {
            get { return i_Unit; }
            set { SetPropertyValue("Unit", ref i_Unit, value); }
        }

        private string i_Value;
        [DisplayName("Значение")]
        public string Value
        {
            get { return i_Value; }
            set { SetPropertyValue("Value", ref i_Value, value); }
        }


        private TechnicalAndEconomicIndicators i_TechnicalAndEconomicIndicators;
        [Association, DisplayName("Технико-экономические показатели")]
        public TechnicalAndEconomicIndicators TechnicalAndEconomicIndicators
        {
            get { return i_TechnicalAndEconomicIndicators; }
            set { SetPropertyValue("TechnicalAndEconomicIndicators", ref i_TechnicalAndEconomicIndicators, value); }
        }

    }

    /// <summary>
    /// Справочник типов сетей и систем инженерно технического обслуживания
    /// </summary>
    public class IngeneeringSystemsType : BaseObject
    {
        public IngeneeringSystemsType(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Тип сети или системы инженерно технического обслуживания")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

    /// <summary>
    /// Категория(класс) линейного объекта
    /// </summary>
    public class LineObjectClass : BaseObject
    {
        public LineObjectClass(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }
    /// <summary>
    /// Класс энергоэффективности
    /// </summary>
    public class EnergyEfficiencyClass : BaseObject
    {
        public EnergyEfficiencyClass(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Класс")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

}
