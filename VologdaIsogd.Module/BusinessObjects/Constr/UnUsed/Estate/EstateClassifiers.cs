using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using WinAisogdIngeo.Module.SystemDir;
using AISOGD.Dictonaries;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Constr
{
    /// <summary>
    /// ��� �����
    /// </summary>
    [ModelDefault("Caption", "��� �����"), System.ComponentModel.DefaultProperty("Name")]
    public class FloorType : BaseClassifier
    {
        public FloorType(Session session) : base(session) { }
        
        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }
    }

    /// <summary>
    /// ��� ���������
    /// </summary>
    [ModelDefault("Caption", "��� ���������"), System.ComponentModel.DefaultProperty("Name")]
    public class ApartmentType : BaseClassifier
    {
        public ApartmentType(Session session) : base(session) { }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }
    }

    /// <summary>
    /// ���������� ���������
    /// </summary>
    [ModelDefault("Caption", "���������� ���������"), System.ComponentModel.DefaultProperty("Name")]
    public class ApartmentPurpose : BaseClassifier
    {
        public ApartmentPurpose(Session session) : base(session) { }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }
    }

    /// <summary>
    /// ������ ������������� ��������
    /// </summary>
    [ModelDefault("Caption", "������ ������������� ��������"), System.ComponentModel.DefaultProperty("Name")]
    public class TransportModel : BaseClassifier
    {
        public TransportModel(Session session) : base(session) { }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }
    }

    /// <summary>
    /// ��� ������������� ��������
    /// </summary>
    [ModelDefault("Caption", "��� ������������� ��������"), System.ComponentModel.DefaultProperty("Name")]
    public class TransportType : BaseClassifier
    {
        public TransportType(Session session) : base(session) { }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }
    }

    /// <summary>
    /// ��������� ������������� ��������
    /// </summary>
    [ModelDefault("Caption", "��������� ������������� ��������"), System.ComponentModel.DefaultProperty("Name")]
    public class TransportCategory : BaseClassifier
    {
        public TransportCategory(Session session) : base(session) { }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }
    }
}