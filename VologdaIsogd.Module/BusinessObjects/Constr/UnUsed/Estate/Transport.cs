﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using WinAisogdIngeo.Module.SystemDir;
using AISOGD.Rosreestr;
using AISOGD.Enums;
using AISOGD.Subject;
using DevExpress.ExpressApp.Model;
using System.Drawing;
using AISOGD.Constr;

namespace AISOGD.Estate
{

    /// <summary>
    /// Этаж
    /// </summary>
    [ModelDefault("Caption", "Этаж"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("Имущество")]
    public class Transport : BaseObjectXAF
    {
        public Transport(Session session) : base(session) { }


        private string i_RegistrationNumber;
        [Size(64), DisplayName("Регистрационный номер")]
        public string RegistrationNumber
        {
            get { return i_RegistrationNumber; }
            set { SetPropertyValue("RegistrationNumber", ref i_RegistrationNumber, value); }
        }

        private TransportCategory i_TransportCategory;
        [DisplayName("Категория транспортного средства")]
        public TransportCategory TransportCategory
        {
            get { return i_TransportCategory; }
            set { SetPropertyValue("TransportCategory", ref i_TransportCategory, value); }
        }

        private TransportType i_TransportType;
        [DisplayName("Тип транспортного средства")]
        public TransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }

        private TransportModel i_TransportModel;
        [DisplayName("Модель, марка транспортного средства")]
        public TransportModel TransportModel
        {
            get { return i_TransportModel; }
            set { SetPropertyValue("TransportModel", ref i_TransportModel, value); }
        }

        private string i_ReleaseYear;
        [Size(4), DisplayName("Год выпуска")]
        public string ReleaseYear
        {
            get { return i_ReleaseYear; }
            set { SetPropertyValue("ReleaseYear", ref i_ReleaseYear, value); }
        }

        private string i_EngineNumber;
        [Size(64), DisplayName("Двигатель №")]
        public string EngineNumber
        {
            get { return i_EngineNumber; }
            set { SetPropertyValue("EngineNumber", ref i_EngineNumber, value); }
        }

        private string i_ChassisNumber;
        [Size(64), DisplayName("Шасси (рама) №")]
        public string ChassisNumber
        {
            get { return i_ChassisNumber; }
            set { SetPropertyValue("ChassisNumber", ref i_ChassisNumber, value); }
        }

        private string i_CarbodyNumber;
        [Size(64), DisplayName("Кузов (рама) №")]
        public string CarbodyNumber
        {
            get { return i_CarbodyNumber; }
            set { SetPropertyValue("CarbodyNumber", ref i_CarbodyNumber, value); }
        }

        [Persistent("Цвет")]
        private Int32 color;
        [NonPersistent]
        public System.Drawing.Color Color
        {
            get { return System.Drawing.Color.FromArgb(color); }
            set
            {
                color = value.ToArgb();
                OnChanged("Color");
            }
        }
//Color (ColorID): Estate/TransportColor →  Цвет  Файлы▼  

        private double i_EngineCapacity;
        [DisplayName("Мощность двигателя (л.с.)")]
        public double EngineCapacity
        {
            get { return i_EngineCapacity; }
            set { SetPropertyValue("EngineCapacity", ref i_EngineCapacity, value); }
        }

        private string i_RegistrationCertificate;
        [DisplayName("Номер паспорта транспортного средства")]
        public string RegistrationCertificate
        {
            get { return i_RegistrationCertificate; }
            set { SetPropertyValue("RegistrationCertificate", ref i_RegistrationCertificate, value); }
        }

        private DateTime i_RegistrationCertificateDate;
        [DisplayName("Дата паспорта транспортного средства")]
        public DateTime RegistrationCertificateDate
        {
            get { return i_RegistrationCertificateDate; }
            set { SetPropertyValue("RegistrationCertificateDate", ref i_RegistrationCertificateDate, value); }
        }
 
//AllowedMaxWeight: integer Разрешенная максимальная масса (кг.)  Файлы▼  
 
//Weight: integer Масса без нагрузки (кг.)  Файлы▼  
 
//HighLowCost: boolean Стоимость более 1000 МРОТ  Файлы▼  
 
//Okof: string[10] ОКОФ (код)  Файлы▼  
 
//OkofID: General/Okof →  ОКОФ (наименование)  Файлы▼  
 
//StartWorkingDate: dateTime Дата ввода в эксплуатацию  Файлы▼  

        
    }
}