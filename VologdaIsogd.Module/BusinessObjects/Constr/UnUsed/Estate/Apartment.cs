﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using WinAisogdIngeo.Module.SystemDir;
using AISOGD.Rosreestr;
using AISOGD.Enums;
using AISOGD.Subject;
using DevExpress.ExpressApp.Model;
using System.Drawing;
using AISOGD.Land;
using AISOGD.Address;

namespace AISOGD.Constr
{

    /// <summary>
    /// Помещение
    /// </summary>
    [ModelDefault("Caption", "Помещение"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("Имущество")]
    public class Apartment : BaseObjectXAF
    {
        public Apartment(Session session) : base(session) { }


        private CapitalStructureBase i_CapitalStructureBase;
        [Association, DisplayName("Объект строительства")]
        public CapitalStructureBase CapitalStructureBase
        {
            get { return i_CapitalStructureBase; }
            set { SetPropertyValue("CapitalStructureBase", ref i_CapitalStructureBase, value); }
        }

        private Floor i_Floor;
        [Association, DisplayName("Этаж")]
        public Floor Floor
        {
            get { return i_Floor; }
            set { SetPropertyValue("Floor", ref i_Floor, value); }
        }

        private string i_ApartmentNumber;
        [DisplayName("Номер помещения")]
        [ImmediatePostData]
        public string ApartmentNumber
        {
            get { return i_ApartmentNumber; }
            set { SetPropertyValue("ApartmentNumber", ref i_ApartmentNumber, value); }
        }

        private int i_ObjectRegNumber;
        [DisplayName("Номер объекта учета")]
        public int ObjectRegNumber
        {
            get { return i_ObjectRegNumber; }
            set { SetPropertyValue("ObjectRegNumber", ref i_ObjectRegNumber, value); }
        }

        private ApartmentType i_ApartmentType;
        [DisplayName("Тип помещения")]
        [ImmediatePostData]
        public ApartmentType ApartmentType
        {
            get { return i_ApartmentType; }
            set { SetPropertyValue("ApartmentType", ref i_ApartmentType, value); }
        }

        private ApartmentPurpose i_ApartmentPurpose;
        [DisplayName("Назначение помещения")]
        [ImmediatePostData]
        public ApartmentPurpose ApartmentPurpose
        {
            get { return i_ApartmentPurpose; }
            set { SetPropertyValue("ApartmentPurpose", ref i_ApartmentPurpose, value); }
        }

        private ApartmentCategory i_ApartmentCategory;
        [DisplayName("Категория помещения")]
        //[ImmediatePostData]
        public ApartmentCategory ApartmentCategory
        {
            get { return i_ApartmentCategory; }
            set { SetPropertyValue("ApartmentCategory", ref i_ApartmentCategory, value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get
            {
                try { i_Name = GetAppartName(); }
                catch { }
                return i_Name;
            }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private string GetAppartName()
        {
            string res = "";
            try { res += ApartmentType.Name; }
            catch { }
            try
            {
                if (res == "")
                    res = "№ " + ApartmentNumber;
                else
                    res += " № " + ApartmentNumber;
            }
            catch { }
            try
            {
                if (res == "")
                    res = ApartmentPurpose.Name;
                else
                    res += ", " + ApartmentPurpose.Name;
            }
            catch { }
            try
            {
                if (res == "")
                    res = "объект строительства: " + CapitalStructureBase.name;
                else
                    res += ", объект строительства: " + CapitalStructureBase.name;
            }
            catch { }
            return res;
        }

        //ObjectKind: string[12] Тип объекта  Файлы▼  

        //HeatingKind (HeatingKindID): Constr/HeatingKind →  Вид отопления  Файлы▼  

        //Okof: string[10] ОКОФ (код)  Файлы▼  

        //OkofID: General/Okof →  ОКОФ (наименование)  Файлы▼  

        private DateTime i_StartWorkingDate;
        [DisplayName("Дата ввода в эксплуатацию")]
        public DateTime StartWorkingDate
        {
            get { return i_StartWorkingDate; }
            set { SetPropertyValue("StartWorkingDate", ref i_StartWorkingDate, value); }
        }

        private IsLiving i_IsLiving;
        [DisplayName("Вид фонда")]
        public IsLiving IsLiving
        {
            get { return i_IsLiving; }
            set { SetPropertyValue("IsLiving", ref i_IsLiving, value); }
        }

        private OwnType i_OwnType;
        [DisplayName("Вид собственности")]
        public OwnType OwnType
        {
            get { return i_OwnType; }
            set { SetPropertyValue("OwnType", ref i_OwnType, value); }
        }

        private string i_Description;
        [Size(1000), DisplayName("Примечание")]
        public string Description
        {
            get { return i_Description; }
            set { SetPropertyValue("Description", ref i_Description, value); }
        }

        private Image image;
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("План помещения")]
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Image
        {
            get { return image; }
            set { SetPropertyValue("Image", ref image, value); }
        }
        [Association, DisplayName("Адрес")]
        public XPCollection<ApartmentAddress> AppartmentAddress
        {
            get { return GetCollection<ApartmentAddress>("AppartmentAddress"); }
        }

        [Association, DisplayName("Правообладание")]
        public XPCollection<Right> Rights
        {
            get
            {
                return GetCollection<Right>("Rights");
            }
        }
        [Association, DisplayName("Документы")]
        public XPCollection<AISOGD.DocFlow.GeneralDocBase> GeneralDocs
        {
            get
            {
                return GetCollection<AISOGD.DocFlow.GeneralDocBase>("GeneralDocs");
            }
        }
    }
}