﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using WinAisogdIngeo.Module.SystemDir;
using AISOGD.Rosreestr;
using AISOGD.Enums;
using AISOGD.Subject;
using DevExpress.ExpressApp.Model;
using System.Drawing;

namespace AISOGD.Constr
{

    /// <summary>
    /// Этаж
    /// </summary>
    [ModelDefault("Caption", "Этаж"), System.ComponentModel.DefaultProperty("Name")]
    //[ImageName("BO_Category"), NavigationItem("Основные справочники")]
    public class Floor : BaseObjectXAF
    {
        public Floor(Session session) : base(session) { }


        private CapitalStructureBase i_CapitalStructureBase;
        [Association, DisplayName("Объект строительства")]
        //[Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= 'юр'", Context = "DetailView")]
        public CapitalStructureBase CapitalStructureBase
        {
            get { return i_CapitalStructureBase; }
            set { SetPropertyValue("CapitalStructureBase", ref i_CapitalStructureBase, value); }
        }

        private FloorType i_FloorType;
        [DisplayName("Тип этажа")]
        public FloorType FloorType
        {
            get { return i_FloorType; }
            set { SetPropertyValue("FloorType", ref i_FloorType, value); }
        }
 
        private int i_FloorNumber;
        [DisplayName("Номер этажа")]
        public int FloorNumber
        {
            get { return i_FloorNumber; }
            set { SetPropertyValue("FloorNumber", ref i_FloorNumber, value); }
        }
        private double i_MOP;
        [DisplayName("Доля МОП на этаже")]
        public double MOP
        {
            get { return i_MOP; }
            set { SetPropertyValue("MOP", ref i_MOP, value); }
        }

        private Image image;
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("Поэтажный план")]
        [ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        public Image Image
        {
            get { return image; }
            set { SetPropertyValue("Image", ref image, value); }
        }
        [Association, DisplayName("Помещения")]
        public XPCollection<Apartment> Apartments
        {
            get
            {
                return GetCollection<Apartment>("Apartments");
            }
        }
    }
}