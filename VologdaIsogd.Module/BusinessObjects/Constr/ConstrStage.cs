﻿using AISOGD.Address;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.General;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.Perm;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Constr
{
    /// <summary>
    /// Этап строительства
    /// </summary>
    [Appearance("isLivingConstrStage", AppearanceItemType = "ViewItem", Priority = 1,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,Жилой#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"PlacesCountProject,RoomCountProject,
ProductPurposeObjectType,PowerProject,ProductivityProject,
LineObjectClass,LengthProject,PowerLineProject,PipesInfoProject,ElectricLinesInfoProject,ConstructiveElementsInfoProject")]
    [Appearance("isNotLivingConstrStage", AppearanceItemType = "ViewItem", Priority = 2,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,Нежилой#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"LivingSpaceProject,TotalLivingSpaceProject,NotLivingSpaceProject,SectionCountProject,PorchCountProject,AppartmentsCountProject,AppartmentsSpaceProject,
StudioCountProject,StudioSpaceProject,OneRoomFlatCountProject,OneRoomFlatSpaceProject,TwoRoomFlatCountProject,TwoRoomFlatSpaceProject,
ThreeRoomFlatCountProject,ThreeRoomFlatSpaceProject,FourRoomFlatCountProject,FourRoomSpaceProject,MoreRoomFlatCountProject,MoreRoomSpaceProject,
ElitAppartmentsCountProject,ElitAppartmentsSpaceProject,
ProductPurposeObjectType,PowerProject,ProductivityProject,
LineObjectClass,LengthProject,PowerLineProject,PipesInfoProject,ElectricLinesInfoProject,ConstructiveElementsInfoProject")]
    [Appearance("isProduktObjectConstrStage", AppearanceItemType = "ViewItem", Priority = 3,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,ПроизводственногоНазначения#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"PlacesCountProject,RoomCountProject,
LivingSpaceProject,TotalLivingSpaceProject,NotLivingSpaceProject,SectionCountProject,PorchCountProject,AppartmentsCountProject,AppartmentsSpaceProject,
StudioCountProject,StudioSpaceProject,OneRoomFlatCountProject,OneRoomFlatSpaceProject,TwoRoomFlatCountProject,TwoRoomFlatSpaceProject,
ThreeRoomFlatCountProject,ThreeRoomFlatSpaceProject,FourRoomFlatCountProject,FourRoomSpaceProject,MoreRoomFlatCountProject,MoreRoomSpaceProject,
ElitAppartmentsCountProject,ElitAppartmentsSpaceProject,
LineObjectClass,LengthProject,PowerLineProject,PipesInfoProject,ElectricLinesInfoProject,ConstructiveElementsInfoProject")]
    [Appearance("isLineObjectConstrStage", AppearanceItemType = "ViewItem", Priority = 4,
    Criteria = "BuildingKind = ##Enum#AISOGD.Enums.eBuildingKind,Линейные#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"PlacesCountProject,RoomCountProject,CapacityProject,
LivingSpaceProject,TotalLivingSpaceProject,NotLivingSpaceProject,SectionCountProject,PorchCountProject,AppartmentsCountProject,AppartmentsSpaceProject,
StudioCountProject,StudioSpaceProject,OneRoomFlatCountProject,OneRoomFlatSpaceProject,TwoRoomFlatCountProject,TwoRoomFlatSpaceProject,
ThreeRoomFlatCountProject,ThreeRoomFlatSpaceProject,FourRoomFlatCountProject,FourRoomSpaceProject,MoreRoomFlatCountProject,MoreRoomSpaceProject,
ElitAppartmentsCountProject,ElitAppartmentsSpaceProject,
ProductPurposeObjectType,PowerProject,ProductivityProject,
FloorCount,UnderGroundFloorCount,FloorCountAbove,FloorCountDiff,FloorsNote,
ElevatorsCountProject,EscalatorsCountProject,InvalidLiftsCountProject,
FundMaterialProject,WallMaterialProject,BorderMaterialProject,RoofMaterialProject,
ElectroProject,HeatProject,WaterProject,GazProject,HouseSeverageProject,PhonesProject,TVProject,RadioProject,SeverageProject,
EnergyEfficiencyClassProject,HeatConsumptionProject,OutdoorIsolationMaterialProject,SkylightsFillingProject")]
    [ModelDefault("Caption", "Этап строительства"), System.ComponentModel.DefaultProperty("FullName")]
    public class ConstrStage : BaseObjectXAF
    {
        public ConstrStage(Session session) : base(session) { }

        private string i_FullName;
        [Size(1024), DisplayName("Полное наименование")]
        public string FullName
        {
            get
            {
                string res = "Этап";
                try { res += StageNo; }
                catch { }
                res += "-";
                try { res += Name; }
                catch { }
                i_FullName = res;
                return i_FullName;
            }
            //set { SetPropertyValue("FullName", ref i_FullName, value); }
        }

        private eConstrStageState i_ConstrStageState;
        [DisplayName("Состояние очереди строительтсва")]
        public eConstrStageState ConstrStageState
        {
            get { return i_ConstrStageState; }
            set { SetPropertyValue("ConstrStageState", ref i_ConstrStageState, value); }
        }
        private string i_MapNo;
        [DisplayName("Номер в карте")]
        public string MapNo
        {
            get { return i_MapNo; }
            set { SetPropertyValue("MapNo", ref i_MapNo, value); }
        }
        private string i_StageNo;
        [Size(255), DisplayName("Очередь(этап) строительства")]
        public string StageNo
        {
            get { return i_StageNo; }
            set { SetPropertyValue("StageNo", ref i_StageNo, value); }
        }

        private string i_Name;
        [Size(1024), DisplayName("Описание")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        private City i_City;
        [DisplayName("Населенный пункт")]
        public City City
        {
            get { return i_City; }
            set
            {
                SetPropertyValue("City", ref i_City, value);
                try
                {
                    i_Address = i_City.FullName;
                }
                catch { }
                OnChanged("CityRegion");
            }
        }
        private Street i_Street;
        [DisplayName("Улица")]
        public Street Street
        {
            get { return i_Street; }
            set
            {
                SetPropertyValue("Street", ref i_Street, value);
                try
                {
                    i_Address = i_Street.FullName;
                    if (City != null)
                        if (i_City.FullName != null && i_City.FullName != "")
                        {
                            i_Address = i_City.FullName + ", " + i_Street.FullName;
                        }
                }
                catch { }
                OnChanged("Street");
            }
        }
        //private string i_ConstrAddress;
        //[Size(255), DisplayName("Строительный адрес")]
        //public string ConstrAddress
        //{
        //    get { return i_ConstrAddress; }
        //    set { SetPropertyValue("ConstrAddress", ref i_ConstrAddress, value); }
        //}
        private string i_ProjectHouseNo;
        [Size(255), DisplayName("Проектный номер дома")]
        public string ProjectHouseNo
        {
            get { return i_ProjectHouseNo; }
            set { SetPropertyValue("ProjectHouseNo", ref i_ProjectHouseNo, value); }
        }
        private string i_Address;
        [Size(255), DisplayName("Описание местоположения (адресная привязка)")]
        public string Address
        {
            get { return i_Address; }
            set { SetPropertyValue("Address", ref i_Address, value); }
        }
        private eWorkKind i_WorkKind;
        [DisplayName("Вид работ")]
        public eWorkKind WorkKind
        {
            get { return i_WorkKind; }
            set { try { SetPropertyValue("WorkKind", ref i_WorkKind, value); } catch { } }
        }
        private dConstructionType i_ConstructionType;
        [DisplayName("Тип объекта(справочник)")]
        [ImmediatePostData]
        public dConstructionType ConstructionType
        {
            get { return i_ConstructionType; }
            set { SetPropertyValue("ConstructionType", ref i_ConstructionType, value);
                try
                {
                    if (i_Name == null || i_Name == "")
                        i_Name = i_ConstructionType.FullName;
                }
                catch { }
                try
                {
                    if (i_ConstructionType.ParentConstructionUsing != null)
                    {
                        if (i_ConstructionType.ConstructionTypes.Count == 0)
                            ConstructionCategory = i_ConstructionType.ParentConstructionUsing;
                        else
                            ConstructionCategory = i_ConstructionType;
                    }
                }
                catch { }
                OnChanged("ConstructionType");
            }
        }
        private dConstructionType i_ConstructionCategory;
        [DisplayName("Категория объекта")]
        public dConstructionType ConstructionCategory
        {
            get { return i_ConstructionCategory; }
            set
            {
                SetPropertyValue("ConstructionCategory", ref i_ConstructionCategory, value);
            }
        }
        private string i_ConstrPermInfo;
        [Size(1024), DisplayName("Разрешения на строительство")]
        public string ConstrPermInfo
        {
            get { return i_ConstrPermInfo; }
            set { SetPropertyValue("ConstrPermInfo", ref i_ConstrPermInfo, value); }
        }
        private string i_UsePermInfo;
        [Size(1024), DisplayName("Разрешения на ввод")]
        public string UsePermInfo
        {
            get { return i_UsePermInfo; }
            set { SetPropertyValue("UsePermInfo", ref i_UsePermInfo, value); }
        }
        private int i_PlaningYearInUse;
        [DisplayName("Год ожидаемого ввода")]
        public int PlaningYearInUse
        {
            get { return i_PlaningYearInUse; }
            set { SetPropertyValue("PlaningYearInUse", ref i_PlaningYearInUse, value); }
        }
        private DateTime i_ConstrPermLastDate;
        [DisplayName("Дата последнего РС")]
        public DateTime ConstrPermLastDate
        {
            get { return i_ConstrPermLastDate; }
            set { SetPropertyValue("ConstrPermLastDate", ref i_ConstrPermLastDate, value); }
        }
        /// <summary>
        /// Характеристики стадии
        /// </summary>

        //private double i_Capacity;
        //[DisplayName("Вместимость"), VisibleInListView(false)]
        //public double Capacity
        //{
        //    get { return i_Capacity; }
        //    set { SetPropertyValue("Capacity", ref i_Capacity, value); }
        //}

        //private double i_FlatCount;
        //[DisplayName("Количество квартир"), VisibleInListView(false)]
        //public double FlatCount
        //{
        //    get { return i_FlatCount; }
        //    set { SetPropertyValue("FlatCount", ref i_FlatCount, value); }
        //}


        //[Association, DisplayName("Иные показатели")]
        //public XPCollection<OtherIndicators> OtherIndicators
        //{
        //    get
        //    {
        //        return GetCollection<OtherIndicators>("OtherIndicators");
        //    }
        //}




        private Material i_ConstrMaterial;
        [DisplayName("Строительный материал"), VisibleInListView(false)]
        public Material ConstrMaterial
        {
            get { return i_ConstrMaterial; }
            set { SetPropertyValue("ConstrMaterial", ref i_ConstrMaterial, value); }
        }
        private eConstrFireDangerClass i_ConstrFireDangerClass;
        [DisplayName("Класс конструктивной пожарной опасности")]
        public eConstrFireDangerClass ConstrFireDangerClass
        {
            get { return i_ConstrFireDangerClass; }
            set { SetPropertyValue("ConstrFireDangerClass", ref i_ConstrFireDangerClass, value); }
        }
        private eConstrFireShield i_ConstrFireShield;
        [DisplayName("Степень огнестойкости")]
        public eConstrFireShield ConstrFireShield
        {
            get { return i_ConstrFireShield; }
            set { SetPropertyValue("ConstrFireShield", ref i_ConstrFireShield, value); }
        }
        private string i_Note;
        [Size(1024), DisplayName("Примечание")]
        public string Note
        {
            get { return i_Note; }
            set { SetPropertyValue("Note", ref i_Note, value); }
        }

        private CapitalStructureBase i_CapitalStructureBase;
        [Association, DisplayName("Объект строительства")]
        public CapitalStructureBase CapitalStructureBase
        {
            get { return i_CapitalStructureBase; }
            set { SetPropertyValue("CapitalStructureBase", ref i_CapitalStructureBase, value); }
        }


        private eBuildingKind i_BuildingKind;
        [DisplayName("Вид объекта")]
        [ImmediatePostData]
        public eBuildingKind BuildingKind
        {
            get { return i_BuildingKind; }
            set { SetPropertyValue("BuildingKind", ref i_BuildingKind, value); }
        }

        private string i_ConstrDeveloperString;
        [Size(SizeAttribute.Unlimited), DisplayName("Застройщики")]
        public string ConstrDeveloperString
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(i_ConstrDeveloperString))
                    return i_ConstrDeveloperString;
                else
                {
                    return i_ConstrDeveloperString;
                }
            }
            set { try { SetPropertyValue("ConstrDeveloperString", ref i_ConstrDeveloperString, value); } catch { } }
        }
        private string i_ConstrDeveloperContactInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("Реквизиты застройщика")]
        public string ConstrDeveloperContactInfo
        {
            get
            {
                return i_ConstrDeveloperContactInfo;
            }
            set { SetPropertyValue("ConstrDeveloperContactInfo", ref i_ConstrDeveloperContactInfo, value); }
        }
        /// <summary>
        /// 1. Общие показатели объекта
        /// </summary>
        #region 1. Общие показатели объекта
        private double i_BuildingSizeProject;
        [DisplayName("Строительный объем - всего по проекту (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeProject
        {
            get { return i_BuildingSizeProject; }
            set { SetPropertyValue("BuildingSizeProject", ref i_BuildingSizeProject, value); }
        }
        
        private double i_BuildingSizeOverGroundPartProject;
        [DisplayName("Строительный объем надземной части по проекту (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeOverGroundPartProject
        {
            get { return i_BuildingSizeOverGroundPartProject; }
            set { SetPropertyValue("BuildingSizeOverGroundPartProject", ref i_BuildingSizeOverGroundPartProject, value); }
        }

        private double i_BuildingSizeUnderGroundPartProject;
        [DisplayName("Строительный объем подземной части по проекту (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeUnderGroundPartProject
        {
            get { return i_BuildingSizeUnderGroundPartProject; }
            set { SetPropertyValue("BuildingSizeUnderGroundPartProject", ref i_BuildingSizeUnderGroundPartProject, value); }
        }

        private double i_TotalBuildSquareProject;
        [DisplayName("Общая площадь по проекту (кв.м)"), VisibleInListView(false)]
        public double TotalBuildSquareProject
        {
            get { return i_TotalBuildSquareProject; }
            set { SetPropertyValue("TotalBuildSquareProject", ref i_TotalBuildSquareProject, value); }
        }
        private double i_BuildSquare;
        [DisplayName("Площадь застройки по проекту (кв.м)"), VisibleInListView(false)]
        public double BuildSquare
        {
            get { return i_BuildSquare; }
            set { SetPropertyValue("BuildSquare", ref i_BuildSquare, value); }
        }
        private double i_Height;
        [DisplayName("Высота по проекту(м.)")]
        public double Height
        {
            get { return i_Height; }
            set { SetPropertyValue("Height", ref i_Height, value); }
        }
        private string i_HeightNote;
        [Size(255),DisplayName("Примечание по высоте")]
        public string HeightNote
        {
            get { return i_HeightNote; }
            set { SetPropertyValue("HeightNote", ref i_HeightNote, value); }
        }
        private double i_NotLivingSquareProject;
        [DisplayName("Площадь нежилых помещений по проекту (кв.м)"), VisibleInListView(false)]
        public double NotLivingSquareProject
        {
            get { return i_NotLivingSquareProject; }
            set { SetPropertyValue("NotLivingSquareProject", ref i_NotLivingSquareProject, value); }
        }

        private double i_OutBuildingSquareProject;
        [DisplayName("Площадь встроенно-пристроенных помещений по проекту (кв.м)"), VisibleInListView(false)]
        public double OutBuildingSquareProject
        {
            get { return i_OutBuildingSquareProject; }
            set { SetPropertyValue("OutBuildingSquareProject", ref i_OutBuildingSquareProject, value); }
        }

        private int i_BuildingCountProject;
        [DisplayName("Количество зданий, сооружений по проекту (шт.)")]
        public int BuildingCountProject
        {
            get { return i_BuildingCountProject; }
            set { SetPropertyValue("BuildingCountProject", ref i_BuildingCountProject, value); }
        }
        
        #endregion

        #region Общие показатели по этажности для нежилых, жилых и производственных (ОКС)
        private int i_FloorCount;
        [DisplayName("Количество этажей (шт.)")]
        public int FloorCount
        {
            get { return i_FloorCount; }
            set { SetPropertyValue("FloorCount", ref i_FloorCount, value); }
        }

        private int i_UnderGroundFloorCount;
        [DisplayName("Количество этажей подземной части (шт.)")]
        public int UnderGroundFloorCount
        {
            get { return i_UnderGroundFloorCount; }
            set { SetPropertyValue("UnderGroundFloorCount", ref i_UnderGroundFloorCount, value); }
        }
        private int i_FloorCountAbove;
        [DisplayName("Этажность")]
        public int FloorCountAbove
        {
            get { return i_FloorCountAbove; }
            set { SetPropertyValue("FloorCountAbove", ref i_FloorCountAbove, value); }
        }
        private string i_FloorCountDiff;
        [DisplayName("Разноэтажность")]
        public string FloorCountDiff
        {
            get { return i_FloorCountDiff; }
            set { SetPropertyValue("FloorCountDiff", ref i_FloorCountDiff, value); }
        }
        private string i_FloorsNote;
        [DisplayName("Примечание по этажности")]
        public string FloorsNote
        {
            get { return i_FloorsNote; }
            set { SetPropertyValue("FloorsNote", ref i_FloorsNote, value); }
        }
        #endregion

        #region Лифты, Эскалаторы, подъемники для ОКС
        private int i_ElevatorsCountProject;
        [DisplayName("Лифты по проекту (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountProject
        {
            get { return i_ElevatorsCountProject; }
            set { SetPropertyValue("ElevatorsCountProject", ref i_ElevatorsCountProject, value); }
        }
        private int i_EscalatorsCountProject;
        [DisplayName("Эскалаторы по проекту (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountProject
        {
            get { return i_EscalatorsCountProject; }
            set { SetPropertyValue("EscalatorsCountProject", ref i_EscalatorsCountProject, value); }
        }
        private int i_InvalidLiftsCountProject;
        [DisplayName("Инвалидные подъемники по проекту (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountProject
        {
            get { return i_InvalidLiftsCountProject; }
            set { SetPropertyValue("InvalidLiftsCountProject", ref i_InvalidLiftsCountProject, value); }
        }
        #endregion

        #region Материалы
        private Material i_FundMaterialProject;
        [DisplayName("Материалы фундаментов по проекту"), VisibleInListView(false)]
        public Material FundMaterialProject
        {
            get { return i_FundMaterialProject; }
            set { SetPropertyValue("FundMaterialProject", ref i_FundMaterialProject, value); }
        }

        private Material i_WallMaterialProject;
        [DisplayName("Материалы стен по проекту"), VisibleInListView(false)]
        public Material WallMaterialProject
        {
            get { return i_WallMaterialProject; }
            set { SetPropertyValue("WallMaterialProject", ref i_WallMaterialProject, value);
                try
                {
                    ConstrMaterial = WallMaterialProject;
                }
                catch { }
                OnChanged("WallMaterialProject");
            }
        }

        private Material i_BorderMaterialProject;
        [DisplayName("Материалы перекрытий по проекту"), VisibleInListView(false)]
        public Material BorderMaterialProject
        {
            get { return i_BorderMaterialProject; }
            set { SetPropertyValue("BorderMaterialProject", ref i_BorderMaterialProject, value); }
        }

        private Material i_RoofMaterialProject;
        [DisplayName("Материалы кровли по проекту"), VisibleInListView(false)]
        public Material RoofMaterialProject
        {
            get { return i_RoofMaterialProject; }
            set { SetPropertyValue("RoofMaterialProject", ref i_RoofMaterialProject, value); }
        }
        #endregion

        #region Сети и системы инженерно технического обслуживания
        private bool i_ElectroProject;
        [DisplayName("Электроснабжение по проекту"), VisibleInListView(false)]
        public bool ElectroProject
        {
            get { return i_ElectroProject; }
            set { SetPropertyValue("ElectroProject", ref i_ElectroProject, value); }
        }

        private bool i_HeatProject;
        [DisplayName("Теплоснабжение по проекту"), VisibleInListView(false)]
        public bool HeatProject
        {
            get { return i_HeatProject; }
            set { SetPropertyValue("HeatProject", ref i_HeatProject, value); }
        }

        private bool i_WaterProject;
        [DisplayName("Водоснабжение по проекту"), VisibleInListView(false)]
        public bool WaterProject
        {
            get { return i_WaterProject; }
            set { SetPropertyValue("WaterProject", ref i_WaterProject, value); }
        }

        private bool i_GazProject;
        [DisplayName("Газофикация по проекту"), VisibleInListView(false)]
        public bool GazProject
        {
            get { return i_GazProject; }
            set { SetPropertyValue("GazProject", ref i_GazProject, value); }
        }

        private bool i_HouseSeverageProject;
        [DisplayName("Водоотведение проекту"), VisibleInListView(false)]
        public bool HouseSeverageProject
        {
            get { return i_HouseSeverageProject; }
            set { SetPropertyValue("HouseSeverageProject", ref i_HouseSeverageProject, value); }
        }

        private bool i_PhonesProject;
        [DisplayName("Телефонизация по проекту"), VisibleInListView(false)]
        public bool PhonesProject
        {
            get { return i_PhonesProject; }
            set { SetPropertyValue("PhonesProject", ref i_PhonesProject, value); }
        }

        private bool i_TVProject;
        [DisplayName("Телевидение по проекту"), VisibleInListView(false)]
        public bool TVProject
        {
            get { return i_TVProject; }
            set { SetPropertyValue("TVProject", ref i_TVProject, value); }
        }

        private bool i_RadioProject;
        [DisplayName("Радиофикация по проекту"), VisibleInListView(false)]
        public bool RadioProject
        {
            get { return i_RadioProject; }
            set { SetPropertyValue("RadioProject", ref i_RadioProject, value); }
        }

        private bool i_SeverageProject;
        [DisplayName("Ливневая канализация по проекту"), VisibleInListView(false)]
        public bool SeverageProject
        {
            get { return i_SeverageProject; }
            set { SetPropertyValue("SeverageProject", ref i_SeverageProject, value); }
        }
        #endregion

        #region Иные показатели
        private string i_OtherIndicatorsProject;
        [Size(4000),DisplayName("Иные показатели по проекту"), VisibleInListView(false)]
        public string OtherIndicatorsProject
        {
            get { return i_OtherIndicatorsProject; }
            set { SetPropertyValue("OtherIndicatorsProject", ref i_OtherIndicatorsProject, value); }
        }
        #endregion

        /// <summary>
        /// 2.1. Нежилые объекты
        /// </summary>
        #region 2.1. Нежилые объекты
        private int i_PlacesCountProject;
        [DisplayName("Количество мест по проекту"), VisibleInListView(false)]
        public int PlacesCountProject
        {
            get { return i_PlacesCountProject; }
            set { SetPropertyValue("PlacesCountProject", ref i_PlacesCountProject, value); }
        }

        private int i_RoomCountProject;
        [DisplayName("Количество помещений по проекту"), VisibleInListView(false)]
        public int RoomCountProject
        {
            get { return i_RoomCountProject; }
            set { SetPropertyValue("RoomCountProject", ref i_RoomCountProject, value); }
        }

        private int i_CapacityProject;
        [DisplayName("Вместимость по проекту"), VisibleInListView(false)]
        public int CapacityProject
        {
            get { return i_CapacityProject; }
            set { SetPropertyValue("CapacityProject", ref i_CapacityProject, value); }
        }
        #endregion

        /// <summary>
        /// 2.2. Объекты жилищного фонда
        /// </summary>
        #region 2.2. Объекты жилищного фонда
        private double i_LivingSpaceProject;
        [DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.) по проекту (кв.м)"), VisibleInListView(false)]
        public double LivingSpaceProject
        {
            get { return i_LivingSpaceProject; }
            set { SetPropertyValue("LivingSpaceProject", ref i_LivingSpaceProject, value); }
        }

        private double i_TotalLivingSpaceProject;
        [DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.) по проекту (кв.м)"), VisibleInListView(false)]
        public double TotalLivingSpaceProject
        {
            get { return i_TotalLivingSpaceProject; }
            set { SetPropertyValue("TotalLivingSpaceProject", ref i_TotalLivingSpaceProject, value); }
        }

        private double i_NotLivingSpaceProject;
        [DisplayName("Общая площадь нежилых помещений по проекту (кв.м)"), VisibleInListView(false)]
        public double NotLivingSpaceProject
        {
            get { return i_NotLivingSpaceProject; }
            set { SetPropertyValue("NotLivingSpaceProject", ref i_NotLivingSpaceProject, value); }
        }

        private int i_SectionCountProject;
        [DisplayName("Количество секций по проекту(жилые)"), VisibleInListView(false)]
        public int SectionCountProject
        {
            get { return i_SectionCountProject; }
            set { SetPropertyValue("SectionCountProject", ref i_SectionCountProject, value); }
        }

        private int i_PorchCountProject;
        [DisplayName("Количество подъездов по проекту"), VisibleInListView(false)]
        public int PorchCountProject
        {
            get { return i_PorchCountProject; }
            set { SetPropertyValue("PorchCountProject", ref i_PorchCountProject, value); }
        }

        private int i_AppartmentsCountProject;
        [DisplayName("Количество квартир всего по проекту (шт.)"), VisibleInListView(false)]
        public int AppartmentsCountProject
        {
            get { return i_AppartmentsCountProject; }
            set { SetPropertyValue("AppartmentsCountProject", ref i_AppartmentsCountProject, value); }
        }
        private double i_AppartmentsSpaceProject;
        [DisplayName("Жилая площадь квартир проекту (кв.м)"), VisibleInListView(false)]
        public double AppartmentsSpaceProject
        {
            get { return i_AppartmentsSpaceProject; }
            set { SetPropertyValue("AppartmentsSpaceProject", ref i_AppartmentsSpaceProject, value); }
        }
        private int i_StudioCountProject;
        [DisplayName("Количество студии по проекту (шт.)"), VisibleInListView(false)]
        public int StudioCountProject
        {
            get { return i_StudioCountProject; }
            set { SetPropertyValue("StudioCountProject", ref i_StudioCountProject, value); }
        }
        private double i_StudioSpaceProject;
        [DisplayName("Площадь студий по проекту (кв.м)"), VisibleInListView(false)]
        public double StudioSpaceProject
        {
            get { return i_StudioSpaceProject; }
            set { SetPropertyValue("StudioSpaceProject", ref i_StudioSpaceProject, value); }
        }
        private int i_OneRoomFlatCountProject;
        [DisplayName("Количество 1-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        public int OneRoomFlatCountProject
        {
            get { return i_OneRoomFlatCountProject; }
            set { SetPropertyValue("OneRoomFlatCountProject", ref i_OneRoomFlatCountProject, value); }
        }
        private double i_OneRoomFlatSpaceProject;
        [DisplayName("Площадь 1-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        public double OneRoomFlatSpaceProject
        {
            get { return i_OneRoomFlatSpaceProject; }
            set { SetPropertyValue("OneRoomFlatSpaceProject", ref i_OneRoomFlatSpaceProject, value); }
        }
        private int i_TwoRoomFlatCountProject;
        [DisplayName("Количество 2-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        public int TwoRoomFlatCountProject
        {
            get { return i_TwoRoomFlatCountProject; }
            set { SetPropertyValue("TwoRoomFlatCountProject", ref i_TwoRoomFlatCountProject, value); }
        }
        private double i_TwoRoomFlatSpaceProject;
        [DisplayName("Площадь 2-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        public double TwoRoomFlatSpaceProject
        {
            get { return i_TwoRoomFlatSpaceProject; }
            set { SetPropertyValue("TwoRoomFlatSpaceProject", ref i_TwoRoomFlatSpaceProject, value); }
        }
        private int i_ThreeRoomFlatCountProject;
        [DisplayName("Количество 3-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        public int ThreeRoomFlatCountProject
        {
            get { return i_ThreeRoomFlatCountProject; }
            set { SetPropertyValue("ThreeRoomFlatCountProject", ref i_ThreeRoomFlatCountProject, value); }
        }
        private double i_ThreeRoomFlatSpaceProject;
        [DisplayName("Площадь 3-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        public double ThreeRoomFlatSpaceProject
        {
            get { return i_ThreeRoomFlatSpaceProject; }
            set { SetPropertyValue("ThreeRoomFlatSpaceProject", ref i_ThreeRoomFlatSpaceProject, value); }
        }
        private int i_FourRoomFlatCountProject;
        [DisplayName("Количество 4-комнатных квартир по проекту (шт.)"), VisibleInListView(false)]
        public int FourRoomFlatCountProject
        {
            get { return i_FourRoomFlatCountProject; }
            set { SetPropertyValue("FourRoomFlatCountProject", ref i_FourRoomFlatCountProject, value); }
        }
        private double i_FourRoomSpaceProject;
        [DisplayName("Площадь 4-комнатных квартир по проекту (кв.м)"), VisibleInListView(false)]
        public double FourRoomSpaceProject
        {
            get { return i_FourRoomSpaceProject; }
            set { SetPropertyValue("FourRoomSpaceProject", ref i_FourRoomSpaceProject, value); }
        }
        private int i_MoreRoomFlatCountProject;
        [DisplayName("Количество квартир > 4 комнат по проекту (шт.)"), VisibleInListView(false)]
        public int MoreRoomFlatCountProject
        {
            get { return i_MoreRoomFlatCountProject; }
            set { SetPropertyValue("MoreRoomFlatCountProject", ref i_MoreRoomFlatCountProject, value); }
        }
        private double i_MoreRoomSpaceProject;
        [DisplayName("Площадь квартир > 4 комнат по проекту (кв.м)"), VisibleInListView(false)]
        public double MoreRoomSpaceProject
        {
            get { return i_MoreRoomSpaceProject; }
            set { SetPropertyValue("MoreRoomSpaceProject", ref i_MoreRoomSpaceProject, value); }
        }
        private int i_ElitAppartmentsCountProject;
        [DisplayName("Количество квартир > 150м2 по проекту (шт.)"), VisibleInListView(false)]
        public int ElitAppartmentsCountProject
        {
            get { return i_ElitAppartmentsCountProject; }
            set { SetPropertyValue("ElitAppartmentsCountProject", ref i_ElitAppartmentsCountProject, value); }
        }
        private double i_ElitAppartmentsSpaceProject;
        [DisplayName("Площадь квартир > 150м2 по проекту (кв.м)"), VisibleInListView(false)]
        public double ElitAppartmentsSpaceProject
        {
            get { return i_ElitAppartmentsSpaceProject; }
            set { SetPropertyValue("ElitAppartmentsSpaceProject", ref i_ElitAppartmentsSpaceProject, value); }
        }
        #endregion

        /// <summary>
        /// 3. Объекты производственного назначения
        /// </summary>
        #region 3. Объекты производственного назначения
        private ProductPurposeObjectType i_ProductPurposeObjectType;
        [DisplayName("Тип объекта производственного назначения")]
        public ProductPurposeObjectType ProductPurposeObjectType
        {
            get { return i_ProductPurposeObjectType; }
            set { SetPropertyValue("ProductPurposeObjectType", ref i_ProductPurposeObjectType, value); }
        }

        private string i_PowerProject;
        [DisplayName("Мощность по проекту(производство)"), VisibleInListView(false)]
        public string PowerProject
        {
            get { return i_PowerProject; }
            set { SetPropertyValue("PowerProject", ref i_PowerProject, value); }
        }

        private string i_ProductivityProject;
        [DisplayName("Производительность по проекту"), VisibleInListView(false)]
        public string ProductivityProject
        {
            get { return i_ProductivityProject; }
            set { SetPropertyValue("ProductivityProject", ref i_ProductivityProject, value); }
        }
        #endregion

        /// <summary>
        /// 4. Линейные объекты
        /// </summary>
        #region 4. Линейные объекты
        private LineObjectClass i_LineObjectClass;
        [DisplayName("Категория (класс) линейного объекта"), VisibleInListView(false)]
        public LineObjectClass LineObjectClass
        {
            get { return i_LineObjectClass; }
            set { SetPropertyValue("LineObjectClass", ref i_LineObjectClass, value); }
        }

        private double i_LengthProject;
        [DisplayName("Протяженность по проекту"), VisibleInListView(false)]
        public double LengthProject
        {
            get { return i_LengthProject; }
            set { SetPropertyValue("LengthProject", ref i_LengthProject, value); }
        }

        private string i_PowerLineProject;
        [DisplayName("Мощность по проекту(линейный)"), VisibleInListView(false)]
        public string PowerLineProject
        {
            get { return i_PowerLineProject; }
            set { SetPropertyValue("PowerLineProject", ref i_PowerLineProject, value); }
        }

        private string i_PipesInfoProject;
        [Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб по проекту"), VisibleInListView(false)]
        public string PipesInfoProject
        {
            get { return i_PipesInfoProject; }
            set { SetPropertyValue("PipesInfoProject", ref i_PipesInfoProject, value); }
        }

        private string i_ElectricLinesInfoProject;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи по проекту"), VisibleInListView(false)]
        public string ElectricLinesInfoProject
        {
            get { return i_ElectricLinesInfoProject; }
            set { SetPropertyValue("ElectricLinesInfoProject", ref i_ElectricLinesInfoProject, value); }
        }

        private string i_ConstructiveElementsInfoProject;
        [Size(1000), DisplayName("Перечень конструктивных элементов по проекту"), VisibleInListView(false)]
        public string ConstructiveElementsInfoProject
        {
            get { return i_ConstructiveElementsInfoProject; }
            set { SetPropertyValue("ConstructiveElementsInfoProject", ref i_ConstructiveElementsInfoProject, value); }
        }
        #endregion

        /// <summary>
        /// 5. Соответствие требованиям энергетической эффективности
        /// </summary>
        #region 5. Соответствие требованиям энергетической эффективности
        private EnergyEfficiencyClass i_EnergyEfficiencyClassProject;
        [DisplayName("Класс энергоэффективности по проекту"), VisibleInListView(false)]
        public EnergyEfficiencyClass EnergyEfficiencyClassProject
        {
            get { return i_EnergyEfficiencyClassProject; }
            set { SetPropertyValue("EnergyEfficiencyClassProject", ref i_EnergyEfficiencyClassProject, value); }
        }
        private dHeatUnit i_HeatUnit;
        [DisplayName("Единица расхода тепловой энергии"), VisibleInListView(false)]
        public dHeatUnit HeatUnit
        {
            get { return i_HeatUnit; }
            set { SetPropertyValue("HeatUnit", ref i_HeatUnit, value); }
        }
        private double i_HeatConsumptionProject;
        [DisplayName("Удельный расход тепловой энергии на 1 кв.м. площади по проекту"), VisibleInListView(false)]
        public double HeatConsumptionProject
        {
            get { return i_HeatConsumptionProject; }
            set { SetPropertyValue("HeatConsumptionProject", ref i_HeatConsumptionProject, value); }
        }

        private string i_OutdoorIsolationMaterialProject;
        [Size(1000), DisplayName("Материалы утепления наружных ограждающих конструкций по проекту"), VisibleInListView(false)]
        public string OutdoorIsolationMaterialProject
        {
            get { return i_OutdoorIsolationMaterialProject; }
            set { SetPropertyValue("OutdoorIsolationMaterialProject", ref i_OutdoorIsolationMaterialProject, value); }
        }

        private string i_SkylightsFillingProject;
        [Size(1000), DisplayName("Заполнение световых проемов по проекту"), VisibleInListView(false)]
        public string SkylightsFillingProject
        {
            get { return i_SkylightsFillingProject; }
            set { SetPropertyValue("SkylightsFillingProject", ref i_SkylightsFillingProject, value); }
        }
        #endregion

        
        [Association, DisplayName("Разрешение на строительство"), VisibleInListView(false)]
        public XPCollection<ConstrPerm> ConstrPerms
        {
            get
            {
                return GetCollection<ConstrPerm>("ConstrPerms");
            }
        }
        [Association, DisplayName("Разрешение на ввод"), VisibleInListView(false)]
        public XPCollection<UsePerm> UsePerms
        {
            get
            {
                return GetCollection<UsePerm>("UsePerms");
            }
        }
        [Association, DisplayName("Уведомление на строительство"), VisibleInListView(false)]
        public XPCollection<PermNoticeIZD> PermNoticeIZDs
        {
            get
            {
                return GetCollection<PermNoticeIZD>("PermNoticeIZDs");
            }
        }
        [Association, DisplayName("Уведомление на ввод"), VisibleInListView(false)]
        public XPCollection<UsePermNoticeIZD> UsePermNoticeIZDs
        {
            get
            {
                return GetCollection<UsePermNoticeIZD>("UsePermNoticeIZDs");
            }
        }

        //private ConstrPerm i_ConstrPerm;
        //[Association, DisplayName("Разрешение на строительство")]
        //public ConstrPerm ConstrPerm
        //{
        //    get { return i_ConstrPerm; }
        //    set { SetPropertyValue("ConstrPerm", ref i_ConstrPerm, value); }
        //}
        //private UsePerm i_UsePerm;
        //[Association, DisplayName("Разрешение на ввод")]
        //public UsePerm UsePerm
        //{
        //    get { return i_UsePerm; }
        //    set { SetPropertyValue("UsePerm", ref i_UsePerm, value); }
        //}
        //[Association, DisplayName("Сведения об объекте капитального строительства"), VisibleInListView(false)]
        //public XPCollection<ObjectConstractionInfo> ObjectConstractionInfo
        //{
        //    get
        //    {
        //        return GetCollection<ObjectConstractionInfo>("ObjectConstractionInfo");
        //    }
        //}
        //[Association("GeneralDocBase-CapitalStructureBase"), DisplayName("Документы")]
        //public XPCollection<GeneralDocBase> generalDocBase
        //{
        //    get
        //    {
        //        return GetCollection<GeneralDocBase>("generalDocBase");
        //    }
        //}

        [Association, DisplayName("Земельные участки")]
        public XPCollection<Parcel> Parcels
        {
            get
            {
                return GetCollection<Parcel>("Parcels");
            }
        }
        [Association, DisplayName("Застройщики")]
        public XPCollection<GeneralSubject> DocSubjects
        {
            get { return GetCollection<GeneralSubject>("DocSubjects"); }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if(i_ProjectHouseNo != null && i_ProjectHouseNo != "")
            {
                if(!Address.Contains(i_ProjectHouseNo))
                    Address += ", " + i_ProjectHouseNo;
            }
        }

        #region Логика при изменении XPCollections
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //изменилась коллекция застройщиков
            if (property.Name == "DocSubjects")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(DocSubjects_CollectionChanged);
            }
            return result;
        }
        ///// <summary>
        ///// Функция, срабатывающая при изменении коллекции застройщиков - заполнение поля по застройщикам
        ///// </summary>
        private void DocSubjects_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetDevelopers();
            }
        }
        /// <summary>
        /// Заполенение поля Застройщик (string) по листу застройщиков (XPCollection)
        /// </summary>
        public void SetDevelopers()
        {
            string dev = "";
            string contact = "";
            foreach (GeneralSubject subj in DocSubjects)
            {
                if (dev == "")
                {
                    dev = subj.FullName;
                }
                else
                    if (!dev.Contains(subj.FullName))
                    dev += ", " + subj.FullName;
                if (contact == "")
                {
                    contact = subj.FullContactInfo;
                }
                else
                    if (!contact.Contains(subj.FullContactInfo))
                    contact += ", " + subj.FullContactInfo;

            }
            
            ConstrDeveloperString = dev;
            ConstrDeveloperContactInfo = contact;
        }
        #endregion
    }
}
