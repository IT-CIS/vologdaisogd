﻿using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.Land;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Constr
{
    /// <summary>
    /// Сведения об объекте капитального строительства
    /// </summary>
    [ModelDefault("Caption", "Проектные и фактические показатели объекта")]
    public class ObjectConstractionInfo : BaseObjectXAF
    {
        public ObjectConstractionInfo(Session session) : base(session) { }

        private double _Height;
        private double _BuildSquare;
        private double _LotSquare;
        private double _ConstructionCost;
        private double _SpecificCost;
        private double _ConstrWorkCost;
        private double _Cost;
        

        //private CapitalStructureBase i_CapitalStructureBase;
        //[Association, DisplayName("Объект")]
        //public CapitalStructureBase CapitalStructureBase
        //{
        //    get { return i_CapitalStructureBase; }
        //    set { SetPropertyValue("CapitalStructureBase", ref i_CapitalStructureBase, value); }
        //}

        //private ConstrPerm i_ConstrPerm;
        //[Association, DisplayName("Разрешение на строительство")]
        //public ConstrPerm ConstrPerm
        //{
        //    get { return i_ConstrPerm; }
        //    set { SetPropertyValue("ConstrPerm", ref i_ConstrPerm, value); }
        //}

        //private UsePerm i_UsePerm;
        //[Association, DisplayName("Разрешение на ввод объекта в эксплуатацию")]
        //public UsePerm UsePerm
        //{
        //    get { return i_UsePerm; }
        //    set { SetPropertyValue("UsePerm", ref i_UsePerm, value); }
        //}
        //private ConstrStage i_ConstrStage;
        //[Association, DisplayName("Этап строительства"), VisibleInListView(false)]
        //public ConstrStage ConstrStage
        //{
        //    get { return i_ConstrStage; }
        //    set { try { SetPropertyValue("ConstrStage", ref i_ConstrStage, value); } catch { } }
        //}
        private eBuildingKind i_BuildingKind;
        [DisplayName("Вид объекта"), VisibleInListView(false)]
        public eBuildingKind BuildingKind
        {
            get { return i_BuildingKind; }
            set { SetPropertyValue("BuildingKind", ref i_BuildingKind, value); }
        }

        //private OKSKind i_OKSKind;
        //[DisplayName("Тип объекта капитального строительства"), VisibleInListView(false)]
        //public OKSKind OKSKind
        //{
        //    get { return i_OKSKind; }
        //    set { SetPropertyValue("OKSKind", ref i_OKSKind, value); }
        //}


        /// <summary>
        /// Общие показатели объекта
        /// </summary>





        /// <summary>
        /// Общие показатели объекта
        /// </summary>
        private double i_BuildingSizeProject;
        [DisplayName("Строительный объем - всего по проекту (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeProject
        {
            get { return i_BuildingSizeProject; }
            set { SetPropertyValue("BuildingSizeProject", ref i_BuildingSizeProject, value); }
        }
        private double i_BuildingSizeFakt;
        [DisplayName("Строительный объем - всего фактически (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeFakt
        {
            get { return i_BuildingSizeFakt; }
            set { SetPropertyValue("BuildingSizeFakt", ref i_BuildingSizeFakt, value); }
        }

        private double i_BuildingSizeOverGroundPartProject;
        [DisplayName("Строительный объем надземной части по проекту (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeOverGroundPartProject
        {
            get { return i_BuildingSizeOverGroundPartProject; }
            set { SetPropertyValue("BuildingSizeOverGroundPartProject", ref i_BuildingSizeOverGroundPartProject, value); }
        }

        private double i_BuildingSizeOverGroundPartFakt;
        [DisplayName("Строительный объем надземной части фактически (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeOverGroundPartFakt
        {
            get { return i_BuildingSizeOverGroundPartFakt; }
            set { SetPropertyValue("BuildingSizeOverGroundPartFakt", ref i_BuildingSizeOverGroundPartFakt, value); }
        }

        private double i_BuildingSizeUnderGroundPartProject;
        [DisplayName("Строительный объем подземной части по проекту (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeUnderGroundPartProject
        {
            get { return i_BuildingSizeUnderGroundPartProject; }
            set { SetPropertyValue("BuildingSizeUnderGroundPartProject", ref i_BuildingSizeUnderGroundPartProject, value); }
        }

        private double i_BuildingSizeUnderGroundPartFakt;
        [DisplayName("Строительный объем подземной части фактически (куб.м)"), VisibleInListView(false)]
        public double BuildingSizeUnderGroundPartFakt
        {
            get { return i_BuildingSizeUnderGroundPartFakt; }
            set { SetPropertyValue("BuildingSizeUnderGroundPartFakt", ref i_BuildingSizeUnderGroundPartFakt, value); }
        }

        private double i_TotalBuildSquareProject;
        [DisplayName("Общая площадь по проекту (кв.м)"), VisibleInListView(false)]
        public double TotalBuildSquareProject
        {
            get { return i_TotalBuildSquareProject; }
            set { SetPropertyValue("TotalBuildSquareProject", ref i_TotalBuildSquareProject, value); }
        }
        private double i_TotalBuildSquareFakt;
        [DisplayName("Общая площадь фактически (кв.м)"), VisibleInListView(false)]
        public double TotalBuildSquareFakt
        {
            get { return i_TotalBuildSquareFakt; }
            set { SetPropertyValue("TotalBuildSquareFakt", ref i_TotalBuildSquareFakt, value); }
        }

        private double i_NotLivingSquareProject;
        [DisplayName("Площадь нежилых помещений по проекту (кв.м)"), VisibleInListView(false)]
        public double NotLivingSquareProject
        {
            get { return i_NotLivingSquareProject; }
            set { SetPropertyValue("NotLivingSquareProject", ref i_NotLivingSquareProject, value); }
        }

        private double i_NotLivingSquareFakt;
        [DisplayName("Площадь нежилых помещений фактически (кв.м)"), VisibleInListView(false)]
        public double NotLivingSquareFakt
        {
            get { return i_NotLivingSquareFakt; }
            set { SetPropertyValue("NotLivingSquareFakt", ref i_NotLivingSquareFakt, value); }
        }

        private double i_OutBuildingSquareProject;
        [DisplayName("Площадь встроенно-пристроенных помещений по проекту (кв.м)"), VisibleInListView(false)]
        public double OutBuildingSquareProject
        {
            get { return i_OutBuildingSquareProject; }
            set { SetPropertyValue("OutBuildingSquareProject", ref i_OutBuildingSquareProject, value); }
        }

        private double i_OutBuildingSquareFakt;
        [DisplayName("Площадь встроенно-пристроенных помещений фактически (кв.м)"), VisibleInListView(false)]
        public double OutBuildingSquareFakt
        {
            get { return i_OutBuildingSquareFakt; }
            set { SetPropertyValue("OutBuildingSquareFakt", ref i_OutBuildingSquareFakt, value); }
        }

        private int i_BuildingCountProject;
        [DisplayName("Количество зданий, сооружений по проекту (шт.)")]
        public int BuildingCountProject
        {
            get { return i_BuildingCountProject; }
            set { SetPropertyValue("BuildingCountProject", ref i_BuildingCountProject, value); }
        }

        private int i_BuildingCountFakt;
        [DisplayName("Количество зданий, сооружений фактически (шт.)")]
        public int BuildingCountFakt
        {
            get { return i_BuildingCountFakt; }
            set { SetPropertyValue("BuildingCountFakt", ref i_BuildingCountFakt, value); }
        }



        /// <summary>
        /// Нежилые объекты
        /// </summary>
        #region Нежилые объекты
        private int i_PlacesCountProject;
        [DisplayName("Количество мест по проекту"), VisibleInListView(false)]
        public int PlacesCountProject
        {
            get { return i_PlacesCountProject; }
            set { SetPropertyValue("PlacesCountProject", ref i_PlacesCountProject, value); }
        }

        private int i_PlacesCountFakt;
        [DisplayName("Количество мест фактически"), VisibleInListView(false)]
        public int PlacesCountFakt
        {
            get { return i_PlacesCountFakt; }
            set { SetPropertyValue("PlacesCountFakt", ref i_PlacesCountFakt, value); }
        }

        private int i_RoomCountProject;
        [DisplayName("Количество помещений по проекту"), VisibleInListView(false)]
        public int RoomCountProject
        {
            get { return i_RoomCountProject; }
            set { SetPropertyValue("RoomCountProject", ref i_RoomCountProject, value); }
        }

        private int i_RoomCountFakt;
        [DisplayName("Количество помещений фактически"), VisibleInListView(false)]
        public int RoomCountFakt
        {
            get { return i_RoomCountFakt; }
            set { SetPropertyValue("RoomCountFakt", ref i_RoomCountFakt, value); }
        }

        private int i_CapacityProject;
        [DisplayName("Вместимость по проекту"), VisibleInListView(false)]
        public int CapacityProject
        {
            get { return i_CapacityProject; }
            set { SetPropertyValue("CapacityProject", ref i_CapacityProject, value); }
        }

        private int i_CapacityFakt;
        [DisplayName("Вместимость фактически"), VisibleInListView(false)]
        public int CapacityFakt
        {
            get { return i_CapacityFakt; }
            set { SetPropertyValue("CapacityFakt", ref i_CapacityFakt, value); }
        }

        private int i_FloorCountNotLivingProject;
        [DisplayName("Количество этажей по проекту")]
        public int FloorCountNotLivingProject
        {
            get { return i_FloorCountNotLivingProject; }
            set { SetPropertyValue("FloorCountNotLivingProject", ref i_FloorCountNotLivingProject, value); }
        }

        private int i_FloorCountNotLivingFakt;
        [DisplayName("Количество этажей фактически")]
        public int FloorCountNotLivingFakt
        {
            get { return i_FloorCountNotLivingFakt; }
            set { SetPropertyValue("FloorCountNotLivingFakt", ref i_FloorCountNotLivingFakt, value); }
        }

        private int i_UnderGroundFloorCountNotLivingProject;
        [DisplayName("Количество этажей подземной части по проекту")]
        public int UnderGroundFloorCountNotLivingProject
        {
            get { return i_UnderGroundFloorCountNotLivingProject; }
            set { SetPropertyValue("UnderGroundFloorCountNotLivingProject", ref i_UnderGroundFloorCountNotLivingProject, value); }
        }

        private int i_UnderGroundFloorCountNotLivingFakt;
        [DisplayName("Количество этажей подземной части фактически")]
        public int UnderGroundFloorCountNotLivingFakt
        {
            get { return i_UnderGroundFloorCountNotLivingFakt; }
            set { SetPropertyValue("UnderGroundFloorCountNotLivingFakt", ref i_UnderGroundFloorCountNotLivingFakt, value); }
        }

        private int i_ElevatorsCountNotLivingProject;
        [DisplayName("Лифты по проекту (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountNotLivingProject
        {
            get { return i_ElevatorsCountNotLivingProject; }
            set { SetPropertyValue("ElevatorsCountNotLivingProject", ref i_ElevatorsCountNotLivingProject, value); }
        }

        private int i_ElevatorsCountNotLivingFakt;
        [DisplayName("Лифты фактически (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountNotLivingFakt
        {
            get { return i_ElevatorsCountNotLivingFakt; }
            set { SetPropertyValue("ElevatorsCountNotLivingFakt", ref i_ElevatorsCountNotLivingFakt, value); }
        }

        private int i_EscalatorsCountNotLivingProject;
        [DisplayName("Эскалаторы по проекту (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountNotLivingProject
        {
            get { return i_EscalatorsCountNotLivingProject; }
            set { SetPropertyValue("EscalatorsCountNotLivingProject", ref i_EscalatorsCountNotLivingProject, value); }
        }

        private int i_EscalatorsCountNotLivingFakt;
        [DisplayName("Эскалаторы фактически (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountNotLivingFakt
        {
            get { return i_EscalatorsCountNotLivingFakt; }
            set { SetPropertyValue("EscalatorsCountNotLivingFakt", ref i_EscalatorsCountNotLivingFakt, value); }
        }

        private int i_InvalidLiftsCountNotLivingProject;
        [DisplayName("Инвалидные подъемники по проекту (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountNotLivingProject
        {
            get { return i_InvalidLiftsCountNotLivingProject; }
            set { SetPropertyValue("InvalidLiftsCountNotLivingProject", ref i_InvalidLiftsCountNotLivingProject, value); }
        }

        private int i_InvalidLiftsCountNotLivingFakt;
        [DisplayName("Инвалидные подъемники фактически (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountNotLivingFakt
        {
            get { return i_InvalidLiftsCountNotLivingFakt; }
            set { SetPropertyValue("InvalidLiftsCountNotLivingFakt", ref i_InvalidLiftsCountNotLivingFakt, value); }
        }

        private Material i_FundMaterialNotLivingProject;
        [DisplayName("Материалы фундаментов по проекту"), VisibleInListView(false)]
        public Material FundMaterialNotLivingProject
        {
            get { return i_FundMaterialNotLivingProject; }
            set { SetPropertyValue("FundMaterialNotLivingProject", ref i_FundMaterialNotLivingProject, value); }
        }

        private Material i_FundMaterialNotLivingFakt;
        [DisplayName("Материалы фундаментов фактически"), VisibleInListView(false)]
        public Material FundMaterialNotLivingFakt
        {
            get { return i_FundMaterialNotLivingFakt; }
            set { SetPropertyValue("FundMaterialNotLivingFakt", ref i_FundMaterialNotLivingFakt, value); }
        }

        private Material i_WallMaterialNotLivingProject;
        [DisplayName("Материалы стен по проекту"), VisibleInListView(false)]
        public Material WallMaterialNotLivingProject
        {
            get { return i_WallMaterialNotLivingProject; }
            set { SetPropertyValue("WallMaterialNotLivingProject", ref i_WallMaterialNotLivingProject, value); }
        }

        private Material i_WallMaterialNotLivingFakt;
        [DisplayName("Материалы стен фактически"), VisibleInListView(false)]
        public Material WallMaterialNotLivingFakt
        {
            get { return i_WallMaterialNotLivingFakt; }
            set { SetPropertyValue("WallMaterialNotLivingFakt", ref i_WallMaterialNotLivingFakt, value); }
        }
        private Material i_BorderMaterialNotLivingProject;
        [DisplayName("Материалы перекрытий по проекту"), VisibleInListView(false)]
        public Material BorderMaterialNotLivingProject
        {
            get { return i_BorderMaterialNotLivingProject; }
            set { SetPropertyValue("BorderMaterialNotLivingProject", ref i_BorderMaterialNotLivingProject, value); }
        }

        private Material i_BorderMaterialNotLivingFakt;
        [DisplayName("Материалы перекрытий фактически"), VisibleInListView(false)]
        public Material BorderMaterialNotLivingFakt
        {
            get { return i_BorderMaterialNotLivingFakt; }
            set { SetPropertyValue("BorderMaterialNotLivingFakt", ref i_BorderMaterialNotLivingFakt, value); }
        }

        private Material i_RoofMaterialNotLivingProject;
        [DisplayName("Материалы кровли по проекту"), VisibleInListView(false)]
        public Material RoofMaterialNotLivingProject
        {
            get { return i_RoofMaterialNotLivingProject; }
            set { SetPropertyValue("RoofMaterialNotLivingProject", ref i_RoofMaterialNotLivingProject, value); }
        }

        private Material i_RoofMaterialNotLivingFakt;
        [DisplayName("Материалы кровли фактически"), VisibleInListView(false)]
        public Material RoofMaterialNotLivingFakt
        {
            get { return i_RoofMaterialNotLivingFakt; }
            set { SetPropertyValue("RoofMaterialNotLivingFakt", ref i_RoofMaterialNotLivingFakt, value); }
        }

        private string i_OtherIndicatorsNotLivingProject;
        [DisplayName("Иные показатели по проекту"), VisibleInListView(false)]
        public string OtherIndicatorsNotLivingProject
        {
            get { return i_OtherIndicatorsNotLivingProject; }
            set { SetPropertyValue("OtherIndicatorsNotLivingProject", ref i_OtherIndicatorsNotLivingProject, value); }
        }

        private string i_OtherIndicatorsNotLivingFakt;
        [DisplayName("Иные показатели фактически"), VisibleInListView(false)]
        public string OtherIndicatorsNotLivingFakt
        {
            get { return i_OtherIndicatorsNotLivingFakt; }
            set { SetPropertyValue("OtherIndicatorsNotLivingFakt", ref i_OtherIndicatorsNotLivingFakt, value); }
        }

        
        #endregion

        /// <summary>
        /// Объекты жилищного фонда
        /// </summary>
        #region Объекты жилищного фонда
        private double i_LivingSpaceProject;
        [DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.) по проекту (кв.м)"), VisibleInListView(false)]
        public double LivingSpaceProject
        {
            get { return i_LivingSpaceProject; }
            set { SetPropertyValue("LivingSpaceProject", ref i_LivingSpaceProject, value); }
        }

        private double i_LivingSpaceFakt;
        [DisplayName("Общая площадь жилых помещений (без учета лоджий, балконов и т.д.) фактически (кв.м)"), VisibleInListView(false)]
        public double LivingSpaceFakt
        {
            get { return i_LivingSpaceFakt; }
            set { SetPropertyValue("LivingSpaceFakt", ref i_LivingSpaceFakt, value); }
        }

        private double i_NotLivingSpaceProject;
        [DisplayName("Общая площадь нежилых помещений по проекту (кв.м)"), VisibleInListView(false)]
        public double NotLivingSpaceProject
        {
            get { return i_NotLivingSpaceProject; }
            set { SetPropertyValue("NotLivingSpaceProject", ref i_NotLivingSpaceProject, value); }
        }

        private double i_NotLivingSpaceFakt;
        [DisplayName("Общая площадь нежилых помещений фактически (кв.м)"), VisibleInListView(false)]
        public double NotLivingSpaceFakt
        {
            get { return i_NotLivingSpaceFakt; }
            set { SetPropertyValue("NotLivingSpaceFakt", ref i_NotLivingSpaceFakt, value); }
        }

        private int i_FloorCountLivingProject;
        [DisplayName("Количество этажей по проекту")]
        public int FloorCountLivingProject
        {
            get { return i_FloorCountLivingProject; }
            set { SetPropertyValue("FloorCountLivingProject", ref i_FloorCountLivingProject, value); }
        }

        private int i_FloorCountLivingFakt;
        [DisplayName("Количество этажей фактически")]
        public int FloorCountLivingFakt
        {
            get { return i_FloorCountLivingFakt; }
            set { SetPropertyValue("FloorCountLivingFakt", ref i_FloorCountLivingFakt, value); }
        }

        private int i_UnderGroundFloorCountLivingProject;
        [DisplayName("Количество этажей подземной части по проекту")]
        public int UnderGroundFloorCountLivingProject
        {
            get { return i_UnderGroundFloorCountLivingProject; }
            set { SetPropertyValue("UnderGroundFloorCountLivingProject", ref i_UnderGroundFloorCountLivingProject, value); }
        }

        private int i_UnderGroundFloorCountLivingFakt;
        [DisplayName("Количество этажей подземной части фактически")]
        public int UnderGroundFloorCountLivingFakt
        {
            get { return i_UnderGroundFloorCountLivingFakt; }
            set { SetPropertyValue("UnderGroundFloorCountLivingFakt", ref i_UnderGroundFloorCountLivingFakt, value); }
        }

        private int i_SectionCountProject;
        [DisplayName("Количество секций по проекту")]
        public int SectionCountProject
        {
            get { return i_SectionCountProject; }
            set { SetPropertyValue("SectionCountProject", ref i_SectionCountProject, value); }
        }

        private int i_SectionCountFakt;
        [DisplayName("Количество секций фактически")]
        public int SectionCountFakt
        {
            get { return i_SectionCountFakt; }
            set { SetPropertyValue("SectionCountFakt", ref i_SectionCountFakt, value); }
        }

        private string i_AppartmentsCountAndSpaceProject;
        [DisplayName("Количество квартир/общая площадь, всего по проекту (шт./кв.м)")]
        public string AppartmentsCountAndSpaceProject
        {
            get { return i_AppartmentsCountAndSpaceProject; }
            set { SetPropertyValue("AppartmentsCountAndSpaceProject", ref i_AppartmentsCountAndSpaceProject, value); }
        }

        private string i_AppartmentsCountAndSpaceFakt;
        [DisplayName("Количество квартир/общая площадь, всего фактически (шт./кв.м)")]
        public string AppartmentsCountAndSpaceFakt
        {
            get { return i_AppartmentsCountAndSpaceFakt; }
            set { SetPropertyValue("AppartmentsCountAndSpaceFakt", ref i_AppartmentsCountAndSpaceFakt, value); }
        }
        private string i_OneRoomFlatCountAndSpaceProject;
        [DisplayName("1-комнатные по проекту (шт./кв.м)"), VisibleInListView(false)]
        public string OneRoomFlatCountAndSpaceProject
        {
            get { return i_OneRoomFlatCountAndSpaceProject; }
            set { SetPropertyValue("OneRoomFlatCountAndSpaceProject", ref i_OneRoomFlatCountAndSpaceProject, value); }
        }
        private string i_OneRoomFlatCountAndSpaceFakt;
        [DisplayName("1-комнатные фактически (шт./кв.м)"), VisibleInListView(false)]
        public string OneRoomFlatCountAndSpaceFakt
        {
            get { return i_OneRoomFlatCountAndSpaceFakt; }
            set { SetPropertyValue("OneRoomFlatCountAndSpaceFakt", ref i_OneRoomFlatCountAndSpaceFakt, value); }
        }

        private string i_TwoRoomFlatCountAndSpaceProject;
        [DisplayName("2-комнатные по проекту (шт./кв.м)"), VisibleInListView(false)]
        public string TwoRoomFlatCountAndSpaceProject
        {
            get { return i_TwoRoomFlatCountAndSpaceProject; }
            set { SetPropertyValue("TwoRoomFlatCountAndSpaceProject", ref i_TwoRoomFlatCountAndSpaceProject, value); }
        }
        private string i_TwoRoomFlatCountAndSpaceFakt;
        [DisplayName("2-комнатные фактически (шт./кв.м)"), VisibleInListView(false)]
        public string TwoRoomFlatCountAndSpaceFakt
        {
            get { return i_TwoRoomFlatCountAndSpaceFakt; }
            set { SetPropertyValue("TwoRoomFlatCountAndSpaceFakt", ref i_TwoRoomFlatCountAndSpaceFakt, value); }
        }

        private string i_ThreeRoomFlatCountAndSpaceProject;
        [DisplayName("3-комнатные по проекту (шт./кв.м)"), VisibleInListView(false)]
        public string ThreeRoomFlatCountAndSpaceProject
        {
            get { return i_ThreeRoomFlatCountAndSpaceProject; }
            set { SetPropertyValue("ThreeRoomFlatCountAndSpaceProject", ref i_ThreeRoomFlatCountAndSpaceProject, value); }
        }
        private string i_ThreeRoomFlatCountAndSpaceFakt;
        [DisplayName("3-комнатные фактически (шт./кв.м)"), VisibleInListView(false)]
        public string ThreeRoomFlatCountAndSpaceFakt
        {
            get { return i_ThreeRoomFlatCountAndSpaceFakt; }
            set { SetPropertyValue("ThreeRoomFlatCountAndSpaceFakt", ref i_ThreeRoomFlatCountAndSpaceFakt, value); }
        }

        private string i_FourRoomFlatCountAndSpaceProject;
        [DisplayName("4-комнатные по проекту (шт./кв.м)"), VisibleInListView(false)]
        public string FourRoomFlatCountAndSpaceProject
        {
            get { return i_FourRoomFlatCountAndSpaceProject; }
            set { SetPropertyValue("FourRoomFlatCountAndSpaceProject", ref i_FourRoomFlatCountAndSpaceProject, value); }
        }
        private string i_FourRoomFlatCountAndSpaceFakt;
        [DisplayName("4-комнатные фактически (шт./кв.м)"), VisibleInListView(false)]
        public string FourRoomFlatCountAndSpaceFakt
        {
            get { return i_FourRoomFlatCountAndSpaceFakt; }
            set { SetPropertyValue("FourRoomFlatCountAndSpaceFakt", ref i_FourRoomFlatCountAndSpaceFakt, value); }
        }

        private string i_MoreRoomFlatCountAndSpaceProject;
        [DisplayName("более чем 4-комнатные по проекту (шт./кв.м)"), VisibleInListView(false)]
        public string MoreRoomFlatCountAndSpaceProject
        {
            get { return i_MoreRoomFlatCountAndSpaceProject; }
            set { SetPropertyValue("MoreRoomFlatCountAndSpaceProject", ref i_MoreRoomFlatCountAndSpaceProject, value); }
        }
        private string i_MoreRoomFlatCountAndSpaceFakt;
        [DisplayName("более чем 4-комнатные фактически (шт./кв.м)"), VisibleInListView(false)]
        public string MoreRoomFlatCountAndSpaceFakt
        {
            get { return i_MoreRoomFlatCountAndSpaceFakt; }
            set { SetPropertyValue("MoreRoomFlatCountAndSpaceFakt", ref i_MoreRoomFlatCountAndSpaceFakt, value); }
        }

        private double i_TotalLivingSpaceProject;
        [DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.) по проекту (кв.м)"), VisibleInListView(false)]
        public double TotalLivingSpaceProject
        {
            get { return i_TotalLivingSpaceProject; }
            set { SetPropertyValue("TotalLivingSpaceProject", ref i_TotalLivingSpaceProject, value); }
        }

        private double i_TotalLivingSpaceFakt;
        [DisplayName("Общая площадь жилых помещений (с учетом балконов, лоджий и т.д.) фактически (кв.м)"), VisibleInListView(false)]
        public double TotalLivingSpaceFakt
        {
            get { return i_TotalLivingSpaceFakt; }
            set { SetPropertyValue("TotalLivingSpaceFakt", ref i_TotalLivingSpaceFakt, value); }
        }

        private int i_ElevatorsCountLivingProject;
        [DisplayName("Лифты по проекту (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountLivingProject
        {
            get { return i_ElevatorsCountLivingProject; }
            set { SetPropertyValue("ElevatorsCountLivingProject", ref i_ElevatorsCountLivingProject, value); }
        }

        private int i_ElevatorsCountLivingFakt;
        [DisplayName("Лифты фактически (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountLivingFakt
        {
            get { return i_ElevatorsCountLivingFakt; }
            set { SetPropertyValue("ElevatorsCountLivingFakt", ref i_ElevatorsCountLivingFakt, value); }
        }

        private int i_EscalatorsCountLivingProject;
        [DisplayName("Эскалаторы по проекту (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountLivingProject
        {
            get { return i_EscalatorsCountLivingProject; }
            set { SetPropertyValue("EscalatorsCountLivingProject", ref i_EscalatorsCountLivingProject, value); }
        }

        private int i_EscalatorsCountLivingFakt;
        [DisplayName("Эскалаторы фактически (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountLivingFakt
        {
            get { return i_EscalatorsCountLivingFakt; }
            set { SetPropertyValue("EscalatorsCountLivingFakt", ref i_EscalatorsCountLivingFakt, value); }
        }

        private int i_InvalidLiftsCountLivingProject;
        [DisplayName("Инвалидные подъемники по проекту (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountLivingProject
        {
            get { return i_InvalidLiftsCountLivingProject; }
            set { SetPropertyValue("InvalidLiftsCountLivingProject", ref i_InvalidLiftsCountLivingProject, value); }
        }

        private int i_InvalidLiftsCountLivingFakt;
        [DisplayName("Инвалидные подъемники фактически (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountLivingFakt
        {
            get { return i_InvalidLiftsCountLivingFakt; }
            set { SetPropertyValue("InvalidLiftsCountLivingFakt", ref i_InvalidLiftsCountLivingFakt, value); }
        }

        private Material i_FundMaterialLivingProject;
        [DisplayName("Материалы фундаментов по проекту"), VisibleInListView(false)]
        public Material FundMaterialLivingProject
        {
            get { return i_FundMaterialLivingProject; }
            set { SetPropertyValue("FundMaterialLivingProject", ref i_FundMaterialLivingProject, value); }
        }

        private Material i_FundMaterialLivingFakt;
        [DisplayName("Материалы фундаментов фактически"), VisibleInListView(false)]
        public Material FundMaterialLivingFakt
        {
            get { return i_FundMaterialLivingFakt; }
            set { SetPropertyValue("FundMaterialLivingFakt", ref i_FundMaterialLivingFakt, value); }
        }

        private Material i_WallMaterialLivingProject;
        [DisplayName("Материалы стен по проекту"), VisibleInListView(false)]
        public Material WallMaterialLivingProject
        {
            get { return i_WallMaterialLivingProject; }
            set { SetPropertyValue("WallMaterialLivingProject", ref i_WallMaterialLivingProject, value); }
        }

        private Material i_WallMaterialLivingFakt;
        [DisplayName("Материалы стен фактически"), VisibleInListView(false)]
        public Material WallMaterialLivingFakt
        {
            get { return i_WallMaterialLivingFakt; }
            set { SetPropertyValue("WallMaterialLivingFakt", ref i_WallMaterialLivingFakt, value); }
        }
        private Material i_BorderMaterialLivingProject;
        [DisplayName("Материалы перекрытий по проекту"), VisibleInListView(false)]
        public Material BorderMaterialLivingProject
        {
            get { return i_BorderMaterialLivingProject; }
            set { SetPropertyValue("BorderMaterialLivingProject", ref i_BorderMaterialLivingProject, value); }
        }

        private Material i_BorderMaterialLivingFakt;
        [DisplayName("Материалы перекрытий фактически"), VisibleInListView(false)]
        public Material BorderMaterialLivingFakt
        {
            get { return i_BorderMaterialLivingFakt; }
            set { SetPropertyValue("BorderMaterialLivingFakt", ref i_BorderMaterialLivingFakt, value); }
        }

        private Material i_RoofMaterialLivingProject;
        [DisplayName("Материалы кровли по проекту"), VisibleInListView(false)]
        public Material RoofMaterialLivingProject
        {
            get { return i_RoofMaterialLivingProject; }
            set { SetPropertyValue("RoofMaterialLivingProject", ref i_RoofMaterialLivingProject, value); }
        }

        private Material i_RoofMaterialLivingFakt;
        [DisplayName("Материалы кровли фактически"), VisibleInListView(false)]
        public Material RoofMaterialLivingFakt
        {
            get { return i_RoofMaterialLivingFakt; }
            set { SetPropertyValue("RoofMaterialLivingFakt", ref i_RoofMaterialLivingFakt, value); }
        }

        private string i_OtherIndicatorsLivingProject;
        [DisplayName("Иные показатели по проекту"), VisibleInListView(false)]
        public string OtherIndicatorsLivingProject
        {
            get { return i_OtherIndicatorsLivingProject; }
            set { SetPropertyValue("OtherIndicatorsLivingProject", ref i_OtherIndicatorsLivingProject, value); }
        }

        private string i_OtherIndicatorsLivingFakt;
        [DisplayName("Иные показатели фактически"), VisibleInListView(false)]
        public string OtherIndicatorsLivingFakt
        {
            get { return i_OtherIndicatorsLivingFakt; }
            set { SetPropertyValue("OtherIndicatorsLivingFakt", ref i_OtherIndicatorsLivingFakt, value); }
        }
        #endregion

        /// <summary>
        [DisplayName("Стоимость строительства объекта (тыс.руб)")]
        public double constructionCost
        {
            get { return _ConstructionCost; }
            set { try { _ConstructionCost = value; } catch { } }
        }
        [DisplayName("Удельная стоимость 1 кв.м. (тыс.руб)")]
        public double specificCost
        {
            get { return _SpecificCost; }
            set { try { _SpecificCost = value; } catch { } }
        }
        [DisplayName("Стоимость строительно-монтажных работ (тыс.руб)"), VisibleInListView(false)]
        public double constrWorkCost
        {
            get { return _ConstrWorkCost; }
            set { try { _ConstrWorkCost = value; } catch { } }
        }
        [DisplayName("Стоимость введенных в действие основных фондов (тыс.руб)"), VisibleInListView(false)]
        public double cost
        {
            get { return _Cost; }
            set { try { _Cost = value; } catch { } }
        }

        /// <summary>
        /// Объекты производственного назначения
        /// </summary>

        private ProductPurposeObjectType i_ProductPurposeObjectType;
        [DisplayName("Тип объекта производственного назначения")]
        public ProductPurposeObjectType ProductPurposeObjectType
        {
            get { return i_ProductPurposeObjectType; }
            set { SetPropertyValue("ProductPurposeObjectType", ref i_ProductPurposeObjectType, value); }
        }

        private string i_PowerProject;
        [DisplayName("Мощность по проекту"), VisibleInListView(false)]
        public string PowerProject
        {
            get { return i_PowerProject; }
            set { SetPropertyValue("PowerProject", ref i_PowerProject, value); }
        }
        private string i_PowerFakt;
        [DisplayName("Мощность фактически"), VisibleInListView(false)]
        public string PowerFakt
        {
            get { return i_PowerFakt; }
            set { SetPropertyValue("PowerFakt", ref i_PowerFakt, value); }
        }

        private string i_ProductivityProject;
        [DisplayName("Производительность по проекту"), VisibleInListView(false)]
        public string ProductivityProject
        {
            get { return i_ProductivityProject; }
            set { SetPropertyValue("ProductivityProject", ref i_ProductivityProject, value); }
        }

        private string i_ProductivityFakt;
        [DisplayName("Производительность фактически"), VisibleInListView(false)]
        public string ProductivityFakt
        {
            get { return i_ProductivityFakt; }
            set { SetPropertyValue("ProductivityFakt", ref i_ProductivityFakt, value); }
        }

        private int i_ElevatorsCountProductProject;
        [DisplayName("Лифты по проекту (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountProductProject
        {
            get { return i_ElevatorsCountProductProject; }
            set { SetPropertyValue("ElevatorsCountProductProject", ref i_ElevatorsCountProductProject, value); }
        }

        private int i_ElevatorsCountProductFakt;
        [DisplayName("Лифты фактически (шт.)"), VisibleInListView(false)]
        public int ElevatorsCountProductFakt
        {
            get { return i_ElevatorsCountProductFakt; }
            set { SetPropertyValue("ElevatorsCountProductFakt", ref i_ElevatorsCountProductFakt, value); }
        }

        private int i_EscalatorsCountProductProject;
        [DisplayName("Эскалаторы по проекту (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountProductProject
        {
            get { return i_EscalatorsCountProductProject; }
            set { SetPropertyValue("EscalatorsCountProductProject", ref i_EscalatorsCountProductProject, value); }
        }

        private int i_EscalatorsCountProductFakt;
        [DisplayName("Эскалаторы фактически (шт.)"), VisibleInListView(false)]
        public int EscalatorsCountProductFakt
        {
            get { return i_EscalatorsCountProductFakt; }
            set { SetPropertyValue("EscalatorsCountProductFakt", ref i_EscalatorsCountProductFakt, value); }
        }

        private int i_InvalidLiftsCountProductProject;
        [DisplayName("Инвалидные подъемники по проекту (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountProductProject
        {
            get { return i_InvalidLiftsCountProductProject; }
            set { SetPropertyValue("InvalidLiftsCountProductProject", ref i_InvalidLiftsCountProductProject, value); }
        }

        private int i_InvalidLiftsCountProductFakt;
        [DisplayName("Инвалидные подъемники фактически (шт.)"), VisibleInListView(false)]
        public int InvalidLiftsCountProductFakt
        {
            get { return i_InvalidLiftsCountProductFakt; }
            set { SetPropertyValue("InvalidLiftsCountProductFakt", ref i_InvalidLiftsCountProductFakt, value); }
        }

        private Material i_FundMaterialProductProject;
        [DisplayName("Материалы фундаментов по проекту"), VisibleInListView(false)]
        public Material FundMaterialProductProject
        {
            get { return i_FundMaterialProductProject; }
            set { SetPropertyValue("FundMaterialProductProject", ref i_FundMaterialProductProject, value); }
        }

        private Material i_FundMaterialProductFakt;
        [DisplayName("Материалы фундаментов фактически"), VisibleInListView(false)]
        public Material FundMaterialProductFakt
        {
            get { return i_FundMaterialProductFakt; }
            set { SetPropertyValue("FundMaterialProductFakt", ref i_FundMaterialProductFakt, value); }
        }

        private Material i_WallMaterialProductProject;
        [DisplayName("Материалы стен по проекту"), VisibleInListView(false)]
        public Material WallMaterialProductProject
        {
            get { return i_WallMaterialProductProject; }
            set { SetPropertyValue("WallMaterialProductProject", ref i_WallMaterialProductProject, value); }
        }

        private Material i_WallMaterialProductFakt;
        [DisplayName("Материалы стен фактически"), VisibleInListView(false)]
        public Material WallMaterialProductFakt
        {
            get { return i_WallMaterialProductFakt; }
            set { SetPropertyValue("WallMaterialProductFakt", ref i_WallMaterialProductFakt, value); }
        }
        private Material i_BorderMaterialProductProject;
        [DisplayName("Материалы перекрытий по проекту"), VisibleInListView(false)]
        public Material BorderMaterialProductProject
        {
            get { return i_BorderMaterialProductProject; }
            set { SetPropertyValue("BorderMaterialProductProject", ref i_BorderMaterialProductProject, value); }
        }

        private Material i_BorderMaterialProductFakt;
        [DisplayName("Материалы перекрытий фактически"), VisibleInListView(false)]
        public Material BorderMaterialProductFakt
        {
            get { return i_BorderMaterialProductFakt; }
            set { SetPropertyValue("BorderMaterialProductFakt", ref i_BorderMaterialProductFakt, value); }
        }

        private Material i_RoofMaterialProductProject;
        [DisplayName("Материалы кровли по проекту"), VisibleInListView(false)]
        public Material RoofMaterialProductProject
        {
            get { return i_RoofMaterialProductProject; }
            set { SetPropertyValue("RoofMaterialProductProject", ref i_RoofMaterialProductProject, value); }
        }

        private Material i_RoofMaterialProductFakt;
        [DisplayName("Материалы кровли фактически"), VisibleInListView(false)]
        public Material RoofMaterialProductFakt
        {
            get { return i_RoofMaterialProductFakt; }
            set { SetPropertyValue("RoofMaterialProductFakt", ref i_RoofMaterialProductFakt, value); }
        }

        private string i_OtherIndicatorsProductProject;
        [DisplayName("Иные показатели по проекту"), VisibleInListView(false)]
        public string OtherIndicatorsProductProject
        {
            get { return i_OtherIndicatorsProductProject; }
            set { SetPropertyValue("OtherIndicatorsProductProject", ref i_OtherIndicatorsProductProject, value); }
        }

        private string i_OtherIndicatorsProductFakt;
        [DisplayName("Иные показатели фактически"), VisibleInListView(false)]
        public string OtherIndicatorsProductFakt
        {
            get { return i_OtherIndicatorsProductFakt; }
            set { SetPropertyValue("OtherIndicatorsProductFakt", ref i_OtherIndicatorsProductFakt, value); }
        }


        /// <summary>
        /// Линейные объекты
        /// </summary>
        private LineObjectClass i_LineObjectClass;
        [DisplayName("Категория (класс) линейного объекта"), VisibleInListView(false)]
        public LineObjectClass LineObjectClass
        {
            get { return i_LineObjectClass; }
            set { SetPropertyValue("LineObjectClass", ref i_LineObjectClass, value); }
        }

        private double i_LengthProject;
        [DisplayName("Протяженность по проекту"), VisibleInListView(false)]
        public double LengthProject
        {
            get { return i_LengthProject; }
            set { SetPropertyValue("LengthProject", ref i_LengthProject, value); }
        }

        private double i_LengthFakt;
        [DisplayName("Протяженность фактически"), VisibleInListView(false)]
        public double LengthFakt
        {
            get { return i_LengthFakt; }
            set { SetPropertyValue("LengthFakt", ref i_LengthFakt, value); }
        }

        private string i_PowerLineProject;
        [DisplayName("Мощность по проекту"), VisibleInListView(false)]
        public string PowerLineProject
        {
            get { return i_PowerLineProject; }
            set { SetPropertyValue("PowerLineProject", ref i_PowerLineProject, value); }
        }
        private string i_PowerLineFakt;
        [DisplayName("Мощность фактически"), VisibleInListView(false)]
        public string PowerLineFakt
        {
            get { return i_PowerLineFakt; }
            set { SetPropertyValue("PowerLineFakt", ref i_PowerLineFakt, value); }
        }

        private string i_PipesInfoProject;
        [Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб по проекту"), VisibleInListView(false)]
        public string PipesInfoProject
        {
            get { return i_PipesInfoProject; }
            set { SetPropertyValue("PipesInfoProject", ref i_PipesInfoProject, value); }
        }
        private string i_PipesInfoFakt;
        [Size(500), DisplayName("Диаметры и количество трубопроводов, характеристики труб фактически"), VisibleInListView(false)]
        public string PipesInfoFakt
        {
            get { return i_PipesInfoFakt; }
            set { SetPropertyValue("PipesInfoFakt", ref i_PipesInfoFakt, value); }
        }

        private string i_ElectricLinesInfoProject;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи по проекту"), VisibleInListView(false)]
        public string ElectricLinesInfoProject
        {
            get { return i_ElectricLinesInfoProject; }
            set { SetPropertyValue("ElectricLinesInfoProject", ref i_ElectricLinesInfoProject, value); }
        }

        private string i_ElectricLinesInfoFakt;
        [Size(500), DisplayName("Тип, уровень напряжения линий электропередачи фактически"), VisibleInListView(false)]
        public string ElectricLinesInfoFakt
        {
            get { return i_ElectricLinesInfoFakt; }
            set { SetPropertyValue("ElectricLinesInfoFakt", ref i_ElectricLinesInfoFakt, value); }
        }

        private string i_ConstructiveElementsInfoProject;
        [Size(1000), DisplayName("Перечень конструктивных элементов по проекту"), VisibleInListView(false)]
        public string ConstructiveElementsInfoProject
        {
            get { return i_ConstructiveElementsInfoProject; }
            set { SetPropertyValue("ConstructiveElementsInfoProject", ref i_ConstructiveElementsInfoProject, value); }
        }

        private string i_ConstructiveElementsInfoFakt;
        [Size(1000), DisplayName("Перечень конструктивных элементов фактически"), VisibleInListView(false)]
        public string ConstructiveElementsInfoFakt
        {
            get { return i_ConstructiveElementsInfoFakt; }
            set { SetPropertyValue("ConstructiveElementsInfoFakt", ref i_ConstructiveElementsInfoFakt, value); }
        }

        private string i_OtherIndicatorsLineProject;
        [DisplayName("Иные показатели по проекту"), VisibleInListView(false)]
        public string OtherIndicatorsLineProject
        {
            get { return i_OtherIndicatorsLineProject; }
            set { SetPropertyValue("OtherIndicatorsLineProject", ref i_OtherIndicatorsLineProject, value); }
        }

        private string i_OtherIndicatorsLineFakt;
        [DisplayName("Иные показатели фактически"), VisibleInListView(false)]
        public string OtherIndicatorsLineFakt
        {
            get { return i_OtherIndicatorsLineFakt; }
            set { SetPropertyValue("OtherIndicatorsLineFakt", ref i_OtherIndicatorsLineFakt, value); }
        }

        /// <summary>
        /// 5. Соответствие требованиям энергетической эффективности
        /// </summary>
        private EnergyEfficiencyClass i_EnergyEfficiencyClassProject;
        [DisplayName("Класс энергоэффективности по проекту"), VisibleInListView(false)]
        public EnergyEfficiencyClass EnergyEfficiencyClassProject
        {
            get { return i_EnergyEfficiencyClassProject; }
            set { SetPropertyValue("EnergyEfficiencyClassProject", ref i_EnergyEfficiencyClassProject, value); }
        }

        private EnergyEfficiencyClass i_EnergyEfficiencyClassFakt;
        [DisplayName("Класс энергоэффективности фактически"), VisibleInListView(false)]
        public EnergyEfficiencyClass EnergyEfficiencyClassFakt
        {
            get { return i_EnergyEfficiencyClassFakt; }
            set { SetPropertyValue("EnergyEfficiencyClassFakt", ref i_EnergyEfficiencyClassFakt, value); }
        }

        private double i_HeatConsumptionProject;
        [DisplayName("Удельный расход тепловой энергии на 1 кв.м. площади по проекту (кВт*ч/м2)"), VisibleInListView(false)]
        public double HeatConsumptionProject
        {
            get { return i_HeatConsumptionProject; }
            set { SetPropertyValue("HeatConsumptionProject", ref i_HeatConsumptionProject, value); }
        }

        private double i_HeatConsumptionFakt;
        [DisplayName("Удельный расход тепловой энергии на 1 кв.м. площади фактически (кВт*ч/м2)"), VisibleInListView(false)]
        public double HeatConsumptionFakt
        {
            get { return i_HeatConsumptionFakt; }
            set { SetPropertyValue("HeatConsumptionFakt", ref i_HeatConsumptionFakt, value); }
        }

        private string i_OutdoorIsolationMaterialProject;
        [Size(1000), DisplayName("Материалы утепления наружных ограждающих конструкций по проекту"), VisibleInListView(false)]
        public string OutdoorIsolationMaterialProject
        {
            get { return i_OutdoorIsolationMaterialProject; }
            set { SetPropertyValue("OutdoorIsolationMaterialProject", ref i_OutdoorIsolationMaterialProject, value); }
        }

        private string i_OutdoorIsolationMaterialFakt;
        [Size(1000), DisplayName("Материалы утепления наружных ограждающих конструкций фактически"), VisibleInListView(false)]
        public string OutdoorIsolationMaterialFakt
        {
            get { return i_OutdoorIsolationMaterialFakt; }
            set { SetPropertyValue("OutdoorIsolationMaterialFakt", ref i_OutdoorIsolationMaterialFakt, value); }
        }

        private string i_SkylightsFillingProject;
        [Size(1000), DisplayName("Заполнение световых проемов по проекту"), VisibleInListView(false)]
        public string SkylightsFillingProject
        {
            get { return i_SkylightsFillingProject; }
            set { SetPropertyValue("SkylightsFillingProject", ref i_SkylightsFillingProject, value); }
        }

        private string i_SkylightsFillingFakt;
        [Size(1000), DisplayName("Заполнение световых проемов фактически"), VisibleInListView(false)]
        public string SkylightsFillingFakt
        {
            get { return i_SkylightsFillingFakt; }
            set { SetPropertyValue("SkylightsFillingFakt", ref i_SkylightsFillingFakt, value); }
        }

        //[Association, DisplayName("Сети и системы инженерно технического обслуживания")]
        //public XPCollection<IngeneeringSystems> IngeneeringSystems
        //{
        //    get { return GetCollection<IngeneeringSystems>("IngeneeringSystems"); }
        //}

        #region Сети и системы инженерно технического обслуживания
        private bool i_ElectroProject;
        [DisplayName("Электроснабжение по проекту"), VisibleInListView(false)]
        public bool ElectroProject
        {
            get { return i_ElectroProject; }
            set { SetPropertyValue("ElectroProject", ref i_ElectroProject, value); }
        }
        private bool i_ElectroFakt;
        [DisplayName("Электроснабжение фактически"), VisibleInListView(false)]
        public bool ElectroFakt
        {
            get { return i_ElectroFakt; }
            set { SetPropertyValue("ElectroFakt", ref i_ElectroFakt, value); }
        }
        private bool i_HeatProject;
        [DisplayName("Теплоснабжение по проекту"), VisibleInListView(false)]
        public bool HeatProject
        {
            get { return i_HeatProject; }
            set { SetPropertyValue("HeatProject", ref i_HeatProject, value); }
        }
        private bool i_HeatFakt;
        [DisplayName("Теплоснабжение фактически"), VisibleInListView(false)]
        public bool HeatFakt
        {
            get { return i_HeatFakt; }
            set { SetPropertyValue("HeatFakt", ref i_HeatFakt, value); }
        }
        private bool i_WaterProject;
        [DisplayName("Водоснабжение по проекту"), VisibleInListView(false)]
        public bool WaterProject
        {
            get { return i_WaterProject; }
            set { SetPropertyValue("WaterProject", ref i_WaterProject, value); }
        }
        private bool i_WaterFakt;
        [DisplayName("Водоснабжение фактически"), VisibleInListView(false)]
        public bool WaterFakt
        {
            get { return i_WaterFakt; }
            set { SetPropertyValue("WaterFakt", ref i_WaterFakt, value); }
        }
        private bool i_GazProject;
        [DisplayName("Газофикация по проекту"), VisibleInListView(false)]
        public bool GazProject
        {
            get { return i_GazProject; }
            set { SetPropertyValue("GazProject", ref i_GazProject, value); }
        }
        private bool i_GazFakt;
        [DisplayName("Газофикация фактически"), VisibleInListView(false)]
        public bool GazFakt
        {
            get { return i_GazFakt; }
            set { SetPropertyValue("GazFakt", ref i_GazFakt, value); }
        }
        private bool i_HouseSeverageProject;
        [DisplayName("Водоотведение проекту"), VisibleInListView(false)]
        public bool HouseSeverageProject
        {
            get { return i_HouseSeverageProject; }
            set { SetPropertyValue("HouseSeverageProject", ref i_HouseSeverageProject, value); }
        }
        private bool i_HouseSeverageFakt;
        [DisplayName("Водоотведение фактически"), VisibleInListView(false)]
        public bool HouseSeverageFakt
        {
            get { return i_HouseSeverageFakt; }
            set { SetPropertyValue("HouseSeverageFakt", ref i_HouseSeverageFakt, value); }
        }
        private bool i_PhonesProject;
        [DisplayName("Телефонизация по проекту"), VisibleInListView(false)]
        public bool PhonesProject
        {
            get { return i_PhonesProject; }
            set { SetPropertyValue("PhonesProject", ref i_PhonesProject, value); }
        }
        private bool i_PhonesFakt;
        [DisplayName("Телефонизация фактически"), VisibleInListView(false)]
        public bool PhonesFakt
        {
            get { return i_PhonesFakt; }
            set { SetPropertyValue("PhonesFakt", ref i_PhonesFakt, value); }
        }
        private bool i_TVProject;
        [DisplayName("Телевидение по проекту"), VisibleInListView(false)]
        public bool TVProject
        {
            get { return i_TVProject; }
            set { SetPropertyValue("TVProject", ref i_TVProject, value); }
        }
        private bool i_TVFakt;
        [DisplayName("Телевидение фактически"), VisibleInListView(false)]
        public bool TVFakt
        {
            get { return i_TVFakt; }
            set { SetPropertyValue("TVFakt", ref i_TVFakt, value); }
        }
        private bool i_RadioProject;
        [DisplayName("Радиофикация по проекту"), VisibleInListView(false)]
        public bool RadioProject
        {
            get { return i_RadioProject; }
            set { SetPropertyValue("RadioProject", ref i_RadioProject, value); }
        }
        private bool i_RadioFakt;
        [DisplayName("Радиофикация фактически"), VisibleInListView(false)]
        public bool RadioFakt
        {
            get { return i_RadioFakt; }
            set { SetPropertyValue("RadioFakt", ref i_RadioFakt, value); }
        }
        private bool i_SeverageProject;
        [DisplayName("Ливневая канализация по проекту"), VisibleInListView(false)]
        public bool SeverageProject
        {
            get { return i_SeverageProject; }
            set { SetPropertyValue("SeverageProject", ref i_SeverageProject, value); }
        }
        private bool i_SeverageFakt;
        [DisplayName("Ливневая канализация фактически"), VisibleInListView(false)]
        public bool SeverageFakt
        {
            get { return i_SeverageFakt; }
            set { SetPropertyValue("SeverageFakt", ref i_SeverageFakt, value); }
        }
        #endregion

    }


    /// <summary>
    /// Справочник материалов
    /// </summary>
    [ModelDefault("Caption", "Материалы"), System.ComponentModel.DefaultProperty("fullName")]
    public class Material : BaseObjectXAF
    {
        public Material(Session session) : base(session) { }

        private string i_Code;
        [DisplayName("Код")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }

        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        private string i_fullName;
        [DisplayName("Описание")]
        public string fullName
        {
            get {
                try { i_fullName = String.Format("({0}){1}", Code, Name); }
                catch { }
                return i_fullName; }
        }
    }

    /// <summary>
    /// Справочник типов объектов производственного назначения
    /// </summary>
    public class ProductPurposeObjectType : BaseObjectXAF
    {
        public ProductPurposeObjectType(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Тип объекта")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

    /// <summary>
    /// Сети и системы инженерно технического обслуживания
    /// </summary>
    //public class IngeneeringSystems : BaseObjectXAF
    //{
    //    public IngeneeringSystems(Session session) : base(session) { }

    //    private IngeneeringSystemsType i_IngeneeringSystemsType;
    //    [DisplayName("Сети или системы инженерно технического обслуживания")]
    //    public IngeneeringSystemsType IngeneeringSystemsType
    //    {
    //        get { return i_IngeneeringSystemsType; }
    //        set { SetPropertyValue("IngeneeringSystemsType", ref i_IngeneeringSystemsType, value); }
    //    }

    //    private dUnit i_Unit;
    //    [DisplayName("Единица измерения")]
    //    public dUnit Unit
    //    {
    //        get { return i_Unit; }
    //        set { SetPropertyValue("Unit", ref i_Unit, value); }
    //    }

    //    private string i_ValueProject;
    //    [DisplayName("Значение по проекту")]
    //    public string ValueProject
    //    {
    //        get { return i_ValueProject; }
    //        set { SetPropertyValue("ValueProject", ref i_ValueProject, value); }
    //    }

    //    private string i_ValueFakt;
    //    [DisplayName("Значение фактически")]
    //    public string ValueFakt
    //    {
    //        get { return i_ValueFakt; }
    //        set { SetPropertyValue("ValueFakt", ref i_ValueFakt, value); }
    //    }

    //    private ObjectConstractionInfo i_ObjectConstractionInfo;
    //    [Association, DisplayName("Сведения об объекте капитального строительства")]
    //    public ObjectConstractionInfo ObjectConstractionInfo
    //    {
    //        get { return i_ObjectConstractionInfo; }
    //        set { SetPropertyValue("ObjectConstractionInfo", ref i_ObjectConstractionInfo, value); }
    //    }

    //}

    /// <summary>
    /// Справочник типов сетей и систем инженерно технического обслуживания
    /// </summary>
    public class IngeneeringSystemsType : BaseObjectXAF
    {
        public IngeneeringSystemsType(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Тип сети или системы инженерно технического обслуживания")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

    /// <summary>
    /// Категория(класс) линейного объекта
    /// </summary>
    public class LineObjectClass : BaseObjectXAF
    {
        public LineObjectClass(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }
    /// <summary>
    /// Класс энергоэффективности
    /// </summary>
    public class EnergyEfficiencyClass : BaseObjectXAF
    {
        public EnergyEfficiencyClass(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Класс")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

}
