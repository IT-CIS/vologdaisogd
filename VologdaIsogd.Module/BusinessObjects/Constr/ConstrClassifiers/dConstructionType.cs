using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.Enums;
using DevExpress.Persistent.Base.General;

namespace AISOGD.Constr
{

    /// <summary>
    /// ���������� ������� �������������
    /// </summary>
    //[NavigationItem("�������"), System.ComponentModel.DefaultProperty("Name")]
    //[ModelDefault("Caption", "���������� ������� �������������")]
    //public class dConstructionUsing : TreeNodeAbstract
    //{
    //    public dConstructionUsing(Session session) : base(session) { }
    //    public dConstructionUsing(Session session, string name) : base(session) { this.Name = name; }

    //    private eBuildingKind i_BuildingKind;
    //    [DisplayName("�����/�������")]
    //    [ImmediatePostData]
    //    public eBuildingKind BuildingKind
    //    {
    //        get { return i_BuildingKind; }
    //        set { SetPropertyValue("BuildingKind", ref i_BuildingKind, value); }
    //    }

    //    [Association, DisplayName("���� �������� �������������")]
    //    public XPCollection<dConstructionType> ConstructionTypes
    //    {
    //        get { return GetCollection<dConstructionType>("ConstructionTypes"); }
    //    }
    //    protected override ITreeNode Parent
    //    {
    //        get
    //        {
    //            return null;
    //        }
    //    }
    //    protected override System.ComponentModel.IBindingList Children
    //    {
    //        get
    //        {
    //            return ConstructionTypes;
    //        }
    //    }
    //}

    /// <summary>
    /// ��� ������� �������������
    /// </summary>
    [ModelDefault("Caption", "��� ������� �������������"), System.ComponentModel.DefaultProperty("FullName")]
    [NavigationItem("�������"), ImageName("BO_Category")]
    public class dConstructionType : TreeNodeAbstract
    {
        public dConstructionType(Session session) : base(session) { }
        public dConstructionType(Session session, string name) : base(session) { this.Name = name; }

        private string i_FullName;
        [DisplayName("������ ������������")]
        public string FullName
        {
            get {
                //if (ParentConstructionUsing != null)
                //    i_FullName = ParentConstructionUsing.Name + " - " + Name;
                //else
                    i_FullName = Name;
                return i_FullName; }
        }

        private dConstructionType i_Parent;
        [Association, DisplayName("��������� ������� �������������")]
        public dConstructionType ParentConstructionUsing
        {
            get { return i_Parent; }
            set { SetPropertyValue("ParentConstructionUsing", ref i_Parent, value); }
        }

        protected override ITreeNode Parent
        {
            get
            {
                return ParentConstructionUsing;
            }
        }
        [Association, DisplayName("���� �������� �������������")]
        public XPCollection<dConstructionType> ConstructionTypes
        {
            get { return GetCollection<dConstructionType>("ConstructionTypes"); }
        }
        
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return ConstructionTypes;
            }
        }
        
    }
    

}