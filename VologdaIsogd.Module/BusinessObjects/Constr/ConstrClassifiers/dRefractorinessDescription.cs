using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.Constr
{

    /// <summary>
    /// ������� ����� ������������
    /// </summary>
    [ModelDefault("Caption", "��������������� �������������"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]
    public class dRefractorinessDescription : dBaseClassifierConstr
    {
        public dRefractorinessDescription (Session session) : base(session) { }

        private string i_ShortName;
        [Size(255), DisplayName("����������� ������������")]
        public string ShortName
        {
            get { return i_ShortName; }
            set { SetPropertyValue("ShortName", ref i_ShortName, value); }
        }
    }
    

}