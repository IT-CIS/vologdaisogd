using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.Constr
{

    /// <summary>
    /// Вид показателя
    /// </summary>
    [ModelDefault("Caption", "Вид показателя"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category")]
    public class dOtherIndicatorType : dBaseClassifierConstr
    {
        public dOtherIndicatorType(Session session) : base(session) { }

        //private string i_ShortName;
        //[Size(255), DisplayName("Сокращенное наименование")]
        //public string ShortName
        //{
        //    get { return i_ShortName; }
        //    set { SetPropertyValue("ShortName", ref i_ShortName, value); }
        //}
    }
    

}