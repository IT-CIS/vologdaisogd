﻿using AISOGD.DocFlow;
using AISOGD.General;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.Perm;
using AISOGD.Subject;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Constr
{
    /// <summary>
    /// Технический паспорт
    /// </summary>
    [ModelDefault("Caption", "Технический паспорт"), System.ComponentModel.DefaultProperty("descript")]
    public class TechPassport : AttachBase
    {
        public TechPassport(Session session) : base(session) { }

        private string _PassportNo = String.Empty;
        private DateTime _PassportDate;
        private string _InventaryNo = String.Empty;
        

        //[Aggregated, DisplayName("Технический паспорт")]
        //public TechPassport techPassport
        //{
        //    get { return _TechPassport; }
        //    set { try { SetPropertyValue("TechPassport", ref _TechPassport, value); } catch { } }
        //}
        private const string shortFormat = "№ тех. паспорта {PassportNo} от {PassportDate}";
        [System.ComponentModel.Browsable(false)]
        [Size(255), DisplayName("Описание")]
        public string descript
        {
            get
            {
                return ObjectFormatter.Format(shortFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(255), DisplayName("Номер тех.паспорта")]
        public string PassportNo
        {
            get { return _PassportNo; }
            set { _PassportNo = value; }
        }
        [DisplayName("Дата  тех.паспорта")]
        public DateTime PassportDate
        {
            get { return _PassportDate; }
            set { _PassportDate = value; }
        }
        [Size(255), DisplayName("Инвентарный номер (по техпаспорту)")]
        public string InventaryNo
        {
            get { return _InventaryNo; }
            set { _InventaryNo = value; }
        }

        private GeneralSubject _Org;
        [DisplayName("Изготовитель тех.паспорта")]
        public GeneralSubject Org
        {
            get { return _Org; }
            set { try { SetPropertyValue("Org", ref _Org, value); } catch { } }
        }
    }
}
