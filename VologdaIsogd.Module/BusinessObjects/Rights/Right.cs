using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.Dictonaries;
using AISOGD.Enums;
using AISOGD.Subject;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.SystemDir;
//using AISOGD.Constr;
using DevExpress.ExpressApp.Model;
using AISOGD.Land;

namespace AISOGD.General
{
    [ModelDefault("Caption", "�������������"), System.ComponentModel.DefaultProperty("RequesterString")]
    public class Right  : BaseObjectXAF
    {
        public Right(Session session) : base(session) { }

        private string i_requesterString;
        
        private DateTime i_lawFinishDate;
        private string i_lawDocument;
        private double i_lawProcent;


        //[Size(510), DisplayName("�������� ����������������")]
        //public string RequesterString
        //{
        //    get
        //    {
        //        try
        //        {
        //            i_requesterString = GetRequesterInfo();
        //        }
        //        catch { }
        //        return i_requesterString;
        //    }
        //    set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        //}

        //public string GetRequesterInfo()
        //{
        //    string result = "";
        //    if (i_org != null)
        //    {
        //        result = i_org.FullName;
        //        if (i_org.Info.Length > 0)
        //            result += ", " + i_org.Info;
        //    }
        //    if (i_person != null)
        //    {
        //        result = i_person.FullName;
        //        if (i_person.Info.Length > 0)
        //            result += ", " + i_person.Info;
        //    }
        //    return result;
        //}
        private string i_Name;
        [Size(500), DisplayName("��� �����")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        [DisplayName("��� ����� (����������)")]
        public dRightKind RightKind { get; set; }

        private string i_RegNumber;
        [DisplayName("����� ��������������� �����������")]
        public string RegNumber
        {
            get { return i_RegNumber; }
            set { SetPropertyValue("RegNumber", ref i_RegNumber, value); }
        }

        private DateTime i_RegDate;
        [DisplayName("���� ��������������� �����������")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }

        [DisplayName("���� ��������� �������� �����")]
        public DateTime LawFinishDate
        {
            get { return i_lawFinishDate; }
            set { SetPropertyValue("LawFinishDate", ref i_lawFinishDate, value); }
        }

        [Size(510),DisplayName("�������� �������� �����")]
        public string LawDocument
        {
            get { return i_lawDocument; }
            set { SetPropertyValue("LawDocument", ref i_lawDocument, value); }
        }
        private string i_Share;
        [DisplayName("�������� ����")]
        public string Share
        {
            get { return i_Share; }
            set { SetPropertyValue("Share", ref i_Share, value); }
        }
        private string i_ShareText;
        [DisplayName("�������� ���� �������")]
        public string ShareText
        {
            get { return i_ShareText; }
            set { SetPropertyValue("ShareText", ref i_ShareText, value); }
        }
        [DisplayName("������� ��������������")]
        public double LawProcent
        {
            get { return i_lawProcent; }
            set { SetPropertyValue("LawProcent", ref i_lawProcent, value); }
        }
        private string i_Desc;
        [DisplayName("������ �������")]
        public string Desc
        {
            get { return i_Desc; }
            set { SetPropertyValue("Desc", ref i_Desc, value); }
        }
        //private Apartment i_Apartment;
        //[Association, DisplayName("���������")]
        //public Apartment Floor
        //{
        //    get { return i_Apartment; }
        //    set { SetPropertyValue("Apartment", ref i_Apartment, value); }
        //}
        [Association, DisplayName("��������� �������")]
        public XPCollection<Parcel> Lots
        {
            get
            {
                return GetCollection<Parcel>("Lots");
            }
        }
        [Association, DisplayName("���������������")]
        public XPCollection<Owner> Owners
        {
            get
            {
                return GetCollection<Owner>("Owners");
            }
        }
        //[Association, DisplayName("��������� - ��������� ��� �������� ��������")]
        //public XPCollection<DocumentWithoutAppliedFile> Documents
        //{
        //    get
        //    {
        //        return GetCollection<DocumentWithoutAppliedFile>("Documents");
        //    }
        //}
        //[Association, DisplayName("���������")]
        //public XPCollection<AISOGD.DocFlow.GeneralDocBase> GeneralDocs
        //{
        //    get
        //    {
        //        return GetCollection<AISOGD.DocFlow.GeneralDocBase>("GeneralDocs");
        //    }
        //}
    }
}