using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.Dictonaries;
using AISOGD.Enums;
using AISOGD.Subject;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using AISOGD.Land;

namespace AISOGD.General
{
    [ModelDefault("Caption", "��������������"), System.ComponentModel.DefaultProperty("RequesterString")]
    public class Owner  : GeneralSubject
    {
        public Owner(Session session) : base(session) { }

        //private string i_requesterString;
        //private Org i_org;
        //private AISOGD.Subject.Person i_person;
        //private eSubjectType i_OwnerType;
        //private double i_lawProcent;

        //[DisplayName("��� �������� �����")]
        //[ImmediatePostData]
        //public eSubjectType OwnerType
        //{
        //    get { return i_OwnerType; }
        //    set { SetPropertyValue("OwnerType", ref i_OwnerType, value); }
        //}
        //private string i_FamilyName;
        //[Size(510), DisplayName("�������")]
        //public string FamilyName
        //{
        //    get { return i_FamilyName; }
        //    set { SetPropertyValue("FamilyName", ref i_FamilyName, value); }
        //}
        //private string i_FirstName;
        //[Size(510), DisplayName("���")]
        //public string FirstName
        //{
        //    get { return i_FirstName; }
        //    set { SetPropertyValue("FirstName", ref i_FirstName, value); }
        //}
        //private string i_Patronymic;
        //[Size(510), DisplayName("��������")]
        //public string Patronymic
        //{
        //    get { return i_Patronymic; }
        //    set { SetPropertyValue("Patronymic", ref i_Patronymic, value); }
        //}

        //private string i_Name;
        //[Size(510), DisplayName("������������")]
        //public string Name
        //{
        //    get { return i_Name; }
        //    set { SetPropertyValue("Name", ref i_Name, value); }
        //}
        //private string i_Address;
        //[Size(4000), DisplayName("�������� ����� ���������������")]
        //public string Address
        //{
        //    get { return i_Address; }
        //    set { SetPropertyValue("Address", ref i_Address, value); }
        //}
        //private string i_Email;
        //[Size(100), DisplayName("����� ����������� �����")]
        //public string Email
        //{
        //    get { return i_Email; }
        //    set { SetPropertyValue("Email", ref i_Email, value); }
        //}
        //[DisplayName("���������������")]
        //[Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        //public Org RequesterOrg
        //{
        //    get { return i_org; }
        //    set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        //}

        //[DisplayName("���������������")]
        //[Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '��'", Context = "DetailView")]
        //public AISOGD.Subject.Person RequesterPerson
        //{
        //    get { return i_person; }
        //    set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        //}
        
       
        //[Size(510), DisplayName("�������� ���������������")]
        //public string RequesterString
        //{
        //    get
        //    {
        //        try
        //        {
        //            i_requesterString = GetRequesterInfo();
        //        }
        //        catch { }
        //        return i_requesterString;
        //    }
        //    set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        //}

        //public string GetRequesterInfo()
        //{
        //    string result = "";
        //    if (i_org != null)
        //    {
        //        result = i_org.FullName;
        //        if (i_org.Info.Length > 0)
        //            result += ", " + i_org.Info;
        //    }
        //    if (i_person != null)
        //    {
        //        result = i_person.FullName;
        //        if (i_person.Info.Length > 0)
        //            result += ", " + i_person.Info;
        //    }
        //    return result;
        //}

        private string i_ShareText;
        [DisplayName("���� � �����")]
        public string ShareText
        {
            get { return i_ShareText; }
            set { SetPropertyValue("ShareText", ref i_ShareText, value); }
        }


        [Association, DisplayName("��������������")]
        public XPCollection<Right> Rights
        {
            get
            {
                return GetCollection<Right>("Rights");
            }
        }
        [Association, DisplayName("�����������")]
        public XPCollection<tEncumbrance> tEncumbrances
        {
            get
            {
                return GetCollection<tEncumbrance>("tEncumbrances");
            }
        }
    }
}