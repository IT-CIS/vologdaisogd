using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;

namespace AISOGD.General
{

    /// <summary>
    /// ��� �����
    /// </summary>
    [ModelDefault("Caption", "��� �����"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class dRightKind : BaseObjectXAF
    {
        public dRightKind(Session session) : base(session) { }

        [Size(64), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
    

}