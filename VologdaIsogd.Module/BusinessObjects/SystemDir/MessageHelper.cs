﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VologdaIsogd.Module.BusinessObjects.SystemDir
{
    public static class MessageHelper
    {
        public static void FullErrorMessage(Exception ex)
        {
            XtraMessageBox.Show($"Произошла ошибка. InnerException: { ex.InnerException}; Message: { ex.Message}; TargetSite: {ex.TargetSite}; Source: {ex.Source}", "Ошибка");
        }
    }
}
