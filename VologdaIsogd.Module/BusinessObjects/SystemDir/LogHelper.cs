﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VologdaIsogd.SystemDir
{
    public class LogHelperClass
    {
        private string _path;
        private string _sourceName;
        public LogHelperClass(string path, string sourceName)
        {
            _path = path;
            _sourceName = sourceName;
        }

        private static object sync = new object();
        public void WriteErr(Exception ex, string message)
        {
            try
            {
                // Путь .\\Log
                string pathToLog = Path.Combine(_path, "Log");
                if (!Directory.Exists(pathToLog))
                    Directory.CreateDirectory(pathToLog); // Создаем директорию, если нужно
                string filename = Path.Combine(pathToLog, string.Format($"Log_{DateTime.Now:dd.MM.yyy}_{_sourceName}Error.log"));
                string fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] [{1}.{2}()] {3}, объект: {4}\r\n",
                DateTime.Now, ex.TargetSite.DeclaringType, ex.TargetSite.Name, ex.Message, message);
                lock (sync)
                {
                    File.AppendAllText(filename, fullText, Encoding.GetEncoding("Windows-1251"));
                }
            }
            catch
            {
                // Перехватываем все и ничего не делаем
            }
        }
        public void WriteText(string message)
        {
            try
            {
                // Путь .\\Log
                string pathToLog = Path.Combine(_path, "Log");
                if (!Directory.Exists(pathToLog))
                    Directory.CreateDirectory(pathToLog); // Создаем директорию, если нужно
                string filename = Path.Combine(pathToLog, string.Format($"Log_{DateTime.Now:dd.MM.yyy}_{_sourceName}.log"));
                string fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] сообщение: {1}\r\n",
                DateTime.Now, message);
                lock (sync)
                {
                    File.AppendAllText(filename, fullText, Encoding.GetEncoding("Windows-1251"));
                }
            }
            catch
            {
                // Перехватываем все и ничего не делаем
            }
        }
    }
}
