﻿using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AISOGD.SystemDir
{
    /// <summary>
    /// Абстрактный класс для создания TreeNode списков. Содержит поле Name
    /// </summary>
    public abstract class TreeNodeAbstract : BaseObjectXAF, ITreeNode
    {
        public TreeNodeAbstract(Session session) : base(session) { }

        private string name;
        protected abstract ITreeNode Parent
        {
            get;
        }
        protected abstract System.ComponentModel.IBindingList Children
        {
            get;
        }

        [Size(255),DisplayName("Наименование")]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetPropertyValue("Name", ref name, value);
            }
        }

        #region ITreeNode
        System.ComponentModel.IBindingList ITreeNode.Children
        {
            get
            {
                return Children;
            }
        }
        string ITreeNode.Name
        {
            get
            {
                return Name;
            }
        }
        ITreeNode ITreeNode.Parent
        {
            get
            {
                return Parent;
            }
        }
        #endregion

    }
}
