﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdMapInfo.Module.SystemDir;

namespace AISOGD.SystemDir
{
    [ModelDefault("Caption", "ГИС слой"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("LayerId")]
    public class GisLayer : BaseObjectXAF, ISpatialLayer
    {
        public GisLayer() : base() { }
        public GisLayer(Session session) : base(session) { }

        private string layer_id;
        [DisplayName("Наименование таблицы(слоя)"), Size(255)]
        public string LayerId
        {
            get { return layer_id; }
            set { SetPropertyValue("LayerId", ref layer_id, value); }
        }

        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        //protected override void OnSaving()
        //{
        //    if (Connect.FromSession(Session).IsExist<GisLayer>(mc => mc.LayerId == this.LayerId))
        //        throw new Exception("Такой слой уже существует");
        //    base.OnSaving();
        //}
    }
}
