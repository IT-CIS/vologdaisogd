﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdMapInfo.Module.SystemDir;

namespace AISOGD.SystemDir
{
    //[Custom("Caption", "ГИС стиль")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GisStyle : BaseObjectXAF
    {
        public GisStyle() : base() { }
        public GisStyle(Session session) : base(session) { }

        private string style_id;
        [DisplayName("ID ГИС стиля"), Size(255)]
        public string StyleId
        {
            get { return style_id; }
            set { SetPropertyValue("StyleId", ref style_id, value); }
        }

        private string local_style_id;
        [DisplayName("Локальный ID ГИС стиля"), Size(255)]
        public string LocalStyleId
        {
            get { return local_style_id; }
            set { SetPropertyValue("LocalStyleId", ref local_style_id, value); }
        }

        private string i_name;
        [DisplayName("Наименование стиля"), VisibleInListView(true), Size(255)]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        private GisLayer i_GisLayer;
        [Association, System.ComponentModel.Browsable(false)]
        public GisLayer GisLayer
        {
            get { return i_GisLayer; }
            set { SetPropertyValue("GisLayer", ref i_GisLayer, value); }
        }
        //protected override void OnSaving()
        //{
        //    if (Connect.FromSession(Session).IsExist<GisLayer>(mc => mc.LayerId == this.LayerId))
        //        throw new Exception("Такой слой уже существует");
        //    base.OnSaving();
        //}
    }
}
