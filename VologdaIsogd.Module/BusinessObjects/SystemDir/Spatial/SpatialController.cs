﻿using DevExpress.Xpo;
using DevExpress.XtraEditors;
//using MapInfo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace AISOGD.SystemDir
{
    public class SpatialController<TLayer>
        where TLayer : ISpatialLayer
    {
        public SpatialController(Connect connect)
        {
            this.connect = connect;
        }

        Connect connect;

        public SpatialLayerItem GetSpatialLayerItem(string isogdClassName, string spatialLayerId)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            var confObj = connect.FindFirstObject<SpatialConfig>(mc => mc.IsogdClassName == connect.GetClassInfo(isogdClassName).ClassType);
            return confObj.Layers.SingleOrDefault(mc => mc.SpatialLayer.LayerId.Equals(spatialLayerId));
        }
        /// <summary>
        /// Получение связанных ИСОГД-классов из конфига
        /// </summary>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <returns></returns>
        public IEnumerable<string> GetLinkedIsogdClasses(string spatialLayerId)
        {
            if (string.IsNullOrEmpty(spatialLayerId))
                throw new FormatException("Не верный ID пространственного слоя");
            return from cl in connect.Query<SpatialConfig>()
                   where cl.Layers.Any(mc => mc.SpatialLayer.LayerId == spatialLayerId)
                   select cl.IsogdClassName.FullName;
        }
        /// <summary>
        /// Получение связанных ИСОГД-классов из конфига
        /// </summary>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <returns></returns>
        public List<string> GetListLinkedIsogdClasses(string spatialLayerId)
        {
            List<string> IsogdClasses = new List<string>();
            if (string.IsNullOrEmpty(spatialLayerId))
                throw new FormatException("Не верный ID пространственного слоя");
            foreach (SpatialConfig spatialConfig in connect.FindObjects<SpatialConfig>(mc=> mc.Oid!=null))
            {
                if (spatialConfig.IsogdClassName != null)
                    foreach (SpatialLayerItem spatialLayerItem in spatialConfig.Layers)
                        if (spatialLayerItem.SpatialLayer != null)
                            if (spatialLayerItem.SpatialLayer.LayerId != null)
                                if (spatialLayerItem.SpatialLayer.LayerId == spatialLayerId)
                                    IsogdClasses.Add(spatialConfig.IsogdClassName.FullName);
            }
            return IsogdClasses;
        }
        /// <summary>
        /// Получение пространственных слоев доступных для связи
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <returns></returns>
        public IEnumerable<string> GetLinkedSpatialLayers(string isogdClassName)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            return from layer in connect.FindFirstObject<SpatialConfig>(
                      mc => mc.IsogdClassName == connect.GetClassInfo(isogdClassName).ClassType).Layers
                   //where !layer.IsViewOnly
                   select layer.SpatialLayer.LayerId;
        }
        /// <summary>
        /// Получение пространственных слоев доступных для связи
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <returns>List(string) spatialLayerId</returns>
        public List<string> GetListLinkedSpatialLayers(string isogdClassName)
        {
            List<string> spatialLayerIds = new List<string>();
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            SpatialConfig spatialConfig = connect.FindFirstObject<SpatialConfig>(
                      mc => mc.IsogdClassName == connect.GetClassInfo(isogdClassName).ClassType);
            if (spatialConfig != null)
                foreach (SpatialLayerItem spatialLayerItem in spatialConfig.Layers)
                {
                    if (spatialLayerItem.SpatialLayer != null)
                        if (spatialLayerItem.SpatialLayer.LayerId != null)
                            spatialLayerIds.Add(spatialLayerItem.SpatialLayer.LayerId);
                }
            else
                throw new FormatException(String.Format("Для класса {0} нет настроек в SpatialConfig."
                + " Обратитесь к администратору Системы.", isogdClassName));
            return spatialLayerIds;
        }
        /// <summary>
        /// Получение пространственных слоев доступных только для просмотра
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <returns></returns>
        public IEnumerable<string> GetViewOnlySpatialLayers(string isogdClassName)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            return from layer in connect.FindFirstObject<SpatialConfig>(
                      mc => mc.IsogdClassName == connect.GetClassInfo(isogdClassName).ClassType).Layers
                   //where layer.IsViewOnly
                   select layer.SpatialLayer.LayerId;
        }
        /// <summary>
        /// Проверяет возможно ли связать объекты данного пространственного слоя
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <returns></returns>
        public bool IsLayerValid(string isogdClassName, string spatialLayerId)
        {
            
            return connect.FindFirstObject<SpatialConfig>(
                mc => mc.IsogdClassName == connect.GetClassInfo(isogdClassName).ClassType).Layers.Any(
                mc => mc.SpatialLayer.LayerId == spatialLayerId);
            //mc => !mc.IsViewOnly && mc.SpatialLayer.Id == spatialLayerId);
        }

        //---------------------------------------------------------------------------------

        /// <summary>
        /// Проверяет есть ли у реестровго объекта связанный пространственный объект(ы)
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <returns></returns>
        public bool IsObjectLinked(string isogdClassName, string isogdObjectId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType appType = WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("Не указан ID реестрового объекта");
            return connect.IsExist<SpatialRepository>(mc => mc.IsogdClassName == isogdClassName &&
                mc.IsogdObjectId == isogdObjectId && mc.ApplicationType == appType);
        }

        /// <summary>
        /// Проверяет на существование реестрового объекта связанного с ГИС объектом
        /// </summary>
        /// <param name="gisObjectId">Id ГИС объекта</param>
        /// <returns></returns>
        public bool IsXafObjectLinkedSpatialExist(string gisLayerId, string gisObjectId)
        {
            if (IsSpatialObjectLinkExist(gisLayerId, gisObjectId))
            {
                SpatialRepository repo = connect.FindFirstObject<SpatialRepository>(mc => mc.SpatialLayerId == gisLayerId && mc.SpatialObjectId == gisObjectId);
                if (connect.GetUnitOfWork().FindObject(connect.GetClassInfo(repo.IsogdClassName), new DevExpress.Data.Filtering.BinaryOperator("Oid", repo.IsogdObjectId)) != null)
                    return true;
                else
                    return false;
                //return connect.IsExist(connect.GetClassInfo(repo.IsogdClassName), String.Format("Oid == {0}",repo.IsogdObjectId));
            }
            else
                return false;
        }

        /// <summary>
        /// Проверяет на существование текущей связи реестрового и пространственного объектов
        /// </summary>
        /// <param name="isogdClassName">ИСОГД класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <param name="gisObjectId">ID ГИС объекта</param>
        /// <returns></returns>
        public bool IsObjectLinkExist(string isogdClassName, string isogdObjectId, string gisLayerId, string gisObjectId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType appType = WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("Не указан ID реестрового объекта");
            if (string.IsNullOrEmpty(gisObjectId))
                throw new FormatException("Не указан ID пространственного объекта");
            return connect.IsExist<SpatialRepository>(mc => mc.IsogdClassName == isogdClassName &&
                mc.IsogdObjectId == isogdObjectId && mc.SpatialLayerId == gisLayerId && mc.SpatialObjectId == gisObjectId); // && mc.ApplicationType == appType
        }

        /// <summary>
        /// Удаляет все связки на реестровые объекты у указанного пространственного(по Id). Удаление определенной связи не предусмотрено (можно через xaf-класс).
        /// </summary>
        /// <param name="gisLayerId">Гис слой</param>
        /// <param name="isogdGisObjectId">ID ГИС объекта</param>
        public void DeleteSpatialGISLinks(string gisLayerId, string isogdGisObjectId)
        {
            if (string.IsNullOrEmpty(gisLayerId))
                throw new FormatException("Не указан класс ИСОГД");
            if (string.IsNullOrEmpty(isogdGisObjectId))
                throw new FormatException("Не указан ID ГИС объекта");
            if (!IsSpatialObjectLinkExist(gisLayerId, isogdGisObjectId))
                throw new Exception("Запись о связи не найдена");
            foreach (var link in GetIsogdLinks(gisLayerId, isogdGisObjectId))
            {
                connect.DeleteObject(link);
            }
        }

        /// <summary>
        /// Массив Id слоев по названию группы слоев
        /// </summary>
        /// <param name="LayerName">Наименование группы слоев</param>
        /// <returns></returns>
        public List<string> GetLayersIdsFromLayerGroupName(string LayerGroupName)
        {
            List<string> res = new List<string>();
            if (string.IsNullOrEmpty(LayerGroupName))
                throw new FormatException("Не указано наименование группы слоев");
            GisLayerGroup gisLayerGroup = connect.FindFirstObject<GisLayerGroup>(mc => mc.Name == LayerGroupName);
            if (gisLayerGroup == null)
                throw new FormatException("По данному наименованию не найдена группа слоев");
            else
            {
                if (gisLayerGroup.Layers.Count > 0)
                    foreach (SpatialLayerItem split in gisLayerGroup.Layers)
                    {
                        if (split.SpatialLayer != null)
                            //if (!split.SpatialLayer.isRaster)
                            if (split.SpatialLayer.LayerId != null || split.SpatialLayer.LayerId != String.Empty)
                                res.Add(split.SpatialLayer.LayerId);
                    }
            }
            return res;
        }

        /// <summary>
        /// Проверяет, есть ли у ГИС объекта связанный реестровый
        /// </summary>
        /// <param name="gisObjectId">Id ГИС объекта</param>
        /// <returns></returns>
        public bool IsSpatialObjectLinkExist(string gisLayerId, string gisObjectId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType appType = WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI)
        {
            if (string.IsNullOrEmpty(gisObjectId))
                throw new FormatException("Не указан ID пространственного объекта");
            return connect.IsExist<SpatialRepository>(mc => mc.SpatialObjectId == gisObjectId && mc.SpatialLayerId == gisLayerId); // && mc.ApplicationType == appType
        }

        /// <summary>
        /// Добавляет к реестровому объекту ссылку на пространственный объект. Проверка на уже существующую связь осуществляется.
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <param name="spatialObjectId">ID пространственного объекта</param>
        public void AddSpatialLink(string isogdClassName, string isogdObjectId,
           string spatialLayerId, string spatialObjectId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType appType = WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("Не указан ID реестрового объекта");
            if (string.IsNullOrEmpty(spatialObjectId))
                throw new FormatException("Не указан ID пространственного объекта");
            if (string.IsNullOrEmpty(spatialLayerId))
                throw new FormatException("Не указан ID слоя пространственного объекта");
            if (!IsLayerValid(isogdClassName, spatialLayerId))
                throw new Exception("Нельзя связать объект данного реестра с объектом данного слоя");
            if (!IsObjectLinkExist(isogdClassName, spatialLayerId,spatialLayerId, spatialObjectId, appType))
            //    throw new Exception("Данный реестровый объект уже связан с текущим ГИС объектом");
            //else
            {
                var repoObject = connect.CreateObject<SpatialRepository>();
                repoObject.IsogdClassName = isogdClassName;
                repoObject.IsogdObjectId = isogdObjectId;
                repoObject.SpatialLayerId = spatialLayerId;
                repoObject.SpatialObjectId = spatialObjectId;
                connect.SaveObject(repoObject);
            }
        }
        /// <summary>
        /// Получает все связанные пространственные объекты
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <returns></returns>
        public IQueryable<SpatialRepository> GetSpatialLinks(string isogdClassName, string isogdObjectId)
        {
            return connect.FindObjects<SpatialRepository>(mc => mc.IsogdClassName == isogdClassName &&
                mc.IsogdObjectId == isogdObjectId);
        }
        /// <summary>
        /// Удаляет все связанные пространственные объекты. Удаление определенной связи не предусмотрено (можно через xaf-класс).
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        public void DeleteSpatialLinks(string isogdClassName, string isogdObjectId)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("Не указан класс ИСОГД");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("Не указан ID реестрового объекта");
            if (!IsObjectLinked(isogdClassName, isogdObjectId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI) && !IsObjectLinked(isogdClassName, isogdObjectId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.QGIS))
                throw new Exception("Запись о связи не найдена");
            foreach (var link in GetSpatialLinks(isogdClassName, isogdObjectId))
            {
                connect.DeleteObject(link);
            }
        }
        /// <summary>
        /// Получает все связанные реестровые объекты
        /// </summary>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <param name="spatialObjectId">ID пространственного объекта</param>
        /// <returns></returns>
        public IQueryable<SpatialRepository> GetIsogdLinks(string spatialLayerId, string spatialObjectId)
        {
            if (string.IsNullOrEmpty(spatialLayerId))
                throw new FormatException("Не указан ID пространственного слоя");
            if (string.IsNullOrEmpty(spatialObjectId))
                throw new FormatException("Не указан ID пространственного объекта");
            return connect.FindObjects<SpatialRepository>(mc => mc.SpatialLayerId == spatialLayerId &&
                mc.SpatialObjectId == spatialObjectId);
        }

        //Для MapInfo ----------------------------------------------------------------------->
        /////вариант связи через таблицы МапИнфо - связи записываются в поля (XafId, XafClass) таблиц
        // I. Для связки
        // 1. Проверяем, есть ли у пространственного объекта связанный реестровый
        // 2. Свзять реестровый и пространственный объекты (записать в поля таблицы МапИнфо у объекта XafId и XafClass информацию об ИД и классе реестрового объекта)
        // 3. 
        //
        /// <summary>
        /// Проверяем, есть ли у реестрового объекта связанный пространственный
        /// </summary>
        /// <param name="aMapInfo">MapInfoApplication</param>
        /// <param name="aMapInfoTable">Таблица(слой), в котором идет проверка </param>
        /// <param name="aXafClass">Класс реестрового объекта</param>
        /// <param name="aXafId">ИД реестрового объекта</param>
        /// <returns>true, если связка есть</returns>
        public bool isMapInfoObjectLinked(WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI.MapInfoApplication aMapInfo, string aMapInfoTable, string aXafClass, string aXafId)
        {
            bool res = false;
            try
            {
                aMapInfo.Do("Select * From " + aMapInfoTable + " Where AisogdGUID = \"" + aXafId + "\" AND AisogdCN = \"" + aXafClass + "\" Into z_CntMapTemp Noselect");
                //aMapInfo.Do("Select * From " + aMapInfoTable + " Where XafId = \"" + aXafId + "\" Into z_CntMapTemp Noselect");
                //XtraMessageBox.Show("Select * From " + aMapInfoTable + " Where XafId = \"" + aXafId + "\" Into z_CntMapTemp Noselect");
                //XtraMessageBox.Show(aMapInfo.Eval("TableInfo(Selection, 8)"));
                if (Convert.ToInt32(aMapInfo.Eval("TableInfo(z_CntMapTemp, 8)")) > 0)
                    res = true;
                aMapInfo.Do("Drop Table z_CntMapTemp");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
            }
            return res;
        }

        /// <summary>
        /// Связать выбранные пространственные объекты с реестровым
        /// </summary>
        /// <param name="aMapInfo">MapInfoApplication</param>
        /// <param name="aMapInfoTable">Таблица(слой), в котором идет проверка </param>
        /// <param name="aXafClass">Класс реестрового объекта</param>
        /// <param name="aXafId">ИД реестрового объекта</param>
        /// <returns>true, если привязка прошла успешно</returns>
        public bool SetLinkToMapInfoObject(WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI.MapInfoApplication aMapInfo, string aMapInfoTable, string aXafClass, string aXafId)
        {
            bool res = true;
            try
            {
                aMapInfo.Do("Update Selection Set AisogdGUID =  \"" + aXafId + "\" Commit table " + aMapInfoTable);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                res = false;
            }
            try
            {
                aMapInfo.Do("Update Selection Set AisogdCN =  \"" + aXafClass + "\" Commit table " + aMapInfoTable);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                res = false;
            }
            return res;
        }

        /// <summary>
        ///  Удалить привязку
        /// </summary>
        /// <param name="aMapInfo">MapInfoApplication</param>
        /// <param name="aMapInfoTable">Таблица(слой), в котором идет проверка </param>
        /// <param name="aXafClass">Класс реестрового объекта</param>
        /// <param name="aXafId">ИД реестрового объекта</param>
        /// <returns>true, если привязка удалена успешно</returns>
        public bool DelLinkMapInfoObject(WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI.MapInfoApplication aMapInfo, string aMapInfoTable, string aXafClass, string aXafId)
        {
            bool res = true;
            try
            {
                aMapInfo.Do("Select * From " + aMapInfoTable + " Where AisogdGUID = \"" + aXafId + "\" AND AisogdCN = \"" + aXafClass + "\" Into z_CntMapTemp Noselect");
                if (Convert.ToInt32(aMapInfo.Eval("TableInfo(z_CntMapTemp, 8)")) > 0)
                {
                    aXafId = "";
                    aXafClass = "";
                    try
                    {
                        aMapInfo.Do("Update z_CntMapTemp Set AisogdGUID =  \"" + aXafId + "\" Commit table " + aMapInfoTable);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                        res = false;
                    }
                    try
                    {
                        aMapInfo.Do("Update z_CntMapTemp Set AisogdCN =  \"" + aXafClass + "\" Commit table " + aMapInfoTable);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                        res = false;
                    }
                }
                aMapInfo.Do("Drop Table z_CntMapTemp");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                res = false;
            }
            return res;
        }

        public void GetLinkedMapInfoObject(WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI.MapInfoApplication aMapInfo, string aMapInfoTable, string aXafClass, string aXafId)
        {
            try
            {
                aMapInfo.Do("Select * From " + aMapInfoTable + " Where AisogdGUID = \"" + aXafId + "\" and AisogdCN = \"" + aXafClass + "\" Into ZoomObject");
                aMapInfo.Do("Add Map Layer ZoomObject Fetch first From ZoomObject Set Map Center (CentroidX(ZoomObject.obj),CentroidY(ZoomObject.obj)) " +
                "Add Map Layer ZoomObject Set Map Zoom Entire Layer ZoomObject");
                aMapInfo.Do("Drop Table ZoomObject");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
            }
        }


        ////Для Ингео ----------------------------------------------------------------------->
        ///// <summary>
        ///// Указаны ли в конфиге у слоя стили
        ///// </summary>
        ///// <param name="LayerName">Наименование слоя</param>
        ///// <returns></returns>
        //public bool IsStylesInLayerExist(string LayerName)
        //{
        //    bool res = false;
        //    if (string.IsNullOrEmpty(LayerName))
        //        throw new FormatException("Не указано наименование слоя");
        //    GisLayer layer = connect.FindFirstObject<GisLayer>(mc=> mc.Name == LayerName);
        //    if (layer == null)
        //        throw new FormatException("По данному наименованию не найден слой");
        //    else
        //    {
        //        if (layer.GisStyles.Count > 0)
        //            res = true;
        //    }
        //    return res;
        //}

        ///// <summary>
        ///// Получить Id стиля по наименованию слоя и стиля
        ///// </summary>
        ///// <param name="LayerName">наименование слоя</param>
        ///// <param name="StyleName">наименование стиля</param>
        ///// <returns></returns>
        //public string GetStyleIdByName(string LayerName, string StyleName)
        //{
        //    string res = "";

        //    if (string.IsNullOrEmpty(LayerName))
        //        throw new FormatException("Не указано наименование слоя");
        //    if (string.IsNullOrEmpty(StyleName))
        //        throw new FormatException("Не указано наименование стиля");

        //    GisLayer layer = connect.FindFirstObject<GisLayer>(mc => mc.Name == LayerName);
        //    if (layer == null)
        //        throw new FormatException(String.Format("Слой с наименованием {0} не найден.", LayerName));
        //    else
        //    {
        //        GisStyle style = connect.FindFirstObject<GisStyle>(mc => mc.Name == StyleName 
        //            && mc.GisLayer == layer);
        //        if (style == null)
        //            throw new FormatException(String.Format("Стиль с наименованием {0} не найден у слоя {1}.",
        //                StyleName, LayerName));
        //        else
        //        {
        //            if (style.StyleId != null || style.StyleId != String.Empty)
        //                res = style.StyleId;
        //            else
        //                throw new FormatException(String.Format("У стиля {0} не указан StyleId.",
        //                StyleName));
        //        }
        //    }
        //    return res;
        //}

        ///// <summary>
        ///// Массив Id векторных слоев по названию группы слоев
        ///// </summary>
        ///// <param name="LayerName">Наименование группы слоев</param>
        ///// <returns></returns>
        //public List<string> GetLayersIdsFromLayerGroupName(string LayerGroupName)
        //{
        //    List<string> res = new List<string>();
        //    if (string.IsNullOrEmpty(LayerGroupName))
        //        throw new FormatException("Не указано наименование группы слоев");
        //    GisLayerGroup gisLayerGroup = connect.FindFirstObject<GisLayerGroup>(mc => mc.Name == LayerGroupName);
        //    if (gisLayerGroup == null)
        //        throw new FormatException("По данному наименованию не найдена группа слоев");
        //    else
        //    {
        //        if (gisLayerGroup.Layers.Count > 0)
        //            foreach(SpatialLayerItem split in gisLayerGroup.Layers)
        //            {
        //            if (split.SpatialLayer != null)
        //                if(!split.SpatialLayer.isRaster)
        //                    if (split.SpatialLayer.LayerId != null || split.SpatialLayer.LayerId!= String.Empty)
        //                        res.Add(split.SpatialLayer.LayerId);
        //            }
        //    }
        //    return res;
        //}
        ///// <summary>
        ///// Массив Id растровых слоев по названию группы слоев
        ///// </summary>
        ///// <param name="LayerName">Наименование группы слоев</param>
        ///// <returns></returns>
        //public List<string> GetMapsIdsFromLayerGroupName(string LayerGroupName)
        //{
        //    List<string> res = new List<string>();
        //    if (string.IsNullOrEmpty(LayerGroupName))
        //        throw new FormatException("Не указано наименование группы слоев");
        //    GisLayerGroup gisLayerGroup = connect.FindFirstObject<GisLayerGroup>(mc => mc.Name == LayerGroupName);
        //    if (gisLayerGroup == null)
        //        throw new FormatException("По данному наименованию не найдена группа слоев");
        //    else
        //    {
        //        if (gisLayerGroup.Layers.Count > 0)
        //            foreach (SpatialLayerItem split in gisLayerGroup.Layers)
        //            {
        //                if (split.SpatialLayer != null)
        //                    if (split.SpatialLayer.isRaster)
        //                        if (split.SpatialLayer.LayerId != null || split.SpatialLayer.LayerId != String.Empty)
        //                            res.Add(split.SpatialLayer.LayerId);
        //            }
        //    }
        //    return res;
        //}
    }
}
