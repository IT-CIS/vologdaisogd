﻿using Ingeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WinAisogdIngeo.Module.SystemDir.Spatial
{
    public enum SpatialImageMark
    {
        None,
        Opaque,
        Hollow,
    }
    public enum SpatialImageLegend
    {
        None,
        All,
        Used,
    }


    public class SpatialImageLegendFile
    {
        public readonly string ImageFileName;
        public readonly string StyleName;

        public SpatialImageLegendFile(string imageFileName, string styleName)
        {
            this.ImageFileName = imageFileName;
            this.StyleName = styleName;
        }
    }
    public class SpatialImageBuilder
    {
        public double HeightInMm = 100.0;
        public double WidthInMm = 100.0;
        public int Dpi = 96;
        public bool YMirror = true;
        public double XAngleInDegree = 90.0;
        public SpatialImageMark Mark = SpatialImageMark.None;
        public SpatialImageLegend Legend = SpatialImageLegend.None;
        private const string NullBrushXML = "<brush style='none'/>";
        private const string NullPenXML = "<pen style='none'/>";
        public double CenterX;
        public double CenterY;
        public double ZoomScale;
        public double MarkX1;
        public double MarkY1;
        public double MarkX2;
        public double MarkY2;
        public double LegendWidthInMm;
        public double LegendHeightInMm;
        public string[] GeneratedImageFiles;
        public SpatialImageLegendFile[] GeneratedLegendFiles;


        public void Generate(SpatialLayerGroup layerGroup, string fileNamePrefix, string fileFormat)
        {
            List<IIngeoStyle> styles = new List<IIngeoStyle>();
            string str1 = "";
            for (int index = 0; index < layerGroup.Items.Count; ++index)
            {
                SpatialLayerGroupItem spatialLayerGroupItem = layerGroup.Items[index];
                string localId = this.Session.GetLocalId(spatialLayerGroupItem.Id);
                if (spatialLayerGroupItem.ItemType == SpatialLayerGroupItemType.RasterMap)
                {
                    IIngeoMap ingeoMap = this.Session.Db.MapFromID(localId);
                    if (SpatialImageBuilder.ZoomScaleInRange(this.ZoomScale, ingeoMap.VisibleMin, ingeoMap.VisibleMax))
                        str1 = str1 + "<map oid='" + localId + "'/>";
                }
                else
                {
                    IIngeoLayer ingeoLayer = this.Session.Db.LayerFromID(localId);
                    if (SpatialImageBuilder.ZoomScaleInRange(this.ZoomScale, ingeoLayer.VisibleMin, ingeoLayer.VisibleMax))
                    {
                        IIngeoVectorMap map = ingeoLayer.Map;
                        if (SpatialImageBuilder.ZoomScaleInRange(this.ZoomScale, map.VisibleMin, map.VisibleMax))
                        {
                            str1 = str1 + "<layer oid='" + localId + "'/><flush-after-layer/>";
                            if (this.Legend != SpatialImageLegend.None)
                            {
                                foreach (IIngeoStyle style in ingeoLayer.Styles)
                                {
                                    if (SpatialImageBuilder.IsStyleDefineGeometry(style))
                                        styles.Add(style);
                                }
                            }
                        }
                    }
                }
            }
            string str2 = str1 + "<flush-after-all/>";
            string str3;
            if (this.Mark != SpatialImageMark.None && this.MarkContour == null)
            {
                double centerX = (this.MarkX1 + this.MarkX2) / 2.0;
                double centerY = (this.MarkY1 + this.MarkY2) / 2.0;
                double num = Math.Sqrt((centerX - this.MarkX1) * (centerX - this.MarkX1) + (centerX - this.MarkX1) * (centerY - this.MarkY1)) * this.ZoomScale + 1.0 / 400.0;
                str3 = this.Mark != SpatialImageMark.Hollow ? SpatialImageBuilder.CircleXML(centerX, centerY, num / this.ZoomScale, "<pen style='none'/>", SpatialImageBuilder.SolidBrushXML(SpatialImageBuilder.RGB((int)byte.MaxValue, (int)byte.MaxValue, (int)byte.MaxValue))) + SpatialImageBuilder.CircleXML(centerX, centerY, (num - 0.001) / this.ZoomScale, "<pen style='none'/>", SpatialImageBuilder.SolidBrushXML(SpatialImageBuilder.RGB(0, 0, 0))) : SpatialImageBuilder.CircleXML(centerX, centerY, num / this.ZoomScale, SpatialImageBuilder.SolidPenXML(SpatialImageBuilder.RGB((int)byte.MaxValue, (int)byte.MaxValue, (int)byte.MaxValue), 2.0, 0.0), "<brush style='none'/>") + SpatialImageBuilder.CircleXML(centerX, centerY, num / this.ZoomScale, SpatialImageBuilder.SolidPenXML(SpatialImageBuilder.RGB(0, 0, 0), 1.0, 0.0), "<brush style='none'/>");
            }
            else
                str3 = "";
            List<string> imageFiles = new List<string>();
            List<SpatialImageLegendFile> imageLegendFiles = new List<SpatialImageLegendFile>();
            double boundsX1;
            double boundsY1;
            double boundsX2;
            double boundsY2;
            this.MakeImage("<paint>" + str2 + str3 + "</paint>", fileNamePrefix, fileFormat, imageFiles, out boundsX1, out boundsY1, out boundsX2, out boundsY2);
            if (this.Legend != SpatialImageLegend.None)
            {
                if (this.Legend == SpatialImageLegend.Used)
                    styles = this.CollectUsedStyles(layerGroup, boundsX1, boundsY1, boundsX2, boundsY2);
                this.MakeLegendImages(styles, fileNamePrefix, fileFormat, imageLegendFiles);
            }
            this.GeneratedImageFiles = imageFiles.ToArray();
            this.GeneratedLegendFiles = imageLegendFiles.ToArray();
        }

        private void MakeLegendImages(List<IIngeoStyle> styles, string fileNamePrefix, string fileFormat, List<SpatialImageLegendFile> imageLegendFiles)
        {
            for (int index = 0; index < styles.Count; ++index)
            {
                IIngeoStyle style = styles[index];
                IInImage ingeoImage = this.Session.CreateIngeoImage();
                int num1 = this.MMToPixels(this.LegendWidthInMm);
                int num2 = this.MMToPixels(this.LegendHeightInMm);
                ingeoImage.Width = num1;
                ingeoImage.Height = num2;
                ingeoImage.ResolutionX = this.Dpi;
                ingeoImage.ResolutionY = this.Dpi;
                IIngeoPaintSurface surface = ingeoImage.Surface;
                IIngeoMatrixProjectionNavigator projectionNavigator = (IIngeoMatrixProjectionNavigator)surface.Navigator;
                projectionNavigator.YMirror = false;
                projectionNavigator.XAngle = 0.0;
                projectionNavigator.Navigate(0.0, 0.0, this.ZoomScale);
                int num3 = num2 / 8;
                double aPaperX1;
                double aPaperY1;
                double aPaperX2;
                double aPaperY2;
                surface.RectDeviceToPaper(num3, num3, num1 - num3, num2 - num3, out aPaperX1, out aPaperY1, out aPaperX2, out aPaperY2);
                double aWorldX1;
                double aWorldY1;
                double aWorldX2;
                double aWorldY2;
                surface.Projection.UnprojectBounds(aPaperX1, aPaperY1, aPaperX2, aPaperY2, out aWorldX1, out aWorldY1, out aWorldX2, out aWorldY2);
                SpatialContour contourForStyleLegend = this.CreateContourForStyleLegend(style, aWorldX1, aWorldY1, aWorldX2, aWorldY2);
                if (contourForStyleLegend != null)
                {
                    style.PaintContour(surface, contourForStyleLegend.IngeoContour, "Текст");
                    string str = fileNamePrefix + style.ID + "." + fileFormat;
                    ingeoImage.SaveToFile(str);
                    imageLegendFiles.Add(new SpatialImageLegendFile(str, style.Name));
                }
            }
        }

        private SpatialContour CreateContourForStyleLegend(IIngeoStyle style, double x1, double y1, double x2, double y2)
        {
            if (!SpatialImageBuilder.IsStyleDefineGeometry(style))
                return (SpatialContour)null;
            bool flag = false;
            SpatialImageBuilder.PainterKind painterKind = (SpatialImageBuilder.PainterKind)0;
            foreach (IIngeoPainter ingeoPainter in style.Painters)
            {
                if (SpatialImageBuilder.ZoomScaleInRange(this.ZoomScale, ingeoPainter.VisibleMin, ingeoPainter.VisibleMax))
                {
                    flag = true;
                    switch (ingeoPainter.PainterType)
                    {
                        case TIngeoPainterType.inptStd:
                            IIngeoStdPainter ingeoStdPainter = (IIngeoStdPainter)ingeoPainter;
                            if (ingeoStdPainter.Pen.Style != TInPenStyle.inpsClear)
                                painterKind |= SpatialImageBuilder.PainterKind.Line;
                            if (ingeoStdPainter.Brush.Style != TInBrushStyle.inbsClear)
                            {
                                painterKind |= SpatialImageBuilder.PainterKind.Area;
                                break;
                            }
                            break;
                        case TIngeoPainterType.inptSymbol:
                            switch (((IIngeoSymbolPainter)ingeoPainter).PaintMethod)
                            {
                                case TIngeoPicturePaintMethod.inppmSingle:
                                    painterKind |= SpatialImageBuilder.PainterKind.Point;
                                    break;
                                case TIngeoPicturePaintMethod.inppmFill:
                                case TIngeoPicturePaintMethod.inppmRect:
                                    painterKind |= SpatialImageBuilder.PainterKind.Area;
                                    break;
                                default:
                                    painterKind |= SpatialImageBuilder.PainterKind.Line;
                                    break;
                            }
                    }
                }
            }
            if (!flag || painterKind == (SpatialImageBuilder.PainterKind)0)
                return (SpatialContour)null;
            if (style.CreationClosing != TIngeoStyleClosing.instCloseNone)
                painterKind |= SpatialImageBuilder.PainterKind.Area;
            SpatialContour contour = this.Session.CreateContour();
            SpatialContourPart spatialContourPart = contour.Parts.Insert(-1);
            if ((painterKind & SpatialImageBuilder.PainterKind.Area) != (SpatialImageBuilder.PainterKind)0)
            {
                spatialContourPart.Closed = true;
                spatialContourPart.InsertVertex(-1, x1, y1, 0.0);
                spatialContourPart.InsertVertex(-1, x2, y1, 0.0);
                spatialContourPart.InsertVertex(-1, x2, y2, 0.0);
                spatialContourPart.InsertVertex(-1, x1, y2, 0.0);
            }
            else if ((painterKind & SpatialImageBuilder.PainterKind.Line) != (SpatialImageBuilder.PainterKind)0)
            {
                spatialContourPart.InsertVertex(-1, x1, y1, 0.0);
                spatialContourPart.InsertVertex(-1, x1 + (x2 - x1) / 3.0, y2, 0.0);
                spatialContourPart.InsertVertex(-1, x2 - (x2 - x1) / 3.0, y1, 0.0);
                spatialContourPart.InsertVertex(-1, x2, y2, 0.0);
            }
            else
                spatialContourPart.InsertVertex(-1, (x1 + x2) / 2.0, (y1 + y2) / 2.0, 0.0);
            return contour;
        }

        private static bool IsStyleDefineGeometry(IIngeoStyle style)
        {
            return (style.Flags & TIngeoStyleFlags.instDefineGeometry) != TIngeoStyleFlags.instPaintOrderNormal;
        }

        private List<IIngeoStyle> CollectUsedStyles(SpatialLayerGroup layerGroup, double boundsX1, double boundsY1, double boundsX2, double boundsY2)
        {
            Dictionary<string, IIngeoStyle> dictionary1 = new Dictionary<string, IIngeoStyle>();
            Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
            for (int index = 0; index < layerGroup.Items.Count; ++index)
            {
                SpatialLayerGroupItem spatialLayerGroupItem = layerGroup.Items[index];
                if (spatialLayerGroupItem.ItemType == SpatialLayerGroupItemType.Layer)
                {
                    string localId1 = this.Session.GetLocalId(spatialLayerGroupItem.Id);
                    IIngeoLayer ingeoLayer = this.Session.Db.LayerFromID(localId1);
                    if (SpatialImageBuilder.ZoomScaleInRange(this.ZoomScale, ingeoLayer.VisibleMin, ingeoLayer.VisibleMax))
                    {
                        IIngeoMapObjectsQuery ingeoMapObjectsQuery = this.Session.Db.MapObjects.QueryByRect((object)localId1, boundsX1, boundsY1, boundsX2, boundsY2, false);
                        while (!ingeoMapObjectsQuery.EOF)
                        {
                            foreach (SpatialShape spatialShape in (IEnumerable<SpatialShape>)this.Session[ingeoMapObjectsQuery.ObjectID].Shapes)
                            {
                                string localId2 = this.Session.GetLocalId(spatialShape.StyleId);
                                if (!dictionary1.ContainsKey(localId2) && !dictionary2.ContainsKey(localId2))
                                {
                                    IIngeoStyle style = this.Session.Db.StyleFromID(localId2);
                                    if (SpatialImageBuilder.IsStyleDefineGeometry(style))
                                    {
                                        foreach (IIngeoPainter ingeoPainter in style.Painters)
                                        {
                                            if (SpatialImageBuilder.ZoomScaleInRange(this.ZoomScale, ingeoPainter.VisibleMin, ingeoPainter.VisibleMax))
                                            {
                                                dictionary1.Add(localId2, style);
                                                style = (IIngeoStyle)null;
                                                break;
                                            }
                                        }
                                    }
                                    if (style != null)
                                        dictionary2.Add(localId2, (object)null);
                                }
                            }
                            ingeoMapObjectsQuery.MoveNext();
                        }
                    }
                }
            }
            return new List<IIngeoStyle>((IEnumerable<IIngeoStyle>)dictionary1.Values);
        }

        private void MakeImage(string paintXml, string fileNamePrefix, string fileFormat, List<string> imageFiles, out double boundsX1, out double boundsY1, out double boundsX2, out double boundsY2)
        {
            int num1 = this.MMToPixels(this.WidthInMm);
            int num2 = this.MMToPixels(this.HeightInMm);
            IInImage ingeoImage = this.Session.CreateIngeoImage();
            ingeoImage.Width = num1;
            ingeoImage.Height = num2;
            ingeoImage.ResolutionX = this.Dpi;
            ingeoImage.ResolutionY = this.Dpi;
            IIngeoPaintSurface surface = ingeoImage.Surface;
            IIngeoMatrixProjectionNavigator projectionNavigator = (IIngeoMatrixProjectionNavigator)surface.Navigator;
            projectionNavigator.XAngle = SpatialImageBuilder.DegToRad(this.XAngleInDegree);
            projectionNavigator.YMirror = this.YMirror;
            projectionNavigator.Navigate(this.CenterX, this.CenterY, this.ZoomScale);
            surface.Projection.UnprojectBounds(surface.PaperX1, surface.PaperY1, surface.PaperX2, surface.PaperY2, out boundsX1, out boundsY1, out boundsX2, out boundsY2);
            this.Session.Db.PaintXml(surface, paintXml);
            if (this.MarkContour != null)
            {
                IIngeoStdPainter ingeoStdPainter = this.Session.CreateIngeoStdPainter();
                ingeoStdPainter.Brush.Style = TInBrushStyle.inbsClear;
                ingeoStdPainter.Pen.Style = TInPenStyle.inpsSolid;
                ingeoStdPainter.Pen.Color = SpatialImageBuilder.RGB((int)byte.MaxValue, (int)byte.MaxValue, (int)byte.MaxValue);
                ingeoStdPainter.Pen.WidthInMM = 2.0;
                ingeoStdPainter.PaintContour(surface, this.MarkContour.IngeoContour, "");
                ingeoStdPainter.Pen.Color = SpatialImageBuilder.RGB(0, 0, 0);
                ingeoStdPainter.Pen.WidthInMM = 1.0;
                ingeoStdPainter.PaintContour(surface, this.MarkContour.IngeoContour, "");
            }
            string aFileName = fileNamePrefix + "0." + fileFormat;
            ingeoImage.SaveToFile(aFileName);
            imageFiles.Add(aFileName);
        }

        private static double DegToRad(double degree)
        {
            return degree * Math.PI / 180.0;
        }

        private int MMToPixels(double mm)
        {
            return (int)Math.Round(mm * (double)this.Dpi / 25.4);
        }

        private static string SolidBrushXML(int color)
        {
            return "<brush style='solid' color='" + SpatialImageBuilder.ColorXML(color) + "'/>";
        }

        private static string CircleXML(double centerX, double centerY, double radius, string penXml, string brushXml)
        {
            return string.Format("<contour>{0}{1}<polygon closed='yes'><point x='{2}' y='{3}' convexity='1'/><point x='{4}' y='{5}' convexity='1'/></polygon></contour>", (object)penXml, (object)brushXml, (object)centerX, (object)(centerY - radius), (object)centerX, (object)(centerY + radius));
        }

        private static string SolidPenXML(int color, double widthInMm, double forZoomScale)
        {
            return string.Format("<pen style='solid' color='{0}' width='{1}' for-zoom-scale='{2}'/>", (object)SpatialImageBuilder.ColorXML(color), (object)widthInMm, (object)forZoomScale);
        }

        private static int RGB(int red, int green, int blue)
        {
            return blue << 16 | green << 8 | red;
        }

        private static string ColorXML(int color)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}", (object)(color & (int)byte.MaxValue), (object)(color >> 8 & (int)byte.MaxValue), (object)(color >> 16 & (int)byte.MaxValue));
        }

        private static bool ZoomScaleInRange(double scale, double scale1, double scale2)
        {
            if (scale == 0.0 || scale1 == 0.0 && scale2 == 0.0)
                return true;
            if (scale1 < scale2)
            {
                if (scale1 == 0.0)
                    return scale >= scale2;
                return scale >= scale1 && scale <= scale2;
            }
            if (scale2 == 0.0)
                return scale >= scale1;
            return scale >= scale2 && scale <= scale1;
        }

        [Flags]
        private enum PainterKind
        {
            Point = 1,
            Line = 2,
            Area = 4,
        }
    }

}
