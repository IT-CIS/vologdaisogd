﻿using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Utils;
using WinAisogdMapInfo.Module.SystemDir;

namespace AISOGD.SystemDir
{
    [Custom("Caption", "Настройки пространственных связей"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("IsogdClassName")]
    public class SpatialConfig : BaseObjectXAF
    {
        public SpatialConfig() : base() { }
        public SpatialConfig(Session session) : base(session) {}

        //private Guid _id;
        //[Browsable(false)]
        //[Key(true)]
        //public Guid ID
        //{
        //    get { return _id; }
        //    set { SetPropertyValue("ID", ref _id, value); }
        //}

        private Type _className;

        [TypeConverter(typeof(ClassInfoTypeConverter))]
        [ValueConverter(typeof(TypeToStringConverter))]
        [DevExpress.Xpo.DisplayName("Класс ИСОГД")]
        public Type IsogdClassName
        {
            get { return _className; }
            set { SetPropertyValue("IsogdClassName", ref _className, value); }
        } 

        [Association, DevExpress.Xpo.DisplayName("Таблицы(слои)")]
        public XPCollection<SpatialLayerItem> Layers
        {
            get { return GetCollection<SpatialLayerItem>("Layers"); }
        }

        //protected override void OnSaving()
        //{
        //    sortLayers();
        //    base.OnSaving();
        //}

        public class ClassInfoTypeConverter : LocalizedClassInfoTypeConverter
        {
            public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                var values = new List<Type>();
                foreach (ITypeInfo info in XafTypesInfo.Instance.PersistentTypes)
                {
                    if ((info.IsVisible && info.IsPersistent) && (info.Type != null))
                    {
                        values.Add(info.Type);
                    }
                }
                values.Sort(this);
                values.Insert(0, null);
                return new StandardValuesCollection(values);
            }
        }
    }
}