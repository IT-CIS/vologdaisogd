﻿using DevExpress.Xpo;
using Ingeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.SystemDir
{
    /// <summary>
    /// Класс содержит набор методов и функций для работы с пространственными объектами
    /// </summary>
    public partial class IngeoSpatial
    {
        public static object Sync = new object();

        [DllImport("oleaut32.dll")]
        private static extern long GetActiveObject(ref Guid rclsid, IntPtr pvResreved, out IntPtr ppunk);

        /// <summary>
        /// Получить активную Ингео
        /// </summary>
        /// <returns>IngeoApplication</returns>
        public IngeoApplication GetActiveIngeo()
        {
            IntPtr ppunk;
            var rclsid = new Guid("{04088492-0485-11D4-9719-000021C6D845}");
            GetActiveObject(ref rclsid, IntPtr.Zero, out ppunk);
            if (ppunk.ToInt32() != 0)
                return Marshal.GetObjectForIUnknown(ppunk) as IngeoApplication;
            else
                return null;
        }

        ///<summary>
        ///Перевести глобальный идетнификатор в локальный 
        ///</summary>
        ///<param name="aIngeoApp">Ингео application</param>
        ///<param name="aGlobalID">Глобальный идетнификатор</param>
        ///<returns>Возвращается локальный идентификатор</returns>
        ///<exception cref="Exception"></exception>
        public static string GlobalIDToLocalID(IIngeoApplication aIngeoApp, string aGlobalID)
        {
            if (aGlobalID.Length < 20)
                return aGlobalID;
            //throw new Exception("GlobalIDToLocalID: Идентификатор не удовлетворяет параметрам глобального = " + aGlobalID);

            string result = aIngeoApp.ActiveDb.GlobalIDToLocalID(aGlobalID.Substring(0, 38), Convert.ToInt32(aGlobalID.Substring(38, aGlobalID.Length - 38)));
            return result;
        }

        ///<summary>
        ///</summary>
        ///<param name="aIngeoApp"></param>
        ///<param name="aGlobalID"></param>
        ///<returns></returns>
        ///<exception cref="Exception"></exception>
        public static string LocalIDToGlobalID(IIngeoApplication aIngeoApp, string aLocalID)
        {
            if (aLocalID.Length > 12)
                return aLocalID;
            string result = "";
            object Space;
            object Number;
            aIngeoApp.ActiveDb.LocalIDToGlobalID(aLocalID, out Space, out Number);
            result = Space.ToString() + Number.ToString();
            return result;
        }


        /// <summary>
        /// Функция возвращает пространственный объект по его идентификатору
        /// </summary>
        /// <param name="aIngeoApp">Приложение ИнГео</param>
        /// <param name="aObjectId">Идентификатор Объекта</param>
        /// <returns>Функция возвращает пространственный объект по его идентификатору, если объект не существует возвращается NULL</returns>
        public static IIngeoMapObject GetOneMapObjectBySpatialObjectID(IIngeoApplication aIngeoApp, string aObjectId)
        {
            IIngeoMapObject mapObject = null;
            IIngeoMapObjects mapObjects = aIngeoApp.ActiveDb.MapObjects;
            if (mapObjects.IsObjectExists(aObjectId))
            {
                mapObject = mapObjects.GetObject(aObjectId);
            }
            return mapObject;
        }

        /// <summary>
        /// Функция возвращает список объектов заданного слоя которые пересекаются с объектом запроса
        /// </summary>
        /// <param name="aMapObject">Исходный пространственный объект</param>
        /// <param name="aLocalLayerID">Локальный идентификатор слоя пересечения</param>
        /// <returns>Список объектов пересечения</returns>
        public static IIngeoMapObject[] CrossingMapObjectsByLayer(string aLocalLayerID, IIngeoMapObject aMapObject)
        {
            IIngeoMapObject[] returnObjectsList = null;
            if (aMapObject != null)
            {
                int n = 0;
                lock (IngeoSpatial.Sync)
                {
                    IIngeoMapObjectsQuery mapObjectsQuery = aMapObject.MapObjects.QueryByObject(aLocalLayerID, aMapObject.ID, TIngeoContourRelation.incrIntersected, TIngeoContourRelation.incrIntersected);
                    while (!mapObjectsQuery.EOF)
                    {
                        n++;
                        mapObjectsQuery.MoveNext();
                    }
                }
                returnObjectsList = new IIngeoMapObject[n];
                int i = -1;
                lock (IngeoSpatial.Sync)
                {
                    IIngeoMapObjectsQuery mapObjectsQuery = aMapObject.MapObjects.QueryByObject(aLocalLayerID, aMapObject.ID, TIngeoContourRelation.incrIntersected, TIngeoContourRelation.incrIntersected);
                    while (!mapObjectsQuery.EOF)
                    {
                        i++;
                        returnObjectsList[i] = aMapObject.MapObjects.GetObject(mapObjectsQuery.ObjectID);
                        mapObjectsQuery.MoveNext();
                    }
                }
            }
            return returnObjectsList;
        }

        /// <summary>
        /// Функция копирует пространственный объект в другой слой(должны быть указаны ID стиля назначения).Копируются все шейпы объекта в один стиль . Семантика объекта не переносится.
        /// </summary>
        /// <param name="aIngeoApp">Приложение ИнГео</param>
        /// <param name="aSourceObject">Исходный пространственный объект</param>
        /// <param name="aDestStyleID">Локальный Id стиля назначения</param>
        /// <returns>Возвращается копия пространственного объекта. Если исходный обект null - возвращается null</returns>
        public static IIngeoMapObject CloneMapObject(IIngeoApplication aIngeoApp, IIngeoMapObject aSourceObject, string aDestStyleID)
        {
            IIngeoMapObject mapObject = null;
            if (!aIngeoApp.ActiveDb.StyleExists(aDestStyleID))
                throw new Exception(string.Format("Невозможно клонировать пространственный объект. Причина: не найден стиль назначения копирования ID = {0}", aDestStyleID));

            IIngeoStyle destStyle = aIngeoApp.ActiveDb.StyleFromID(aDestStyleID);

            IIngeoMapObjects mapObjects = aIngeoApp.ActiveDb.MapObjects;
            if (aSourceObject != null)
            {
                if (mapObjects.IsObjectExists(aSourceObject.ID))
                {
                    mapObject = mapObjects.AddObject(destStyle.Layer.ID);
                    foreach (IIngeoShape sourceShape in aSourceObject.Shapes)
                    {
                        IIngeoShape destShape = mapObject.Shapes.Insert(-1, aDestStyleID);
                        destShape.Contour.AddPartsFrom(sourceShape.Contour);
                    }
                    mapObjects.UpdateChanges();
                }
            }
            return mapObject;
        }
        /// <summary>
        /// Изменить стиль пространственного объекта
        /// </summary>
        /// <param name="aMapObject">Исходный пространственный объект</param>
        /// <param name="aToStyleID">ID конечного стиля пространственного объекта</param>
        /// <param name="aIngeoApp">Приложение ИнГео</param>
        public static void ChangeSpatialObjectStyle(IIngeoMapObject aMapObject, string aToStyleID, IIngeoApplication aIngeoApp)
        {
            if (aMapObject != null)
            {
                if (aIngeoApp == null)
                    throw new Exception(string.Format("Невозможно изменить стиль пространственного объекта. Причина: отсутствует приложение ИнГео."));
                if (!aIngeoApp.ActiveDb.StyleExists(aToStyleID))
                    throw new Exception("Невозможно изменить стиль пространственного объекта. Причина: в ИнГео не обнаружен исходный стиль для замены.");
                foreach (IIngeoShape shape in aMapObject.Shapes)
                {
                    if (shape.DefineGeometry)
                    {
                        shape.StyleID = aToStyleID;
                    }
                }
                aMapObject.MapObjects.UpdateChanges();
            }
            else throw new Exception(string.Format("Невозможно изменить стиль пространственного объекта. Причина: отсутствует пространственный объект."));
        }
    }
}
