﻿using System;
using System.Collections;
using System.Xml;
using Ingeo;
using IngeoMapX;
//using Integro.InMeta.Runtime;
using System.IO;
using DevExpress.XtraEditors;

namespace AISOGD.SystemDir
{
    /// <summary>
    /// Дополнение содержит функции по получению и формированию картинок в Ингео в ИнГео
    /// </summary>
    public partial class IngeoSpatial
    {
        /// <summary>
        /// Метод возврящает координаты прямоугольника описывающего пространственный объект
        /// </summary>
        /// <param name="aMapObjectId">ID пространственного объекта</param>
        /// <param name="aX1">Координата x1</param>
        /// <param name="aY1">Координата y1</param>
        /// <param name="aX2">Координата x2</param>
        /// <param name="aY2">Координата y2</param>
        /// <param name="aIngeo">Приложение ИнГео</param>
        public static void GetImageBounds(string aMapObjectId, out double aX1, out double aY1, out double aX2, out double aY2, IIngeoApplication aIngeo)
        {
            IIngeoMapObject mapObject = aIngeo.ActiveDb.MapObjects.GetObject(aMapObjectId);

            aX1 = mapObject.X1;
            aY1 = mapObject.Y1;
            aX2 = mapObject.X2;
            aY2 = mapObject.Y2;
        }

        /// <summary>
        /// Метод возвращает квадрат в который вписываются пространственные объекты
        /// </summary>
        /// <param name="aMapObjects">Пространственные объекты</param>
        /// <returns>Метод возвращает квадрат в который вписываются пространственные объекты</returns>
        public static Rect GetBoundRect(IIngeoMapObject[] aMapObjects)
        {
            Rect boundRect = new Rect();
            boundRect.X1 = aMapObjects[0].X1;
            boundRect.Y1 = aMapObjects[0].Y1;
            boundRect.X2 = aMapObjects[0].X2;
            boundRect.Y2 = aMapObjects[0].Y2;
            for (int i = 1; i < aMapObjects.Length; i++)
            {
                if (aMapObjects[i].X1 < boundRect.X1)
                {
                    boundRect.X1 = aMapObjects[i].X1;
                }
                if (aMapObjects[i].Y1 < boundRect.Y1)
                {
                    boundRect.Y1 = aMapObjects[i].Y1;
                }
                if (aMapObjects[i].X2 > boundRect.X2)
                {
                    boundRect.X2 = aMapObjects[i].X2;
                }
                if (aMapObjects[i].Y2 > boundRect.Y2)
                {
                    boundRect.Y2 = aMapObjects[i].Y2;
                }
            }
            return boundRect;
        }
        /// <summary>
        /// Класс описывает координаты квадратного объекта
        /// </summary>
        public class Rect
        {
            ///<summary>
            ///</summary>
            public double X1;
            ///<summary>
            ///</summary>
            public double Y1;
            ///<summary>
            ///</summary>
            public double X2;
            ///<summary>
            ///</summary>
            public double Y2;
        }

        /// <summary>
        /// кол-во дюймов в одном метре
        /// </summary>
        const double K = 39.37;
        /// <summary>
        /// Метод возвращает строку, которую потом можно конвертировать в изображение
        /// </summary>
        /// <param name="aIngeoApp">Приложение ИнГео</param>
        /// <param name="aMapIDs">ID растровых карт</param>
        /// <param name="aLayerIds">ID слоев</param>
        /// <param name="aMapObjects">Пространственные объекты</param>
        /// <param name="aZoomScale">масштаб</param>
        /// <param name="aDpi">Разрешение</param>
        /// <param name="aImageType">TInImageDataType обект типа изображения</param>
        /// <param name="aBoundRect">описывающий объект</param>
        /// <returns>Метод возвращает строку, которую потом можно преобразовать в изображение</returns>
        public static string MakeImageBase64(IIngeoApplication aIngeoApp, string[] aMapIDs, string[] aLayerIds,
            IIngeoMapObject[] aMapObjects, double aZoomScale, int aDpi, TInImageDataType aImageType, Rect aBoundRect)
        {
            int dpiX = aDpi;
            int dpiY = aDpi;

            IInImage image = aIngeoApp.CreateObject(TIngeoObjectClass.inocImage, null) as IInImage;
            image.ResolutionX = dpiX;
            image.ResolutionY = dpiY;

            XmlDocument paintXmlDoc = new XmlDocument();
            paintXmlDoc.LoadXml("<paint/>");
            if (aMapIDs != null)
            {
                for (int i = 0; i < aMapIDs.Length; i++)
                {
                    XmlElement mapElem = paintXmlDoc.CreateElement("map");
                    mapElem.SetAttribute("oid", GlobalIDToLocalID(aIngeoApp,aMapIDs[i]));
                    if (paintXmlDoc.DocumentElement != null)
                        paintXmlDoc.DocumentElement.AppendChild(mapElem);
                }
            }
            if (aLayerIds != null)
            {
                for (int i = 0; i < aLayerIds.Length; i++)
                {
                    XmlElement layerElem = paintXmlDoc.CreateElement("layer");
                    layerElem.SetAttribute("oid",GlobalIDToLocalID(aIngeoApp, aLayerIds[i]));
                    if (paintXmlDoc.DocumentElement != null)
                        paintXmlDoc.DocumentElement.AppendChild(layerElem);
                }
            }
            if (aMapObjects != null)
            {
                for (int i = 0; i < aMapObjects.Length; i++)
                {
                    XmlElement mapObjectElem = paintXmlDoc.CreateElement("object");
                    mapObjectElem.SetAttribute("oid", aMapObjects[i].ID);
                    if (paintXmlDoc.DocumentElement != null)
                        paintXmlDoc.DocumentElement.AppendChild(mapObjectElem);
                }
            }
            image.Height = MMToPixels(200, 192); //Convert.ToInt32(dpiX * (aBoundRect.X2 - aBoundRect.X1) * K * aZoomScale);
            image.Width = MMToPixels(150, 192);//Convert.ToInt32(dpiY * (aBoundRect.Y2 - aBoundRect.Y1) * K * aZoomScale);

            IIngeoPaintSurface surface = image.Surface;
            IIngeoMatrixProjectionNavigator projectionNavigator = (IIngeoMatrixProjectionNavigator)surface.Navigator;
            projectionNavigator.XAngle = 1.5708; 
            projectionNavigator.YMirror = true;
            projectionNavigator.Navigate((aBoundRect.X2 + aBoundRect.X1) / 2, (aBoundRect.Y2 + aBoundRect.Y1) / 2, aZoomScale);

            double boundsX1;
            double boundsY1;
            double boundsX2;
            double boundsY2;

            surface.Projection.UnprojectBounds(surface.PaperX1, surface.PaperY1, surface.PaperX2, surface.PaperY2, out boundsX1, out boundsY1, out boundsX2, out boundsY2);
            aIngeoApp.ActiveDb.PaintXml(image.Surface, paintXmlDoc.OuterXml);
            //if (this.MarkContour != null)
            //{
            //    IIngeoStdPainter ingeoStdPainter = this.Session.CreateIngeoStdPainter();
            //    ingeoStdPainter.Brush.Style = TInBrushStyle.inbsClear;
            //    ingeoStdPainter.Pen.Style = TInPenStyle.inpsSolid;
            //    ingeoStdPainter.Pen.Color = SpatialImageBuilder.RGB((int)byte.MaxValue, (int)byte.MaxValue, (int)byte.MaxValue);
            //    ingeoStdPainter.Pen.WidthInMM = 2.0;
            //    ingeoStdPainter.PaintContour(surface, this.MarkContour.IngeoContour, "");
            //    ingeoStdPainter.Pen.Color = SpatialImageBuilder.RGB(0, 0, 0);
            //    ingeoStdPainter.Pen.WidthInMM = 1.0;
            //    ingeoStdPainter.PaintContour(surface, this.MarkContour.IngeoContour, "");
            //}
            //string aFileName = "__0." + fileFormat;
            //ingeoImage.SaveToFile(aFileName);
            //imageFiles.Add(aFileName);



            //image.Surface.Navigator.ZoomToFitWorldRect(aBoundRect.X1 - 20, aBoundRect.Y1- 20,
            //    aBoundRect.X2+ 20, aBoundRect.Y2+ 20);
            

            string base64Data = Convert.ToBase64String(image.get_Data(aImageType) as byte[]);
            return base64Data;
        }
        /// <summary>
        /// Метод возвращает строку, которую потом можно конвертировать в изображение
        /// </summary>
        /// <param name="aIngeoApp">Приложение ИнГео</param>
        /// <param name="aRMapId">ID карты</param>
        /// <param name="aMapObject">Пространственный объект</param>
        /// <param name="aZoomScale">Масштаб</param>
        /// <param name="aDpi">Разрешение</param>
        /// <param name="aImageType">TInImageDataType тип изображения</param>
        /// <returns>Метод возвращает строку, которую потом можно конвертировать в изображение</returns>
        public static string MakeAreaImageBase64(IIngeoApplication aIngeoApp, string aRMapId, IIngeoMapObject aMapObject,
                                                  double aZoomScale, int aDpi, TInImageDataType aImageType)
        {
            int dpiX = aDpi;
            int dpiY = aDpi;

            //IInImage image = (IInImage)aIngeoApp.CreateObject(TIngeoObjectClass.inocImage, null);
            IInImage image = aIngeoApp.CreateObject(TIngeoObjectClass.inocImage, null) as IInImage;
            image.ResolutionX = dpiX;
            image.ResolutionY = dpiY;

            XmlDocument paintXmlDoc = new XmlDocument();
            paintXmlDoc.LoadXml("<paint/>");
            if (aRMapId != String.Empty)
            {
                try
                {
                    foreach (string map in aRMapId.Split(';'))
                    {
                        XmlElement mapElem = paintXmlDoc.CreateElement("map");
                        mapElem.SetAttribute("oid", IngeoSpatial.GlobalIDToLocalID(aIngeoApp, map));
                        if (paintXmlDoc.DocumentElement != null)
                            paintXmlDoc.DocumentElement.AppendChild(mapElem);
                    }
                }
                catch { }
            }

            image.Height = Convert.ToInt32(dpiX * (aMapObject.X2 - aMapObject.X1) * K * aZoomScale);
            image.Width = Convert.ToInt32(dpiY * (aMapObject.Y2 - aMapObject.Y1) * K * aZoomScale);
            image.Surface.Navigator.ZoomToFitWorldRect(aMapObject.X1, aMapObject.Y1, aMapObject.X2, aMapObject.Y2);
            aIngeoApp.ActiveDb.PaintXml(image.Surface, paintXmlDoc.OuterXml);

            //string base64Data = Convert.ToBase64String((byte[])image.get_Data(aImageType));
            string base64Data = Convert.ToBase64String(image.get_Data(aImageType) as byte[]);
            return base64Data;
        }
        /// <summary>
        /// сфомировать изображение (которое состоит из одного пространственного объекта)
        /// </summary>
        /// <param name="aTableName">Наименование таблицы связи ИнГео с ИнМетой</param>
        /// <param name="aFieldName">Наименование поля связи в таблице</param>
        /// <param name="aFieldValue">Идентификатор реестрового объекта</param>
        /// <param name="aRasterMapId">ID растровой карты</param>
        /// <param name="aIngeo">Приложение ИнГео</param>
        /// <returns>Возврящается имя файля изабражения</returns>
        public static string GetObjectImage(IIngeoMapObject mapObj, string aRasterMapId, IIngeoApplication aIngeo)
        {
            // 
            IIngeoMapObject mapObject = mapObj;
            IIngeoMapObject[] mapObjects = new[] { mapObject };
            string[] mapIDs;
            if (aRasterMapId != String.Empty)
                mapIDs = new[] { aRasterMapId };
            else
                mapIDs = null;
            const double gaugeScale = 0.002;
            const int dpi = 192;
            string base64Image = MakeImageBase64(aIngeo, mapIDs, null, mapObjects, gaugeScale, dpi, TInImageDataType.inidBMP, GetBoundRect(mapObjects));
            byte[] byteImage = Convert.FromBase64String(base64Image);

            string fileName = Path.GetTempPath() + "\\" + mapObject.ID + ".bmp";
            FileInfo f = new FileInfo(fileName);
            BinaryWriter bw = new BinaryWriter(f.OpenWrite());
            bw.Write(byteImage);
            bw.Close();
            return fileName;
        }
        
        /// <summary>
        /// сфомировать растровое изображение
        /// </summary>
        /// <param name="aMapObjectId">ID пространственного объекта</param>
        /// <param name="aRasterMapId">ID растровой карты</param>
        /// <param name="aIngeo">Приложение ИнГео</param>
        /// <param name="aRConfig">xml элемент описывающий растровый слой в _config.xml</param>
        /// <returns>возврящается имя файла изображения</returns>
        public static string GetRasterAreaImage(string aMapObjectId, string aRasterMapId, IIngeoApplication aIngeo, string filename)
        {
            IIngeoMapObject mapObject = aIngeo.ActiveDb.MapObjects.GetObject(aMapObjectId);

            // Значения по умолчанию
            double gaugeScale = 0.002; // Масштаб 1:500
            int dpi = 192; // Разрешение

            //string rMapId;
            //if (aRasterMapId != String.Empty)
            //    rMapId = aRasterMapId;
            //else
            //{
            //    if (aRConfig == null)
            //    {
            //        throw new Exception(string.Format("Ошибка доступа к растровому слою дежурного плана: Проверьте настройки в файле _config.xml"));
            //    }
            //    rMapId = aRConfig.GetAttribute("layerid");
            //    dpi = Convert.ToInt32(aRConfig.GetAttribute("dpi"));
            //    int gauge = Convert.ToInt32(aRConfig.GetAttribute("gauge"));
            //    gaugeScale = 1.0 / gauge;
            //}
            string fileName = "";
            string base64Image = MakeAreaImageBase64(aIngeo, aRasterMapId, mapObject, gaugeScale, dpi, TInImageDataType.inidBMP);
            byte[] byteImage = Convert.FromBase64String(base64Image);

            //XtraMessageBox.Show("aRasterMapId:" + aRasterMapId );
            if (base64Image != null && base64Image != String.Empty)
            {
                fileName = Path.GetTempPath() + "\\SurveyAreaRasterMap_" + filename + ".bmp";
                FileInfo f = new FileInfo(fileName);
                BinaryWriter bw = new BinaryWriter(f.OpenWrite());
                bw.Write(byteImage);
                bw.Close();
            }
            return fileName;
        }
        private static int MMToPixels(double mm, double Dpi)
        {
            return (int)Math.Round(mm * (double)Dpi / 25.4);
        }
    }
}
