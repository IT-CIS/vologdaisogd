﻿using System;
using Ingeo;


namespace AISOGD.SystemDir
{
    /// <summary>
    /// Класс содержит функции по геометрическим построениям в ИнГео
    /// </summary>
    public static class GeometryFunc
    {


            //        Посчитать его площадь по формуле
            //s = 0.5 * ((X1-X2)*(Y1+Y2) + (X2-X3)*(Y2+Y3) + ... + (Xn-X1)*(Yn+Y1))
            //Если больше нуля, то многоугольник задан по часовой стрелке. 
        /// <summary>
        /// Функция возвращает направление обхода контура пространственного объекта
        /// </summary>
        /// <returns></returns>
        public static int ContourDirection(IIngeoContourPart cnt)
        {
            int result;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double sum = 0;
            double[,] arrayVertex = FillVertexArray(cnt);
            if (arrayVertex != null)
            {
                if (arrayVertex.Length != 0)
                {
                    for (int i = 0; i < arrayVertex.Length/2; i++)
                    {
                        if (i + 1 != arrayVertex.Length/2)
                        {
                            x1 = arrayVertex[0, i];
                            y1 = arrayVertex[1, i];
                            x2 = arrayVertex[0, i + 1];
                            y2 = arrayVertex[1, i + 1];
                            sum = sum + (x1 - x2)*(y1 + y2);
                        }
                        else
                        {
                            x1 = arrayVertex[0, i];
                            y1 = arrayVertex[1, i];
                            x2 = arrayVertex[0, 0];
                            y2 = arrayVertex[1, 0];
                            sum = sum + (x1 - x2)*(y1 + y2);
                        }
                    }
                }
            }

            if (sum > 0)
                return 0; 
            else
            {
                return 1; 
            }
        }


        /// <summary>
        /// Ф-кция возвращает заполненный координатами вершин объекта массив
        /// </summary>
        /// <param name="aMapObject"></param>
        /// <returns></returns>
        public static double[,] FillVertexArray(IIngeoContourPart cnt)
        {
            double x;
            double y;
            double c;
            double[,] ArrayVertexFirst;
            double[,] ArrayVertex = null;

            if (cnt.Closed) //Исключаем точки вне замкнутого контура ЗУ
            {
                //Первичный массив(может быть с совпадающими точками)
                ArrayVertexFirst = new double[2,cnt.VertexCount];
                for (int i = 0; i < cnt.VertexCount; i++)
                {
                    // Получаем координаты и номер точки
                    cnt.GetVertex(i, out x, out y, out c);
                    ArrayVertexFirst[0, i] = x;
                    ArrayVertexFirst[1, i] = y;
                }
                //Если последняя и первая точкм совпадают, перезаписываем массив без последней точки
                if (ArrayVertexFirst[0, 0] == ArrayVertexFirst[0, cnt.VertexCount - 1] &
                    ArrayVertexFirst[1, 0] == ArrayVertexFirst[1, cnt.VertexCount - 1])
                {
                    //Рабочий массив (урезанный)
                    ArrayVertex = new double[2,cnt.VertexCount - 1];
                    for (int i = 0; i < cnt.VertexCount - 1; i++)
                    {
                        // Получаем координаты и номер точки
                        cnt.GetVertex(i, out x, out y, out c);
                        ArrayVertex[0, i] = x;
                        ArrayVertex[1, i] = y;
                    }
                }
                else
                {
                    //Рабочий массив (полный)
                    ArrayVertex = new double[2,cnt.VertexCount];
                    for (int i = 0; i < cnt.VertexCount; i++)
                    {
                        // Получаем координаты и номер точки
                        cnt.GetVertex(i, out x, out y, out c);
                        ArrayVertex[0, i] = x;
                        ArrayVertex[1, i] = y;
                    }
                }

            }

            return ArrayVertex;
        }

        /// <summary>
        /// Ф-ция возвращает координаты точки пересечения 2х прямых, которые задаются на входе по двум точкам (x11,y11,x12,y12 и x21,y21,x22,y22)
        /// </summary>
        /// <param name="aX11"></param>
        /// <param name="aY11"></param>
        /// <param name="aX12"></param>
        /// <param name="aY12"></param>
        /// <param name="aX21"></param>
        /// <param name="aY21"></param>
        /// <param name="aX22"></param>
        /// <param name="aY22"></param>
        /// <param name="aX">Возвращаемое значение координаты x точки пересечения</param>
        /// <param name="aY">Возвращаемое значение координаты y точки пересечения</param>

        public static void CrossPoint(double aX11, double aY11, double aX12, double aY12, double aX21, double aY21, double aX22, double aY22, out double aX, out double aY)
        {
            double k1;
            double k2;
            double c1;
            double c2;

            k1 = (aY11 - aY12) / (aX11 - aX12);
            c1 = aY11 - k1 * aX11;

            k2 = (aY21 - aY22) / (aX21 - aX22);
            c2 = aY21 - k2 * aX21;

            aX = (c2 - c1) / (k1 - k2);
            aY = aX * k1 + c1;
        }

        /// <summary>
        /// Ф-ция определяющая, принадлежит ли т. отрезку, если заведомо известно, что она принадлежит прямой, на которой находится отрезок
        /// </summary>
        /// <param name="aX">Координата x точки</param>
        /// <param name="aY">Координата y точки</param>
        /// <param name="aX1">Координата x первой точки отрезка</param>
        /// <param name="aY1">Координата y первой точки отрезка</param>
        /// <param name="aX2">Координата x второй точки отрезка</param>
        /// <param name="aY2">Координата y второй точки отрезка</param>
        /// <returns></returns>

        public static bool PointBelongsToPieces(double aX, double aY, double aX1, double aY1, double aX2, double aY2)
        {
            bool belongs = false;

            if (Math.Abs(aX1 - aX) < Math.Abs(aX2 - aX1) & Math.Abs(aX2 - aX) < Math.Abs(aX2 - aX1) & Math.Abs(aY1 - aY) < Math.Abs(aY2 - aY1) & Math.Abs(aY2 - aY) < Math.Abs(aY2 - aY1))
                belongs = true;

            return belongs;
        }

        /// <summary>
        /// Ф-ция считающая длину отрезка по двум точкам
        /// </summary>
        /// <param name="aX1"></param>
        /// <param name="aY1"></param>
        /// <param name="aX2"></param>
        /// <param name="aY2"></param>
        /// <returns></returns>
        public static double PiecesLength(double aX1, double aY1, double aX2, double aY2)
        {
            double l;

            l = Math.Sqrt((aX2 - aX1) * (aX2 - aX1) + (aY2 - aY1) * (aY2 - aY1));

            return l;
        }

        //public static double[,] FillDistanceArray(double[,] ArrayVertex1, double[,] ArrayVertex2)
        //{
        //    double[,] ArrayDistance = new double[2, 4];

        //    double l1 = 0;
        //    double l2 = 0;

        //    for (int i = 0; i < ArrayVertex1.Length / 2; i++)
        //    {
        //        int x1 = ArrayVertex1[0, i];
        //        int y1 = ArrayVertex1[1, i];
        //        int x2 = ArrayVertex1[0, i + 1];
        //        int y2 = ArrayVertex1[1, i + 1];


        //        //Console.WriteLine("hi");
        //        ArrayDistance[0, i] = l1;
        //        ArrayDistance[1, i] = l2;
        //    }

        //    return ArrayDistance;
        //}
    }
}
