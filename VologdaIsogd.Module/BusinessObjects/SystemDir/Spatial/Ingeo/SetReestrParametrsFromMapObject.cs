﻿using AISOGD.Dictonaries;
using AISOGD.Land;
using AISOGD.Rosreestr;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using Ingeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AISOGD.SystemDir
{
    /// <summary>
    /// Класс содержит набор методов и функций для работы с пространственными объектами
    /// </summary>
    public partial class IngeoSpatial
    {

        public static void SetLotDataFromMapObject(IIngeoMapObject aMapObject, Lot lot, Connect connect)
        {
            string ingeoTableName = "LotInfo";
            //if (aIngeoApp == null)
            //{
            //    XtraMessageBox.Show("Не запущена ГИС Ингео. Для продолжения запустите Ингео и попробуйте снова!", "Инфо");
            //    return;
            //}
            if (aMapObject == null)
            {
                XtraMessageBox.Show("Не найден пространственный объект земельного участка.", "Инфо");
                return;
            }
            if (lot == null)
            {
                XtraMessageBox.Show("Не найден реестровый объект земельного участка.", "Инфо");
                return;
            }
            DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
            XtraMessageBoxForm xtraMessageBoxForm = new XtraMessageBoxForm();
            if (
                xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                        "Записать семантические данные пространственного объекта земельного участка в реестровый объект?",
                    "Запись семантических данных", dialogResults, null, 0)) == DialogResult.Yes)
            {
                try
                {
                    lot.CadastralNumber = aMapObject.SemData.GetDisplayText(ingeoTableName, "CadastralNumber", 0);
                }
                catch { }
                try
                {
                    lot.DateCreated = Convert.ToDateTime(aMapObject.SemData.GetDisplayText(ingeoTableName, "DateCreated", 0));
                }
                catch { }
                try
                {
                    lot.Adress = aMapObject.SemData.GetDisplayText(ingeoTableName, "Address", 0);
                }
                catch { }
                try
                {
                    string util = aMapObject.SemData.GetDisplayText(ingeoTableName, "Utilization", 0);
                    lot.ByDoc = util;
                    if (connect.FindFirstObject<dUtilizations>(mc => mc.Name == util) != null)
                        lot.Utilization = connect.FindFirstObject<dUtilizations>(mc => mc.Name == util);
                }
                catch { }
                try
                {
                    lot.Area = aMapObject.Square;
                    lot.Unit = connect.FindFirstObject<dUnit>(mc => mc.Name == "кв.м" || mc.Name == "м2");
                }
                catch { }

                try
                {
                    string rightkindstring = aMapObject.SemData.GetDisplayText(ingeoTableName, "Right", 0);
                    string ownerstring = aMapObject.SemData.GetDisplayText(ingeoTableName, "Owner", 0);

                    LawKind lawKind = null;
                    lawKind = connect.FindFirstObject<LawKind>(mc => mc.Name == rightkindstring);
                    if (lawKind == null)
                    {
                        lawKind = connect.CreateObject<LawKind>();
                        lawKind.Name = rightkindstring;
                    }
                    Right r = connect.CreateObject<Right>();
                    r.LawKind = lawKind;

                    Owner owner = null;
                    owner = connect.FindFirstObject<Owner>(mc => mc.Name == ownerstring);
                    if (owner == null)
                    {
                        owner = connect.CreateObject<Owner>();
                        owner.Name = ownerstring;
                        owner.Save();
                    }
                    r.Owners.Add(owner);
                    r.Save();
                    lot.Rights.Add(r);
                }
                catch { }
                lot.Save();
                UnitOfWork unitOfWork = (UnitOfWork)lot.Session;
                unitOfWork.CommitChanges();
            }

        }
    }
}
