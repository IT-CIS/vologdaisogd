﻿using DevExpress.Xpo;
using Ingeo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.SystemDir
{
    /// <summary>
    /// Класс содержит набор методов и функций для работы с пространственными объектами
    /// </summary>
    public partial class IngeoSpatial
    {
        //Hashtable pointHashStyles;
        // Создать точки с подписями номеров вершин и координат по краям контура участка
        public static void CreatePointsForContour(IIngeoApplication aIngeoApp, IIngeoMapObject mapObject, Connect connect,
            SpatialController<GisLayer> spatial)
        {
            // находим реестровый объект настроек слоя с точками
            string pointLayerName = "Линии выносок";
            GisLayer gisLayerPoint = connect.FindFirstObject<GisLayer>(mc => mc.Name == pointLayerName);
            string pointLayerLocalId = GlobalIDToLocalID(aIngeoApp, gisLayerPoint.LayerId);
            int count = gisLayerPoint.GisStyles.Count;
            //if (this.pointHashStyles == null)
            //    this.pointHashStyles = new Hashtable(count);
            //// ActivityWebApp.LogUtils.WriteTXT("if (this.pointHashStyles.Count != count)");
            //if (this.pointHashStyles.Count != count)
            //    foreach (XmlElement pointStyleNode in pointConfig.GetElementsByTagName("style"))
            //        if (!this.pointHashStyles.ContainsKey(pointStyleNode.GetAttribute("type")))
            //            pointHashStyles.Add(pointStyleNode.GetAttribute("type"), InGeo.GlobalIDToLocalID(session.Application, pointStyleNode.GetAttribute("globalId")));
            IIngeoMapObjects mapObjects = mapObject.MapObjects;
            string ID = mapObject.ID;
            if(count != 0)
                Direct(aIngeoApp, mapObject, mapObjects, ID, pointLayerName, pointLayerLocalId, spatial);// Вызов функции отрисовки
            // ActivityWebApp.LogUtils.WriteTXT("  Direct(SpatialObject, pointLayerId, mapObjects, ID, this.pointHashStyles, isCreated, GPZUObject); - 1");
        }

        /// <summary>
        /// Постоить выноски и номера точек
        /// </summary>
        /// <param name="MapObj"></param>
        /// <param name="pointLayerId"></param>
        /// <param name="mapObjects"></param>
        /// <param name="objID"></param>
        /// <param name="pointHashStyles"></param>
        /// <param name="isCreated"></param>
        /// <param name="GPZUObject"></param>
        private static void Direct(IIngeoApplication aIngeoApp, IIngeoMapObject MapObj, IIngeoMapObjects mapObjects,
            string objID, string pointLayerName, string pointLayerLocalId, SpatialController<GisLayer> spatial)
        {
            double x;
            double y;
            double c;
            double xO;
            double yO;
            double xN;
            double yN;
            double Direct;
            double xV, xNum;
            double yV, yNum;
            bool Direction = true;
            string styleNameCoord = "";
            string styleIdCoord = "";
            string styleNamePoint = "Поворотная точка";
            string styleIdPoint = GlobalIDToLocalID(aIngeoApp, spatial.GetStyleIdByName(pointLayerName, styleNamePoint));
            string styleNameNumber = "Номер точки";
            string styleIdNumber = GlobalIDToLocalID(aIngeoApp, spatial.GetStyleIdByName(pointLayerName, styleNameNumber));
            string styleNameLine = "Соединение";
            string styleIdLine = GlobalIDToLocalID(aIngeoApp, spatial.GetStyleIdByName(pointLayerName, styleNameLine));
            IIngeoMapObject pointObject = null;
            IIngeoContourPart pointContourPart=null;
            double xs = 0;
            double ys = 0;
            int pointCount = 0;
            double[,] ArrayVertex;
            foreach (IIngeoShape shp in MapObj.Shapes)
            {
                if (shp.DefineGeometry)
                {
                    //Границы контура
                    //  shp.Contour.GetBounds(out DB, out LB, out UB, out RB);
                    foreach (IIngeoContourPart cnt in shp.Contour)
                    {
                        ArrayVertex = GeometryFunc.FillVertexArray(cnt);
                        //Получаем направление обхода контура)
                        if (GeometryFunc.ContourDirection(cnt) == 1)
                        {
                            Direction = false;
                        }
                        else Direction = true;
                        for (int i = 0; i < (ArrayVertex.Length / 2); i++)
                        {
                            pointCount++;
                            //см. описание функции
                            SetDirectPoints(ArrayVertex, i, out x, out y, out xO, out yO, out xN, out yN);
                            //Получаем угол биссектрисы внешнего угла (направоение выноски)
                            Direct = DirectionAngle(xO, yO, x, y, xN, yN, Direction);

                            //Считаем координаты точки через которую пройдет Выноска
                            ShiftPoint(Direct, x, y, 10, out xV, out yV);
                            //Считаем координаты точки для Номера
                            ShiftPoint(Direct, x, y, 2, out xNum, out yNum);
                            // Создаем пространственный объект
                            pointObject = mapObjects.AddObject(pointLayerLocalId);
                            pointContourPart =
                                pointObject.Shapes.Insert(0, styleIdPoint.ToString()).Contour.Insert(0);
                            pointContourPart.InsertVertex(-1, x, y, 0);
                            mapObjects.UpdateChanges();
                            IIngeoContourPart LineContourPart =
                                pointObject.Shapes.Insert(0, styleIdLine.ToString()).Contour.Insert(0);
                            LineContourPart.InsertVertex(-1, x, y, 0);
                            LineContourPart.InsertVertex(-1, xV, yV, 0); //Смещение точки выноски
                            if (Direct < Math.PI)
                                styleNameCoord = "Координаты точки вправо";
                            else styleNameCoord = "Координаты точки влево";
                            styleIdCoord = GlobalIDToLocalID(aIngeoApp, spatial.GetStyleIdByName(pointLayerName, styleNameCoord));
                            IIngeoContourPart OutContourPart =
                                pointObject.Shapes.Insert(0, styleIdCoord.ToString()).Contour.Insert(-1);
                            OutContourPart.InsertVertex(-1, xV, yV, 0); //Смещение точки подписи выноски

                            IIngeoContourPart NumberContourPart =
                                pointObject.Shapes.Insert(0, styleIdNumber.ToString()).Contour.Insert(0);
                            NumberContourPart.InsertVertex(-1, xNum, yNum, 0); //Смещение номера точки

                            pointObject.MapObjects.UpdateChanges();
                            xs = Math.Round(x, 2);
                            ys = Math.Round(y, 2);
                            // Записываем данные в семантику
                            pointObject.SemData.SetValue("Координаты точки", "Npoint", i + 1, 0);
                            pointObject.SemData.SetValue("Координаты точки", "X", xs, 0);
                            pointObject.SemData.SetValue("Координаты точки", "Y", ys, 0);
                            mapObjects.UpdateChanges();
                            //  ActivityWebApp.LogUtils.WriteTXT("// Добавляем топологическую связь");
                            // Добавляем топологическую связь
                            pointObject.TopoLinks.Clear();
                            pointObject.TopoLinks.Add(objID);
                            mapObjects.UpdateChanges();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Фу-ция вызова из массива координат Каждой точки с координатами До и После идущих точек
        /// </summary>
        /// <param name="ArrayVertex"></param>
        /// <param name="i"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="xO"></param>
        /// <param name="yO"></param>
        /// <param name="xN"></param>
        /// <param name="yN"></param>
        private static void SetDirectPoints(double[,] ArrayVertex, int i, out double x, out double y, out double xO, out double yO, out double xN, out double yN)
        {
            x = 0;
            y = 0;
            xO = 0;
            yO = 0;
            xN = 0;
            yN = 0;
            if (i != 0)
            {
                if (i != (ArrayVertex.Length / 2 - 1))
                {
                    xO = ArrayVertex[0, i - 1];
                    yO = ArrayVertex[1, i - 1];
                    xN = ArrayVertex[0, i + 1];
                    yN = ArrayVertex[1, i + 1];
                    x = ArrayVertex[0, i];
                    y = ArrayVertex[1, i];
                }
                else
                {
                    xO = ArrayVertex[0, i - 1];
                    yO = ArrayVertex[1, i - 1];
                    xN = ArrayVertex[0, 0];
                    yN = ArrayVertex[1, 0];
                    x = ArrayVertex[0, i];
                    y = ArrayVertex[1, i];
                }
            }
            else
            {
                xO = ArrayVertex[0, ArrayVertex.Length / 2 - 1];
                yO = ArrayVertex[1, ArrayVertex.Length / 2 - 1];
                xN = ArrayVertex[0, 1];
                yN = ArrayVertex[1, 1];
                x = ArrayVertex[0, i];
                y = ArrayVertex[1, i];
            }
        }
        /// <summary>
        /// Определение сдвига координат по заданному углу и на заданное расстояние
        /// </summary>
        /// <param name="Direct"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="B"></param>
        /// <param name="xV"></param>
        /// <param name="yV"></param>
        private static void ShiftPoint(double Direct, double x, double y, double B, out double xV, out double yV)
        {
            double V = Math.Abs(Math.Tan(Direct));
            double dx = B / Math.Sqrt(V * V + 1);
            double dy = dx * V;
            xV = x + B;
            yV = y + B;
            //Случаи осей
            if (Direct == Math.PI / 2)
            {
                yV = y + B;
                xV = x;
            }
            if (Direct == Math.PI)
            {
                yV = y;
                xV = x - B;
            }
            if (Direct == Math.PI * 3 / 2)
            {
                yV = y - B;
                xV = x;
            }
            if (Direct == Math.PI * 2 | Direct == 0)
            {
                yV = y;
                xV = x + B;
            }
            //Случаи между осями
            if (Direct < Math.PI / 2)
            {
                xV = x + dx;
                yV = y + dy;
            }
            if ((Math.PI / 2) < Direct & Direct < Math.PI)
            {
                xV = x - dx;
                yV = y + dy;
            }
            if (Math.PI < Direct & Direct < (Math.PI * 3 / 2))
            {
                xV = x - dx;
                yV = y - dy;
            }
            if ((Math.PI * 3 / 2) < Direct)
            {
                xV = x + dx;
                yV = y - dy;
            }
        }

        /// <summary>
        /// Определение дирекционного угла
        /// </summary>
        /// <param name="Xo"></param>
        /// <param name="Yo"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Xn"></param>
        /// <param name="Yn"></param>
        /// <param name="Direct"></param>
        /// <returns>Определение дирекционного угла</returns>
        private static double DirectionAngle(double Xo, double Yo, double X, double Y, double Xn, double Yn, bool Direct)
        {
            double Rumb1, Rumb2, dX1, dY1, dX2, dY2, Angle;
            double Result = Math.PI;
            dX1 = X - Xo;
            dX2 = Xn - X;
            dY1 = Y - Yo;
            dY2 = Yn - Y;
            //Угол входящей линнии
            if (dX1 != 0)
            {
                Rumb1 = Math.Atan(Math.Abs(dY1 / dX1));
            }
            else Rumb1 = Math.PI / 2;

            if ((dX1 < 0) & (dY1 >= 0)) //ЮВ
                Rumb1 = Math.PI - Rumb1;
            if ((dX1 < 0) & (dY1 < 0)) //ЮЗ
                Rumb1 = Math.PI + Rumb1;
            if ((dX1 >= 0) & (dY1 < 0)) //СЗ
                Rumb1 = Math.PI * 2 - Rumb1;
            //Угол выходящей линии
            if (dX2 != 0)
            {
                Rumb2 = Math.Atan(Math.Abs(dY2 / dX2));
            }
            else Rumb2 = Math.PI / 2;

            if ((dX2 < 0) & (dY2 >= 0)) //ЮВ
                Rumb2 = Math.PI - Rumb2;
            if ((dX2 < 0) & (dY2 < 0)) //ЮЗ
                Rumb2 = Math.PI + Rumb2;
            if ((dX2 >= 0) & (dY2 < 0)) //СЗ
                Rumb2 = Math.PI * 2 - Rumb2;

            if (Rumb1 < Math.PI)
            {
                if ((Rumb2 - Rumb1) > Math.PI)
                    Angle = (Rumb2 + Rumb1 + Math.PI) / 2;
                else Angle = (Rumb2 + Rumb1 + (3 * Math.PI)) / 2;
            }
            else
            {
                if ((Rumb1 - Rumb2) < Math.PI)
                    Angle = (Rumb2 + Rumb1 - Math.PI) / 2;
                else Angle = (Rumb2 + Rumb1 + Math.PI) / 2;
            }
            if (Angle > (Math.PI * 2))
                Angle = Angle % (Math.PI * 2);
            if (!Direct)
            {
                if (Angle < Math.PI) //Смена обхода контура
                    Angle = Angle + Math.PI;
                else Angle = Angle - Math.PI;
            }

            Result = Angle;

            //if (Angle < (Math.PI / 2))
            //    Result = "NE";
            //if ((Math.PI * 3 / 2) < Angle & Angle < (Math.PI * 2))
            //    Result = "NW";
            //if (Math.PI < Angle & Angle < (Math.PI * 3 / 2))
            //    Result = "SW";
            //if ((Math.PI / 2) < Angle & Angle < Math.PI)
            //    Result = "SE";
            return Result;

        }
    }
}
