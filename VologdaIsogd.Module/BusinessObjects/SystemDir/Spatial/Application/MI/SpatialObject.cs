using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class SpatialObject
    {
        MapInfoApplication mapinfo;
        string tableName;
        string objectID;
        // public string rowID;
        public bool isError;

        public TableInfo tableInfo
        {
            get; private set;
        }

        public SpatialObject(MapInfoApplication aMapInfoApplication, string aTableName, string aObjectID)
        {
            try
            {
                mapinfo = aMapInfoApplication;
                tableName = aTableName;
                objectID = aObjectID;
                GetTableInfo();
                //GetRowID(aObjectID);
            }
            catch (Exception ex) { isError = true; }
        }

        //public void GetRowID(string aObjectID)
        //{
        //    TableInfoTemp tempTable = new TableInfoTemp(mapinfo, ETempTableType.Query);
        //    //if (tableInfo.ColumnInfo("MainRowID")?.Name == null)
        //    // mapinfo.Do(string.Format("Add Column {0} (MainRowID) From {0} Set To RowID", tableInfo.Name));
        //    //else
        //    //{
        //    //    mapinfo.Do(string.Format("Alter Table {0} (Drop MainRowID)", tableInfo.Name));
        //    //    mapinfo.Do(string.Format("Add Column {0} (MainRowID Integer) From {0} Set To RowID Dynamic", tableInfo.Name));
        //    //}
        //    mapinfo.Do("Sub SelChangedHandler  ID = CommandInfo(CMD_INFO_ROWID) End Sub ");
        //    mapinfo.Select("*", tableName, "MI_PRINX", aObjectID, "Selection");
        //    mapinfo.Do("Fetch First From Selection");
        //    string temp = mapinfo.Eval("Selection.RowID");


        //    // = mapinfo.Eval(tempTable.Name + ".RowID");
        //    //mapinfo.SelectionTable()
        //    rowID = mapinfo.Eval(tempTable.Name + ".MainRowID");

        //    tempTable.CommitChanges();
        //}

        /// <summary>
        /// ���������� �� ���������������� ������
        /// </summary>
        public bool isExist
        {
            get
            {
                try
                {
                    using (TableInfoTemp tempTableInfo = new TableInfoTemp(mapinfo, ETempTableType.Query))
                    {
                        mapinfo.Select("*", tableName, "MI_PRINX", objectID, tempTableInfo.Name);
                        long rowCount = tempTableInfo.GetRowCount() ?? 0;
                        if (tempTableInfo.GetRowCount() > 0)
                            return true;
                        else
                            return false;
                    }
                }
                catch
                {
                    isError = true;
                    return false;
                }
            }
        }

        private void GetTableInfo()
        {
            tableInfo = new TableInfo(mapinfo, tableName);
        }

        /// <summary>
        /// ���������� �������� ����
        /// </summary>
        /// <param name="aFeeldName"></param>
        /// <param name="aValue"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        public void SetValue(string aFeeldName, string aValue, bool isCommit = true)
        {
            try
            {
                if (isCommit)
                {
                    mapinfo.Do(string.Format("Update {0} Set {1} =  \"{2}\" Commit table {0}", tableInfo.Name, aFeeldName, aValue));
                }
                else
                {
                    mapinfo.Do(string.Format("Update {0} Set {1} =  \"{2}\"", tableInfo.Name, aFeeldName, aValue));
                }
            }
            catch { throw new Exception(string.Format("�� ������� ���������� �������� ���� {0} � ���� {1}", aFeeldName, tableInfo.Name)); }
        }

        /// <summary>
        /// ���������� RowID ���������� ������� � ����� �������
        /// </summary>
        /// <param name="aDestinationTableName"></param>
        /// <param name="isCommit"></param>
        /// <param name="isCopyCopy"></param>
        /// <returns></returns>
        public string CloneSpatialObject(string aDestinationTableName, bool isCopyCopy = true)
        {
            TableInfo destinationTableInfo = new TableInfo(mapinfo, aDestinationTableName);
            TableInfoTemp sourceTempTable = new TableInfoTemp(mapinfo, ETempTableType.Query);
            mapinfo.Select("*", tableName, "MI_PRINX", objectID, sourceTempTable.Name);

            mapinfo.Do(string.Format("Insert Into {0} (obj) Select obj From {1}",
                aDestinationTableName, sourceTempTable.Name));
            destinationTableInfo.CommitChanges();

            //�������� RowID ������ �������
            mapinfo.Do("Fetch Last From " + aDestinationTableName);
            string newRowID = mapinfo.Eval(aDestinationTableName + ".RowID");

            //������� ������ �� ��������� �������
            TableInfoTemp tempTableInfo = new TableInfoTemp(mapinfo, ETempTableType.Query);
            mapinfo.Select("*", aDestinationTableName, "RowID", newRowID, tempTableInfo.Name);
            //�������� ������ ��� ������ - ���������� ���������������
            mapinfo.SetId(tempTableInfo.Name);

            if (isCopyCopy)
            {

                int? columnCount = tableInfo.GetColumnCount();
                if (columnCount != null)
                {
                    for (int i = 1; i <= columnCount; i++)
                    {
                        try
                        {
                            mapinfo.Do(string.Format("Fetch First From {0}", sourceTempTable.Name));
                            string value = mapinfo.Eval(string.Format("{0}.{1}", sourceTempTable.Name, tableInfo.ColumnInfo(i).Name));
                            mapinfo.Do(string.Format("Update {0} Set {1} =  {2} Where RowID = {3}",
                                destinationTableInfo.Name, tableInfo.ColumnInfo(i).Name, value, newRowID));
                            destinationTableInfo.CommitChanges();
                        }
                        catch (Exception ex) { }
                    }
                }
            }
            return newRowID;
        }
    }
}
