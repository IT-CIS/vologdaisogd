using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class SelectCommand
    {
        private MapInfoApplication mapinfo;
        public SelectCommand(MapInfoApplication aMapInfoApplication)
        {
            mapinfo = aMapInfoApplication;
        }

        //проработать операции = <> > < 
        public void Select(string aTableName, string aValue, string aIntoTableName = "Selection", string aWhere = "MI_PRINX", string aExpressionList = "*")
        {
            try
            {
                mapinfo.Do(string.Format("Select {4} From {0} Where {1} = {2} Into {3}", aTableName, aWhere, aValue, aIntoTableName, aExpressionList));
            }
            catch { }
        }
    }
}
