using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class TargetCommand
    {
        private MapInfoApplication mapinfo;
        public TargetCommand(MapInfoApplication aMapInfoApplication)
        {
            mapinfo = aMapInfoApplication;
        }
        public void SetTargetOn()
        {
            try
            {
                if (mapinfo.SelectionTable.GetCount() == 0)
                    return;
            }
            catch { }
            mapinfo.Do("Set Target On");
        }
        public void SetTargetOff()
        {
            mapinfo.Do("Set Target Off");
        }
    }
}
