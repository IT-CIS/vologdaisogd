using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class SelectionTable
    {
        private MapInfoApplication mapinfo = null;
        public readonly string Name = null;

        public SelectionTable(MapInfoApplication aMapInfoApplication)
        {
            try
            {
                mapinfo = aMapInfoApplication;
                Name = mapinfo.Eval("SelectionInfo(1)");
            }
            catch { }
        }
        public int? GetCount()
        {
            try
            {
                return Convert.ToInt32(mapinfo.Eval("SelectionInfo(3)"));
            }
            catch { return null; }
        }

        public string GetFirstID()
        {
            try
            {
                return mapinfo.Eval("Selection.MI_PRINX");
            }
            catch { return null; }
        }
        public void Close()
        {
            try
            {
                mapinfo.Do("Close Table Selection");
            }
            catch { }
        }

        public void CommitChanges()
        {
            try
            {
                mapinfo.Do("Commit table " + Name);
            }
            catch { }
        }

    }
}
