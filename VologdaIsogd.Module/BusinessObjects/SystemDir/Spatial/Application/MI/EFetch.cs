using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public enum EFetch
    {
        Rec,
        First,
        Last,
        Next,
        Prev
    }
}
