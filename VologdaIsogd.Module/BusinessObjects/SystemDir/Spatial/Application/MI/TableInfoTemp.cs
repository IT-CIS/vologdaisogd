using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class TableInfoTemp : TableInfo, IDisposable
    {
        /// <summary>
        /// ������������� ��� �������� �������
        /// </summary>
        /// <param name="eTempTableType"></param>
        /// <returns></returns>
        public TableInfoTemp(MapInfoApplication mapInfoApplication, ETempTableType eTempTableType) :
            base(mapInfoApplication, eTempTableType.ToString() + Guid.NewGuid().ToString().Replace("-", "").Remove(0, 6))
        { }

        // ����������. �������������� ����� ��������� ������, ���� �� ��������� ������
        ~TableInfoTemp()
        {
            try
            {
                mapinfo.Do(string.Format("Drop Table {0}", Name));
            }
            catch { }
        }
    }
}
