using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class ColumnInfo
    {
        private MapInfoApplication mapinfo;
        private string tableName;
        public string Name { get; private set; }
        public int? Number { get; private set; }

        public ColumnInfo(MapInfoApplication aMapInfoApplication, string aTableName, string aColumnName)
        {
            mapinfo = aMapInfoApplication;
            tableName = aTableName;
            Name = aColumnName;
            SetNumber();
        }
        public ColumnInfo(MapInfoApplication aMapInfoApplication, string aTableName, int aColumnNumber)
        {
            mapinfo = aMapInfoApplication;
            tableName = aTableName;
            Number = aColumnNumber;
            SetName();
        }

        private void SetNumber()
        {
            try
            {
                // 2 - COL_INFO_NUM
                Number = Convert.ToInt16(mapinfo.Eval(string.Format("ColumnInfo(\"{0}\", \"col{1}\", 2)", tableName, Name)));
            }
            catch { Number = null; }
        }
        private void SetName()
        {
            try
            {
                // 1 - COL_INFO_NAME
                Name = mapinfo.Eval(string.Format("ColumnInfo(\"{0}\", \"col{1}\", 1)", tableName, Number));
            }
            catch { Number = null; }
        }
    }
}
