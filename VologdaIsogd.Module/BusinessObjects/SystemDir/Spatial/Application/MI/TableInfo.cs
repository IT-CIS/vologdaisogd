using System;
using System.Collections.Generic;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    public class TableInfo : IDisposable
    {
        protected MapInfoApplication mapinfo;
        public string Name { get; private set; }
        public bool isError { get; private set; }

        public TableInfo(MapInfoApplication mapInfoApplication, string aTableName)
        {
            mapinfo = mapInfoApplication;
            Name = aTableName;
        }

        public TableInfo(MapInfoApplication mapInfoApplication, int aNumber)
        {
            mapinfo = mapInfoApplication;
            SetName(aNumber);
        }

        private void SetName(int aNumber)
        {
            try
            {
                Name = mapinfo.Eval((string.Format("TableInfo({0},  1)", aNumber)));
            }
            catch { isError = true; }
        }

        public ColumnInfo ColumnInfo(string aColumnName)
        {
            try
            {
                return new ColumnInfo(mapinfo, Name, aColumnName);
            }
            catch { return null; }
        }
        public ColumnInfo ColumnInfo(int aColumnNumber)
        {
            try
            {
                string columnName = mapinfo.Eval(string.Format("ColumnInfo(\"{0}\", \"col{1}\", 1)", Name, aColumnNumber));
                return new ColumnInfo(mapinfo, Name, aColumnNumber);
            }
            catch { return null; }
        }


        public int? GetColumnCount()
        {
            //TAB_INFO_NCOLS - 4 - ����� ����� (��� SmallInt), ���������� ������� � �������.
            try
            {
                return Convert.ToInt16(mapinfo.Eval((string.Format("TableInfo(\"{0}\", 4)", Name))));
            }
            catch { return null; }
        }

        public Int64? GetRowCount()
        {
            //TAB_INFO_NROWS -  8 - ����� ����� (��� Integer), ���������� ����� � �������.
            try
            {
                return Convert.ToInt16(mapinfo.Eval((string.Format("TableInfo(\"{0}\",  8)", Name))));
            }
            catch { return null; }
        }

        public void Dispose()
        {
            try
            {
                mapinfo.Do(string.Format("Drop Table {0}", Name));
            }
            catch { }
        }

        public void CommitChanges()
        {
            try
            {
                mapinfo.Do("Commit table " + Name);
            }
            catch { throw new Exception(string.Format("�� ������� ��������� ��������� � ���� \"{0}\"", Name)); }
        }

        public bool isTableExist
        {
            get
            {
                int NumTables = Convert.ToInt16(mapinfo.Eval("NumTables()"));
                List<string> TablesList = new List<string>();
                for (int k = 1; k <= NumTables; k++)
                {
                    TableInfo openedTable = new TableInfo(mapinfo, k);
                    if (openedTable.Name.IndexOf(Name) != -1)
                        return true;
                }
                isError = true;
                throw new Exception(string.Format("�� ������ ���� {0}", Name));
            }
        }
    }
}
