﻿using AISOGD.SystemDir;
using DevExpress.ExpressApp.Utils;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using VologdaIsogd.Module.BusinessObjects.SystemDir;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI
{
    /// <summary>
    /// Класс содержит набор методов и функций для работы с пространственными объектами
    /// </summary>
    public class MapInfoApplication : GISApplication
    {
        private Type mapinfoType;
        public object mapinfo;

        public TableInfo TableInfo(string aTableName)
        {
            return new TableInfo(this, aTableName);
        }

        public SelectionTable SelectionTable
        {
            get { return new SelectionTable(this); }
        }

        public SpatialObject SpatialObject(string aSpatialLayerID, string aSpatialObjectID)
        {
            return new SpatialObject(this, aSpatialLayerID, aSpatialObjectID);
        }

        /// <summary>
        /// Захватить объект для выполнения пространственных операций
        /// </summary>
        public TargetCommand Target
        {
            get { return new TargetCommand(this); }
        }

        public MapInfoApplication()
        {
            string defaultProgID = "MapInfo.Application";
            mapinfoType = Type.GetTypeFromProgID(defaultProgID);
            try
            {
                mapinfo = Marshal.GetActiveObject(defaultProgID);
            }
            catch { }


            int versionCount = 89; //начинаем с версии 9.0 
            if (mapinfoType == null)
            {
                while (versionCount < 200 && mapinfoType == null && mapinfo == null)
                {
                    versionCount++;
                    mapinfoType = Type.GetTypeFromProgID($"{defaultProgID}.{ versionCount * 10}");
                    if (mapinfoType != null)
                        defaultProgID = $"{defaultProgID}.{versionCount * 10}";
                    else
                    {
                        if (mapinfoType == null)
                        {
                            mapinfoType = Type.GetTypeFromProgID($"MapInfo.Applicationx64.{versionCount * 10}");
                        }
                        if (mapinfoType != null)
                            defaultProgID = $"MapInfo.Applicationx64.{versionCount * 10}";
                    }
                }
                if (mapinfoType != null)
                {
                    try
                    {
                        mapinfo = Marshal.GetActiveObject(defaultProgID);
                    }
                    catch { }
                }
            }
        }

        public string Eval(string command)
        {
            try
            {
                return (string)mapinfoType.InvokeMember
                    ("Eval", System.Reflection.BindingFlags.InvokeMethod, null, mapinfo, new object[] { command });
            }
            catch { return null; }
        }

        public string Do(string command)
        {
            //try
            //{
            return (string)mapinfoType.InvokeMember
                ("Do", System.Reflection.BindingFlags.InvokeMethod, null, mapinfo, new object[] { command });
            //}
            //catch { return null; }
        }

        /// <summary>
        /// Разрезать захваченные объекты выделенными
        /// </summary>
        /// <param name="isSaveParentData">Сохранить значения полей в полученных объектах</param>
        public void SplitObjects(bool isSaveParentData = true)
        {
            try
            {
                if (isSaveParentData)
                {
                    //!! Проверить на реальность колонки
                    string dataCopyArgs = "";
                    string tableName = SelectionTable.Name;
                    int? columnCount = TableInfo(tableName).GetColumnCount();
                    if (columnCount != null)
                    {
                        for (int i = 1; i <= columnCount; i++)
                        {
                            string columnName = TableInfo(tableName).ColumnInfo(i).Name;
                            if (columnName == "MI_PRINX")
                                continue;
                            if (dataCopyArgs == "")
                            {
                                dataCopyArgs = string.Format("{0} = {0}", columnName);
                            }
                            else
                            {
                                dataCopyArgs += string.Format(", {0} = {0}", columnName);
                            }
                        }
                        Do("Objects Split Into Target Data " + dataCopyArgs);
                    }
                }
                else
                {
                    Do("Objects Split Into Target");
                }
                //Заглушка только для дебага - Присвоение идентификаторов при разрезании объектов
                SetId();
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Присвоить идентификаторы выделенным объектам и сохранить изменения
        /// </summary>
        public void SetId(string aTableName = "Selection")
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                try
                {
                    TableInfo selectionTableInfo = new TableInfo(this, "Selection");
                    for (int i = 1; i <= selectionTableInfo.GetRowCount(); i++)
                    {
                        Fetch(EFetch.Rec, selectionTableInfo.Name, i);
                        Random rnd1 = new Random();
                        if (string.IsNullOrEmpty(Eval(string.Format("{0}.MI_PRINX", selectionTableInfo.Name))) ||
                            Eval($"{selectionTableInfo.Name}.MI_PRINX") == "0")
                        {
                            Do($"Update Selection Set MI_PRINX =  \"{rnd1.Next(1000000, 100000000)}\"  Where RowID = {i} Commit table {SelectionTable.Name}");
                        }
                    }
                    this.SelectionTable.CommitChanges();
                }
                catch { }
            }

        }

        /// <summary>
        /// Установить курсор в таблице
        /// </summary>
        /// <param name="eFetch"></param>
        /// <param name="aTableName"></param>
        /// <param name="aRecNo"></param>
        public void Fetch(EFetch eFetch, string aTableName = "Selection", int aRecNo = 0)
        {
            try
            {
                if (eFetch == EFetch.Rec)
                    Do(string.Format("Fetch Rec {0} From {1}", aRecNo, aTableName));
            }
            catch { }
        }

        public void Select(string aExpressionList, string aTableName, string aWhere, string aValue, string aIntoTableName)// = "Selection")
        {
            try
            {
                Do($"Select {aExpressionList} From {aTableName} Where {aWhere} = {aValue} Into {aIntoTableName}");
            }
            catch (Exception ex) { }
        }

        public override GISApplicationType GetApplicationType()
        {
            return GISApplicationType.MI;
        }

        public override List<string> SetLink(string aClassName, string aDataObjectID, bool isSystem = false)
        {
            List<string> resultList = new List<string>();
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);
            // получаем количество выделенных объектов МапИнфо
            int selObjCount = ((IGIS.IGISApplication)this).CountSelectedObject();
            string layer = ((IGIS.IGISApplication)this).GetActiveLayerName();

            DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
            var xtraMessageBoxForm = new XtraMessageBoxForm();
            // Если выделено больше одного пространственного объекта на карте - выводим сообщение об этом
            if (selObjCount > 1)
            {
                if (
                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                        String.Format("Выделено {0} объектов таблицы {1}. Продолжить?",
                            selObjCount.ToString(), layer),
                        "Внимание!", dialogResults, null, 0)) == DialogResult.No)
                { return resultList; }
            }

            // если у реестрового объекта нет связи с пространственными
            if (!spatial.IsObjectLinked(aClassName, aDataObjectID, GISApplicationType.MI))
            {
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    Do($"Fetch Rec {i} From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                            {
                                xtraMessageBoxForm = new XtraMessageBoxForm();
                                if (
                                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                        $"Данный ГИС объект [MI_PRINX: {gisObjId}] уже связан с реестровым. Пересвязать?",
                                        "Данный объект уже связан", dialogResults, null, 0)) == DialogResult.Yes)
                                {
                                    //удаляем старую связь
                                    spatial.DeleteSpatialGISLinks(layer, gisObjId);

                                    spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI);
                                    // FillSemanticFromReestr(isogdClass, connect, id, layer);
                                }
                            }
                            else
                            {
                                spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI);
                                // FillSemanticFromReestr(isogdClass, connect, id, layer);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Перед связкой необходимо сохранить пространственные данные");
                            return resultList;
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show($"InnerException: { ex.InnerException}; Message: { ex.Message}; TargetSite: {ex.TargetSite}");
                    }
                }
            }
            //если есть, сообщаем об этом, и спаршиваем, связать еще с этими или перепривязать
            // если связываем - перебираем выделенные и проверяем - может реестровый уже связан с одним из выделенных
            else
            {
                //DialogResult[] dialogResult = { DialogResult.OK, DialogResult.Yes, DialogResult.Cancel };
                var form = new VologdaIsogd.Controllers.DialogForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    label1 = { Text = $"Данный реестровый объект [{CaptionHelper.GetClassCaption(aClassName)}] уже связан с пространственными. Нажмите на кнопку " +
                        " 'ОК' если хотите пересвязать, на кнопку 'Добавить', если хотите добавить связи, на кнопку 'Отмена' для отмены" }
                };
                form.ShowDialog();

                var result = form.DialogResult;
                if (result == DialogResult.Cancel)
                {
                    return resultList;
                }

                if (result == DialogResult.OK)
                {
                    spatial.DeleteSpatialLinks(aClassName, aDataObjectID);
                    connect.GetUnitOfWork().CommitChanges();
                    for (int i = 1; i < selObjCount + 1; i++)
                    {
                        Do($"Fetch Rec {i} From Selection");
                        try
                        {
                            // Id (MI_PRINX) выделенного объекта мапинфо
                            string gisObjId = Eval("Selection.MI_PRINX");
                            if (gisObjId != "0")
                            {
                                if (!spatial.IsObjectLinkExist(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI))
                                {
                                    if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                                    {
                                        xtraMessageBoxForm = new XtraMessageBoxForm();
                                        if (
                                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                                $"Данный ГИС объект [MI_PRINX: {gisObjId}] уже связан с реестровым. Пересвязать?",
                                                "Данный объект уже связан", dialogResults, null, 0)) == DialogResult.Yes)
                                        {

                                            //удаляем старую связь для ГИС объекта
                                            try
                                            {
                                                spatial.DeleteSpatialGISLinks(layer, gisObjId);
                                            }
                                            catch { }

                                            //удаляем старую связь для реестрового объекта
                                            try
                                            {
                                                spatial.DeleteSpatialLinks(aClassName, aDataObjectID);
                                            }
                                            catch { }

                                            spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI);
                                            resultList.Add(gisObjId);
                                            //FillSemanticFromReestr(isogdClass, connect, id, layer);
                                        }
                                    }
                                    else
                                    {
                                        //удаляем старую связь для реестрового объекта
                                        try
                                        {
                                            spatial.DeleteSpatialLinks(aClassName, aDataObjectID);
                                        }
                                        catch { }
                                        spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI);
                                        resultList.Add(gisObjId);
                                        // FillSemanticFromReestr(isogdClass, connect, id, layer);
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Перед связкой необходимо сохранить пространственные данные");
                                return resultList;
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show($"InnerException: { ex.InnerException}; Message: { ex.Message}; TargetSite: {ex.TargetSite}");
                        }
                    }
                }
                if (result == DialogResult.Ignore)
                {
                    for (int i = 1; i < selObjCount + 1; i++)
                    {
                        Do($"Fetch Rec {i} From Selection");
                        // Id (MI_PRINX) выделенного объекта мапинфо
                        string gisObjId = Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            if (!spatial.IsObjectLinkExist(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI))
                            {
                                if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                                {
                                    xtraMessageBoxForm = new XtraMessageBoxForm();
                                    if (
                                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                            $"Данный ГИС объект [MI_PRINX: {gisObjId}] уже связан с реестровым. Пересвязать?",
                                            "Данный объект уже связан", dialogResults, null, 0)) == DialogResult.Yes)
                                    {
                                        spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI);
                                        resultList.Add(gisObjId);
                                        //FillSemanticFromReestr(isogdClass, connect, id, layer);
                                    }
                                }
                                else
                                {
                                    spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.MI);
                                    resultList.Add(gisObjId);
                                    //FillSemanticFromReestr(isogdClass, connect, id, layer);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Перед связкой необходимо сохранить пространственные данные");
                            return resultList;
                        }
                    }
                }
                XtraMessageBox.Show("Привязка завершена", "Инфо");
            }


            #region /// вариант связи через таблицы МапИнфо - связи записываются в поля (XafId, XafClass) таблиц
            //// 4. Проверяем, связан ли текущий реестровый объект с каким нибудь пространственным
            //bool isOk = false;
            ////XtraMessageBox.Show("Связан ли текущий реестровый объект с каким нибудь пространственным: " + spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()).ToString());
            //if (!spatial.isMapInfoObjectLinked(m_MapInfo, selectedLayerName, isogdClass.FullName, id.ToString()))
            //{
            //    // реестровый не связан
            //    // записываем в пространственный объект данные о связи
            //    isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, selectedLayerName, isogdClass.FullName, id.ToString());
            //    // записываем информацию, что связали
            //    string gisObjId = "1";
            //    try
            //    {
            //        Do("Select AisogdGUID from Selection Into tmpTable");
            //        //    selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
            //        //    for (int i = 1; i < selObjCount + 1; i++)
            //        //    {
            //        Do("Fetch Rec first From tmpTable");
            //        // Id реестрового объекта Документа ИСОГД
            //        gisObjId = Eval("tmpTable.Col1");
            //    }
            //    catch { }
            //    //spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, gisObjId);
            //}
            //else
            //{
            //    // реестровый уже связан с одним из пространственных
            //    //var xtraMessageBoxForm = new XtraMessageBoxForm();
            //    //if (
            //    //    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
            //    //        String.Format("Данный реестровый объект [{0}] уже связан с пространственным. Пересвязать?",
            //    //            CaptionHelper.GetClassCaption(isogdClass.FullName)),
            //    //        "Данный объект уже связан", dialogResults, null, 0)) == DialogResult.Yes)
            //    //{
            //    //    spatial.DelLinkMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //    //    isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //    //    // записываем в SpatialRepository информацию, что связали
            //    //    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
            //    //}

            //    // спаршиваем, отменить привязку, связать еще с этим или перепривязать
            //    var form = new DialogForm
            //    {
            //        StartPosition = FormStartPosition.CenterParent,
            //        label1 =
            //            {
            //                Text = String.Format(
            //                    "Данный реестровый объект [{0}] уже связан с пространственным. Нажмите на кнопку " +
            //                    " 'ОК' если хотите пересвязать, на кнопку 'Добавить', если хотите добавить связь, на кнопку 'Отмена' для отмены",
            //                    CaptionHelper.GetClassCaption(isogdClass.FullName))
            //            }
            //    };
            //    form.ShowDialog();
            //    XtraMessageBoxForm xtraMessageBoxForm;
            //    var result = form.DialogResult;
            //    // Отмена
            //    if (result == DialogResult.Cancel)
            //    {
            //        return "";
            //    }
            //    // Пересвязать
            //    if (result == DialogResult.OK)
            //    {
            //        spatial.DelLinkMapInfoObject(m_MapInfo, selectedLayerName, isogdClass.FullName, id.ToString());
            //        isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, selectedLayerName, isogdClass.FullName, id.ToString());
            //        // записываем в SpatialRepository информацию, что связали
            //        //spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
            //    }
            //    // Добавить связь
            //    if (result == DialogResult.Ignore)
            //    {
            //        isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, selectedLayerName, isogdClass.FullName, id.ToString());
            //        // записываем в SpatialRepository информацию, что связали
            //        //spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
            //    }
            //}
            //if (isOk)
            //    XtraMessageBox.Show("Привязка завершена", "Инфо");
            //else
            //    XtraMessageBox.Show("Привязка не удалась", "Инфо");
            #endregion
            return resultList;
        }

        //bool IGISApplication.ZoomLinkedObject(string aClassName, string aDataObjectID, string aLayerName)
        //{
        //    throw new NotImplementedException();
        //}

        public override string GetActiveLayerName()
        {
            // 2. Получаем таблицу (слой) выделения
            return Eval("SelectionInfo(1)");
        }

        public override bool isReady()
        {
            return (mapinfo != null);
        }

        public override int CountSelectedObject()
        {
            return Convert.ToInt32(Eval("SelectionInfo(3)"));
        }

        public override string ZoomLinkedObject(string aObjectID, string aLayerName)
        {
            //m_MapInfo.Do("Select * From \"Документ_ИСОГД\" Where MI_PRINX = 1 Into ZoomObject");
            Do($"Select * From {aLayerName} Where MI_PRINX = {aObjectID} Into ZoomObject");
            //m_MapInfo.Do("Select * From Isogddoc Where MI_PRINX = 7792 Into ZoomObject");
            Do("Add Map Layer ZoomObject Fetch first From ZoomObject Set Map Center (CentroidX(ZoomObject.obj),CentroidY(ZoomObject.obj)) " +
            "Add Map Layer ZoomObject Set Map Zoom Entire Layer ZoomObject");
            Do("Drop Table ZoomObject");
            return "True";

            /////вариант связи через таблицы МапИнфо - связи записываются в поля (XafId, XafClass) таблиц
            //// Из Конфига получаем список слоев, в которых будем искать
            //foreach (string layer in spatial.GetListLinkedSpatialLayers(isogdClass.FullName))
            //{
            //    if (spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()))
            //    {
            //        isExits = true;
            //        spatial.GetLinkedMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //        break;
            //    }
            //}
        }

        public override void Activate()
        {
            System.Diagnostics.Process[] p = System.Diagnostics.Process.GetProcessesByName("MapInfow");
            if (p.Length > 0)
            {
                ShowWindow(p[0].MainWindowHandle, 10);
                ShowWindow(p[0].MainWindowHandle, 5);
                SetForegroundWindow(p[0].MainWindowHandle);
            }
        }

        public override List<string> GetSelectedObjectIds()
        {
            List<string> resultList = new List<string>();
            // получаем количество выделенных объектов МапИнфо
            int selObjCount = ((IGIS.IGISApplication)this).CountSelectedObject();
            string layer = ((IGIS.IGISApplication)this).GetActiveLayerName();

            for (int i = 1; i < selObjCount + 1; i++)
            {
                Do($"Fetch Rec {i} From Selection");
                // Id (MI_PRINX) выделенного объекта мапинфо
                string gisObjId = Eval("Selection.MI_PRINX");
                if (gisObjId != "0")
                {
                    resultList.Add(gisObjId);
                }
                else
                {
                    XtraMessageBox.Show("Перед связкой необходимо сохранить пространственные данные. Выделенный объект не сохранен");
                    return resultList;
                }
            }
            return resultList;
        }

        public override bool SelectMapObject(string layerName, string mapObjId)
        {
            //выделить пространственный объект в таблицу zObject
            try
            {
                Do($"Select * From {layerName} Where MI_PRINX = {mapObjId}");
                return true;
            }
            catch (Exception ex)
            {
                MessageHelper.FullErrorMessage(ex);
                return false;
            }
        }

        public override List<string> GetCrossingMapObjects(string sourceLayerName, string sourceMapObjId, string targerLayer)
        {
            List<string> result = new List<string>();
            //--------------------- GISApp.SelectMapObject(
            // получаем наш пространственный объект заявки
            try
            {
                Do($"Select * From {sourceLayerName} Where MI_PRINX = {sourceMapObjId} Into zObject");
            }
            catch (Exception ex)
            {
                MessageHelper.FullErrorMessage(ex);
                return result;
            }
            //-----------------------

            //Partly Within - пересечение. Select ... "objectA Partly Within objectB" часть первого объекта помещается внутри второго"
            Do($"Select MI_PRINX from {targerLayer} Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
            int selObjCount = Convert.ToInt16(Eval("SelectionInfo(3)"));
            for (int i = 1; i < selObjCount + 1; i++)
            {
                Do($"Fetch Rec {i} From tmpTable");
                // Id(MI_PRINX) пространственного объекта Документа ИСОГД
                string ReqestObjID = Eval("tmpTable.Col1");
                //XtraMessageBox.Show("ReqestObjID: " + ReqestObjID + ", selObjCount: " + selObjCount.ToString());
            }
            return result;
        }

        public override void SetFieldValue(string layerName, string mapObjectId, string fieldName, string fieldValue)
        {
            try
            {
                Do($"Select * From {layerName} Where MI_PRINX = {mapObjectId} Into zObject");
                Do($"Fetch Rec 0 From zObject");
                Do($"Update Selection Set {fieldName} = \"{fieldValue}\" Commit table {layerName}");
                Do($"Close Table zObject");
                Do($"Close Table Selection");
            }
            catch { }
        }

        public override string GetFieldValue(string layerName, string mapObjectId, string fieldName)
        {
            throw new NotImplementedException();
        }

        public override void CommitLayer(string layerName)
        {
            Do($"Commit table {layerName}");
        }
    }

}