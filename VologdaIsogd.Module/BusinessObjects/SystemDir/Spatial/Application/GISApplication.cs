using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.QGIS;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application
{

    public abstract class GISApplication : IGISApplication
    {
        [DllImport("user32.dll")]
        protected static extern bool ShowWindow(IntPtr handle, int cmdShow);
        [DllImport("user32.dll")]
        protected static extern int SetForegroundWindow(IntPtr handle);

        [DllImport("oleaut32.dll", PreserveSig = false)]
        [ComVisible(true)]
        private static extern void GetActiveObject(ref Guid rclsid, IntPtr reserved, [MarshalAs(UnmanagedType.Interface)] out object ppunk);

        static IGISApplication IGISApplication = null;
        public static IGISApplication GetGISApplication()
        {
            if (IGISApplication == null)
            {
                QGISApplication qgis = new QGIS.QGISApplication();
                if (((IGISApplication)qgis).isReady())
                {
                    IGISApplication = qgis;
                }
                else
                {
                    MapInfoApplication MI = new MI.MapInfoApplication();
                    if (((IGISApplication)MI).isReady())
                    {
                        IGISApplication = MI;
                    }
                }
            }
            return IGISApplication;
        }

        public abstract void Activate();
        public abstract int CountSelectedObject();
        public abstract string GetActiveLayerName();
        public abstract GISApplicationType GetApplicationType();
        public abstract List<string> GetSelectedObjectIds();
        public abstract bool isReady();
        public abstract List<string> SetLink(string aClassName, string aDataObjectID, bool isSystem = false);
        public abstract string ZoomLinkedObject(string aMapObjectID, string aLayerName);
        public abstract bool SelectMapObject(string layerName, string mapObjId);
        public abstract List<string> GetCrossingMapObjects(string sourceLayerName, string sourceMapObjId, string targerLayer);
        public abstract void SetFieldValue(string layerName, string mapObjectId, string fieldName, string fieldValue);
        public abstract string GetFieldValue(string layerName, string mapObjectId, string fieldName);
        public abstract void CommitLayer(string layerName);
    }
}
