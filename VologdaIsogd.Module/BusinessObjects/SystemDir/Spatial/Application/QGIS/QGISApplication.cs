using AISOGD.SystemDir;
using DevExpress.ExpressApp.Utils;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using VologdaIsogd.Controllers;
using VologdaIsogd.Module.BusinessObjects.SystemDir;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.QGIS
{
    public class QGISApplication : GISApplication
    {
        public override GISApplicationType GetApplicationType()
        {
            return GISApplicationType.QGIS;
        }


        public override List<string> SetLink(string aClassName, string aDataObjectID, bool isSystem = false)
        {
            List<string> linkedObjIds = new List<string>();
            List<string> selObjIds = GetSelectedObjectIds();
            if (selObjIds.Count() == 0)
            {
                XtraMessageBox.Show("��� ��������� ��������");
                return linkedObjIds;
            }
            IGISApplication gisApp = ((IGISApplication)this);
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);
            // �������� ���������� ���������� �������� 
            int selObjCount = gisApp.CountSelectedObject();
            string layer = gisApp.GetActiveLayerName();

            #region ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository 
            DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
            // ���� � ����������� ������� ��� ����� � �����������������
            if (!spatial.IsObjectLinked(aClassName, aDataObjectID, GISApplicationType.QGIS))
            {
                for (int i = 0; i < selObjCount; i++)
                {
                    try
                    {
                        string gisObjId = selObjIds[i];
                        if (gisObjId != "0")
                        {
                            gisObjId = gisObjId.Replace(".0", "");
                            if (spatial.IsSpatialObjectLinkExist(layer, gisObjId, GISApplicationType.QGIS))
                            {
                                var xtraMessageBoxForm = new XtraMessageBoxForm();
                                if (
                                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                        $"������ ��� ������ ID: {gisObjId} ��� ������ � ����������. �����������?",
                                        "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                                {
                                    spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS);
                                }
                            }
                            else
                                spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS);
                        }
                        else
                        {
                            XtraMessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
                            return linkedObjIds;
                        }
                    }
                    catch (Exception ex)
                    {
                       
                    }
                }
            }
            //���� ����, �������� �� ����, � ����������, ������� ��� � ����� ��� �������������
            //     ���� ��������� -���������� ���������� � ��������� - ����� ���������� ��� ������ � ����� �� ����������
            else
            {
                //DialogResult[] dialogResult = { DialogResult.OK, DialogResult.Yes, DialogResult.Cancel };
                var form = new DialogForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    label1 = { Text = $"������ ���������� ������ [{CaptionHelper.GetClassCaption(aClassName)}] ��� ������ � �����������������. ������� �� ������ " +
                            " '��' ���� ������ �����������, �� ������ '��������', ���� ������ �������� �����, �� ������ '������' ��� ������" }
                };
                form.ShowDialog();

                XtraMessageBoxForm xtraMessageBoxForm;
                var result = form.DialogResult;
                if (result == DialogResult.Cancel)
                {
                    return linkedObjIds;
                }

                if (result == DialogResult.OK)
                {
                    spatial.DeleteSpatialLinks(aClassName, aDataObjectID);
                    connect.GetUnitOfWork().CommitChanges();
                    for (int i = 0; i < selObjCount; i++)
                    {
                       
                        try
                        {
                            string gisObjId = selObjIds[i];
                            if (gisObjId != "0")
                            {
                                gisObjId = gisObjId.Replace(".0", "");
                                if (!spatial.IsObjectLinkExist(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS))
                                {
                                    xtraMessageBoxForm = new XtraMessageBoxForm();
                                    if (
                                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                            $"������ ��� ������ ID: {gisObjId}] ��� ������ � ����������. �����������?",
                                            "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                                    {
                                        spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS);
                                        if (!isSystem)
                                            XtraMessageBox.Show("�������� ���������", "����");
                                    }


                                }
                                else
                                {
                                    spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS);
                                    if (!isSystem)
                                        XtraMessageBox.Show("�������� ���������", "����");
                                }
                            }
                            else
                            {
                                if (!isSystem)
                                    MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
                                return linkedObjIds;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageHelper.FullErrorMessage(ex);
                        }
                    }
                }
                if (result == DialogResult.Ignore)
                {
                    for (int i = 0; i < selObjCount; i++)
                    {
                        string gisObjId = selObjIds[i];
                        if (gisObjId != "0")
                        {
                            gisObjId = gisObjId.Replace(".0", "");
                            if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                            {
                                xtraMessageBoxForm = new XtraMessageBoxForm();
                                if (
                                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                        $"������ ��� ������ {gisObjId} ��� ������ � ����������. �����������?",
                                        "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                                {
                                    spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS);
                                    if (!isSystem)
                                        XtraMessageBox.Show("�������� ���������", "����");
                                }


                            }
                            else
                            {
                                spatial.AddSpatialLink(aClassName, aDataObjectID, layer, gisObjId, GISApplicationType.QGIS);
                                if (!isSystem)
                                    XtraMessageBox.Show("�������� ���������", "����");
                            }
                        }
                        else
                        {
                            if (!isSystem)
                                MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
                            return linkedObjIds;
                        }
                    }
                }
            }
            #endregion
            // return SendMessageFromSocket(string.Format("SetLink&{0}&{1}", aClassName, aDataObjectID));
            return linkedObjIds;
        }

        public override string ZoomLinkedObject(string aObjectID, string aLayerName)
        {
            string result = "";
            try
            {
                result = SendMessageFromSocket($"ZoomLinkedObject&MI_PRINX&{aObjectID}&{aLayerName}");
            }
            catch { }
            return result;
        }

        public override string GetActiveLayerName()
        {
            return SendMessageFromSocket("GetActiveLayerName");
        }

        public override bool isReady()
        {
            bool result = false;
            try
            {
                result = Convert.ToBoolean(SendMessageFromSocket("isReady"));
            }
            catch { }
            return result;
        }

        public override int CountSelectedObject()
        {
            int result = 0;
            try
            {
                result = Convert.ToInt16(SendMessageFromSocket("CountSelectedObject"));
            }
            catch { }
            return result;
        }


        public override void Activate()
        {
            System.Diagnostics.Process[] p = System.Diagnostics.Process.GetProcessesByName("qgis-ltr-bin");
            if (p.Length > 0)
            {
                ShowWindow(p[0].MainWindowHandle, 10);
                ShowWindow(p[0].MainWindowHandle, 5);
                SetForegroundWindow(p[0].MainWindowHandle);
            }
        }

        public override List<string> GetSelectedObjectIds()
        {
            return SendMessageFromSocket("SelectedFeaturesIds").Split('&').ToList();
        }

        static string SendMessageFromSocket(string aMessage, int aPort = 52369)
        {
            string result = "";
            // ����� ��� �������� ������
            byte[] bytes = new byte[1024];
            Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                // ��������� ����� 
                sender.Connect("localhost", aPort);

                byte[] msg = Encoding.UTF8.GetBytes(aMessage);

                // ���������� ������ ����� �����
                int bytesSent = sender.Send(msg);

                // �������� ����� �� �������
                int bytesRec = sender.Receive(bytes);
                result = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            }
            catch { }
            finally
            {
                // ����������� �����
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            return result;
        }

        public override bool SelectMapObject(string layerName, string mapObjId)
        {
            bool result = true;
            try
            {
                SendMessageFromSocket($"SelectMapObject&MI_PRINX&{mapObjId}&{layerName}");
            }
            catch { result = false; }
            return result;
        }

        public override List<string> GetCrossingMapObjects(string sourceLayerName, string sourceMapObjId, string targerLayer)
        {
            List<string> result = new List<string>();
            try
            {
                result = SendMessageFromSocket($"GetCrossingMapObjects&{sourceLayerName}&{sourceMapObjId}&{targerLayer}").Split('&').ToList();
            }
            catch{ }
            return result;
        }

        public override void SetFieldValue(string layerName, string mapObjectId, string fieldName, string fieldValue)
        {
            try
            {
                SendMessageFromSocket($"SetFieldValue&{layerName}&{mapObjectId}&{fieldName}&{fieldValue}");
            }
            catch { }
        }

        public override string GetFieldValue(string layerName, string mapObjectId, string fieldName)
        {
            string result = "";
            try
            {
                result = SendMessageFromSocket($"GetFieldValue&{layerName}&{mapObjectId}&{fieldName}");
                if (result.IndexOf("True") != -1)
                {
                    result = result.Replace("True&", "");
                }
                else
                    result = "";
            }
            catch { }
            return result;
        }

        public override void CommitLayer(string layerName)
        {
            try
            {
                SendMessageFromSocket($"CommitLayer&{layerName}");
            }
            catch { }
        }
    }
}
