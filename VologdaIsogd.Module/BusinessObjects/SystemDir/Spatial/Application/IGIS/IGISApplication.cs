using System;
using System.Collections.Generic;
using System.Linq;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS
{
    public interface IGISApplication
    {
        WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType GetApplicationType();
        List<string> SetLink(string aClassName, string aDataObjectID, bool isSystem = false);
        string GetActiveLayerName();
        int CountSelectedObject();
        //bool ZoomLinkedObject(string aClassName, string aDataObjectID, string aLayerName);
        bool isReady();
        string ZoomLinkedObject(string aMapObjectID, string aLayerName);
        void Activate();
        List<string> GetSelectedObjectIds();
        bool SelectMapObject(string layerName, string mapObjId);
        List<string> GetCrossingMapObjects(string sourceLayerName, string sourceMapObjId, string targerLayer);
        void SetFieldValue(string layerName, string mapObjectId, string fieldName, string fieldValue);
        string GetFieldValue(string layerName, string mapObjectId, string fieldName);
        void CommitLayer(string layerName);
    }

}
