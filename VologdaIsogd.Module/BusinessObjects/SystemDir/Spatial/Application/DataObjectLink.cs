using System;
using System.Collections.Generic;
using System.Linq;

namespace WinAisogdMapInfo.Module.SystemDir.Spatial.Application
{
    public class DataObjectLink
    {
        public DataObjectLink(string aDataObjectClass, string aDataObjectID, string aSpatialLayerName)
        {
            DataObjectClass = aDataObjectClass;
            DataObjectID = aDataObjectID;
            SpatialLayerName = aSpatialLayerName;
        }

        public string DataObjectClass { get; }
        public string DataObjectID { get; }
        public string SpatialLayerName { get; }

    }
}
