﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdMapInfo.Module.SystemDir;

namespace AISOGD.SystemDir
{
    public class SpatialLayerItem : BaseObjectXAF
    {
        public SpatialLayerItem() : base() { }
        public SpatialLayerItem(Session session) : base(session) { }

        //private Guid _id;
        //[Browsable(false), Key(true)]
        //public Guid ID
        //{
        //    get { return _id; }
        //    set { SetPropertyValue("ID", ref _id, value); }
        //}

        //private GisMap _spatialMap;
        //[DevExpress.Xpo.DisplayName("Карта")]
        ////[Indexed(Unique = true)]
        //public GisMap SpatialMap
        //{
        //    get { return _spatialMap; }
        //    set { SetPropertyValue("SpatialMap", ref _spatialMap, value);    
        //    }
        //}

        private GisLayer _spatialLayer;

        //[DataSourceCriteria("GisMap = '@This.SpatialMap'")]
        [DevExpress.Xpo.DisplayName("Таблица(слой)")]
        //[Indexed(Unique = true)]
        public GisLayer SpatialLayer
        {
            get { return _spatialLayer; }
            set { 
                SetPropertyValue("SpatialLayer", ref _spatialLayer, value);
            }
        }


        //private GisStyle _spatialStyle;
        // [DataSourceCriteria("GisLayer = '@This.SpatialLayer'")]
        //[DevExpress.Xpo.DisplayName("Стиль")]
        ////[Indexed(Unique = true)]
        //public GisStyle SpatialStyle
        //{
        //    get { return _spatialStyle; }
        //    set { SetPropertyValue("SpatialStyle", ref _spatialStyle, value); }
        //}

        private SpatialConfig _spatialConfig;
        [Association, Browsable(false)]
        public SpatialConfig SpatialConfig
        {
            get { return _spatialConfig; }
            set { SetPropertyValue("SpatialConfig", ref _spatialConfig, value); }
        }


        private GisLayerGroup i_GisLayerGroup;
        [Association, Browsable(false)]
        public GisLayerGroup GisLayerGroup
        {
            get { return i_GisLayerGroup; }
            set { SetPropertyValue("GisLayerGroup", ref i_GisLayerGroup, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
