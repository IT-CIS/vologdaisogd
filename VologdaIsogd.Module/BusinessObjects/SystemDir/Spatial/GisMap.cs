﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAisogdMapInfo.Module.SystemDir;

namespace AISOGD.SystemDir
{
   // [Custom("Caption", "ГИС карта"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GisMap : BaseObjectXAF
    {
        public GisMap() : base() { }
        public GisMap(Session session) : base(session) { }

        private string map_id;
        [DisplayName("ID ГИС Карты"), Size(255)]
        public string MapId
        {
            get { return map_id; }
            set { SetPropertyValue("MapId", ref map_id, value); }
        }

        private string local_map_id;
        [DisplayName("Локальный ID ГИС карты"), Size(255)]
        public string LocalMapId
        {
            get { return local_map_id; }
            set { SetPropertyValue("LocalMapId", ref local_map_id, value); }
        }

        private string i_name;
        [DisplayName("Наименование карты"), VisibleInListView(true), Size(255)]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        private bool is_Raster;
        [DisplayName("Растр"), VisibleInListView(true)]
        public bool isRaster
        {
            get { return is_Raster; }
            set { SetPropertyValue("isRaster", ref is_Raster, value); }
        }

        [Association, DevExpress.Xpo.DisplayName("Слои карты")]
        public XPCollection<GisLayer> GisLayers
        {
            get { return GetCollection<GisLayer>("GisLayers"); }
        }

        //protected override void OnSaving()
        //{
        //    if (Connect.FromSession(Session).IsExist<GisLayer>(mc => mc.LayerId == this.LayerId))
        //        throw new Exception("Такой слой уже существует");
        //    base.OnSaving();
        //}
    }
}
