﻿using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using WinAisogdMapInfo.Module.SystemDir;

namespace AISOGD.SystemDir
{
    [Custom("Caption", "Группы пространственных слоев"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class GisLayerGroup : BaseObjectXAF
    {
        public GisLayerGroup() : base() { }
        public GisLayerGroup(Session session) : base(session) { }


        private string i_name;
        [DevExpress.Xpo.DisplayName("Наименование группы"), VisibleInListView(true), Size(255)]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        [Association, DevExpress.Xpo.DisplayName("Слои")]
        public XPCollection<SpatialLayerItem> Layers
        {
            get { return GetCollection<SpatialLayerItem>("Layers"); }
        }
    }
}