﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AISOGD.SystemDir;

namespace AISOGD.SystemDir
{
    public class SpatialStyleItem : BaseObjectXAF
    {
        public SpatialStyleItem() : base() { }
        public SpatialStyleItem(Session session) : base(session) { }

        private SpatialLayerItem i_SpatialLayerItem;
        [Association, DevExpress.Xpo.DisplayName("Карта")]
        public SpatialLayerItem SpatialLayerItem
        {
            get { return i_SpatialLayerItem; }
            set
            {
                SetPropertyValue("SpatialLayerItem", ref i_SpatialLayerItem, value);    
            }
        }

        private string i_Name;
        [DevExpress.Xpo.DisplayName("Описание")]
        //[Indexed(Unique = true)]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private GisStyle _spatialStyle;
         [DataSourceCriteria("GisLayer = '@This.SpatialLayerItem.SpatialLayer'")]
        [DevExpress.Xpo.DisplayName("Стиль")]
        //[Indexed(Unique = true)]
        public GisStyle SpatialStyle
        {
            get { return _spatialStyle; }
            set { SetPropertyValue("SpatialStyle", ref _spatialStyle, value); }
        }

        

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
