﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AISOGD.SystemDir
{
    public class  BaseObjectXAF : BaseObject
    {
        public BaseObjectXAF(Session session) : base(session) { }
        public BaseObjectXAF() : base() { }

        private string _imporCode;

        [DisplayName("Идентификатор импорта")]
        [System.ComponentModel.Browsable(false)]
        public string ImportCode
        {
            get { return _imporCode; }
            set
            {
                _imporCode = value;
            }
        }
    }
}
