﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AISOGD.SystemDir
{
     public static class ExtensionBaseObject
    {
        public static bool GetOrCreateMemberByValue(this BaseObject baseObject, string propName, string condPropName, string condValue)
        {
            bool flag = false;
            try
            {
                Type t = baseObject.GetType();
                foreach (var prop in t.GetProperties())
                {
                    if (prop.Name == propName)
                    {
                        t = prop.PropertyType;
                        break;
                    }
                }
                
                BaseObject result = baseObject.Session.FindObject(t, new DevExpress.Data.Filtering.BinaryOperator(condPropName, condValue)) as BaseObject;
                if (result == null)
                {
                    result = baseObject.Session.GetClassInfo(t).CreateNewObject(baseObject.Session) as BaseObject;
                    result.SetMemberValue(condPropName, condValue);
                    baseObject.SetMemberValue(propName, result);
                    baseObject.Save();
                    flag = true;
                }
                else
                {
                    baseObject.SetMemberValue(propName, result);
                    baseObject.Save();

                }
            }
            catch (Exception e)
            {

            }
            return flag;
        }


        

        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeName);
                if (type != null)
                    return type;
            }
            return null;
        }
    }

}
