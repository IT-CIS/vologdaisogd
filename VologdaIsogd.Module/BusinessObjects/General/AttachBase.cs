using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DomainComponents.Common;

using AISOGD.SystemDir;

namespace AISOGD.General
{

    public class AttachBase : BaseObjectXAF
    {
        public AttachBase() : base() { }
        public AttachBase(Session session) : base(session) { }

        [Aggregated, Association("AttachBase-AttachmentFiles"), DisplayName("����������� �����")]
        [FileTypeFilter("��� �����", 1, "*.*")]
        [FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg", "*.tiff")]
        [FileTypeFilter("���������", 3, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        public XPCollection<AttachmentFiles> AttachmentFiles
        {
            get { return GetCollection<AttachmentFiles>("AttachmentFiles"); }
        }
     
    }

}