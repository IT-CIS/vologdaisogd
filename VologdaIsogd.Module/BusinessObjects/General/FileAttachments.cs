using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System.ComponentModel;
using DevExpress.Persistent.Validation;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using FileSystemData.BusinessObjects;
//using FileSystemData.BusinessObjects;
//using AISOGD.Constr;


namespace AISOGD.General
{
    //public class AttachmentFiles : BaseObjectXAF
    [FileAttachment("FileStore")]
    //[DeferredDeletionAttribute(false)]
    public class AttachmentFiles : BaseObject //FileAttachmentBase//
    {
        public AttachmentFiles(Session session) : base(session) { }
        //private DocumentType documentType;
        protected AttachBase _attachBase;

        private string i_Name;
        private DateTime i_RegDate;
        private Employee i_Registrator;
        private string i_Description;
        private string i_Sha256;

        [Size(255), DevExpress.Xpo.DisplayName("������������")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [DevExpress.Xpo.DisplayName("���� ����������")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        [DevExpress.Xpo.DisplayName("���������, ������������ ����")]
        public Employee Registrator
        {
            get { return i_Registrator; }
            set { SetPropertyValue("Registrator", ref i_Registrator, value); }
        }
        [Size(4000), DevExpress.Xpo.DisplayName("����������")]
        public string Description
        {
            get { return i_Description; }
            set { SetPropertyValue("Description", ref i_Description, value); }
        }
        [Size(SizeAttribute.Unlimited), System.ComponentModel.Browsable(false)]
        public string Sha256
        {
            get { return i_Sha256; }
            set { SetPropertyValue("Sha256", ref i_Sha256, value); }
        }
        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never), NoForeignKey, ImmediatePostData]
        [DevExpress.Xpo.DisplayName("����")]
        public FileSystemStoreObject FileStore
        {
            get { return GetPropertyValue<FileSystemStoreObject>("FileStore"); }
            set {
                SetPropertyValue<FileSystemStoreObject>("FileStore", value);
            }
        }

        //Address.Address i_Adress;
        //[Persistent, Association, DevExpress.Xpo.DisplayName("����������� �����")]
        //[FileTypeFilter("�����", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        //[FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        //[System.ComponentModel.Browsable(false)]
        //public Address.Address Adress
        //{
        //    get { return i_Adress; }
        //    set
        //    {
        //        SetPropertyValue("Adress", ref i_Adress, value);
        //    }
        //}
        //Address.ApartmentAddress i_ApartmentAddress;
        //[Persistent, Association, DevExpress.Xpo.DisplayName("����������� �����")]
        //[FileTypeFilter("�����", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        //[FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        //[System.ComponentModel.Browsable(false)]
        //public Address.ApartmentAddress ApartmentAddress
        //{
        //    get { return i_ApartmentAddress; }
        //    set
        //    {
        //        SetPropertyValue("ApartmentAddress", ref i_ApartmentAddress, value);
        //    }
        //}
        [Persistent, Association("AttachBase-AttachmentFiles"), DevExpress.Xpo.DisplayName("����������� �����")]
        [FileTypeFilter("��� �����", 1, "*.*")]
        [FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg", "*.tiff")]
        [FileTypeFilter("���������", 3, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        public AttachBase AttachBase
        {
            get { return _attachBase; }
            set
            {
                SetPropertyValue("AttachBase", ref _attachBase, value);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Registrator = currentEmpl;
            }
            i_RegDate = DateTime.Now.Date;
            try
            {
                i_Name = FileStore.FileName;
            }
            catch { }

        }

        protected override void OnSaving()
        {
            base.OnSaving();
        }

    }
}