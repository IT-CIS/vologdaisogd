﻿using AISOGD.GPZU;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Coordinates
{
    /// <summary>
    /// координата
    /// </summary>
    [ModelDefault("Caption", "Координата"), System.ComponentModel.DefaultProperty("CoordinateNo")]
    public class Coordinate : BaseObjectXAF
    {
        public Coordinate(Session session) : base(session) { }

        [DisplayName("Номер координаты")]
        public int CoordinateNo { get; set; }


        private double i_X;
        private double i_Y;
        public double X
        {
            get { return i_X; }
            set { i_X = Math.Round(value, 2); }
        }
        public double Y
        {
            get { return i_Y; }
            set { i_Y = Math.Round(value, 2); }
        }

        private GradPlan i_GPZU;
        //private SpecialZone i_SpecialZone;
        //private Easement i_Easement;
        //private RedLine i_RedLine;
        //private tSubParcel i_SubParcel;

        [Association, DisplayName("ГПЗУ")]
        [System.ComponentModel.Browsable(false)]
        public GradPlan GPZU
        {
            get { return i_GPZU; }
            set { SetPropertyValue("GPZU", ref i_GPZU, value); }
        }


        //[Association, DisplayName("Сервитут")]
        //[System.ComponentModel.Browsable(false)]
        //public Easement Easement
        //{
        //    get { return i_Easement; }
        //    set { SetPropertyValue("Easement", ref i_Easement, value); }
        //}

        //[Association, DisplayName("Красная линия")]
        //[System.ComponentModel.Browsable(false)]
        //public RedLine RedLine
        //{
        //    get { return i_RedLine; }
        //    set { SetPropertyValue("RedLine", ref i_RedLine, value); }
        //}

        //[Association, DisplayName("Зона")]
        //[System.ComponentModel.Browsable(false)]
        //public SpecialZone SpecialZone
        //{
        //    get { return i_SpecialZone; }
        //    set { SetPropertyValue("SpecialZone", ref i_SpecialZone, value); }
        //}

        //[Association, DisplayName("Часть участка")]
        //[System.ComponentModel.Browsable(false)]
        //public tSubParcel subParcel
        //{
        //    get { return i_SubParcel; }
        //    set { SetPropertyValue("subParcel", ref i_SubParcel, value); }
        //}

    }
}
