﻿using AISOGD.GPZU;
//using AISOGD.GPZU;
using AISOGD.Reglament;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Monuments
{

    //[NavigationItem("ГПЗУ")]
    [ModelDefault("Caption", "Объект культурного наследия"), System.ComponentModel.DefaultProperty("descript")]
    public class Monument : BaseObjectXAF
    {
        public Monument(Session session) : base(session) { }

        private string i_no;
        private string i_regNo;
        private string i_RegOrg;
        private DateTime i_regDate;
        private string i_name;
        //private GradPlan i_GPZU;


        private const string format = "{Name}";

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return ObjectFormatter.Format(format, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        //[Size(4), DisplayName("Номер, согласно чертежу")]
        //public string No
        //{
        //    get { return i_no; }
        //    set { SetPropertyValue("No", ref i_no, value); }
        //}

        [Size(255), DisplayName("Назначение")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        [Size(255), DisplayName("Регистрационный номер в реестре")]
        public string RegNo
        {
            get { return i_regNo; }
            set { SetPropertyValue("RegNo", ref i_regNo, value); }
        }
        [DisplayName("Дата регистрации")]
        public DateTime RegDate
        {
            get { return i_regDate; }
            set { SetPropertyValue("RegDate", ref i_regDate, value); }
        }
        [DisplayName("Наименование органа, принявшего решение о включении объекта в реестр")]
        public string RegOrg
        {
            get { return i_RegOrg; }
            set { try { SetPropertyValue("RegOrg", ref i_RegOrg, value); } catch { } }
        }
        //[Association, DisplayName("ГПЗУ")]
        //public XPCollection<GradPlan> GradPlan
        //{
        //    get { return GetCollection<GradPlan>("GradPlan"); }
        //}
        [Association, DisplayName("Зона объекта культурного наследия")]
        public XPCollection<MonumentZone> MonumentZone
        {
            get { return GetCollection<MonumentZone>("MonumentZone"); }
        }
        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }
    }
}
