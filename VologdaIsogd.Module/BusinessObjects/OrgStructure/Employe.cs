﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using AISOGD.Subject;
//using AISOGD.DocFlow;
using DevExpress.ExpressApp.Security.Strategy;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using AISOGD.SystemDir;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.OrgStructure
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    [ModelDefault("Caption", "Сотрудник"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class Employee : PersonalData
    {

        private string i_Code;
        private string i_UserName;
        private string i_Position;
        private Departament i_Departament;
        



        //[PersistentAlias("ToStr(SequentialNumber)"), Size(32), DisplayName("Код сотрудника")]
        [Size(32), DisplayName("Код сотрудника")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }
        [Size(255), DisplayName("Псевдоним")]
        public string UserName
        {
            get { return i_UserName; }
            set { SetPropertyValue("UserName", ref i_UserName, value); }
        }
        private Position i_PositionLink;
        [Size(255), DisplayName("Должность")]
        public Position PositionLink
        {
            get { return i_PositionLink; }
            set { try { SetPropertyValue("PositionLink", ref i_PositionLink, value); } catch { } }
        }
        [Size(255), DisplayName("Должность (описание)")]
        public string Position
        {
            get { return i_Position; }
            set { try { SetPropertyValue("Position", ref i_Position, value); } catch { } }
        }
        private PermissionPolicyUser i_user;
        [DisplayName("Пользователь системы")]
        public PermissionPolicyUser SysUser
        {
            get { return i_user; }
            set { SetPropertyValue("SysUser", ref i_user, value); }
        }

        [DisplayName("Отдел")]
        [Association("Departament-Employee", typeof(Departament))]
        public Departament Departament
        {
            get { return i_Departament; }
            set { try { SetPropertyValue("Departament", ref i_Departament, value); } catch { } }
        }

        //[Association, System.ComponentModel.Browsable(false)]
        //public OutLetter OutLetter { get; set; }


        [NonPersistent, System.ComponentModel.Browsable(false)]
        public object Id
        {
            get { return Oid; }
        }
        [NonPersistent, System.ComponentModel.Browsable(false)]
        public string Caption
        {
            get { return BriefName; }
            set { BriefName = value; }
        }

        [NonPersistent, System.ComponentModel.Browsable(false)]
        public Guid UserId
        {
            get
            {
                User usr = Session.FindObject<User>(new BinaryOperator("Employee", Id));
                if (usr != null)
                    return usr.Oid;
                else return Guid.Empty;
            }
        }

        public static Employee GetCurrentEmployee()
        {
            Connect connect = Connect.FromSettings();
            Employee currentEmpl = null;
            if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
            {
                currentEmpl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
            }
            return currentEmpl;
            //if (SecuritySystem.CurrentUser != null)
            //{
            //    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //    if (currentEmpl != null)
            //        return currentEmpl;
            //    else
            //        return null;
            //}
            //else return null;
        }

        

        //private string GetEmployeeCode() {
        //    //try { seqNo = XpoSequencer.GetNextValue(Session.DataLayer, employeeSeqName); }
        //    //catch { }
        //    //if (SequentialNumber == 0)
        //    //    SequentialNumber++;
        //    string codeNo = SequentialNumber.ToString();
        //    //while (codeNo.Length < employeeSeqNameLength)
        //    //    codeNo = "0" + codeNo;
        //    return codeNo;
        //}

        //private void SetEmployeeCode() {
        //    try {
        //        string codeNo = this.GetEmployeeCode();
        //        this.i_Code = codeNo;
        //        SetPropertyValue("Code", ref i_Code, codeNo);
        //    }
        //    catch { }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Дата регистрации
            // Номер сотрудника
            //string codeNo = GetLastSequenceNo().ToString();
            //while (codeNo.Length < employeeSeqNameLength)
            //    codeNo = "0" + codeNo;
            //Code = codeNo;
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        public Employee(Session session) : base(session) { }
    }
}
