﻿using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.OrgStructure
{

    /// <summary>
    /// Должность
    /// </summary>
    [ModelDefault("Caption", "Должность"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class Position : BaseObjectXAF
    {
        public Position(Session session) : base(session) { }
        
        private string i_Code;
        private string i_Name;
        private string i_BriefName;
        private string i_Note;


        [Size(32), DisplayName("Код")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }
        [Size(1000), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [Size(255), DisplayName("Сокращенное наименование")]
        public string BriefName
        {
            get { return i_BriefName; }
            set { SetPropertyValue("BriefName", ref i_BriefName, value); }
        }
        private string i_NameFrom;
        [Size(1000), DisplayName("Наименование (род. падеж)")]
        public string NameFrom
        {
            get { return i_NameFrom; }
            set { SetPropertyValue("NameFrom", ref i_NameFrom, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
        }



        
    }


}
