using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using AISOGD.SystemDir;

namespace AISOGD.OrgStructure
{
    /// <summary>
    /// ��� ���������������-��������������� �������
    /// </summary>
    [System.ComponentModel.DefaultProperty("Name")]
    public class dMunicipalityKind : BaseObjectXAF
    {
        public dMunicipalityKind(Session session) : base(session) { }

        [Size(16), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
}