using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using DevExpress.Persistent.Base.General;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;

namespace AISOGD.OrgStructure
{
    /// <summary>
    /// ���������� ��������� ��� ������ �������� �����
    /// </summary>
    public class MunicipalityBankDetails : BaseObjectXAF
    {
        public MunicipalityBankDetails(Session session) : base(session) { }

        [Size(255), DisplayName("������������ �������")]
        public string PaymentName { get; set; }

        [Size(64), DisplayName("���")]
        public string INN { get; set; }

        [Size(64), DisplayName("���")]
        public string KPP { get; set; }

        [Size(255), DisplayName("������������ �����")]
        public string BankName { get; set; }
        
        [Size(20), DisplayName("����� �����")]
        public string AccountNo { get; set; }

        [Size(16), DisplayName("��� �����")]
        public string Bik { get; set; }
        
        [Size(32), DisplayName("����. ���� �����")]
        public string CorrAccount { get; set; }


        [Association, DisplayName("����� ���������� ���������������-��������������� ��������")]
        public MainDepartment MainDepartment { get; set; }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}