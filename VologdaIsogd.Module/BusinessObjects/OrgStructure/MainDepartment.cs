using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using System.Collections.Generic;
using AISOGD.Isogd;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.OrgStructure
{


    [ModelDefault("Caption", "����� ���������� ���������������-��������������� ��������"), NavigationItem("��������� �����������")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Department")]
    public class MainDepartment : BaseObjectXAF
    {
        public MainDepartment(Session session) : base(session) { }

        private string i_Name;

        [Size(255), DisplayName("������������")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [Association, DisplayName("���������������-��������������� �������")]
        public Municipality Municipality { get; set; }

        [Association]
        [DisplayName("����������� �������������")]
        public XPCollection<Departament> Departament
        {
            get { return GetCollection<Departament>("Departament"); }
        }

        [Association]
        [DisplayName("���������� ��������� ��� ������ �������� �����")]
        public XPCollection<MunicipalityBankDetails> MunicipalityBankDetails
        {
            get { return GetCollection<MunicipalityBankDetails>("MunicipalityBankDetails"); }
        }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
        }



    }
}
