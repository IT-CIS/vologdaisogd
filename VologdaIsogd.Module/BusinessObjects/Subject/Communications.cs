﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;


namespace AISOGD.Subject
{
    /// <summary>
    /// Средство связи
    /// </summary>
    [ModelDefault("Caption", "Средство связи"), System.ComponentModel.DefaultProperty("FullName")]
    public class Communication : BaseObjectXAF
    {
        public Communication(Session session) : base(session) { }
        private dCommunicationKind _communicationKind;
        private string _value;
        private bool _mainFlag;
        private SubjectBase _subject;

        [DisplayName("Вид средства связи")]
        public dCommunicationKind CommunicationKind
        {
            get { return _communicationKind; }
            set { try { SetPropertyValue("CommunicationKind", ref _communicationKind, value); } catch { } }
        }
        [Size(255), DisplayName("Значение")]
        public string Value
        {
            get { return _value; }
            set { SetPropertyValue("Value", ref _value, value); }
        }

        [DisplayName("Признак основного средства связи")]
        public bool MainFlag
        {
            get { return _mainFlag; }
            set { SetPropertyValue("MainFlag", ref _mainFlag, value); }
        }

        [Size(510), System.ComponentModel.Browsable(false)]
        public string FullName
        {
            get
            {
                string fname;
                if (_communicationKind != null)
                    fname = _communicationKind.Name + ": " + _value;
                else
                    fname = "";
                return fname;
            }
        }

        public override string ToString()
        {
            return FullName;
        }

        [Association("SubjectBase-Communication", typeof(SubjectBase))]
        [System.ComponentModel.Browsable(false), DisplayName("Субъект")]
        public SubjectBase Subject
        {
            get { return _subject; }
            set { try { SetPropertyValue("Subject", ref _subject, value); } catch { } }
        }
        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }
    }


}