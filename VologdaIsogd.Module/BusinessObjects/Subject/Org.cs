﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Model;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Subject
{
    /// <summary>
    /// Юридическое лицо
    /// </summary>
    [ModelDefault("Caption", "Юридическое лицо"), NavigationItem("Общие реестры")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("Org")]
    public class Org : SubjectBase
    {
        
        public Org(Session session) : base(session) { }

        private string _briefName;
        private string _fullName;
        private string _name;
        private dOrgFormKind _orgForm;
        private Person _director;
        private string _directorPositionName;

        [DisplayName("Организационно-правовая форма")]
        [ImmediatePostData]
        public dOrgFormKind OrgForm
        {
            get { return _orgForm; }
            set { try { SetPropertyValue("OrgForm", ref _orgForm, value); } catch { } }
        }
        [Size(500), DisplayName("Наименование")]
        [ImmediatePostData]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        

        //[System.ComponentModel.Browsable(false)]
        [Size(500), DisplayName("Сокращенное наименование")]
        public string BriefName
        {
            get
            {                
                string res = String.Empty;
                try
                {
                    if (OrgForm != null)
                    {
                        res = String.Concat(OrgForm.BriefName.ToString(), " ");
                    }
                    res = String.Concat(res, '"', Name, '"');
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("BriefName", ref _briefName, value); }
        }

        private string i_BriefNameDat;
        [Size(500), DisplayName("Наименование (дат. падеж)")]
        public string BriefNameDat
        {
            get
            {
                string res = String.Empty;
                try
                {
                    if (OrgForm != null)
                    {
                        res = String.Concat(OrgForm.BriefName.ToString(), " ");
                    }
                    res = String.Concat(res, '"', Name, '"');
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("BriefNameDat", ref i_BriefNameDat, value); }
        }

        [Size(500), DisplayName("Полное наименование")]
        public string FullName
        {
            get
            {
                string res = String.Empty;
                try
                {
                    if (OrgForm != null)
                    {
                        res = String.Concat(OrgForm.Name.ToString(), " ");
                    }
                    res = String.Concat(res, '"', Name, '"');
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("FullName", ref _fullName, value); }
        }
        private string i_INN;
        [Size(12), DisplayName("ИНН")]
        [ImmediatePostData]
        public string INN
        {
            get { return i_INN; }
            set { SetPropertyValue("INN", ref i_INN, value); }
        }
        private string i_CompanySite;
        [Size(255), DisplayName("Сайт организации")]
        public string CompanySite
        {
            get
            {
                return i_CompanySite;
            }
            set { SetPropertyValue("CompanySite", ref i_CompanySite, value); }
        }

        [DisplayName("Руководитель организации")]
        public Person Director
        {
            get { return _director; }
            set { SetPropertyValue("Director", ref _director, value); }
        }
        [Size(255), DisplayName("Должность руководителя")]
        public string DirectorPositionName
        {
            get { return _directorPositionName; }
            set { SetPropertyValue("DirectorPositionName", ref _directorPositionName, value); }
        }
        

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        
    }
}
