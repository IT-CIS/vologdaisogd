﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.Enums;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.Surveys;
using AISOGD.DocFlow;
using AISOGD.Perm;
using AISOGD.Isogd;
using AISOGD.Constr;
using AISOGD.GPZU;

namespace AISOGD.Subject
{

    [NavigationItem("Общие реестры")]
    [ModelDefault("Caption", "Субъект\\Застройщик"), System.ComponentModel.DefaultProperty("FullName")]
    public class GeneralSubject : SubjectBase
    {
        public GeneralSubject(Session session) : base(session) { }

        private eSubjectType i_SubjectType;
        [DisplayName("Тип субъекта")]
        [ImmediatePostData]
        public eSubjectType SubjectType
        {
            get { return i_SubjectType; }
            set { SetPropertyValue("SubjectType", ref i_SubjectType, value); }
        }

        private string _name;
        [Size(500), DisplayName("Наименование\\ФИО")]
        [ImmediatePostData]
        public string Name
        {
            get
            {
                return _name;
            }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        private string i_NameFrom;
        [Size(500), DisplayName("Наименование (род. падеж)")]
        public string NameFrom
        {
            get
            {
                return i_NameFrom;
            }
            set { SetPropertyValue("NameFrom", ref i_NameFrom, value); }
        }
        private string i_NameDat;
        [Size(500), DisplayName("Наименование (дат. падеж)")]
        public string NameDat
        {
            get
            {
                return i_NameDat;
            }
            set { SetPropertyValue("NameDat", ref i_NameDat, value); }
        }
        private string i_NameBy;
        [Size(500), DisplayName("Наименование (твор. падеж)")]
        public string NameBy
        {
            get
            {
                return i_NameBy;
            }
            set { SetPropertyValue("NameBy", ref i_NameBy, value); }
        }

        private string _FullName;
        [Size(500), DisplayName("Полное наименование")]
        public string FullName
        {
            get
            {
                string res = String.Empty;
                try
                {
                    if (OrgForm != null)
                    {
                        res = String.Concat(OrgForm.BriefName.ToString(), " ");
                    }
                    res = String.Concat(res, Name);
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("FullName", ref _FullName, value); }
        }
        
        private string i_INN;
        [Size(12), DisplayName("ИНН")]
        [ImmediatePostData]
        public string INN
        {
            get { return i_INN; }
            set { SetPropertyValue("INN", ref i_INN, value); }
        }


        // сведения по организации
        private dOrgFormKind _orgForm;
        [DisplayName("Организационно-правовая форма")]
        [ImmediatePostData]
        public dOrgFormKind OrgForm
        {
            get { return _orgForm; }
            set { try
                {
                    SetPropertyValue("OrgForm", ref _orgForm, value);
                    //GetName();
                    //OnChanged("OrgForm");
                }
                catch { } }
        }
        private void GetName()
        {
            try
            {
                if (OrgForm != null)
                {
                    if (OrgForm.BriefName != null && OrgForm.BriefName != "")
                    {
                        Name = String.Concat(OrgForm.BriefName, " ", Name);
                    }
                    else
                        Name = Name;
                }
                else
                    Name = Name;
            }
            catch { }
        }
        private string i_CompanySite;
        [Size(255), DisplayName("Сайт организации")]
        public string CompanySite
        {
            get
            {
                return i_CompanySite;
            }
            set { SetPropertyValue("CompanySite", ref i_CompanySite, value); }
        }


        protected override void OnSaving()
        {
            try
            {
                if (String.IsNullOrEmpty(NameFrom))
                {
                    if (FullName != null)
                        if (FullName.Length > 0)
                            NameFrom = FullName;
                }
            }
            catch { }
            try
            {
                if (String.IsNullOrEmpty(NameBy))
                {
                    if (FullName != null)
                        if (FullName.Length > 0)
                            NameBy = FullName;
                }
            }
            catch { }
            try
            {
                if (String.IsNullOrEmpty(NameDat))
                {
                    if (FullName != null)
                        if (FullName.Length > 0)
                            NameDat = FullName;
                }
            }
            catch { }
            base.OnSaving();
        }

        [Association, DisplayName("Изыскания")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<Survey> Surveys
        {
            get { return GetCollection<Survey>("Surveys"); }
        }
        [Association, DisplayName("Входящие обращения")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<InLetter> InLetters
        {
            get { return GetCollection<InLetter>("InLetters"); }
        }
        //[Association, DisplayName("Исходящие обращения")]
        //[System.ComponentModel.Browsable(false)]
        //public XPCollection<OutLetter> OutLetters
        //{
        //    get { return GetCollection<OutLetter>("OutLetters"); }
        //}
        [Association, DisplayName("Разрешения на строительство")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<ConstrPerm> ConstrPerms
        {
            get { return GetCollection<ConstrPerm>("ConstrPerms"); }
        }
        [Association, DisplayName("Уведомления на строительство")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<PermNoticeIZD> PermNoticeIZDs
        {
            get { return GetCollection<PermNoticeIZD>("PermNoticeIZDs"); }
        }
        [Association, DisplayName("Градпланы")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<GradPlan> GradPlans
        {
            get { return GetCollection<GradPlan>("GradPlans"); }
        }
        [Association, DisplayName("Разрешения на ввод")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<UsePerm> UsePerms
        {
            get { return GetCollection<UsePerm>("UsePerms"); }
        }
        [Association, DisplayName("Уведомления на ввод")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<UsePermNoticeIZD> UsePermNoticeIZDs
        {
            get { return GetCollection<UsePermNoticeIZD>("UsePermNoticeIZDs"); }
        }
        [Association, DisplayName("Объект строительства")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<CapitalStructureBase> CapitalStructureBases
        {
            get { return GetCollection<CapitalStructureBase>("CapitalStructureBases"); }
        }
        [Association, DisplayName("Этап строительства")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<ConstrStage> ConstrStages
        {
            get { return GetCollection<ConstrStage>("ConstrStages"); }
        }
        [Association, DisplayName("ДЗУ")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdPhysStorageBook> IsogdPhysStorageBooks
        {
            get { return GetCollection<IsogdPhysStorageBook>("IsogdPhysStorageBooks"); }
        }
    }
}