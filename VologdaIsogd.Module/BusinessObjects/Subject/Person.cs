﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Model;
//using DevExpress.ExpressApp.Security.Strategy;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Subject
{
    /// <summary>
    /// Физическое лицо
    /// </summary>
    [ModelDefault("Caption", "Физическое лицо"), NavigationItem("Общие реестры")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class Person : PersonalData
    {
        
        
        [NonPersistent, System.ComponentModel.Browsable(false)]
        public string Caption
        {
            get { return BriefName; }
            set { BriefName = value; }
        }
        

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        public Person(Session session) : base(session) { }
    }
}
