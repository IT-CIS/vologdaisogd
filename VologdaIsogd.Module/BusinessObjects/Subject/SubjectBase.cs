﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
//using AISOGD.Address;
//using AISOGD.Survey;
//using AISOGD.DocFlow;
using AISOGD.SystemDir;
using AISOGD.Address;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace AISOGD.Subject
{
    public partial class SubjectBase : BaseObjectXAF
    {
        private string _note;
        private string _contactPhones = String.Empty;
        private string _contactMainEmail = String.Empty;
        private string _contactAddress;
        private const string infoFormat = "т.:{ContactPhones}, адрес: {ContactAddress}";
        private const string adressFormat = "{ContactAddress}";
        private const string phonesFormat = "{ContactPhones}";



        [Size(255), DisplayName("Телефоны")]
        public string ContactPhones
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(_contactPhones))
                    return _contactPhones;
                else
                {
                    try
                    {

                        foreach (Communication comm in Communications)
                            if (comm.CommunicationKind.Name == "Телефон" ||
                                comm.CommunicationKind.Name == "Сотовый телефон")
                            {
                                if (res != String.Empty)
                                    res += ", ";
                                res += comm.Value;
                            }
                    }
                    catch { }
                }
                _contactPhones = res;
                return _contactPhones;
            }
            set { SetPropertyValue("ContactPhones", ref _contactPhones, value); }
        }

        [Size(255), DisplayName("E-mail")]
        public string ContactMainEmail
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(_contactMainEmail))
                    return _contactMainEmail;
                else
                {
                    try
                    {
                        foreach (Communication comm in Communications)
                            if (comm.CommunicationKind.Name == "E-mail")
                            {
                                if (res != String.Empty)
                                    res += ", ";
                                res += comm.Value;
                            }
                    }
                    catch { }
                }
                _contactMainEmail = res;
                return _contactMainEmail;
            }
            set { SetPropertyValue("ContactMainEmail", ref _contactMainEmail, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("Адрес")]
        [ImmediatePostData]
        public string ContactAddress
        {
            get
            {
                string res = String.Empty;
                if (!String.IsNullOrEmpty(_contactAddress))
                    return _contactAddress;
                else
                {
                    try
                    {
                        AISOGD.Address.ApartmentAddress addr;
                        if (Addresses.Count > 0)
                        {
                            addr = Addresses[0] as AISOGD.Address.ApartmentAddress;
                            res = addr.ShortAddress;
                        }
                    }
                    catch { }
                }
                _contactAddress = res;
                return _contactAddress;
            }
            set { SetPropertyValue("ContactAddress", ref _contactAddress, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("Примечание")]
        public string Note
        {
            get { return _note; }
            set { SetPropertyValue("Note", ref _note, value); }
        }
        private string _FullContactInfoImport;
        [Size(500), DisplayName("Реквизиты импорт")]
        public string FullContactInfoImport
        {
            get
            {
                return _FullContactInfoImport;
            }
            set { SetPropertyValue("FullContactInfoImport", ref _FullContactInfoImport, value); }
        }

        private string _FullContactInfo;
        [Size(500), DisplayName("Реквизиты")]
        public string FullContactInfo
        {
            get
            {
                string res = String.Empty;
                if (FullContactInfoImport != null && FullContactInfoImport != "")
                    res = FullContactInfoImport;
                else
                {
                    try
                    {
                        res = "Адрес: " + ContactAddress + ", ";
                    }
                    catch { }
                    try
                    {
                        res += String.Format("т.: {0}, ", ContactPhones);
                    }
                    catch { }
                    try
                    {
                        res += String.Format("E-mail: {0}", ContactMainEmail);
                    }
                    catch { }
                    res = res.Trim();
                    res = res.TrimEnd(',');
                }
                return res;
            }
            set { SetPropertyValue("FullContactInfo", ref _FullContactInfo, value); }
        }

        [Size(1000), System.ComponentModel.Browsable(false)]
        public string Info
        {
            get
            {
                return String.Format("т.:{0}", ContactPhones); //ObjectFormatter.Format(infoFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        [Size(1000), System.ComponentModel.Browsable(false)]
        public string address
        {
            get
            {
                return String.Format("{0}", ContactAddress);//ObjectFormatter.Format(adressFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        [Size(510), System.ComponentModel.Browsable(false)]
        public string phones
        {
            get
            {
                return ContactPhones; //ObjectFormatter.Format(phonesFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        //[Association("SubjectBase-ApartmentAddress", typeof(AISOGD.Address.ApartmentAddress))]
        [Association, DisplayName("Адреса")]
        public XPCollection<ApartmentAddress> Addresses
        {
            get { return GetCollection<ApartmentAddress>("Addresses"); }
        }

        [Association("SubjectBase-Communication", typeof(Communication))]
        [DisplayName("Средства связи")]
        public XPCollection Communications
        {
            get { return GetCollection("Communications"); }
        }

        //[Association, DisplayName("Лицензии")]
        //public XPCollection<License> Licenses
        //{
        //    get { return GetCollection<License>("Licenses"); }
        //}
        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public SubjectBase(Session session) : base(session) { }
    }
}
