﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Subject
{

    /// <summary>
    /// Вид средства связи
    /// </summary>
    [ModelDefault("Caption", "Вид средства связи"), System.ComponentModel.DefaultProperty("Name")]
    //[NavigationItem("Основные справочники")]
    public class dCommunicationKind : BaseObjectXAF
    {
        private string _name;

        [Size(64), DisplayName("Код")]
        public string Code { get; set; }

        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public override string ToString()
        {
            return Name;
        }

        public dCommunicationKind(Session session) : base(session) { }
    }


}