using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Subject
{
    /// <summary>
    /// ��������������-�������� �����
    /// </summary>
    [ModelDefault("Caption", "��������������-�������� �����")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    [ImageName("BO_Category")]//, NavigationItem("�������� �����������")]
    public class dOrgFormKind : BaseObjectXAF {
        private string _name;
        private string _briefName;

        [Size(64), DisplayName("���")]
        public string Code { get; set; }

        [Size(64), DisplayName("������� ������������")]
        [ImmediatePostData]
        public string BriefName {
            get { return _briefName; }
            set { SetPropertyValue("BriefName", ref _briefName, value); }
        }
        [Size(255), DisplayName("������������")]
        [ImmediatePostData]
        public string Name {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public dOrgFormKind(Session session) : base(session) { }
    }
}
