using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "������ �����")]
    public class IsogdPartition : BaseObjectXAF
    {

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������ ����� {0} {1}", PartNo, Name);
            }
        }

        public IsogdPartition(Session session) : base(session) { }
        [Size(255), DisplayName("����� �������")]
        public string PartNo { get; set; }

        [Size(255), DisplayName("������������ �������")]
        public string Name { get; set; }

        [Size(255), DisplayName("������ ������������ �������")]
        public string FullName { get; set; }

        [DisplayName("��� �������")]
        public ePartitionType PartitionType { get; set; }


        private int i_InitialRegNo;
        [DisplayName("������ ��������� ��������������� �������")]
        public int InitialRegNo
        {
            get
            {
                return i_InitialRegNo;
            }
            set { SetPropertyValue("InitialRegNo", ref i_InitialRegNo, value); }

        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        
        //public IsogdDocument IsogdDocument { get; set; }
        [Association, DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

        [Association, DisplayName("����� ����� ���������� �����")]
        public XPCollection<IsogdMap> IsogdMap
        {
            get { return GetCollection<IsogdMap>("IsogdMap"); }
        }

        [Association, DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBook
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBook"); }
        }

        [Association, DisplayName("�������� ����������� ���������� �������")]
        public XPCollection<IsogdRegistrationCard> IsogdRegistrationCard
        {
            get { return GetCollection<IsogdRegistrationCard>("IsogdRegistrationCard"); }
        }
        
        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

    }

   

    //public class IsogdPartition : XPObject
    //{
    //    public IsogdPartition()
    //        : base()
    //    {
    //        // This constructor is used when an object is loaded from a persistent storage.
    //        // Do not place any code here.
    //    }

    //    public IsogdPartition(Session session)
    //        : base(session)
    //    {
    //        // This constructor is used when an object is loaded from a persistent storage.
    //        // Do not place any code here.
    //    }

    //    public override void AfterConstruction()
    //    {
    //        base.AfterConstruction();
    //        // Place here your initialization code.
    //    }
    //}

}