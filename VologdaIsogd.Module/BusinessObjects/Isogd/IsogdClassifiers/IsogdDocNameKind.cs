using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{

    /// <summary>
    /// ��� ������������ ��������� �����
    /// </summary>
    [NavigationItem("�������������� �����")]
    [ModelDefault("Caption", "������������ ��������� (���������)")]
    public class dIsogdDocumentNameKind : dIsogdClassifierBase
    {

        public dIsogdDocumentNameKind(Session session) : base(session) { }

        private IsogdDocumentClass i_IsogdDocumentClass;
        [DisplayName("����� ��������� �����")]
        public IsogdDocumentClass IsogdDocumentClass
        {
            get { return i_IsogdDocumentClass; }
            set
            {
                SetPropertyValue("IsogdDocumentClass", ref i_IsogdDocumentClass, value);
            }
        }

        private bool i_LandFlag;
        [DisplayName("��������� �� ����� �������")]
        public bool LandFlag
        {
            get { return i_LandFlag; }
            set
            {
                SetPropertyValue("LandFlag", ref i_LandFlag, value);
            }
        }

        [Size(500), DisplayName("������������� ��������")]
        public string AdditionNotes { get; set; }
    }
}