using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{

    /// <summary>
    /// ��� ����� ����. ������������
    /// </summary>
    [NavigationItem("�������������� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "��� ����� ����. ������������")]
    public class dIsogdTerrSchemeClass : dIsogdClassifierBase
    {

        public dIsogdTerrSchemeClass(Session session) : base(session) { }

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("��� ����� ����. ������������ {0}: {1}", Code, Name);
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }

        //[Size(255), DisplayName("������������ ������")]
        //public string Name { get; set; }

        [DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

    }


    /// <summary>
    /// ��� �����/����� �����
    /// </summary>
    [NavigationItem("�������������� �����"), ModelDefault("Caption", "��� �����/����� �����")]
    public class dIsogdMapKind : dIsogdClassifierBase
    {

        public dIsogdMapKind(Session session) : base(session) { }

        //[Size(255), DisplayName("���")]
        //public string Code { get; set; }

        //[Size(255), DisplayName("������������")]
        //public string Name { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }
    }

}