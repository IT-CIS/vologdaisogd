using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base.General;


namespace AISOGD.Isogd
{

    /// <summary>
    /// ����� ��������� �����
    /// </summary>
    [NavigationItem("�������������� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "����� ��������� �����")]
    public class IsogdDocumentClass : TreeNodeAbstract
    {

        public IsogdDocumentClass(Session session) : base(session) { }
        public IsogdDocumentClass(Session session, string name) : base(session) { this.Name = name; }

        [Size(255), DisplayName("�������� ������")]
        public string descript
        {
            get
            {
                return String.Format("����� ��������� ����� {0}: {1}", Code, Name);
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        
        [Size(255), DisplayName("���")]
        public string Code { get; set; }

        //[Size(255), DisplayName("������ ���(�� �������)")]
        //public string OldCode { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        //[Association, DisplayName("������ �����")]
        [DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        [DisplayName("����� ��������")]
        public IsogdStorageBook IsogdStorageBook { get; set; }

        

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        private IsogdDocumentMainClass i_Parent;
        [Association, DisplayName("����� �������� ������")]
        public IsogdDocumentMainClass ParentIsogdDocumentClass
        {
            get { return i_Parent; }
            set { SetPropertyValue("ParentIsogdDocumentClass", ref i_Parent, value); }
        }

        //[Association, DisplayName("������ ���������� �����")]
        //public XPCollection<IsogdDocumentClass> IsogdDocumentClassChilds
        //{
        //    get { return GetCollection<IsogdDocumentClass>("IsogdDocumentClassChilds"); }
        //}
        [Association, DisplayName("������� �� �������")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdAbstractRequest> IsogdAbstractRequests
        {
            get { return GetCollection<IsogdAbstractRequest>("IsogdAbstractRequests"); }
        }

        protected override ITreeNode Parent
        {
            get
            {
                return ParentIsogdDocumentClass;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return new System.ComponentModel.BindingList<object>();
            }
        }
    }

    /// <summary>
    /// ����� �������� ������
    /// </summary>
    [NavigationItem("�������������� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "����� ��������� ����� �������� ������")]
    public class IsogdDocumentMainClass : TreeNodeAbstract
    {
        public IsogdDocumentMainClass(Session session) : base(session) { }
        public IsogdDocumentMainClass(Session session, string name) : base(session) { this.Name = name; }

        [Size(255), DisplayName("���")]
        public string Code { get; set; }

        //[Size(255), DisplayName("������������ ������")]
        //public string Name { get; set; }

        [Association, DisplayName("������ ���������� �����")]
        public XPCollection<IsogdDocumentClass> IsogdDocumentClassChilds
        {
            get { return GetCollection<IsogdDocumentClass>("IsogdDocumentClassChilds"); }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return null;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return IsogdDocumentClassChilds;
            }
        }
    }



    //public enum IsogdDocumentMainClass
    //{
    //    //private string 1;
    //    ������������������������������������� = 0,
    //    ������������������������������������������������������ = 1,
    //    ���������������������������������� = 2,
    //    ��������������������������������������� = 3,
    //    ������������������������������������������������������������������������� = 4,
    //    ��������������������������������������������������������������� = 5,
    //    ���� = 6

    //}

}