using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using AISOGD.SystemDir;

namespace AISOGD.Isogd
{

    /// <summary>
    /// ������� ������������� ��� ���������� �����
    /// </summary>
    //[NavigationItem("�������������� �����")]
    public class dIsogdClassifierBase : BaseObjectXAF
    {
        public dIsogdClassifierBase(Session session) : base(session) { }

        [Size(32), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
}