using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{
    /// <summary>
    /// ������ ����� ����� ��������
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "������ ����� ����� ��������")]
    public class IsogdInDataCard : BaseObjectXAF
    {
        public IsogdInDataCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private string i_docName;
        private dIsogdDocumentNameKind i_docNameKind;
        private Employee i_Employee;
        private string i_tomeNo;

        private const string descriptFormat = "������ ����� ����� �������� � {RecNo} �� {RecordDate}";
        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������ ����� ����� �������� � {0} �� {1}", CardNo, RecordDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Size(4), DisplayName("����� ���� ����� ����� ��������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("CardNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ������")]
        public string CardNo
        {
            get { return RecNoCore; }
            set { SetPropertyValue("CardNo", ref RecNoCore, value); }
        }

        [Persistent("CardNoOKTMO")]
        private string RecNoCoreOKTMO;
        [PersistentAlias("RecNoCoreOKTMO")]
        [DisplayName("����� ������ + �����")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoOKTMO
        {
            get { return RecNoCoreOKTMO; }
        }

        [Persistent("RecNoCoreCommon")]
        private string RecNoCoreCommon;
        [PersistentAlias("RecNoCoreCommon")]
        [DisplayName("����� ������ � ������ ������")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoCommon
        {
            get { return RecNoCoreCommon; }
        }

        

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        [DisplayName("������ �������� ���������")]
        public dIsogdReceivingMethod IsogdReceivingMethod { get; set; }

        [Size(64), DisplayName("����� ��������� � ������")]
        public string DocNo { get; set; }

        [DisplayName("������������ ��������� (���������)")]
        [ImmediatePostData]
        public dIsogdDocumentNameKind IsogdDocumentNameKind
        {
            get { return i_docNameKind; }
            set
            {
                try { SetPropertyValue("IsogdDocumentNameKind", ref i_docNameKind, value); }
                catch { }
                OnChanged("IsogdDocumentNameKind");
            }
        }

        [Size(255), DisplayName("������������ ���������")]
        public string DocName
        {
            get
            {
                try
                {
                    {
                        if (i_docName == null || i_docName == "")
                            if (i_docNameKind.Name.Length > 0)
                                i_docName = i_docNameKind.Name;
                    }
                }
                catch { }
                return i_docName;
            }
            set
            {
                SetPropertyValue("DocName", ref i_docName, value);
            }
        }

        [DisplayName("����� ������������� ���������")]
        public dIsogdDocumentForm IsogdDocumentForm { get; set; }

        [DisplayName("����� ���������"), Size(64)]
        public string DocumentVolume { get; set; }

        [DisplayName("��� ��������")]
        public dIsogdDataStorageKind IsogdDataStorageKind { get; set; }

        [DisplayName("���������������� ������")]
        public IsogdLetter IsogdLetter { get; set; }

        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [DisplayName("�������� �����")]
        public IsogdDocument IsogdDocument { get; set; }

        [Association, DisplayName("�������� ����������� ��������� �����")]
        public IsogdRegistrationCard IsogdRegistrationCard { get; set; }

        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality;
            }
            set { try { SetPropertyValue("Municipality", ref i_Municipality, value); } catch { } }
        }
        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        private bool i_isCopyFromInmeta;
        [DisplayName("��������� �� ������ �����")]
        [System.ComponentModel.Browsable(false)]
        public bool isCopyFromInmeta
        {
            get { return i_isCopyFromInmeta; }
            set { SetPropertyValue("isCopyFromInmeta", ref i_isCopyFromInmeta, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

       
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            // �������� ����� ��������� ���� ����� ����� �� ������� ���� �����
            if (i_tomeNo == null || i_tomeNo == "")
            {
                IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.eIsogdBooksKind.������������������, true)));
                if (i_IsogdBooksCard != null)
                {
                    if (i_IsogdBooksCard.BookTomeNo != null)
                    {
                        i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                    }
                }
            }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {

                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                    {
                        if (Municipality != null)
                        {
                            if (Municipality.OKTMO != null || Municipality.OKTMO != String.Empty)
                            {
                                this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + Municipality.OKTMO, string.Empty));
                                this.RecNoCoreOKTMO = Municipality.OKTMO +"_" + RecNoCore;
                            }
                        }
                    }
                }
                catch { }

                try
                {
                    if (this.RecNoCoreCommon == String.Empty || this.RecNoCoreCommon == null)
                        this.RecNoCoreCommon = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}