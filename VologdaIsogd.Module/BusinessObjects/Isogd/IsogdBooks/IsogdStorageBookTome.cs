using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{
    /// <summary>
    /// ��� ����� ��������
    /// </summary>
    [System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "��� ����� ��������")]
    public class IsogdStorageBookTome : BaseObjectXAF
    {
        public IsogdStorageBookTome(Session session) : base(session) { }

        private string i_name;
        private DateTime i_OpenenigTomeDate;
        private DateTime i_ClosingTomeDate;
        private bool i_ActiveTomeFlag;

        private const string descriptFormat = "��� ����� �������� ����� � {TomeNo}, {Name}";
        [System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("��� ����� �������� ����� � {0}, {1}", TomeNo, Name);
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        [Association, DisplayName("����� �������� �����")]
        public IsogdStorageBook IsogdStorageBook { get; set; }

        [Size(64), DisplayName("����� ���� ����� ��������")]
        public string TomeNo { get; set; }


        [Size(255), DisplayName("������������ ����")]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        [DisplayName("���� ������ �������")]
        public DateTime OpenenigTomeDate
        {
            get { return i_OpenenigTomeDate; }
            set { SetPropertyValue("OpenenigTomeDate", ref i_OpenenigTomeDate, value); }
        }
        [DisplayName("���� �������� ����")]
        public DateTime ClosingTomeDate
        {
            get { return i_ClosingTomeDate; }
            set { SetPropertyValue("ClosingTomeDate", ref i_ClosingTomeDate, value); }
        }
        [DisplayName("�������� ���")]
        public bool ActiveTomeFlag
        {
            get { return i_ActiveTomeFlag; }
            set { SetPropertyValue("ActiveTomeFlag", ref i_ActiveTomeFlag, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

       
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_OpenenigTomeDate == DateTime.MinValue)
            {
                i_OpenenigTomeDate = DateTime.Now.Date;
                i_ActiveTomeFlag = true;
            }

        }
    }
}