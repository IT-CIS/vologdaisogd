using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.Land;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.XtraEditors;
using AISOGD.Enums;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;

namespace AISOGD.Isogd
{
    /// <summary>
    /// ����� �������� �������� �����
    /// </summary>
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "����� �������� �������� �����")]
    public class IsogdStorageBook : BaseObjectXAF
    {
        public IsogdStorageBook(Session session) : base(session) { }

        private string i_name;

        private const string descriptFormat = "����� �������� �������� ����� � {Code}";
        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("����� �������� �������� ����� � {0}", Code);
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Association, DisplayName("������ �����")]
        //[ImmediatePostData]
        public IsogdPartition IsogdPartition { get; set; }

        private string i_Code;
        [Size(64), DisplayName("��� ����� ��������")]
        [ImmediatePostData]
        public string Code
        {
            get { return i_Code; }
            set
            {
                SetPropertyValue("Code", ref i_Code, value);
                if(IsogdPartition!= null)
                {
                    if (IsogdPartition.PartNo != null)
                    {
                        if(IsogdPartition.PartNo == "8" || IsogdPartition.PartNo == "08")
                            i_name = "����� �������� ���������� ����� ���������� ������� � ���.� " + i_Code;
                        else
                            i_name = "����� �������� ���������� ����� ��� � " + i_Code;
                    }
                }
                else
                    i_name = "����� �������� ���������� ����� ���������� ������� � ���.� " + i_Code;
                OnChanged("Code");
            }
        }
        

        private DateTime i_OpenenigBookDate;
        [DisplayName("���� ������ �������")]
        public DateTime OpenenigBookDate
        {
            get { return i_OpenenigBookDate; }
            set { SetPropertyValue("OpenenigBookDate", ref i_OpenenigBookDate, value);
                i_Year = i_OpenenigBookDate.Year;
                i_eMonth = (eMonths)Enum.Parse(typeof(eMonths), (i_OpenenigBookDate.Month - 1).ToString());
                OnChanged("OpenenigBookDate");
            }
        }
        private eMonths i_eMonth;
        [DisplayName("�����")]
        public eMonths eMonth
        {
            get { return i_eMonth; }
            set { SetPropertyValue("eMonth", ref i_eMonth, value); }
        }

        private int i_Year;
        [DisplayName("���")]
        public int Year
        {
            get
            {
                return i_Year;
            }
            set { SetPropertyValue("Year", ref i_Year, value); }
        }
        [Size(255), DisplayName("������������ ����� ��������")]
        public string Name
        {
            get
            {
                return i_name;
            }
            set
            {
                SetPropertyValue("Name", ref i_name, value);
            }
        }
        private string i_AddressInfo;
        [Size(1024), DisplayName("�������������� (�����)")]
        public string AddressInfo
        {
            get { return i_AddressInfo; }
            set { SetPropertyValue("AddressInfo", ref i_AddressInfo, value); }
        }
        //private Parcel i_Parcel;
        //[DisplayName("��������� �������")]
        //[ImmediatePostData]
        //public Parcel Parcel
        //{
        //    get
        //    {
        //        try
        //        {
        //            i_Parcel = GetParcel();
        //        }
        //        catch { }
        //        return i_Parcel;
        //    }
        //    set
        //    {
        //        SetPropertyValue("Parcel", ref i_Parcel, value);
        //        if (i_Parcel != null)
        //        {
        //            if(i_Parcel.IsogdStorageBook == null)
        //                i_Parcel.IsogdStorageBook = this;
        //        }
        //        OnChanged();
        //    }
        //}
        //private Parcel GetParcel()
        //{
        //    Parcel res = null;
        //    Connect connect = Connect.FromSession(Session);
        //    res = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber.Trim() == this.Code.Trim());
        //    return res;
        //}


        private Employee i_Employee;
        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get
            {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality;
            }
            set { try { SetPropertyValue("Municipality", ref i_Municipality, value); } catch { } }
        }
        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        private int? fDocsCount = null;
        [DisplayName("���������� ���������� ����� � �����")]
        public int? DocsCount
        {
            get
            {
                if (!IsLoading && !IsSaving && fDocsCount == null)
                    UpdateDocsCount(false);
                return fDocsCount;
            }
        }
        public void UpdateDocsCount(bool forceChangeEvents)
        {
            int? oldDocsCount = fDocsCount;
            fDocsCount = Convert.ToInt32(Evaluate(CriteriaOperator.Parse("IsogdDocuments.Count")));
            if (forceChangeEvents)
                OnChanged("OrdersCount", oldDocsCount, fDocsCount);
        }
        private bool i_isCopyFromInmeta;
        [DisplayName("��������� �� ������ �����")]
        [System.ComponentModel.Browsable(false)]
        public bool isCopyFromInmeta
        {
            get { return i_isCopyFromInmeta; }
            set { SetPropertyValue("isCopyFromInmeta", ref i_isCopyFromInmeta, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association("IsogdDocument-IsogdStorageBook"), DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocuments
        {
            get { return GetCollection<IsogdDocument>("IsogdDocuments"); }
        }

        [Association, DisplayName("��������� �������, ��������� � ������")]
        public XPCollection<Parcel> Parcels
        {
            get { return GetCollection<Parcel>("Parcels"); }
        }
        [Association, DisplayName("���")]
        public XPCollection<IsogdPhysStorageBook> PhysStorageBooks
        {
            get { return GetCollection<IsogdPhysStorageBook>("PhysStorageBooks"); }
        }
        //[Association, DisplayName("���� ����� ��������")]
        //public XPCollection<IsogdStorageBookTome> IsogdStorageBookTomes
        //{
        //    get { return GetCollection<IsogdStorageBookTome>("IsogdStorageBookTomes"); }
        //}
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            if (OpenenigBookDate == DateTime.MinValue)
                OpenenigBookDate = DateTime.Now.Date;
        }


        #region
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ��� �������� ������ �� �������� � ��� (���������� ����� ��������) � ��
        /// </summary>
        private void IsogdDocuments_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ��������
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdDocument newIsogdDocument = (IsogdDocument)e.ChangedObject;

                    // ���������, ���� �� ����� �������� � �� � ���, ���� ��� - ���������
                    foreach (Parcel parcel in this.Parcels)
                    {
                        bool isExist = false;
                        foreach (IsogdDocument parcelDocument in parcel.IsogdDocuments)
                        {
                            if (parcelDocument == newIsogdDocument)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            parcel.IsogdDocuments.Add(newIsogdDocument);
                            parcel.Save();
                        }
                    }
                    //try
                    //{
                    //    if (newIsogdDocument.IsogdDocumentNameKind.LandFlag)
                    //    {
                    //        foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    //        {
                    //            bool isExist = false;
                    //            foreach (IsogdDocument isogdDocument in isogdPhysStorageBook.IsogdDocuments)
                    //            {
                    //                if (isogdDocument == newIsogdDocument)
                    //                {
                    //                    isExist = true;
                    //                    break;
                    //                }
                    //            }
                    //            if (!isExist)
                    //            {
                    //                isogdPhysStorageBook.IsogdDocuments.Add(newIsogdDocument);
                    //                isogdPhysStorageBook.Save();
                    //            }
                    //        }
                    //    }
                    //}
                    //catch { }
                }
                #endregion

                #region ���� �� ��������� ������� ��������
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdDocument newIsogdDocument = (IsogdDocument)e.ChangedObject;
                    // ���������, ���� �� ����� �������� � �� � ���, ���� ���� - �������
                    foreach (Parcel parcel in this.Parcels)
                    {
                        bool isExist = false;
                        foreach (IsogdDocument parcelDocument in parcel.IsogdDocuments)
                        {
                            if (parcelDocument == newIsogdDocument)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            parcel.IsogdDocuments.Remove(newIsogdDocument);
                            parcel.Save();
                        }
                    }
                    //foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument isogdDocument in isogdPhysStorageBook.IsogdDocuments)
                    //    {
                    //        if (isogdDocument == newIsogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        isogdPhysStorageBook.IsogdDocuments.Remove(newIsogdDocument);
                    //        isogdPhysStorageBook.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }


        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ������ �� �� � ��� (���������� ����� ��������) � ���������
        /// </summary>
        private void Parcels_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    Parcel newObj = (Parcel)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
                    foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    {
                        bool isExist = false;
                        foreach (Parcel parcel in isogdDocument.Parcels)
                        {
                            if (parcel == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            isogdDocument.Parcels.Add(newObj);
                            isogdDocument.Save();
                        }
                    }
                    //foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    //{
                    //    bool isExist = false;
                    //    foreach (Parcel parcel in isogdPhysStorageBook.Parcels)
                    //    {
                    //        if (parcel == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        isogdPhysStorageBook.Parcels.Add(newObj);
                    //        isogdPhysStorageBook.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    Parcel newObj = (Parcel)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ���� - �������
                    foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    {
                        bool isExist = false;
                        foreach (Parcel parcel in isogdDocument.Parcels)
                        {
                            if (parcel == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            isogdDocument.Parcels.Remove(newObj);
                            isogdDocument.Save();
                        }
                    }
                    //foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    //{
                    //    bool isExist = false;
                    //    foreach (Parcel parcel in isogdPhysStorageBook.Parcels)
                    //    {
                    //        if (parcel == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        isogdPhysStorageBook.Parcels.Remove(newObj);
                    //        isogdPhysStorageBook.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }
        ///// <summary>
        ///// ������� �������� �, ��� �������������, ���������� ������ �� ��� (���������� ����� ��������) c �� � ���������
        ///// </summary>
        private void PhysStorBooks_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ���
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdPhysStorageBook newObj = (IsogdPhysStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
                    //foreach (IsogdDocument isogdDocument in newObj.IsogdDocuments)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument thisDoc in this.IsogdDocuments)
                    //    {
                    //        if (thisDoc == isogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        this.IsogdDocuments.Add(isogdDocument);
                    //        this.Save();
                    //    }
                    //}
                    //if (Parcel != null)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdPhysStorageBook isogdPhysStorageBook in Parcel.PhysStorageBooks)
                    //    {
                    //        if (isogdPhysStorageBook == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        Parcel.PhysStorageBooks.Add(newObj);
                    //        Parcel.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ���
                //if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                //{
                //    IsogdPhysStorageBook newObj = (IsogdPhysStorageBook)e.ChangedObject;
                //    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
                //    foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                //    {
                //        bool isExist = false;
                //        foreach (IsogdPhysStorageBook isogdPhysStorageBook in isogdDocument.PhysStorageBooks)
                //        {
                //            if (isogdPhysStorageBook == newObj)
                //            {
                //                isExist = true;
                //                break;
                //            }
                //        }
                //        if (isExist)
                //        {
                //            isogdDocument.PhysStorageBooks.Remove(newObj);
                //            isogdDocument.Save();
                //        }
                //    }
                //    //if (Parcel != null)
                //    //{
                //    //    bool isExist = false;
                //    //    foreach (IsogdPhysStorageBook isogdPhysStorageBook in Parcel.PhysStorageBooks)
                //    //    {
                //    //        if (isogdPhysStorageBook == newObj)
                //    //        {
                //    //            isExist = true;
                //    //            break;
                //    //        }
                //    //    }
                //    //    if (isExist)
                //    //    {
                //    //        Parcel.PhysStorageBooks.Remove(newObj);
                //    //        Parcel.Save();
                //    //    }
                //    //}
                //}
                #endregion
            }
        }
        #endregion

        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            // ���������� ��������� ���������� �����
            if (property.Name == "IsogdDocuments")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(IsogdDocuments_CollectionChanged);
            }
            //���������� ��������� ��������� ��������
            if (property.Name == "Parcels")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            }
            //���������� ��������� ���
            if (property.Name == "PhysStorageBooks")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(PhysStorBooks_CollectionChanged);
            }
            return result;
        }

        /// <summary>
        /// ������� ���������� ��������� � ��� ������� ����� �������� �� ����������� �������
        /// </summary>
        public void FillStorageBookSemantic(IGISApplication GisApp, string aLayer, string mapObjectId)
        {
            GisApp.SetFieldValue(aLayer, mapObjectId, "�����������_�����", Code);

            string dzunoms = "";
            foreach (IsogdPhysStorageBook dzu in PhysStorageBooks)
            {
                dzunoms = dzu.BookNo + ", ";
            }
            dzunoms = dzunoms.Trim().TrimEnd(',');

            GisApp.SetFieldValue(aLayer, mapObjectId, "�����_����", dzunoms);
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_����_�����", OpenenigBookDate.ToShortDateString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "����������", Notes);

            // ����� �� ����� "����� �������� ���"
            GisApp.SetFieldValue(aLayer, mapObjectId, "�����_��������_���", Name?.Replace('\"', '\'').Replace("�", "'").Replace("�", "'"));
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_����_�����", OpenenigBookDate.ToShortDateString());

            GisApp.CommitLayer(aLayer);
        }


    }
}