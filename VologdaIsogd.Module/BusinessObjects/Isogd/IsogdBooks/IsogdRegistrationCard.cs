using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.Enums;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{

    /// <summary>
    /// �������� ����������� ��������� �����
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "�������� ����������� ��������� �����")]
    public class IsogdRegistrationCard : BaseObjectXAF
    {
        public IsogdRegistrationCard(Session session) : base(session) { }

        //private int i_RecNo;
        private DateTime i_RecordDate;
        private Employee i_Employee;

        private const string descriptFormat = "�������� ����������� ��������� ����� � {RecNo} �� {RecordDate}";
        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("�������� ����������� ��������� ����� � {0} �� {1}", RecNo, RecordDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Association, DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        private string i_tomeNo;
        [DisplayName("����� ���� ����� �����������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("RecNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ��������")]
        public string RecNo
        {
            get { return RecNoCore; }
            set { SetPropertyValue("CardNo", ref RecNoCore, value); }
        }
        //[DisplayName("����� ��������")]
        //public int RecNo
        //{
        //    get { return i_RecNo; }
        //    set { SetPropertyValue("RecNo", ref i_RecNo, value); }
        //}

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        [DisplayName("�������� �����")]
        public IsogdDocument IsogdDocument { get; set; }

        [DisplayName("���������������� ������")]
        public IsogdLetter IsogdLetter { get; set; }

        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get
            {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality;
            }
            set { try { SetPropertyValue("Municipality", ref i_Municipality, value); } catch { } }
        }
        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        [Persistent("CardNoOKTMO")]
        private string RecNoCoreOKTMO;
        [PersistentAlias("RecNoCoreOKTMO")]
        [DisplayName("����� ������ + �����")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoOKTMO
        {
            get { return RecNoCoreOKTMO; }
        }

        [Persistent("RecNoCoreCommon")]
        private string RecNoCoreCommon;
        [PersistentAlias("RecNoCoreCommon")]
        [DisplayName("����� ������ � ������ ������")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoCommon
        {
            get { return RecNoCoreCommon; }
        }
        private bool i_isCopyFromInmeta;
        [DisplayName("��������� �� ������ �����")]
        [System.ComponentModel.Browsable(false)]
        public bool isCopyFromInmeta
        {
            get { return i_isCopyFromInmeta; }
            set { SetPropertyValue("isCopyFromInmeta", ref i_isCopyFromInmeta, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("������ � ����� ����� ��������")]
        public XPCollection<IsogdInDataCard> IsogdInDataCard
        {
            get { return GetCollection<IsogdInDataCard>("IsogdInDataCard"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            try
            {
                // �������� ����� ��������� ���� ����� ����������� �� ������� ���� �����
                if (i_tomeNo == null || i_tomeNo == "")
                {
                    //IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                    //            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.IsogdBooksKind.����������������, true)));
                    IsogdBooksCard i_IsogdBooksCard = Connect.FromSession(Session).FindFirstObject<IsogdBooksCard>(mc =>
                        mc.IsogdBooksKind == eIsogdBooksKind.���������������� &&
                       mc.ActiveTomeFlag == true && mc.Municipality == Municipality);
                    if (i_IsogdBooksCard != null)
                    {
                        if (i_IsogdBooksCard.BookTomeNo != null)
                        {
                            i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                        }
                    }
                }
            }
            catch { }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {

                try
                {
                    if (this.RecNoCore == "" || this.RecNoCore == null)
                    {
                        if (Municipality != null)
                        {
                            if (Municipality.OKTMO != null || Municipality.OKTMO != String.Empty)
                            {
                                this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + Municipality.OKTMO, string.Empty));
                                this.RecNoCoreOKTMO = Municipality.OKTMO + "_" + RecNoCore;
                            }
                        }
                    }
                }
                catch { }

                try
                {
                    if (this.RecNoCoreCommon == String.Empty || this.RecNoCoreCommon == null)
                    {
                        this.RecNoCoreCommon = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                    }
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}