using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.Enums;
using AISOGD.Subject;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
//using AISOGD.DocFlow;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{
    /// <summary>
    /// ������� ����� ������
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "������� ����� ������")]
    public class IsogdRequestCard : AttachBase
    {
        public IsogdRequestCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private Employee i_Employee;
        private eSubjectType i_requesterType;
        //private Org i_org;
        //private Subject.Person i_person;
        //private Subject.Person i_TrustSubject;
        private eServiceStatus i_ServiceStatus;
        //private ISOGDOutDataCard i_ISOGDOutDataCard;

        private const string descriptFormat = "������� ����� ������ � {CardNo} �� {RecordDate}";
        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������� ����� ������ � {0} �� {1}", CardNo, RecordDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        private string i_tomeNo;
        [Size(4), DisplayName("����� ���� ����� ����� ������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("CardNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� �������")]
        public string CardNo
        {
            get { return RecNoCore; }
            set { SetPropertyValue("CardNo", ref RecNoCore, value); }
        }
        //[Size(64), DisplayName("����� �������")]
        //public string CardNo { get; set; }

        [DisplayName("���� �������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        //[DisplayName("��� ���������")]
        //[ImmediatePostData]
        //public eSubjectType RequesterType
        //{
        //    get { return i_requesterType; }
        //    set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        //}

        //[DisplayName("��������")]
        //[Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        //public Org RequesterOrg
        //{
        //    get { return i_org; }
        //    set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        //}

        //[DisplayName("��������")]
        //[Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '��'", Context = "DetailView")]
        //public Subject.Person RequesterPerson
        //{
        //    get { return i_person; }
        //    set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        //}
        private GeneralSubject i_Requester;
        [DisplayName("��������")]
        public GeneralSubject Requester
        {
            get { return i_Requester; }
            set { SetPropertyValue("Requester", ref i_Requester, value); }
        }
        //[DisplayName("���������� ����")]
        //[Appearance("isPerson2", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        //public Subject.Person TrustSubject
        //{
        //    get { return i_TrustSubject; }
        //    set { SetPropertyValue("TrustSubject", ref i_TrustSubject, value); }
        //}
         

        [DisplayName("������ ������")]
        [ImmediatePostData]
        public eServiceStatus ServiceStatus
        {
            get { return i_ServiceStatus; }
            set
            {
                try { SetPropertyValue("ServiceStatus", ref i_ServiceStatus, value); }
                catch { }
            }
        }

        [DisplayName("�����"), Size(16)]
        [Appearance("isMoney", Visibility = ViewItemVisibility.Hide, Criteria = "ServiceStatus= '���������'", Context = "DetailView")]
        public string MoneySum { get; set; }

        [DisplayName("���������, ������������� �����������")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        //[Association, DisplayName("��������������� ������ � �������������� ��������")]
        //public ISOGDOutDataCard ISOGDOutDataCard
        //{
        //    get { return i_ISOGDOutDataCard; }
        //    set { SetPropertyValue("ISOGDOutDataCard", ref i_ISOGDOutDataCard, value); }
        //}
        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get
            {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality;
            }
            set { try { SetPropertyValue("Municipality", ref i_Municipality, value); } catch { } }
        }
        private bool i_isCopyFromInmeta;
        [DisplayName("��������� �� ������ �����")]
        [System.ComponentModel.Browsable(false)]
        public bool isCopyFromInmeta
        {
            get { return i_isCopyFromInmeta; }
            set { SetPropertyValue("isCopyFromInmeta", ref i_isCopyFromInmeta, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        [Persistent("CardNoOKTMO")]
        private string RecNoCoreOKTMO;
        [PersistentAlias("RecNoCoreOKTMO")]
        [DisplayName("����� ������ + �����")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoOKTMO
        {
            get { return RecNoCoreOKTMO; }
        }

        [Persistent("RecNoCoreCommon")]
        private string RecNoCoreCommon;
        [PersistentAlias("RecNoCoreCommon")]
        [DisplayName("����� ������ � ������ ������")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoCommon
        {
            get { return RecNoCoreCommon; }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("������������� ��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

        [Association, DisplayName("��������������� ������ � �������������� ��������")]
        public XPCollection<IsogdOutDataCard> IsogdOutDataCard
        {
            get { return GetCollection<IsogdOutDataCard>("IsogdOutDataCard"); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            try
            {
                // �������� ����� ��������� ���� ����� ����� ������ �� ������� ���� �����
                if (i_tomeNo == null || i_tomeNo == "")
                {
                    //IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                    //            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.IsogdBooksKind.����������������, true)));
                    IsogdBooksCard i_IsogdBooksCard = Connect.FromSession(Session).FindFirstObject<IsogdBooksCard>(mc =>
                        mc.IsogdBooksKind == eIsogdBooksKind.���������������� &&
                       mc.ActiveTomeFlag == true && mc.Municipality == Municipality);
                    if (i_IsogdBooksCard != null)
                    {
                        if (i_IsogdBooksCard.BookTomeNo != null)
                        {
                            i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                        }
                    }
                }
            }
            catch { }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {

                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                    {
                        if (Municipality != null)
                        {
                            if (Municipality.OKTMO != null || Municipality.OKTMO != String.Empty)
                            {
                                this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + Municipality.OKTMO, string.Empty));
                                this.RecNoCoreOKTMO = Municipality.OKTMO + "_" + RecNoCore;
                            }
                        }
                    }
                }
                catch { }

                try
                {
                    if (this.RecNoCoreCommon == String.Empty || this.RecNoCoreCommon == null)
                        this.RecNoCoreCommon = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}