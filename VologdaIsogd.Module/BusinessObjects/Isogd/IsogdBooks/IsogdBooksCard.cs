using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.Enums;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.XtraEditors;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{

    /// <summary>
    /// ������ ������� ���� �����
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "������ ������� ���� �����")]
    public class IsogdBooksCard : BaseObjectXAF
    {
        public IsogdBooksCard(Session session) : base(session) { }

        //private int i_RecNo;
        private DateTime i_RecordDate;
        private DateTime i_OpenenigTomeDate;
        private DateTime i_ClosingTomeDate;
        private Employee i_Employee;
        private bool i_ActiveTomeFlag;

        //[DisplayName("����� ���� ������� ����")]
        //public int TomeNo { get; set; }
        private const string descriptFormat = "������ ������� ���� ����� � {RecNo} �� {RecordDate}";
        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������ ������� ���� ����� � {0} �� {1}", RecNo, RecordDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        [Persistent("RecNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ������")]
        public string RecNo
        {
            get { return RecNoCore; }
            set { SetPropertyValue("RecNo", ref RecNoCore, value); }
        }

        //[DisplayName("����� ������")]
        //public int RecNo
        //{
        //    get { return i_RecNo; }
        //    set { SetPropertyValue("RecNo", ref i_RecNo, value); }
        //}

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        private eIsogdBooksKind i_IsogdBooksKind;
        [DisplayName("��� �����")]
        [ImmediatePostData]
        public eIsogdBooksKind IsogdBooksKind
        {
            get { return i_IsogdBooksKind; }
            set { 
                SetPropertyValue("IsogdBooksKind", ref i_IsogdBooksKind, value);
            if (i_BookTomeNo == null || i_BookTomeNo == "")
                i_BookTomeNo = SetBookTomeNo();
            }
        }

        private string i_BookTomeNo;
        //[PersistentAlias("i_BookTomeNo")]
        [DisplayName("����� ���� �����")]
        public string BookTomeNo
        {
            get
            {
                
                return i_BookTomeNo;
            }
            set { SetPropertyValue("BookTomeNo", ref i_BookTomeNo, value); }
        }

        // ���������� ����� ���� �����
        private string SetBookTomeNo()
        {
            string res = "";
            //XtraMessageBox.Show(IsogdBooksKind.ToString());
            try
            {
                if (IsogdBooksKind != Enums.eIsogdBooksKind.�������������)
                {
                    //IsogdBooksCard ibc = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind)));
                    IsogdBooksCard ibc = Connect.FromSession(Session).FindFirstObject<IsogdBooksCard>(mc =>
                        mc.IsogdBooksKind == IsogdBooksKind &&
                       mc.ActiveTomeFlag == true && mc.Municipality == Municipality);
                    if (ibc != null)
                    {
                        if (ibc != this)
                        {
                            res = (Convert.ToInt16(ibc.BookTomeNo) + 1).ToString();
                            ibc.ClosingTomeDate = DateTime.Now.Date;
                            ibc.NextBookTomeNo = res;
                            ibc.ActiveTomeFlag = false;
                            this.ActiveTomeFlag = true;
                            ibc.Save();
                        }
                    }
                    else
                    {
                        res = "1";
                        this.ActiveTomeFlag = true;
                    }
                }
                // ��� ����� �������� ���� ��-�������
                else
                {
                    if (IsogdStorageBook != null)
                    {
                        //IsogdBooksCard ibc = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdStorageBook.Oid = '{0}' and ActiveTomeFlag= '{1}'", IsogdStorageBook.Oid, true)));
                        IsogdBooksCard ibc = Connect.FromSession(Session).FindFirstObject<IsogdBooksCard>(mc =>
                        mc.IsogdStorageBook == IsogdStorageBook &&
                       mc.ActiveTomeFlag == true && mc.Municipality == Municipality);
                        if (ibc != null)
                        {
                            if (ibc != this)
                            {
                                res = (Convert.ToInt16(ibc.BookTomeNo) + 1).ToString();
                                ibc.ClosingTomeDate = DateTime.Now.Date;
                                ibc.NextBookTomeNo = res;
                                ibc.ActiveTomeFlag = false;
                                this.ActiveTomeFlag = true;
                                ibc.Save();
                            }
                        }
                        else
                        {
                            res = "1";
                            this.ActiveTomeFlag = true;
                        }
                    }
                    else
                    {
                        this.Notes = IsogdBooksKind.ToString();
                    }
                }
            }
            catch { }
            //res = (Session.GetObjects(Session.GetClassInfo<IsogdBooksCard>(),
            //    CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind)), null, 0, false, false).Count + 1).ToString();
            return res;
        }

        //private string i_StoreBookNo;
        //[DisplayName("��� ����� ��������")]
        //[Appearance("isStorBook", Visibility = ViewItemVisibility.Show, Criteria = "IsogdBooksKind= '�������������'", Context = "DetailView")]
        //public string StoreBookNo
        //{
        //    get { return i_StoreBookNo; }
        //    set { SetPropertyValue("StoreBookNo", ref i_StoreBookNo, value); }
        //}

        private IsogdStorageBook i_StoreBook;
        [DisplayName("����� ��������")]
        public IsogdStorageBook IsogdStorageBook
        {
            get { return i_StoreBook; }
            set { SetPropertyValue("IsogdStorageBook", ref i_StoreBook, value); }
        }

        [DisplayName("�������� ���")]
        public bool ActiveTomeFlag
        {
            get { return i_ActiveTomeFlag; }
            set { SetPropertyValue("ActiveTomeFlag", ref i_ActiveTomeFlag, value); }
        }
        [DisplayName("���� ������ ������� ���� �����")]
        public DateTime OpenenigTomeDate
        {
            get { return i_OpenenigTomeDate; }
            set { SetPropertyValue("OpenenigTomeDate", ref i_OpenenigTomeDate, value); }
        }
        [DisplayName("���� �������� ����")]
        public DateTime ClosingTomeDate
        {
            get { return i_ClosingTomeDate; }
            set { SetPropertyValue("ClosingTomeDate", ref i_ClosingTomeDate, value); }
        }
        [DisplayName("����� ���� ����� ����������� �� �������")]
        public string NextBookTomeNo { get; set; }

        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get
            {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality;
            }
            set { try { SetPropertyValue("Municipality", ref i_Municipality, value); } catch { } }
        }
        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        private bool i_isCopyFromInmeta;
        [DisplayName("���������� �� ������ �����")]
        [System.ComponentModel.Browsable(false)]
        public bool isCopyFromInmeta
        {
            get { return i_isCopyFromInmeta; }
            set { SetPropertyValue("isCopyFromInmeta", ref i_isCopyFromInmeta, value); }
        }

        [Persistent("CardNoOKTMO")]
        private string RecNoCoreOKTMO;
        [PersistentAlias("RecNoCoreOKTMO")]
        [DisplayName("����� ������ + �����")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoOKTMO
        {
            get { return RecNoCoreOKTMO; }
        }

        [Persistent("RecNoCoreCommon")]
        private string RecNoCoreCommon;
        [PersistentAlias("RecNoCoreCommon")]
        [DisplayName("����� ������ � ������ ������")]
        [System.ComponentModel.Browsable(false)]
        public string CardNoCommon
        {
            get { return RecNoCoreCommon; }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

      
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            if (i_OpenenigTomeDate == DateTime.MinValue)
            {
                i_OpenenigTomeDate = DateTime.Now.Date;
            }
            
        }

        protected override void OnSaving()
        {
            if (!IsDeleted)
            {

                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                    {
                        if (Municipality != null)
                        {
                            if (Municipality.OKTMO != null || Municipality.OKTMO != String.Empty)
                            {
                                this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName + Municipality.OKTMO, string.Empty));
                                this.RecNoCoreOKTMO = Municipality.OKTMO + "_" + RecNoCore;
                            }
                        }
                    }
                }
                catch { }

                try
                {
                    if (this.RecNoCoreCommon == String.Empty || this.RecNoCoreCommon == null)
                        this.RecNoCoreCommon = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
                try
                {
                    if (this.i_BookTomeNo == null || this.i_BookTomeNo == String.Empty)
                        this.i_BookTomeNo = SetBookTomeNo();
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}