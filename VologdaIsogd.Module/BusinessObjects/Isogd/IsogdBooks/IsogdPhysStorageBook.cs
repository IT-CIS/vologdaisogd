using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.Land;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.XtraEditors;
using AISOGD.Constr;
using AISOGD.Surveys;
using AISOGD.Subject;
using AISOGD.Enums;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;

namespace AISOGD.Isogd
{
    /// <summary>
    /// ����� ����������� �������� �������� �����
    /// </summary>
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("Name")]
    [ModelDefault("Caption", "��� - ���� ����������� �������� ����������")]
    public class IsogdPhysStorageBook : BaseObjectXAF
    {
        public IsogdPhysStorageBook(Session session) : base(session) { }

        private string i_name;

        [Size(128), DisplayName("����� �����")]
        [ImmediatePostData]
        public string BookNo { get; set; }

        [Size(255), DisplayName("������������ ����� ��������")]
        public string Name
        {
            get
            {
                i_name = "���";
                if (BookNo != null || BookNo != "")
                    i_name += " � " + BookNo;
                return i_name;
            }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        private DateTime i_OpenenigBookDate;
        [DisplayName("���� ������ �������")]
        public DateTime OpenenigBookDate
        {
            get { return i_OpenenigBookDate; }
            set
            {
                SetPropertyValue("OpenenigBookDate", ref i_OpenenigBookDate, value);
                i_Year = i_OpenenigBookDate.Year;
                i_eMonth = (eMonths)Enum.Parse(typeof(eMonths), (i_OpenenigBookDate.Month - 1).ToString());
                OnChanged("OpenenigBookDate");
            }
        }
        private eMonths i_eMonth;
        [DisplayName("�����")]
        public eMonths eMonth
        {
            get { return i_eMonth; }
            set { SetPropertyValue("eMonth", ref i_eMonth, value); }
        }

        private int i_Year;
        [DisplayName("���")]
        public int Year
        {
            get
            {
                return i_Year;
            }
            set { SetPropertyValue("Year", ref i_Year, value); }
        }

        private string i_CadNumbers;
        [Size(255), DisplayName("����������� ������")]
        public string CadNumbers
        {
            get { return i_CadNumbers; }
            set { SetPropertyValue("CadNumbers", ref i_CadNumbers, value); }
        }
        private string i_ObjectInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("�������� �������")]
        public string ObjectInfo
        {
            get { return i_ObjectInfo; }
            set { SetPropertyValue("ObjectInfo", ref i_ObjectInfo, value); }
        }
        private string i_AddressInfo;
        [Size(1024), DisplayName("��������������(�����)")]
        public string AddressInfo
        {
            get { return i_AddressInfo; }
            set { SetPropertyValue("AddressInfo", ref i_AddressInfo, value); }
        }
        private string i_SubjectInfo;
        [Size(4000), DisplayName("����������")]
        public string SubjectInfo
        {
            get { return i_SubjectInfo; }
            set { SetPropertyValue("SubjectInfo", ref i_SubjectInfo, value); }
        }
        private string i_ReserveNo;
        [Size(SizeAttribute.Unlimited), DisplayName("���� ������")]
        public string ReserveNo
        {
            get { return i_ReserveNo; }
            set { SetPropertyValue("ReserveNo", ref i_ReserveNo, value); }
        }
        private string i_ConstrNo;
        [Size(SizeAttribute.Unlimited), DisplayName("���� � ���� �������������")]
        public string ConstrNo
        {
            get { return i_ConstrNo; }
            set { SetPropertyValue("ConstrNo", ref i_ConstrNo, value); }
        }
        private string i_SurveyNo;
        [Size(SizeAttribute.Unlimited), DisplayName("���� � ���� ���������")]
        public string SurveyNo
        {
            get { return i_SurveyNo; }
            set { SetPropertyValue("SurveyNo", ref i_SurveyNo, value); }
        }
        private string i_ProjectNo;
        [Size(SizeAttribute.Unlimited), DisplayName("����� �������")]
        public string ProjectNo
        {
            get { return i_ProjectNo; }
            set { SetPropertyValue("ProjectNo", ref i_ProjectNo, value); }
        }

        private string i_APZInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("��������� ���")]
        public string APZInfo
        {
            get { return i_APZInfo; }
            set { SetPropertyValue("APZInfo", ref i_APZInfo, value); }
        }
        private string i_GPZUNo;
        [Size(SizeAttribute.Unlimited), DisplayName("��������� ����������")]
        public string GPZUNo
        {
            get { return i_GPZUNo; }
            set { SetPropertyValue("GPZUNo", ref i_GPZUNo, value); }
        }
        private string i_ConstrPermInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("��������� ���������� �� �������������")]
        public string ConstrPermInfo
        {
            get { return i_ConstrPermInfo; }
            set { SetPropertyValue("ConstrPermInfo", ref i_ConstrPermInfo, value); }
        }

        private string i_ReserveInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("��������� ������ �� ��� �������������")]
        public string ReserveInfo
        {
            get { return i_ReserveInfo; }
            set { SetPropertyValue("ReserveInfo", ref i_ReserveInfo, value); }
        }
        private string i_UsePermInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("��������� ���������� �� ����")]
        public string UsePermInfo
        {
            get { return i_UsePermInfo; }
            set { SetPropertyValue("UsePermInfo", ref i_UsePermInfo, value); }
        }
        private string i_UseActInfo;
        [Size(SizeAttribute.Unlimited), DisplayName("��������� ���� �����")]
        public string UseActInfo
        {
            get { return i_UseActInfo; }
            set { SetPropertyValue("UseActInfo", ref i_UseActInfo, value); }
        }

        private int? fDocsCount = null;
        [DisplayName("���������� ���������� ����� � �����")]
        public int? DocsCount
        {
            get
            {
                if (!IsLoading && !IsSaving && fDocsCount == null)
                    UpdateDocsCount(false);
                return fDocsCount;
            }
        }
        public void UpdateDocsCount(bool forceChangeEvents)
        {
            int? oldDocsCount = fDocsCount;
            fDocsCount = Convert.ToInt32(Evaluate(CriteriaOperator.Parse("IsogdDocuments.Count")));
            if (forceChangeEvents)
                OnChanged("OrdersCount", oldDocsCount, fDocsCount);
        }
        #region /// ��������� �������� � �����

        private DateTime i_InvDate;
        [DisplayName("���� ��������������")]
        public DateTime InvDate
        {
            get { return i_InvDate; }
            set { SetPropertyValue("InvDate", ref i_InvDate, value); }
        }
        private string i_ZCProtocolInfo;
        [Size(1024), DisplayName("�������� ��������� �������")]
        public string ZCProtocolInfo
        {
            get { return i_ZCProtocolInfo; }
            set { SetPropertyValue("ZCProtocolInfo", ref i_ZCProtocolInfo, value); }
        }
        private string i_ZUActInfo;
        [Size(1024), DisplayName("��� ������ ��")]
        public string ZUActInfo
        {
            get { return i_ZUActInfo; }
            set { SetPropertyValue("ZUActInfo", ref i_ZUActInfo, value); }
        }
        private string i_PredSoglZUtInfo;
        [Size(1024), DisplayName("��������������� ������������ ��")]
        public string PredSoglZUtInfo
        {
            get { return i_PredSoglZUtInfo; }
            set { SetPropertyValue("PredSoglZUtInfo", ref i_PredSoglZUtInfo, value); }
        }
        private string i_ZUBounsProjInfo;
        [Size(1024), DisplayName("������ ������ ��")]
        public string ZUBounsProjInfo
        {
            get { return i_ZUBounsProjInfo; }
            set { SetPropertyValue("ZUBounsProjInfo", ref i_ZUBounsProjInfo, value); }
        }
        private string i_ZUSchemeStateInfo;
        [Size(1024), DisplayName("����������� ����� �� � ��������")]
        public string ZUSchemeStateInfo
        {
            get { return i_ZUSchemeStateInfo; }
            set { SetPropertyValue("ZUSchemeStateInfo", ref i_ZUSchemeStateInfo, value); }
        }
        private string i_ZUProjConstrInfo;
        [Size(1024), DisplayName("��������� ��� �������������� � �������������")]
        public string ZUProjConstrInfo
        {
            get { return i_ZUProjConstrInfo; }
            set { SetPropertyValue("ZUProjConstrInfo", ref i_ZUProjConstrInfo, value); }
        }
        private string i_DZOVOInfo;
        [Size(1024), DisplayName(" ��������� ��� ��")]
        public string DZOVOInfo
        {
            get { return i_DZOVOInfo; }
            set { SetPropertyValue("DZOVOInfo", ref i_DZOVOInfo, value); }
        }
        private string i_ReglConfirmInfo;
        [Size(1024), DisplayName("��������, �����. ������������ �� ����������� �������������� � ��")]
        public string ReglConfirmInfo
        {
            get { return i_ReglConfirmInfo; }
            set { SetPropertyValue("ReglConfirmInfo", ref i_ReglConfirmInfo, value); }
        }
        private string i_ReportCodeInfo;
        [Size(1024), DisplayName("��� ������")]
        public string ReportCodeInfo
        {
            get { return i_ReportCodeInfo; }
            set { SetPropertyValue("ReportCodeInfo", ref i_ReportCodeInfo, value); }
        }
        
        private string i_NoChangeInfo;
        [Size(128), DisplayName("����� ��������� ��")]
        public string NoChangeInfo
        {
            get { return i_NoChangeInfo; }
            set { SetPropertyValue("NoChangeInfo", ref i_NoChangeInfo, value); }
        }
        #endregion

        private string i_InvDateString;
        [Size(255), DisplayName("���� ��������������")]
        public string InvDateString
        {
            get { return i_InvDateString; }
            set { SetPropertyValue("InvDateString", ref i_InvDateString, value); }
        }
        
        //private CapitalStructureBase i_CapitalStructureBase;
        //[DisplayName("������ �������������")]
        //public CapitalStructureBase CapitalStructureBase
        //{
        //    get { return i_CapitalStructureBase; }
        //    set { SetPropertyValue("CapitalStructureBase", ref i_CapitalStructureBase, value);
        //        if (i_CapitalStructureBase != null)
        //        {
        //            i_CapitalStructureBase.IsogdPhysStorageBook = this;
        //        }
        //        OnChanged();
        //    }
        //}

        private Employee i_Employee;
        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get
            {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality;
            }
            set { try { SetPropertyValue("Municipality", ref i_Municipality, value); } catch { } }
        }
        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            Connect connect = Connect.FromSession(Session);
            try { mun = connect.FindFirstObject<Municipality>(x => x.OKTMO == "19701000"); }
            catch { }
            if (mun == null)
                if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }


        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        
        [Size(SizeAttribute.Unlimited), DisplayName("�������������� ��������")]
        public string AdditionalNotes { get; set; }

        [Association, DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocuments
        {
            get { return GetCollection<IsogdDocument>("IsogdDocuments"); }
        }

        [Association, DisplayName("��������� �������, ��������� � ������")]
        public XPCollection<Parcel> Parcels
        {
            get { return GetCollection<Parcel>("Parcels"); }
        }
        [Association, DisplayName("����� ��������")]
        public XPCollection<IsogdStorageBook> IsogdStorageBooks
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBooks"); }
        }
        [Association, DisplayName("������ �������������")]
        public XPCollection<CapitalStructureBase> CapitalStructureBase
        {
            get { return GetCollection<CapitalStructureBase>("CapitalStructureBase"); }
        }
        [Association, DisplayName("�����������")]
        public XPCollection<GeneralSubject> ConstrDevelopers
        {
            get { return GetCollection<GeneralSubject>("ConstrDevelopers"); }
        }

        [Association, DisplayName("���������")]
        public XPCollection<Survey> Surveys
        {
            get { return GetCollection<Survey>("Surveys"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.

            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
            }
            if (OpenenigBookDate == DateTime.MinValue)
                OpenenigBookDate = DateTime.Now.Date;
        }

        private void SetUpdateDates()
        {
            SortProperty sort = new SortProperty("PlacementDate", DevExpress.Xpo.DB.SortingDirection.Ascending);
            IsogdDocuments.Sorting.Add(sort);
            foreach (IsogdDocument doc in IsogdDocuments)
            {
                if(InvDateString == null || InvDateString == "")  
                    InvDateString = doc.PlacementDate.ToShortDateString();
                else
                    if(!InvDateString.Contains(doc.PlacementDate.ToShortDateString()))
                        if ((InvDateString + ", " + doc.PlacementDate.ToShortDateString()).Length < 255)
                            InvDateString += ", " + doc.PlacementDate.ToShortDateString();
            }
        }
        /// <summary>
        /// ����������� ���� ���������� (string) �� ����� ������������ (XPCollection)
        /// </summary>
        private void SetDevelopers()
        {
            string dev = "";
            foreach (GeneralSubject subj in ConstrDevelopers)
            {
                if (dev == "")
                    dev = subj.FullName;
                else
                    if (!dev.Contains(subj.FullName))
                    dev += ", " + subj.FullName;
            }
            if (dev.Length > 1024)
                dev = dev.Remove(1023, dev.Length);
            SubjectInfo = dev;
        }
        /// <summary>
        /// ����������� ���� ���������� (string) �� ����� ������������ (XPCollection)
        /// </summary>
        private void SetCapitalStructure()
        {
            string res = "";
            foreach (CapitalStructureBase obj in CapitalStructureBase)
            {
                if (res == "")
                    res = obj.Name;
                else
                    if (!res.Contains(obj.Name))
                    res += ", " + obj.Name;
            }
            if (res.Length > 1024)
                res = res.Remove(1023, res.Length);
            ObjectInfo = res;
        }

        #region ���������� ������ ��� ��������� ��������� ��, ���� ��������, ����������
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ��� �������� ������ �� �������� � ����� �������� � ��
        /// </summary>
        private void IsogdDocuments_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ��������
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdDocument newIsogdDocument = (IsogdDocument)e.ChangedObject;
                    string info = "";
                    info = String.Format("� {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                    // ������� ����� ���������, � ���� �� ������������� ��������, ��������� ������ � ��� �� ���������� ���������
                    switch (newIsogdDocument.IsogdDocumentClass.Code)
                    {
                        case "062000":
                            {
                                try
                                {
                                    if (APZInfo == null || APZInfo == "")
                                    {
                                        if (info.Length < 255)
                                            APZInfo = info;
                                    }
                                    else
                                        if ((APZInfo + ", " + info).Length < 255)
                                            APZInfo = APZInfo + ", " + info;
                                }
                                catch { }
                            }
                            break;
                        case "060100":
                            {
                                try
                                {
                                    if (GPZUNo == null || GPZUNo == "")
                                    {
                                        if (info.Length < 255)
                                            GPZUNo = info;
                                    }
                                    else
                                      if ((GPZUNo + ", " + info).Length < 255)
                                        GPZUNo = GPZUNo + ", " + info;
                                    //GPZUNo = String.Format("� {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                    //else
                                    //    GPZUNo += String.Format(", � {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                }
                                catch { }
                            }
                            break;
                        case "060600":
                            {
                                try
                                {
                                    if (ConstrPermInfo == null || ConstrPermInfo == "")
                                    {
                                        if (info.Length < 255)
                                            ConstrPermInfo = info;
                                    }
                                    else
                                        if ((ConstrPermInfo + ", " + info).Length < 255)
                                            ConstrPermInfo = ConstrPermInfo + ", " + info;
                                    //ConstrPermInfo = String.Format("� {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                    //else
                                    //    ConstrPermInfo += String.Format(", � {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                }
                                catch { }
                            }
                            break;
                        case "060601":
                            {
                                try
                                {
                                    if (ReserveInfo == null || ReserveInfo == "")
                                    {
                                        if (info.Length < 255)
                                            ReserveInfo = info;
                                    }
                                    else
                                       if ((ReserveInfo + ", " + info).Length < 255)
                                            ReserveInfo = ReserveInfo + ", " + info;
                                    //ReserveInfo = String.Format("� {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                    //else
                                    //    ReserveInfo += String.Format(", � {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                }
                                catch { }
                            }
                            break;
                        case "061100":
                            {
                                try
                                {
                                    if (UsePermInfo == null || UsePermInfo == "")
                                    {
                                        if (info.Length < 255)
                                            UsePermInfo = info;
                                    }
                                    else
                                        if ((UsePermInfo + ", " + info).Length < 255)
                                            UsePermInfo = UsePermInfo + ", " + info;
                                    //UsePermInfo = String.Format("� {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                    //else
                                    //    UsePermInfo += String.Format(", � {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                }
                                catch { }
                            }
                            break;
                        case "061000":
                            {
                                try
                                {
                                    if (UseActInfo == null || UseActInfo == "")
                                    {
                                        if (info.Length < 255)
                                            UseActInfo = info;
                                    }
                                    else
                                      if ((UseActInfo + ", " + info).Length < 255)
                                            UseActInfo = UseActInfo + ", " + info;
                                    //if (UseActInfo == null && APZInfo == "")
                                    //    UseActInfo = String.Format("� {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                    //else
                                    //    UseActInfo += String.Format(", � {0} �� {1}", newIsogdDocument.DocNo, newIsogdDocument.StatementDate.ToShortDateString());
                                }
                                catch { }
                            }
                            break;
                    }
                    this.Save();
                    // ���������, ���� �� ����� �������� � �� � ���, ���� ��� - ���������
                    foreach (Parcel parcel in this.Parcels)
                    {
                        bool isExist = false;
                        foreach (IsogdDocument isogdDocument in parcel.IsogdDocuments)
                        {
                            if (isogdDocument == newIsogdDocument)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            parcel.IsogdDocuments.Add(newIsogdDocument);
                            parcel.Save();
                        }
                    }
                    //foreach (IsogdStorageBook isogdStorageBook in this.IsogdStorageBooks)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument isogdDocument in isogdStorageBook.IsogdDocuments)
                    //    {
                    //        if (isogdDocument == newIsogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        isogdStorageBook.IsogdDocuments.Add(newIsogdDocument);
                    //        isogdStorageBook.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ��������
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdDocument newIsogdDocument = (IsogdDocument)e.ChangedObject;
                    // ���������, ���� �� ����� �������� � �� � ���, ���� ���� - �������
                    //foreach (Parcel parcel in this.Parcels)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument isogdDocument in parcel.IsogdDocuments)
                    //    {
                    //        if (isogdDocument == newIsogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        parcel.IsogdDocuments.Remove(newIsogdDocument);
                    //        parcel.Save();
                    //    }
                    //}
                    //foreach (IsogdStorageBook isogdStorageBook in this.IsogdStorageBooks)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdDocument isogdDocument in isogdStorageBook.IsogdDocuments)
                    //    {
                    //        if (isogdDocument == newIsogdDocument)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        isogdStorageBook.IsogdDocuments.Remove(newIsogdDocument);
                    //        isogdStorageBook.Save();
                    //    }
                    //}
                }
                #endregion

                SetUpdateDates();
            }
        }
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ������ �� �� � ����� �������� � ���������
        /// </summary>
        private void Parcels_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                Connect connect = Connect.FromSession(Session);
                #region ���� � ��������� �������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    Parcel newObj = (Parcel)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
                    //foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    //{
                    //    bool isExist = false;
                    //    foreach (Parcel parcel in isogdDocument.Parcels)
                    //    {
                    //        if (parcel == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        isogdDocument.Parcels.Add(newObj);
                    //        isogdDocument.Save();
                    //    }
                    //}

                    //IsogdStorageBook isogdStorageBook = connect.FindFirstObject<IsogdStorageBook>(mc => mc.Code.Trim() == newObj.CadastralNumber.Trim());
                    //if (isogdStorageBook != null)
                    //{
                    //    isogdStorageBook.Parcel = newObj;
                    //    newObj.IsogdStorageBook = isogdStorageBook;
                    //    bool isExist = false;
                    //    foreach (IsogdStorageBook isogdStorageBookThis in this.IsogdStorageBooks)
                    //    {
                    //        if (isogdStorageBookThis == isogdStorageBook)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        IsogdStorageBooks.Add(isogdStorageBook);
                    //        this.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    Parcel newObj = (Parcel)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ���� - �������
                    //foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    //{
                    //    bool isExist = false;
                    //    foreach (Parcel parcel in isogdDocument.Parcels)
                    //    {
                    //        if (parcel == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        isogdDocument.Parcels.Remove(newObj);
                    //        isogdDocument.Save();
                    //    }
                    //}
                    //IsogdStorageBook isogdStorageBook = connect.FindFirstObject<IsogdStorageBook>(mc => mc.Code.Trim() == newObj.CadastralNumber.Trim());
                    //if (isogdStorageBook != null)
                    //{
                    //    isogdStorageBook.Parcel = newObj;
                    //    newObj.IsogdStorageBook = isogdStorageBook;
                    //    bool isExist = false;
                    //    foreach (IsogdStorageBook isogdStorageBookThis in this.IsogdStorageBooks)
                    //    {
                    //        if (isogdStorageBookThis == isogdStorageBook)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        IsogdStorageBooks.Remove(isogdStorageBook);
                    //        this.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }
        ///// <summary>
        ///// ������� �������� �, ��� �������������, ���������� ������ �� ����� �������� c �� � ���������
        ///// </summary>
        private void IsogdStorageBooks_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                Connect connect = Connect.FromSession(Session);
                #region ���� � ��������� �������� �� ��������� ��� 
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdStorageBook newObj = (IsogdStorageBook)e.ChangedObject;
                    // ��������� ��� ��������� �� ����� ����� ��������, ������� ��������� �� ����� �� (����� ��� ������������ ���������) � ���
                    //foreach (IsogdDocument isogdDocument in newObj.IsogdDocuments)
                    //{
                    //    try
                    //    {
                    //        if (isogdDocument.IsogdDocumentNameKind.LandFlag)
                    //        {
                    //            // ���������, ���� �� ��� ����� �������� � ������� ���
                    //            bool isExist = false;
                    //            foreach (IsogdDocument thisDoc in this.IsogdDocuments)
                    //            {
                    //                if (thisDoc == isogdDocument)
                    //                {
                    //                    isExist = true;
                    //                    break;
                    //                }
                    //            }
                    //            if (!isExist)
                    //            {
                    //                this.IsogdDocuments.Add(isogdDocument);
                    //                this.Save();
                    //            }
                    //        }
                                
                    //    } catch { }
                    //}
                    //Parcel parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber.Trim() == newObj.Code.Trim());
                    //if (parcel != null)
                    //{
                    //    newObj.Parcel = parcel;
                    //    parcel.IsogdStorageBook = newObj;
                    //    bool isExist = false;
                    //    foreach (Parcel parcelThis in this.Parcels)
                    //    {
                    //        if (parcelThis == parcel)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        Parcels.Add(parcel);
                    //        this.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdStorageBook newObj = (IsogdStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
                    //foreach (IsogdDocument isogdDocument in this.IsogdDocuments)
                    //{
                    //    bool isExist = false;
                    //    foreach (IsogdStorageBook isogdStorageBook in isogdDocument.IsogdStorageBooks)
                    //    {
                    //        if (isogdStorageBook == newObj)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        isogdDocument.IsogdStorageBooks.Remove(newObj);
                    //        isogdDocument.Save();
                    //    }
                    //}
                    //Parcel parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber.Trim() == newObj.Code.Trim());
                    //if (parcel != null)
                    //{
                    //    newObj.Parcel = parcel;
                    //    parcel.IsogdStorageBook = newObj;
                    //    bool isExist = false;
                    //    foreach (Parcel parcelThis in this.Parcels)
                    //    {
                    //        if (parcelThis == parcel)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        Parcels.Remove(parcel);
                    //        this.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }
        ///// <summary>
        ///// �������, ������������� ��� ��������� ��������� ������������ - ���������� ���� �� ������������
        ///// </summary>
        private void ConstrDevelopers_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetDevelopers();
            }
        }
        ///// <summary>
        ///// �������, ������������� ��� ��������� ��������� �������� ������������� - ���������� ���� ������� �������������
        ///// </summary>
        private void CapitalStructureBase_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                SetCapitalStructure();
            }
        }
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //���������� ��������� ���������� �����
            if (property.Name == "IsogdDocuments")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(IsogdDocuments_CollectionChanged);
            }
            ////���������� ��������� ��������� ��������
            //if (property.Name == "Parcels")
            //{
            //    result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            //}
            //���������� ��������� ���
            if (property.Name == "IsogdStorageBooks")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(IsogdStorageBooks_CollectionChanged);
            }
            if (property.Name == "ConstrDevelopers")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(ConstrDevelopers_CollectionChanged);
            }
            if (property.Name == "CapitalStructureBase")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(CapitalStructureBase_CollectionChanged);
            }
            return result;
        }
        #endregion


        /// <summary>
        /// ������� ���������� ��������� � ��� ������� ��� �������� �� ����������� �������
        /// </summary>
        public void FillPhysStorageBookSemantic(IGISApplication GisApp, string aLayer, string mapObjectId)
        {
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_�_��", BookNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "��_��", CadNumbers);
            GisApp.SetFieldValue(aLayer, mapObjectId, "��������������", AddressInfo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "����������", SubjectInfo?.Replace('\"', '\'').Replace("�", "'").Replace("�", "'"));
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_������", ReserveNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "�������", ProjectNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_�_����_���", ConstrNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "���������", GPZUNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "���������", SurveyNo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "����_����_�_����", OpenenigBookDate.ToShortDateString());
            GisApp.SetFieldValue(aLayer, mapObjectId, "���_������", ObjectInfo);
            GisApp.SetFieldValue(aLayer, mapObjectId, "����������", Notes);
            GisApp.CommitLayer(aLayer);
        }
    }
}