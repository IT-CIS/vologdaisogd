using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using AISOGD.Subject;
using AISOGD.General;
using DevExpress.ExpressApp.Model;
using AISOGD.SystemDir;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
//using AISOGD.DocFlow;

namespace AISOGD.Isogd
{
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("descript")]
    [ModelDefault("Caption", "�������� ���������� �����")]
    public class IsogdAbstractRequest : BaseObjectXAF
    {
        public IsogdAbstractRequest(Session session) : base(session) { }


        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("�������� ���������� ����� �� {0}", RegDate.ToShortDateString());
                //return ObjectFormatter.Format(descriptFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }

        private DateTime i_RegDate;
        [DisplayName("���� �������")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        private Employee i_Employee;
        [DisplayName("�����������")]
        //[ImmediatePostData]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        private bool i_AttachFlag;
        [DisplayName("���������� ��.����� ����������")]
        public bool AttachFlag
        {
            get { return i_AttachFlag; }
            set { SetPropertyValue("AttachFlag", ref i_AttachFlag, value); }
        }

        [Size(1024), DisplayName("��������")]
        public string Description { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        private DateTime i_DocRegDateFrom;
        [DisplayName("���� ����������� ���������� �")]
        public DateTime DocRegDateFrom
        {
            get { return i_DocRegDateFrom; }
            set { SetPropertyValue("DocRegDateFrom", ref i_DocRegDateFrom, value); }
        }
        private DateTime i_DocRegDateTill;
        [DisplayName("���� ����������� ���������� ��")]
        public DateTime DocRegDateTill
        {
            get { return i_DocRegDateTill; }
            set { SetPropertyValue("DocRegDateTill", ref i_DocRegDateTill, value); }
        }

        [Association, DisplayName("������ ���������� ��� �������")]
        public XPCollection<IsogdDocumentClass> IsogdDocumentClasses
        {
            get { return GetCollection<IsogdDocumentClass>("IsogdDocumentClasses"); }
        }

        [Association, DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocuments
        {
            get { return GetCollection<IsogdDocument>("IsogdDocuments"); }
        }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (i_Employee == null)
            {
                i_Employee = Employee.GetCurrentEmployee();
                //if (SecuritySystem.CurrentUser != null)
                //{
                //    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                //    if (currentEmpl != null)
                //        i_Employee = currentEmpl;
                //}
            }
            i_RegDate = DateTime.Now.Date;
            i_AttachFlag = true;
            // Place here your initialization code.
        }
    }
}