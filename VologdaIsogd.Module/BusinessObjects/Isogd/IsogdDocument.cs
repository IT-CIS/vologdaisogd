using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using AISOGD.Land;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Address;

namespace AISOGD.Isogd
{
    [NavigationItem("������� � ��������� �����")]
    [ModelDefault("Caption", "�������� �����"), System.ComponentModel.DefaultProperty("FullName")]
    public class IsogdDocument : AttachBase
    {
        public IsogdDocument(Session session) : base(session) { }

        private string i_docName;
        private string i_regNo;
        private dIsogdDocumentNameKind i_docNameKind;
        private DateTime i_placementDate;
        private Employee i_Employee;
        private eDocumentStatus i_DocumentStatus;
        private eSecretLevel i_SecretLevel;

        //[System.ComponentModel.Browsable(false)]
        [Size(SizeAttribute.Unlimited),DisplayName("�������� �����")]
        public string FullName
        {
            get
            {
                return String.Format("���.� {0}, ���� ���������� {1},"
            + " ������������: {2}", RegNo, PlacementDate.ToShortDateString(), DocName);
                //return ObjectFormatter.Format(FullNameFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        [ImmediatePostData]
        public Municipality Municipality
        {
            get {
                if (i_Municipality == null)
                {
                    try { i_Municipality = GetMunicipality(); }
                    catch { }
                }
                return i_Municipality; }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            Connect connect = Connect.FromSession(Session);
            try { mun = connect.FindFirstObject<Municipality>(x => x.OKTMO == "19701000"); }
            catch { }
            if(mun == null)
                if (Empl != null)
                    if (Empl.Departament != null)
                        if (Empl.Departament.MainDepartment != null)
                            if (Empl.Departament.MainDepartment.Municipality != null)
                                mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }
        private IsogdDocumentClass i_IsogdDocumentClass;
        [DisplayName("����� ��������� �����")]
        [ImmediatePostData]
        public IsogdDocumentClass IsogdDocumentClass
        {
            get { return i_IsogdDocumentClass; }
            set { SetPropertyValue("IsogdDocumentClass", ref i_IsogdDocumentClass, value);
                RefreshAvailableDocNames();
                try
                {
                    GetDocSubjects();
                }
                catch { }
                OnChanged("IsogdDocumentClass");
            }
        }

        private IsogdPartition i_IsogdPartition;
        [Association, DisplayName("������ �����")]
        [ImmediatePostData]
        public IsogdPartition IsogdPartition
        {
            get {
                if (IsogdDocumentClass != null)
                    if (IsogdDocumentClass.IsogdPartition != null)
                        i_IsogdPartition = IsogdDocumentClass.IsogdPartition;
                return i_IsogdPartition; }
            set { SetPropertyValue("IsogdPartition", ref i_IsogdPartition, value); }
        }

        [Size(255), DisplayName("��������������� ����� ���������")]
        public string RegNo
        {
            get {
                return i_regNo; }
            set { SetPropertyValue("RegNo", ref i_regNo, value); }
            
        }
        public string GetRegNo()
        {
            string res = "";
            if (Municipality != null)
                if (Municipality.OKTMO != null)
                    res = Municipality.OKTMO;
            if (IsogdPartition != null)
            {
                if (IsogdPartition.PartNo != null)
                {
                    if (IsogdPartition.PartNo.Length == 1)
                        res += "0" + IsogdPartition.PartNo;
                    if (IsogdPartition.PartNo.Length > 1)
                        res += IsogdPartition.PartNo;
                    
                }
                string count = Convert.ToString(IsogdPartition.IsogdDocument.Count + 1);
                UnitOfWork unitOfWork = (UnitOfWork)this.Session;
                AISOGD.SystemDir.Connect connect = AISOGD.SystemDir.Connect.FromUnitOfWork(unitOfWork);
                count = (connect.FindObjects<IsogdDocument>(x => x.IsogdPartition.PartNo == this.IsogdPartition.PartNo && (x.RegNo != "" || x.RegNo != null) && x != this).Count() + 1).ToString();
                if (count.Length == 1)
                    count = String.Concat("000", count);
                if (count.Length == 2)
                    count = String.Concat("00", count);
                if (count.Length == 3)
                    count = String.Concat("0", count);
                if (count.Length == 0)
                    count = String.Concat("0000", count);
                res += count;
            }
            return res;
        }
        
        private string i_CadNo;
        [Size(255),DisplayName("����������� ������ ��������")]
        //[Custom("EditMask", "(35:2[4-5]{1}:[0-9]{7}:[1-9]{1}[0-9]{0,4},)+")]
        //[Custom("EditMaskType", "RegEx")]
        public string CadNo
        {
            get { return i_CadNo; }
            set { SetPropertyValue("CadNo", ref i_CadNo, value); }
        }
        private string i_IsogdPhysStorageBooksNo;
        [Size(255), DisplayName("������ ���")]
        public string IsogdPhysStorageBooksNo
        {
            get { return i_IsogdPhysStorageBooksNo; }
            set { SetPropertyValue("IsogdPhysStorageBooksNo", ref i_IsogdPhysStorageBooksNo, value); }
        }

        [DisplayName("������������ ��������� (���������)")]
        [ImmediatePostData]
        [DataSourceProperty("AvailableDocNames")]
        public dIsogdDocumentNameKind IsogdDocumentNameKind
        {
            get { return i_docNameKind; }
            set
            {
                try { SetPropertyValue("IsogdDocumentNameKind", ref i_docNameKind, value); }
                catch { }
                try
                {
                    if (i_docNameKind?.Name != null)
                        i_docName = GetDocName();
                }
                catch { }
                try
                {
                    if(i_StatementOrg == null && !string.IsNullOrWhiteSpace(DocName) )
                        i_StatementOrg = GetStatementOrg();
                }
                catch { }
                OnChanged("IsogdDocumentNameKind");
            }
        }
        //----------------------------------------------------
        // �������� ��������� ��� ������ ������������ ����������, ������� ��������� �� ��������� �����
        private XPCollection<dIsogdDocumentNameKind> availableDocNames;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<dIsogdDocumentNameKind> AvailableDocNames
        {
            get
            {
                if (availableDocNames == null)
                {
                    // Retrieve all Terminals objects 
                    availableDocNames = new XPCollection<dIsogdDocumentNameKind>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableDocNames();
                }
                return availableDocNames;
            }
        }
        private void RefreshAvailableDocNames()
        {
            if (availableDocNames == null)
                return;
            if(IsogdDocumentClass!=null)
            {
                Connect connect = Connect.FromSession(Session);
                availableDocNames.Criteria = new BinaryOperator("IsogdDocumentClass.Code", IsogdDocumentClass.Code);
                //availableDocNames = connect.FindObjects<dIsogdDocumentNameKind>(mc => mc.IsogdDocumentClass.Code == IsogdDocumentClass.Code) as XPCollection<dIsogdDocumentNameKind>;
            }
            else
            {
                availableDocNames = new XPCollection<dIsogdDocumentNameKind>(Session);
            }
            IsogdDocumentNameKind = null;
        }
        //----------------------------------------------------------



        //private IsogdPhysStorageBook i_IsogdPhysStorageBookLink;
        //[Association, DisplayName("���")]
        //[ImmediatePostData]
        //public IsogdPhysStorageBook IsogdPhysStorageBookLink
        //{
        //    get { return i_IsogdPhysStorageBookLink; }
        //    set { SetPropertyValue("IsogdPhysStorageBookLink", ref i_IsogdPhysStorageBookLink, value); }
        //}
        [Size(SizeAttribute.Unlimited), DisplayName("������������ ���������")]
        public string DocName
        {
            get
            {
                //try {
                //    if(i_docName == null || i_docName == "")
                //        i_docName = GetDocName(); }
                //catch { }
                return i_docName;
            }
            set
            {
                SetPropertyValue("DocName", ref i_docName, value);
            }
        }
        private string GetDocName()
        {
            string res = "";
            string na = "", no = "", da = "";
            try
            {
                if (i_docNameKind != null)
                    if (i_docNameKind.Name.Length > 0)
                        na = i_docNameKind.Name;
                if (DocNo != null)
                    if (DocNo.Length > 0)
                        no = " � " + DocNo;
                if (StatementDate != DateTime.MinValue)
                    da = " �� " + StatementDate.ToShortDateString();
                res = na + no + da;

                try
                {
                    string[] seps = { "�������������", "�������", "������������", "���", "�������������" };
                    foreach (string sep in seps)
                    {
                        if (na.Contains(sep))
                        {
                            res = sep + no + da + na.Replace(sep, "");
                        }
                    }
                }
                catch { }
            }
            catch { }
            return res;
        }
        [DisplayName("���� ���������� ���������")]
        [ImmediatePostData]
        public DateTime PlacementDate
        {
            get
            {
                try
                {
                    if (i_placementDate == DateTime.MinValue)
                        i_placementDate = DateTime.Now.Date;
                }
                catch { }
                return i_placementDate;
            }
            set
            {
                SetPropertyValue("PlacementDate", ref i_placementDate, value);
                //i_Year = i_placementDate.Year;
                //i_eMonth = (eMonths)Enum.Parse(typeof(eMonths), (i_placementDate.Month - 1).ToString());
                //OnChanged("PlacementDate");
            }
        }
        [DisplayName("���������, ������������ ��������")]
        //[ImmediatePostData]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        private string i_DocNo;
        [DisplayName("����� ���������"), Size(255)]
        [ImmediatePostData]
        public string DocNo {
            get { return i_DocNo; }
            set { SetPropertyValue("DocNo", ref i_DocNo, value);
                //CheckDuplicateDocument();
            }
            
        }

        private DateTime i_StatementDate;
        [DisplayName("���� ����������� ���������")]
        //[ImmediatePostData]
        public DateTime StatementDate {
            get { return i_StatementDate; }
            set { SetPropertyValue("StatementDate", ref i_StatementDate, value);
                //CheckDuplicateDocument();
                //OnChanged("StatementDate");
            }
        }
        [DisplayName("������ �������� ���������")]
        public dIsogdReceivingMethod IsogdReceivingMethod { get; set; }

        [DisplayName("����� ������������� ���������")]
        public dIsogdDocumentForm IsogdDocumentForm { get; set; }

        private dIsogdDocVolumeType i_IsogdDocVolumeType;
        [DisplayName("����� ���������(����������)")]
        public dIsogdDocVolumeType IsogdDocVolumeType
        {
            get { return i_IsogdDocVolumeType; }
            set { SetPropertyValue("IsogdDocVolumeType", ref i_IsogdDocVolumeType, value);
                if (!string.IsNullOrWhiteSpace(IsogdDocVolumeType?.Name))
                    DocumentVolume = IsogdDocVolumeType.Name; 
                OnChanged("IsogdDocVolumeType");
            }
        }

        [DisplayName("����� ���������"), Size(64)]
        public string DocumentVolume { get; set; }

        [DisplayName("��� ��������")]
        public dIsogdDataStorageKind IsogdDataStorageKind { get; set; }

        [DisplayName("������ ���������")]
        public eDocumentStatus DocumentStatus
        {
            get { return i_DocumentStatus; }
            set { SetPropertyValue("DocumentStatus", ref i_DocumentStatus, value); }
        }

        [DisplayName("���� �����������")]
        public eSecretLevel SecretLevel
        {
            get { return i_SecretLevel; }
            set { SetPropertyValue("SecretLevel", ref i_SecretLevel, value); }
        }

        [DisplayName("���������� ��������"), Size(255)]
        public string ActivityArea { get; set; }


        private Street i_Street;
        [DisplayName("�����")]
        public Street Street
        {
            get { return i_Street; }
            set { SetPropertyValue("Street", ref i_Street, value);

                if (i_Street != null)
                    i_AddressInfo = i_Street?.FullName; 
                OnChanged("Street");
            }
        }
        private string i_AddressInfo;
        [Size(255),DisplayName("����� (�������� ��������������)")]
        public string AddressInfo
        {
            get {
                try {
                    if (string.IsNullOrWhiteSpace(i_AddressInfo) && !string.IsNullOrWhiteSpace(i_Street?.FullName))
                    {
                        i_AddressInfo = i_Street.FullName;
                    } }
                catch { }
                return i_AddressInfo; }
            set { SetPropertyValue("AddressInfo", ref i_AddressInfo, value); }
        }

        private string i_FolderNo;
        [Size(255), DisplayName("����� �����, ��������������� ���������(�����)")]
        public string FolderNo
        {
            get { return i_FolderNo; }
            set { SetPropertyValue("FolderNo", ref i_FolderNo, value); }
        }
        private eIsogdTomeName i_IsogdTomeName;
        [DisplayName("������������  �����")]
        public eIsogdTomeName IsogdTomeName
        {
            get { return i_IsogdTomeName; }
            set { SetPropertyValue("IsogdTomeName", ref i_IsogdTomeName, value); }
        }
        //{ 
        //    get {
        //        try
        //        {
        //            if (Municipality != null && String.IsNullOrEmpty(ActivityArea))
        //                if (!String.IsNullOrEmpty(Municipality.ActivityArea))
        //                    ActivityArea = Municipality.ActivityArea;
        //        }
        //        catch { }
        //        return ActivityArea;
        //    }
        //    set { ActivityArea = value; }  

        //}
        private GeneralSubject i_Developer;
        [DisplayName("����������� ���������")]
        public GeneralSubject Developer {
            get
            {
               
                return i_Developer;
            }
            set { SetPropertyValue("Developer", ref i_Developer, value); }
        }

        private GeneralSubject i_StatementOrg;
        [DisplayName("����������� ���������")]
        public GeneralSubject StatementOrg {
            get
            {
                //if (i_StatementOrg == null)
                //{ try
                //    {
                        
                //    }
                //catch { }
                //}
                return i_StatementOrg;
            }
            set { SetPropertyValue("StatementOrg", ref i_StatementOrg, value); }
        }

        private GeneralSubject GetStatementOrg()
        {
            GeneralSubject res = null;
            UnitOfWork unitOfWork = (UnitOfWork)this.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                if (DocName.ToLower().Contains("�������������"))
                { res = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "������������� ������ �������"); }
                if (DocName.ToLower().Contains("�������"))
                { res = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "����������� �����������"); }
            }
            catch { }
            return res;
        }

        private void GetDocSubjects()
        {
            UnitOfWork unitOfWork = (UnitOfWork)this.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                if (IsogdDocumentClass.Code == "062000")
                {
                    i_StatementOrg = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "���");
                    i_Developer = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "���");
                }
                if (IsogdDocumentClass.Code == "060601")
                {
                    i_Developer = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "���");
                }
            }
            catch { }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        #region �������� ��� ������ � �������� �������
        //[NonPersistent]
        private eMonths i_eMonth;
        [DisplayName("�����")]
        public eMonths eMonth
        {
            get {
                
                i_eMonth = (eMonths)Enum.Parse(typeof(eMonths), (i_placementDate.Month - 1).ToString());
                return i_eMonth; }
            set { SetPropertyValue("eMonth", ref i_eMonth, value); }
        }

        //private string i_Month;
        //[Size(255), DisplayName("�����")]
        //public string Month
        //{
        //    get {
        //        try { if (PlacementDate != DateTime.MinValue)
        //            {
        //                switch (PlacementDate.Month)
        //                {
        //                    case 1:
        //                        i_Month = "1. ������";
        //                        break;
        //                    case 2:
        //                        i_Month = "2. �������";
        //                        break;
        //                    case 3:
        //                        i_Month = "3. ����";
        //                        break;
        //                    case 4:
        //                        i_Month = "4. ������";
        //                        break;
        //                    case 5:
        //                        i_Month = "5. ���";
        //                        break;
        //                    case 6:
        //                        i_Month = "6. ����";
        //                        break;
        //                    case 7:
        //                        i_Month = "7. ����";
        //                        break;
        //                    case 8:
        //                        i_Month = "8. ������";
        //                        break;
        //                    case 9:
        //                        i_Month = "9. ��������";
        //                        break;
        //                    case 10:
        //                        i_Month = "10. �������";
        //                        break;
        //                    case 11:
        //                        i_Month = "11. ������";
        //                        break;
        //                    case 12:
        //                        i_Month = "12. �������";
        //                        break;
        //                    default:
        //                        i_Month = PlacementDate.Month.ToString();
        //                        break;
        //                }
        //            }
        //        }
        //        catch { }
        //        return i_Month; }
        //    set { SetPropertyValue("Month", ref i_Month, value); }
        //}
        //[NonPersistent]

        private int i_Year;
        [DisplayName("���")]
        public int Year
        {
            get {
                i_Year = i_placementDate.Year;
                return i_Year; }
            set { SetPropertyValue("Year", ref i_Year, value); }
        }
        //[NonPersistent]
        private bool i_AttachmentsFlag;
        [DisplayName("���� ��.�����")]
        public bool AttachmentsFlag
        {
            get {
                //try
                //{
                //    if (AttachmentFiles.Count > 0)
                //        i_AttachmentsFlag = true;
                //}
                //catch { }
                return i_AttachmentsFlag; }
            set { SetPropertyValue("AttachmentsFlag", ref i_AttachmentsFlag, value); }
        }
        //[NonPersistent]
        private int i_AttachmentsCount;
        [DisplayName("���-�� ��. �����")]
        public int AttachmentsCount
        {
            get {
                //try
                //{
                //    i_AttachmentsCount = AttachmentFiles.Count;
                //}
                //catch { }
                return i_AttachmentsCount; }
            set { SetPropertyValue("AttachmentsCount", ref i_AttachmentsCount, value); }
        }
        #endregion

        [Association, DisplayName("����� ����� ���������� �����")]
        public XPCollection<IsogdMap> IsogdMap
        {
            get { return GetCollection<IsogdMap>("IsogdMap"); }
        }

        [Association, DisplayName("��������� �������, ��������� � ����������")]
        //[System.ComponentModel.Browsable(false)]
        public XPCollection<Parcel> Parcels
        {
            get { return GetCollection<Parcel>("Parcels"); }
        }

        [Association("IsogdDocument-IsogdStorageBook"), DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBooks
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBooks"); }
        }
        [Association, DisplayName("���")]
        public XPCollection<IsogdPhysStorageBook> PhysStorageBooks
        {
            get { return GetCollection<IsogdPhysStorageBook>("PhysStorageBooks"); }
        }

        [Association, DisplayName("�������������� �������� �����")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdRequestCard> IsogdRequestCard
        {
            get { return GetCollection<IsogdRequestCard>("IsogdRequestCard"); }
        }

        [Association, DisplayName("������� �� ���������� �����")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdAbstractRequest> IsogdAbstractRequests
        {
            get { return GetCollection<IsogdAbstractRequest>("IsogdAbstractRequests"); }
        }

        //[Association, DisplayName("����������������� ������������")]
        //[System.ComponentModel.Browsable(false)]
        //public GradPlanningDoc GradPlanningDoc { get; set; }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }


        private void CheckDuplicateDocument()
        {
            if (i_DocNo != null && i_DocNo != "" && i_StatementDate != DateTime.MinValue && i_IsogdDocumentClass != null
                && i_IsogdDocumentClass.Code != null && i_IsogdDocumentClass.Code != "")
            {
                Connect connect = Connect.FromSession(Session);
                IsogdDocument isogdDoc = null;
                isogdDoc = connect.FindFirstObject<IsogdDocument>(mc => mc.DocNo == i_DocNo && mc.StatementDate == i_StatementDate
                && mc.IsogdDocumentClass.Code == i_IsogdDocumentClass.Code);
                if (isogdDoc != null)
                    if (isogdDoc != this)
                    {
                        XtraMessageBox.Show(String.Format("�������� � ����������� � {0} �� {1} ��� ������ {2} ��� ���������� � �������. �������� ���������.",
                            i_DocNo, i_StatementDate.ToShortDateString(), i_IsogdDocumentClass.Code));
                        return;
                    }
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            
            if (i_Employee == null)
            {
                if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                {
                    i_Employee = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                }
                //i_Employee = Employee.GetCurrentEmployee();
                //if (SecuritySystem.CurrentUser != null)
                //{
                //    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                //    if (currentEmpl != null)
                //        i_Employee = currentEmpl;
                //}
            }
            

            //if (i_placementDate == DateTime.MinValue)
            //    i_placementDate = DateTime.Now.Date;
            //try
            //{
            //    i_regNo = GetRegNo();
            //}
            //catch { }
            //if (i_requesterType)
        }
        protected override void OnSaving()
        {
            //CheckDuplicateDocument();
            base.OnSaving();
            //GetBooks();
            IsogdDocument isogdDoc;
            string docname = "";
            try
            {
                if (i_docName == null || i_docName == "")
                    i_docName = GetDocName();
            }
            catch { }
        }

        // ��� �������� ��������� ������������� � ��������������� ���������, ���� ������ ���������
        protected override void OnDeleted()
        {
            //string isogdPart = IsogdPartition.PartNo;
            //string oktmo = Municipality.OKTMO;
            base.OnDeleted();
            //Connect connect = Connect.FromSettings();//.FromSession(Session);
            //var c = connect.FindObjects<IsogdDocument>(x => x.IsogdPartition.PartNo == isogdPart && x.Oid!= this.Oid).OrderBy(x => x.PlacementDate).ToList();
            //int i = 0;
            //foreach (IsogdDocument isogdDoc in c)
            //{
            //    //if (isogdDoc != this)
            //    //{

            //        string res = "1";
            //        res = oktmo;
            //        if (isogdPart.Length == 1)
            //            res += "0" + isogdPart;
            //        if (isogdPart.Length > 1)
            //            res += isogdPart;
            //        string count = (i + 1).ToString();
            //        if (count.Length == 1)
            //            count = String.Concat("0000", count);
            //        if (count.Length == 2)
            //            count = String.Concat("000", count);
            //        if (count.Length == 3)
            //            count = String.Concat("00", count);
            //        if (count.Length == 4)
            //            count = String.Concat("0", count);
            //        if (count.Length > 4)
            //            count = count;
            //        if (count.Length == 0)
            //            count = String.Concat("0000", count);
            //        res += count;
            //        isogdDoc.RegNo = res;
            //        isogdDoc.Save();
            //    i++;
            //    //connect.GetUnitOfWork().CommitChanges();
            //    //}
            //}
            //for (int i = 0; i < c.Count; i++)
            //{
            //    IsogdDocument isogdDoc = (IsogdDocument)c[i];
            //    string res = "1";
            //    //string partNom = "�� ����������";
            //    //if (Municipality != null)
            //    //    if (Municipality.OKTMO != null)
            //    //        res = Municipality.OKTMO;
            //    //if (IsogdPartition != null)
            //    //{
            //    //    if (IsogdPartition.PartNo != null)
            //    //    {
            //    //        partNom = IsogdPartition.PartNo;
            //    //        if (IsogdPartition.PartNo.Length == 1)
            //    //            res += "0" + IsogdPartition.PartNo;
            //    //        if (IsogdPartition.PartNo.Length > 1)
            //    //            res += IsogdPartition.PartNo;

            //    //    }
            //    //}
            //    res = oktmo;
            //    if (isogdPart.Length == 1)
            //        res += "0" + isogdPart;
            //    if (isogdPart.Length > 1)
            //        res += isogdPart;
            //    string count = (i+1).ToString();
            //    //if (count.Length == 1)
            //    //    count = String.Concat("000", count);
            //    //if (count.Length == 2)
            //    //    count = String.Concat("00", count);
            //    //if (count.Length == 3)
            //    //    count = String.Concat("0", count);
            //    //if (count.Length > 3)
            //    //    count = count;
            //    //if (count.Length == 0)
            //    //    count = String.Concat("0000", count);
            //    if (count.Length == 1)
            //        count = String.Concat("0000", count);
            //    if (count.Length == 2)
            //        count = String.Concat("000", count);
            //    if (count.Length == 3)
            //        count = String.Concat("00", count);
            //    if (count.Length == 4)
            //        count = String.Concat("0", count);
            //    if (count.Length > 4)
            //        count = count;
            //    if (count.Length == 0)
            //        count = String.Concat("0000", count);
            //    res += count;
            //    isogdDoc.RegNo = res;
            //    isogdDoc.Save();

            //    XtraMessageBox.Show(String.Format("i:{0}, res: {1}", i.ToString(), res));
            //}

        }
        #region ���������� ������ ��� ��������� ��������� ��, ���� ��������, ���
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ��� �������� ������ �� �������� � ����� �������� � ��
        /// </summary>
        private void Parcels_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                Connect connect = Connect.FromSession(Session);
                #region ���� � ��������� �������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    Parcel newObj = (Parcel)e.ChangedObject;
                    // ���������, ���� �� ����� �� � �� � ���, ���� ��� - ���������
                    foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    {
                        bool isExist = false;
                        foreach (Parcel obj in isogdPhysStorageBook.Parcels)
                        {
                            if (obj == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            isogdPhysStorageBook.Parcels.Add(newObj);
                            isogdPhysStorageBook.Save();
                        }
                    }
                    
                    //IsogdStorageBook isogdStorageBook = connect.FindFirstObject<IsogdStorageBook>(mc => mc.Code.Trim() == newObj.CadastralNumber.Trim());
                    //if (isogdStorageBook != null)
                    //{
                    //    isogdStorageBook.Parcel = newObj;
                    //    newObj.IsogdStorageBook = isogdStorageBook;
                    //    bool isExist = false;
                    //    foreach (IsogdStorageBook isogdStorageBookThis in this.IsogdStorageBooks)
                    //    {
                    //        if (isogdStorageBookThis == isogdStorageBook)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        IsogdStorageBooks.Add(isogdStorageBook);
                    //        this.Save();
                    //    }
                    //}
                }
                #endregion

                #region ���� �� ��������� ������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    Parcel newObj = (Parcel)e.ChangedObject;
                    // ���������, ���� �� ����� �� � �� � ���, ���� ���� - �������
                    foreach (IsogdPhysStorageBook isogdPhysStorageBook in this.PhysStorageBooks)
                    {
                        bool isExist = false;
                        foreach (Parcel obj in isogdPhysStorageBook.Parcels)
                        {
                            if (obj == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            isogdPhysStorageBook.Parcels.Remove(newObj);
                            isogdPhysStorageBook.Save();
                        }
                    }
                    //IsogdStorageBook isogdStorageBook = connect.FindFirstObject<IsogdStorageBook>(mc => mc.Code.Trim() == newObj.CadastralNumber.Trim());
                    //if (isogdStorageBook != null)
                    //{
                    //    isogdStorageBook.Parcel = newObj;
                    //    newObj.IsogdStorageBook = isogdStorageBook;
                    //    bool isExist = false;
                    //    foreach (IsogdStorageBook isogdStorageBookThis in this.IsogdStorageBooks)
                    //    {
                    //        if (isogdStorageBookThis == isogdStorageBook)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        IsogdStorageBooks.Remove(isogdStorageBook);
                    //        this.Save();
                    //    }
                    //}
                }
                #endregion
            }
        }
        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ������ �� ��� � ����� �������� � ��
        /// </summary>
        private void PhysStorageBooks_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                #region ���� � ��������� �������� ���
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdPhysStorageBook newObj = (IsogdPhysStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� ��� � �� � ��, ���� ��� - ���������
                    foreach (Parcel parcel in this.Parcels)
                    {
                        bool isExist = false;
                        foreach (IsogdPhysStorageBook obj in parcel.PhysStorageBooks)
                        {
                            if (obj == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            parcel.PhysStorageBooks.Add(newObj);
                            parcel.Save();
                        }
                    }
                    foreach (IsogdStorageBook isogdtorageBook in this.IsogdStorageBooks)
                    {
                        bool isExist = false;
                        foreach (IsogdPhysStorageBook physStorageBook in isogdtorageBook.PhysStorageBooks)
                        {
                            if (physStorageBook == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            isogdtorageBook.PhysStorageBooks.Add(newObj);
                            isogdtorageBook.Save();
                        }
                    }
                }
                #endregion

                #region ���� �� ��������� ������� ���
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdPhysStorageBook newObj = (IsogdPhysStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� ��� � �� � ��, ���� ���� - �������
                    foreach (Parcel parcel in this.Parcels)
                    {
                        bool isExist = false;
                        foreach (IsogdPhysStorageBook obj in parcel.PhysStorageBooks)
                        {
                            if (obj == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            parcel.PhysStorageBooks.Remove(newObj);
                            parcel.Save();
                        }
                    }
                    foreach (IsogdStorageBook isogdtorageBook in this.IsogdStorageBooks)
                    {
                        bool isExist = false;
                        foreach (IsogdPhysStorageBook physStorageBook in isogdtorageBook.PhysStorageBooks)
                        {
                            if (physStorageBook == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            isogdtorageBook.PhysStorageBooks.Remove(newObj);
                            isogdtorageBook.Save();
                        }
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// ������� �������� �, ��� �������������, ���������� ������ �� ����� �������� c �� � ���
        /// </summary>
        private void IsogdStorageBooks_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                Connect connect = Connect.FromSession(Session);
                #region ���� � ��������� �������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterAdd)
                {
                    IsogdStorageBook newObj = (IsogdStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � �� � ���, ���� ��� - ���������
                    //Parcel parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber.Trim() == newObj.Code.Trim());
                    //if (parcel != null)
                    //{
                    //    newObj.Parcel = parcel;
                    //    parcel.IsogdStorageBook = newObj;
                    //    bool isExist = false;
                    //    foreach (Parcel parcelThis in this.Parcels)
                    //    {
                    //        if (parcelThis == parcel)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (!isExist)
                    //    {
                    //        Parcels.Add(parcel);
                    //        this.Save();
                    //    }
                    //}
                    foreach (IsogdPhysStorageBook physStorageBook in this.PhysStorageBooks)
                    {
                        bool isExist = false;
                        foreach (IsogdStorageBook isogdStorageBook in physStorageBook.IsogdStorageBooks)
                        {
                            if (isogdStorageBook == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            physStorageBook.IsogdStorageBooks.Add(newObj);
                            physStorageBook.Save();
                        }
                    }
                }
                #endregion

                #region ���� �� ��������� ������� ��
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    IsogdStorageBook newObj = (IsogdStorageBook)e.ChangedObject;
                    // ���������, ���� �� ������ �� ���� �� � ��������� � ���, ���� ��� - ���������
                    //Parcel parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber.Trim() == newObj.Code.Trim());
                    //if (parcel != null)
                    //{
                    //    newObj.Parcel = parcel;
                    //    parcel.IsogdStorageBook = newObj;
                    //    bool isExist = false;
                    //    foreach (Parcel parcelThis in this.Parcels)
                    //    {
                    //        if (parcelThis == parcel)
                    //        {
                    //            isExist = true;
                    //            break;
                    //        }
                    //    }
                    //    if (isExist)
                    //    {
                    //        Parcels.Remove(parcel);
                    //        this.Save();
                    //    }
                    //}
                    foreach (IsogdPhysStorageBook physStorageBook in this.PhysStorageBooks)
                    {
                        bool isExist = false;
                        foreach (IsogdStorageBook isogdStorageBook in physStorageBook.IsogdStorageBooks)
                        {
                            if (isogdStorageBook == newObj)
                            {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist)
                        {
                            physStorageBook.IsogdStorageBooks.Remove(newObj);
                            physStorageBook.Save();
                        }
                    }
                }
                #endregion
            }
        }

        private void AttachmentFiles_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (!IsLoading)
            {
                // ������������ ���������� ��. ������
                AttachmentsCount = AttachmentFiles.Count;
                if (AttachmentsCount > 0)
                    AttachmentsFlag = true;
                else
                    AttachmentsFlag = false;
            }
        }
        protected override XPCollection<T> CreateCollection<T>(DevExpress.Xpo.Metadata.XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            //// ���������� ��������� ������������� ������
            //if (property.Name == "AttachmentFiles")
            //{
            //    result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            //}
            //// ���������� ��������� ��
            //if (property.Name == "Parcels")
            //{
            //    result.CollectionChanged += new XPCollectionChangedEventHandler(Parcels_CollectionChanged);
            //}
            ////���������� ��������� ��������� ��������
            //if (property.Name == "PhysStorageBooks")
            //{
            //    result.CollectionChanged += new XPCollectionChangedEventHandler(PhysStorageBooks_CollectionChanged);
            //}
            ////���������� ��������� ���
            //if (property.Name == "IsogdStorageBooks")
            //{
            //    result.CollectionChanged += new XPCollectionChangedEventHandler(IsogdStorageBooks_CollectionChanged);
            //}
            //���������� ��������� ������������� ������
            if (property.Name == "AttachmentFiles")
            {
                result.CollectionChanged += new XPCollectionChangedEventHandler(AttachmentFiles_CollectionChanged);
            }
            return result;
        }
        #endregion
    }
}