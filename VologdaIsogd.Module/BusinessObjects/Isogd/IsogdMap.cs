using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Enums;
using AISOGD.General;
using AISOGD.Dictonaries;
using AISOGD.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using AISOGD.Subject;
using DevExpress.ExpressApp.Model;

namespace AISOGD.Isogd
{
    [NavigationItem("������� � ��������� �����")]
    [ModelDefault("Caption", "�����/����� ��������� �����")]
    public class IsogdMap : AttachBase
    {
        public IsogdMap(Session session) : base(session) { }


        [Association, DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        [Association, DisplayName("�������� �����")]
        public IsogdDocument IsogdDocument { get; set; }

        [Size(255), DisplayName("����������������� �����")]
        public string IdNo { get; set; }

        [DisplayName("����� ����� � ���������")]
        public int inDocNo { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }

        [DisplayName("���� ���������� �����/�����")]
        public DateTime DistributionDate { get; set; }

        [DisplayName("��� �����/�����")]
        public dIsogdMapKind IsogdMapKind { get; set; }

        private eSecretLevel i_SecretLevel;
        [DisplayName("���� �����������")]
        public eSecretLevel SecretLevel
        {
            get { return i_SecretLevel; }
            set { SetPropertyValue("SecretLevel", ref i_SecretLevel, value); }
        }

        [DisplayName("��� ����� ����. ������������")]
        public dIsogdTerrSchemeClass IsogdTerrSchemeClass { get; set; }

        [DisplayName("���� ������������ �����/�����")]
        public DateTime StatementDate { get; set; }

        [DisplayName("�������")]
        public dScale Scale { get; set; }

        [Size(255), DisplayName("���������� ��������")]
        public string MapArea { get; set; }

        [DisplayName("������� ���������")]
        public dCoordSys CoordSys { get; set; }

        [DisplayName("����������� ���������")]
        public GeneralSubject Developer { get; set; }

        private Employee i_Employee;
        [DisplayName("���������, ������������ ��������")]
        [ImmediatePostData]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("������� ���������")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_Employee == null)
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                    if (currentEmpl != null)
                        i_Employee = currentEmpl;
                }
            }
        }
    }
}