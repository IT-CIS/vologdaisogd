﻿using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Enums;
using AISOGD.General;
using AISOGD.Isogd;
//using AISOGD.Constr;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AISOGD.Delo
{
    [NavigationItem("Интеграция с СЭД-ДЕЛО")]
    [ModelDefault("Caption", "Документ для импорта в ИСОГД"), System.ComponentModel.DefaultProperty("DocName")]
    public class DocImportToIsogd : AttachBase
    {
        public DocImportToIsogd() : base() { }
        public DocImportToIsogd(Session session) : base(session) { }

        private DateTime i_DocDate;
        private Employee i_Employee;
        private Signer _Signer;

        private eDocumentFromDeloStatus i_eDocumentFromDeloStatus;
        [DisplayName("Статус обработки")]
        public eDocumentFromDeloStatus eDocumentFromDeloStatus
        {
            get { return i_eDocumentFromDeloStatus; }
            set { SetPropertyValue("eDocumentFromDeloStatus", ref i_eDocumentFromDeloStatus, value); }
        }

       
        private string i_DocNo;
        [Size(255), DisplayName("Номер документа")]
        public string DocNo
        {
            get { return i_DocNo; }
            set { SetPropertyValue("DocNo", ref i_DocNo, value); }
        }
        [DisplayName("Дата документа")]
        public DateTime DocDate
        {
            get { return i_DocDate; }
            set { SetPropertyValue("DocDate", ref i_DocDate, value); }
        }

        private DateTime i_ImportDate;
        [DisplayName("Дата импорта")]
        public DateTime ImportDate
        {
            get { return i_ImportDate; }
            set { SetPropertyValue("ImportDate", ref i_ImportDate, value); }
        }

        //private string i_FullDocNo;
        //[Size(255), DisplayName("Полный номер документа")]
        //public string FullDocNo
        //{
        //    get { return i_FullDocNo; }
        //    set { SetPropertyValue("FullDocNo", ref i_FullDocNo, value); }
        //}



        //private string i_DocName;
        //[Size(SizeAttribute.Unlimited), DisplayName("Наименование документа")]
        //public string DocName
        //{
        //    get { return i_DocName; }
        //    set { SetPropertyValue("DocName", ref i_DocName, value); }
        //}
        //private string i_DocumentDeveloper;
        //[Size(SizeAttribute.Unlimited), DisplayName("Разработчик документа")]
        //public string DocumentDeveloper
        //{
        //    get { return i_DocumentDeveloper; }
        //    set { SetPropertyValue("DocumentDeveloper", ref i_DocumentDeveloper, value); }
        //}
        //private string i_DocumentStatementOrg;
        //[Size(SizeAttribute.Unlimited), DisplayName("Утвердитель документа")]
        //public string DocumentStatementOrg
        //{
        //    get { return i_DocumentStatementOrg; }
        //    set { SetPropertyValue("DocumentStatementOrg", ref i_DocumentStatementOrg, value); }
        //}

        private string i_CadNo;
        [Size(255), DisplayName("Кадастровые номера участков")]
        //[Custom("EditMask", "(35:2[4-5]{1}:[0-9]{7}:[1-9]{1}[0-9]{0,4}, )+")]
        //[Custom("EditMaskType", "RegEx")]
        public string CadNo
        {
            get { return i_CadNo; }
            set { SetPropertyValue("CadNo", ref i_CadNo, value); }
        }
        // Данные для ИСОГД
        private IsogdDocumentClass i_IsogdDocumentClass;
        [DisplayName("Класс документа ИСОГД")]
        [ImmediatePostData]
        public IsogdDocumentClass IsogdDocumentClass
        {
            get { return i_IsogdDocumentClass; }
            set
            {
                SetPropertyValue("IsogdDocumentClass", ref i_IsogdDocumentClass, value);
                RefreshAvailableDocNames();
                try
                {
                    GetDocSubjects();
                }
                catch { }
                OnChanged("IsogdDocumentClass");
            }
        }
        private dIsogdDocumentNameKind i_docNameKind;
        [DisplayName("Наименование документа (справочно)")]
        [ImmediatePostData]
        [DataSourceProperty("AvailableDocNames")]
        public dIsogdDocumentNameKind IsogdDocumentNameKind
        {
            get { return i_docNameKind; }
            set
            {
                try { SetPropertyValue("IsogdDocumentNameKind", ref i_docNameKind, value); }
                catch { }
                //try
                //{
                //    i_StatementOrg = GetStatementOrg();
                //}
                //catch { }
                //OnChanged("IsogdDocumentNameKind");
            }
        }
        //----------------------------------------------------
        // Получаем возможные для выбора наименования документов, которые ссылаются на выбранный класс
        private XPCollection<dIsogdDocumentNameKind> availableDocNames;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<dIsogdDocumentNameKind> AvailableDocNames
        {
            get
            {
                if (availableDocNames == null)
                {
                    // Retrieve all Terminals objects 
                    availableDocNames = new XPCollection<dIsogdDocumentNameKind>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableDocNames();
                }
                return availableDocNames;
            }
        }
        private void RefreshAvailableDocNames()
        {
            if (availableDocNames == null)
                return;
            if (IsogdDocumentClass != null)
            {
                Connect connect = Connect.FromSession(Session);
                availableDocNames.Criteria = new BinaryOperator("IsogdDocumentClass.Code", IsogdDocumentClass.Code);
                //availableDocNames = connect.FindObjects<dIsogdDocumentNameKind>(mc => mc.IsogdDocumentClass.Code == IsogdDocumentClass.Code) as XPCollection<dIsogdDocumentNameKind>;
            }
            else
            {
                availableDocNames = new XPCollection<dIsogdDocumentNameKind>(Session);
            }
            IsogdDocumentNameKind = null;
        }
        private string i_AddressInfo;
        [Size(255), DisplayName("Адрес (описание местоположения)")]
        public string AddressInfo
        {
            get
            {
                return i_AddressInfo;
            }
            set { SetPropertyValue("AddressInfo", ref i_AddressInfo, value); }
        }
        private GeneralSubject i_Developer;
        [DisplayName("Разработчик документа")]
        public GeneralSubject Developer
        {
            get
            {
                return i_Developer;
            }
            set { SetPropertyValue("Developer", ref i_Developer, value); }
        }

        private GeneralSubject i_StatementOrg;
        [DisplayName("Утвердитель документа")]
        public GeneralSubject StatementOrg
        {
            get
            {
                return i_StatementOrg;
            }
            set { SetPropertyValue("StatementOrg", ref i_StatementOrg, value); }
        }

        //private GeneralSubject GetStatementOrg()
        //{
        //    GeneralSubject res = null;
        //    UnitOfWork unitOfWork = (UnitOfWork)this.Session;
        //    Connect connect = Connect.FromUnitOfWork(unitOfWork);
        //    try
        //    {
        //        if (DocName.ToLower().Contains("постановление"))
        //        { res = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "Администрация города Вологды"); }
        //        if (DocName.ToLower().Contains("решение"))
        //        { res = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "Вологодский горисполком"); }
        //    }
        //    catch { }
        //    return res;
        //}

        private void GetDocSubjects()
        {
            UnitOfWork unitOfWork = (UnitOfWork)this.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                if (IsogdDocumentClass.Code == "062200")
                {
                    i_StatementOrg = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "УАГ");
                    i_Developer = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "УАГ");
                }
                if (IsogdDocumentClass.Code == "060601")
                {
                    i_Developer = connect.FindFirstObject<GeneralSubject>(x => x.FullName == "УАГ");
                }
            }
            catch { }
        }

        private XPCollection<AuditDataItemPersistent> auditTrail;
        [DisplayName("История изменений")]
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (auditTrail == null)
                    auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return auditTrail;
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
