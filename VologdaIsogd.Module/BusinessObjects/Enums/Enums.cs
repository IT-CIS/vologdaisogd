﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;

namespace AISOGD.Enums
{

    /// <summary>
    /// Тип субъекта
    /// </summary>
    public enum eSubjectType
    {
        [XafDisplayName("Не определено")]
        неопределено = 0,
        [XafDisplayName("Физическое лицо")]
        физ = 1,
        [XafDisplayName("Юридическое лицо")]
        юр = 2
        //[XafDisplayName("Субъект публичного права")]
        //Governance = 2,
    }

    /// <summary>
    /// Статус изысканий
    /// </summary>
    public enum eSurveyStatus
    {
        [XafDisplayName("Не определено")]
        неопределено = 0,
        [XafDisplayName("Новые")]
        новые = 1,
        [XafDisplayName("Архив")]
        архив = 2
        
    }
    ///// <summary>
    ///// Тип субъекта права
    ///// </summary>
    //public enum eOwnerType
    //{
    //    [XafDisplayName("Физическое лицо")]
    //    Person = 0,
    //    [XafDisplayName("Юридическое лицо")]
    //    Organization = 1,
    //    [XafDisplayName("Субъект публичного права")]
    //    Governance = 2

    //}

    /// <summary>
    /// Вид площади
    /// </summary>
    public enum eSquareType
    {
        [XafDisplayName("Нет данных")]
        НетДанных = 0,
        [XafDisplayName("Декларированная")]
        Декларированная = 1,
        [XafDisplayName("Уточненная")]
        Уточненная = 2,
        [XafDisplayName("Полученная из ГИС")]
        ПолученнаяИзГИС = 3
    }

    ///// <summary>
    ///// Тип объекта адресации
    ///// </summary>
    //public enum eAddressObjectType
    //{
    //    [XafDisplayName("Земельный участок")]
    //    ЗУ = 0,
    //    [XafDisplayName("Объект строительства")]
    //    ОКС = 1,
    //    [XafDisplayName("Помещение")]
    //    Помещение = 2

    //}

    ///// <summary>
    ///// Тип документа об адресе
    ///// </summary>
    //public enum eAddressDocType
    //{
    //    [XafDisplayName("Присвоение")]
    //    Присвоение = 0,
    //    [XafDisplayName("Изменение")]
    //    Изменение = 1,
    //    [XafDisplayName("Аннулирование")]
    //    Аннулирование = 2,
    //    [XafDisplayName("Аннулирование с присвоением")]
    //    АннулированиеСПрисвоением = 3

    //}

    /// <summary>
    /// Статус адреса
    /// </summary>
    public enum eAddressStatus
    {
        Действующий = 0,
        Аннулированный = 1,
        //НаУтверждении = 2,
        //НаАннулировании = 3
    }

    ///// <summary>
    ///// Масштаб
    ///// </summary>
    //public enum Scale
    //{
    //    ф = 0,
    //    юр = 1
    //}

    /// <summary>
    /// Тип раздела ИСОГД
    /// </summary>
    public enum ePartitionType
    {
        Основной = 0,
        Дополнительный = 1
    }

    /// <summary>
    /// Статус документа
    /// </summary>
    public enum eDocumentStatus
    {
        Действует = 0,
        Изменен = 1,
        Отменен = 2
    }

    /// <summary>
    /// Гриф секретности
    /// </summary>
    public enum eSecretLevel
    {
        НеСекретно = 0,
        ДляСлужебногоПользования = 1,
        Секретно = 2,
        СовершенноСекретно = 3
    }

    /// <summary>
    /// Тип раздела ИСОГД
    /// </summary>
    public enum eReasonKind
    {
        СопроводительноеПисьмо = 0,
        ДокументОснование = 1
    }

    /// <summary>
    /// Тип книги ИСОГД
    /// </summary>
    public enum eIsogdBooksKind
    {
        КнигаРегистрации = 0,
        КнигаУчетаСведений = 1,
        КнигаХранения = 2,
        КнигаУчетаЗаявок = 3,
        КнигаПредоставленияСведений = 4
    }

    /// <summary>
    /// Месяца
    /// </summary>
    public enum eMonths
    {
        Январь = 0,
        Февраль = 1,
        Март = 2,
        Апрель = 3,
        Май = 4,
        Июнь = 5,
        Июль = 6,
        Август = 7,
        Сентябрь = 8,
        Октябрь = 9,
        Ноябрь = 10,
        Декабрь = 11,
        Неизвестно = 12
    }

    /// <summary>
    /// Статус услуги
    /// </summary>
    public enum eServiceStatus
    {
        ЗаПлату = 0,
        Бесплатно = 1
    }

    /// <summary>
    /// Наименование тома
    /// </summary>
    public enum eIsogdTomeName
    {
        [XafDisplayName("ДЕЛО ОТВОДА")]
        ДО = 0,
        [XafDisplayName("ДЕЛО ПРОЕКТА")]
        ДП = 1,
        [XafDisplayName("ИЗЫСКАНИЯ")]
        ИЗ = 2,
        [XafDisplayName("ДЕЛО СТРОИТЕЛЬСТВА")]
        СТР = 3,
        [XafDisplayName("ИНОЕ")]
        ИН = 4,
        [XafDisplayName("ДЗУ")]
        ДЗУ = 5
    }

    /// <summary>
    /// Категория объекта строительства
    /// </summary>
    public enum eBuildingCategory
    {
        [XafDisplayName("Объект капитального строительства")]
        ОбъектКапитальногоСтроительства = 0,
        [XafDisplayName("Линейный объект")]
        ЛинейныйОбъект = 1,
        [XafDisplayName("ОКС в составе линейного объекта")]
        ОксВСоставеЛинейного = 2,
        [XafDisplayName("Объект культурного наследия")]
        ОбъектКультурногоНаследия = 3
    }

    /// <summary>
    /// Вид объекта строительства
    /// </summary>
    public enum eBuildingKind
    {
        [XafDisplayName("(57)Жилой")]
        Жилой = 0,
        [XafDisplayName("(56)Нежилой")]
        Нежилой = 1,
        [XafDisplayName("(58)Производственного назначения")]
        ПроизводственногоНазначения = 2,
        [XafDisplayName("(59)Линейный")]
        Линейные = 3
    }

    

    /// <summary>
    /// Вид работ
    /// </summary>
    public enum eWorkKind
    {
        Строительство = 0, 
        Реконструкция = 1, 
        РаботыПоСохранению = 2,
        Благоустройство = 3,
        Капремонт = 4,
        ПодготовительныеРаботы = 5,
        Иное = 6
    }

    public enum eConstrFireDangerClass
    {
        [XafDisplayName("Не известно")]
        НеИзвестно = 0,
        [XafDisplayName("С0")]
        С0 = 1,
        [XafDisplayName("С1")]
        С1 = 2,
        [XafDisplayName("С2")]
        С2 = 3,
        [XafDisplayName("С3")]
        С3 = 4
    }

    public enum eConstrFireShield
    {
        [XafDisplayName("Не известно")]
        НеИзвестно = 0,
        [XafDisplayName("I степень")]
        I = 1,
        [XafDisplayName("II степень")]
        II = 2,
        [XafDisplayName("III степень")]
        III = 3,
        [XafDisplayName("IV степень")]
        IV = 4,
        [XafDisplayName("V степень")]
        V = 5
    }

    /// <summary>
    /// Состояние объекта строительства
    /// </summary>
    public enum eConstructionState
    {
        Проектируемое = 0, Строящееся = 1, Эксплуатируемое = 3, Реконструируемое = 4, Неизвестно = 5
    };
    /// <summary>
    /// Стадия строительства очереди
    /// </summary>
    public enum eConstrStageState
    {
        [XafDisplayName("Подготовка РС")]
        ПодготовкаРС = 0,
        [XafDisplayName("Выдано РС")]
        Стройка = 1,
        [XafDisplayName("Введено")]
        Введено = 2,
        [XafDisplayName("Отмена")]
        Отмена = 3,
        [XafDisplayName("Отказ ввода")]
        ОтказВвода = 4,
        [XafDisplayName("Не известно")]
        НеИзвестно = 5
    }

    /// <summary>
    /// Вид ИЖД
    /// </summary>
    public enum eIZDKind
    {
        [XafDisplayName("ИЖД")]
        ИЖД = 0,
        [XafDisplayName("Садовый дом")]
        СадовыйДом = 1,
    }
    /// <summary>
    /// Цель подачи уведомления на ИЖД
    /// </summary>
    public enum eIZDAim
    {
        [XafDisplayName("Строительство")]
        Строительство = 0,
        [XafDisplayName("Реконструкция")]
        Реконструкция = 1,
    }

    /// <summary>
    /// Вид разрешения на строительство
    /// </summary>
    public enum ePermKind
    {
        [Description("Новое")]
        Новое = 0,
        [Description("Продление")]
        Продление = 1,
        [Description("Взамен")]
        Взамен = 2,
        [Description("Утратило действие")]
        УтратилоДействие = 3,
        [Description("Прекращение делопроизводства")]
        ПрекращениеДелопроизводства = 4,
        [Description("Отказ")]
        Отказ = 4
    }
    /// <summary>
    /// Статус документа
    /// </summary>
    public enum eDocStatus
    {
        [XafDisplayName("Подготовка")]
        Подготовка = 0,
        [XafDisplayName("Согласование")]
        Согласование = 1,
        [XafDisplayName("На утверждении")]
        На_утверждении = 2,
        [XafDisplayName("Доработка")]
        Доработка = 3,
        [XafDisplayName("Утвержден")]
        Утвержден = 4,
        [XafDisplayName("Продление")]
        Продление = 5,
        [XafDisplayName("Взамен")]
        Взамен = 6,
        [XafDisplayName("Отказ")]
        Отказ = 7,
        [XafDisplayName("Утратило действие")]
        УтратилоДействие = 8,
        [XafDisplayName("Утратило действие по сроку")]
        УтратилоДействиеПоСроку = 9,
        [Description("Прекращение делопроизводства")]
        ПрекращениеДелопроизводства = 10,
        [XafDisplayName("Ожидание утверждения взамен")]
        ОжиданиеУтвержденияВзамен = 11,
        [XafDisplayName("Иное")]
        Иное = 12
    }
    /// <summary>
    /// Статус входящего обращения
    /// </summary>
    public enum eInLetterStatus
    {
        [XafDisplayName("Новое")]
        Новое = 0,
        [XafDisplayName("В работе")]
        Вработе = 1,
        [XafDisplayName("Получен ответ")]
        ПолученОтвет = 2,
        [XafDisplayName("Завершено")]
        Завершено = 3
    }

    /// Блок ГПЗУ
    /// <summary>
    /// Вид объекта застройки
    /// </summary>
    public enum eGPZUBuildingKind
    {
        [XafDisplayName("ОКС жилое")]
        ОКСЖилое = 0,
        [XafDisplayName("ОКС нежилое")]
        ОКСНеЖилое = 2,
        [XafDisplayName("ИЖД")]
        ИндивидуальныйЖилойДом = 3,
        [XafDisplayName("Линейный объект")]
        ЛинейныйОбъект = 4,
        [XafDisplayName("Иное")]
        Иное = 5

    }
    /// <summary>
    /// Вид работ для ГПЗУ
    /// </summary>
    public enum eGPZUWorkKind
    {
        [XafDisplayName("Строительство")]
        Строительство = 0,
        [XafDisplayName("Реконструкция")]
        Реконструкция = 1,
    }
    /////////////


    /// <summary>
    /// Статус импортированного документа из Дело 
    /// </summary>
    public enum eDocumentFromDeloStatus
    {
        [XafDisplayName("Новый")]
        Новый = 0,
        [XafDisplayName("Зарегистрирован")]
        Зарегистрирован = 1
    }

    /// <summary>
    /// Вид ОКС
    /// </summary>
    public enum eAddressObjectLevel
    {
        [Description("Не определено")]
        Отсутствует = 0,
        [Description("Регион")]
        Регион = 1,
        [Description("Автономный округ")]
        АвтономныйОкруг = 2,
        [Description("Район")]
        Район = 3,
        [Description("Город")]
        Город = 4,
        [Description("Внутригородская территория")]
        ВнутригородскаяТерритория = 5,
        [Description("Населенный пункт")]
        НаселенныйПункт = 6,
        [Description("Улица")]
        Улица = 7,
        [Description("Дополнительные территории")]
        ДополнительныеТерритории = 90,
        [Description("Подчиненные дополнительным территориям")]
        ПодчиненныеДополнительнымТерриториям = 91,
    }

    /// <summary>
    /// Вид работ по изысканиям
    /// </summary>
    public enum eSurveyType
    {
        [XafDisplayName("Инженерные изыскания")]
        Инженерные_изыскания = 0,
        [XafDisplayName("Исполнительная съемка")]
        Исполнительная_съемка = 1,
        [XafDisplayName("Инвентаризация")]
        Инвентаризация = 2
    }

    /// <summary>
    /// Предоставления файла по изысканиям (по-полншетно, одним файлом)
    /// </summary>
    public enum eSurveyMaterialType
    {
        [XafDisplayName("По-планшетно")]
        ByPlanetable = 0,
        [XafDisplayName("Одним файлом")]
        ByFile = 1
    }

    /// <summary>
    /// Вид фонда
    /// </summary>
    public enum eIsLiving
    {
        [XafDisplayName("Жилой")]
        Жилой = 0,
        [XafDisplayName("Нежилой")]
        Нежилой = 1,
        [XafDisplayName("Производственного назначения")]
        ПроизводственногоНазначения = 2,
    }

    /// <summary>
    /// Категория помещения
    /// </summary>
    public enum eApartmentCategory
    {
        [XafDisplayName("Основное")]
        Основное = 0,
        [XafDisplayName("Вспомогательное")]
        Вспомогательное = 1,
    }

    /// <summary>
    /// Вид собственности
    /// </summary>
    public enum eOwnType
    {
        [XafDisplayName("Частная")]
        Частная = 0,
        [XafDisplayName("Муниципальная")]
        Муниципальная = 1,
    }


    /// Да/Нет
    /// </summary>
    public enum eYesOrNo
    {
        [XafDisplayName("Да")]
        Да = 0,
        [XafDisplayName("Нет")]
        Нет = 1,
    }


    /// <summary>
    /// Статус техотчета по изысканиям
    /// </summary>
    public enum eSurveyReportStatus
    {
        [XafDisplayName("Рассмотрение")]
        Рассмотрение = 0,
        [XafDisplayName("Замечания")]
        Замечания = 1,
        [XafDisplayName("Возврат")]
        Возврат = 2,
        [XafDisplayName("Актуализация")]
        Актуализация = 3,
        [XafDisplayName("Работы завершены")]
        Работы_завершены = 4
    }

    /// <summary>
    /// Вид задания по изысканиям
    /// </summary>
    public enum eSurveyTaskKind
    {
        [XafDisplayName("Актуализация данных")]
        Актуализация = 0,
        [XafDisplayName("Размещение растра планшета в ГИС")]
        Размещение_растра = 1,
        [XafDisplayName("Сообщение заявителю")]
        Сообщение_заявителю = 2,
        [XafDisplayName("Иное")]
        Иное = 3
    }
     
}