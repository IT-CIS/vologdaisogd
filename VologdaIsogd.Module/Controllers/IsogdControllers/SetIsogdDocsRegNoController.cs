﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Isogd;
using DevExpress.Xpo;

namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SetIsogdDocsRegNoController : ViewController
    {
        public SetIsogdDocsRegNoController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.GetController<SetIsogdDocsRegNoController>().SetIsogdDocsRegNoAction.Active.SetItemValue("myReason", false);
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция перепростановки регистрационных номеров у документов ИСОГД, отсортированных по дате внесения в ИСОГД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetIsogdDocsRegNoAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //Connect connect = Connect.FromSession(Session);

            IObjectSpace os = Application.CreateObjectSpace();
            
            foreach (IsogdPartition isogdPart in os.GetObjects<IsogdPartition>())
            {
                //XPCollection collection = new XPCollection(typeof(IsogdDocument), new BinaryOperator("IsogdPartition", isogdPart));

                //// Populate the Sorting collection. The values in the "Name" field will be sorted in Ascending order 
                //// before the XPCollection retrieves the objects from a data store. 
                //collection.Sorting.Add(new SortProperty("PlacementDate", DevExpress.Xpo.DB.SortingDirection.Ascending));
                //foreach (IsogdDocument isogdDoc in collection)//connect.FindObjects<IsogdDocument>(mc => mc.IsogdPartition == IsogdPartition))
                List<SortProperty> sortprops = new List<SortProperty>();
                SortProperty sortprop = new SortProperty("PlacementDate", DevExpress.Xpo.DB.SortingDirection.Ascending);
                sortprops.Add(sortprop);
                var collection = os.GetObjects<IsogdDocument>(new BinaryOperator("IsogdPartition", isogdPart), sortprops, false);
                for (int i = 0; i < collection.Count; i++)
                {
                    IsogdDocument isogdDoc = (IsogdDocument)collection[i];
                    string res = "1";
                    string partNom = "не определено";
                    if (isogdDoc.Municipality != null)
                        if (isogdDoc.Municipality.OKTMO != null)
                            res = isogdDoc.Municipality.OKTMO;
                    if (isogdPart != null)
                    {
                        if (isogdPart.PartNo != null)
                        {
                            partNom = isogdPart.PartNo;
                            if (isogdPart.PartNo.Length == 1)
                                res += "0" + isogdPart.PartNo;
                            if (isogdPart.PartNo.Length > 1)
                                res += isogdPart.PartNo;

                        }
                    }
                    string count = (i + 1).ToString();
                    //if (count.Length == 1)
                    //    count = String.Concat("000", count);
                    //if (count.Length == 2)
                    //    count = String.Concat("00", count);
                    //if (count.Length == 3)
                    //    count = String.Concat("0", count);
                    //if (count.Length > 3)
                    //    count = count;
                    //if (count.Length == 0)
                    //    count = String.Concat("0000", count);
                    if (count.Length == 1)
                        count = String.Concat("0000", count);
                    if (count.Length == 2)
                        count = String.Concat("000", count);
                    if (count.Length == 3)
                        count = String.Concat("00", count);
                    if (count.Length == 4)
                        count = String.Concat("0", count);
                    if (count.Length == 0)
                        count = String.Concat("0000", count);
                    res += count;
                    isogdDoc.RegNo = res;
                    isogdDoc.Save();
                    os.CommitChanges();
                }
            }

        }
    }
}
