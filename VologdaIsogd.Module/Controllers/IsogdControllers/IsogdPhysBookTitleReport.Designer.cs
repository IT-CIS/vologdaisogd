﻿namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    partial class IsogdPhysBookTitleReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IsogdPhysBookTitleReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.IsogdPhysBookDocsListReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // IsogdPhysBookTitleReportAction
            // 
            this.IsogdPhysBookTitleReportAction.Caption = "Титул";
            this.IsogdPhysBookTitleReportAction.ConfirmationMessage = null;
            this.IsogdPhysBookTitleReportAction.Id = "IsogdPhysBookTitleReportAction";
            this.IsogdPhysBookTitleReportAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.IsogdPhysBookTitleReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdPhysStorageBook);
            this.IsogdPhysBookTitleReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.IsogdPhysBookTitleReportAction.ToolTip = "Сформировать титульный лист ДЗУ";
            this.IsogdPhysBookTitleReportAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.IsogdPhysBookTitleReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdPhysBookTitleReportAction_Execute);
            // 
            // IsogdPhysBookDocsListReportAction
            // 
            this.IsogdPhysBookDocsListReportAction.Caption = "Опись документов в ДЗУ";
            this.IsogdPhysBookDocsListReportAction.ConfirmationMessage = null;
            this.IsogdPhysBookDocsListReportAction.Id = "IsogdPhysBookDocsListReportAction";
            this.IsogdPhysBookDocsListReportAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.IsogdPhysBookDocsListReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdPhysStorageBook);
            this.IsogdPhysBookDocsListReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.IsogdPhysBookDocsListReportAction.ToolTip = "Сформировать опись документов ИСОГД в ДЗУ";
            this.IsogdPhysBookDocsListReportAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.IsogdPhysBookDocsListReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdPhysBookDocsListReportAction_Execute);
            // 
            // IsogdPhysBookTitleReport
            // 
            this.Actions.Add(this.IsogdPhysBookTitleReportAction);
            this.Actions.Add(this.IsogdPhysBookDocsListReportAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IsogdPhysBookTitleReportAction;
        private DevExpress.ExpressApp.Actions.SimpleAction IsogdPhysBookDocsListReportAction;
    }
}
