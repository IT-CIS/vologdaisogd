using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using System.Reflection;
using AISOGD.SystemDir;
using AISOGD.Isogd;
using DevExpress.Xpo;
using AISOGD.OrgStructure;
using DevExpress.Xpo.DB;
using System.Collections;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class IsogdBooksReport : ViewController
    {
        public IsogdBooksReport()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void IsogdBooksReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //IsogdBooksCard i_IsogdBooksCard;
            //i_IsogdBooksCard = (IsogdBooksCard)e.CurrentObject;
            

            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            UnitOfWork unitOfWork = (UnitOfWork)connect.GetSession();
            
            // ������� ������������ �� ��� ������� ������
            Municipality mun = connect.FindFirstObject<Municipality>(mc=>
                mc.dMunicipalityKind.Code == "1" || mc.dMunicipalityKind.Code == "2");

            string oktmo = "";
            string munname = "";
            string openBookDate = "";
            string closeBookDate = DateTime.Now.ToShortDateString();
            if (mun.Name != null || mun.Name != "")
                munname = mun.Name;
            if (mun.OKTMO != null || mun.OKTMO != "")
                oktmo = mun.OKTMO;

            IsogdBooksCard i_IsogdBooksCard = connect.FindFirstObject<IsogdBooksCard>(mc=> mc.RecNo == "1");
            if (i_IsogdBooksCard != null)
                if (i_IsogdBooksCard.RecordDate != null)
                    openBookDate = i_IsogdBooksCard.RecordDate.ToShortDateString();

            //IEnumerable<IsogdBooksCard> isogdBooksCardList = connect.FindObjects<IsogdBooksCard>(mc=> mc.Oid!=null)
            //    .AsEnumerable<IsogdBooksCard>;
            SortingCollection sortCollection = new SortingCollection();
            sortCollection.Add(new SortProperty("RecordDate", SortingDirection.Ascending));

            ICollection isogdBooksCardList =  unitOfWork.GetObjects(connect.GetClassInfo<IsogdBooksCard>(), 
                CriteriaOperator.Parse("1 = 1"),
                sortCollection,Int32.MaxValue,false,false);
            //#region #serverprint
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\BooksListTemplate.rtf");
            
            richServer.LoadDocumentTemplate(templatePath);
            //var doc = XDocument.Load(Path.Combine(assemblyFile, "isogd-config.xml"));
            
            //richServer.LoadDocumentTemplate(@"C:\XAF\WinAisogdMapInfo\DocTemplates\BooksListTemplate.rtf");
            string text = "";
            Bookmark bookmarkMO = null;
            Bookmark bookmarkOKTMO = null;
            Bookmark bookmarkOpenBookDate = null;
            Bookmark bookmarkCloseBookDate = null;
            // ������� ������� �������� � �������
            foreach (Bookmark bookmark in richServer.Document.Bookmarks)
            {
                if (bookmark.Name == "MOName")
                    bookmarkMO = bookmark;
                if (bookmark.Name == "OKTMO")
                    bookmarkOKTMO = bookmark;
                if (bookmark.Name == "OpeningTomeDate")
                    bookmarkOpenBookDate = bookmark;
                if (bookmark.Name == "ClosingTomeDate")
                    bookmarkCloseBookDate = bookmark;
            }
         //DocumentPosition pos = bookmark.Range.Start;
         //           richServer.Document.InsertText(pos, "asdadadadsada");
            // ����� ������ � �������� ����������
            richServer.Document.Replace(bookmarkMO.Range, munname);
            richServer.Document.Replace(bookmarkOKTMO.Range, oktmo);
            richServer.Document.Replace(bookmarkOpenBookDate.Range, openBookDate);
            richServer.Document.Replace(bookmarkCloseBookDate.Range, closeBookDate);

            // ��������� ������ � �������
            int i = 0;
            DocumentRange range = richServer.Document.CreateRange(0,0);
            foreach (IsogdBooksCard isogdBooksCard in isogdBooksCardList)
            {
                if (richServer.Document.Tables[0].Rows.Count < i + 2)
                    richServer.Document.Tables[0].Rows.InsertAfter(i);

                range = richServer.Document.Tables[0].Rows[i + 1].Cells[0].ContentRange;
                richServer.Document.Replace(range, (i + 1).ToString());

                range = richServer.Document.Tables[0].Rows[i + 1].Cells[1].ContentRange;
                richServer.Document.Replace(range, isogdBooksCard.IsogdBooksKind.ToString());

                if (isogdBooksCard.OpenenigTomeDate != DateTime.MinValue)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[2].ContentRange;
                    richServer.Document.Replace(range, isogdBooksCard.OpenenigTomeDate.ToShortDateString());
                }
                if (isogdBooksCard.ClosingTomeDate != DateTime.MinValue)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[3].ContentRange;
                    richServer.Document.Replace(range, isogdBooksCard.ClosingTomeDate.ToShortDateString());
                }
                if (isogdBooksCard.NextBookTomeNo != null && isogdBooksCard.NextBookTomeNo != "")
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[4].ContentRange;
                    richServer.Document.Replace(range, isogdBooksCard.NextBookTomeNo);
                }
                i++;
            }
                //XtraMessageBox.Show(richServer.Document.Tables[0].Rows[0].Cells[0].Range);
                //XtraMessageBox.Show(text);
                //richServer.Document.re.Bookmarks
                //MOName

                //// Specify default formatting
                //richServer.Document.DefaultParagraphProperties.Alignment = ParagraphAlignment.Center;
                //// Specify page settings
                //richServer.Document.Sections[0].Page.Landscape = true;
                //richServer.Document.Sections[0].Page.Height = DevExpress.Office.Utils.Units.InchesToDocumentsF(10.0f);
                //richServer.Document.Sections[0].Page.Width = DevExpress.Office.Utils.Units.InchesToDocumentsF(4.5f);
                //// Add document content
                //richServer.Document.AppendText("This content is created programmatically\n");
                //richServer.Document.AppendParagraph();
                //InsertTableIntoDocument(richServer);
                //// Invoke the Print Preview dialog
                using (PrintingSystem printingSystem = new PrintingSystem())
                {
                    using (PrintableComponentLink link = new PrintableComponentLink(printingSystem))
                    {
                        link.Component = richServer;
                        link.CreateDocument();
                        link.ShowPreviewDialog();
                    }
                }
            //#endregion #serverprint
        }
    }
}
