using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using AISOGD.Isogd;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using AISOGD.OrgStructure;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.Reflection;
using DevExpress.XtraPrinting;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class IsogdOutDataInfoReport : ViewController
    {
        public IsogdOutDataInfoReport()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void IsogdOutDataInfoAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IsogdOutDataCard i_IsogdOutDataCard;
            i_IsogdOutDataCard = (IsogdOutDataCard)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdOutDataCard.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            // ������� ������������ �� ��� ������� ������
            Municipality mun = connect.FindFirstObject<Municipality>(mc =>
                mc.dMunicipalityKind.Code == "1" || mc.dMunicipalityKind.Code == "2");

            string munname = "";
            if (mun.Name != null)
                munname = mun.Name;

            string outdatacardno = "";
            string outdatacarddate = "";
            string requesinfo = "";
            string requester = "";
            string requestertype = "";
            string moneysum = "";
            string moneydoc = "";
            string datacompletedate = "";
            string datacompleteempl = "";
            string dataissueempl = "";
            string trustdoc = "";
            string person = "";


            if (i_IsogdOutDataCard != null)
            {
                if (i_IsogdOutDataCard.IsogdRequestCard != null)
                {
                    // ����� �������
                    if (i_IsogdOutDataCard.CardNo != null)
                        outdatacardno = i_IsogdOutDataCard.CardNo;

                    // ���� ������ ����������
                    if (i_IsogdOutDataCard.DataIssueDate != DateTime.MinValue)
                        outdatacarddate = i_IsogdOutDataCard.DataIssueDate.ToShortDateString();

                    // ������
                    if (i_IsogdOutDataCard.IsogdRequestCard.descript != null)
                        requesinfo = i_IsogdOutDataCard.IsogdRequestCard.descript;
                    // ���������
                    try { requester = i_IsogdOutDataCard.IsogdRequestCard.Requester.FullName; }
                    catch { }
                    //if (i_IsogdOutDataCard.IsogdRequestCard.RequesterType ==
                    //    AISOGD.Enums.eSubjectType.���)
                    //{
                    //    if (i_IsogdOutDataCard.IsogdRequestCard.RequesterPerson != null)
                    //        if (i_IsogdOutDataCard.IsogdRequestCard.RequesterPerson.BriefName != null)
                    //            requester = i_IsogdOutDataCard.IsogdRequestCard.RequesterPerson.BriefName;
                    //}
                    //else
                    //{
                    //    if (i_IsogdOutDataCard.IsogdRequestCard.RequesterOrg != null)
                    //        if (i_IsogdOutDataCard.IsogdRequestCard.RequesterOrg.Name != null)
                    //            requester = i_IsogdOutDataCard.IsogdRequestCard.RequesterOrg.Name;
                    //}
                    // ������ ������
                    if (i_IsogdOutDataCard.ServiceStatus == AISOGD.Enums.eServiceStatus.�������)
                        requestertype = "�� �����";
                    else
                        requestertype = "���������";

                    // ����� ������
                    if (i_IsogdOutDataCard.MoneySum != null)
                        moneysum = i_IsogdOutDataCard.MoneySum;

                    // ��������
                    if (i_IsogdOutDataCard.PaymentDoc != null)
                        moneydoc = i_IsogdOutDataCard.PaymentDoc;

                    // ���� ���������� ��������
                    if (i_IsogdOutDataCard.DataCompleteDate != DateTime.MinValue)
                        datacompletedate = i_IsogdOutDataCard.DataCompleteDate.ToShortDateString();

                    // �������� ����������
                    if (i_IsogdOutDataCard.Empl != null)
                        if (i_IsogdOutDataCard.Empl.BriefName != null)
                            datacompleteempl = i_IsogdOutDataCard.Empl.BriefName;

                    // �������� �����������
                    if (i_IsogdOutDataCard.EmplDataIssue != null)
                        if (i_IsogdOutDataCard.EmplDataIssue.BriefName != null)
                            dataissueempl = i_IsogdOutDataCard.EmplDataIssue.BriefName;

                    // �������� �������
                    if (i_IsogdOutDataCard.Person != null)
                        if (i_IsogdOutDataCard.Person.FullName != null && i_IsogdOutDataCard.Person.FullName != "")
                            person = i_IsogdOutDataCard.Person.FullName;

                    // ������������
                    if (i_IsogdOutDataCard.TrustingDoc != null)
                        trustdoc = i_IsogdOutDataCard.TrustingDoc;


                    RichEditDocumentServer richServer = new RichEditDocumentServer();
                    string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                        LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\IsogdOutDataInfoTemplate.rtf");

                    richServer.LoadDocumentTemplate(templatePath);

                    Bookmark CardNo = null;
                    Bookmark DataIssue = null;
                    Bookmark MOName = null;
                    Bookmark ReqcardInfo = null;
                    Bookmark Requester = null;
                    Bookmark RequestType = null;
                    Bookmark MoneySum = null;
                    Bookmark PaymentDoc = null;
                    Bookmark DataCompleteDate = null;
                    Bookmark DataCompleteEmpl = null;
                    Bookmark DataIssueEmpl = null;
                    Bookmark Person = null;
                    Bookmark TrustDoc = null;

                    // ������� ������� �������� � �������
                    foreach (Bookmark bookmark in richServer.Document.Bookmarks)
                    {
                        if (bookmark.Name == "CardNo")
                            CardNo = bookmark;
                        if (bookmark.Name == "DataIssue")
                            DataIssue = bookmark;
                        if (bookmark.Name == "MOName")
                            MOName = bookmark;
                        if (bookmark.Name == "ReqcardInfo")
                            ReqcardInfo = bookmark;
                        if (bookmark.Name == "Requester")
                            Requester = bookmark;
                        if (bookmark.Name == "RequestType")
                            RequestType = bookmark;
                        if (bookmark.Name == "MoneySum")
                            MoneySum = bookmark;
                        if (bookmark.Name == "PaymentDoc")
                            PaymentDoc = bookmark;
                        if (bookmark.Name == "DataCompleteDate")
                            DataCompleteDate = bookmark;
                        if (bookmark.Name == "DataCompleteEmpl")
                            DataCompleteEmpl = bookmark;
                        if (bookmark.Name == "DataIssueEmpl")
                            DataIssueEmpl = bookmark;
                        if (bookmark.Name == "Person")
                            Person = bookmark;
                        if (bookmark.Name == "TrustDoc")
                            TrustDoc = bookmark;
                    }

                    // ����� ������ � �������� ����������
                    richServer.Document.Replace(CardNo.Range, outdatacardno);
                    richServer.Document.Replace(DataIssue.Range, outdatacarddate);
                    richServer.Document.Replace(MOName.Range, munname);
                    richServer.Document.Replace(ReqcardInfo.Range, requesinfo);

                    richServer.Document.Replace(Requester.Range, requester);
                    richServer.Document.Replace(RequestType.Range, requestertype);
                    richServer.Document.Replace(MoneySum.Range, moneysum);
                    richServer.Document.Replace(PaymentDoc.Range, moneydoc);

                    richServer.Document.Replace(DataCompleteDate.Range, datacompletedate);
                    richServer.Document.Replace(DataCompleteEmpl.Range, datacompleteempl);
                    richServer.Document.Replace(DataIssueEmpl.Range, dataissueempl);
                    richServer.Document.Replace(Person.Range, person);
                    richServer.Document.Replace(TrustDoc.Range, trustdoc);

                    // ��������� ������ � �������
                    int i = 0;
                    DocumentRange range = richServer.Document.CreateRange(0, 0);
                    foreach (IsogdDocument isogdDocument in i_IsogdOutDataCard.IsogdRequestCard.IsogdDocument)
                    {
                        if (richServer.Document.Tables[1].Rows.Count < i + 2)
                            richServer.Document.Tables[1].Rows.InsertAfter(i);


                        // �����
                        if (isogdDocument.IsogdDocumentClass != null)
                            if (isogdDocument.IsogdDocumentClass.Name != null)
                            {
                                range = richServer.Document.Tables[1].Rows[i + 1].Cells[0].ContentRange;
                                richServer.Document.Replace(range, isogdDocument.IsogdDocumentClass.Name);
                            }
                        // �����
                        if (isogdDocument.RegNo != null)
                        {
                            range = richServer.Document.Tables[1].Rows[i + 1].Cells[1].ContentRange;
                            richServer.Document.Replace(range, isogdDocument.RegNo);
                        }
                        // �������
                        if (isogdDocument.PlacementDate != DateTime.MinValue)
                        {
                            range = richServer.Document.Tables[1].Rows[i + 1].Cells[2].ContentRange;
                            richServer.Document.Replace(range, isogdDocument.PlacementDate.ToShortDateString());
                        }
                        // �����
                        if (isogdDocument.DocNo != null)
                        {
                            range = richServer.Document.Tables[1].Rows[i + 1].Cells[3].ContentRange;
                            richServer.Document.Replace(range, isogdDocument.DocNo);
                        }
                        // ����
                        if (isogdDocument.StatementDate != DateTime.MinValue)
                        {
                            range = richServer.Document.Tables[1].Rows[i + 1].Cells[4].ContentRange;
                            richServer.Document.Replace(range, isogdDocument.StatementDate.ToShortDateString());
                        }
                        i++;
                    }
                    //// Invoke the Print Preview dialog
                    using (PrintingSystem printingSystem = new PrintingSystem())
                    {
                        using (PrintableComponentLink link = new PrintableComponentLink(printingSystem))
                        {
                            link.Component = richServer;
                            link.CreateDocument();
                            link.ShowPreviewDialog();
                        }
                    }
                }
                else
                    XtraMessageBox.Show("�� ������� ������ �� �������������� ��������!");
            }
        }
    }
}
