namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class IsogdBillReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IsogdBillReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // IsogdBillReportAction
            // 
            this.IsogdBillReportAction.Caption = "������������ ���������";
            this.IsogdBillReportAction.ConfirmationMessage = null;
            this.IsogdBillReportAction.Id = "IsogdBillReportAction";
            this.IsogdBillReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdRequestCard);
            this.IsogdBillReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.IsogdBillReportAction.ToolTip = null;
            this.IsogdBillReportAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.IsogdBillReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdBillReportAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IsogdBillReportAction;
    }
}
