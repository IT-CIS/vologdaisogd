namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class IsogdOutDataInfoReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IsogdOutDataInfoAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // IsogdOutDataInfoAction
            // 
            this.IsogdOutDataInfoAction.Caption = "������������ ������� � �������������� ��������";
            this.IsogdOutDataInfoAction.ConfirmationMessage = null;
            this.IsogdOutDataInfoAction.Id = "IsogdOutDataInfoAction";
            this.IsogdOutDataInfoAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdOutDataCard);
            this.IsogdOutDataInfoAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.IsogdOutDataInfoAction.ToolTip = null;
            this.IsogdOutDataInfoAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.IsogdOutDataInfoAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdOutDataInfoAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IsogdOutDataInfoAction;
    }
}
