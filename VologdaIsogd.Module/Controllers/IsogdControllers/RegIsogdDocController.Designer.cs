namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class RegIsogdDocController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RegIsogdDocAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RegIsogdDocAction
            // 
            this.RegIsogdDocAction.Caption = "���������������� �������� �����";
            this.RegIsogdDocAction.ConfirmationMessage = null;
            this.RegIsogdDocAction.Id = "RegIsogdDocAction";
            this.RegIsogdDocAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RegIsogdDocAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdDocument);
            this.RegIsogdDocAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RegIsogdDocAction.ToolTip = null;
            this.RegIsogdDocAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RegIsogdDocAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RegIsogdDocAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RegIsogdDocAction;
    }
}
