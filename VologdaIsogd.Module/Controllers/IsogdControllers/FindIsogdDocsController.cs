using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.ExpressApp.Actions;
using DevExpress.XtraEditors;
using AISOGD.Isogd;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using AISOGD.General;
using AISOGD.OrgStructure;
using FileSystemData.BusinessObjects;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;
using VologdaIsogd.Module.BusinessObjects.SystemDir;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    public partial class FindIsogdDocsController : ViewController
    {
        public FindIsogdDocsController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        /// <summary>
        /// ����������� ������������� ����� ���������� ����� 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateDocsAttachFileAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ������ � ��� ������ (UnitOfWork)
            View.ObjectSpace.CommitChanges();
            IsogdOutDataCard i_IsogdOutDataCard = (IsogdOutDataCard)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdOutDataCard.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            if (i_IsogdOutDataCard.IsogdRequestCard == null)
            {
                XtraMessageBox.Show("�� ������� ������ �� �������������� �������� �����");
                return;
            }

            bool isAttachExist = false;
            if (i_IsogdOutDataCard.IsogdRequestCard != null)
            {
                IsogdRequestCard i_IsogdRequestCard = i_IsogdOutDataCard.IsogdRequestCard;
                if (i_IsogdRequestCard.IsogdDocument.Count > 0)
                {
                    foreach (IsogdDocument isogdDoc in i_IsogdRequestCard.IsogdDocument)
                    {
                        if (isogdDoc != null)
                        {
                            if (isogdDoc.AttachmentFiles.Count > 0)
                            {
                                foreach (AttachmentFiles attach in isogdDoc.AttachmentFiles)
                                {
                                    if (i_IsogdOutDataCard.AttachmentFiles.Count > 0)
                                    {
                                        foreach (AttachmentFiles isogdattach in i_IsogdOutDataCard.AttachmentFiles)
                                            if (isogdattach.FileStore.FileName == attach.FileStore.FileName && isogdattach.FileStore.Size == attach.FileStore.Size)
                                            {
                                                isAttachExist = true;
                                                break;
                                            }
                                    }
                                    if (!isAttachExist)
                                    {
                                        AttachmentFiles att = new AttachmentFiles(unitOfWork);
                                        if (attach.Name != null)
                                            att.Name = attach.Name;
                                        att.RegDate = DateTime.Now.Date;
                                        if (attach.Description != null)
                                            att.Description = attach.Description;
                                        if (SecuritySystem.CurrentUser != null)
                                        {
                                            Employee currentEmpl = unitOfWork.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                                            if (currentEmpl != null)
                                                att.Registrator = currentEmpl;
                                        }
                                        if (attach.FileStore != null)
                                        {


                                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                            {
                                                attach.FileStore.SaveToStream(ms);
                                                FileSystemStoreObject fileSystemStoreObject = connect.CreateObject<FileSystemStoreObject>();
                                                fileSystemStoreObject.LoadFromStream(attach.FileStore.FileName, ms);
                                                att.FileStore = fileSystemStoreObject;
                                                ms.Dispose();
                                            }
                                        }

                                        i_IsogdOutDataCard.AttachmentFiles.Add(att);
                                        i_IsogdOutDataCard.Save();
                                        unitOfWork.CommitChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                    XtraMessageBox.Show("� ������ �� ������ �������� ����� �� ���������� �� ������ ���������!");
            }
            i_IsogdOutDataCard.Save();
        }



        /// <summary>
        /// ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetIsogdDocsFromGisAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //�������� ��� ����������
            IGISApplication GisApp = GISApplication.GetGISApplication();

            if (!GisApp.isReady())
            {
                XtraMessageBox.Show("�� �������� ��� ����������", "����");
                return;
            }
            // �������� ������ � ��� ������ (UnitOfWork)
            IsogdRequestCard i_IsogdRequestCard = (IsogdRequestCard)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdRequestCard.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            //XtraMessageBox.Show(i_IsogdRequestCard.ClassInfo.FullName);
            SpatialController<GisLayer> spatial = new SpatialController<GisLayer>(connect);
            
            // ���� ������ � ��� � SpatialRepository
            SpatialRepository spatialRepo = connect.FindFirstObject<SpatialRepository>(mc =>
                mc.IsogdClassName == i_IsogdRequestCard.ClassInfo.FullName &&
                mc.IsogdObjectId == i_IsogdRequestCard.Oid.ToString());

            
            //XtraMessageBox.Show("2");
            // ���� ��������� ���� � ������� �������� �����
            List<string> isogdDocumentLayersIds = spatial.GetListLinkedSpatialLayers(typeof(IsogdDocument).FullName);
            //XtraMessageBox.Show("3");
            if (spatialRepo != null)
            {
                string reqLayerName = "";
                string reqObjId = "";
                try
                {
                    reqLayerName = spatialRepo.SpatialLayerId;
                    reqObjId = spatialRepo.SpatialObjectId;
                }
                catch
                {
                    XtraMessageBox.Show("� �������� ������ �� ������� ����� � ���������������� ��������");
                    return;
                }
                if (reqObjId == "0")
                {
                    XtraMessageBox.Show("����� ���, ��� ������� ���������� � ���������������� ������� �� ���������� ���������!", "��������");
                    return;
                }

                //�������� ���������������� ������
                GisApp.SelectMapObject(reqLayerName, reqObjId);

                // ���������� ��������� ������� (������ ������)
                if (GisApp.CountSelectedObject() == 0)
                {
                    XtraMessageBox.Show("� ������ ��� ���������� ����������������� �������. ����������, ��������� ���������� ������ ��� �������������� �������� ����� � ������� � ���������� ���������.", "����");
                    return;
                }

                // 2.1 �������� ������� ��� ��������� ����� � ���� ����������� � ��������� ���� ������
                List<string> documentMapObjIDs = new List<string>();
                try
                {
                    foreach (string layer in isogdDocumentLayersIds)
                    {
                        foreach (string objId in GisApp.GetCrossingMapObjects(reqLayerName, reqObjId, layer))
                        {
                            documentMapObjIDs.Add($"{objId}&{layer}");
                        }
                    }
                }
                catch (Exception ex) { MessageHelper.FullErrorMessage(ex); }

                // ������� �������� �����
                //�������� ��������� ���������� ������� ���������� �����, �������, ���� �� ��� ����� �
                // ������, ���� ��� - ���������
                // �� ������� ����� �������� �� ����������� ������� ��������� �����
                foreach (string documentMapObjID in documentMapObjIDs)
                {
                    SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                        mc.SpatialLayerId == documentMapObjID.Split('&')[1] &&
                        mc.SpatialObjectId == documentMapObjID.Split('&')[0] &&
                        mc.IsogdClassName == typeof(IsogdDocument).FullName);
                    if (objLink != null)
                    {
                        // �� �� ������� ��� ���������� ������ ��������� �����
                        IsogdDocument i_IsogdDocument = unitOfWork.FindObject<IsogdDocument>(new BinaryOperator("Oid", objLink.IsogdObjectId));
                        if (i_IsogdDocument != null)
                        {
                            bool isDocExist = false;

                            foreach (IsogdDocument isogdDoc in i_IsogdRequestCard.IsogdDocument)
                            {
                                if (i_IsogdDocument == isogdDoc)
                                    isDocExist = true;
                            }
                            if (!isDocExist)
                            {
                                i_IsogdRequestCard.IsogdDocument.Add(i_IsogdDocument);
                                unitOfWork.CommitChanges();
                            }
                        }
                    }
                }
                try
                {
                    string className = typeof(IsogdStorageBook).FullName;
                    List<string> bookMapObjIDs = new List<string>();
                    // 2.1 �������� ������� ��� ����� �������� � ���� ����������� � ��������� ���� ������
                    foreach (string layer in spatial.GetListLinkedSpatialLayers(className))
                    {
                        foreach (string objId in GisApp.GetCrossingMapObjects(reqLayerName, reqObjId, layer))
                        {
                            bookMapObjIDs.Add($"{objId}&{layer}");
                        }     
                    }
                    foreach (string bookMapObjId in bookMapObjIDs)
                    {
                        // ������� �������� ����� ��������
                        // �� ������� ����� �������� �� ����������� ������� ����� ��������
                        SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                            mc.SpatialLayerId == bookMapObjId.Split('&')[1] &&
                            mc.SpatialObjectId == bookMapObjId.Split('&')[0] &&
                            mc.IsogdClassName == className);


                        if (objLink != null)
                        {
                            IsogdStorageBook i_IsogdStorageBook = unitOfWork.FindObject<IsogdStorageBook>(new BinaryOperator("Oid", objLink.IsogdObjectId));
                            // ���������� ��������� ����� ��������
                            foreach (IsogdDocument isogdDocument in i_IsogdStorageBook.IsogdDocuments)
                            {
                                bool isDocExist = false;
                                // ���������, ����� �� ��� ���� � ������
                                foreach (IsogdDocument isogdDoc in i_IsogdRequestCard.IsogdDocument)
                                {
                                    if (isogdDocument == isogdDoc)
                                        isDocExist = true;
                                }
                                // ���� ���, �� ����������� � ���
                                if (!isDocExist)
                                {
                                    i_IsogdRequestCard.IsogdDocument.Add(isogdDocument);
                                    unitOfWork.CommitChanges();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) { MessageHelper.FullErrorMessage(ex); }
            }
            else
                XtraMessageBox.Show("� ������ ������ ��� ��������� ���������� � ���.", "����");
        }        
    }
}
