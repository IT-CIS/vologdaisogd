namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class IsogdOutDataBookReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OutDataBookReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // OutDataBookReportAction
            // 
            this.OutDataBookReportAction.Caption = "������������ ����� ����� �������� �� �����";
            this.OutDataBookReportAction.ConfirmationMessage = null;
            this.OutDataBookReportAction.Id = "OutDataBookReportAction";
            this.OutDataBookReportAction.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.OutDataBookReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdOutDataCard);
            this.OutDataBookReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.OutDataBookReportAction.ToolTip = null;
            this.OutDataBookReportAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.OutDataBookReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OutDataBookReportAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction OutDataBookReportAction;
    }
}
