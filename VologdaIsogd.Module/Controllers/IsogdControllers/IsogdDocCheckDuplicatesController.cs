﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Isogd;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.Persistent.BaseImpl;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IsogdDocCheckDuplicatesController : ViewController
    {
        public IsogdDocCheckDuplicatesController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CheckDocDublicateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            //var x_id = ((BaseObject)e.CurrentObject).Oid;
            IsogdDocument i_IsogdDocument = null;
            //i_IsogdDocument = os.FindObject<IsogdDocument>(new BinaryOperator("Oid", x_id));
            i_IsogdDocument = (IsogdDocument)e.CurrentObject;
            //UnitOfWork unitOfWork = (UnitOfWork)i_IsogdDocument.Session;
            Connect connect = Connect.FromObjectSpace(os);//.FromUnitOfWork(unitOfWork);

            bool isExist = false;
            if (i_IsogdDocument.DocNo != null && i_IsogdDocument.DocNo != "" && i_IsogdDocument.StatementDate != DateTime.MinValue && i_IsogdDocument.IsogdDocumentClass != null
                && i_IsogdDocument.IsogdDocumentClass.Code != null && i_IsogdDocument.IsogdDocumentClass.Code != "")
            {
                IsogdDocument isogdDoc = null;
                isogdDoc = connect.FindFirstObject<IsogdDocument>(mc => mc.DocNo == i_IsogdDocument.DocNo && mc.StatementDate == i_IsogdDocument.StatementDate
                && mc.IsogdDocumentClass.Code == i_IsogdDocument.IsogdDocumentClass.Code);
                if (isogdDoc != null)
                    if (isogdDoc != i_IsogdDocument)
                    {
                        //XtraMessageBox.Show(String.Format("Документ с реквизитами № {0} от {1} код класса {2} уже существует в системе. Уточните реквизиты.",
                        //    i_IsogdDocument.DocNo, i_IsogdDocument.StatementDate.ToShortDateString(), i_IsogdDocument.IsogdDocumentClass.Code));
                        isExist = true;
                        var form = new CheckDuplicateForm
                        {
                            StartPosition = FormStartPosition.CenterParent,
                            label1 =
                            {
                        Text = String.Format(
                            @"Документ с реквизитами № {0} от {1} код класса {2} уже существует в системе. Уточните реквизиты.
Для того, чтобы открыть документ с данными реквизитами нажмите на кнопку 'Показать документ'.",
                            i_IsogdDocument.DocNo, i_IsogdDocument.StatementDate.ToShortDateString(), i_IsogdDocument.IsogdDocumentClass.Code)
                            }
                        };
                        form.ShowDialog();

                        //XtraMessageBoxForm xtraMessageBoxForm;
                        var result = form.DialogResult;
                        if (result == DialogResult.Cancel)
                        {
                            return;
                        }

                        if (result == DialogResult.OK)
                        {
                            // открываем окно документа 
                            DetailView dv = Application.CreateDetailView(os, isogdDoc);//Specify the IsRoot parameter if necessary.
                            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.View;
                            e.ShowViewParameters.CreatedView = dv;
                        }
                    }
            }
            if(isExist == false)
            {
                XtraMessageBox.Show(String.Format("Документ с реквизитами № {0} от {1} код класса {2} не найден в реестрах ИСОГД. Вы можете продолжить регистрацию документа.",
                                            i_IsogdDocument.DocNo, i_IsogdDocument.StatementDate.ToShortDateString(), i_IsogdDocument.IsogdDocumentClass.Code));
            }
        }
    }
}
