﻿namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class IsogdDocCheckDuplicatesController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CheckDocDublicateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CheckDocDublicateAction
            // 
            this.CheckDocDublicateAction.Caption = "Проверить на дублирование";
            this.CheckDocDublicateAction.Category = "CheckDocDublicate";
            this.CheckDocDublicateAction.ConfirmationMessage = null;
            this.CheckDocDublicateAction.Id = "CheckDocDublicateAction";
            this.CheckDocDublicateAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.CheckDocDublicateAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdDocument);
            this.CheckDocDublicateAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CheckDocDublicateAction.ToolTip = null;
            this.CheckDocDublicateAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CheckDocDublicateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CheckDocDublicateAction_Execute);
            // 
            // IsogdDocCheckDuplicatesController
            // 
            this.Actions.Add(this.CheckDocDublicateAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CheckDocDublicateAction;
    }
}
