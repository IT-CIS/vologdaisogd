using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.XtraPrinting;
using DevExpress.XtraRichEdit.API.Native;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using System.Collections;
using AISOGD.Isogd;
using DevExpress.Xpo.DB;
using DevExpress.XtraRichEdit;
using System.Reflection;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class InDataBookReport : ViewController
    {
        public InDataBookReport()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void InDataBookReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            UnitOfWork unitOfWork = (UnitOfWork)connect.GetSession();

            // ������� ������������ �� ��� ������� ������
            Municipality mun = connect.FindFirstObject<Municipality>(mc =>
                mc.dMunicipalityKind.Code == "1" || mc.dMunicipalityKind.Code == "2");

            string oktmo = "";
            string munname = "";
            string openBookDate = "";
            string closeBookDate = DateTime.Now.ToShortDateString();
            if (mun.Name != null || mun.Name != "")
                munname = mun.Name;
            if (mun.OKTMO != null || mun.OKTMO != "")
                oktmo = mun.OKTMO;

            IsogdBooksCard i_IsogdBooksCard = connect.FindFirstObject<IsogdBooksCard>(mc => 
                mc.IsogdBooksKind == AISOGD.Enums.eIsogdBooksKind.������������������);
            if (i_IsogdBooksCard != null)
                if (i_IsogdBooksCard.RecordDate != null)
                    openBookDate = i_IsogdBooksCard.RecordDate.ToShortDateString();

            //IEnumerable<IsogdBooksCard> isogdBooksCardList = connect.FindObjects<IsogdBooksCard>(mc=> mc.Oid!=null)
            //    .AsEnumerable<IsogdBooksCard>;
            SortingCollection sortCollection = new SortingCollection();
            sortCollection.Add(new SortProperty("RecordDate", SortingDirection.Ascending));

            ICollection IsogdInDataCardList = unitOfWork.GetObjects(connect.GetClassInfo<IsogdInDataCard>(),
                CriteriaOperator.Parse("1 = 1"),
                sortCollection, Int32.MaxValue, false, false);
            //#region #serverprint
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\InDataBookTemplate.rtf");

            richServer.LoadDocumentTemplate(templatePath);
            //var doc = XDocument.Load(Path.Combine(assemblyFile, "isogd-config.xml"));

            //richServer.LoadDocumentTemplate(@"C:\XAF\WinAisogdMapInfo\DocTemplates\BooksListTemplate.rtf");
            string text = "";
            Bookmark bookmarkMO = null;
            Bookmark bookmarkOKTMO = null;
            Bookmark bookmarkOpenBookDate = null;
            Bookmark bookmarkCloseBookDate = null;
            // ������� ������� �������� � �������
            foreach (Bookmark bookmark in richServer.Document.Bookmarks)
            {
                if (bookmark.Name == "MOName")
                    bookmarkMO = bookmark;
                if (bookmark.Name == "OKTMO")
                    bookmarkOKTMO = bookmark;
                if (bookmark.Name == "OpeningTomeDate")
                    bookmarkOpenBookDate = bookmark;
                if (bookmark.Name == "ClosingTomeDate")
                    bookmarkCloseBookDate = bookmark;
            }
            //DocumentPosition pos = bookmark.Range.Start;
            //           richServer.Document.InsertText(pos, "asdadadadsada");
            // ����� ������ � �������� ����������
            richServer.Document.Replace(bookmarkMO.Range, munname);
            richServer.Document.Replace(bookmarkOKTMO.Range, oktmo);
            richServer.Document.Replace(bookmarkOpenBookDate.Range, openBookDate);
            richServer.Document.Replace(bookmarkCloseBookDate.Range, closeBookDate);

            // ��������� ������ � �������
            int i = 1;
            DocumentRange range = richServer.Document.CreateRange(0, 0);
            foreach (IsogdInDataCard isogdInDataCard in IsogdInDataCardList)
            {
                if (richServer.Document.Tables[0].Rows.Count < i + 2)
                    richServer.Document.Tables[0].Rows.InsertAfter(i);

                range = richServer.Document.Tables[0].Rows[i + 1].Cells[0].ContentRange;
                richServer.Document.Replace(range, (i).ToString());
                if (isogdInDataCard.RecordDate != DateTime.MinValue)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[1].ContentRange;
                    richServer.Document.Replace(range, isogdInDataCard.RecordDate.ToShortDateString());
                }
                if (isogdInDataCard.IsogdLetter != null)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[2].ContentRange;
                    if (isogdInDataCard.IsogdLetter.ReasonKind == AISOGD.Enums.eReasonKind.�����������������)
                        if (isogdInDataCard.IsogdLetter.ReasonDoc != null)
                            richServer.Document.Replace(range, isogdInDataCard.IsogdLetter.ReasonDoc);
                    if (isogdInDataCard.IsogdLetter.ReasonKind == AISOGD.Enums.eReasonKind.����������������������)
                        if (isogdInDataCard.IsogdLetter.requesterString != null)
                            richServer.Document.Replace(range, isogdInDataCard.IsogdLetter.requesterString);

                    string letter = "";
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[3].ContentRange;
                    if (isogdInDataCard.IsogdLetter.OutNo != null)
                        letter = "� " + isogdInDataCard.IsogdLetter.OutNo;
                    if (isogdInDataCard.IsogdLetter.OutDate != DateTime.MinValue)
                        letter += " �� " + isogdInDataCard.IsogdLetter.OutDate.ToShortDateString();
                    richServer.Document.Replace(range, letter);
                }

                if (isogdInDataCard.DocNo != null)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[4].ContentRange;
                    richServer.Document.Replace(range, isogdInDataCard.DocNo);
                }
                if (isogdInDataCard.DocName != null )
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[5].ContentRange;
                    richServer.Document.Replace(range, isogdInDataCard.DocName);
                }
                if (isogdInDataCard.DocumentVolume != null)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[6].ContentRange;
                    richServer.Document.Replace(range, isogdInDataCard.DocumentVolume);
                }
                if (isogdInDataCard.IsogdDataStorageKind != null)
                    if (isogdInDataCard.IsogdDataStorageKind.Name != null)
                    {
                        range = richServer.Document.Tables[0].Rows[i + 1].Cells[7].ContentRange;
                        richServer.Document.Replace(range, isogdInDataCard.IsogdDataStorageKind.Name);
                    }
                if (isogdInDataCard.IsogdDocument != null)
                    if (isogdInDataCard.IsogdDocument.PlacementDate != DateTime.MinValue)
                    {
                        range = richServer.Document.Tables[0].Rows[i + 1].Cells[8].ContentRange;
                        richServer.Document.Replace(range, isogdInDataCard.IsogdDocument.PlacementDate.ToShortDateString());
                    }
                if (isogdInDataCard.IsogdDocument != null)
                    if (isogdInDataCard.IsogdDocument.RegNo != null)
                    {
                        range = richServer.Document.Tables[0].Rows[i + 1].Cells[9].ContentRange;
                        richServer.Document.Replace(range, isogdInDataCard.IsogdDocument.RegNo);
                    }
                if (isogdInDataCard.IsogdDocument != null)
                    if (isogdInDataCard.IsogdDocument.Empl != null)
                        if (isogdInDataCard.IsogdDocument.Empl.BriefName != null)
                    {
                        range = richServer.Document.Tables[0].Rows[i + 1].Cells[10].ContentRange;
                        richServer.Document.Replace(range, isogdInDataCard.IsogdDocument.Empl.BriefName);
                    }
                i++;
            }
            
            //// Invoke the Print Preview dialog
            using (PrintingSystem printingSystem = new PrintingSystem())
            {
                using (PrintableComponentLink link = new PrintableComponentLink(printingSystem))
                {
                    link.Component = richServer;
                    link.CreateDocument();
                    link.ShowPreviewDialog();
                }
            }
            //#endregion #serverprint
        }
    }
}
