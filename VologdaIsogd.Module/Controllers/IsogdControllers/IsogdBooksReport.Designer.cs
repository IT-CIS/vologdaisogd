namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class IsogdBooksReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IsogdBooksReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // IsogdBooksReportAction
            // 
            this.IsogdBooksReportAction.Caption = "������������ ������ ���� �����";
            this.IsogdBooksReportAction.ConfirmationMessage = null;
            this.IsogdBooksReportAction.Id = "IsogdBooksReportAction";
            this.IsogdBooksReportAction.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.IsogdBooksReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdBooksCard);
            this.IsogdBooksReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.IsogdBooksReportAction.ToolTip = null;
            this.IsogdBooksReportAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.IsogdBooksReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdBooksReportAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IsogdBooksReportAction;
    }
}
