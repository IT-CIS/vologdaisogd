namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class IsogdReqBookReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IsogdReqBookReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // IsogdReqBookReportAction
            // 
            this.IsogdReqBookReportAction.Caption = "������������ ����� ����� ������";
            this.IsogdReqBookReportAction.ConfirmationMessage = null;
            this.IsogdReqBookReportAction.Id = "IsogdReqBookReportAction";
            this.IsogdReqBookReportAction.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.IsogdReqBookReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdRequestCard);
            this.IsogdReqBookReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.IsogdReqBookReportAction.ToolTip = null;
            this.IsogdReqBookReportAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.IsogdReqBookReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdReqBookReportAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IsogdReqBookReportAction;
    }
}
