﻿namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    partial class IsogdAbstractRequestController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IsogdAbstractRequestController));
            this.IsogdAbstractGetDocsFromGISAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.IsogdAbstractExportDocsAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // IsogdAbstractGetDocsFromGISAction
            // 
            this.IsogdAbstractGetDocsFromGISAction.Caption = "Определить документы ИСОГД из ГИС";
            this.IsogdAbstractGetDocsFromGISAction.ConfirmationMessage = null;
            this.IsogdAbstractGetDocsFromGISAction.Id = "IsogdAbstractGetDocsFromGISAction";
            this.IsogdAbstractGetDocsFromGISAction.ToolTip = "Документы ИСОГД для выгрузки определяются \r\nпространственным запросом по территор" +
    "ии запроса";
            this.IsogdAbstractGetDocsFromGISAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdAbstractGetDocsFromGISAction_Execute);
            // 
            // IsogdAbstractExportDocsAction
            // 
            this.IsogdAbstractExportDocsAction.Caption = "Выгрузить документы";
            this.IsogdAbstractExportDocsAction.ConfirmationMessage = null;
            this.IsogdAbstractExportDocsAction.Id = "IsogdAbstractExportDocsAction";
            this.IsogdAbstractExportDocsAction.ToolTip = resources.GetString("IsogdAbstractExportDocsAction.ToolTip");
            this.IsogdAbstractExportDocsAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IsogdAbstractExportDocsAction_Execute);
            // 
            // IsogdAbstractRequestController
            // 
            this.Actions.Add(this.IsogdAbstractGetDocsFromGISAction);
            this.Actions.Add(this.IsogdAbstractExportDocsAction);
            this.TargetObjectType = typeof(AISOGD.Isogd.IsogdAbstractRequest);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IsogdAbstractGetDocsFromGISAction;
        private DevExpress.ExpressApp.Actions.SimpleAction IsogdAbstractExportDocsAction;
    }
}
