using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using AISOGD.OrgStructure;
using AISOGD.Isogd;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.Reflection;
using DevExpress.XtraPrinting;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class IsogdBillReport : ViewController
    {
        public IsogdBillReport()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void IsogdBillReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IsogdRequestCard i_IsogdRequestCard;
            i_IsogdRequestCard = (IsogdRequestCard)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdRequestCard.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            // ������� ������������ �� ��� ������� ������
            Municipality mun = connect.FindFirstObject<Municipality>(mc =>
                mc.dMunicipalityKind.Code == "1" || mc.dMunicipalityKind.Code == "2");

            string munadminname = "";
            string inn= "       ";
            string kpp = "       ";
            string rs = "                       ";
            string bankname = "";
            string bik = "         ";
            string ks = "                ";
            string requester = "";
            string requesteraddress = "";
            string moneysum = "";
            string requesinfo = "";
            string date = DateTime.Now.ToShortDateString();

            MainDepartment mainDepartment = connect.FindFirstObject<MainDepartment>(mc => 
                mc.Municipality == mun);
            if (mainDepartment != null)
            {
                if (mainDepartment.Name != null)
                    munadminname = mainDepartment.Name;

                MunicipalityBankDetails bankDetails = connect.FindFirstObject<MunicipalityBankDetails>(mc => 
                mc.MainDepartment == mainDepartment);
                if (bankDetails != null)
                {
                    if (bankDetails.INN != null && bankDetails.INN != "")
                        inn = bankDetails.INN;
                    if (bankDetails.KPP != null && bankDetails.KPP != "")
                        kpp = bankDetails.KPP;
                    if (bankDetails.AccountNo != null && bankDetails.AccountNo != "")
                        rs = bankDetails.AccountNo;
                    if (bankDetails.BankName != null)
                        bankname = bankDetails.BankName;
                    if (bankDetails.Bik != null && bankDetails.Bik != "")
                        bik = bankDetails.Bik;
                    if (bankDetails.CorrAccount != null && bankDetails.CorrAccount != "")
                        ks = bankDetails.CorrAccount;
                }
            }
            if (i_IsogdRequestCard != null)
            {
                try {
                    requester = i_IsogdRequestCard.Requester.FullName;
                }
                catch { }
                try
                {
                    requesteraddress = i_IsogdRequestCard.Requester.ContactAddress;
                }
                catch { }
                //if (i_IsogdRequestCard.RequesterType == AISOGD.Enums.eSubjectType.���)
                //{
                //    if (i_IsogdRequestCard.RequesterPerson != null)
                //    {
                //        if (i_IsogdRequestCard.RequesterPerson.BriefName != null)
                //        {
                //            requester = i_IsogdRequestCard.RequesterPerson.BriefName;
                //        }
                //        if (i_IsogdRequestCard.RequesterPerson.ContactAddress != null)
                //        {
                //            requesteraddress = i_IsogdRequestCard.RequesterPerson.ContactAddress;
                //        }
                //    }
                //}
                //else
                //{
                //    if (i_IsogdRequestCard.RequesterOrg != null)
                //    {
                //        if (i_IsogdRequestCard.RequesterOrg.BriefName != null)
                //        {
                //            requester = i_IsogdRequestCard.RequesterOrg.BriefName;
                //        }
                //        if (i_IsogdRequestCard.RequesterOrg.ContactAddress != null)
                //        {
                //            requesteraddress = i_IsogdRequestCard.RequesterOrg.ContactAddress;
                //        }
                //    }
                //}
                if (i_IsogdRequestCard.descript != null)
                    requesinfo = i_IsogdRequestCard.descript;
                if (i_IsogdRequestCard.MoneySum != null)
                    moneysum = i_IsogdRequestCard.MoneySum;
            }

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\IsogdBillTemplate.rtf");

            richServer.LoadDocumentTemplate(templatePath);

            Bookmark AccountNo1 = null;
            Bookmark AccountNo2 = null;
            Bookmark BankName1 = null;
            Bookmark BankName2 = null;
            Bookmark Bik1 = null;
            Bookmark Bik2 = null;
            Bookmark CorrAccount1 = null;
            Bookmark CorrAccount2 = null;
            Bookmark Inn1 = null;
            Bookmark Inn2 = null;
            Bookmark Kpp1 = null;
            Bookmark Kpp2 = null;
            Bookmark OMSUName1 = null;
            Bookmark OMSUName2 = null;

            Bookmark Requester1 = null;
            Bookmark Requester2 = null;
            Bookmark RequesterAdress1 = null;
            Bookmark RequesterAdress2 = null;

            Bookmark IsogdRequestInfo1 = null;
            Bookmark IsogdRequestInfo2 = null;

            Bookmark Date1 = null;
            Bookmark Date2 = null;

            Bookmark MoneySum1 = null;
            Bookmark MoneySum2 = null;
            

            // ������� ������� �������� � �������
            foreach (Bookmark bookmark in richServer.Document.Bookmarks)
            {
                if (bookmark.Name == "AccountNo1")
                    AccountNo1 = bookmark;
                if (bookmark.Name == "AccountNo2")
                    AccountNo2 = bookmark;
                if (bookmark.Name == "BankName1")
                    BankName1 = bookmark;
                if (bookmark.Name == "BankName2")
                    BankName2 = bookmark;
                if (bookmark.Name == "Bik1")
                    Bik1 = bookmark;
                if (bookmark.Name == "Bik2")
                    Bik2 = bookmark;
                if (bookmark.Name == "CorrAccount1")
                    CorrAccount1 = bookmark;
                if (bookmark.Name == "CorrAccount2")
                    CorrAccount2 = bookmark;
                if (bookmark.Name == "Inn1")
                    Inn1 = bookmark;
                if (bookmark.Name == "Inn2")
                    Inn2 = bookmark;
                if (bookmark.Name == "Kpp1")
                    Kpp1 = bookmark;
                if (bookmark.Name == "Kpp2")
                    Kpp2 = bookmark;
                if (bookmark.Name == "OMSUName1")
                    OMSUName1 = bookmark;
                if (bookmark.Name == "OMSUName2")
                    OMSUName2 = bookmark;
                if (bookmark.Name == "Requester1")
                    Requester1 = bookmark;
                if (bookmark.Name == "Requester2")
                    Requester2 = bookmark;
                if (bookmark.Name == "RequesterAdress1")
                    RequesterAdress1 = bookmark;
                if (bookmark.Name == "RequesterAdress2")
                    RequesterAdress2 = bookmark;
                if (bookmark.Name == "IsogdRequestInfo1")
                    IsogdRequestInfo1 = bookmark;
                if (bookmark.Name == "IsogdRequestInfo2")
                    IsogdRequestInfo2 = bookmark;
                if (bookmark.Name == "Date1")
                    Date1 = bookmark;
                if (bookmark.Name == "Date2")
                    Date2 = bookmark;
                if (bookmark.Name == "MoneySum1")
                    MoneySum1 = bookmark;
                if (bookmark.Name == "MoneySum2")
                    MoneySum2 = bookmark;
            }

            // ����� ������ � �������� ����������
            richServer.Document.Replace(AccountNo1.Range, rs);
            richServer.Document.Replace(AccountNo2.Range, rs);
            richServer.Document.Replace(BankName1.Range, bankname);
            richServer.Document.Replace(BankName2.Range, bankname);

            richServer.Document.Replace(Bik1.Range, bik);
            richServer.Document.Replace(Bik2.Range, bik);
            richServer.Document.Replace(CorrAccount1.Range, ks);
            richServer.Document.Replace(CorrAccount2.Range, ks);

            richServer.Document.Replace(Inn1.Range, inn);
            richServer.Document.Replace(Inn2.Range, inn);
            richServer.Document.Replace(Kpp1.Range, kpp);
            richServer.Document.Replace(Kpp2.Range, kpp);

            richServer.Document.Replace(OMSUName1.Range, munadminname);
            richServer.Document.Replace(OMSUName2.Range, munadminname);
            richServer.Document.Replace(Requester1.Range, requester);
            richServer.Document.Replace(Requester2.Range, requester);

            richServer.Document.Replace(RequesterAdress1.Range, requesteraddress);
            richServer.Document.Replace(RequesterAdress2.Range, requesteraddress);
            richServer.Document.Replace(IsogdRequestInfo1.Range, requesinfo);
            richServer.Document.Replace(IsogdRequestInfo2.Range, requesinfo);

            richServer.Document.Replace(Date1.Range, date);
            richServer.Document.Replace(Date2.Range, date);
            richServer.Document.Replace(MoneySum1.Range, moneysum);
            richServer.Document.Replace(MoneySum2.Range, moneysum);

            //// Invoke the Print Preview dialog
            using (PrintingSystem printingSystem = new PrintingSystem())
            {
                using (PrintableComponentLink link = new PrintableComponentLink(printingSystem))
                {
                    link.Component = richServer;
                    link.CreateDocument();
                    link.ShowPreviewDialog();
                }
            }
        }
    }
}
