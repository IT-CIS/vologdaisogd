﻿namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    partial class AddDocsFromPhysStorBookController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddDocsFromPhysStorBookAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AddDocsFromPhysStorBookAction
            // 
            this.AddDocsFromPhysStorBookAction.Caption = "Добавить документы из ДЗУ";
            this.AddDocsFromPhysStorBookAction.ConfirmationMessage = null;
            this.AddDocsFromPhysStorBookAction.Id = "AddDocsFromPhysStorBookAction";
            this.AddDocsFromPhysStorBookAction.ToolTip = null;
            // 
            // AddDocsFromPhysStorBookController
            // 
            this.Actions.Add(this.AddDocsFromPhysStorBookAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction AddDocsFromPhysStorBookAction;
    }
}
