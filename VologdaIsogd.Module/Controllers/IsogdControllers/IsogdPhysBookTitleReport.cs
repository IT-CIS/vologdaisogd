﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Isogd;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.XtraRichEdit;
using System.Reflection;
using DevExpress.XtraRichEdit.API.Native;
using System.IO;

namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IsogdPhysBookTitleReport : ViewController
    {
        public IsogdPhysBookTitleReport()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция формирования титульного листа ДЗУ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IsogdPhysBookTitleReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IsogdPhysStorageBook i_IsogdPhysStorageBook;
            i_IsogdPhysStorageBook = (IsogdPhysStorageBook)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdPhysStorageBook.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            CreateTitlePhysStorBookTemplate(i_IsogdPhysStorageBook, unitOfWork);
        }
        public void CreateTitlePhysStorBookTemplate(IsogdPhysStorageBook isogdPhysStorageBook, UnitOfWork unitOfWork)
        {
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\TitleIsogdPhysBook.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            // вносим в документ 
            string ObjectInfo = "";
            try
            {
                try { ObjectInfo = isogdPhysStorageBook.ObjectInfo; }
                catch { }
                Bookmark ObjectInfoBook = richServer.Document.Bookmarks["ObjectInfo"];
                richServer.Document.Replace(ObjectInfoBook.Range, ObjectInfo);
            }
            catch { }
            
            string ObjectAddress = "";
            try
            {
                try { ObjectAddress = isogdPhysStorageBook.AddressInfo; }
                catch { }
                Bookmark ObjectAddressBook = richServer.Document.Bookmarks["ObjectAddress"];
                richServer.Document.Replace(ObjectAddressBook.Range, ObjectAddress);
            }
            catch { }
            string ReserveNo = "";
            try
            {
                try { ReserveNo = isogdPhysStorageBook.ReserveNo; }
                catch { }
                Bookmark ReserveNoBook = richServer.Document.Bookmarks["ReserveNo"];
                richServer.Document.Replace(ReserveNoBook.Range, ReserveNo);
            }
            catch { }
            string ProjectNo = "";
            try
            {
                try { ProjectNo = isogdPhysStorageBook.ProjectNo; }
                catch { }
                Bookmark ProjectNoBook = richServer.Document.Bookmarks["ProjectNo"];
                richServer.Document.Replace(ProjectNoBook.Range, ProjectNo);
            }
            catch { }
            string ConstrNo = "";
            try
            {
                try { ConstrNo = isogdPhysStorageBook.ConstrNo; }
                catch { }
                Bookmark ConstrNoBook = richServer.Document.Bookmarks["ConstrNo"];
                richServer.Document.Replace(ConstrNoBook.Range, ConstrNo);
            }
            catch { }
            string SurveyNo = "";
            try
            {
                try { SurveyNo = isogdPhysStorageBook.SurveyNo; }
                catch { }
                Bookmark SurveyNoBook = richServer.Document.Bookmarks["SurveyNo"];
                richServer.Document.Replace(SurveyNoBook.Range, SurveyNo);
            }
            catch { }
            string BookNo = "";
            try
            {
                try { BookNo = isogdPhysStorageBook.BookNo; }
                catch { }
                Bookmark BookNo1Book = richServer.Document.Bookmarks["BookNo1"];
                richServer.Document.Replace(BookNo1Book.Range, BookNo);
                Bookmark BookNo2Book = richServer.Document.Bookmarks["BookNo2"];
                richServer.Document.Replace(BookNo2Book.Range, BookNo);
                Bookmark BookNo3Book = richServer.Document.Bookmarks["BookNo3"];
                richServer.Document.Replace(BookNo3Book.Range, BookNo);
            }
            catch { }
            string CadNoms = "";
            try
            {
                try { CadNoms = isogdPhysStorageBook.CadNumbers.Trim().TrimEnd(',').Trim(); }
                catch { }
                Bookmark CadNomsBook = richServer.Document.Bookmarks["CadNoms"];
                richServer.Document.Replace(CadNomsBook.Range, CadNoms);
            }
            catch { }
            string OpenenigBookDate = "";
            try
            {
                try {
                    if (isogdPhysStorageBook.OpenenigBookDate != DateTime.MinValue)
                        OpenenigBookDate = isogdPhysStorageBook.OpenenigBookDate.ToShortDateString();
                    else
                        OpenenigBookDate = DateTime.Now.Date.ToShortDateString();
                }
                catch { }
                Bookmark OpenenigBookDateBook = richServer.Document.Bookmarks["OpenenigBookDate"];
                richServer.Document.Replace(OpenenigBookDateBook.Range, OpenenigBookDate);
            }
            catch { }

            string path = Path.GetTempPath() + @"\IsogdPhysStorageBookTitle_" + isogdPhysStorageBook.Oid.ToString() + "_" +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".doc";// +"_" + DateTime.Now.Date.ToString(); 
            // Save the document file under the specified name.
            //workbook.SaveDocument(Path.GetTempPath() + @"\TestDoc.xlsx", DocumentFormat.OpenXml);
            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        /// <summary>
        /// Сформировать опись(лист) документов в ДЗУ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IsogdPhysBookDocsListReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IsogdPhysStorageBook i_IsogdPhysStorageBook;
            i_IsogdPhysStorageBook = (IsogdPhysStorageBook)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdPhysStorageBook.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            CreateDocListPhysStorBookTemplate(i_IsogdPhysStorageBook, unitOfWork);
        }
        public void CreateDocListPhysStorBookTemplate(IsogdPhysStorageBook isogdPhysStorageBook, UnitOfWork unitOfWork)
        {
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\IsogdDocsListInPhysBook.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            // вносим в документ 
            string BookNo = "";
            try
            {
                try { BookNo = isogdPhysStorageBook.BookNo; }
                catch { }
                Bookmark BookNoBook = richServer.Document.Bookmarks["BookNo"];
                richServer.Document.Replace(BookNoBook.Range, BookNo);
            }
            catch { }

            string CadRegion = "35:24";
            try
            {
                try { CadRegion = isogdPhysStorageBook.CadNumbers.Split(':')[0] + ":" + isogdPhysStorageBook.CadNumbers.Split(':')[1]; }
                catch { }
                Bookmark CadRegionBook = richServer.Document.Bookmarks["CadRegion"];
                richServer.Document.Replace(CadRegionBook.Range, CadRegion);
            }
            catch { }

            string CadNumbers = "";
            try
            {
                try { CadNumbers = isogdPhysStorageBook.CadNumbers.Trim().TrimEnd(',').Trim(); }
                catch { }
                Bookmark CadNumbersBook = richServer.Document.Bookmarks["CadNumbers"];
                richServer.Document.Replace(CadNumbersBook.Range, CadNumbers);
            }
            catch { }

            string ObjectInfo = "";
            try
            {
                try { ObjectInfo = isogdPhysStorageBook.ObjectInfo; }
                catch { }
                Bookmark ObjectInfoBook = richServer.Document.Bookmarks["ObjectInfo"];
                richServer.Document.Replace(ObjectInfoBook.Range, ObjectInfo);
            }
            catch { }

            string ObjectAddress = "";
            try
            {
                try { ObjectAddress = isogdPhysStorageBook.AddressInfo; }
                catch { }
                Bookmark ObjectAddressBook = richServer.Document.Bookmarks["ObjectAddress"];
                richServer.Document.Replace(ObjectAddressBook.Range, ObjectAddress);
            }
            catch { }
            string PartNo = "08";
            try
            {
                try { PartNo = isogdPhysStorageBook.IsogdDocuments[0].IsogdPartition.PartNo;
                    if (PartNo.Length == 1)
                        PartNo = "0" + PartNo;
                }
                catch { }
                Bookmark PartNoBook = richServer.Document.Bookmarks["PartNo"];
                richServer.Document.Replace(PartNoBook.Range, PartNo);
            }
            catch { }

            string NowDate = "";
            try
            {
                try { NowDate = DateTime.Now.Date.ToShortDateString(); }
                catch { }
                Bookmark NowDateBook = richServer.Document.Bookmarks["NowDate"];
                richServer.Document.Replace(NowDateBook.Range, NowDate);
            }
            catch { }

            try {
                Table table = richServer.Document.Tables[1];

                // очищаем данные в таблице
                for (int i = 0; i < table.Rows[2].Cells.Count; i++)
                {
                    range = table.Rows[2].Cells[i].ContentRange;
                    richServer.Document.Replace(range, "");
                }

                // вставляем данные в таблицу по документам
                int j = 0;
                
                foreach (IsogdDocument isogdDoc in isogdPhysStorageBook.IsogdDocuments)
                {
                    string no = "";
                    string docclass = "";
                    string docname = "";
                    string docautor = "";
                    string docno = "";
                    string docdate = "";
                    string folderno = isogdPhysStorageBook.BookNo;

                    no = (j + 1).ToString();
                    try { docclass = isogdDoc.IsogdDocumentClass.Code; }
                    catch { }
                    try { docname = isogdDoc.DocName; }
                    catch { }
                    try { docautor = isogdDoc.Developer.Name; }
                    catch { }
                    try { docno = isogdDoc.DocNo; }
                    catch { }
                    try { docdate = isogdDoc.StatementDate.ToShortDateString(); }
                    catch { }
                    try { folderno = isogdDoc.FolderNo; }
                    catch { }
                    if (j == 0)
                    {
                        range = table.Rows[2].Cells[0].ContentRange;
                        richServer.Document.Replace(range, no);

                        range = table.Rows[2].Cells[1].ContentRange;
                        richServer.Document.Replace(range, docclass);

                        range = table.Rows[2].Cells[2].ContentRange;
                        richServer.Document.Replace(range, docname);

                        range = table.Rows[2].Cells[3].ContentRange;
                        richServer.Document.Replace(range, docautor);

                        range = table.Rows[2].Cells[4].ContentRange;
                        richServer.Document.Replace(range, docno);

                        range = table.Rows[2].Cells[5].ContentRange;
                        richServer.Document.Replace(range, docdate);

                        range = table.Rows[2].Cells[6].ContentRange;
                        richServer.Document.Replace(range, folderno);
                    }
                    // если объектов больше 1 копируем таблицу и вставляем новые значения
                    else
                    {
                        table.Rows.InsertAfter(table.LastRow.Index);

                        range = table.Rows[j+2].Cells[0].ContentRange;
                        richServer.Document.Replace(range, no);

                        range = table.Rows[j + 2].Cells[1].ContentRange;
                        richServer.Document.Replace(range, docclass);

                        range = table.Rows[j + 2].Cells[2].ContentRange;
                        richServer.Document.Replace(range, docname);

                        range = table.Rows[j + 2].Cells[3].ContentRange;
                        richServer.Document.Replace(range, docautor);

                        range = table.Rows[j + 2].Cells[4].ContentRange;
                        richServer.Document.Replace(range, docno);

                        range = table.Rows[j + 2].Cells[5].ContentRange;
                        richServer.Document.Replace(range, docdate);

                        range = table.Rows[j + 2].Cells[6].ContentRange;
                        richServer.Document.Replace(range, folderno);
                    }
                    j++;
                }
            }
            catch { }

            string path = Path.GetTempPath() + @"\IsogdPhysStorageBookDocList_" + isogdPhysStorageBook.Oid.ToString() + "_" +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".doc";// +"_" + DateTime.Now.Date.ToString(); 
            // Save the document file under the specified name.
            //workbook.SaveDocument(Path.GetTempPath() + @"\TestDoc.xlsx", DocumentFormat.OpenXml);
            richServer.SaveDocument(path, DocumentFormat.OpenXml);

            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }
    }
}
