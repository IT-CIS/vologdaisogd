using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using AISOGD.OrgStructure;
using AISOGD.Isogd;
using System.Collections;
using DevExpress.Xpo.DB;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.Reflection;
using DevExpress.XtraPrinting;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class IsogdReqBookReport : ViewController
    {
        public IsogdReqBookReport()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void IsogdReqBookReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            UnitOfWork unitOfWork = (UnitOfWork)connect.GetSession();

            // ������� ������������ �� ��� ������� ������
            Municipality mun = connect.FindFirstObject<Municipality>(mc =>
                mc.dMunicipalityKind.Code == "1" || mc.dMunicipalityKind.Code == "2");

            string oktmo = "";
            string munname = "";
            string openBookDate = "";
            string closeBookDate = DateTime.Now.ToShortDateString();
            if (mun.Name != null || mun.Name != "")
                munname = mun.Name;
            if (mun.OKTMO != null || mun.OKTMO != "")
                oktmo = mun.OKTMO;

            IsogdBooksCard i_IsogdBooksCard = connect.FindFirstObject<IsogdBooksCard>(mc =>
                mc.IsogdBooksKind == AISOGD.Enums.eIsogdBooksKind.����������������);
            if (i_IsogdBooksCard != null)
                if (i_IsogdBooksCard.RecordDate != null)
                    openBookDate = i_IsogdBooksCard.RecordDate.ToShortDateString();

            //IEnumerable<IsogdBooksCard> isogdBooksCardList = connect.FindObjects<IsogdBooksCard>(mc=> mc.Oid!=null)
            //    .AsEnumerable<IsogdBooksCard>;
            SortingCollection sortCollection = new SortingCollection();
            sortCollection.Add(new SortProperty("RecordDate", SortingDirection.Ascending));

            ICollection IsogdRequestBookList = unitOfWork.GetObjects(connect.GetClassInfo<IsogdRequestCard>(),
                CriteriaOperator.Parse("1 = 1"),
                sortCollection, Int32.MaxValue, false, false);
            //#region #serverprint
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\IsogdReqBookTemplate.rtf");

            richServer.LoadDocumentTemplate(templatePath);
            string text = "";
            Bookmark bookmarkMO = null;
            Bookmark bookmarkOKTMO = null;
            Bookmark bookmarkOpenBookDate = null;
            Bookmark bookmarkCloseBookDate = null;
            // ������� ������� �������� � �������
            foreach (Bookmark bookmark in richServer.Document.Bookmarks)
            {
                if (bookmark.Name == "MOName")
                    bookmarkMO = bookmark;
                if (bookmark.Name == "OKTMO")
                    bookmarkOKTMO = bookmark;
                if (bookmark.Name == "OpeningTomeDate")
                    bookmarkOpenBookDate = bookmark;
                if (bookmark.Name == "ClosingTomeDate")
                    bookmarkCloseBookDate = bookmark;
            }
            //DocumentPosition pos = bookmark.Range.Start;
            //           richServer.Document.InsertText(pos, "asdadadadsada");
            // ����� ������ � �������� ����������
            richServer.Document.Replace(bookmarkMO.Range, munname);
            richServer.Document.Replace(bookmarkOKTMO.Range, oktmo);
            richServer.Document.Replace(bookmarkOpenBookDate.Range, openBookDate);
            richServer.Document.Replace(bookmarkCloseBookDate.Range, closeBookDate);

            // ��������� ������ � �������
            int i = 1;
            DocumentRange range = richServer.Document.CreateRange(0, 0);
            foreach (IsogdRequestCard isogdRequestCard in IsogdRequestBookList)
            {
                if (richServer.Document.Tables[0].Rows.Count < i + 2)
                    richServer.Document.Tables[0].Rows.InsertAfter(i);

                range = richServer.Document.Tables[0].Rows[i + 1].Cells[0].ContentRange;
                richServer.Document.Replace(range, (i).ToString());
                if (isogdRequestCard.RecordDate != DateTime.MinValue)
                {
                    range = richServer.Document.Tables[0].Rows[i + 1].Cells[1].ContentRange;
                    richServer.Document.Replace(range, isogdRequestCard.RecordDate.ToShortDateString());
                }

                range = richServer.Document.Tables[0].Rows[i + 1].Cells[2].ContentRange;
                if (isogdRequestCard.Requester != null)
                    if (isogdRequestCard.Requester.FullName != null && isogdRequestCard.Requester.FullName != "")
                        richServer.Document.Replace(range, isogdRequestCard.Requester.FullName);
                //if (isogdRequestCard.RequesterType == AISOGD.Enums.eSubjectType.���)
                //    if (isogdRequestCard.RequesterPerson != null)
                //        if (isogdRequestCard.RequesterPerson.BriefName != null)
                //            richServer.Document.Replace(range, isogdRequestCard.RequesterPerson.BriefName);
                //if (isogdRequestCard.RequesterType == AISOGD.Enums.eSubjectType.��)
                //    if (isogdRequestCard.RequesterOrg != null)
                //        if (isogdRequestCard.RequesterOrg.Name != null)
                //            richServer.Document.Replace(range, isogdRequestCard.RequesterOrg.Name);
                
                
                i++;
            }

            //// Invoke the Print Preview dialog
            using (PrintingSystem printingSystem = new PrintingSystem())
            {
                using (PrintableComponentLink link = new PrintableComponentLink(printingSystem))
                {
                    link.Component = richServer;
                    link.CreateDocument();
                    link.ShowPreviewDialog();
                }
            }
            //#endregion #serverprint
        }
    }
}

