namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class InDataBookReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InDataBookReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InDataBookReportAction
            // 
            this.InDataBookReportAction.Caption = "������������ ����� ����� ��������";
            this.InDataBookReportAction.ConfirmationMessage = null;
            this.InDataBookReportAction.Id = "InDataBookReportAction";
            this.InDataBookReportAction.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.InDataBookReportAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdInDataCard);
            this.InDataBookReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InDataBookReportAction.ToolTip = null;
            this.InDataBookReportAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InDataBookReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InDataBookReportAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InDataBookReportAction;
    }
}
