namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class FindIsogdDocsController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateDocsAttachFileAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.GetIsogdDocsFromGisAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CreateDocsAttachFileAction
            // 
            this.CreateDocsAttachFileAction.Caption = "����������� �������� ��� ������";
            this.CreateDocsAttachFileAction.ConfirmationMessage = null;
            this.CreateDocsAttachFileAction.Id = "CreateDocsAttachFileAction";
            this.CreateDocsAttachFileAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdOutDataCard);
            this.CreateDocsAttachFileAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CreateDocsAttachFileAction.ToolTip = null;
            this.CreateDocsAttachFileAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CreateDocsAttachFileAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateDocsAttachFileAction_Execute);
            // 
            // GetIsogdDocsFromGisAction
            // 
            this.GetIsogdDocsFromGisAction.Caption = "���������� ��������� ����� �� ���";
            this.GetIsogdDocsFromGisAction.ConfirmationMessage = null;
            this.GetIsogdDocsFromGisAction.Id = "GetIsogdDocsFromGisAction";
            this.GetIsogdDocsFromGisAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdRequestCard);
            this.GetIsogdDocsFromGisAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.GetIsogdDocsFromGisAction.ToolTip = null;
            this.GetIsogdDocsFromGisAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.GetIsogdDocsFromGisAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GetIsogdDocsFromGisAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CreateDocsAttachFileAction;
        private DevExpress.ExpressApp.Actions.SimpleAction GetIsogdDocsFromGisAction;
    }
}
