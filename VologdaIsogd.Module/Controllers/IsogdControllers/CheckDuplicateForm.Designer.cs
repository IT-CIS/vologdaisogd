﻿namespace VologdaIsogd.Controllers.IsogdControllers
{
    partial class CheckDuplicateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.showDuplicateObjectBtn = new DevExpress.XtraEditors.SimpleButton();
            this.closeBtn = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(556, 43);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // showDuplicateObjectBtn
            // 
            this.showDuplicateObjectBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.showDuplicateObjectBtn.Location = new System.Drawing.Point(137, 68);
            this.showDuplicateObjectBtn.Name = "showDuplicateObjectBtn";
            this.showDuplicateObjectBtn.Size = new System.Drawing.Size(121, 29);
            this.showDuplicateObjectBtn.TabIndex = 1;
            this.showDuplicateObjectBtn.Text = "Показать документ";
            // 
            // closeBtn
            // 
            this.closeBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeBtn.Location = new System.Drawing.Point(313, 68);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(110, 29);
            this.closeBtn.TabIndex = 2;
            this.closeBtn.Text = "Закрыть";
            // 
            // CheckDuplicateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 108);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.showDuplicateObjectBtn);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimizeBox = false;
            this.Name = "CheckDuplicateForm";
            this.ShowIcon = false;
            this.Text = "Внимание!";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton showDuplicateObjectBtn;
        private DevExpress.XtraEditors.SimpleButton closeBtn;
    }
}