using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.XtraEditors;
using AISOGD.Isogd;
using System.Windows.Forms;
using AISOGD.Land;
using AISOGD.OrgStructure;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class RegIsogdDocController : ViewController
    {
        public RegIsogdDocController()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void RegIsogdDocAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� �������� ����� � ��� ������ (UnitOfWork)
            IsogdDocument i_IsogdDocument;
            i_IsogdDocument = (IsogdDocument)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdDocument.Session;

            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            IsogdPartition i_IsogdPartition;
            Parcel i_lot;
            IsogdStorageBook i_storBook;
            IsogdRegistrationCard i_IsogdRegistrationCard;
            IsogdInDataCard i_IsogdInDataCard;
            IsogdBooksCard i_IsogdBooksCard;
            IsogdPhysStorageBook i_PhysStorBook;
            Employee currentEmpl = null;
            // ������� ������� ������ �� � ������������
            string message = "";

            DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
            // ��������� ������������� ������
            if (i_IsogdDocument.Municipality == null)
            {
                XtraMessageBox.Show("�� ������� ������������� �����������. ��� ����������� ������� ���.");
                return;
                //throw new Exception("�� ������� ������������� �����������. ��� ����������� ������� ���.");
            }

            if (i_IsogdDocument.IsogdDocumentClass == null)
            {
                XtraMessageBox.Show("�� ������ ����� ���������. ��� ����������� ������� ���.");
                return;
                //throw new Exception("�� ������ ����� ���������. ��� ����������� ������� ���.");
            }

            if (i_IsogdDocument.IsogdPartition == null)
            {
                XtraMessageBox.Show("�� ������ ������ �����. ��� ����������� ������� ���.");
                return;
                //throw new Exception("�� ������ ������ �����. ��� ����������� ������� ���.");
            }
            else
                i_IsogdPartition = i_IsogdDocument.IsogdPartition;

            if (i_IsogdDocument.DocNo == null || i_IsogdDocument.DocNo == String.Empty)
            {
                XtraMessageBox.Show("�� ������ ����� ���������. ��� ����������� ������� ���.");
                return;
                //throw new Exception("�� ������ ����� ���������. ��� ����������� ������� ���.");
            }
            if (i_IsogdDocument.StatementDate == null)
            {
                XtraMessageBox.Show("�� ������� ���� ���������. ��� ����������� ������� ��.");
                return;
                //throw new Exception("�� ������ ����� ���������. ��� ����������� ������� ���.");
            }
            // ������� ���������, ����� � ������ ����������� ��� ���� �������� � �������
            if (i_IsogdDocument.DocNo != null && i_IsogdDocument.DocNo != "" && i_IsogdDocument.StatementDate != DateTime.MinValue && i_IsogdDocument.IsogdDocumentClass != null
                    && i_IsogdDocument.IsogdDocumentClass.Code != null && i_IsogdDocument.IsogdDocumentClass.Code != "")
            {
                IsogdDocument isogdDoc = null;
                isogdDoc = connect.FindFirstObject<IsogdDocument>(mc => mc.DocNo == i_IsogdDocument.DocNo && mc.StatementDate == i_IsogdDocument.StatementDate
                && mc.IsogdDocumentClass.Code == i_IsogdDocument.IsogdDocumentClass.Code);
                if (isogdDoc != null)
                    if (isogdDoc != i_IsogdDocument)
                    {
                        XtraMessageBox.Show(String.Format("�������� � ����������� � {0} �� {1} ��� ������ {2} ��� ���������� � �������. �������� ���������.",
                            i_IsogdDocument.DocNo, i_IsogdDocument.StatementDate.ToShortDateString(), i_IsogdDocument.IsogdDocumentClass.Code));
                        return;
                    }
            }
            
            // ����������� ���������������� ������
            // ���� ��� ����������� ��� ������ ��������� ������������ ���������� ���������� ���������� � �������, �� ��� �������� ��������� �����, ��������� ��������� �������
            // ������� ������ ���������� �� �������:
            // 1. � ������ ������� ���� ���� - ��������� �������� ��������� - � ����� �������� + 1 ����� ���� ������ ���������� � �������
            // 2. ��� �������� ���������� ����� ���������� ��������� ������� (�� ������ �������)
            string res = "1";
            int initialRegNo = 0;
            string partNom = "�� ����������";
            if (i_IsogdDocument.Municipality != null)
                if (i_IsogdDocument.Municipality.OKTMO != null)
                    res = i_IsogdDocument.Municipality.OKTMO;
            if (i_IsogdDocument.IsogdPartition != null)
            {
                if (i_IsogdDocument.IsogdPartition.PartNo != null)
                {
                    partNom = i_IsogdDocument.IsogdPartition.PartNo;
                    if (i_IsogdDocument.IsogdPartition.PartNo.Length == 1)
                        res += "0" + i_IsogdDocument.IsogdPartition.PartNo;
                    if (i_IsogdDocument.IsogdPartition.PartNo.Length > 1)
                        res += i_IsogdDocument.IsogdPartition.PartNo;
                    
                }
                if (i_IsogdDocument.IsogdPartition.InitialRegNo != null)
                {
                    initialRegNo = i_IsogdDocument.IsogdPartition.InitialRegNo;
                }
                string count = "";
                count = Convert.ToString(initialRegNo + DistributedIdGeneratorHelper.Generate(i_IsogdDocument.Session.DataLayer, partNom, string.Empty));
                //count = (connect.FindObjects<IsogdDocument>(x => x.IsogdPartition.PartNo == i_IsogdDocument.IsogdPartition.PartNo &&
                //    (x.RegNo != "" || x.RegNo != null) && x != i_IsogdDocument).Count() + 1).ToString();

                if (count.Length == 1)
                    count = String.Concat("0000", count);
                if (count.Length == 2)
                    count = String.Concat("000", count);
                if (count.Length == 3)
                    count = String.Concat("00", count);
                if (count.Length == 4)
                    count = String.Concat("0", count);
                if (count.Length > 4 )
                    count = count;
                if (count.Length == 0)
                    count = String.Concat("0000", count);
                res += count;
            }
            //XtraMessageBox.Show(res);
            if (i_IsogdDocument.RegNo == null || i_IsogdDocument.RegNo == string.Empty)
                i_IsogdDocument.RegNo = res;
            // ������� ��������������� �������� � �������� �����
            // �������� ����������� ����������
            // ������� ��������� - ���� �� ��� ����� ��������� (����� ������ ���������)
            i_IsogdRegistrationCard = connect.FindFirstObject<IsogdRegistrationCard>(mc =>
                mc.IsogdDocument == i_IsogdDocument);
            if (i_IsogdRegistrationCard == null)
            {
                i_IsogdRegistrationCard = new IsogdRegistrationCard(unitOfWork);
                i_IsogdRegistrationCard.IsogdDocument = i_IsogdDocument;
                i_IsogdRegistrationCard.IsogdPartition = i_IsogdPartition;
                i_IsogdRegistrationCard.Municipality = i_IsogdDocument.Municipality;
                i_IsogdRegistrationCard.RecordDate = DateTime.Now.Date;
                // � ������� ���� ���� ������, ��� ������ �������� ���  ����� �����������
                //try
                //{
                i_IsogdBooksCard = connect.FindFirstObject<IsogdBooksCard>(mc =>
                mc.IsogdBooksKind == AISOGD.Enums.eIsogdBooksKind.���������������� &&
                mc.ActiveTomeFlag == true);
                if (i_IsogdBooksCard != null)
                {
                    if (i_IsogdBooksCard.BookTomeNo != null)
                    {
                        i_IsogdRegistrationCard.TomeNo = i_IsogdBooksCard.BookTomeNo;
                        i_IsogdBooksCard = null;
                    }
                }
                if (i_IsogdRegistrationCard.TomeNo == null || i_IsogdRegistrationCard.TomeNo == "")
                    i_IsogdRegistrationCard.TomeNo = "1";
                //}
                //catch { }
                i_IsogdRegistrationCard.Save();
            }

            // �������� ����� ��������
            // ����������. ������� ���� ��� ���������, ���� �� ������� - �������
            i_IsogdInDataCard = connect.FindFirstObject<IsogdInDataCard>(mc =>
                mc.IsogdDocument == i_IsogdDocument);
            if (i_IsogdInDataCard == null)
            {
                i_IsogdInDataCard = new IsogdInDataCard(unitOfWork);
                i_IsogdInDataCard.IsogdDocument = i_IsogdDocument;
                i_IsogdInDataCard.RecordDate = DateTime.Now.Date;
                i_IsogdInDataCard.DocNo = "1";
                i_IsogdInDataCard.IsogdRegistrationCard = i_IsogdRegistrationCard;
                try
                {
                    dIsogdDocumentForm idf = connect.FindFirstObject<dIsogdDocumentForm>(mc =>
                        mc.Code == "2");
                    if (idf != null)
                        i_IsogdInDataCard.IsogdDocumentForm = idf;
                }
                catch { }
                try
                {
                    i_IsogdInDataCard.DocName = i_IsogdDocument.DocName;
                }
                catch { }
                try
                {
                    i_IsogdBooksCard = connect.FindFirstObject<IsogdBooksCard>(mc =>
                mc.IsogdBooksKind == AISOGD.Enums.eIsogdBooksKind.������������������ &&
                mc.ActiveTomeFlag == true);
                    if (i_IsogdBooksCard != null)
                    {
                        if (i_IsogdBooksCard.BookTomeNo != null)
                        {
                            i_IsogdInDataCard.TomeNo = i_IsogdBooksCard.BookTomeNo;
                            i_IsogdBooksCard = null;
                        }
                    }
                }
                catch { }
                try { i_IsogdInDataCard.IsogdReceivingMethod = i_IsogdDocument.IsogdReceivingMethod; }
                catch { }
                try { i_IsogdInDataCard.IsogdDocumentForm = i_IsogdDocument.IsogdDocumentForm; }
                catch { }
                try { i_IsogdInDataCard.DocumentVolume = i_IsogdDocument.DocumentVolume; }
                catch { }
                try { i_IsogdInDataCard.Municipality = i_IsogdDocument.Municipality; }
                catch { }
            }
            i_IsogdInDataCard.Save();
            // ����� ����������� �������� ����������
            if (i_IsogdDocument.Empl != null)
            {
                //currentEmpl = connect.FindFirstObject<Employee>(x=> x.Oid == i_IsogdDocument.Empl.Oid);
                i_IsogdRegistrationCard.Empl = i_IsogdDocument.Empl;
                i_IsogdInDataCard.Empl = i_IsogdDocument.Empl;
            }
            //try
            //{ 
            //    if (currentEmpl != null)
            //    {
            //        i_IsogdRegistrationCard.Empl = currentEmpl;
            //        i_IsogdInDataCard.Empl = currentEmpl;
            //    }
            //}
            //catch { }
            //try { currentEmpl = Employee.GetCurrentEmployee();
            //    if (currentEmpl != null)
            //    {
            //        i_IsogdRegistrationCard.Empl = currentEmpl;
            //        i_IsogdInDataCard.Empl = currentEmpl;
            //        i_IsogdDocument.Empl = currentEmpl;
            //    }
            //}
            //catch { }
            //if (SecuritySystem.CurrentUser != null)
            //{
            //    currentEmpl = unitOfWork.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //    if (currentEmpl != null)
            //    {
            //        i_IsogdRegistrationCard.Empl = currentEmpl;
            //        i_IsogdInDataCard.Empl = currentEmpl;
            //        i_IsogdDocument.Empl = currentEmpl;
            //    }
            //}

            i_IsogdDocument.Save();
            unitOfWork.CommitChanges();
            // ��������� ��� ������� ����� ��������.
            // ���� �������� �� �������� �������, � � ������� ��������� ���� �������� - ��������� ���� 
            //��� �������� ����� ��������.
            if (i_IsogdPartition.PartNo != null || i_IsogdPartition.PartNo != String.Empty)
                if (i_IsogdPartition.PartNo != "8" && i_IsogdPartition.PartNo != "11" && i_IsogdPartition.PartNo != "13")
                {
                    // ���� � ������� ��� ���� ��������
                    if (i_IsogdPartition.IsogdStorageBook.Count == 0)
                    {
                        message = "� ��������� ������� ����������� ����� ��������." +
                            "��� ����������� ��������� � ������ ������� ���������� �� �������������� �������.";
                        XtraMessageBox.Show(message);
                        return;
                        //throw new Exception(message);
                    }

                    // ���� � ������ ���� ������ �� ����� ��������
                    if (i_IsogdDocument.IsogdDocumentClass.IsogdStorageBook != null)
                    {
                        i_IsogdDocument.IsogdStorageBooks.Add(i_IsogdDocument.IsogdDocumentClass.IsogdStorageBook);
                        i_IsogdDocument.Save();
                        unitOfWork.CommitChanges();
                        XtraMessageBox.Show("�������� ����� ���������������!");
                    }
                    else
                    {
                        string partNo = i_IsogdPartition.PartNo;
                        string oktmo = i_IsogdDocument.Municipality.OKTMO;
                        // ���� � ������� ��������� ���� �������� - ��������� ���� � ������� �������� �������
                        if (i_IsogdPartition.IsogdStorageBook.Count > 1)
                        {
                            CollectionSourceBase source = Application.CreateCollectionSource(Application.CreateObjectSpace(), typeof(IsogdStorageBook), "123list");
                            string ID = Application.FindListViewId(typeof(IsogdStorageBook));

                            source.Criteria.Add("partNom", CriteriaOperator.Parse("IsogdPartition.PartNo=?", partNo));
                            //source.Criteria.Add("mun", CriteriaOperator.Parse("Municipality.OKTMO =?", oktmo));
                            e.ShowViewParameters.CreatedView = Application.CreateListView(ID, source, true);
                            //throw new Exception("1");
                            DialogController contr = new DialogController();
                            contr.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(contr_Accepting);
                            e.ShowViewParameters.Controllers.Add(contr);
                            //throw new Exception("2");
                            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
                            unitOfWork.CommitChanges();
                            //throw new Exception("3");
                        }
                        // ���� � ������� ���� ����� ��������
                        if (i_IsogdPartition.IsogdStorageBook.Count == 1)
                        {
                            i_IsogdDocument.IsogdStorageBooks.Add(i_IsogdPartition.IsogdStorageBook[0]);
                            i_IsogdDocument.Save();
                            unitOfWork.CommitChanges();
                            XtraMessageBox.Show("�������� ����� ���������������!");
                        }
                    }
                }
                // ���� 8, 11, 13 ������
                else
                {
                    if (i_IsogdDocument.CadNo == null || i_IsogdDocument.CadNo == "")
                    {
                        message = String.Format("��� ��� �� ������� {0} ������ ���������� ������� �����������/�� " +
                            "������ ��������� ��������, �� �������/�� ��������� �������� �����.", i_IsogdPartition.PartNo);
                        XtraMessageBox.Show(message);
                        return;
                        //throw new Exception(message);
                    }
                    else
                    {
                        // ������� �� ������������ ������� ��������
                        char[] sep = { ',', ';' };
                        string[] cadNoms = i_IsogdDocument.CadNo.Split(sep);

                        //i_PhysStorBook = connect.FindFirstObject<IsogdPhysStorageBook>(mc => mc.CadNumbers == i_IsogdDocument.CadNo);
                        //if (i_PhysStorBook != null)
                        //    i_PhysStorBook.IsogdDocuments.Add(i_IsogdDocument);
                        foreach (string c in cadNoms)
                        {
                            string cadNo = c.Trim();
                            if (cadNo != "" && cadNo != " ")
                            {
                                // ���� �� � ����� �������� � ��������� ����������� �������
                                // ���� �� ������� - ������� � ���������� �� ��������
                                // ���� ������� - ��������, ���� �� �� � ���������, ���� ��� - ����������
                                i_lot = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber == cadNo);
                                i_storBook = connect.FindFirstObject<IsogdStorageBook>(mc => mc.Code == cadNo);

                                if (i_lot != null)
                                {
                                    i_IsogdDocument.Parcels.Add(i_lot);
                                    i_IsogdDocument.Save();
                                }
                                else
                                {
                                    var xtraMessageBoxForm = new XtraMessageBoxForm();
                                    if (
                                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                            "��������� ������� � ����������� ������� " + Environment.NewLine +
                                            cadNo + Environment.NewLine +
                                            "�� ������ � �������� ������! �������?",
                                            "��������!", dialogResults, null, 0)) == DialogResult.Yes)
                                    {
                                        i_lot = new Parcel(unitOfWork);
                                        i_lot.CadastralNumber = cadNo;
                                        try { i_lot.Adress = i_IsogdDocument.AddressInfo; }
                                        catch { }
                                        i_IsogdDocument.Parcels.Add(i_lot);
                                        i_lot.Save();
                                    }
                                    //else
                                    //{
                                    //    return;
                                    //}
                                }
                                if (i_storBook != null)
                                {
                                    i_IsogdDocument.IsogdStorageBooks.Add(i_storBook);
                                    i_IsogdDocument.Save();
                                }
                                else
                                {
                                    var xtraMessageBoxForm = new XtraMessageBoxForm();
                                    if (
                                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                            "����� �������� � ����� " + Environment.NewLine +
                                            cadNo + Environment.NewLine +
                                            "�� ������� � �������� ������! �������?",
                                            "��������!", dialogResults, null, 0)) == DialogResult.Yes)
                                    {
                                        i_storBook = new IsogdStorageBook(unitOfWork);
                                        i_storBook.Code = cadNo;
                                        i_storBook.OpenenigBookDate = DateTime.Now.Date;
                                        i_storBook.Empl = i_IsogdDocument.Empl;
                                        //if (currentEmpl != null)
                                        //{
                                        //    i_storBook.Empl = currentEmpl;
                                        //}
                                        try { i_storBook.AddressInfo = i_IsogdDocument.AddressInfo; }
                                        catch { }
                                        i_storBook.Name = "����� �������� ���������� ����� ���������� ������� � ���.� " + cadNo;
                                        i_storBook.IsogdPartition = i_IsogdPartition;
                                        i_IsogdDocument.IsogdStorageBooks.Add(i_storBook);
                                        i_storBook.Save();
                                        // ������� ����� �������� - ���������� ������� ������ �� ���� � ������� ���� �����
                                        IsogdBooksCard IsogdBooksCard = connect.CreateObject<IsogdBooksCard>();
                                        IsogdBooksCard.BookTomeNo = "1";
                                        IsogdBooksCard.ActiveTomeFlag = true;
                                        IsogdBooksCard.IsogdStorageBook = i_storBook;
                                        IsogdBooksCard.IsogdBooksKind = AISOGD.Enums.eIsogdBooksKind.�������������;
                                        IsogdBooksCard.OpenenigTomeDate = DateTime.Now.Date;
                                        IsogdBooksCard.Municipality = i_IsogdDocument.Municipality;
                                        IsogdBooksCard.Empl = i_IsogdDocument.Empl;
                                        IsogdBooksCard.Save();
                                    }
                                    //else
                                    //{
                                    //    return;
                                    //}
                                }
                                if (i_storBook != null && i_lot != null)
                                {
                                    i_storBook.Parcels.Add(i_lot);
                                }
                            }
                        }
                        // ���� ������� ������ ���
                        if (i_IsogdDocument.IsogdPhysStorageBooksNo != null && i_IsogdDocument.IsogdPhysStorageBooksNo != "")
                        {
                            string[] dzuNoms = i_IsogdDocument.IsogdPhysStorageBooksNo.Split(sep);
                            foreach (string dzuNom in dzuNoms)
                            {
                                dzuNom.Trim();
                                if (dzuNom != "")
                                {
                                    // ���� ��� � ��������� �������
                                    // ���� �� ������� - ������� � ���������� �� ��������
                                    // ���� ������� - ��������, ���� �� �� � ���������, ���� ��� - ����������
                                    IsogdPhysStorageBook i_IsogdPhysStorageBook = connect.FindFirstObject<IsogdPhysStorageBook>(mc => mc.BookNo == dzuNom);
                                    if (i_IsogdPhysStorageBook != null)
                                    {
                                        i_IsogdDocument.PhysStorageBooks.Add(i_IsogdPhysStorageBook);
                                        i_IsogdDocument.Save();
                                    }
                                    else
                                    {
                                        var xtraMessageBoxForm = new XtraMessageBoxForm();
                                        if (
                                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                                "��� � ������� " + Environment.NewLine +
                                                dzuNom + Environment.NewLine +
                                                "�� ������� � �������� ������! �������?",
                                                "��������!", dialogResults, null, 0)) == DialogResult.Yes)
                                        {
                                            i_IsogdPhysStorageBook = connect.CreateObject<IsogdPhysStorageBook>();
                                            i_IsogdPhysStorageBook.BookNo = dzuNom;
                                            i_IsogdPhysStorageBook.OpenenigBookDate = DateTime.Now.Date;
                                            i_IsogdPhysStorageBook.Empl = i_IsogdDocument.Empl;
                                            //if (currentEmpl != null)
                                            //{
                                            //    i_IsogdPhysStorageBook.Empl = currentEmpl;
                                            //}
                                            try
                                            { i_IsogdPhysStorageBook.CadNumbers = i_IsogdDocument.CadNo.Trim().TrimEnd(',').Trim(); }
                                            catch { }
                                            try { i_IsogdPhysStorageBook.AddressInfo = i_IsogdDocument.AddressInfo; }
                                            catch { }
                                            i_IsogdDocument.PhysStorageBooks.Add(i_IsogdPhysStorageBook);
                                            i_IsogdPhysStorageBook.Save();
                                        }
                                        //else
                                        //{
                                        //    return;
                                        //}
                                    }
                                }
                            }
                        }
                        // ������� ������ �� ���� ���� �������� � ��������� �������� �� ��� ��� � ���������
                        foreach (Parcel parcel in i_IsogdDocument.Parcels)
                        {
                            foreach (IsogdPhysStorageBook isogdPhysStorageBook in i_IsogdDocument.PhysStorageBooks)
                            {
                                bool isPExist = false;
                                foreach (Parcel p in isogdPhysStorageBook.Parcels)
                                {
                                    if (p.Oid == parcel.Oid)
                                    {
                                        isPExist = true;
                                        break;
                                    }
                                }
                                if (!isPExist)
                                {
                                    isogdPhysStorageBook.Parcels.Add(parcel);
                                    isogdPhysStorageBook.Save();
                                }
                            }
                        }
                        foreach (IsogdStorageBook isogdStorageBook in i_IsogdDocument.IsogdStorageBooks)
                        {
                            foreach (IsogdPhysStorageBook isogdPhysStorageBook in i_IsogdDocument.PhysStorageBooks)
                            {
                                bool isPExist = false;
                                foreach (IsogdStorageBook sb in isogdPhysStorageBook.IsogdStorageBooks)
                                {
                                    if (sb.Oid == isogdStorageBook.Oid)
                                    {
                                        isPExist = true;
                                        break;
                                    }
                                }
                                if (!isPExist)
                                {
                                    isogdPhysStorageBook.IsogdStorageBooks.Add(isogdStorageBook);
                                    isogdPhysStorageBook.Save();
                                }
                            }
                        }
                        // ���������� ��� ����� ��������, ��������� � ���������, ���������� � ��� ��� ��������� � ������� � ����������� ������������ ������� "�����"
                        // � ���������� ��� ��� ��������� � ����� 
                        //if (i_storBook != null && i_PhysStorBook != null)
                        //{
                        //    i_PhysStorBook.IsogdStorageBooks.Add(i_storBook);
                        //}
                        //if (i_lot != null && i_PhysStorBook != null)
                        //{
                        //    i_PhysStorBook.Parcels.Add(i_lot);
                        //}
                        //unitOfWork.CommitChanges();
                        i_IsogdDocument.Save();
                        unitOfWork.CommitChanges();
                        XtraMessageBox.Show("�������� ����� ���������������!");
                    }

                }
            unitOfWork.CommitChanges();
            //XtraMessageBox.Show("�������� ����� ���������������!");
        }

        void contr_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            //this.View - currentVIew
            //e.AcceptActionArgs.SelectedObjects - selected object in the PopupWindow

            IObjectSpace objectSpace = Application.CreateObjectSpace();
            if (e.AcceptActionArgs.SelectedObjects.Count == 0)
            {
                XtraMessageBox.Show("�� ������� ����� ��������.");
                return;
                //throw new Exception("�� ������� ����� ��������.");
            }
            if (e.AcceptActionArgs.SelectedObjects.Count > 1)
            {
                XtraMessageBox.Show("������� ������ ����� ����� ��������.");
                return;
            }
            //XtraMessageBox.Show(this.View.Caption);
            IsogdDocument i_IsogdDocument;
            IsogdStorageBook i_IsogdStorageBook;
            IsogdDocument i_IsogdDoc = (IsogdDocument)this.View.CurrentObject;
            IsogdStorageBook i_IsogdStorBook = (IsogdStorageBook)e.AcceptActionArgs.SelectedObjects[0];
            i_IsogdDocument = objectSpace.FindObject<IsogdDocument>(new BinaryOperator("Oid", i_IsogdDoc.Oid));
            i_IsogdStorageBook = objectSpace.FindObject<IsogdStorageBook>(new BinaryOperator("Oid", i_IsogdStorBook.Oid));
            i_IsogdStorageBook.IsogdDocuments.Add(i_IsogdDocument);
            //i_IsogdDocument.Save();
            //i_IsogdStorageBook.Save();
            //i_IsogdDocument.IsogdStorageBook.Add(i_IsogdStorageBook);
            objectSpace.CommitChanges();
            XtraMessageBox.Show("�������� ����� ���������������!");
        }
    }
}