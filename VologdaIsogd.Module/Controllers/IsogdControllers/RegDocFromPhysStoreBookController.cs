﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Isogd;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using DevExpress.ExpressApp.Win;

namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RegDocFromPhysStoreBookController : ViewController
    {
        public RegDocFromPhysStoreBookController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Добавление в ДЗУ Документа ИСОГД и открытие его карточки для последующей регистрации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegDocFromPhysStoreBookAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;
            //RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            IsogdPhysStorageBook isogdPhysStorageBook;
            isogdPhysStorageBook = os.FindObject<IsogdPhysStorageBook>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)isogdPhysStorageBook.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            IsogdDocument isogdDocument;

            isogdDocument = connect.CreateObject<IsogdDocument>();
            try { isogdDocument.AddressInfo = isogdPhysStorageBook.AddressInfo; }
            catch { }
            try { isogdDocument.IsogdPhysStorageBooksNo = isogdPhysStorageBook.BookNo; }
            catch { }
            try { isogdDocument.CadNo = isogdPhysStorageBook.CadNumbers; }
            catch { }
            // открываем окно документа ИСОГД
            DetailView dv = Application.CreateDetailView(os, isogdDocument);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
        }

        /// <summary>
        /// Открывается выбор всех КХ у данной ДЗУ, при выборе которых при нажатии на кнопку Ок в ДЗУ добавляются все документы из выбранных КХ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddDocsFromStorageBooksAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            isogdPhysStorageBookId = ((BaseObject)e.CurrentObject).Oid;
            IsogdPhysStorageBook isogdPhysStorageBook;
            isogdPhysStorageBook = os.FindObject<IsogdPhysStorageBook>(new BinaryOperator("Oid", isogdPhysStorageBookId));
            Connect connect = Connect.FromObjectSpace(os);

            CollectionSourceBase source = Application.CreateCollectionSource(os, typeof(IsogdStorageBook), "123List");
            //Application.FindListViewId(typeof(ConstrPerm)));// "123list");
            string ID = Application.FindListViewId(typeof(IsogdStorageBook));
            //CriteriaOperator criteria = new InOperator("Terminal.Oid", GetIdTerminals(terminalEmpl));
            source.Criteria.Add("isogdPhysStorageBooks", new InOperator("Oid", GetStorageBooks(isogdPhysStorageBook)));
            //source.Criteria.Add("DocStatus", CriteriaOperator.Parse("EDocStatus=?", AISOGD.Enums.EDocStatus.Утвержден));
            e.ShowViewParameters.CreatedView = Application.CreateListView(ID, source, false);
            DialogController contr = new DialogController();

            contr.AcceptAction.Caption = "Добавить документы из выбранных КХ";
            contr.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(GetStorageBooksFromListView);
            //contr.CancelAction.Caption = "Создать новый";
            //contr.Cancelling += new EventHandler(CreateNewConstrPermFromListView);
            e.ShowViewParameters.Controllers.Add(contr);
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
        }

        private List<Guid> GetStorageBooks(IsogdPhysStorageBook isogdPhysStorageBook)
        {
            List<Guid> res = new List<Guid>();
            foreach (IsogdStorageBook isogdStorageBook in isogdPhysStorageBook.IsogdStorageBooks)
            {
                res.Add(isogdStorageBook.Oid);
            }
            return res;
        }
        object isogdPhysStorageBookId;
        private void GetStorageBooksFromListView(object sender, DialogControllerAcceptingEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            //IObjectSpace os = View.ObjectSpace;
            if (e.AcceptActionArgs.SelectedObjects.Count == 0)
            {
                XtraMessageBox.Show("Не выбрана ни одна книга хранения.");
                return;
                //throw new Exception("Не выбрана книга хранения.");
            }

            Connect connect = Connect.FromObjectSpace(os);
            IsogdPhysStorageBook isogdPhysStorageBook = os.GetObjectByKey<IsogdPhysStorageBook>(isogdPhysStorageBookId);
            if (isogdPhysStorageBook != null)
            {
                for (int i = 0; i < e.AcceptActionArgs.SelectedObjects.Count; i++)
                {
                    object storageBookId = ((BaseObjectXAF)e.AcceptActionArgs.SelectedObjects[i]).Oid;
                    IsogdStorageBook isogdStorageBook = os.GetObjectByKey<IsogdStorageBook>(storageBookId);
                    foreach (IsogdDocument isogdDoc in isogdStorageBook.IsogdDocuments)
                    {
                        if (!isogdPhysStorageBook.IsogdDocuments.Contains<IsogdDocument>(isogdDoc))
                        {
                            isogdPhysStorageBook.IsogdDocuments.Add(isogdDoc);
                            isogdPhysStorageBook.Save();
                        }
                    }
                }
            }
            os.CommitChanges();
            //RefreshAllWindows();
        }

        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }
    }
}
