﻿namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    partial class RegDocFromStoreBookController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RegDocFromStoreBookAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RegDocFromStoreBookAction
            // 
            this.RegDocFromStoreBookAction.Caption = "Добавить документ в КХ";
            this.RegDocFromStoreBookAction.ConfirmationMessage = null;
            this.RegDocFromStoreBookAction.Id = "RegDocFromStoreBookAction";
            this.RegDocFromStoreBookAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RegDocFromStoreBookAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdStorageBook);
            this.RegDocFromStoreBookAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RegDocFromStoreBookAction.ToolTip = null;
            this.RegDocFromStoreBookAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RegDocFromStoreBookAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RegDocFromStoreBookAction_Execute);
            // 
            // RegDocFromStoreBookController
            // 
            this.Actions.Add(this.RegDocFromStoreBookAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RegDocFromStoreBookAction;
    }
}
