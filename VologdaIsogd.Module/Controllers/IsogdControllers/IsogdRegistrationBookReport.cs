using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.XtraPrinting;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using AISOGD.OrgStructure;
using AISOGD.Isogd;
using AISOGD.SystemDir;
using System.Reflection;
using DevExpress.XtraEditors;
using System.IO;

namespace VologdaIsogd.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class IsogdRegistrationBookReport : ViewController
    {
        public IsogdRegistrationBookReport()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        // ��������� �� �������
        private void IsogdRegistrationBookAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IsogdPartition i_IsogdPartition;
            i_IsogdPartition = (IsogdPartition)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_IsogdPartition.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            // ������� ������������ �� ��� ������� ������
            Municipality mun = connect.FindFirstObject<Municipality>(mc =>
                mc.dMunicipalityKind.Code == "1" || mc.dMunicipalityKind.Code == "2");

            string oktmo = "";
            string munname = "";
            string cadMOno = "";
            string openBookDate = "";
            string closeBookDate = DateTime.Now.ToShortDateString();
            if (mun.Name != null)
                munname = mun.Name;
            if (mun.OKTMO != null)
                oktmo = mun.OKTMO;
            if (mun.CadastreNo != null)
                cadMOno = mun.CadastreNo;

            IsogdBooksCard i_IsogdBooksCard = connect.FindFirstObject<IsogdBooksCard>(mc =>
                mc.IsogdBooksKind == AISOGD.Enums.eIsogdBooksKind.����������������);
            if (i_IsogdBooksCard != null)
                if (i_IsogdBooksCard.RecordDate != null)
                    openBookDate = i_IsogdBooksCard.RecordDate.ToShortDateString();

            string isogdDocClassCode = "";
            string isogdDocName = "";
            string isogdDocRegNo = "";
            
            //IEnumerable<IsogdBooksCard> isogdBooksCardList = connect.FindObjects<IsogdBooksCard>(mc=> mc.Oid!=null)
            //    .AsEnumerable<IsogdBooksCard>;
            SortingCollection sortCollection = new SortingCollection();
            sortCollection.Add(new SortProperty("RecordDate", SortingDirection.Ascending));

            ICollection IsogdInDataCardList = unitOfWork.GetObjects(connect.GetClassInfo<IsogdInDataCard>(),
                CriteriaOperator.Parse("1 = 1"),
                sortCollection, Int32.MaxValue, false, false);
            //#region #serverprint
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\RegistrationBook.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            string text = "";
            Bookmark bookmarkMO = null;
            Bookmark bookmarkOKTMO = null;
            Bookmark bookmarkOpenBookDate = null;
            Bookmark bookmarkCloseBookDate = null;
            Bookmark bookmarkPartNo = null;
            Bookmark bookmarkPartName = null;

            Bookmark bookmarkIsogdDocClassCode = null;
            Bookmark bookmarkIsogdDocName = null;
            Bookmark bookmarkIsogdDocRegNo = null;
            Bookmark bookmarkIsogdStatmentDate = null;
            Bookmark bookmarkIsogdStatmentOrg = null;
            Bookmark bookmarkIsogdAddress = null;
            Bookmark bookmarkIsogdStorBookCode = null;

            Bookmark bookmarkRegistrationCardNo = null;
            Bookmark bookmarkRegistrationCardDate = null;
            Bookmark bookmarkRegistrationEmpl1 = null;
            Bookmark bookmarkRegistrationEmpl2 = null;

            // ������� ������� �������� � �������
            foreach (Bookmark bookmark in richServer.Document.Bookmarks)
            {
                if (bookmark.Name == "MOName")
                    bookmarkMO = bookmark;
                if (bookmark.Name == "OKTMO")
                    bookmarkOKTMO = bookmark;
                if (bookmark.Name == "OpeningTomeDate")
                    bookmarkOpenBookDate = bookmark;
                if (bookmark.Name == "ClosingTomeDate")
                    bookmarkCloseBookDate = bookmark;
                if (bookmark.Name == "PartNo")
                    bookmarkPartNo = bookmark;
                if (bookmark.Name == "PartName")
                    bookmarkPartName = bookmark;

                if (bookmark.Name == "DocCode")
                    bookmarkIsogdDocClassCode = bookmark;
                if (bookmark.Name == "DocName")
                    bookmarkIsogdDocName = bookmark;
                if (bookmark.Name == "IsogdDocRegNo")
                    bookmarkIsogdDocRegNo = bookmark;
                if (bookmark.Name == "DocStateDate")
                    bookmarkIsogdStatmentDate = bookmark;
                if (bookmark.Name == "StatementOrg")
                    bookmarkIsogdStatmentOrg = bookmark;
                if (bookmark.Name == "DocumentArea")
                    bookmarkIsogdAddress = bookmark;
                if (bookmark.Name == "StorageBookCode")
                    bookmarkIsogdStorBookCode = bookmark;

                if (bookmark.Name == "CardNo2")
                    bookmarkRegistrationCardNo = bookmark;
                if (bookmark.Name == "DocRegDate")
                    bookmarkRegistrationCardDate = bookmark;
                if (bookmark.Name == "CardEmpl1")
                    bookmarkRegistrationEmpl1 = bookmark;
                if (bookmark.Name == "CardEmpl2")
                    bookmarkRegistrationEmpl2 = bookmark;
            }
            //DocumentPosition pos = bookmark.Range.Start;
            //           richServer.Document.InsertText(pos, "asdadadadsada");
            // ����� ������ � �������� ����������
            richServer.Document.Replace(bookmarkMO.Range, munname);
            richServer.Document.Replace(bookmarkOKTMO.Range, oktmo);
            richServer.Document.Replace(bookmarkOpenBookDate.Range, openBookDate);
            richServer.Document.Replace(bookmarkCloseBookDate.Range, closeBookDate);

            string partno = "";
            string partname = "";
            if (i_IsogdPartition.PartNo != null)
                partno = i_IsogdPartition.PartNo;
            if (i_IsogdPartition.Name != null)
                partname = i_IsogdPartition.Name;

            

            richServer.Document.Replace(bookmarkPartNo.Range, partno);
            richServer.Document.Replace(bookmarkPartName.Range, partname);

            // ��������� ������ � �������
            int i = 1;
            DocumentRange range = richServer.Document.CreateRange(0, 0);

            
           //XtraMessageBox.Show(richServer.Document.GetText(bookmarkIsogdDocClassCode.Range));

            foreach (IsogdRegistrationCard isogdRegistrationCard in i_IsogdPartition.IsogdRegistrationCard)
            {
                // ������ �������
                range = richServer.Document.Tables[0].Rows[0].Cells[3].ContentRange;
                richServer.Document.Replace(range, partno);

                range = richServer.Document.Tables[0].Rows[1].Cells[1].ContentRange;
                richServer.Document.Replace(range, munname);

                if (isogdRegistrationCard.RecNo != null)
                {
                    range = richServer.Document.Tables[0].Rows[1].Cells[3].ContentRange;
                    richServer.Document.Replace(range, isogdRegistrationCard.RecNo);
                }

                range = richServer.Document.Tables[0].Rows[2].Cells[1].ContentRange;
                richServer.Document.Replace(range, cadMOno);

                range = richServer.Document.Tables[0].Rows[2].Cells[3].ContentRange;
                richServer.Document.Replace(range, oktmo);

                //XtraMessageBox.Show(richServer.Document.GetText(bookmarkIsogdDocClassCode.Range));
                // ������ ������� (����� �����)
                if (isogdRegistrationCard.IsogdDocument != null)
                {
                    if (isogdRegistrationCard.IsogdDocument.IsogdDocumentClass != null)
                        if (isogdRegistrationCard.IsogdDocument.IsogdDocumentClass.Code != null)
                            richServer.Document.Replace(bookmarkIsogdDocClassCode.Range,
                                isogdRegistrationCard.IsogdDocument.IsogdDocumentClass.Code);

                    if (isogdRegistrationCard.IsogdDocument.DocName != null)
                        richServer.Document.Replace(bookmarkIsogdDocName.Range,
                                isogdRegistrationCard.IsogdDocument.DocName);

                    if (isogdRegistrationCard.IsogdDocument.RegNo != null)
                        richServer.Document.Replace(bookmarkIsogdDocRegNo.Range,
                                isogdRegistrationCard.IsogdDocument.RegNo);

                    if (isogdRegistrationCard.IsogdDocument.StatementDate != DateTime.MinValue)
                        richServer.Document.Replace(bookmarkIsogdStatmentDate.Range,
                                isogdRegistrationCard.IsogdDocument.StatementDate.ToShortDateString());

                    if (isogdRegistrationCard.IsogdDocument.StatementOrg != null)
                        if (isogdRegistrationCard.IsogdDocument.StatementOrg.Name != null)
                            richServer.Document.Replace(bookmarkIsogdStatmentOrg.Range,
                                isogdRegistrationCard.IsogdDocument.StatementOrg.Name);

                    if (isogdRegistrationCard.IsogdDocument.ActivityArea != null)
                        richServer.Document.Replace(bookmarkIsogdAddress.Range,
                                isogdRegistrationCard.IsogdDocument.ActivityArea);

                    string isogdStorBookCode = "";
                    foreach (IsogdStorageBook isogdStorageBook in isogdRegistrationCard.
                        IsogdDocument.IsogdStorageBooks)
                    {
                        if (isogdStorageBook.Code != null)
                        {
                            if (isogdStorBookCode == "")
                                isogdStorBookCode = isogdStorageBook.Code;
                            else
                                isogdStorBookCode = isogdStorBookCode + ", " + isogdStorageBook.Code;
                        }

                    }
                    richServer.Document.Replace(bookmarkIsogdStorBookCode.Range, isogdStorBookCode);

                    // ������ ������� (����������� �����)
                    int j = 1;
                    foreach (IsogdMap isogdMap in isogdRegistrationCard.IsogdDocument.IsogdMap)
                    {
                        if (richServer.Document.Tables[2].Rows.Count < j + 1)
                            richServer.Document.Tables[2].Rows.InsertAfter(j);

                        range = richServer.Document.Tables[2].Rows[j].Cells[0].ContentRange;
                        richServer.Document.Replace(range, j.ToString());

                        range = richServer.Document.Tables[2].Rows[j].Cells[1].ContentRange;
                        if (isogdMap.Name != null)
                            richServer.Document.Replace(range, isogdMap.Name);

                        range = richServer.Document.Tables[2].Rows[j].Cells[2].ContentRange;
                        if (isogdMap.IsogdMapKind != null)
                            if (isogdMap.IsogdMapKind.Name != null)
                                richServer.Document.Replace(range, isogdMap.IsogdMapKind.Name);

                        range = richServer.Document.Tables[2].Rows[j].Cells[4].ContentRange;
                        if (isogdMap.Scale != null)
                            if (isogdMap.Scale.Name != null)
                                richServer.Document.Replace(range, isogdMap.Scale.Name);

                        range = richServer.Document.Tables[2].Rows[j].Cells[7].ContentRange;
                        if (isogdMap.IdNo != null)
                            richServer.Document.Replace(range, isogdMap.IdNo);
                        j++;
                    }
                }


                if (isogdRegistrationCard.RecNo != null)
                    richServer.Document.Replace(bookmarkRegistrationCardNo.Range,
                        isogdRegistrationCard.RecNo);

                if (isogdRegistrationCard.RecordDate != DateTime.MinValue)
                    richServer.Document.Replace(bookmarkRegistrationCardDate.Range,
                        isogdRegistrationCard.RecordDate.ToShortDateString());

                if (isogdRegistrationCard.Empl != null)
                    if (isogdRegistrationCard.Empl.BriefName != null)
                        richServer.Document.Replace(bookmarkRegistrationEmpl1.Range,
                            isogdRegistrationCard.Empl.BriefName);

                //if (isogdRegistrationCard.Empl != null)
                //    if (isogdRegistrationCard.Empl.BriefName != null)
                //        richServer.Document.Replace(bookmarkRegistrationEmpl2.Range,
                //            isogdRegistrationCard.RecNo);

                i++;
            }

            string path = Path.GetTempPath() + @"\�����_�����������_����������_�������_" + i_IsogdPartition.PartNo + "_" +
                    DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".docx";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }

            //// Invoke the Print Preview dialog
            //using (PrintingSystem printingSystem = new PrintingSystem())
            //{
            //    using (PrintableComponentLink link = new PrintableComponentLink(printingSystem))
            //    {
            //        link.Component = richServer;
            //        link.CreateDocument();
            //        link.ShowPreviewDialog();
            //    }
            //}
            //#endregion #serverprint
        }
    }
}
