﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Isogd;
using DevExpress.Xpo;
using AISOGD.SystemDir;

namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RegDocFromStoreBookController : ViewController
    {
        public RegDocFromStoreBookController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Добавление в книгу хранения Документа ИСОГД и открытие его карточки для последующей регистрации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegDocFromStoreBookAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            IsogdStorageBook isogdStorageBook;
            isogdStorageBook = os.FindObject<IsogdStorageBook>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)isogdStorageBook.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            IsogdDocument isogdDocument;

            isogdDocument = connect.CreateObject<IsogdDocument>();
            try { isogdDocument.AddressInfo = isogdStorageBook.AddressInfo; }
            catch { }
            //if(isogdStorageBook.IsogdPartition)
            try { isogdDocument.CadNo = isogdStorageBook.Code; }
            catch { }
            try
            {
                if (isogdDocument.IsogdPhysStorageBooksNo == null)
                    isogdDocument.IsogdPhysStorageBooksNo = "";
                foreach (IsogdPhysStorageBook psb in isogdStorageBook.PhysStorageBooks)
                {
                    try { isogdDocument.IsogdPhysStorageBooksNo += psb.BookNo + ","; }
                    catch { }
                }
                isogdDocument.IsogdPhysStorageBooksNo = isogdDocument.IsogdPhysStorageBooksNo.TrimEnd(',');
            }
            catch { }
            // открываем окно документа ИСОГД
            DetailView dv = Application.CreateDetailView(os, isogdDocument);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
        }
    }
}
