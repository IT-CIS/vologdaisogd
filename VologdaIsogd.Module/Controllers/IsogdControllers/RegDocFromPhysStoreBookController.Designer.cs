﻿namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    partial class RegDocFromPhysStoreBookController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RegDocFromPhysStoreBookAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AddDocsFromStorageBooksAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RegDocFromPhysStoreBookAction
            // 
            this.RegDocFromPhysStoreBookAction.Caption = "Добавить документ в ДЗУ";
            this.RegDocFromPhysStoreBookAction.ConfirmationMessage = null;
            this.RegDocFromPhysStoreBookAction.Id = "RegDocFromPhysStoreBookAction";
            this.RegDocFromPhysStoreBookAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RegDocFromPhysStoreBookAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdPhysStorageBook);
            this.RegDocFromPhysStoreBookAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RegDocFromPhysStoreBookAction.ToolTip = null;
            this.RegDocFromPhysStoreBookAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RegDocFromPhysStoreBookAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RegDocFromPhysStoreBookAction_Execute);
            // 
            // AddDocsFromStorageBooksAction
            // 
            this.AddDocsFromStorageBooksAction.Caption = "Добавить документы в ДЗУ из КХ";
            this.AddDocsFromStorageBooksAction.ConfirmationMessage = null;
            this.AddDocsFromStorageBooksAction.Id = "AddDocsFromStorageBooksAction";
            this.AddDocsFromStorageBooksAction.ToolTip = null;
            this.AddDocsFromStorageBooksAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AddDocsFromStorageBooksAction_Execute);
            // 
            // RegDocFromPhysStoreBookController
            // 
            this.Actions.Add(this.RegDocFromPhysStoreBookAction);
            this.Actions.Add(this.AddDocsFromStorageBooksAction);
            this.TargetObjectType = typeof(AISOGD.Isogd.IsogdPhysStorageBook);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RegDocFromPhysStoreBookAction;
        private DevExpress.ExpressApp.Actions.SimpleAction AddDocsFromStorageBooksAction;
    }
}
