﻿namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    partial class SetIsogdDocsRegNoController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SetIsogdDocsRegNoAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SetIsogdDocsRegNoAction
            // 
            this.SetIsogdDocsRegNoAction.Caption = "Перепроставить регномера";
            this.SetIsogdDocsRegNoAction.ConfirmationMessage = null;
            this.SetIsogdDocsRegNoAction.Id = "SetIsogdDocsRegNoAction";
            this.SetIsogdDocsRegNoAction.TargetObjectType = typeof(AISOGD.Isogd.IsogdDocument);
            this.SetIsogdDocsRegNoAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SetIsogdDocsRegNoAction.ToolTip = null;
            this.SetIsogdDocsRegNoAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SetIsogdDocsRegNoAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SetIsogdDocsRegNoAction_Execute);
            // 
            // SetIsogdDocsRegNoController
            // 
            this.Actions.Add(this.SetIsogdDocsRegNoAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SetIsogdDocsRegNoAction;
    }
}
