﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Isogd;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Data;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Helpers;
using DevExpress.ExpressApp.Xpo;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using DevExpress.Spreadsheet;
using AISOGD.General;
using System.Diagnostics;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.IsogdControllers
{
    public partial class IsogdAbstractRequestController : ViewController
    {
        public IsogdAbstractRequestController()
        {
            InitializeComponent();

        }

        /// <summary>
        /// Определение документов ИСОГД для выгрузки пространственным запросом по территории запроса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IsogdAbstractGetDocsFromGISAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IsogdAbstractRequest isogdAbstractRequest = (IsogdAbstractRequest)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)isogdAbstractRequest.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            MapInfoApplication m_MapInfo = null;
            m_MapInfo = new MapInfoApplication();
            int selObjCount = 0;
            string ReqestObjID = "";
            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return;
            }

            SpatialController<GisLayer> spatial = new SpatialController<GisLayer>(connect);

            // ищем запись о нем в SpatialRepository
            SpatialRepository spatialRepo = connect.FindFirstObject<SpatialRepository>(mc =>
                mc.IsogdClassName == isogdAbstractRequest.ClassInfo.FullName &&
                mc.IsogdObjectId == isogdAbstractRequest.Oid.ToString());

            string reqLayerName = "";
            string reqObjId = "";

            // ищем связанные слои с классом Книга хранения
            List<string> isogdStorageBookLayersIds = spatial.GetListLinkedSpatialLayers("AISOGD.Isogd.IsogdStorageBook");
            //XtraMessageBox.Show("3");
            if (spatialRepo != null)
            {
                try
                {
                    reqLayerName = spatialRepo.SpatialLayerId;
                    reqObjId = spatialRepo.SpatialObjectId;
                }
                catch
                {
                    XtraMessageBox.Show("У карточки запроса не найдена связь с пространственным объектом");
                    return;
                }
                if (reqObjId == "0")
                {
                    XtraMessageBox.Show("Перед тем, как связать реестровый и пространственные объекты их необходимо сохранить!", "Внимание");
                    return;
                }
                // получаем наш пространственный объект заявки
                try
                {
                    m_MapInfo.Do("Select * From " + reqLayerName + " Where MI_PRINX = " + reqObjId + " Into zObject");
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(String.Format("Произошла ошибка. Обратитесь к администратору Системы. Текст ошибки: (IsogdAbstractGetDocsFromGISAction_1) Message: {0}, Source: {1}, TargetSite: {2}",
                                ex.Message, ex.Source, ex.TargetSite), "Ошибка");
                    return;
                }
                // Количество выбранных записей (объект заявки)
                if (Convert.ToInt32(m_MapInfo.Eval("SelectionInfo(3)")) == 0)
                {
                    XtraMessageBox.Show(
                        "У запроса нет связанного пространственного объекта. Подалуйста, отрисуйте территорию заявки для предоставления сведений ИСОГД и свяжите с реестровой карточкой.", "Инфо");
                    return;
                }
                try
                {
                    // 2.1 Получаем таблицы для книги хранения и ищем пересечения с объектами этих таблиц
                    foreach (string layer in spatial.GetListLinkedSpatialLayers("AISOGD.Isogd.IsogdStorageBook"))
                    {
                        m_MapInfo.Do("Select MI_PRINX from " + layer + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
                        selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                        for (int i = 1; i < selObjCount + 1; i++)
                        {
                            m_MapInfo.Do("Fetch Rec " + i.ToString() + " From tmpTable");
                            // Id пространственного объекта книги хранения
                            ReqestObjID = m_MapInfo.Eval("tmpTable.Col1");

                            // находим книгу хранения
                            // из таблицы связи получаем Ид реестрового объекта книги хранения
                            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                mc.SpatialLayerId == layer &&
                                mc.SpatialObjectId == ReqestObjID &&
                                mc.IsogdClassName == "AISOGD.Isogd.IsogdStorageBook");

                            if (objLink != null)
                            {
                                IsogdStorageBook i_IsogdStorageBook = unitOfWork.FindObject<IsogdStorageBook>(new BinaryOperator("Oid", objLink.IsogdObjectId));
                                // Перебираем документы книги хранения по определенным критериям, если указаны
                                //XPCollection<IsogdDocument> IsogdDocList = new XPCollection<IsogdDocument>(i_IsogdStorageBook.IsogdDocuments, 
                                //    new BinaryOperator("IsogdDocumentClass.Code", "060100"));
                                //IsogdDocList.Criteria = new BinaryOperator("IsogdDocumentClass.Code", "060100");//isogdAbstractRequest.IsogdDocumentClasses);


                                //CriteriaOperator criteria1 = new BinaryOperator("IsogdDocumentClass", isogdAbstractRequest.IsogdDocumentClasses);
                                CriteriaOperator criteria1 = null;
                                if (isogdAbstractRequest.IsogdDocumentClasses.Count > 0)
                                {
                                    //IsogdDocList.Criteria = new BinaryOperator("IsogdDocumentClass.Code", "060100");//isogdAbstractRequest.IsogdDocumentClasses);
                                    //XtraMessageBox.Show(IsogdDocList.Count.ToString());
                                    //IsogdDocList.Criteria = new InOperator("IsogdDocumentClass", isogdAbstractRequest.IsogdDocumentClasses);
                                    criteria1 = new InOperator("IsogdDocumentClass", isogdAbstractRequest.IsogdDocumentClasses);
                                }
                                CriteriaOperator criteria2 = null;
                                if (isogdAbstractRequest.DocRegDateFrom != DateTime.MinValue)
                                {
                                    DateTime from = isogdAbstractRequest.DocRegDateFrom;
                                    DateTime till = DateTime.Now.Date;
                                    if (isogdAbstractRequest.DocRegDateTill != DateTime.MinValue)
                                        till = isogdAbstractRequest.DocRegDateTill;
                                    criteria2 = new BetweenOperator("PlacementDate", from, till);// CriteriaOperator.Parse(String.Format("PlacementDate between ({0}, {1})", from, till));
                                }
                                XPCollection<IsogdDocument> IsogdDocList = new XPCollection<IsogdDocument>(i_IsogdStorageBook.IsogdDocuments,
                                    GroupOperator.And(criteria1,criteria2));
                                XtraMessageBox.Show(IsogdDocList.Count.ToString());
                                foreach (IsogdDocument isogdDocument in IsogdDocList)
                                {
                                    bool isDocExist = false;
                                    // проверяем, может он уже есть в заявке
                                    foreach (IsogdDocument isogdDoc in isogdAbstractRequest.IsogdDocuments)
                                    {
                                        if (isogdDocument == isogdDoc)
                                            isDocExist = true;
                                    }
                                    // если нет, то прикрепляем к ней
                                    if (!isDocExist)
                                    {
                                        isogdAbstractRequest.IsogdDocuments.Add(isogdDocument);
                                        unitOfWork.CommitChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(String.Format("Произошла ошибка. Обратитесь к администратору Системы. Текст ошибки: (IsogdAbstractGetDocsFromGISAction_3) Message: {0}, Source: {1}, TargetSite: {2}",
                                ex.Message, ex.Source, ex.TargetSite), "Ошибка");
                }
            }
            else
                XtraMessageBox.Show("У данного запроса нет связанной территории в ГИС.");
        }

        /// <summary>
        /// Выгрузка документов ИСОГД в Excel файл с электронными копиями
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IsogdAbstractExportDocsAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IsogdAbstractRequest isogdAbstractRequest;
            isogdAbstractRequest = (IsogdAbstractRequest)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)isogdAbstractRequest.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            string pathToFiles = "";
            string filenameXLS = "";
            // открываем окно указания папки для сохранения
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            //fbd.RootFolder = @"";
            if (fbd.ShowDialog() == DialogResult.OK)
                pathToFiles = fbd.SelectedPath;
            else
                return;
            if (pathToFiles != "")
            {
                //isogdAbstractRequest.IsogdDocuments.
                //ExportController exportController;// = new ExportController();
                //exportController = Frame.GetController<ExportController>();
                //exportController.Exportable = 
                string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                    LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Isogd\AbstractRequest.xls");


                Workbook workbook = new Workbook();
                // Access the first worksheet in the workbook.
                
                workbook.LoadDocument(templatePath);
                Worksheet worksheet = workbook.Worksheets[0];

                Cell cell;

                int i = 2;
                foreach (IsogdDocument isogdDoc in isogdAbstractRequest.IsogdDocuments)
                {
                    try
                    {
                        cell = worksheet.Cells["A" + i.ToString()];
                        cell.Value = (i-1).ToString();
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["B" + i.ToString()];
                        cell.Value = isogdDoc.DocNo;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["C" + i.ToString()];
                        cell.Value = isogdDoc.StatementDate.ToShortDateString();
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["D" + i.ToString()];
                        cell.Value = String.Format("({1}) {0}", isogdDoc.IsogdDocumentClass.Name, isogdDoc.IsogdDocumentClass.Code);
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["E" + i.ToString()];
                        cell.Value = isogdDoc.RegNo;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["F" + i.ToString()];
                        cell.Value = isogdDoc.PlacementDate;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["G" + i.ToString()];
                        cell.Value = isogdDoc.Empl.BriefName;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["H" + i.ToString()];
                        cell.Value = isogdDoc.Developer.FullName;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["J" + i.ToString()];
                        cell.Value = isogdDoc.StatementOrg.FullName;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["K" + i.ToString()];
                        cell.Value = isogdDoc.DocName;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["I" + i.ToString()];
                        cell.Value = isogdDoc.AddressInfo;
                    }
                    catch { }

                    // копируем электронные копии
                    if (isogdAbstractRequest.AttachFlag)
                    {
                        if (isogdDoc.AttachmentFiles.Count > 0)
                        {
                            string attachpath = "";
                            try { attachpath = Path.Combine(pathToFiles, isogdDoc.RegNo); }
                            catch { }
                            if (attachpath == "")
                            {
                                attachpath = Path.Combine(pathToFiles, isogdDoc.Oid.ToString());
                            }
                            Directory.CreateDirectory(attachpath);
                            foreach (AttachmentFiles attach in isogdDoc.AttachmentFiles)
                            {
                                if (attach.FileStore != null)
                                {
                                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                    {

                                        attach.FileStore.SaveToStream(ms);
                                        ms.Seek(0, System.IO.SeekOrigin.Begin);
                                        string filename = Path.Combine(attachpath, attach.FileStore.FileName);
                                        File.WriteAllBytes(filename, ms.ToArray());
                                        //ms.Flush();
                                        //ms.Position = 0;
                                        //FileStream DestinationStream = File.Create
                                        //att.File.LoadFromStream(attach.File.FileName, ms);
                                        ms.Dispose();
                                    }
                                }
                            }
                        }
                    }
                    i++;
                }

                filenameXLS = pathToFiles +  @"\Отчет_по_документам_" +
                    isogdAbstractRequest.RegDate.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xls";

                workbook.SaveDocument(filenameXLS, DocumentFormat.OpenXml);
                
                XtraMessageBox.Show("Выгрузка завершена!");
                Process.Start(pathToFiles);
            }
        }
        
    }
}
