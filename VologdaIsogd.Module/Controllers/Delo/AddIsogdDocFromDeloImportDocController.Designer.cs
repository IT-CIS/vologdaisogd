﻿namespace VologdaIsogd.Module.Controllers.Delo
{
    partial class AddIsogdDocFromDeloImportDocController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddIsogdDocFromDeloImportDocAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AddIsogdDocFromDeloImportDocAction
            // 
            this.AddIsogdDocFromDeloImportDocAction.Caption = "Создать документ ИСОГД";
            this.AddIsogdDocFromDeloImportDocAction.ConfirmationMessage = null;
            this.AddIsogdDocFromDeloImportDocAction.Id = "AddIsogdDocFromDeloImportDocAction";
            this.AddIsogdDocFromDeloImportDocAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.AddIsogdDocFromDeloImportDocAction.TargetObjectType = typeof(AISOGD.Delo.DocumentFromDelo);
            this.AddIsogdDocFromDeloImportDocAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.AddIsogdDocFromDeloImportDocAction.ToolTip = null;
            this.AddIsogdDocFromDeloImportDocAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.AddIsogdDocFromDeloImportDocAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AddIsogdDocFromDeloImportDocAction_Execute);
            // 
            // AddIsogdDocFromDeloImportDocController
            // 
            this.Actions.Add(this.AddIsogdDocFromDeloImportDocAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction AddIsogdDocFromDeloImportDocAction;
    }
}
