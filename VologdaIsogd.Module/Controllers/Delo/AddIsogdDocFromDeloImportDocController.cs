﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Delo;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using AISOGD.Isogd;
using AISOGD.General;
using AISOGD.OrgStructure;
using FileSystemData.BusinessObjects;

namespace VologdaIsogd.Module.Controllers.Delo
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AddIsogdDocFromDeloImportDocController : ViewController
    {
        public AddIsogdDocFromDeloImportDocController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Создание документа ИСОГД для его дальнейшей регистрации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddIsogdDocFromDeloImportDocAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            DocumentFromDelo documentFromDelo;
            documentFromDelo = os.FindObject<DocumentFromDelo>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)documentFromDelo.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            IsogdDocument isogdDocument;

            isogdDocument = connect.CreateObject<IsogdDocument>();
            try { isogdDocument.DocNo = documentFromDelo.DocNo; }
            catch { }
            try { isogdDocument.StatementDate = documentFromDelo.DocDate; }
            catch { }
            try { isogdDocument.IsogdDocumentClass = documentFromDelo.IsogdDocumentClass; }
            catch { }
            try { isogdDocument.IsogdDocumentNameKind = documentFromDelo.IsogdDocumentNameKind; }
            catch { }
            try { isogdDocument.Developer = documentFromDelo.Developer; }
            catch { }
            try { isogdDocument.StatementOrg = documentFromDelo.StatementOrg; }
            catch { }
            try { isogdDocument.DocName = documentFromDelo.DocName; }
            catch { }
            documentFromDelo.eDocumentFromDeloStatus = AISOGD.Enums.eDocumentFromDeloStatus.Зарегистрирован;
            // копируем прикрепленные файлы
            try {
                foreach (AttachmentFiles attach in documentFromDelo.AttachmentFiles)
                {
                    try
                    {
                        bool isAttachExist = false;
                        if (isogdDocument.AttachmentFiles.Count > 0)
                        {
                            foreach (AttachmentFiles isogdattach in isogdDocument.AttachmentFiles)
                                if (isogdattach.FileStore.FileName == attach.FileStore.FileName && isogdattach.FileStore.Size == attach.FileStore.Size)
                                {
                                    isAttachExist = true;
                                    break;
                                }
                        }
                        if (!isAttachExist)
                        {
                            AttachmentFiles att = new AttachmentFiles(unitOfWork);
                            if (attach.Name != null)
                                att.Name = attach.Name;
                            att.RegDate = DateTime.Now.Date;
                            if (attach.Description != null)
                                att.Description = attach.Description;

                            if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                            {
                                Employee currentEmpl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                                if (currentEmpl != null)
                                    att.Registrator = currentEmpl;
                            }
                            if (attach.FileStore != null)
                            {
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    attach.FileStore.SaveToStream(ms);
                                    FileSystemStoreObject fileSystemStoreObject = connect.CreateObject<FileSystemStoreObject>();
                                    fileSystemStoreObject.LoadFromStream(attach.FileStore.FileName, ms);
                                    att.FileStore = fileSystemStoreObject;
                                    ms.Dispose();
                                }
                            }
                            isogdDocument.AttachmentFiles.Add(att);
                            isogdDocument.Save();
                            unitOfWork.CommitChanges();
                        }
                    }
                    catch { }
                }
            }
            catch { }
            // открываем окно документа ИСОГД
            DetailView dv = Application.CreateDetailView(os, isogdDocument);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
        }
    }
}
