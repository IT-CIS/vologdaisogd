﻿namespace VologdaIsogd.Module.Controllers.Delo
{
    partial class DocImportToIsogdController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddIsogdDocFromDocImportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AddIsogdDocFromDocImportAction
            // 
            this.AddIsogdDocFromDocImportAction.Caption = "Добавить документ ИСОГД";
            this.AddIsogdDocFromDocImportAction.ConfirmationMessage = null;
            this.AddIsogdDocFromDocImportAction.Id = "AddIsogdDocFromDocImportAction";
            this.AddIsogdDocFromDocImportAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.AddIsogdDocFromDocImportAction.TargetObjectType = typeof(AISOGD.Delo.DocImportToIsogd);
            this.AddIsogdDocFromDocImportAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.AddIsogdDocFromDocImportAction.ToolTip = null;
            this.AddIsogdDocFromDocImportAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.AddIsogdDocFromDocImportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AddIsogdDocFromDocImportAction_Execute);
            // 
            // DocImportToIsogdController
            // 
            this.Actions.Add(this.AddIsogdDocFromDocImportAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction AddIsogdDocFromDocImportAction;
    }
}
