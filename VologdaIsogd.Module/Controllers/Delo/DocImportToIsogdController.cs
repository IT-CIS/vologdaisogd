﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using AISOGD.Delo;
using AISOGD.Isogd;
using AISOGD.General;
using AISOGD.OrgStructure;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using FileSystemData.BusinessObjects;

namespace VologdaIsogd.Module.Controllers.Delo
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DocImportToIsogdController : ViewController
    {
        public DocImportToIsogdController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция создания промежуточного документа(из подготавливаемых в АИСОГД), при его утверждении, для последующего импорта в ИСОГД
        /// </summary>
        /// <param name="connect"></param>
        /// <param name="docNo"></param>
        /// <param name="docDate"></param>
        /// <param name="classCode"></param>
        /// <param name="docName"></param>
        /// <param name="cadNoms"></param>
        /// <param name="addressInfo"></param>
        /// <param name="attachs"></param>
        public void CreateDocImportToIsogd(Connect connect, string docNo, DateTime docDate, string classCode, string docName,
            string cadNoms, string addressInfo, List<AttachmentFiles> attachs)
        {
            //IObjectSpace os = Application.CreateObjectSpace();

            //Connect connect = Connect.FromObjectSpace(os);

            DocImportToIsogd docImportToIsogd = null;
            //try
            //{
            docImportToIsogd = connect.FindFirstObject<DocImportToIsogd>(x => x.DocNo == docNo && x.DocDate == docDate && x.IsogdDocumentClass.Code == classCode);

            if (docImportToIsogd == null)
            {
                docImportToIsogd = connect.CreateObject<DocImportToIsogd>();

                docImportToIsogd.eDocumentFromDeloStatus = AISOGD.Enums.eDocumentFromDeloStatus.Новый;
                docImportToIsogd.DocNo = docNo;
                docImportToIsogd.DocDate = docDate;
                docImportToIsogd.ImportDate = DateTime.Now.Date;
                try { docImportToIsogd.IsogdDocumentClass = connect.FindFirstObject<IsogdDocumentClass>(x => x.Code == classCode); }
                catch { }

                try { docImportToIsogd.IsogdDocumentNameKind = connect.FindFirstObject<dIsogdDocumentNameKind>(x => x.Name == docName); }
                catch { }

                try { docImportToIsogd.CadNo = cadNoms; }
                catch { }

                try { docImportToIsogd.AddressInfo = addressInfo; }
                catch { }
                docImportToIsogd.Save();

            }

            foreach (AttachmentFiles attach in attachs)
            {
                bool isAttachExist = false;
                if (docImportToIsogd.AttachmentFiles.Count > 0)
                {
                    foreach (AttachmentFiles isogdattach in docImportToIsogd.AttachmentFiles)
                        if (isogdattach.FileStore.FileName == attach.FileStore.FileName && isogdattach.FileStore.Size == attach.FileStore.Size)
                        {
                            isAttachExist = true;
                            break;
                        }
                }
                if (!isAttachExist)
                {
                    AttachmentFiles att = connect.CreateObject<AttachmentFiles>();
                    if (attach.Name != null)
                        att.Name = attach.Name;
                    att.RegDate = DateTime.Now.Date;
                    if (attach.Description != null)
                        att.Description = attach.Description;

                    if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                    {
                        Employee currentEmpl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                        if (currentEmpl != null)
                            att.Registrator = currentEmpl;
                    }
                    if (attach.FileStore != null)
                    {
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            attach.FileStore.SaveToStream(ms);
                            FileSystemStoreObject fileSystemStoreObject = connect.CreateObject<FileSystemStoreObject>();
                            fileSystemStoreObject.LoadFromStream(attach.FileStore.FileName, ms);
                            att.FileStore = fileSystemStoreObject;
                            ms.Dispose();
                        }
                    }
                    docImportToIsogd.AttachmentFiles.Add(att);
                    docImportToIsogd.Save();
                    connect.GetUnitOfWork().CommitChanges();
                }
            }
            connect.GetUnitOfWork().CommitChanges();
            //}
            //catch { }
        }

        /// <summary>
        /// Создание документа ИСОГД для его дальнейшей регистрации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddIsogdDocFromDocImportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            DocImportToIsogd docImportToIsogd;
            docImportToIsogd = os.FindObject<DocImportToIsogd>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)docImportToIsogd.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            IsogdDocument isogdDocument;

            isogdDocument = connect.CreateObject<IsogdDocument>();
            try { isogdDocument.DocNo = docImportToIsogd.DocNo; }
            catch { }
            try { isogdDocument.StatementDate = docImportToIsogd.DocDate; }
            catch { }
            try { isogdDocument.IsogdDocumentClass = docImportToIsogd.IsogdDocumentClass; }
            catch { }
            try { isogdDocument.IsogdDocumentNameKind = docImportToIsogd.IsogdDocumentNameKind; }
            catch { }
            try { isogdDocument.Developer = docImportToIsogd.Developer; }
            catch { }
            try { isogdDocument.StatementOrg = docImportToIsogd.StatementOrg; }
            catch { }
            try { isogdDocument.AddressInfo = docImportToIsogd.AddressInfo; }
            catch { }
            try { isogdDocument.CadNo = docImportToIsogd.CadNo; }
            catch { }
            //try { isogdDocument.DocName = docImportToIsogd.DocName; }
            //catch { }
            docImportToIsogd.eDocumentFromDeloStatus = AISOGD.Enums.eDocumentFromDeloStatus.Зарегистрирован;
            // копируем прикрепленные файлы
            try
            {
                foreach (AttachmentFiles attach in docImportToIsogd.AttachmentFiles)
                {
                    try
                    {
                        bool isAttachExist = false;
                        if (isogdDocument.AttachmentFiles.Count > 0)
                        {
                            foreach (AttachmentFiles isogdattach in isogdDocument.AttachmentFiles)
                                if (isogdattach.FileStore.FileName == attach.FileStore.FileName && isogdattach.FileStore.Size == attach.FileStore.Size)
                                {
                                    isAttachExist = true;
                                    break;
                                }
                        }
                        if (!isAttachExist)
                        {
                            AttachmentFiles att = new AttachmentFiles(unitOfWork);
                            if (attach.Name != null)
                                att.Name = attach.Name;
                            att.RegDate = DateTime.Now.Date;
                            if (attach.Description != null)
                                att.Description = attach.Description;

                            if (System.Security.Principal.WindowsIdentity.GetCurrent().Name != null)
                            {
                                Employee currentEmpl = connect.FindFirstObject<Employee>(x => x.SysUser.UserName.ToLower() == System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToLower());
                                if (currentEmpl != null)
                                    att.Registrator = currentEmpl;
                            }
                            if (attach.FileStore != null)
                            {
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    attach.FileStore.SaveToStream(ms);
                                    FileSystemStoreObject fileSystemStoreObject = connect.CreateObject<FileSystemStoreObject>();
                                    fileSystemStoreObject.LoadFromStream(attach.FileStore.FileName, ms);
                                    att.FileStore = fileSystemStoreObject;
                                    ms.Dispose();
                                }
                            }
                            isogdDocument.AttachmentFiles.Add(att);
                            isogdDocument.Save();
                            unitOfWork.CommitChanges();
                        }
                    }
                    catch { }
                }
            }
            catch { }
            // открываем окно документа ИСОГД
            DetailView dv = Application.CreateDetailView(os, isogdDocument);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
        }
    }
}
