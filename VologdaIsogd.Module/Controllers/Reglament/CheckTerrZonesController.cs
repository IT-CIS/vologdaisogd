﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Reglament;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Controllers.Reglament
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CheckTerrZonesController : ViewController
    {
        public CheckTerrZonesController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция синхронизации пространственных и реестровых данных по территориальным зонам (по ГИС данным)
        /// 1. Проверяем все объекты слоя ПЗЗ и ищем связанные реестровые. Если реестровый не находим, то создаем и связваем
        /// 2. По всем ссылкам в SpatialRepository ищем пространственные объекты - если не находим (ссылка пустая, то ее удаляем)
        /// 3. По всем реесровым объектам Террзон смотрим, есть ли связанный пространственный, если связи не находим - удаляем реестровый объект
        /// </summary>
        /// <param name="connect"></param>
        public void CheckTerrZonesLinks(Connect connect)
        {

            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();


            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return;
            }
            // определяем слои связи с Террзонами
            var layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.TerrZone");
            int j = 0;
            foreach (string layer in layers)
            {
                int selObjCount = 0;
                //TableInfoTemp temp = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                //try { m_MapInfo.Do(string.Format("Pack Table {0} Data", layer)); }
                //catch { }
                //try { m_MapInfo.Do(string.Format("Drop Table {0}", "Selection")); }
                //catch { }
                
                //Console.WriteLine(String.Format("Слой: {0}", layer));
                m_MapInfo.Do("Select * From " + layer);// + " into " + temp.Name);

                // получаем количество выделенных объектов МапИнфо
                //selObjCount = Convert.ToInt16(temp.GetRowCount());
                selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    try
                    {
                        m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                        //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                        // Id (MI_PRINX) выделенного объекта мапинфо
                        //try
                        //{
                        #region Заглушка только для дебага - Присвоение идентификаторов 
                        //m_MapInfo.SetId();
                        #endregion
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneName = m_MapInfo.Eval("Selection.ИНТЕГРАЛЬНАЯ_ЗОНА");
                                if (zoneName != null || zoneName != "")
                                {
                                    // создаем террзону 
                                    TerrZone terrZone = connect.CreateObject<TerrZone>();
                                    terrZone.NameSmall = zoneName;
                                    TerrZoneKind terrZoneKind = connect.FindFirstObject<TerrZoneKind>(mc => mc.NameSmall == zoneName);
                                    if (terrZoneKind != null)
                                    {
                                        terrZone.TerrZoneKind = terrZoneKind;
                                        terrZone.Name = terrZoneKind.Name;
                                    }
                                    try { terrZone.ZoneNo = m_MapInfo.Eval("Selection.НОМЕР"); }
                                    catch { }
                                    try { terrZone.YearChange = Convert.ToInt16(m_MapInfo.Eval("Selection.ГОД_ИЗМ")); }
                                    catch { }
                                    try { terrZone.OldZone = m_MapInfo.Eval("Selection.БЫВШАЯ_ЗОНА"); }
                                    catch { }
                                    try { terrZone.TerrZoneDoc = m_MapInfo.Eval("Selection.ДОКУМЕНТ"); }
                                    catch { }
                                    terrZone.Save();
                                    spatial.AddSpatialLink(terrZone.ClassInfo.FullName, terrZone.Oid.ToString(), layer, gisObjId);
                                    connect.GetUnitOfWork().CommitChanges();
                                    //Console.WriteLine(String.Format("Создана {0} территориальная зона: {1}", i.ToString(), zoneName));
                                    j++;
                                }
                            }
                        }
                    }
                    catch { }
                    //}
                    //catch { }
                }
                //m_MapInfo.Do(string.Format("Drop Table {0}", "Selection"));
            }
            // удаляем все пустые ссылки yf ГИС объекты из спатиал репозитория 
            int spd = 0;
            List<SpatialRepository> spRepoListToDel = new List<SpatialRepository>();
            foreach (SpatialRepository spRepo in connect.FindObjects<SpatialRepository>(x=> x.IsogdClassName == "AISOGD.Reglament.TerrZone"))
            {
                SpatialObject sp = new SpatialObject(m_MapInfo, spRepo.SpatialLayerId, spRepo.SpatialObjectId);
                if (!sp.isExist)
                { spRepo.Delete();
                    spd++;
                }
                    //spRepoListToDel.Add(spRepo);
            }
            connect.GetUnitOfWork().CommitChanges();
            // удаляем все пустые ссылки на реестровые объекты из спатиал репозитория 
            //int spdx = 0;
            foreach (SpatialRepository spRepo in connect.FindObjects<SpatialRepository>(x => x.IsogdClassName == "AISOGD.Reglament.TerrZone"))
            {
                if (connect.GetUnitOfWork().FindObject(connect.GetClassInfo(spRepo.IsogdClassName), new BinaryOperator("Oid", spRepo.IsogdObjectId)) == null)
                {
                    spRepo.Delete();
                    spd++;
                }
                //spRepoListToDel.Add(spRepo);
            }
            connect.GetUnitOfWork().CommitChanges();
            //int spd = spRepoListToDel.Count;
            //while (spRepoListToDel.Count > 0)
            //    spRepoListToDel[0].Delete();

            // удаляем реестровые объекты по зонам, которых нет в спатиал репозитории
            List<TerrZone> terrZonesListToDel = new List<TerrZone>();
            int tzd = 0;
            foreach (TerrZone terrZone in connect.FindObjects<TerrZone>(x => x.Oid != null))
            {
                if (!connect.IsExist<SpatialRepository>(x => x.IsogdObjectId == terrZone.Oid.ToString()))
                {
                    //terrZone.Delete();
                    connect.DeleteObject(terrZone);
                    tzd++;
                }
                    //terrZonesListToDel.Add(terrZone);
            }
            connect.GetUnitOfWork().CommitChanges();
            //int tzd = terrZonesListToDel.Count;
            //while (terrZonesListToDel.Count > 0)
            //{
            //    terrZonesListToDel[0].Delete();
            //    connect.GetUnitOfWork().CommitChanges();
            //}

            XtraMessageBox.Show(String.Format(@"Синхронизация по территориальным зонам завершена!
Создано {0} новых реестровых объектов зон. 
Удалено {1} пустых связей реестровых и ГИС объектов.
Удалено {2} старых реестровых объектов зон.",
j.ToString(), spd.ToString(), tzd.ToString()));
        }

        private void CheckTerrZonesGISLinksAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            CheckTerrZonesLinks(connect);
        }
    }
}
