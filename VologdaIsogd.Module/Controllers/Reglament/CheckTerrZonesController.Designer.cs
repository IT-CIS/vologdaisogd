﻿namespace VologdaIsogd.Controllers.Reglament
{
    partial class CheckTerrZonesController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CheckTerrZonesGISLinksAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CheckTerrZonesGISLinksAction
            // 
            this.CheckTerrZonesGISLinksAction.Caption = "Синхронизировать терр.зоны";
            this.CheckTerrZonesGISLinksAction.ConfirmationMessage = null;
            this.CheckTerrZonesGISLinksAction.Id = "CheckTerrZonesGISLinksAction";
            this.CheckTerrZonesGISLinksAction.TargetObjectType = typeof(AISOGD.Reglament.TerrZone);
            this.CheckTerrZonesGISLinksAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.CheckTerrZonesGISLinksAction.ToolTip = null;
            this.CheckTerrZonesGISLinksAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.CheckTerrZonesGISLinksAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CheckTerrZonesGISLinksAction_Execute);
            // 
            // CheckTerrZonesController
            // 
            this.Actions.Add(this.CheckTerrZonesGISLinksAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CheckTerrZonesGISLinksAction;
    }
}
