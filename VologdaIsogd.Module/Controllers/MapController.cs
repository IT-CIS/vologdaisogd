using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Actions;
using DevExpress.Xpo;
using System.Collections;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.Persistent.BaseImpl;
using System.Runtime.InteropServices;
using VologdaIsogd.Module.Controllers.Survey;
using AISOGD.Surveys;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using AISOGD.Isogd;
using VologdaIsogd.Module.Controllers.IsogdControllers;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.IGIS;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application;

namespace VologdaIsogd.Controllers
{
    public partial class MapController : ViewController
    {
        public MapController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            var currentObject = View.CurrentObject as XPBaseObject;
            IList list = ObjectSpace.GetObjects(typeof(SpatialConfig));
            if (View is DetailView)
            {
                if (currentObject != null)
                {
                    if (list.Cast<SpatialConfig>().FirstOrDefault(mc => mc.IsogdClassName == currentObject.ClassInfo.ClassType) != null)// View.ObjectTypeInfo.Type) != null)
                    {
                        Frame.GetController<MapController>().SetInGeoLinkAction.Active.SetItemValue("myReason", true);
                        if (currentObject != null && Connect.FromObjectSpace(ObjectSpace)
                            .IsExist<SpatialRepository>(mc => mc.IsogdObjectId == currentObject.ClassInfo.KeyProperty.GetValue(currentObject).ToString()))
                        {
                            Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", true);
                            Frame.GetController<MapController>().ViewMapAction.Caption = "�������� �� �����";
                        }
                        else
                        { Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", false); }
                    }
                    else
                    {
                        Frame.GetController<MapController>().SetInGeoLinkAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", false);
                    }
                }
            }
            else
            {
                Frame.GetController<MapController>().SetInGeoLinkAction.Active.SetItemValue("myReason", false);
                Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", false);
            }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

       

        private IGISApplication GisApp = null;

        /// <summary>
        /// ����� ���������� � ���������������� �������� ������ � �������� MapInfo (� ��������). � �������� �������� ������ ��������� ��� �����.
        /// �������� 1 � 1.
        /// 1. �������� ������� �� � ����� ����������� �������
        /// 2. �������� �������� ������� � ���� (�������) ����������� ������� �� �����
        /// 3. ���������, ����� �� ������� ���������� ������ �� ����� � ������� ���������� (SpatialConfig)
        /// 4. ���������, ���� �� � �������� ����������� ��� ��������� ���������������� ������� (����� ���������, �� �������� �� ���������� ������ �������)
        /// 5. �����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetLinkAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            GisApp = GISApplication.GetGISApplication();

            if (!GisApp.isReady())
            {
                XtraMessageBox.Show("�� �������� ��� ����������", "����");
                return;
            }

            // 1. �������� id, ����� ����������� ������� � UnitOfWork
            if (e.CurrentObject == null) return;
            var id = ((BaseObject)e.CurrentObject).Oid;
            var isogdClass = ((BaseObject)e.CurrentObject).ClassInfo;
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);

            string selectedLayerName = GisApp.GetActiveLayerName();
            //���������, ����� �� ������� ������� ���������� ������ � ���������� �� �����
            if (!spatial.IsLayerValid(isogdClass.FullName, selectedLayerName))
            {
                XtraMessageBox.Show(
                    "������ ����� ������ ������� � ���������� ��� ��������. ��������� ��������� �����.", "����");
                return;
            }

            // ���������� ��������� ������� � ���
            if (GisApp.CountSelectedObject() == 0)
            {
                XtraMessageBox.Show(
                    "�� ������� �� ������ ����������������� �������.", "����");
                return;
            }
            if (selectedLayerName == EGetActiveLayerName.layer_not_exist.ToString() || selectedLayerName.ToLower().IndexOf("error") != -1)
            {
                XtraMessageBox.Show(
                    "�� ������ ���� ���.", "����");
                return;
            }
            else
            {
                // 3. ���������, ����� �� ������� ������� ���������� ������ � ���������� �� �����
                if (!spatial.IsLayerValid(isogdClass.FullName, selectedLayerName))
                {
                    XtraMessageBox.Show(
                        "������ ����� ������ ������� � ���������� ��� ��������. ��������� ��������� �����.", "����");
                    return;
                }
                List<string> linkedObjIds = GisApp.SetLink(isogdClass.FullName, id.ToString());
                foreach (string mapObjectId in linkedObjIds)
                {
                    FillSemanticFromReestr(isogdClass, connect, id, selectedLayerName, mapObjectId);
                }
            }
        }


        /// <summary>
        /// �������� ��������� ���������������� ������ �� �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewMapAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            GisApp = GISApplication.GetGISApplication();
            if (!GisApp.isReady())
            {
                XtraMessageBox.Show("�� �������� ��� ����������", "����");
                return;
            }

            // 1. �������� id, ����� ����������� ������� � UnitOfWork
            if (e.CurrentObject == null) return;
            var id = ((BaseObject)e.CurrentObject).Oid;
            var isogdClass = ((BaseObject)e.CurrentObject).ClassInfo;
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);

            bool isExits = false;
            var objs = spatial.GetSpatialLinks(isogdClass.FullName, id.ToString());


            // ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository 
            foreach (var spatialRepository in objs)
            {
                try
                {
                    //TODO ������� ��� �������������� ���������

                    //string gisObjId = spatialRepository.SpatialObjectId;
                    //if (idQuery == "")
                    //    idQuery = " MI_PRINX = " + gisObjId;
                    //else
                    //    idQuery += " OR MI_PRINX = " + gisObjId;

                    string gisObjId = spatialRepository.SpatialObjectId;
                    
                    string layer = spatialRepository.SpatialLayerId;
                    //XtraMessageBox.Show(String.Format("gisObjId: {0}; layer: {1}; , spatialRepository: {2}", gisObjId, layer, spatialRepository.Oid.ToString()));
                    if (GisApp.ZoomLinkedObject(gisObjId, layer) == "True")
                    {
                        isExits = true;
                        GisApp.Activate();
                        break;
                    }

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                }
            }

            if (!isExits)
                XtraMessageBox.Show("������� �� ����������", "����");
        }


        private void FillSemanticFromReestr(XPClassInfo isogdClass, Connect connect, Guid dataObjectID, string layer, string mapObjectID)
        {
            GisApp = GISApplication.GetGISApplication();
            if (!GisApp.isReady())
            {
                XtraMessageBox.Show("�� �������� ��� ����������", "����");
                return;
            }
            else
            {
                if (isogdClass.FullName == typeof(Survey).FullName)
                {
                    Survey survey = connect.FindFirstObject<Survey>(x => x.Oid == dataObjectID);
                    survey.FillSurveySemantic(GisApp, layer, mapObjectID);
                    CheckSubSurvey(connect, layer, mapObjectID);
                }
                if (isogdClass.FullName == typeof(IsogdStorageBook).FullName)
                {
                    IsogdStorageBook book = connect.FindFirstObject<IsogdStorageBook>(x => x.Oid == dataObjectID);
                    book.FillStorageBookSemantic(GisApp, layer, mapObjectID);
                }
                if (isogdClass.FullName == typeof(IsogdPhysStorageBook).FullName)
                {
                    IsogdPhysStorageBook book = connect.FindFirstObject<IsogdPhysStorageBook>(x => x.Oid == dataObjectID);
                    book.FillPhysStorageBookSemantic(GisApp, layer, mapObjectID);
                }
            }
        }


        public void CheckSubSurvey(Connect connect, string layerName, string mapObjectId)
        {
            GisApp = GISApplication.GetGISApplication();
            MapInfoApplication m_MapInfo = null;
            if (!GisApp.isReady())
            {
                XtraMessageBox.Show("�� �������� ��� ����������", "����");
                return;
            }

            //TODO ������� ��� QGIS
            if (GisApp.GetApplicationType() == GISApplicationType.MI)
            {
                m_MapInfo = (MapInfoApplication)GisApp;
            }
            else
                return;

            //�������� ���� ��� �������� ��������
            TableInfo archiveTableInfo = new TableInfo(m_MapInfo, SurveyHelper.GetArchiveLayerName(ObjectSpace));

            if (!archiveTableInfo.isTableExist)
            {
                XtraMessageBox.Show("�� ���������� ������� " + archiveTableInfo.Name);
                return;
            }

            var spatial = new SpatialController<GisLayer>(connect);

            //�������� ������ � ��������� �� ���� ������
            m_MapInfo.Do($"Select * From {layerName} Where MI_PRINX = {mapObjectId}");
            m_MapInfo.Fetch(EFetch.Rec, "Selection", 0);

            //������� �������
            TableInfo selectedObjectsOrigTable = new TableInfo(m_MapInfo, m_MapInfo.SelectionTable.Name);

            //�������� ������ ��� ������ - ���������� ��������������� 
            m_MapInfo.SetId();


            //��������� ID ����������� �������
            string selectionID = m_MapInfo.SelectionTable.GetFirstID();
            //������� ���� � ������� ��������
            TableInfo originalTable = new TableInfo(m_MapInfo, m_MapInfo.SelectionTable.Name);
            //��������� ���������� ������ � �������
            TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
            m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));
            //XtraMessageBox.Show("selectedObjectsTempTable=" + selectedObjectsTempTable.Name);
            //�������� ������ ��� ������ - ���������� ��������������� 
            m_MapInfo.SetId();

            m_MapInfo.SelectionTable.Close();
            //����� ����������� � ������� ��������� �������
            //obj - ���� ���������
            //������� �������������� �������
            TableInfo intersectsTempTable = new TableInfo(m_MapInfo, "intersectsTempTable");
            m_MapInfo.Do(string.Format("Select * From {0} Where obj Intersects (Select obj From {1} Where MI_PRINX = {2}) AND MI_PRINX <> {2} Into {3}",
                    originalTable.Name, selectedObjectsTempTable.Name, selectionID, intersectsTempTable.Name));

            //XtraMessageBox.Show("intersectsTempTable=" + intersectsTempTable.Name);
            //����������� �������������� ��������
            long rowCount = intersectsTempTable.GetRowCount() ?? 0;

            //XtraMessageBox.Show("����������� �������������� ��������=" + rowCount.ToString());
            for (int i = 1; i <= rowCount; i++)
            {
                //�������� �������������� ������
                m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1}", intersectsTempTable.Name, i));

                //�������� ��� ���������� �������� ... 
                string crossObjectID = m_MapInfo.SelectionTable.GetFirstID();
                //XtraMessageBox.Show("crossObjectID=" + crossObjectID);
                IQueryable<SpatialRepository> links = null;
                Survey crossSurveyCard = null;
                if (!string.IsNullOrWhiteSpace(crossObjectID))
                {
                    links = spatial.GetIsogdLinks(m_MapInfo.SelectionTable.Name, crossObjectID);
                }
                if (links != null)
                {
                    if (links.Count() > 0)
                    {
                        crossSurveyCard = ObjectSpace.GetObjects<Survey>(new BinaryOperator("Oid", links.First().IsogdObjectId)).First();//connect.FindFirstObject<AISOGD.Surveys.Survey>(x => x.Oid.ToString() == links.First().IsogdObjectId);
                        //XtraMessageBox.Show("crossSurveyCardSurveyObject=" + crossSurveyCard.SurveyObject);
                    }
                }

                //�������� ��� � target
                m_MapInfo.Target.SetTargetOn();

                if (crossSurveyCard != null)
                {
                    //������� ������ �����
                    spatial.DeleteSpatialLinks(typeof(Survey).FullName, crossSurveyCard.Oid.ToString());
                }

                //����� ���������
                m_MapInfo.SelectionTable.Close();

                //�������� �������� ������
                m_MapInfo.Do(string.Format("Select * From {0} Where MI_PRINX = {1}  Into Selection", selectedObjectsTempTable.Name, selectionID));
                //XtraMessageBox.Show("SplitObjects(true) - ������");
                //��������� � ����������� ���������/ true - ���������� ��� ����
                m_MapInfo.SplitObjects(true);
                selectedObjectsOrigTable.CommitChanges();
                //XtraMessageBox.Show("SplitObjects(true) - �����");
                //��������� ��������� ���������� �� ��������� �������
                TableInfo resultSplitTempTable = new TableInfo(m_MapInfo, "resultSplitTempTable");
                m_MapInfo.Do(string.Format("Select * From Selection Into {0}", resultSplitTempTable.Name));
                //XtraMessageBox.Show("resultSplitTempTable:" + resultSplitTempTable.Name);
                //����� ���������
                m_MapInfo.SelectionTable.Close();

                //����������� ����������� ��������
                long resultRowCount = resultSplitTempTable.GetRowCount() ?? 0;

                //������� ������� �� ������� �����������, ������� ������ ��� ������� �������������
                TableInfoTemp containsObjectTempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                //XtraMessageBox.Show("containsObjectTempTable:" + containsObjectTempTable.Name);
                for (int j = 1; j <= resultRowCount; j++)
                {
                    m_MapInfo.SelectionTable.Close();
                    TableInfoTemp tempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                    m_MapInfo.Do(string.Format("Select * From {0} Where RowID = {1} Into {2}",
                        resultSplitTempTable.Name, j, tempTable.Name));
                    m_MapInfo.SelectionTable.Close();
                    //XtraMessageBox.Show("tempTable2:");
                    TableInfoTemp tempTable2 = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                    m_MapInfo.Do(string.Format("Select * From {0} Where obj Within (Select obj from {1} Into {2}) Into {3}",
                        tempTable.Name, selectedObjectsTempTable.Name, tempTable2.Name, containsObjectTempTable.Name));
                    tempTable.Dispose();
                    tempTable2.Dispose();

                    //XtraMessageBox.Show("111111:");
                    long resultIntersectsRowCount = containsObjectTempTable.GetRowCount() ?? 0;
                    if (resultIntersectsRowCount == 0)
                    {
                        //���� �������� ������ �� ������������, �� ������� �� ������ ���������
                        //XtraMessageBox.Show("22222222:");
                        //����� ���������
                        m_MapInfo.SelectionTable.Close();

                        //�������� ������
                        m_MapInfo.Do(string.Format("Select * From {0} Where RowID = {1}", resultSplitTempTable.Name, j));
                        if (crossSurveyCard != null)
                        {
                            // ��������� ����� �����
                            spatial.AddSpatialLink(typeof(AISOGD.Surveys.Survey).FullName, crossSurveyCard.Oid.ToString(),
                                originalTable.Name,
                                m_MapInfo.SelectionTable.GetFirstID());
                        }

                    }
                    else
                    {
                        //�����, ����������� ���������������� ������ � ����� �
                        //����������� �������� ��� ��������� �������
                        //XtraMessageBox.Show("33333333:");
                        //����� ���������
                        m_MapInfo.SelectionTable.Close();

                        //�������� ������
                        m_MapInfo.Do(string.Format("Select * From {0} Where RowID = {1}", resultSplitTempTable.Name, j));

                      
                        SpatialObject sp = new SpatialObject(m_MapInfo, m_MapInfo.SelectionTable.Name, m_MapInfo.SelectionTable.GetFirstID());
                        string newObjectRowID = sp.CloneSpatialObject(archiveTableInfo.Name);
                        // ���������� ���� �������� � �����
                        //try
                        //{
                        //�������� ������������� ������
                        m_MapInfo.Do(string.Format("Select * From {0} Where RowID = {1}", archiveTableInfo.Name, newObjectRowID));
                        m_MapInfo.Do(string.Format("Update Selection Set ����_�������� = CurDate( )"));//, DateTime.Now.Date));
                        //}
                        //catch { }
                        archiveTableInfo.CommitChanges();

                        m_MapInfo.Do(string.Format("Delete From {0} Where RowID = {1}", resultSplitTempTable.Name, j));
                        selectedObjectsOrigTable.CommitChanges();

                        
                        //���� � ����������������� ������� ��� ��������� ������, �� ��������� ���
                        if (crossSurveyCard != null)
                        {
                            #region ����������� ��������
                            //ObjectSpace.GetObjects<AISOGD.Surveys.Survey>(new BinaryOperator("Oid", links.First().IsogdObjectId));
                            AISOGD.Surveys.Survey cloneSurveyCard = ObjectSpace.CreateObject<AISOGD.Surveys.Survey>();
                            try
                            {
                                cloneSurveyCard.ArhNo = (crossSurveyCard.ArhNo ?? "") + " �����";
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.DrillingDepth = crossSurveyCard.DrillingDepth;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.DZUProjectText = crossSurveyCard.DZUProjectText;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.Elevation = crossSurveyCard.Elevation;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.Empl = crossSurveyCard.Empl;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.InvNo = crossSurveyCard.InvNo;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.MapMaterialForm = crossSurveyCard.MapMaterialForm;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.MoraineDepth = crossSurveyCard.MoraineDepth;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.MoraineElevation = crossSurveyCard.MoraineElevation;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.MoundPower = crossSurveyCard.MoundPower;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.Notes = crossSurveyCard.Notes;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.ObjectAddress = crossSurveyCard.ObjectAddress;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.PeatPower = crossSurveyCard.PeatPower;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.RegDate = crossSurveyCard.RegDate;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.RequesterString = crossSurveyCard.RequesterString;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyDocs = crossSurveyCard.SurveyDocs;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyingJobType = crossSurveyCard.SurveyingJobType;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyKind = crossSurveyCard.SurveyKind;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyLocation = crossSurveyCard.SurveyLocation;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyName = crossSurveyCard.SurveyName;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyObject = crossSurveyCard.SurveyObject;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyStatus = AISOGD.Enums.eSurveyStatus.�����;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveySubject = crossSurveyCard.SurveySubject;
                            }
                            catch { }
                            try
                            {
                                cloneSurveyCard.SurveyYear = crossSurveyCard.SurveyYear;
                            }
                            catch { }

                            try
                            {
                                cloneSurveyCard.WaterDepth = crossSurveyCard.WaterDepth;
                            }
                            catch { }

                            try
                            {
                                foreach (AISOGD.Isogd.IsogdPhysStorageBook isogdPhysStorageBook in crossSurveyCard.IsogdPhysStorageBooks)
                                {
                                    cloneSurveyCard.IsogdPhysStorageBooks.Add(isogdPhysStorageBook);
                                }
                            }
                            catch { }
                            try
                            {
                                foreach (AISOGD.Subject.GeneralSubject generalSubject in crossSurveyCard.GeneralSubjects)
                                {
                                    cloneSurveyCard.GeneralSubjects.Add(generalSubject);
                                }
                            }
                            catch { }

                            #endregion
                            cloneSurveyCard.Save();
                            ObjectSpace.CommitChanges();
                            m_MapInfo.Do(string.Format("Fetch Rec {0} From {1}", newObjectRowID, archiveTableInfo.Name));
                            // ��������� ����� �����
                            string newObjectID = m_MapInfo.Eval(archiveTableInfo.Name + ".MI_PRINX");
                            spatial.AddSpatialLink(typeof(Survey).FullName, cloneSurveyCard.Oid.ToString(),
                                archiveTableInfo.Name,
                                newObjectID);
                        }
                    }
                }
                resultSplitTempTable.Dispose();
            }


            //�������� �������� ������
            m_MapInfo.Do(string.Format("Select * From {0} Where MI_PRINX = {1} Into Selection", selectedObjectsTempTable.Name, selectionID));

            //��������� ��������� � �������� ����
            originalTable.CommitChanges();

            //������� ��������� ����
            intersectsTempTable.Dispose();
            selectedObjectsTempTable.Dispose();

            //������� ��� ���� ��������
            int NumTables = Convert.ToInt16(m_MapInfo.Eval("NumTables()"));
            List<string> queryTablesList = new List<string>();
            for (int k = 1; k <= NumTables; k++)
            {
                TableInfo TableToClose = new TableInfo(m_MapInfo, k);
                if (TableToClose.Name.IndexOf("Query") != -1)
                    queryTablesList.Add(TableToClose.Name);
            }
            foreach (string queryTable in queryTablesList)
            {
                try
                {
                    m_MapInfo.TableInfo(queryTable).Dispose();
                }
                catch { }
            }
        }




        ///// <summary>
        ///// �������� ��������� ���������������� ������ �� �����
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void ViewMapAction_Execute_old (object sender, SimpleActionExecuteEventArgs e)
        //{
        //    // 1. �������� id, ����� ����������� ������� � UnitOfWork
        //    if (e.CurrentObject == null) return;
        //    var id = ((BaseObject)e.CurrentObject).Oid;
        //    var isogdClass = ((BaseObject)e.CurrentObject).ClassInfo;
        //    Connect connect = Connect.FromSettings();
        //    var spatial = new SpatialController<GisLayer>(connect);

        //    // 2. �������� �������� �������
        //    m_MapInfo = new MapInfoApplication();
        //    if (m_MapInfo.mapinfo == null)
        //    {
        //        XtraMessageBox.Show("MapInfo �� ��������", "����");
        //        return;
        //    }
        //    // ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository 
        //    var objs = spatial.GetSpatialLinks(isogdClass.FullName, id.ToString());
        //    string idQuery = "";
        //    string layer = "";
        //    foreach (var spatialRepository in objs)
        //    {
        //        try
        //        {

        //            string gisObjId = spatialRepository.SpatialObjectId;
        //            if (idQuery == "")
        //                idQuery = " MI_PRINX = " + gisObjId;
        //            else
        //                idQuery += " OR MI_PRINX = " + gisObjId;
        //            layer = spatialRepository.SpatialLayerId;
        //        }
        //        catch (Exception ex)
        //        {
        //            XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
        //        }

        //    }
        //    try
        //    {
        //        m_MapInfo.Do("Select * From " + layer + " Where " + idQuery + " Into ZoomObject");
        //        // m_MapInfo.Do("Run Menu Command 306");
        //        m_MapInfo.Do("Add Map Layer ZoomObject Fetch first From ZoomObject Set Map Center (CentroidX(ZoomObject.obj),CentroidY(ZoomObject.obj)) " +
        //            "Add Map Layer ZoomObject Set Map Zoom Entire Layer ZoomObject");
        //        m_MapInfo.Do("Remove Map Layer \"ZoomObject\"");
        //        m_MapInfo.Do("Remove Map Layer \"ZoomObject\"");
        //    }
        //    catch { }

        //    /////������� ����� ����� ������� ������� - ����� ������������ � ���� (XafId, XafClass) ������
        //    //// �� ������� �������� ������ �����, � ������� ����� ������
        //    //foreach (string layer in spatial.GetListLinkedSpatialLayers(isogdClass.FullName))
        //    //{
        //    //    if (spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()))
        //    //    {
        //    //        spatial.GetLinkedMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //        break;
        //    //    }
        //    //}

        //    System.Diagnostics.Process[] p = System.Diagnostics.Process.GetProcessesByName("MapInfow");
        //    if (p.Length > 0)
        //    {
        //        ShowWindow(p[0].MainWindowHandle, 10);
        //        ShowWindow(p[0].MainWindowHandle, 5);
        //        SetForegroundWindow(p[0].MainWindowHandle);
        //    }
        //}


        /// <summary>
        /// ����� ���������� � ���������������� �������� ������ � �������� MapInfo (� ��������). � �������� �������� ������ ��������� ��� �����.
        /// �������� 1 � 1.
        /// 1. �������� ������� �� � ����� ����������� �������
        /// 2. �������� �������� ������� � ���� (�������) ����������� ������� �� �����
        /// 3. ���������, ����� �� ������� ���������� ������ �� ����� � ������� ���������� (SpatialConfig)
        /// 4. ���������, ���� �� � �������� ����������� ��� ��������� ���������������� ������� (����� ���������, �� �������� �� ���������� ������ �������)
        /// 5. �����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void SetInGeoLinkAction_Execute_old(object sender, SimpleActionExecuteEventArgs e)
        //{
        //    View.ObjectSpace.CommitChanges();
        //    // 1. �������� id, ����� ����������� ������� � UnitOfWork
        //    if (e.CurrentObject == null) return;
        //    var id = ((BaseObject)e.CurrentObject).Oid;
        //    var isogdClass = ((BaseObject)e.CurrentObject).ClassInfo;
        //    Connect connect = Connect.FromSettings();
        //    var spatial = new SpatialController<GisLayer>(connect);

        //    // 2. �������� �������� �������
        //    m_MapInfo = new MapInfoApplication();

        //    if (m_MapInfo.mapinfo == null)
        //    {
        //        XtraMessageBox.Show("MapInfo �� ��������", "����");
        //        return;
        //    }


        //    // �������� ���������� ���������� �������� �������
        //    int selObjCount = m_MapInfo.SelectionTable.GetCount() ?? 0;

        //    // ���������� ��������� ������� � �������
        //    if (selObjCount == 0)
        //    {
        //        XtraMessageBox.Show(
        //            "�� ������� �� ������ ����������������� �������.", "����");
        //        return;
        //    }

        //    // 2. �������� ������� (����) ���������
        //    var layer = m_MapInfo.SelectionTable.Name;

        //    //XtraMessageBox.Show("layer: " + layer);
        //    // 3. ���������, ����� �� ������� ������� ���������� ������ � ���������� �� �����
        //    if (!spatial.IsLayerValid(isogdClass.FullName, layer))
        //    {
        //        XtraMessageBox.Show(
        //            "������ ����� ������ ������� � ���������� ��� ��������. ��������� ��������� �����.", "����");
        //        return;
        //    }


        //    // ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository 
        //    DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
        //    var xtraMessageBoxForm = new XtraMessageBoxForm();

        //    // ���� �������� ������ ������ ����������������� ������� �� ����� - ������� ��������� �� ����
        //    if (selObjCount > 1)
        //    {
        //        if (
        //            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
        //                String.Format("�������� {0} �������� ������� {1}. ����������?",
        //                    selObjCount.ToString(), layer),
        //                "��������!", dialogResults, null, 0)) == DialogResult.No)
        //        { return; }
        //    }

        //    // ���� � ����������� ������� ��� ����� � �����������������
        //    if (!spatial.IsObjectLinked(isogdClass.FullName, id.ToString()))
        //    {
        //        for (int i = 1; i < selObjCount + 1; i++)
        //        {
        //            m_MapInfo.Fetch(EFetch.Rec, "Selection", i);
        //            //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
        //            // Id (MI_PRINX) ����������� ������� �������
        //            try
        //            {
        //                #region �������� ������ ��� ������ - ���������� ��������������� 
        //                m_MapInfo.SetId();
        //                #endregion
        //                string gisObjId = m_MapInfo.SelectionTable.GetFirstID();
        //                if (gisObjId != "0")
        //                {
        //                    if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
        //                    {
        //                        xtraMessageBoxForm = new XtraMessageBoxForm();
        //                        if (
        //                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
        //                                String.Format("������ ��� ������ [MI_PRINX: {0}] ��� ������ � ����������. �����������?",
        //                                    gisObjId),
        //                                "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
        //                        {
        //                            //������� ������ �����
        //                            spatial.DeleteSpatialGISLinks(layer, gisObjId);

        //                            // ��������� �����
        //                            spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, gisObjId);
        //                            FillSemanticFromReestr(isogdClass, connect, id, layer);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, gisObjId);
        //                        FillSemanticFromReestr(isogdClass, connect, id, layer);
        //                    }
        //                }
        //                else
        //                {
        //                    MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
        //                    return;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
        //            }
        //        }
        //    }
        //    //���� ����, �������� �� ����, � ����������, ������� ��� � ����� ��� �������������
        //    //���� ��������� - ���������� ���������� � ��������� - ����� ���������� ��� ������ � ����� �� ����������
        //    else
        //    {
        //        //DialogResult[] dialogResult = { DialogResult.OK, DialogResult.Yes, DialogResult.Cancel };
        //        var form = new DialogForm
        //        {
        //            StartPosition = FormStartPosition.CenterParent,
        //            label1 =
        //            {
        //                Text = String.Format(
        //                    "������ ���������� ������ [{0}] ��� ������ � �����������������. ������� �� ������ " +
        //                    " '��' ���� ������ �����������, �� ������ '��������', ���� ������ �������� �����, �� ������ '������' ��� ������",
        //                    CaptionHelper.GetClassCaption(isogdClass.FullName))
        //            }
        //        };
        //        form.ShowDialog();

        //        var result = form.DialogResult;
        //        if (result == DialogResult.Cancel)
        //        {
        //            return;
        //        }
        //        if (result == DialogResult.OK)
        //        {
        //            spatial.DeleteSpatialLinks(isogdClass.FullName, id.ToString());
        //            ObjectSpace.CommitChanges();
        //            for (int i = 1; i < selObjCount + 1; i++)
        //            {
        //                m_MapInfo.Fetch(EFetch.Rec, "Selection", i);
        //                try
        //                {
        //                    #region �������� ������ ��� ������ - ���������� ��������������� 
        //                    m_MapInfo.SetId();
        //                    #endregion
        //                    // Id (MI_PRINX) ����������� ������� �������
        //                    string gisObjId = m_MapInfo.SelectionTable.GetFirstID();
        //                    if (gisObjId != "0")
        //                    {
        //                        if (!spatial.IsObjectLinkExist(isogdClass.FullName, id.ToString(), layer, gisObjId))
        //                        {
        //                            if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
        //                            {
        //                                xtraMessageBoxForm = new XtraMessageBoxForm();
        //                                if (
        //                                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
        //                                        String.Format("������ ��� ������ [MI_PRINX: {0}] ��� ������ � ����������. �����������?",
        //                                            gisObjId),
        //                                        "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
        //                                {
        //                                    //������� ������ ����� ��� ��� �������
        //                                    try
        //                                    {
        //                                        spatial.DeleteSpatialGISLinks(layer, gisObjId);
        //                                    }
        //                                    catch { }

        //                                    //������� ������ ����� ��� ����������� �������
        //                                    try
        //                                    {
        //                                        spatial.DeleteSpatialLinks(isogdClass.FullName, id.ToString());
        //                                    }
        //                                    catch { }

        //                                    spatial.AddSpatialLink
        //                                        (isogdClass.FullName, id.ToString(), layer, gisObjId);

        //                                    FillSemanticFromReestr(isogdClass, connect, id, layer);
        //                                }
        //                            }
        //                            else
        //                            {

        //                                //������� ������ ����� ��� ����������� �������
        //                                try
        //                                {
        //                                    spatial.DeleteSpatialLinks(isogdClass.FullName, id.ToString());
        //                                }
        //                                catch { }
        //                                spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
        //                                    layer,
        //                                    gisObjId);
        //                                FillSemanticFromReestr(isogdClass, connect, id, layer);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
        //                        return;
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
        //                }
        //            }
        //        }
        //        if (result == DialogResult.Ignore)
        //        {
        //            for (int i = 1; i < selObjCount + 1; i++)
        //            {
        //                m_MapInfo.Fetch(EFetch.Rec, "Selection", i);
        //                // Id (MI_PRINX) ����������� ������� �������
        //                string gisObjId = m_MapInfo.SelectionTable.GetFirstID();
        //                #region �������� ������ ��� ������ - ���������� ��������������� 
        //                m_MapInfo.SetId();
        //                #endregion
        //                if (gisObjId != "0")
        //                {
        //                    if (!spatial.IsObjectLinkExist(isogdClass.FullName, id.ToString(), layer, gisObjId))
        //                    {
        //                        if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
        //                        {
        //                            xtraMessageBoxForm = new XtraMessageBoxForm();
        //                            if (
        //                                xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
        //                                    String.Format("������ ��� ������ [MI_PRINX: {0}] ��� ������ � ����������. �����������?",
        //                                        gisObjId),
        //                                    "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
        //                            {
        //                                spatial.AddSpatialLink
        //                                    (isogdClass.FullName, id.ToString(), layer, gisObjId);
        //                                FillSemanticFromReestr(isogdClass, connect, id, layer);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            spatial.AddSpatialLink
        //                                (isogdClass.FullName, id.ToString(), layer, gisObjId);

        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
        //                    return;
        //                }
        //            }
        //        }
        //    }

        //    #region /// ������� ����� ����� ������� ������� - ����� ������������ � ���� (XafId, XafClass) ������
        //    // 4. ���������, ������ �� ������� ���������� ������ � ����� ������ ����������������

        //    ////XtraMessageBox.Show("������ �� ������� ���������� ������ � ����� ������ ����������������: " + spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()).ToString());
        //    //if (!spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()))
        //    //{
        //    //    // ���������� �� ������
        //    //    // ���������� � ���������������� ������ ������ � �����
        //    //    isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //    // ���������� � SpatialRepository ����������, ��� �������
        //    //    string gisObjId = "1";
        //    //    try
        //    //    {
        //    //        m_MapInfo.Do("Select MI_PRINX from Selection Into tmpTable");
        //    //        //    selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
        //    //        //    for (int i = 1; i < selObjCount + 1; i++)
        //    //        //    {
        //    //        m_MapInfo.Do("Fetch Rec first From tmpTable");
        //    //        // Id ����������� ������� ��������� �����
        //    //        gisObjId = m_MapInfo.Eval("tmpTable.Col1");
        //    //    }
        //    //    catch { }
        //    //    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, gisObjId);
        //    //}
        //    //else
        //    //{
        //    //    // ���������� ��� ������ � ����� �� ����������������
        //    //    //var xtraMessageBoxForm = new XtraMessageBoxForm();
        //    //    //if (
        //    //    //    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
        //    //    //        String.Format("������ ���������� ������ [{0}] ��� ������ � ����������������. �����������?",
        //    //    //            CaptionHelper.GetClassCaption(isogdClass.FullName)),
        //    //    //        "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
        //    //    //{
        //    //    //    spatial.DelLinkMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //    //    isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //    //    // ���������� � SpatialRepository ����������, ��� �������
        //    //    //    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
        //    //    //}

        //    //    // ����������, �������� ��������, ������� ��� � ���� ��� �������������
        //    //    var form = new DialogForm
        //    //    {
        //    //        StartPosition = FormStartPosition.CenterParent,
        //    //        label1 =
        //    //        {
        //    //            Text = String.Format(
        //    //                "������ ���������� ������ [{0}] ��� ������ � ����������������. ������� �� ������ " +
        //    //                " '��' ���� ������ �����������, �� ������ '��������', ���� ������ �������� �����, �� ������ '������' ��� ������",
        //    //                CaptionHelper.GetClassCaption(isogdClass.FullName))
        //    //        }
        //    //    };
        //    //    form.ShowDialog();
        //    //    XtraMessageBoxForm xtraMessageBoxForm;
        //    //    var result = form.DialogResult;
        //    //    // ������
        //    //    if (result == DialogResult.Cancel)
        //    //    {
        //    //        return;
        //    //    }
        //    //    // �����������
        //    //    if (result == DialogResult.OK)
        //    //    {
        //    //        spatial.DelLinkMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //        isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //        // ���������� � SpatialRepository ����������, ��� �������
        //    //        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
        //    //    }
        //    //    // �������� �����
        //    //    if (result == DialogResult.Ignore)
        //    //    {
        //    //        isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
        //    //        // ���������� � SpatialRepository ����������, ��� �������
        //    //        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
        //    //    }
        //    //}
        //    //if (isOk)
        //    //    XtraMessageBox.Show("�������� ���������", "����");
        //    //else
        //    //    XtraMessageBox.Show("��� �� ����� �� ���", "����");
        //    #endregion

        //    XtraMessageBox.Show("�������� ���������", "����");
        //}

        //[DllImport("user32.dll")]
        //private static extern bool ShowWindow(IntPtr handle, int cmdShow);
        //[DllImport("user32.dll")]
        //private static extern int SetForegroundWindow(IntPtr handle);
    }
}
