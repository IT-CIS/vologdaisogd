namespace VologdaIsogd.Module.Controllers.Constr
{
    partial class ConstrPermAddInfoController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ConstrStageAddInfoAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.UsePermAddInfoAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ConstrStageAddInfoAction
            // 
            this.ConstrStageAddInfoAction.Caption = "�������� ���������� �� ���";
            this.ConstrStageAddInfoAction.ConfirmationMessage = null;
            this.ConstrStageAddInfoAction.Id = "ConstrStageAddInfoAction";
            this.ConstrStageAddInfoAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ConstrStageAddInfoAction.TargetObjectType = typeof(AISOGD.Constr.ConstrStage);
            this.ConstrStageAddInfoAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ConstrStageAddInfoAction.ToolTip = null;
            this.ConstrStageAddInfoAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ConstrStageAddInfoAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ConstrStageAddInfoAction_Execute);
            // 
            // UsePermAddInfoAction
            // 
            this.UsePermAddInfoAction.Caption = "�������� ���������� �� ���";
            this.UsePermAddInfoAction.ConfirmationMessage = null;
            this.UsePermAddInfoAction.Id = "UsePermAddInfoAction";
            this.UsePermAddInfoAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.UsePermAddInfoAction.TargetObjectType = typeof(AISOGD.Perm.UsePerm);
            this.UsePermAddInfoAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.UsePermAddInfoAction.ToolTip = null;
            this.UsePermAddInfoAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.UsePermAddInfoAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UsePermAddInfoAction_Execute);
            // 
            // ConstrPermAddInfoController
            // 
            this.Actions.Add(this.ConstrStageAddInfoAction);
            this.Actions.Add(this.UsePermAddInfoAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ConstrStageAddInfoAction;
        private DevExpress.ExpressApp.Actions.SimpleAction UsePermAddInfoAction;
    }
}
