using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Constr;
using DevExpress.Xpo;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.Persistent.BaseImpl;

namespace VologdaIsogd.Module.Controllers.Constr
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class ConstrPermAddInfoController : ViewController
    {
        public ConstrPermAddInfoController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ConstrStageAddInfoAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();

            var x_id = ((BaseObject)e.CurrentObject).Oid;


            ConstrStage constrStage;
            constrStage = os.FindObject<ConstrStage>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)constrStage.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            
            ObjectConstractionInfo objectConstractionInfo;
            //IObjectSpace os = View.ObjectSpace;//Application.CreateObjectSpace(); // Create IObjectSpace or use the existing one, e.g. View.ObjectSpace
            
            if(constrStage.ObjectConstractionInfo.Count > 0)
                objectConstractionInfo = constrStage.ObjectConstractionInfo.FirstOrDefault();
            else
            {
                objectConstractionInfo = os.CreateObject<ObjectConstractionInfo>();
                objectConstractionInfo.ConstrStage = constrStage;

                try { objectConstractionInfo.BuildingKind = constrStage.CapitalStructureBase.BuildingKind; }
                catch { }
                #region ������� ������ �� ��� ������� � ��� - ����� ����� ������
                //try
                //{
                //    if (constrStage.ConstructionType != null)
                //    {
                //        if (constrStage.ConstructionType.ParentConstructionUsing == null)
                //        {
                //            if (constrStage.ConstructionType.Name.ToLower() == "�����")
                //                objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�����;
                //            else
                //                objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //        }
                //        else
                //        {
                //            if (constrStage.ConstructionType.ParentConstructionUsing.ParentConstructionUsing == null)
                //            {
                //                if (constrStage.ConstructionType.ParentConstructionUsing.Name.ToLower() == "�����")
                //                    objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�����;
                //                else
                //                {
                //                    switch (constrStage.ConstructionType.Name.ToLower())
                //                    {
                //                        case "������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "���������- ���������������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.���������������������������;
                //                            break;
                //                        case "��������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "��������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������� ������� ���������������-������������� ����������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "�����������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������������ �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.���������������������������;
                //                            break;
                //                        case "�������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "��������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.���������������������������;
                //                            break;
                //                        case "��������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "���������� �������� �����":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "�����":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "��������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "�������� ������-�������� ����":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.��������;
                //                            break;
                //                        default:
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                if (constrStage.ConstructionType.ParentConstructionUsing.ParentConstructionUsing.Name.ToLower() == "�����")
                //                    objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�����;
                //                else
                //                {
                //                    switch (constrStage.ConstructionType.ParentConstructionUsing.Name.ToLower())
                //                    {
                //                        case "������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "���������- ���������������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.���������������������������;
                //                            break;
                //                        case "��������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "��������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������� ������� ���������������-������������� ����������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "�����������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������������ �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.���������������������������;
                //                            break;
                //                        case "�������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "��������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.���������������������������;
                //                            break;
                //                        case "��������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "���������� �������� �����":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "�����":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "��������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "������������� �������":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                        case "�������� ������-�������� ����":
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.��������;
                //                            break;
                //                        default:
                //                            objectConstractionInfo.BuildingKind = AISOGD.Enums.eBuildingKind.�������;
                //                            break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                //catch { }
                #endregion

                if (constrStage.TotalBuildSquareProject != 0)
                    objectConstractionInfo.TotalBuildSquareProject = constrStage.TotalBuildSquareProject;
                if (constrStage.BuildingSizeProject != 0)
                    objectConstractionInfo.BuildingSizeProject = constrStage.BuildingSizeProject;
                if (constrStage.BuildingSizeUnderGroundPartProject != 0)
                    objectConstractionInfo.BuildingSizeUnderGroundPartProject = constrStage.BuildingSizeUnderGroundPartProject;
                if (constrStage.TotalLivingSpaceProject != 0)
                    objectConstractionInfo.TotalLivingSpaceProject = constrStage.TotalLivingSpaceProject;
                //if (constrStage.FlatCount != 0)
                //    objectConstractionInfo.AppartmentsCountAndSpaceProject = constrStage.FlatCount.ToString() + "/";

            }
            DetailView dv = Application.CreateDetailView(os, objectConstractionInfo);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
            unitOfWork.CommitChanges();
        }

        private void UsePermAddInfoAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            UsePerm usePerm = e.CurrentObject as UsePerm;
            UsePerm i_UsePerm = os.GetObjectByKey<UsePerm>(usePerm.Oid);
            //i_ConstrPerm = (ConstrPerm)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_UsePerm.Session;

            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            ObjectConstractionInfo objectConstractionInfo;
            //IObjectSpace os = View.ObjectSpace;//Application.CreateObjectSpace(); // Create IObjectSpace or use the existing one, e.g. View.ObjectSpace

            if (i_UsePerm.ObjectConstractionInfo.Count > 0)
                objectConstractionInfo = i_UsePerm.ObjectConstractionInfo.FirstOrDefault();
            else
            {
                objectConstractionInfo = os.CreateObject<ObjectConstractionInfo>();
                objectConstractionInfo.UsePerm = i_UsePerm;
                if (i_UsePerm.CapitalStructureBase.Count > 0)
                    objectConstractionInfo.CapitalStructureBase = i_UsePerm.CapitalStructureBase.FirstOrDefault();
            }
            DetailView dv = Application.CreateDetailView(os, objectConstractionInfo);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
            unitOfWork.CommitChanges();
        }
    }
}
