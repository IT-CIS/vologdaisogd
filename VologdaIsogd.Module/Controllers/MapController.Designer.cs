namespace VologdaIsogd.Controllers
{
    partial class MapController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SetInGeoLinkAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ViewMapAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SetInGeoLinkAction
            // 
            this.SetInGeoLinkAction.Caption = "������� � ��� ��������";
            this.SetInGeoLinkAction.ConfirmationMessage = null;
            this.SetInGeoLinkAction.Id = "SetInGeoLinkAction";
            this.SetInGeoLinkAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.SetInGeoLinkAction.ToolTip = null;
            this.SetInGeoLinkAction.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.SetInGeoLinkAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SetLinkAction_Execute);
            // 
            // ViewMapAction
            // 
            this.ViewMapAction.Caption = "�������� �� �����";
            this.ViewMapAction.ConfirmationMessage = null;
            this.ViewMapAction.Id = "ViewMapAction";
            this.ViewMapAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ViewMapAction.ToolTip = null;
            this.ViewMapAction.TypeOfView = typeof(DevExpress.ExpressApp.View);
            this.ViewMapAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ViewMapAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SetInGeoLinkAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ViewMapAction;
    }
}
