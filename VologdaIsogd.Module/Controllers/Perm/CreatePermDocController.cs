﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Perm;
using AISOGD.Constr;
using AISOGD.Land;
using AISOGD.DocFlow;
using DevExpress.Persistent.BaseImpl;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreatePermDocController : ViewController
    {
        public CreatePermDocController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        // необходимо добавить различные проверки - 
        // на существование разрешения
        // на существование этапов
        public ConstrPerm CreateConstrPermDoc(Connect connect)
        {
            ConstrPerm constrPerm = null;
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();


            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return null;
            }

            string layer = "";
            // получаем название слоя выделения
            SelectionTable selectionTable = m_MapInfo.SelectionTable;// new SelectionTable(m_MapInfo);
            layer = selectionTable.Name;
            TableInfoTemp startSelectObjects = new TableInfoTemp(m_MapInfo, ETempTableType.Temp);
            m_MapInfo.Do(String.Format("Select * From Selection into {0}", startSelectObjects.Name));
            if (layer == "")
            {
                XtraMessageBox.Show("Не удалось получить название таблицы выделенных объектов. Пожалуйста, обратитесь к администратору Системы!", "Ошибка");
                return null;
            }
            if (layer != "")
            {
                List<string>  mapObjInfos = new List<string>();
                int selObjCount = selectionTable.GetCount().Value; // Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                int GISobjNo = 0;
                for (int j = 1; j < selObjCount + 1; j++)
                {
                    m_MapInfo.Fetch(EFetch.Rec, startSelectObjects.Name, j);
                    //m_MapInfo.Do("Fetch Rec " + j.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = selectionTable.GetFirstID();//m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // ищем, есть ли у выделенного объекта созданный реестровый. Если есть - сообщаем об этом - переделать, на проверку утвержденного РС
                            //if (spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            //{
                            //    XtraMessageBox.Show("У ГИС объекта MI_PRINX: " + gisObjId  
                            //        + " уже есть связанный реестровый объект этапа! Разрешение на строительство можно создать только у новых этапов!", "Ошибка");
                            //    return null;
                            //}
                            mapObjInfos.Add(gisObjId);
                        }
                        else
                        {
                            XtraMessageBox.Show("Имеются несохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                            return null;
                        }
                    }
                    catch { }
                    try
                    {
                        GISobjNo = Convert.ToInt16(m_MapInfo.Eval(String.Format("{0}.НОМЕР_В_БАЗЕ", startSelectObjects.Name)));
                    }
                    catch { }
                }
                // если номер не указан, то указываем его автоматически и записываем в семантику
                if (GISobjNo == 0)
                {
                    GISobjNo = DistributedIdGeneratorHelper.Generate(connect.GetSession().DataLayer, "ConstrPerm_GISObjNo", string.Empty);
                }
                //try
                //{
                //    objMapNumber = objMapNumber.Split('_')[0].Trim();
                //}
                //catch { }
                if (GISobjNo == 0)
                {
                    XtraMessageBox.Show("У выделенных объектов не удалось определить родительский объект строительства. Пожалайста, обратитесь к администратору Системы!", "Ошибка");
                    return null;
                }
                if (mapObjInfos.Count > 0)
                {
                    // по Номеру на схеме ищем объект строительства. Если не находим - создаем
                    CapitalStructureBase constrObject = connect.FindFirstObject<CapitalStructureBase>(x=> x.GISObjNo == GISobjNo);
                    if (constrObject == null)
                    {
                        constrObject = connect.CreateObject<CapitalStructureBase>();
                        constrObject.GISObjNo = GISobjNo;

                        constrObject.Save();
                    }
                    constrObject.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;
                    constrPerm = connect.CreateObject<ConstrPerm>();
                    constrPerm.DocDate = DateTime.Now.Date;
                    constrPerm.EDocStatus = AISOGD.Enums.eDocStatus.Подготовка;
                    constrPerm.DocKind = connect.FindFirstObject<dDocKind>(x=> x.Name == "Разрешение на строительство");
                    //try { constrPerm.ObjectAddress = constrObject.Address; }
                    //catch { }
                    //try { constrPerm.ObjectName = constrObject.FullName; }
                    //catch { }
                    //try { constrPerm = constrObject.Address; }
                    //catch { }
                    for (int i = 1; i < mapObjInfos.Count + 1; i++)
                    {
                        ConstrStage constrStage = connect.CreateObject<ConstrStage>();

                        constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;
                        m_MapInfo.Do("Fetch Rec " + i.ToString() + " From " + startSelectObjects.Name);
                        try
                        {
                            //XtraMessageBox.Show(m_MapInfo.Eval(startSelectObjects.Name +".НОМЕР_НА_СХЕМЕ"));
                            constrStage.StageNo = m_MapInfo.Eval(startSelectObjects.Name + ".ОЧЕРЕДЬ_СТР");
                        }
                        catch { }

                        // записываем в семантику номер ОКС
                        try
                        {
                            m_MapInfo.Do("Update Selection Set НОМЕР_В_БАЗЕ =  " + GISobjNo + " Commit table " + layer);
                        }
                        catch { }
                        //try { constrStage.Address = constrObject.Address; }
                        //catch { }
                        //try { constrStage.ConstrAddress = constrObject.ConstrAddress; }
                        //catch { }
                        try { constrStage.ConstructionType = constrObject.ConstructionType; }
                        catch { }
                        constrStage.Save();
                        connect.GetUnitOfWork().CommitChanges();
                        spatial.AddSpatialLink(constrStage.ClassInfo.FullName, constrStage.Oid.ToString(), layer, mapObjInfos[i - 1]);
                        // делаем пространственные запросы по ЗУ
                        try
                        {
                            //Сохранить выделенный объект в выборку
                            //TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
                            //m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));

                            m_MapInfo.Do("Select * From " + layer + " Where MI_PRINX = " + mapObjInfos[i-1] + " Into zObject");
                            // получаем слои для ЗУ
                            List<string> parcelLayersIds = spatial.GetListLinkedSpatialLayers("AISOGD.Land.Parcel");
                            foreach (string parcelLayer in parcelLayersIds)
                            {
                                // ищем пересечение с нашим объектом
                                m_MapInfo.Do("Select * from " + parcelLayer + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
                                int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                                for (int j = 1; j < selObjCount1 + 1; j++)
                                {
                                    m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable");
                                    // Id(MI_PRINX) пространственного объекта ЗУ
                                    string parcelReqestObjID = m_MapInfo.Eval("tmpTable.MI_PRINX");
                                    // находим ЗУ
                                    //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
                                    // этапа, если нет - добавляем
                                    // из таблицы связи получаем Ид реестрового объекта ЗУ
                                    SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                        mc.SpatialLayerId == parcelLayer &&
                                        mc.SpatialObjectId == parcelReqestObjID &&
                                        mc.IsogdClassName == "AISOGD.Land.Parcel");
                                    Parcel parcel = null;
                                    if (objLink != null)
                                    {
                                        // по ИД находим сам реестровый объект
                                        parcel = connect.FindFirstObject<Parcel>(mc => mc.Oid.ToString() == objLink.IsogdObjectId);
                                        
                                    }
                                    else
                                    {
                                        try { parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber == m_MapInfo.Eval("tmpTable.CADASTRAL_NUMBER")); }
                                        catch { }
                                        if (parcel == null)
                                        {
                                            parcel = connect.CreateObject<Parcel>();
                                            try { parcel.CadastralNumber = m_MapInfo.Eval("tmpTable.CADASTRAL_NUMBER"); }
                                            catch { }
                                            parcel.Save();
                                        }
                                        spatial.AddSpatialLink(parcel.ClassInfo.FullName, parcel.Oid.ToString(), parcelLayer, parcelReqestObjID);
                                    }
                                    if (parcel != null)
                                    {
                                        bool isParcelExist = false;

                                        if (parcel.Area == 0)
                                        {
                                            try {
                                                string area = m_MapInfo.Eval("tmpTable.AREA");
                                                if (area.Contains("+"))
                                                    area = area.Split('+')[0];
                                                area = area.Replace(" ","");
                                                parcel.Area = Convert.ToDouble(area);
                                            }
                                            catch { }
                                            try
                                                {
                                                    parcel.Unit = connect.FindFirstObject<dUnit>(x => x.Name == "кв.м");
                                                }
                                                catch { }
                                            parcel.Save();
                                        }
                                        
                                        foreach (Parcel p in constrStage.Parcels)
                                        {
                                            if (parcel == p)
                                                isParcelExist = true;
                                        }
                                        if (!isParcelExist)
                                        {
                                            constrStage.Parcels.Add(parcel);
                                            constrStage.Save();
                                        }
                                    }
                                }
                            }
                        }
                        catch { }
                        // делаем пространственные запросы по градпланам
                        try
                        {
                            constrPerm.GPZU = "";
                            m_MapInfo.Do("Select * From " + layer + " Where MI_PRINX = " + mapObjInfos[i - 1] + " Into zObject");
                            // получаем слои для ЗУ
                            List<string> gradPlanLayersIds = spatial.GetListLinkedSpatialLayers("AISOGD.GPZU.GradPlan");
                            // Должно вставляться
                            // №RU35327000_____ утвержден распоряжением начальника Департамента градостроительства Администрации города Вологды от_______ № ____" 
                            foreach (string gradPlanLayer in gradPlanLayersIds)
                            {
                                // ищем пересечение с нашим объектом
                                m_MapInfo.Do("Select * from " + gradPlanLayer + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable1");
                                int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                                for (int j = 1; j < selObjCount1 + 1; j++)
                                {
                                    m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable1");
                                    // номер ГПЗУ
                                    string gradPlanNo = "№RU35327000";
                                    try
                                    {
                                        gradPlanNo += m_MapInfo.Eval("tmpTable1.Номер_ГП");
                                    }
                                    catch { }
                                    // реквизиты распоряжения
                                    string gradPlanSubmit = "от_______ № ____";
                                    try
                                    {
                                        gradPlanSubmit = m_MapInfo.Eval("tmpTable1.Документы_ГП");
                                        gradPlanSubmit = gradPlanSubmit.Replace("Вологды", "*").Split('*')[1].Trim();//gradPlanSubmit.Substring();
                                    }
                                    catch { }
                                    string gpzu = gradPlanNo + " утвержден распоряжением начальника Департамента градостроительства Администрации города Вологды " + gradPlanSubmit;
                                    if (!constrPerm.GPZU.Contains(gpzu))
                                    {
                                        if(constrPerm.GPZU == "")
                                            constrPerm.GPZU = gpzu;
                                        else
                                            constrPerm.GPZU +=", " + gpzu;
                                    }
                                        
                                }
                            }
                            if (constrPerm.GPZU == "")
                                constrPerm.GPZU = "№RU35327000_____ утвержден распоряжением начальника Департамента градостроительства Администрации города Вологды от_______ № ____";
                        }
                        catch { }
                        // делаем пространственные запросы по ППТ
                        try
                        {
                            constrPerm.PlanningDocInfo = "";
                        m_MapInfo.Do("Select * From " + layer + " Where MI_PRINX = " + mapObjInfos[i - 1] + " Into zObject");
                            // получаем слои для ЗУ
                            List<string> gradPPTLayersIds = spatial.GetLayersIdsFromLayerGroupName("ППТ для стройки");
                            foreach (string gradPlanLayer in gradPPTLayersIds)
                            {
                                // ищем пересечение с нашим объектом
                                m_MapInfo.Do("Select * from " + gradPlanLayer + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable2");
                                int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                                for (int j = 1; j < selObjCount1 + 1; j++)
                                {
                                    m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable2");
                                    // cтатус документа ППТ
                                    string pptStatus = "";
                                    try {
                                        pptStatus = m_MapInfo.Eval("tmpTable2.Статус");
                                        pptStatus = pptStatus.Trim();
                                    }
                                    catch { }
                                    if(pptStatus.ToLower() == "утвержден" || pptStatus.ToLower() == "в работе – изменения")
                                    {
                                        // вид документа
                                        string pptType = "";
                                        try { pptType = m_MapInfo.Eval("tmpTable2.Вид_док_утв").Trim(); }
                                        catch { }

                                        // наименование
                                        string pptName = "";
                                        try { pptName = m_MapInfo.Eval("tmpTable2.Наименование_док_утв").Trim(); }
                                        catch { }
                                        // реквизиты
                                        string pptDocs = "";
                                        try { pptDocs = m_MapInfo.Eval("tmpTable2.Реквизиты_док_утв").Trim(); }
                                        catch { }
                                        string ppt = $"{pptType} {pptName} {pptDocs}".Trim().Replace("  "," ");

                                        try {
                                            if (!String.IsNullOrEmpty(m_MapInfo.Eval("tmpTable2.Реквизиты_док_изм")))
                                                ppt += " (с последующими изменениями)";
                                        }
                                        catch { }
                                        
                                        if (!constrPerm.PlanningDocInfo.Contains(ppt.Trim()))
                                            constrPerm.PlanningDocInfo = ppt.Trim();

                                        constrPerm.Save();
                                    }
                                }
                            }
                        }
                        catch { }
                        constrPerm.ConstrStages.Add(constrStage);
                        constrObject.ConstrStages.Add(constrStage);
                        constrPerm.CapitalStructureBase.Add(constrObject);
                        constrPerm.Save();
                        constrStage.Save();
                        connect.GetUnitOfWork().CommitChanges();
                    }
                    if (constrPerm.ConstrStages.Count > 0)
                    {
                        try
                        {
                            constrPerm.WorkKind = constrPerm.ConstrStages[0].WorkKind;
                        }
                        catch { }
                        foreach (ConstrStage constrStage in constrPerm.ConstrStages)
                        {
                            //try
                            //{
                            //    constrPerm.ObjectName += constrStage.Name + Environment.NewLine;
                            //}
                            //catch { }
                            try
                            {
                                foreach (Parcel p in constrStage.Parcels)
                                {
                                    constrPerm.Parcels.Add(p);
                                    constrObject.Parcels.Add(p);
                                    //if (p.CadastralNumber != null && p.CadastralNumber != "")
                                    //{ constrPerm.LotCadNo += p.CadastralNumber + "; "; }
                                }
                            }
                            catch { }
                        }
                        try
                        {
                            constrPerm.LotCadNo.Trim().TrimEnd(';');
                        }
                        catch { }
                    }
                    constrPerm.Save();
                    connect.GetUnitOfWork().CommitChanges();

                }
            }
            return constrPerm;
        }

        

    }
}
