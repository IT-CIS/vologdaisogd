﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class PermNoticeIZDTemplateController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PermNoticeIZDTemplateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PermNoticeIZDRefuseTemplateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PermNoticeIZDTemplateAction
            // 
            this.PermNoticeIZDTemplateAction.Caption = "Соответствие";
            this.PermNoticeIZDTemplateAction.ConfirmationMessage = null;
            this.PermNoticeIZDTemplateAction.Id = "PermNoticeIZDTemplateAction";
            this.PermNoticeIZDTemplateAction.ToolTip = null;
            this.PermNoticeIZDTemplateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PermNoticeIZDTemplateAction_Execute);
            // 
            // PermNoticeIZDRefuseTemplateAction
            // 
            this.PermNoticeIZDRefuseTemplateAction.Caption = "Несоответствие";
            this.PermNoticeIZDRefuseTemplateAction.ConfirmationMessage = null;
            this.PermNoticeIZDRefuseTemplateAction.Id = "PermNoticeIZDRefuseTemplateAction";
            this.PermNoticeIZDRefuseTemplateAction.ToolTip = null;
            this.PermNoticeIZDRefuseTemplateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PermNoticeIZDRefuseTemplateAction_Execute);
            // 
            // PermNoticeIZDTemplateController
            // 
            this.Actions.Add(this.PermNoticeIZDTemplateAction);
            this.Actions.Add(this.PermNoticeIZDRefuseTemplateAction);
            this.TargetObjectType = typeof(AISOGD.Perm.PermNoticeIZD);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PermNoticeIZDTemplateAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PermNoticeIZDRefuseTemplateAction;
    }
}
