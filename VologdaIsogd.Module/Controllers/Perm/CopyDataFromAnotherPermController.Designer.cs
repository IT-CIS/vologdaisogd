﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class CopyDataFromAnotherPermController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyDataFromAnotherPermController));
            this.CopyDataFromAnotherPermAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CopyDataFromAnotherPermAction
            // 
            this.CopyDataFromAnotherPermAction.Caption = "Скопировать данные";
            this.CopyDataFromAnotherPermAction.ConfirmationMessage = null;
            this.CopyDataFromAnotherPermAction.Id = "CopyDataFromAnotherPermAction";
            this.CopyDataFromAnotherPermAction.ToolTip = resources.GetString("CopyDataFromAnotherPermAction.ToolTip");
            this.CopyDataFromAnotherPermAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CopyDataFromAnotherPermAction_Execute);
            // 
            // CopyDataFromAnotherPermController
            // 
            this.Actions.Add(this.CopyDataFromAnotherPermAction);
            this.TargetObjectType = typeof(AISOGD.Perm.ConstrPerm);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CopyDataFromAnotherPermAction;
    }
}
