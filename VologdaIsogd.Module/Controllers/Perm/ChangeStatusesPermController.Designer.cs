﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class ChangeStatusesPermController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ChangeStatusesPermChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ChangeStatusesPermChoiceAction
            // 
            this.ChangeStatusesPermChoiceAction.Caption = "Изменить статус";
            this.ChangeStatusesPermChoiceAction.ConfirmationMessage = null;
            this.ChangeStatusesPermChoiceAction.Id = "ChangeStatusesPermChoiceAction";
            this.ChangeStatusesPermChoiceAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ChangeStatusesPermChoiceAction.ToolTip = "Изменить статус документа:\r\n -утвердить\r\n -продлить\r\n -подготовить взамен\r\n -отка" +
    "з\r\n -прекращение делопроизводства";
            this.ChangeStatusesPermChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ChangeStatusesPermChoiceAction_Execute);
            // 
            // ChangeStatusesPermController
            // 
            this.Actions.Add(this.ChangeStatusesPermChoiceAction);
            this.TargetObjectType = typeof(AISOGD.Perm.ConstrPerm);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ChangeStatusesPermChoiceAction;
    }
}
