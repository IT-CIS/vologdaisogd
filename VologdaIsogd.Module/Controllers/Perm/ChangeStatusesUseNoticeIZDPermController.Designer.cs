﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class ChangeStatusesUseNoticeIZDPermController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ChangeStatusesUseNoticeIZDPermChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ChangeStatusesUseNoticeIZDPermChoiceAction
            // 
            this.ChangeStatusesUseNoticeIZDPermChoiceAction.Caption = "Изменить статус";
            this.ChangeStatusesUseNoticeIZDPermChoiceAction.ConfirmationMessage = null;
            this.ChangeStatusesUseNoticeIZDPermChoiceAction.Id = "ChangeStatusesUseNoticeIZDPermChoiceAction";
            this.ChangeStatusesUseNoticeIZDPermChoiceAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ChangeStatusesUseNoticeIZDPermChoiceAction.ToolTip = "Утверждение документа или отказ по нему";
            this.ChangeStatusesUseNoticeIZDPermChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ChangeStatusesUseNoticeIZDPermChoiceAction_Execute);
            // 
            // ChangeStatusesUseNoticeIZDPermController
            // 
            this.Actions.Add(this.ChangeStatusesUseNoticeIZDPermChoiceAction);
            this.TargetObjectType = typeof(AISOGD.Perm.UsePermNoticeIZD);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ChangeStatusesUseNoticeIZDPermChoiceAction;
    }
}
