﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AISOGD.Constr;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraEditors;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class UsePermNoticeIZDCreateController : ViewController
    {
        public UsePermNoticeIZDCreateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        public UsePermNoticeIZD CreateUsePermNoticeIZD(Connect connect)
        {
            UsePermNoticeIZD usePerm = null;
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();


            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return null;
            }

            string layer = "";
            // получаем название слоя выделения
            SelectionTable selectionTable = m_MapInfo.SelectionTable;// new SelectionTable(m_MapInfo);
            layer = selectionTable.Name;
            TableInfoTemp startSelectObjects = new TableInfoTemp(m_MapInfo, ETempTableType.Temp);
            m_MapInfo.Do(String.Format("Select * From Selection into {0}", startSelectObjects.Name));
            if (layer == "")
            {
                XtraMessageBox.Show("Не удалось получить название таблицы выделенных объектов. Пожалуйста, обратитесь к администратору Системы!", "Ошибка");
                return null;
            }
            if (layer != "")
            {
                string objMapNumber = "";
                CapitalStructureBase constrObj = null;
                int selObjCount = selectionTable.GetCount().Value; // Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                if (selObjCount > 1)
                {
                    XtraMessageBox.Show("Выделено более одного объекта. Уведомление можно создать только по одному объекту ИЖД!", "Ошибка");
                    return null;
                }
                m_MapInfo.Fetch(EFetch.Rec, startSelectObjects.Name, 1);
                // Id (MI_PRINX) выделенного объекта мапинфо
                try
                {
                    string gisObjId = selectionTable.GetFirstID();//m_MapInfo.Eval("Selection.MI_PRINX");
                    if (gisObjId != "0")
                    {
                        // ищем, есть ли у выделенного объекта созданный реестровый. Если нет - сообщаем об этом
                        if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                        {
                            XtraMessageBox.Show("У ГИС объекта MI_PRINX: " + gisObjId
                                + " нет связанного реестрового объекта строительства ИЖД! Уведомление на ввод можно создать только у зарегистрированного обхекта строительства!", "Ошибка");
                            return null;
                        }
                        // находим связанный реестровый объект объекта строительства
                        foreach (SpatialRepository sprepo in spatial.GetIsogdLinks(layer, gisObjId))
                        {
                            constrObj = connect.FindFirstObject<CapitalStructureBase>(x => x.Oid.ToString() == sprepo.IsogdObjectId);
                            if (constrObj == null)
                            {
                                XtraMessageBox.Show("У ГИС объекта MI_PRINX: " + gisObjId
                                + " нет связанного реестрового объекта строительства! Разрешение на ввод можно создать только у зарегистрированных объектов строительства!", "Ошибка");
                                return null;
                            }

                            // проверяем, введен ли уже объект строительства
                            if (constrObj.ConstrStageState == AISOGD.Enums.eConstrStageState.Введено)
                            {
                                XtraMessageBox.Show("Объект: " + constrObj.Name
                                + " уже введен! Проверьте правильность выбранного для ввода объекта строительства", "Ошибка");
                                return null;
                            }
                        }

                        // если все нормально - создаем уведомление на ввод и связываем со всеми 
                        // 1. объектом строительства
                        // 2. уведомлениями на строительство

                        usePerm = connect.CreateObject<UsePermNoticeIZD>();
                        usePerm.EDocStatus = AISOGD.Enums.eDocStatus.Подготовка;

                        if (!usePerm.CapitalStructureBase.Contains(constrObj))
                            usePerm.CapitalStructureBase.Add(constrObj);

                        try
                        {
                            if (!usePerm.ConstrStages.Contains(constrObj.ConstrStages[0]))
                                usePerm.ConstrStages.Add(constrObj.ConstrStages[0]);
                        }
                        catch { }
                        foreach (PermNoticeIZD constrPerm in constrObj.generalDocBase)
                        {
                            if (constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.Утвержден)
                            {
                                usePerm.PermNoticeIZDs.Add(constrPerm);
                                usePerm.MapNo = constrPerm.MapNo;
                            }
                        }
                        try { usePerm.MapNo = constrObj.MapNo; }
                        catch { }
                        
                        


                    }
                    else
                    {
                        XtraMessageBox.Show("Имеются несохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                        return null;
                    }
                }
                catch { }
            }


            return usePerm;
        }
    }
}
