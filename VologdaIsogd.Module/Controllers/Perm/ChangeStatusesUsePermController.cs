﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Perm;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Constr;
using AISOGD.DocFlow;
using VologdaIsogd.Module.Controllers.Delo;
using AISOGD.General;
using DevExpress.Persistent.BaseImpl;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ChangeStatusesUsePermController : ViewController
    {

        private ChoiceActionItem permAcceptAction;
        private ChoiceActionItem permRefuseAction;

        public ChangeStatusesUsePermController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            ChangeStatusesUsePermChoiceAction.Items.Clear();
            permAcceptAction =
               new ChoiceActionItem("Утверждено", null);
            ChangeStatusesUsePermChoiceAction.Items.Add(permAcceptAction);

            permRefuseAction =
               new ChoiceActionItem("Отказ", null);
            ChangeStatusesUsePermChoiceAction.Items.Add(permRefuseAction);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        /// <summary>
        /// Изменить статус документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeStatusesUsePermChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            UsePerm perm;
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            perm = os.FindObject<UsePerm>(new BinaryOperator("Oid", x_id));

            if (perm != null)
            {
                if (e.SelectedChoiceActionItem == permAcceptAction)
                {
                    UsePermAccept(os, connect, perm);
                }
                
                if (e.SelectedChoiceActionItem == permRefuseAction)
                {
                    UsePermRefuse(os, connect, perm);
                }
                
            }
        }

       
        /// <summary>
        /// Утверждение разрешения на ввод
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsePermAccept(IObjectSpace os, Connect connect, UsePerm usePerm)
        {

            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            SynhronizePermDataController SynhronizePermDataController = new SynhronizePermDataController();
            if (usePerm.EDocStatus == AISOGD.Enums.eDocStatus.Утвержден)
            {
                XtraMessageBox.Show("Данный документ уже утвержден!");
                return;
            }
            if (usePerm.EDocStatus == AISOGD.Enums.eDocStatus.Отказ)
            {
                XtraMessageBox.Show("Документ со статусом 'Отказ' нельзя утвердить!");
                return;
            }
            if (usePerm.DocDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Не указана дата документа");
                return;
            }
            if (usePerm.DocNo == null || usePerm.DocNo == "")
            {
                XtraMessageBox.Show("Необходимо указать корректный номер документа");
                return;
            }
            UsePerm anotherPerm = connect.FindFirstObject<UsePerm>(x => x.DocNo == usePerm.DocNo && x.DocDate == usePerm.DocDate);
            if (usePerm.Oid.ToString() != anotherPerm.Oid.ToString())
            {
                XtraMessageBox.Show("В Системе уже внесен другой документ с подобными реквизитам.");
                return;
            }
            //if (usePerm.DocNo.Contains("35-32357000-         -"))
            //{
            //    XtraMessageBox.Show("Необходимо указать корректный номер документа");
            //    return;
            //}
            usePerm.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден;
            try
            {
                usePerm.FullDocNo = usePerm.DocKind.Prefix + usePerm.DocNo + usePerm.DocKind.Suffix + usePerm.DocDate.Year.ToString();
            }
            catch { }

            string temp = "";
            
            foreach (ConstrStage stage in usePerm.ConstrStages)
            {
                stage.ConstrStageState = AISOGD.Enums.eConstrStageState.Введено;
                try
                {
                    if (stage.UsePermInfo == null || stage.UsePermInfo == "")
                        stage.UsePermInfo = String.Format("от {0} №{1}{2}", usePerm.DocDate.ToShortDateString(), usePerm.DocNo, temp);
                    else
                        stage.UsePermInfo += ", " + String.Format("от {0} №{1}{2}", usePerm.DocDate.ToShortDateString(), usePerm.DocNo, temp);
                }
                catch { }
                stage.Save();
                // заполняем семантику
                // из таблицы связи получаем Ид реестрового объекта ЗУ
                SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", stage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                if (objLink != null)
                {
                    try
                    {
                        m_MapInfo.Do("Update Selection Set СТАТУС =  \"" + stage.ConstrStageState + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                    try
                    {
                        m_MapInfo.Do("Update Selection Set РАЗРЕШ_ВВОД =  \"" + stage.UsePermInfo + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                    try
                    {
                        m_MapInfo.Do("Update Selection Set ГОД_ВВОДА =  " + usePerm.DocDate.Year + " Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                }
            }
            
            foreach (CapitalStructureBase constrObj in usePerm.CapitalStructureBase)
            {
                try
                {
                    if (constrObj.UsePermInfo == null || constrObj.ConstrPermInfo == "")
                        constrObj.UsePermInfo = String.Format("от {0} №{1}{2}", usePerm.DocDate.ToShortDateString(), usePerm.DocNo, temp);
                    else
                        constrObj.UsePermInfo += ", " + String.Format("от {0} №{1}{2}", usePerm.DocDate.ToShortDateString(), usePerm.DocNo, temp);
                }
                catch { }
                // заполняем данные по характеристикам объекта если была стройка
                if(usePerm.WorkKind == AISOGD.Enums.eWorkKind.Строительство)
                    SetConstrObjFaktInfo(constrObj, usePerm, connect);
                //try
                //{
                //    constrObj.TotalBuildSquareFakt += usePerm.TotalBuildSquareFakt;
                //}
                //catch { }
                //try
                //{
                //    constrObj.TotalLivingSpaceFakt += usePerm.TotalLivingSpaceFakt;
                //}
                //catch { }
                //try
                //{
                //    constrObj.LivingSpaceFakt += usePerm.LivingSpaceFakt;
                //}
                //catch { }

                // вычисляем - полностью ли введен объект строительства
                bool isTotalInUse = true;
                foreach (ConstrStage stage in constrObj.ConstrStages)
                {
                    if(stage.ConstrStageState != AISOGD.Enums.eConstrStageState.Введено)
                    {
                        isTotalInUse = false;
                        break;
                    }
                }
                if (isTotalInUse)
                {
                    constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.Введено;
                    constrObj.DocInUseDate = usePerm.DocDate;
                    try { constrObj.ConstrCountDays = ((usePerm.DocDate - constrObj.StartConstrDate).Days + 1).ToString(); }
                    catch { }
                }
                constrObj.Save();
            }
            foreach (InLetter letter in usePerm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            
            usePerm.Save();
            os.CommitChanges();

            // создаем документ для регистрации в ИСОГД 
            try
            {
                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = usePerm.FullDocNo; }
                catch { }

                try { idocDate = usePerm.DocDate; }
                catch { }

                try { icadNo = usePerm.LotCadNo; }
                catch { }

                try { iaddrInfo = usePerm.AddressObjectName; }
                catch { }

                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in usePerm.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }

                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "061100", "Разрешение на ввод  ОКС", icadNo, iaddrInfo, attachs);
            }
            catch { }

        }


        
        /// <summary>
        /// Отказ по разрешению
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsePermRefuse(IObjectSpace os, Connect connect, UsePerm usePerm)
        {
            if (usePerm.EDocStatus != AISOGD.Enums.eDocStatus.Подготовка)
            {
                XtraMessageBox.Show("Можно отменить только документ со статусом 'Подготовка'!");
                return;
            }

            usePerm.EDocStatus = AISOGD.Enums.eDocStatus.Отказ;
            
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            foreach (ConstrStage stage in usePerm.ConstrStages)
            {
                stage.ConstrStageState = AISOGD.Enums.eConstrStageState.ОтказВвода;
                stage.Save();
                try
                {
                    SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", stage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                    m_MapInfo.Do("Select * From " + objLink.SpatialLayerId + " Where MI_PRINX = " + objLink.SpatialObjectId);
                    string mapNo = "";
                    if (stage.MapNo != null)
                        mapNo = stage.MapNo;
                    //m_MapInfo.Do("Update Selection Set НОМЕР_НА_СХЕМЕ =  \"" + mapNo + "_отказ ввода" + "\" Commit table " + objLink.SpatialLayerId);
                    try
                    {
                        m_MapInfo.Do("Update Selection Set СТАТУС =  \"" + stage.ConstrStageState + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                }
                catch { }
            }
            foreach (InLetter letter in usePerm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            usePerm.Save();
            os.CommitChanges();
        }

        /// <summary>
        /// Заполнение проектных показателей у ОКС по этапам при утверждении РС
        /// </summary>
        /// <param name="constrObj"></param>
        /// <param name="ConstrStageList"></param>
        /// <param name="connect"></param>
        public void SetConstrObjFaktInfo(CapitalStructureBase constrObj, UsePerm usePerm, Connect connect)
        {
            char[] seps = { '/', '\\' };

            #region Общие показатели
            try { constrObj.BuildingSizeFakt += usePerm.BuildingSizeFakt; }
            catch { }
            try { constrObj.BuildingSizeOverGroundPartFakt += usePerm.BuildingSizeOverGroundPartFakt; }
            catch { }
            try { constrObj.BuildingSizeUnderGroundPartFakt += usePerm.BuildingSizeUnderGroundPartFakt; }
            catch { }
            try { constrObj.TotalBuildSquareFakt += usePerm.TotalBuildSquareFakt; }
            catch { }
            try { constrObj.BuildSquareFakt += usePerm.BuildSquareFakt; }
            catch { }
            try { constrObj.HeightFakt += usePerm.HeightFakt; }
            catch { }
            try { constrObj.NotLivingSquareFakt += usePerm.NotLivingSquareFakt; }
            catch { }
            try { constrObj.OutBuildingSquareFakt += usePerm.OutBuildingSquareFakt; }
            catch { }
            try { constrObj.BuildingCountFakt += usePerm.BuildingCountFakt; }
            catch { }
            #endregion

            #region Этажность 
            try { constrObj.FloorCountFakt += usePerm.FloorCountFakt; }
            catch { }
            try { constrObj.UnderGroundFloorCountFakt += usePerm.UnderGroundFloorCountFakt; }
            catch { }
            try { constrObj.FloorCountAboveFakt += usePerm.FloorCountAboveFakt; }
            catch { }
            try { constrObj.FloorCountAboveFakt += usePerm.FloorCountAboveFakt; }
            catch { }
            try
            {
                constrObj.FloorCountDiffFakt += usePerm.FloorCountDiffFakt + Environment.NewLine;
                constrObj.FloorCountDiffFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            try
            {
                constrObj.FloorsNoteFakt += usePerm.FloorsNoteFakt + Environment.NewLine;
                constrObj.FloorsNoteFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            #endregion

            #region Лифты, Эскалаторы, подъемники для ОКС
            try { constrObj.ElevatorsCountFakt += usePerm.ElevatorsCountFakt; }
            catch { }
            try { constrObj.EscalatorsCountFakt += usePerm.EscalatorsCountFakt; }
            catch { }
            try { constrObj.InvalidLiftsCountFakt += usePerm.InvalidLiftsCountFakt; }
            catch { }
            #endregion

            #region Материалы
            //try
            //{
            //    if (constrObj.FundMaterialFakt == null)
            //        constrObj.FundMaterialFakt = connect.FindFirstObject<Material>(x => x.Oid == usePerm.FundMaterialFakt.Oid);
            //}
            //catch { }
            //try
            //{
            //    if (constrObj.WallMaterialFakt == null)
            //        constrObj.WallMaterialFakt = connect.FindFirstObject<Material>(x => x.Oid == usePerm.WallMaterialFakt.Oid);
            //}
            //catch { }
            //try
            //{
            //    if (constrObj.BorderMaterialFakt == null)
            //        constrObj.BorderMaterialFakt = connect.FindFirstObject<Material>(x => x.Oid == usePerm.BorderMaterialFakt.Oid);
            //}
            //catch { }
            //try
            //{
            //    if (constrObj.RoofMaterialFakt == null)
            //        constrObj.RoofMaterialFakt = connect.FindFirstObject<Material>(x => x.Oid == usePerm.RoofMaterialFakt.Oid);
            //}
            //catch { }


            #endregion

            #region Сети и системы инженерно технического обслуживания
            try
            {
                if (usePerm.ElectroFakt == true)
                    constrObj.ElectroFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.HeatFakt == true)
                    constrObj.HeatFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.WaterFakt == true)
                    constrObj.WaterFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.GazFakt == true)
                    constrObj.GazFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.HouseSeverageFakt == true)
                    constrObj.HouseSeverageFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.PhonesFakt == true)
                    constrObj.PhonesFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.TVFakt == true)
                    constrObj.TVFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.RadioFakt == true)
                    constrObj.RadioFakt = true;
            }
            catch { }
            try
            {
                if (usePerm.SeverageFakt == true)
                    constrObj.SeverageFakt = true;
            }
            catch { }
            #endregion

            #region Иные показатели
            try
            {
                constrObj.OtherIndicatorsFakt += usePerm.OtherIndicatorsFakt + Environment.NewLine;
                constrObj.OtherIndicatorsFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            #endregion

            #region нежилые
            try { constrObj.PlacesCountFakt += usePerm.PlacesCountFakt; }
            catch { }
            try { constrObj.RoomCountFakt += usePerm.RoomCountFakt; }
            catch { }
            try { constrObj.CapacityFakt += usePerm.CapacityFakt; }
            catch { }
            #endregion

            #region жилые
            try { constrObj.LivingSpaceFakt += usePerm.LivingSpaceFakt; }
            catch { }
            try { constrObj.TotalLivingSpaceFakt += usePerm.TotalLivingSpaceFakt; }
            catch { }
            try { constrObj.NotLivingSpaceFakt += usePerm.NotLivingSpaceFakt; }
            catch { }
            try { constrObj.SectionCountFakt += usePerm.SectionCountFakt; }
            catch { }
            try { constrObj.PorchCountFakt += usePerm.PorchCountFakt; }
            catch { }
            try { constrObj.AppartmentsCountFakt += usePerm.AppartmentsCountFakt; }
            catch { }

            try { constrObj.StudioCountFakt += usePerm.StudioCountFakt; }
            catch { }
            try { constrObj.StudioSpaceFakt += usePerm.StudioSpaceFakt; }
            catch { }
            try { constrObj.OneRoomFlatCountFakt += usePerm.OneRoomFlatCountFakt; }
            catch { }
            try { constrObj.OneRoomFlatSpaceFakt += usePerm.OneRoomFlatSpaceFakt; }
            catch { }
            try { constrObj.TwoRoomFlatCountFakt += usePerm.TwoRoomFlatCountFakt; }
            catch { }
            try { constrObj.TwoRoomFlatSpaceFakt += usePerm.TwoRoomFlatSpaceFakt; }
            catch { }
            try { constrObj.ThreeRoomFlatCountFakt += usePerm.ThreeRoomFlatCountFakt; }
            catch { }
            try { constrObj.ThreeRoomFlatSpaceFakt += usePerm.ThreeRoomFlatSpaceFakt; }
            catch { }
            try { constrObj.FourRoomFlatCountFakt += usePerm.FourRoomFlatCountFakt; }
            catch { }
            try { constrObj.FourRoomSpaceFakt += usePerm.FourRoomSpaceFakt; }
            catch { }
            try { constrObj.MoreRoomFlatCountFakt += usePerm.MoreRoomFlatCountFakt; }
            catch { }
            try { constrObj.MoreRoomSpaceFakt += usePerm.MoreRoomSpaceFakt; }
            catch { }
            try { constrObj.ElitAppartmentsCountFakt += usePerm.ElitAppartmentsCountFakt; }
            catch { }
            try { constrObj.ElitAppartmentsSpaceFakt += usePerm.ElitAppartmentsSpaceFakt; }
            catch { }
            #endregion

            #region Объекты производственного назначения
            try
            {
                if (constrObj.ProductPurposeObjectType == null)
                    constrObj.ProductPurposeObjectType = connect.FindFirstObject<ProductPurposeObjectType>(x => x.Oid == usePerm.ProductPurposeObjectType.Oid);
            }
            catch { }
            try
            {
                constrObj.PowerFakt = "";
                constrObj.PowerFakt += usePerm.PowerFakt + Environment.NewLine;
                constrObj.PowerFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            try
            {
                constrObj.ProductivityFakt = "";
                constrObj.ProductivityFakt += usePerm.ProductivityFakt + Environment.NewLine;
                constrObj.ProductivityFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            #endregion

            #region  Линейные объекты
            try
            {
                if (constrObj.LineObjectClass == null)
                    constrObj.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid == usePerm.LineObjectClass.Oid);
            }
            catch { }
            try { constrObj.LengthFakt += usePerm.LengthFakt; }
            catch { }
            try
            {
                constrObj.PowerLineFakt = "";
                constrObj.PowerLineFakt += usePerm.PowerLineFakt + Environment.NewLine;
                constrObj.PowerLineFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            try
            {
                constrObj.PipesInfoFakt = "";
                constrObj.PipesInfoFakt += usePerm.PipesInfoFakt + Environment.NewLine;
                constrObj.PipesInfoFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            try
            {
                constrObj.ElectricLinesInfoFakt = "";
                constrObj.ElectricLinesInfoFakt += usePerm.ElectricLinesInfoFakt + Environment.NewLine;
                constrObj.ElectricLinesInfoFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            try
            {
                constrObj.ConstructiveElementsInfoFakt = "";
                constrObj.ConstructiveElementsInfoFakt += usePerm.ConstructiveElementsInfoFakt + Environment.NewLine;
                constrObj.ConstructiveElementsInfoFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            #endregion

            #region Соответствие требованиям энергетической эффективности
            try
            {
                if (constrObj.EnergyEfficiencyClassFakt == null)
                    constrObj.EnergyEfficiencyClassFakt = connect.FindFirstObject<EnergyEfficiencyClass>(x => x.Oid == usePerm.EnergyEfficiencyClassFakt.Oid);
            }
            catch { }

            try { constrObj.HeatConsumptionFakt += usePerm.HeatConsumptionFakt; }
            catch { }
            try
            {
                constrObj.OutdoorIsolationMaterialFakt = "";
                constrObj.OutdoorIsolationMaterialFakt += usePerm.OutdoorIsolationMaterialFakt + Environment.NewLine;
                constrObj.OutdoorIsolationMaterialFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            try
            {
                constrObj.SkylightsFillingFakt = "";
                constrObj.SkylightsFillingFakt += usePerm.SkylightsFillingFakt + Environment.NewLine;
                constrObj.SkylightsFillingFakt.TrimEnd(Environment.NewLine.ToCharArray());
            }
            catch { }
            #endregion
        }

        
    }
}
