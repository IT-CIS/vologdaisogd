﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class ChangeStatusesUsePermController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ChangeStatusesUsePermChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ChangeStatusesUsePermChoiceAction
            // 
            this.ChangeStatusesUsePermChoiceAction.Caption = "Изменить статус";
            this.ChangeStatusesUsePermChoiceAction.ConfirmationMessage = null;
            this.ChangeStatusesUsePermChoiceAction.Id = "ChangeStatusesUsePermChoiceAction";
            this.ChangeStatusesUsePermChoiceAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ChangeStatusesUsePermChoiceAction.ToolTip = "Изменить статус документа:\r\n -утвердить\r\n -отказ";
            this.ChangeStatusesUsePermChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ChangeStatusesUsePermChoiceAction_Execute);
            // 
            // ChangeStatusesUsePermController
            // 
            this.Actions.Add(this.ChangeStatusesUsePermChoiceAction);
            this.TargetObjectType = typeof(AISOGD.Perm.UsePerm);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ChangeStatusesUsePermChoiceAction;
    }
}
