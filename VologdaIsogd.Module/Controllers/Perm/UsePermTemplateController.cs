using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using System.Reflection;
using AISOGD.Perm;
using System.IO;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using AISOGD.Subject;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class UsePermTemplateController : ViewController
    {
        public UsePermTemplateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void UsePermTemplateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ������ � ��� ������

            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            UsePerm perm = os.GetObjectByKey<UsePerm>(x_id);
            UnitOfWork unitOfWork = (UnitOfWork)perm.Session;

            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePerm.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);
            //����������

            //����������
            // ������� �������� �������� ��� ������������ � ��������� ������, ����� ����� �� ����� ��� ������� � ������
            string developer = "";
            foreach (GeneralSubject docSubject in perm.DocSubjects)
            {
                try
                {
                    if (docSubject.NameDat != null && docSubject.NameDat != "")
                        developer += docSubject.NameDat + ", ";
                    else
                        developer += docSubject.FullName + ", ";
                }
                catch { }
            }
            developer = developer.Trim();
            developer = developer.TrimEnd(',');


            try
            {
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = developer.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                //do
                //{
                //    text1 += words[i] + " ";
                //    i++;
                //}
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterBook1 = richServer.Document.Bookmarks["RequesterBook1"];
                richServer.Document.Replace(RequesterBook1.Range, text1.TrimEnd());
                Bookmark RequesterBook2 = richServer.Document.Bookmarks["RequesterBook2"];
                richServer.Document.Replace(RequesterBook2.Range, text2.TrimEnd());
            }
            catch { }

            // ����� �����������
            try
            {
                string RequesterAddr = "";
                try { RequesterAddr = perm.ConstrDeveloperContactInfo; }
                catch { }
                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = RequesterAddr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 25)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterAddrBook1 = richServer.Document.Bookmarks["RequesterAddrBook1"];
                richServer.Document.Replace(RequesterAddrBook1.Range, text1);
                Bookmark RequesterAddrBook2 = richServer.Document.Bookmarks["RequesterAddrBook2"];
                richServer.Document.Replace(RequesterAddrBook2.Range, text2);
                Bookmark RequesterAddrBook3 = richServer.Document.Bookmarks["RequesterAddrBook3"];
                richServer.Document.Replace(RequesterAddrBook3.Range, text3);
            }
            catch { }

            // ���� ���������
            try
            {
                string DocDate = "";
                try { DocDate = perm.DocDate.ToShortDateString() + " �."; }
                catch { }
                Bookmark DocDateBook = richServer.Document.Bookmarks["DocDateBook"];
                richServer.Document.Replace(DocDateBook.Range, DocDate);
            }
            catch { }

            // ����� ���������
            try
            {
                string DocNo = "        ";
                //try { DocNo = perm.DocNo; }
                //catch { }
                Bookmark DocNoBook = richServer.Document.Bookmarks["DocNoBook"];
                richServer.Document.Replace(DocNoBook.Range, DocNo);
            }
            catch { }

            // ������� ���
            try
            {
                string YearDocDate = DateTime.Now.Year.ToString();
                if (perm.EDocStatus == AISOGD.Enums.eDocStatus.��������� || perm.EDocStatus == AISOGD.Enums.eDocStatus.����)
                    if (perm.DocDate != DateTime.MinValue)
                        YearDocDate = perm.DocDate.Year.ToString();
                Bookmark YearDocDateBook = richServer.Document.Bookmarks["YearDocDateBook"];
                richServer.Document.Replace(YearDocDateBook.Range, YearDocDate);
            }
            catch { }

            // ������ ������ � ������������ ... 
            Bookmark AccordingBook = richServer.Document.Bookmarks["AccordingBook"];
            string workKind = "";
            List<string> strikeout = new List<string>();
            if (perm.WorkKind == AISOGD.Enums.eWorkKind.�������������)
            {
                workKind = "������������";
                strikeout.Add("�������������������");
            }
            if (perm.WorkKind == AISOGD.Enums.eWorkKind.�������������)
            {
                workKind = "�������������������";
                strikeout.Add("������������,");
            }
            if (perm.WorkKind == AISOGD.Enums.eWorkKind.������������������)
            {
                workKind = "������������ �������� �� ����������";
                strikeout.Add("������������, ������������������� ������� ������������ �������������; ��������� �������; ������� ������������ �������������, ��������� � ������ ��������� �������;");
            }
            if(perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.�������������������������������)
            {
                strikeout.Add("��������� �������; ������� ������������ �������������, ��������� � ������ ��������� �������; ������������ �������� �� ���������� ������� ����������� ��������, ��� ������� ������������� �������������� � ������ �������������� ���������� � ������������ �������");
            }
            if (perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.��������������)
            {
                strikeout.Add("������� ������������ �������������;");
                strikeout.Add("������� ������������ �������������, ��������� � ������ ��������� �������; ������������ �������� �� ���������� ������� ����������� ��������, ��� ������� ������������� �������������� � ������ �������������� ���������� � ������������ �������");
            }
            if (perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.��������������������)
            {
                strikeout.Add("������� ������������ �������������; ��������� �������;");
                strikeout.Add("������������ �������� �� ���������� ������� ����������� ��������, ��� ������� ������������� �������������� � ������ �������������� ���������� � ������������ �������");
            }
            DocumentRange[] ranges = richServer.Document.FindAll(workKind, DevExpress.XtraRichEdit.API.Native.SearchOptions.None, AccordingBook.Range);
            SubDocument doc = ranges[0].BeginUpdateDocument();
            DevExpress.XtraRichEdit.API.Native.CharacterProperties cp = doc.BeginUpdateCharacters(ranges[0]);
            cp.Bold = true;
            cp.Underline = UnderlineType.Single;
            doc.EndUpdateCharacters(cp);

            foreach(string strout in strikeout)
            {
                DocumentRange[] ranges1 = richServer.Document.FindAll(strout, DevExpress.XtraRichEdit.API.Native.SearchOptions.None, AccordingBook.Range);
                SubDocument doc1 = ranges1[0].BeginUpdateDocument();
                DevExpress.XtraRichEdit.API.Native.CharacterProperties cp1 = doc.BeginUpdateCharacters(ranges1[0]);
                cp1.Strikeout = StrikeoutType.Single;
                doc1.EndUpdateCharacters(cp1);
            }

            //������������ ������� 
            // ��������� �� ������ �� 46 �������� 
            try
            {

                string ObjectName = "";
                ObjectName = perm.ObjectName;
                Bookmark ObjectNameBook1 = richServer.Document.Bookmarks["ObjectNameBook1"];
                richServer.Document.Replace(ObjectNameBook1.Range, ObjectName);
               
            }
            catch { }

            // ����������� ����� �������
            try
            {
                string ObjChangeCadNo = "";
                try { ObjChangeCadNo = perm.ObjChangeCadNo; }
                catch { }
                Bookmark ObjChangeCadNoBook = richServer.Document.Bookmarks["ObjChangeCadNoBook"];
                richServer.Document.Replace(ObjChangeCadNoBook.Range, ObjChangeCadNo);
            }
            catch { }

            //����� (��������������) ������� 
            // ��������� �� ������ - ������ 27 ��������, ��������� �� 46 �������� 
            try
            {
                string AddressObjectName = "";
                AddressObjectName = "���������� ���������, ����������� �������, ������������� ����������� \"����� �������\", ����� �������, " + perm.AddressObjectName;
               
                Bookmark AddressObjectNameBook1 = richServer.Document.Bookmarks["AddressObjectNameBook1"];
                richServer.Document.Replace(AddressObjectNameBook1.Range, AddressObjectName);

            }
            catch { }
            // �������� �� ������
            try
            {
                string AddressDocInfo = "";
                AddressDocInfo = "(" + perm.AddressDocInfo + ")";

                Bookmark AddressDocInfoBook = richServer.Document.Bookmarks["AddressDocInfoBook"];
                richServer.Document.Replace(AddressDocInfoBook.Range, AddressDocInfo);

            }
            catch { }

            // ����������� ����� �������
            try
            {
                string LotCadNo = "";
                try { LotCadNo = perm.LotCadNo; }
                catch { }
                Bookmark LotCadNoBook = richServer.Document.Bookmarks["LotCadNoBook"];
                richServer.Document.Replace(LotCadNoBook.Range, LotCadNo);
            }
            catch { }

            //������������ ����� 
            // ��������� �� ������ - ������ 27 ��������, ��������� �� 46 �������� 
            try
            {
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = perm.ConstrAddress.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;

                while (i < wordscount && (text1 + words[i]).Length < 50)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark AddressLotBook1 = richServer.Document.Bookmarks["AddressLotBook1"];
                richServer.Document.Replace(AddressLotBook1.Range, text1.TrimEnd());
                Bookmark AddressLotBook2 = richServer.Document.Bookmarks["AddressLotBook2"];
                richServer.Document.Replace(AddressLotBook2.Range, text2.TrimEnd());

            }
            catch { }

            // ������ ���������� �� �������������
            // ���� ���������� �� �������������
            try
            {
                string ConstrPermDocNo = "";
                string ConstrPermDocDate = "";
                foreach (ConstrPerm constrPerm in perm.ConstrPerm)
                {
                    if (ConstrPermDocNo == "")
                    {
                        try { ConstrPermDocNo = constrPerm.FullDocNo; }
                        catch { }
                        try {
                            if(constrPerm.ConstrPermInstead != null)
                                ConstrPermDocNo += Environment.NewLine + String.Format("(������ � {0} �� {1} �.)", 
                            constrPerm.ConstrPermInstead.FullDocNo, constrPerm.ConstrPermInstead.DocDate.ToShortDateString()); }
                        catch { }
                        try { ConstrPermDocDate = constrPerm.DocDate.ToShortDateString() + " ����"; }
                        catch { }
                    }
                    else
                    {
                        try { ConstrPermDocNo += ", " + Environment.NewLine + constrPerm.FullDocNo; }
                        catch { }
                        try
                        {
                            if (constrPerm.ConstrPermInstead != null)
                                ConstrPermDocNo += Environment.NewLine + String.Format("(������ � {0} �� {1} �.)",
                            constrPerm.ConstrPermInstead.FullDocNo, constrPerm.ConstrPermInstead.DocDate.ToShortDateString());
                        }
                        catch { }
                        try { ConstrPermDocDate += ", " + constrPerm.DocDate.ToShortDateString() + " ����"; }
                        catch { }
                    }
                }
                
                Bookmark ConstrPermDocNoBook = richServer.Document.Bookmarks["ConstrPermDocNoBook"];
                richServer.Document.Replace(ConstrPermDocNoBook.Range, ConstrPermDocNo);
                Bookmark ConstrPermDocDateBook = richServer.Document.Bookmarks["ConstrPermDocDateBook"];
                richServer.Document.Replace(ConstrPermDocDateBook.Range, ConstrPermDocDate);
            }
            catch { }


            #region// ��������������

            #region 1. ����� ���������� �������
            try
            {
                string BuildingSizeProject = "";
                try {
                    if (perm.BuildingSizeProject != 0)
                        BuildingSizeProject = perm.BuildingSizeProject.ToString();}
                catch { }
                Bookmark BuildingSizeProjectBook = richServer.Document.Bookmarks["BuildingSizeProjectBook"];
                richServer.Document.Replace(BuildingSizeProjectBook.Range, BuildingSizeProject);
            }
            catch { }
            try
            {
                string BuildingSizeFakt = "";
                try
                {
                    if (perm.BuildingSizeFakt != 0)
                        BuildingSizeFakt = perm.BuildingSizeFakt.ToString();
                }
                catch { }
                Bookmark BuildingSizeFaktBook = richServer.Document.Bookmarks["BuildingSizeFaktBook"];
                richServer.Document.Replace(BuildingSizeFaktBook.Range, BuildingSizeFakt);
            }
            catch { }
            try
            {
                string BuildingSizeOverGroundPartProject = "";
                try
                {
                    if (perm.BuildingSizeOverGroundPartProject != 0)
                        BuildingSizeOverGroundPartProject = perm.BuildingSizeOverGroundPartProject.ToString();
                }
                catch { }
                Bookmark BuildingSizeOverGroundPartProjectBook = richServer.Document.Bookmarks["BuildingSizeOverGroundPartProjectBook"];
                richServer.Document.Replace(BuildingSizeOverGroundPartProjectBook.Range, BuildingSizeOverGroundPartProject);
            }
            catch { }
            try
            {
                string BuildingSizeOverGroundPartFakt = "";
                try
                {
                    if (perm.BuildingSizeOverGroundPartFakt != 0)
                        BuildingSizeOverGroundPartFakt = perm.BuildingSizeOverGroundPartFakt.ToString();
                }
                catch { }
                Bookmark BuildingSizeOverGroundPartFaktBook = richServer.Document.Bookmarks["BuildingSizeOverGroundPartFaktBook"];
                richServer.Document.Replace(BuildingSizeOverGroundPartFaktBook.Range, BuildingSizeOverGroundPartFakt);
            }
            catch { }
            try
            {
                string BuildingSizeUnderGroundPartProject = "";
                try
                {
                    if (perm.BuildingSizeUnderGroundPartProject != 0)
                        BuildingSizeUnderGroundPartProject = perm.BuildingSizeUnderGroundPartProject.ToString();
                }
                catch { }
                Bookmark BuildingSizeUnderGroundPartProjectBook = richServer.Document.Bookmarks["BuildingSizeUnderGroundPartProjectBook"];
                richServer.Document.Replace(BuildingSizeUnderGroundPartProjectBook.Range, BuildingSizeUnderGroundPartProject);
            }
            catch { }
            try
            {
                string BuildingSizeUnderGroundPartFakt = "";
                try
                {
                    if (perm.BuildingSizeUnderGroundPartFakt != 0)
                        BuildingSizeUnderGroundPartFakt = perm.BuildingSizeUnderGroundPartFakt.ToString();
                }
                catch { }
                Bookmark BuildingSizeUnderGroundPartFaktBook = richServer.Document.Bookmarks["BuildingSizeUnderGroundPartFaktBook"];
                richServer.Document.Replace(BuildingSizeUnderGroundPartFaktBook.Range, BuildingSizeUnderGroundPartFakt);
            }
            catch { }
            try
            {
                string TotalBuildSquareProject = "";
                try
                {
                    if (perm.TotalBuildSquareProject != 0)
                        TotalBuildSquareProject = perm.TotalBuildSquareProject.ToString();
                }
                catch { }
                Bookmark TotalBuildSquareProjectBook = richServer.Document.Bookmarks["TotalBuildSquareProjectBook"];
                richServer.Document.Replace(TotalBuildSquareProjectBook.Range, TotalBuildSquareProject);
            }
            catch { }
            try
            {
                string TotalBuildSquareFakt = "";
                try
                {
                    if (perm.TotalBuildSquareFakt != 0)
                        TotalBuildSquareFakt = perm.TotalBuildSquareFakt.ToString();
                }
                catch { }
                Bookmark TotalBuildSquareFaktBook = richServer.Document.Bookmarks["TotalBuildSquareFaktBook"];
                richServer.Document.Replace(TotalBuildSquareFaktBook.Range, TotalBuildSquareFakt);
            }
            catch { }
            try
            {
                string NotLivingSquareProject = "";
                try
                {
                    if (perm.NotLivingSquareProject != 0)
                        NotLivingSquareProject = perm.NotLivingSquareProject.ToString();
                }
                catch { }
                Bookmark NotLivingSquareProjectBook = richServer.Document.Bookmarks["NotLivingSquareProjectBook"];
                richServer.Document.Replace(NotLivingSquareProjectBook.Range, NotLivingSquareProject);
            }
            catch { }
            try
            {
                string NotLivingSquareFakt = "";
                try
                {
                    if (perm.NotLivingSquareFakt != 0)
                        NotLivingSquareFakt = perm.NotLivingSquareFakt.ToString();
                }
                catch { }
                Bookmark NotLivingSquareFaktBook = richServer.Document.Bookmarks["NotLivingSquareFaktBook"];
                richServer.Document.Replace(NotLivingSquareFaktBook.Range, NotLivingSquareFakt);
            }
            catch { }
            try
            {
                string OutBuildingSquareProject = "";
                try
                {
                    if (perm.OutBuildingSquareProject != 0)
                        OutBuildingSquareProject = perm.OutBuildingSquareProject.ToString();
                }
                catch { }
                Bookmark OutBuildingSquareProjectBook = richServer.Document.Bookmarks["OutBuildingSquareProjectBook"];
                richServer.Document.Replace(OutBuildingSquareProjectBook.Range, OutBuildingSquareProject);
            }
            catch { }
            try
            {
                string OutBuildingSquareFakt = "";
                try
                {
                    if (perm.OutBuildingSquareFakt != 0)
                        OutBuildingSquareFakt = perm.OutBuildingSquareFakt.ToString();
                }
                catch { }
                Bookmark OutBuildingSquareFaktBook = richServer.Document.Bookmarks["OutBuildingSquareFaktBook"];
                richServer.Document.Replace(OutBuildingSquareFaktBook.Range, OutBuildingSquareFakt);
            }
            catch { }
            try
            {
                string BuildingCountProject = "";
                try
                {
                    if (perm.BuildingCountProject != 0)
                        BuildingCountProject = perm.BuildingCountProject.ToString();
                }
                catch { }
                Bookmark BuildingCountProjectBook = richServer.Document.Bookmarks["BuildingCountProjectBook"];
                richServer.Document.Replace(BuildingCountProjectBook.Range, BuildingCountProject);
            }
            catch { }
            try
            {
                string BuildingCountFakt = "";
                try
                {
                    if (perm.BuildingCountFakt != 0)
                        BuildingCountFakt = perm.BuildingCountFakt.ToString();
                }
                catch { }
                Bookmark BuildingCountFaktBook = richServer.Document.Bookmarks["BuildingCountFaktBook"];
                richServer.Document.Replace(BuildingCountFaktBook.Range, BuildingCountFakt);
            }
            catch { }
            #endregion

            #region ���������� �� ��������� ��� �������, ����� (���) (����������� � ��������, � ����������� �� ���� �������)
            try
            {
                string FloorCountProject = "";
                string bookFloorCountProject = "";
                try
                {
                    if (perm.FloorCountProject != 0)
                        FloorCountProject = perm.FloorCountProject.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookFloorCountProject = "FloorCountLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookFloorCountProject = "FloorCountNotLivingProjectBook";
                if (bookFloorCountProject != "")
                {
                    Bookmark FloorCountProjectBook = richServer.Document.Bookmarks[bookFloorCountProject];
                    richServer.Document.Replace(FloorCountProjectBook.Range, FloorCountProject);
                }
            }
            catch { }
            try
            {
                string FloorCountFakt = "";
                string bookFloorCountFakt = "";
                try
                {
                    if (perm.FloorCountFakt != 0)
                        FloorCountFakt = perm.FloorCountFakt.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookFloorCountFakt = "FloorCountLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookFloorCountFakt = "FloorCountNotLivingFaktBook";
                if (bookFloorCountFakt != "")
                {
                    Bookmark FloorCountFaktBook = richServer.Document.Bookmarks[bookFloorCountFakt];
                    richServer.Document.Replace(FloorCountFaktBook.Range, FloorCountFakt);
                }
            }
            catch { }

            try
            {
                string UnderGroundFloorCountProject = "";
                string bookUnderGroundFloorCountProject = "";
                try
                {
                    if (perm.UnderGroundFloorCounProject != 0)
                        UnderGroundFloorCountProject = perm.UnderGroundFloorCounProject.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookUnderGroundFloorCountProject = "UnderGroundFloorCountLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookUnderGroundFloorCountProject = "UnderGroundFloorCountNotLivingProjectB";
                if (bookUnderGroundFloorCountProject != "")
                {
                    Bookmark UnderGroundFloorCountProjectBook = richServer.Document.Bookmarks[bookUnderGroundFloorCountProject];
                    richServer.Document.Replace(UnderGroundFloorCountProjectBook.Range, UnderGroundFloorCountProject);
                }
            }
            catch { }

            try
            {
                string UnderGroundFloorCountFakt = "";
                string bookUnderGroundFloorCountFakt = "";
                try
                {
                    if (perm.UnderGroundFloorCountFakt != 0)
                        UnderGroundFloorCountFakt = perm.UnderGroundFloorCountFakt.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookUnderGroundFloorCountFakt = "UnderGroundFloorCountLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookUnderGroundFloorCountFakt = "UnderGroundFloorCountNotLivingFaktB";
                if (bookUnderGroundFloorCountFakt != "")
                {
                    Bookmark UnderGroundFloorCountFaktBook = richServer.Document.Bookmarks[bookUnderGroundFloorCountFakt];
                    richServer.Document.Replace(UnderGroundFloorCountFaktBook.Range, UnderGroundFloorCountFakt);
                }
            }
            catch { }
            #endregion

            #region �����, ����������, ���������� ��� ���
            try
            {
                string ElevatorsCountProject = "";
                string bookElevatorsCountProject = "";
                try
                {
                    if (perm.ElevatorsCountProject != 0)
                        ElevatorsCountProject = perm.ElevatorsCountProject.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookElevatorsCountProject = "ElevatorsCountLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookElevatorsCountProject = "ElevatorsCountNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookElevatorsCountProject = "ElevatorsCountProductProjectBook";
                if (bookElevatorsCountProject != "")
                {
                    Bookmark ElevatorsCountProjectBook = richServer.Document.Bookmarks[bookElevatorsCountProject];
                    richServer.Document.Replace(ElevatorsCountProjectBook.Range, ElevatorsCountProject);
                }
            }
            catch { }
            try
            {
                string ElevatorsCountFakt = "";
                string bookElevatorsCountFakt = "";
                try
                {
                    if (perm.ElevatorsCountFakt != 0)
                        ElevatorsCountFakt = perm.ElevatorsCountFakt.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookElevatorsCountFakt = "ElevatorsCountLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookElevatorsCountFakt = "ElevatorsCountNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookElevatorsCountFakt = "ElevatorsCountProductFaktBook";
                if (bookElevatorsCountFakt != "")
                {
                    Bookmark ElevatorsCountFaktBook = richServer.Document.Bookmarks[bookElevatorsCountFakt];
                    richServer.Document.Replace(ElevatorsCountFaktBook.Range, ElevatorsCountFakt);
                }
            }
            catch { }

            try
            {
                string EscalatorsCountProject = "";
                string bookEscalatorsCountProject = "";
                try
                {
                    if (perm.EscalatorsCountProject != 0)
                        EscalatorsCountProject = perm.EscalatorsCountProject.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookEscalatorsCountProject = "EscalatorsCountLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookEscalatorsCountProject = "EscalatorsCountNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookEscalatorsCountProject = "EscalatorsCountProductProjectBook";
                if (bookEscalatorsCountProject != "")
                {
                    Bookmark EscalatorsCountProjectBook = richServer.Document.Bookmarks[bookEscalatorsCountProject];
                    richServer.Document.Replace(EscalatorsCountProjectBook.Range, EscalatorsCountProject);
                }
            }
            catch { }
            try
            {
                string EscalatorsCountFakt = "";
                string bookEscalatorsCountFakt = "";
                try
                {
                    if (perm.EscalatorsCountFakt != 0)
                        EscalatorsCountFakt = perm.EscalatorsCountFakt.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookEscalatorsCountFakt = "EscalatorsCountLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookEscalatorsCountFakt = "EscalatorsCountNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookEscalatorsCountFakt = "EscalatorsCountProductFaktBook";
                if (bookEscalatorsCountFakt != "")
                {
                    Bookmark EscalatorsCountFaktBook = richServer.Document.Bookmarks[bookEscalatorsCountFakt];
                    richServer.Document.Replace(EscalatorsCountFaktBook.Range, EscalatorsCountFakt);
                }
            }
            catch { }

            try
            {
                string InvalidLiftsCountProject = "";
                string bookInvalidLiftsCountProject = "";
                try
                {
                    if (perm.InvalidLiftsCountProject != 0)
                        InvalidLiftsCountProject = perm.InvalidLiftsCountProject.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookInvalidLiftsCountProject = "InvalidLiftsCountLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookInvalidLiftsCountProject = "InvalidLiftsCountNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookInvalidLiftsCountProject = "InvalidLiftsCountProductProjectBook";
                if (bookInvalidLiftsCountProject != "")
                {
                    Bookmark InvalidLiftsCountProjectBook = richServer.Document.Bookmarks[bookInvalidLiftsCountProject];
                    richServer.Document.Replace(InvalidLiftsCountProjectBook.Range, InvalidLiftsCountProject);
                }
            }
            catch { }
            try
            {
                string InvalidLiftsCountFakt = "";
                string bookInvalidLiftsCountFakt = "";
                try
                {
                    if (perm.InvalidLiftsCountFakt != 0)
                        InvalidLiftsCountFakt = perm.InvalidLiftsCountFakt.ToString();
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookInvalidLiftsCountFakt = "InvalidLiftsCountLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookInvalidLiftsCountFakt = "InvalidLiftsCountNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookInvalidLiftsCountFakt = "InvalidLiftsCountProductFaktBook";
                if (bookInvalidLiftsCountFakt != "")
                {
                    Bookmark InvalidLiftsCountFaktBook = richServer.Document.Bookmarks[bookInvalidLiftsCountFakt];
                    richServer.Document.Replace(InvalidLiftsCountFaktBook.Range, InvalidLiftsCountFakt);
                }
            }
            catch { }
            #endregion

            #region ���������
            try
            {
                string FundMaterialProject = "";
                string bookFundMaterialProject = "";
                try
                {
                   FundMaterialProject = perm.FundMaterialProject.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookFundMaterialProject = "FundMaterialLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookFundMaterialProject = "FundMaterialNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookFundMaterialProject = "FundMaterialProductProjectBook";
                if (bookFundMaterialProject != "")
                {
                    Bookmark FundMaterialProjectBook = richServer.Document.Bookmarks[bookFundMaterialProject];
                    richServer.Document.Replace(FundMaterialProjectBook.Range, FundMaterialProject);
                }
            }
            catch { }
            try
            {
                string FundMaterialFakt = "";
                string bookFundMaterialFakt = "";
                try
                {
                        FundMaterialFakt = perm.FundMaterialFakt.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookFundMaterialFakt = "FundMaterialLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookFundMaterialFakt = "FundMaterialNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookFundMaterialFakt = "FundMaterialProductFaktBook";
                if (bookFundMaterialFakt != "")
                {
                    Bookmark FundMaterialFaktBook = richServer.Document.Bookmarks[bookFundMaterialFakt];
                    richServer.Document.Replace(FundMaterialFaktBook.Range, FundMaterialFakt);
                }
            }
            catch { }

            try
            {
                string WallMaterialProject = "";
                string bookWallMaterialProject = "";
                try
                {
                    WallMaterialProject = perm.WallMaterialProject.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookWallMaterialProject = "WallMaterialLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookWallMaterialProject = "WallMaterialNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookWallMaterialProject = "WallMaterialProductProjectBook";
                if (bookWallMaterialProject != "")
                {
                    Bookmark WallMaterialProjectBook = richServer.Document.Bookmarks[bookWallMaterialProject];
                    richServer.Document.Replace(WallMaterialProjectBook.Range, WallMaterialProject);
                }
            }
            catch { }
            try
            {
                string WallMaterialFakt = "";
                string bookWallMaterialFakt = "";
                try
                {
                    WallMaterialFakt = perm.WallMaterialFakt.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookWallMaterialFakt = "WallMaterialLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookWallMaterialFakt = "WallMaterialNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookWallMaterialFakt = "WallMaterialProductFaktBook";
                if (bookWallMaterialFakt != "")
                {
                    Bookmark WallMaterialFaktBook = richServer.Document.Bookmarks[bookWallMaterialFakt];
                    richServer.Document.Replace(WallMaterialFaktBook.Range, WallMaterialFakt);
                }
            }
            catch { }

            try
            {
                string BorderMaterialProject = "";
                string bookBorderMaterialProject = "";
                try
                {
                    BorderMaterialProject = perm.BorderMaterialProject.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookBorderMaterialProject = "BorderMaterialLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookBorderMaterialProject = "BorderMaterialNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookBorderMaterialProject = "BorderMaterialProductProjectBook";
                if (bookBorderMaterialProject != "")
                {
                    Bookmark BorderMaterialProjectBook = richServer.Document.Bookmarks[bookBorderMaterialProject];
                    richServer.Document.Replace(BorderMaterialProjectBook.Range, BorderMaterialProject);
                }
            }
            catch { }
            try
            {
                string BorderMaterialFakt = "";
                string bookBorderMaterialFakt = "";
                try
                {
                    BorderMaterialFakt = perm.BorderMaterialFakt.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookBorderMaterialFakt = "BorderMaterialLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookBorderMaterialFakt = "BorderMaterialNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookBorderMaterialFakt = "BorderMaterialProductFaktBook";
                if (bookBorderMaterialFakt != "")
                {
                    Bookmark BorderMaterialFaktBook = richServer.Document.Bookmarks[bookBorderMaterialFakt];
                    richServer.Document.Replace(BorderMaterialFaktBook.Range, BorderMaterialFakt);
                }
            }
            catch { }

            try
            {
                string RoofMaterialProject = "";
                string bookRoofMaterialProject = "";
                try
                {
                    RoofMaterialProject = perm.RoofMaterialProject.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookRoofMaterialProject = "RoofMaterialLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookRoofMaterialProject = "RoofMaterialNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookRoofMaterialProject = "RoofMaterialProductProjectBook";
                if (bookRoofMaterialProject != "")
                {
                    Bookmark RoofMaterialProjectBook = richServer.Document.Bookmarks[bookRoofMaterialProject];
                    richServer.Document.Replace(RoofMaterialProjectBook.Range, RoofMaterialProject);
                }
            }
            catch { }
            try
            {
                string RoofMaterialFakt = "";
                string bookRoofMaterialFakt = "";
                try
                {
                    RoofMaterialFakt = perm.RoofMaterialFakt.Name;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookRoofMaterialFakt = "RoofMaterialLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookRoofMaterialFakt = "RoofMaterialNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookRoofMaterialFakt = "RoofMaterialProductFaktBook";
                if (bookRoofMaterialFakt != "")
                {
                    Bookmark RoofMaterialFaktBook = richServer.Document.Bookmarks[bookRoofMaterialFakt];
                    richServer.Document.Replace(RoofMaterialFaktBook.Range, RoofMaterialFakt);
                }
            }
            catch { }
            #endregion

            #region ���� ����������
            try
            {
                string OtherIndicatorsProject = "";
                string bookOtherIndicatorsProject = "";
                try
                {
                    OtherIndicatorsProject = perm.OtherIndicatorsProject;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookOtherIndicatorsProject = "OtherIndicatorsLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookOtherIndicatorsProject = "OtherIndicatorsNotLivingProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookOtherIndicatorsProject = "OtherIndicatorsProductProjectBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.��������)
                    bookOtherIndicatorsProject = "OtherIndicatorsLineProjectBook";
                if (bookOtherIndicatorsProject != "")
                {
                    Bookmark OtherIndicatorsProjectBook = richServer.Document.Bookmarks[bookOtherIndicatorsProject];
                    richServer.Document.Replace(OtherIndicatorsProjectBook.Range, OtherIndicatorsProject);
                }
            }
            catch { }
            try
            {
                string OtherIndicatorsFakt = "";
                string bookOtherIndicatorsFakt = "";
                try
                {
                    OtherIndicatorsFakt = perm.OtherIndicatorsFakt;
                }
                catch { }
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�����)
                    bookOtherIndicatorsFakt = "OtherIndicatorsLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.�������)
                    bookOtherIndicatorsFakt = "OtherIndicatorsNotLivingFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.���������������������������)
                    bookOtherIndicatorsFakt = "OtherIndicatorsProductFaktBook";
                if (perm.BuildingKind == AISOGD.Enums.eBuildingKind.��������)
                    bookOtherIndicatorsFakt = "OtherIndicatorsLineFaktBook";
                if (bookOtherIndicatorsFakt != "")
                {
                    Bookmark OtherIndicatorsFaktBook = richServer.Document.Bookmarks[bookOtherIndicatorsFakt];
                    richServer.Document.Replace(OtherIndicatorsFaktBook.Range, OtherIndicatorsFakt);
                }
            }
            catch { }

            #endregion

            #region ������� �������
            try
            {
                string PlacesCountProject = "";
                try
                {
                    if (perm.PlacesCountProject != 0)
                        PlacesCountProject = perm.PlacesCountProject.ToString();
                }
                catch { }
                Bookmark PlacesCountProjectBook = richServer.Document.Bookmarks["PlacesCountProjectBook"];
                richServer.Document.Replace(PlacesCountProjectBook.Range, PlacesCountProject);
            }
            catch { }
            try
            {
                string PlacesCountFakt = "";
                try
                {
                    if (perm.PlacesCountFakt != 0)
                        PlacesCountFakt = perm.PlacesCountFakt.ToString();
                }
                catch { }
                Bookmark PlacesCountFaktBook = richServer.Document.Bookmarks["PlacesCountFaktBook"];
                richServer.Document.Replace(PlacesCountFaktBook.Range, PlacesCountFakt);
            }
            catch { }
            try
            {
                string RoomCountProject = "";
                try
                {
                    if (perm.RoomCountProject != 0)
                        RoomCountProject = perm.RoomCountProject.ToString();
                }
                catch { }
                Bookmark RoomCountProjectBook = richServer.Document.Bookmarks["RoomCountProjectBook"];
                richServer.Document.Replace(RoomCountProjectBook.Range, RoomCountProject);
            }
            catch { }
            try
            {
                string RoomCountFakt = "";
                try
                {
                    if (perm.RoomCountFakt != 0)
                        RoomCountFakt = perm.RoomCountFakt.ToString();
                }
                catch { }
                Bookmark RoomCountFaktBook = richServer.Document.Bookmarks["RoomCountFaktBook"];
                richServer.Document.Replace(RoomCountFaktBook.Range, RoomCountFakt);
            }
            catch { }
            try
            {
                string CapacityProject = "";
                try
                {
                    if (perm.CapacityProject != 0)
                        CapacityProject = perm.CapacityProject.ToString();
                }
                catch { }
                Bookmark CapacityProjectBook = richServer.Document.Bookmarks["CapacityProjectBook"];
                richServer.Document.Replace(CapacityProjectBook.Range, CapacityProject);
            }
            catch { }
            try
            {
                string CapacityFakt = "";
                try
                {
                    if (perm.CapacityFakt != 0)
                        CapacityFakt = perm.CapacityFakt.ToString();
                }
                catch { }
                Bookmark CapacityFaktBook = richServer.Document.Bookmarks["CapacityFaktBook"];
                richServer.Document.Replace(CapacityFaktBook.Range, CapacityFakt);
            }
            catch { }
            #endregion

            #region ������� ��������� �����
            try
            {
                string LivingSpaceProject = "";
                try
                {
                    if (perm.LivingSpaceProject != 0)
                        LivingSpaceProject = perm.LivingSpaceProject.ToString();
                }
                catch { }
                Bookmark LivingSpaceProjectBook = richServer.Document.Bookmarks["LivingSpaceProjectBook"];
                richServer.Document.Replace(LivingSpaceProjectBook.Range, LivingSpaceProject);
            }
            catch { }
            try
            {
                string LivingSpaceFakt = "";
                try
                {
                    if (perm.LivingSpaceFakt != 0)
                        LivingSpaceFakt = perm.LivingSpaceFakt.ToString();
                }
                catch { }
                Bookmark LivingSpaceFaktBook = richServer.Document.Bookmarks["LivingSpaceFaktBook"];
                richServer.Document.Replace(LivingSpaceFaktBook.Range, LivingSpaceFakt);
            }
            catch { }
            try
            {
                string BalconySpaceFakt = "";
                try
                {
                    if (perm.BalconySpaceFakt != 0)
                        BalconySpaceFakt = perm.BalconySpaceFakt.ToString();
                }
                catch { }
                Bookmark BalconySpaceFaktBook = richServer.Document.Bookmarks["BalconySpaceFaktBook"];
                richServer.Document.Replace(BalconySpaceFaktBook.Range, BalconySpaceFakt);
            }
            catch { }
            try
            {
                string NotLivingSpaceProject = "";
                try
                {
                    if (perm.NotLivingSpaceProject != 0)
                        NotLivingSpaceProject = perm.NotLivingSpaceProject.ToString();
                }
                catch { }
                Bookmark NotLivingSpaceProjectBook = richServer.Document.Bookmarks["NotLivingSpaceProjectBook"];
                richServer.Document.Replace(NotLivingSpaceProjectBook.Range, NotLivingSpaceProject);
            }
            catch { }
            try
            {
                string NotLivingSpaceFakt = "";
                try
                {
                    if (perm.NotLivingSpaceFakt != 0)
                        NotLivingSpaceFakt = perm.NotLivingSpaceFakt.ToString();
                }
                catch { }
                Bookmark NotLivingSpaceFaktBook = richServer.Document.Bookmarks["NotLivingSpaceFaktBook"];
                richServer.Document.Replace(NotLivingSpaceFaktBook.Range, NotLivingSpaceFakt);
            }
            catch { }
            try
            {
                string SectionCountProject = "";
                try
                {
                    if (perm.SectionCountProject != 0)
                        SectionCountProject = perm.SectionCountProject.ToString();
                }
                catch { }
                Bookmark SectionCountProjectBook = richServer.Document.Bookmarks["SectionCountProjectBook"];
                richServer.Document.Replace(SectionCountProjectBook.Range, SectionCountProject);
            }
            catch { }
            try
            {
                string SectionCountFakt = "";
                try
                {
                    if (perm.SectionCountFakt != 0)
                        SectionCountFakt = perm.SectionCountFakt.ToString();
                }
                catch { }
                Bookmark SectionCountFaktBook = richServer.Document.Bookmarks["SectionCountFaktBook"];
                richServer.Document.Replace(SectionCountFaktBook.Range, SectionCountFakt);
            }
            catch { }
            try
            {
                string AppartmentsCountAndSpaceProject = "";
                try
                {
                    if(perm.AppartmentsCountProject!=0)
                        AppartmentsCountAndSpaceProject = perm.AppartmentsCountProject.ToString();
                }
                catch { }
                try
                {
                    double totalLivingSpaceProject = 0;
                    totalLivingSpaceProject = perm.OneRoomFlatSpaceFakt + perm.TwoRoomFlatSpaceProject + perm.ThreeRoomFlatSpaceProject + perm.FourRoomSpaceProject +
                        perm.MoreRoomSpaceProject + perm.StudioSpaceProject;
                    if (totalLivingSpaceProject != 0)
                        
                        AppartmentsCountAndSpaceProject += "/" + totalLivingSpaceProject.ToString();
                }
                catch { }
                Bookmark AppartmentsCountAndSpaceProjectBook = richServer.Document.Bookmarks["AppartmentsCountAndSpaceProjectBook"];
                richServer.Document.Replace(AppartmentsCountAndSpaceProjectBook.Range, AppartmentsCountAndSpaceProject);
            }
            catch { }
            try
            {
                string AppartmentsCountAndSpaceFakt = "";
                try
                {
                    if (perm.AppartmentsCountFakt != 0)
                        AppartmentsCountAndSpaceFakt = perm.AppartmentsCountFakt.ToString();
                }
                catch { }
                try
                {
                    double totalLivingSpaceFakt = 0;
                    totalLivingSpaceFakt = perm.OneRoomFlatSpaceFakt + perm.TwoRoomFlatSpaceFakt + perm.ThreeRoomFlatSpaceFakt + perm.FourRoomSpaceFakt +
                        perm.MoreRoomSpaceFakt + perm.StudioSpaceFakt;
                    if (totalLivingSpaceFakt != 0)
                        AppartmentsCountAndSpaceFakt += "/" + totalLivingSpaceFakt.ToString();
                }
                catch { }
                Bookmark AppartmentsCountAndSpaceFaktBook = richServer.Document.Bookmarks["AppartmentsCountAndSpaceFaktBook"];
                richServer.Document.Replace(AppartmentsCountAndSpaceFaktBook.Range, AppartmentsCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string StudioCountAndSpaceProject = "";
                try
                {
                    if (perm.StudioCountProject != 0)
                        StudioCountAndSpaceProject = perm.StudioCountProject.ToString();
                }
                catch { }
                try
                {
                    if (perm.StudioSpaceProject != 0)
                        StudioCountAndSpaceProject += "/" + perm.StudioSpaceProject.ToString();
                }
                catch { }
                Bookmark StudioCountAndSpaceProjectBook = richServer.Document.Bookmarks["StudioCountAndSpaceProjectBook"];
                richServer.Document.Replace(StudioCountAndSpaceProjectBook.Range, StudioCountAndSpaceProject);
            }
            catch { }
            try
            {
                string StudioCountAndSpaceFakt = "";
                try
                {
                    if (perm.StudioCountFakt != 0)
                        StudioCountAndSpaceFakt = perm.StudioCountFakt.ToString();
                }
                catch { }
                try
                {
                    if (perm.StudioSpaceFakt != 0)
                        StudioCountAndSpaceFakt += "/" + perm.StudioSpaceFakt.ToString();
                }
                catch { }
                Bookmark StudioCountAndSpaceFaktBook = richServer.Document.Bookmarks["StudioCountAndSpaceFaktBook"];
                richServer.Document.Replace(StudioCountAndSpaceFaktBook.Range, StudioCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string OneRoomFlatCountAndSpaceProject = "";
                try
                {
                    if (perm.OneRoomFlatCountProject != 0)
                        OneRoomFlatCountAndSpaceProject = perm.OneRoomFlatCountProject.ToString();
                }
                catch { }
                try
                {
                    if (perm.OneRoomFlatSpaceProject != 0)
                        OneRoomFlatCountAndSpaceProject += "/" + perm.OneRoomFlatSpaceProject.ToString();
                }
                catch { }
                Bookmark OneRoomFlatCountAndSpaceProjectBook = richServer.Document.Bookmarks["OneRoomFlatCountAndSpaceProjectBook"];
                richServer.Document.Replace(OneRoomFlatCountAndSpaceProjectBook.Range, OneRoomFlatCountAndSpaceProject);
            }
            catch { }
            try
            {
                string OneRoomFlatCountAndSpaceFakt = "";
                try
                {
                    if (perm.OneRoomFlatCountFakt != 0)
                        OneRoomFlatCountAndSpaceFakt = perm.OneRoomFlatCountFakt.ToString();
                }
                catch { }
                try
                {
                    if (perm.OneRoomFlatSpaceFakt != 0)
                        OneRoomFlatCountAndSpaceFakt += "/" + perm.OneRoomFlatSpaceFakt.ToString();
                }
                catch { }
                Bookmark OneRoomFlatCountAndSpaceFaktBook = richServer.Document.Bookmarks["OneRoomFlatCountAndSpaceFaktBook"];
                richServer.Document.Replace(OneRoomFlatCountAndSpaceFaktBook.Range, OneRoomFlatCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string TwoRoomFlatCountAndSpaceProject = "";
                try
                {
                    if (perm.TwoRoomFlatCountProject != 0)
                        TwoRoomFlatCountAndSpaceProject = perm.TwoRoomFlatCountProject.ToString();
                }
                catch { }
                try
                {
                    if (perm.TwoRoomFlatSpaceProject != 0)
                        TwoRoomFlatCountAndSpaceProject += "/" + perm.TwoRoomFlatSpaceProject.ToString();
                }
                catch { }
                Bookmark TwoRoomFlatCountAndSpaceProjectBook = richServer.Document.Bookmarks["TwoRoomFlatCountAndSpaceProjectBook"];
                richServer.Document.Replace(TwoRoomFlatCountAndSpaceProjectBook.Range, TwoRoomFlatCountAndSpaceProject);
            }
            catch { }
            try
            {
                string TwoRoomFlatCountAndSpaceFakt = "";
                try
                {
                    if (perm.TwoRoomFlatCountFakt != 0)
                        TwoRoomFlatCountAndSpaceFakt = perm.TwoRoomFlatCountFakt.ToString();
                }
                catch { }
                try
                {
                    if (perm.TwoRoomFlatSpaceFakt != 0)
                        TwoRoomFlatCountAndSpaceFakt += "/" + perm.TwoRoomFlatSpaceFakt.ToString();
                }
                catch { }
                Bookmark TwoRoomFlatCountAndSpaceFaktBook = richServer.Document.Bookmarks["TwoRoomFlatCountAndSpaceFaktBook"];
                richServer.Document.Replace(TwoRoomFlatCountAndSpaceFaktBook.Range, TwoRoomFlatCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string ThreeRoomFlatCountAndSpaceProject = "";
                try
                {
                    if (perm.ThreeRoomFlatCountProject != 0)
                        ThreeRoomFlatCountAndSpaceProject = perm.ThreeRoomFlatCountProject.ToString();
                }
                catch { }
                try
                {
                    if (perm.ThreeRoomFlatSpaceProject != 0)
                        ThreeRoomFlatCountAndSpaceProject += "/" + perm.ThreeRoomFlatSpaceProject.ToString();
                }
                catch { }
                Bookmark ThreeRoomFlatCountAndSpaceProjectBook = richServer.Document.Bookmarks["ThreeRoomFlatCountAndSpaceProjectBook"];
                richServer.Document.Replace(ThreeRoomFlatCountAndSpaceProjectBook.Range, ThreeRoomFlatCountAndSpaceProject);
            }
            catch { }
            try
            {
                string ThreeRoomFlatCountAndSpaceFakt = "";
                try
                {
                    if (perm.ThreeRoomFlatCountFakt != 0)
                        ThreeRoomFlatCountAndSpaceFakt = perm.ThreeRoomFlatCountFakt.ToString();
                }
                catch { }
                try
                {
                    if (perm.ThreeRoomFlatSpaceFakt != 0)
                        ThreeRoomFlatCountAndSpaceFakt += "/" + perm.ThreeRoomFlatSpaceFakt.ToString();
                }
                catch { }
                Bookmark ThreeRoomFlatCountAndSpaceFaktBook = richServer.Document.Bookmarks["ThreeRoomFlatCountAndSpaceFaktBook"];
                richServer.Document.Replace(ThreeRoomFlatCountAndSpaceFaktBook.Range, ThreeRoomFlatCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string FourRoomFlatCountAndSpaceProject = "";
                try
                {
                    if (perm.FourRoomFlatCountProject != 0)
                        FourRoomFlatCountAndSpaceProject = perm.FourRoomFlatCountProject.ToString();
                }
                catch { }
                try
                {
                    if (perm.FourRoomSpaceProject != 0)
                        FourRoomFlatCountAndSpaceProject += "/" + perm.FourRoomSpaceProject.ToString();
                }
                catch { }
                Bookmark FourRoomFlatCountAndSpaceProjectBook = richServer.Document.Bookmarks["FourRoomFlatCountAndSpaceProjectBook"];
                richServer.Document.Replace(FourRoomFlatCountAndSpaceProjectBook.Range, FourRoomFlatCountAndSpaceProject);
            }
            catch { }
            try
            {
                string FourRoomFlatCountAndSpaceFakt = "";
                try
                {
                    if (perm.FourRoomFlatCountFakt != 0)
                        FourRoomFlatCountAndSpaceFakt = perm.FourRoomFlatCountFakt.ToString();
                }
                catch { }
                try
                {
                    if (perm.FourRoomSpaceFakt != 0)
                        FourRoomFlatCountAndSpaceFakt += "/" + perm.FourRoomSpaceFakt.ToString();
                }
                catch { }
                Bookmark FourRoomFlatCountAndSpaceFaktBook = richServer.Document.Bookmarks["FourRoomFlatCountAndSpaceFaktBook"];
                richServer.Document.Replace(FourRoomFlatCountAndSpaceFaktBook.Range, FourRoomFlatCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string MoreRoomFlatCountAndSpaceProject = "";
                try
                {
                    if (perm.MoreRoomFlatCountProject != 0)
                        MoreRoomFlatCountAndSpaceProject = perm.MoreRoomFlatCountProject.ToString();
                }
                catch { }
                try
                {
                    if (perm.MoreRoomSpaceProject != 0)
                        MoreRoomFlatCountAndSpaceProject += "/" + perm.MoreRoomSpaceProject.ToString();
                }
                catch { }
                Bookmark MoreRoomFlatCountAndSpaceProjectBook = richServer.Document.Bookmarks["MoreRoomFlatCountAndSpaceProjectBook"];
                richServer.Document.Replace(MoreRoomFlatCountAndSpaceProjectBook.Range, MoreRoomFlatCountAndSpaceProject);
            }
            catch { }
            try
            {
                string MoreRoomFlatCountAndSpaceFakt = "";
                try
                {
                    if (perm.MoreRoomFlatCountFakt != 0)
                        MoreRoomFlatCountAndSpaceFakt = perm.MoreRoomFlatCountFakt.ToString();
                }
                catch { }
                try
                {
                    if (perm.MoreRoomSpaceFakt != 0)
                        MoreRoomFlatCountAndSpaceFakt += "/" + perm.MoreRoomSpaceFakt.ToString();
                }
                catch { }
                Bookmark MoreRoomFlatCountAndSpaceFaktBook = richServer.Document.Bookmarks["MoreRoomFlatCountAndSpaceFaktBook"];
                richServer.Document.Replace(MoreRoomFlatCountAndSpaceFaktBook.Range, MoreRoomFlatCountAndSpaceFakt);
            }
            catch { }
            try
            {
                string TotalLivingSpaceProject = "";
                try
                {
                    if (perm.TotalLivingSpaceProject != 0)
                        TotalLivingSpaceProject = perm.TotalLivingSpaceProject.ToString();
                }
                catch { }
                Bookmark TotalLivingSpaceProjectBook = richServer.Document.Bookmarks["TotalLivingSpaceProjectBook"];
                richServer.Document.Replace(TotalLivingSpaceProjectBook.Range, TotalLivingSpaceProject);
            }
            catch { }
            try
            {
                string TotalLivingSpaceFakt = "";
                try
                {
                    if (perm.TotalLivingSpaceFakt != 0)
                        TotalLivingSpaceFakt = perm.TotalLivingSpaceFakt.ToString();
                }
                catch { }
                Bookmark TotalLivingSpaceFaktBook = richServer.Document.Bookmarks["TotalLivingSpaceFaktBook"];
                richServer.Document.Replace(TotalLivingSpaceFaktBook.Range, TotalLivingSpaceFakt);
            }
            catch { }
            #endregion

            #region ������� ����������������� ����������
            try
            {
                string ProductPurposeObjectType = "";
                try
                {
                    ProductPurposeObjectType = perm.ProductPurposeObjectType.Name;
                }
                catch { }
                Bookmark ProductPurposeObjectTypeBook1 = richServer.Document.Bookmarks["ProductPurposeObjectTypeBook1"];
                richServer.Document.Replace(ProductPurposeObjectTypeBook1.Range, ProductPurposeObjectType);
                Bookmark ProductPurposeObjectTypeBook2 = richServer.Document.Bookmarks["ProductPurposeObjectTypeBook2"];
                richServer.Document.Replace(ProductPurposeObjectTypeBook2.Range, ProductPurposeObjectType);
            }
            catch { }

            try
            {
                string PowerProject = "";
                try
                {
                    PowerProject = perm.PowerProject;
                }
                catch { }
                Bookmark PowerProjectBook = richServer.Document.Bookmarks["PowerProjectBook"];
                richServer.Document.Replace(PowerProjectBook.Range, PowerProject);
            }
            catch { }
            try
            {
                string PowerFakt = "";
                try
                {
                    PowerFakt = perm.PowerFakt;
                }
                catch { }
                Bookmark PowerFaktBook = richServer.Document.Bookmarks["PowerFaktBook"];
                richServer.Document.Replace(PowerFaktBook.Range, PowerFakt);
            }
            catch { }
            try
            {
                string ProductivityProject = "";
                try
                {
                    ProductivityProject = perm.ProductivityProject;
                }
                catch { }
                Bookmark ProductivityProjectBook = richServer.Document.Bookmarks["ProductivityProjectBook"];
                richServer.Document.Replace(ProductivityProjectBook.Range, ProductivityProject);
            }
            catch { }
            try
            {
                string ProductivityFakt = "";
                try
                {
                    ProductivityFakt = perm.ProductivityFakt;
                }
                catch { }
                Bookmark ProductivityFaktBook = richServer.Document.Bookmarks["ProductivityFaktBook"];
                richServer.Document.Replace(ProductivityFaktBook.Range, ProductivityFakt);
            }
            catch { }
            #endregion

            #region �������� �������
            try
            {
                string LineObjectClass = "";
                try
                {
                    LineObjectClass = perm.LineObjectClass.Name;
                }
                catch { }
                Bookmark LineObjectClassBook1 = richServer.Document.Bookmarks["LineObjectClassBook1"];
                richServer.Document.Replace(LineObjectClassBook1.Range, LineObjectClass);
                Bookmark LineObjectClassBook2 = richServer.Document.Bookmarks["LineObjectClassBook2"];
                richServer.Document.Replace(LineObjectClassBook2.Range, LineObjectClass);
            }
            catch { }
            try
            {
                string LengthProject = "";
                try
                {
                    if (perm.LengthProject!=0)
                        LengthProject = perm.LengthProject.ToString();
                }
                catch { }
                Bookmark LengthProjectBook = richServer.Document.Bookmarks["LengthProjectBook"];
                richServer.Document.Replace(LengthProjectBook.Range, LengthProject);

            }
            catch { }
            try
            {
                string LengthFakt = "";
                try
                {
                    if (perm.LengthFakt != 0)
                        LengthFakt = perm.LengthFakt.ToString();
                }
                catch { }
                Bookmark LengthFaktBook = richServer.Document.Bookmarks["LengthFaktBook"];
                richServer.Document.Replace(LengthFaktBook.Range, LengthFakt);
            }
            catch { }
            try
            {
                string PowerLineProject = "";
                try
                {
                    PowerLineProject = perm.PowerLineProject.ToString();
                }
                catch { }
                Bookmark PowerLineProjectBook = richServer.Document.Bookmarks["PowerLineProjectBook"];
                richServer.Document.Replace(PowerLineProjectBook.Range, PowerLineProject);
            }
            catch { }
            try
            {
                string PowerLineFakt = "";
                try
                {
                    PowerLineFakt = perm.PowerLineFakt.ToString();
                }
                catch { }
                Bookmark PowerLineFaktBook = richServer.Document.Bookmarks["PowerLineFaktBook"];
                richServer.Document.Replace(PowerLineFaktBook.Range, PowerLineFakt);
            }
            catch { }
            try
            {
                string PipesInfoProject = "";
                try
                {
                    PipesInfoProject = perm.PipesInfoProject.ToString();
                }
                catch { }
                Bookmark PipesInfoProjectBook = richServer.Document.Bookmarks["PipesInfoProjectBook"];
                richServer.Document.Replace(PipesInfoProjectBook.Range, PipesInfoProject);
            }
            catch { }
            try
            {
                string PipesInfoFakt = "";
                try
                {
                    PipesInfoFakt = perm.PipesInfoFakt.ToString();
                }
                catch { }
                Bookmark PipesInfoFaktBook = richServer.Document.Bookmarks["PipesInfoFaktBook"];
                richServer.Document.Replace(PipesInfoFaktBook.Range, PipesInfoFakt);
            }
            catch { }
            try
            {
                string ElectricLinesInfoProject = "";
                try
                {
                    ElectricLinesInfoProject = perm.ElectricLinesInfoProject.ToString();
                }
                catch { }
                Bookmark ElectricLinesInfoProjectBook = richServer.Document.Bookmarks["ElectricLinesInfoProjectBook"];
                richServer.Document.Replace(ElectricLinesInfoProjectBook.Range, ElectricLinesInfoProject);
            }
            catch { }
            try
            {
                string ElectricLinesInfoFakt = "";
                try
                {
                    ElectricLinesInfoFakt = perm.ElectricLinesInfoFakt.ToString();
                }
                catch { }
                Bookmark ElectricLinesInfoFaktBook = richServer.Document.Bookmarks["ElectricLinesInfoFaktBook"];
                richServer.Document.Replace(ElectricLinesInfoFaktBook.Range, ElectricLinesInfoFakt);
            }
            catch { }
            try
            {
                string ConstructiveElementsInfoProject = "";
                try
                {
                    ConstructiveElementsInfoProject = perm.ConstructiveElementsInfoProject.ToString();
                }
                catch { }
                Bookmark ConstructiveElementsInfoProjectBook = richServer.Document.Bookmarks["ConstructiveElementsInfoProjectBook"];
                richServer.Document.Replace(ConstructiveElementsInfoProjectBook.Range, ConstructiveElementsInfoProject);
            }
            catch { }
            try
            {
                string ConstructiveElementsInfoFakt = "";
                try
                {
                    ConstructiveElementsInfoFakt = perm.ConstructiveElementsInfoFakt.ToString();
                }
                catch { }
                Bookmark ConstructiveElementsInfoFaktBook = richServer.Document.Bookmarks["ConstructiveElementsInfoFaktBook"];
                richServer.Document.Replace(ConstructiveElementsInfoFaktBook.Range, ConstructiveElementsInfoFakt);
            }
            catch { }
            #endregion

            #region 5. ������������ ����������� �������������� �������������
            try
            {
                string EnergyEfficiencyClassProject = "";
                try
                {
                    EnergyEfficiencyClassProject = perm.EnergyEfficiencyClassProject.Name;
                }
                catch { }
                Bookmark EnergyEfficiencyClassProjectBook = richServer.Document.Bookmarks["EnergyEfficiencyClassProjectBook"];
                richServer.Document.Replace(EnergyEfficiencyClassProjectBook.Range, EnergyEfficiencyClassProject);
            }
            catch { }
            try
            {
                string EnergyEfficiencyClassFakt = "";
                try
                {
                    EnergyEfficiencyClassFakt = perm.EnergyEfficiencyClassFakt.Name;
                }
                catch { }
                Bookmark EnergyEfficiencyClassFaktBook = richServer.Document.Bookmarks["EnergyEfficiencyClassFaktBook"];
                richServer.Document.Replace(EnergyEfficiencyClassFaktBook.Range, EnergyEfficiencyClassFakt);
            }
            catch { }
            try
            {
                string HeatUnit = "";
                try
                {
                    HeatUnit = perm.HeatUnit.Name;
                }
                catch { }
                Bookmark HeatUnitBook = richServer.Document.Bookmarks["HeatUnitBook"];
                richServer.Document.Replace(HeatUnitBook.Range, HeatUnit);
            }
            catch { }
            try
            {
                string HeatConsumptionProject = "";
                try
                {
                    if (perm.HeatConsumptionProject != 0)
                        HeatConsumptionProject = perm.HeatConsumptionProject.ToString();
                }
                catch { }
                Bookmark EnergyEfficiencyClassFaktBook = richServer.Document.Bookmarks["EnergyEfficiencyClassFaktBook"];
                richServer.Document.Replace(EnergyEfficiencyClassFaktBook.Range, HeatConsumptionProject);
            }
            catch { }
            try
            {
                string HeatConsumptionProject = "";
                try
                {
                    if (perm.HeatConsumptionProject != 0)
                        HeatConsumptionProject = perm.HeatConsumptionProject.ToString();
                }
                catch { }
                Bookmark HeatConsumptionProjectBook = richServer.Document.Bookmarks["HeatConsumptionProjectBook"];
                richServer.Document.Replace(HeatConsumptionProjectBook.Range, HeatConsumptionProject);
            }
            catch { }
            try
            {
                string HeatConsumptionFakt = "";
                try
                {
                    if (perm.HeatConsumptionFakt != 0)
                        HeatConsumptionFakt = perm.HeatConsumptionFakt.ToString();
                }
                catch { }
                Bookmark HeatConsumptionFaktBook = richServer.Document.Bookmarks["HeatConsumptionFaktBook"];
                richServer.Document.Replace(HeatConsumptionFaktBook.Range, HeatConsumptionFakt);
            }
            catch { }

            try
            {
                string OutdoorIsolationMaterialProject = "";
                try
                {
                    OutdoorIsolationMaterialProject = perm.OutdoorIsolationMaterialProject.ToString();
                }
                catch { }
                Bookmark OutdoorIsolationMaterialProjectBook = richServer.Document.Bookmarks["OutdoorIsolationMaterialProjectBook"];
                richServer.Document.Replace(OutdoorIsolationMaterialProjectBook.Range, OutdoorIsolationMaterialProject);
            }
            catch { }
            try
            {
                string OutdoorIsolationMaterialFakt = "";
                try
                {
                    OutdoorIsolationMaterialFakt = perm.OutdoorIsolationMaterialFakt.ToString();
                }
                catch { }
                Bookmark OutdoorIsolationMaterialFaktBook = richServer.Document.Bookmarks["OutdoorIsolationMaterialFaktBook"];
                richServer.Document.Replace(OutdoorIsolationMaterialFaktBook.Range, OutdoorIsolationMaterialFakt);
            }
            catch { }
            try
            {
                string SkylightsFillingProject = "";
                try
                {
                    SkylightsFillingProject = perm.SkylightsFillingProject.ToString();
                }
                catch { }
                Bookmark SkylightsFillingProjectBook = richServer.Document.Bookmarks["SkylightsFillingProjectBook"];
                richServer.Document.Replace(SkylightsFillingProjectBook.Range, SkylightsFillingProject);
            }
            catch { }
            try
            {
                string SkylightsFillingFakt = "";
                try
                {
                    SkylightsFillingFakt = perm.SkylightsFillingFakt.ToString();
                }
                catch { }
                Bookmark SkylightsFillingFaktBook = richServer.Document.Bookmarks["SkylightsFillingFaktBook"];
                richServer.Document.Replace(SkylightsFillingFaktBook.Range, SkylightsFillingFakt);
            }
            catch { }
            #endregion

            #region ���� � ������� ��������� ������������ ������������

            string ISTextProject = "";
            string ISTextFact = "";
            // ���������� ���� �� �������
            try {
                if(perm.ElectroProject)
                ISTextProject += "����������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.HeatProject)
                ISTextProject += "��������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.WaterProject)
                ISTextProject += "�������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.HouseSeverageProject)
                ISTextProject += "������� �����������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.PhonesProject)
                ISTextProject += "�������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.SeverageProject)
                ISTextProject += "�������� �����������" + Environment.NewLine;
            }
            catch { }
            ISTextProject.TrimEnd(Environment.NewLine.ToCharArray());

            // ����������
            try
            {
                if (perm.ElectroFakt)
                    ISTextFact += "����������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.HeatFakt)
                    ISTextFact += "��������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.WaterFakt)
                    ISTextFact += "�������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.HouseSeverageFakt)
                    ISTextFact += "������� �����������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.PhonesFakt)
                    ISTextFact += "�������������" + Environment.NewLine;
            }
            catch { }
            try
            {
                if (perm.SeverageFakt)
                    ISTextFact += "�������� �����������" + Environment.NewLine;
            }
            catch { }
            ISTextFact.TrimEnd(Environment.NewLine.ToCharArray());

            string ISNotLivingProject = "";
            string ISNotLivingFakt = "";

            string ISLivingProject = "";
            string ISLivingFakt = "";

            string ISProductProject = "";
            string ISProductFakt = "";
            
            switch (perm.BuildingKind)
            {
                case AISOGD.Enums.eBuildingKind.�������:
                    ISNotLivingProject = ISTextProject;
                    ISNotLivingFakt = ISTextFact;
                    break;
                case AISOGD.Enums.eBuildingKind.�����:
                    ISLivingProject = ISTextProject;
                    ISLivingFakt = ISTextFact;
                    break;
                case AISOGD.Enums.eBuildingKind.���������������������������:
                    ISProductProject = ISTextProject;
                    ISProductFakt = ISTextFact;
                    break;
            }
            Bookmark ISNotLivingProjectBook = richServer.Document.Bookmarks["ISNotLivingProjectBook"];
            richServer.Document.Replace(ISNotLivingProjectBook.Range, ISNotLivingProject);
            Bookmark ISNotLivingFaktBook = richServer.Document.Bookmarks["ISNotLivingFaktBook"];
            richServer.Document.Replace(ISNotLivingFaktBook.Range, ISNotLivingFakt);


            Bookmark ISLivingProjectBook = richServer.Document.Bookmarks["ISLivingProjectBook"];
            richServer.Document.Replace(ISLivingProjectBook.Range, ISLivingProject);
            Bookmark ISLivingFaktBook = richServer.Document.Bookmarks["ISLivingFaktBook"];
            richServer.Document.Replace(ISLivingFaktBook.Range, ISLivingFakt);

            Bookmark ISProductProjectBook = richServer.Document.Bookmarks["ISProductProjectBook"];
            richServer.Document.Replace(ISProductProjectBook.Range, ISProductProject);
            Bookmark ISProductFaktBook = richServer.Document.Bookmarks["ISProductFaktBook"];
            richServer.Document.Replace(ISProductFaktBook.Range, ISProductFakt);

            #endregion


            #endregion

            // ���������� �� ��������
            //try
            //{
            //    string TechPlanInfo = "";
            //    try
            //    {
            //        TechPlanInfo = perm.TechPlansInfo;
            //    }
            //    catch { }
            //    Bookmark TechPlanDateBook = richServer.Document.Bookmarks["TechPlanInfoBook"];
            //    richServer.Document.Replace(TechPlanDateBook.Range, TechPlanInfo);
            //}
            //catch { }
            try
            {
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = perm.TechPlansInfo.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;

                while (i < wordscount && (text1 + words[i]).Length < 60)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark TechPlanInfoBook1 = richServer.Document.Bookmarks["TechPlanInfoBook1"];
                richServer.Document.Replace(TechPlanInfoBook1.Range, text1.TrimEnd());
                Bookmark TechPlanInfoBook2 = richServer.Document.Bookmarks["TechPlanInfoBook2"];
                richServer.Document.Replace(TechPlanInfoBook2.Range, text2.TrimEnd());

            }
            catch { }

            // ���� ���� ������ �� ������� - ����� ���� ������, ���� ��� - �� ���������� ���� � ��������� ���
            // ��������� �� ������ - ������ 27 ��������, ��������� �� 46 �������� 
            try
            {
                string TechPlanDate = "";
                //try
                //{
                //    TechPlanDate = perm.TechPlan.TechPlanDate.ToShortDateString();
                //}
                //catch { }
                Bookmark TechPlanDateBook = richServer.Document.Bookmarks["TechPlanDateBook"];
                richServer.Document.Replace(TechPlanDateBook.Range, TechPlanDate);
            }
            catch { }
            try
            {
                string CadEngineerFIO = "";
                //try
                //{
                //    CadEngineerFIO = perm.TechPlan.CadEngineer.FIO;
                //}
                //catch { }
                Bookmark CadEngineerFIOBook = richServer.Document.Bookmarks["CadEngineerFIOBook"];
                richServer.Document.Replace(CadEngineerFIOBook.Range, CadEngineerFIO);
            }
            catch { }
            try
            {
                string CadEngineerNCertificate = "";
                //try
                //{
                //    CadEngineerNCertificate = perm.TechPlan.CadEngineer.NCertificate;
                //}
                //catch { }
                Bookmark CadEngineerNCertificateBook = richServer.Document.Bookmarks["CadEngineerNCertificateBook"];
                richServer.Document.Replace(CadEngineerNCertificateBook.Range, CadEngineerNCertificate);
            }
            catch { }
            try
            {
                string CadEngineerNCertificateDate = "";
                //try
                //{
                //    CadEngineerNCertificateDate = perm.TechPlan.CadEngineer.NCertificateDate.ToShortDateString();
                //}
                //catch { }
                Bookmark CadEngineerNCertificateDateBook = richServer.Document.Bookmarks["CadEngineerNCertificateDateBook"];
                richServer.Document.Replace(CadEngineerNCertificateDateBook.Range, CadEngineerNCertificateDate);
            }
            catch { }
            try
            {
                string CadEngineerAuthority = "";
                //try
                //{
                //    CadEngineerAuthority = perm.TechPlan.CadEngineer.Authority;
                //}
                //catch { }
                Bookmark CadEngineerAuthorityBook = richServer.Document.Bookmarks["CadEngineerAuthorityBook"];
                richServer.Document.Replace(CadEngineerAuthorityBook.Range, CadEngineerAuthority);
            }
            catch { }
            try
            {
                string CadEngineerEntryDataDate = "";
                //try
                //{
                //    CadEngineerEntryDataDate = perm.TechPlan.CadEngineer.EntryDataDate.ToShortDateString();
                //}
                //catch { }
                Bookmark CadEngineerEntryDataDateBook = richServer.Document.Bookmarks["CadEngineerEntryDataDateBook"];
                richServer.Document.Replace(CadEngineerEntryDataDateBook.Range, CadEngineerEntryDataDate);
            }
            catch { }
           

            // ������������� ����
            try
            {
                string SignerCapacity = "";
                try
                {
                    SignerCapacity = perm.signer.capacity;
                }
                catch { }
                Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
            }
            catch { }
            try
            {
                string SignerName = "";
                try
                {
                    SignerName = perm.signer.name;
                }
                catch { }
                Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                richServer.Document.Replace(SignerNameBook.Range, SignerName);
            }
            catch { }


            // ����
            try
            {
                string DayDate = "";
                try
                {
                    string d = DateTime.Now.Day.ToString();
                    if (d.Length == 1)
                        d = "0" + d;
                    DayDate = d;
                }
                catch { }
                Bookmark DayDateBook = richServer.Document.Bookmarks["DayDateBook"];
                richServer.Document.Replace(DayDateBook.Range, DayDate);
            }
            catch { }

            try
            {
                string MonthDate = "";
                try
                {
                    switch (DateTime.Now.Month)
                    {
                        case 1:
                            MonthDate = "������";
                            break;
                        case 2:
                            MonthDate = "�������";
                            break;
                        case 3:
                            MonthDate = "�����";
                            break;
                        case 4:
                            MonthDate = "������";
                            break;
                        case 5:
                            MonthDate = "���";
                            break;
                        case 6:
                            MonthDate = "����";
                            break;
                        case 7:
                            MonthDate = "����";
                            break;
                        case 8:
                            MonthDate = "�������";
                            break;
                        case 9:
                            MonthDate = "��������";
                            break;
                        case 10:
                            MonthDate = "�������";
                            break;
                        case 11:
                            MonthDate = "������";
                            break;
                        case 12:
                            MonthDate = "�������";
                            break;
                        default:
                            MonthDate = DateTime.Now.Month.ToString();
                            break;
                    }
                }
                catch { }
                Bookmark MonthDateBook = richServer.Document.Bookmarks["MonthDateBook"];
                richServer.Document.Replace(MonthDateBook.Range, MonthDate);
            }
            catch { }
            try
            {
                string YearDate = "";
                try { YearDate = DateTime.Now.Year.ToString().Substring(2); }
                catch { }
                Bookmark YearDateBook = richServer.Document.Bookmarks["YearDateBook"];
                richServer.Document.Replace(YearDateBook.Range, YearDate);
            }
            catch { }

            //string path = Path.GetTempPath() + @"\UsePerm_" + perm.Oid.ToString() + "_" +
            //    DateTime.Now.Date.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            string permInfo = "";
            try { permInfo = String.Format($"� {perm.FullDocNo} �� {perm.DocDate.ToShortDateString()}"); }
            catch { }
            string path = Path.GetTempPath() + @"\���������� �� ����_" + permInfo + "_" +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";


            richServer.SaveDocument(path, DocumentFormat.Rtf);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }
    }
}
