﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VologdaIsogd.Module.Controllers.Perm
{
    public partial class SetDatesForPermReportsForm : XtraForm
    {
        public SetDatesForPermReportsForm()
        {
            InitializeComponent();
            // заполняем данные по дате начала отчета (первый день текущего месяца) и дате окончания отчета - текущая дата
            startDateEdit.DateTime = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);
            finishDateEdit.DateTime = DateTime.Now.Date;
        }
    }
}
