﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Perm;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Constr;
using AISOGD.DocFlow;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Subject;
using AISOGD.Land;
using VologdaIsogd.Module.Controllers.Delo;
using AISOGD.General;
using DevExpress.ExpressApp.Win;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ChangeStatusesPermController : ViewController
    {
        private ChoiceActionItem permAcceptAction;
        private ChoiceActionItem permProlongueAction;
        private ChoiceActionItem permInsteadAction;
        private ChoiceActionItem permRefuseAction; 
        private ChoiceActionItem permStopAction;

        public ChangeStatusesPermController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ChangeStatusesPermChoiceAction.Items.Clear();
            permAcceptAction =
               new ChoiceActionItem("Утверждено", null);
            ChangeStatusesPermChoiceAction.Items.Add(permAcceptAction);

            permProlongueAction =
               new ChoiceActionItem("Продление", null);
            ChangeStatusesPermChoiceAction.Items.Add(permProlongueAction);

            permInsteadAction =
               new ChoiceActionItem("Взамен", null);
            ChangeStatusesPermChoiceAction.Items.Add(permInsteadAction);

            permRefuseAction =
               new ChoiceActionItem("Отказ", null);
            ChangeStatusesPermChoiceAction.Items.Add(permRefuseAction);

            permStopAction =
               new ChoiceActionItem("Прекращение делопроизводства", null);
            ChangeStatusesPermChoiceAction.Items.Add(permStopAction);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        // обновить все окна
        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }

        /// <summary>
        /// Изменить статус документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeStatusesPermChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            ConstrPerm perm;
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            perm = os.FindObject<ConstrPerm>(new BinaryOperator("Oid", x_id));

            if (perm != null)
            {
                if (e.SelectedChoiceActionItem == permAcceptAction)
                {
                    ConstrPermAccept(os, connect, perm);
                }
                if (e.SelectedChoiceActionItem == permProlongueAction)
                {
                    ConstrPermProlongue(os, connect, perm, e);
                }
                if (e.SelectedChoiceActionItem == permInsteadAction)
                {
                    ConstrPermInstead(os, connect, perm, e);
                }
                if (e.SelectedChoiceActionItem == permRefuseAction)
                {
                    ConstrPermRefuse(os, connect, perm);
                }
                if (e.SelectedChoiceActionItem == permStopAction)
                {
                    StopPermWork(os, connect, perm);
                }
            }
        }

        /// <summary>
        /// Утверждение разрешения на строительство
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConstrPermAccept(IObjectSpace os, Connect connect, ConstrPerm constrPerm)
        {

            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            SynhronizePermDataController SynhronizePermDataController = new SynhronizePermDataController();
            if (constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.Отказ || constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.ОжиданиеУтвержденияВзамен ||
                constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.ПрекращениеДелопроизводства || constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.УтратилоДействие)
            {
                XtraMessageBox.Show("Документ с данным статусом нельзя утвердить!");
                return;
            }
            if (constrPerm.DocDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Не указана дата документа");
                return;
            }
            if (constrPerm.DocNo == null || constrPerm.DocNo == "")
            {
                XtraMessageBox.Show("Необходимо указать корректный номер документа");
                return;
            }
            ConstrPerm anotherPerm = connect.FindFirstObject<ConstrPerm>(x => x.DocNo == constrPerm.DocNo && x.DocDate == constrPerm.DocDate);
            if (constrPerm.Oid.ToString() != anotherPerm.Oid.ToString())
            {
                XtraMessageBox.Show("В Системе уже внесен другой документ с подобными реквизитам.");
                return;
            }

            //if (constrPerm.DocNo.Contains("35-32357000-         -"))
            //{
            //    XtraMessageBox.Show("Необходимо указать корректный номер документа");
            //    return;
            //}
            constrPerm.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден;

            try
            {
                constrPerm.FullDocNo = constrPerm.DocKind.Prefix + constrPerm.DocNo + constrPerm.DocKind.Suffix + constrPerm.DocDate.Year.ToString();
            }
            catch { }
            string temp = "";
            if (constrPerm.PermKind == AISOGD.Enums.ePermKind.Взамен)
            {
                temp = " (взамен)";
                try { constrPerm.ConstrPermInstead.EDocStatus = AISOGD.Enums.eDocStatus.УтратилоДействие; }
                catch { }
            }
            string docNo = constrPerm.DocNo;
            if (docNo.Length == 1)
                docNo = "00" + docNo;
            if (docNo.Length == 2)
                docNo = "0" + docNo;
            // проставляем номер на карте для объекта строительства
            if (constrPerm.PermKind != AISOGD.Enums.ePermKind.Взамен)
            {
                try
                {
                    if (constrPerm.CapitalStructureBase[0].MapNo == null || constrPerm.CapitalStructureBase[0].MapNo == "")
                        constrPerm.CapitalStructureBase[0].MapNo = constrPerm.DocDate.Year.ToString().Remove(0, 2) + docNo;//.Replace("35-32357000-", "").Split('-')[0];
                }
                catch { }
            }

            // лист этапов для заполнения потом проектных характеристик в ОКС по этим этапам
            List<ConstrStage> ConstrStageList = new List<ConstrStage>();
            foreach (ConstrStage stage in constrPerm.ConstrStages)
            {
                ConstrStageList.Add(stage);
                stage.ConstrStageState = AISOGD.Enums.eConstrStageState.Стройка;
                if (stage.MapNo == null || stage.MapNo == "")
                {
                    try { stage.MapNo = constrPerm.DocDate.Year.ToString().Remove(0, 2) + docNo; }//.Replace("35-32357000-", "").Split('-')[0]; }
                    catch { }

                    try
                    {
                        if (constrPerm.ConstrStages.Count > 1)
                            stage.MapNo += "_" + stage.StageNo;
                    }
                    catch { }
                }
                try
                {
                    string constrPermInfo = String.Format("от {0} №{1}{2}", constrPerm.DocDate.ToShortDateString(), constrPerm.DocNo, temp);

                    if (stage.ConstrPermInfo == null || stage.ConstrPermInfo == "")
                        stage.ConstrPermInfo = constrPermInfo;
                    else
                        if (!stage.ConstrPermInfo.Contains(constrPermInfo))
                        stage.ConstrPermInfo += ", " + constrPermInfo;
                }
                catch { }
                // проставляем дату последнего РС в этапе
                try
                {
                    stage.ConstrPermLastDate = constrPerm.DocDate;
                }
                catch { }
                // проставляем год планируемого ввода
                try
                {
                    stage.PlaningYearInUse = constrPerm.ValidDateTotal.Year;
                }
                catch { }
                stage.Save();
                // заполняем семантику
                // из таблицы связи получаем Ид реестрового объекта ЗУ
                SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", stage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                if (objLink != null)
                {
                    try
                    {
                        SynhronizePermDataController.SetConstrStageSemantic(m_MapInfo, stage, constrPerm, objLink.SpatialLayerId, objLink.SpatialObjectId);
                    }
                    catch { }
                }
            }
            os.CommitChanges();
            // проставляем номер на карте для РС
            try
            {
                if (constrPerm.MapNo == null || constrPerm.MapNo == "")
                    constrPerm.MapNo = constrPerm.CapitalStructureBase[0].MapNo;
            }
            catch { }
            foreach (CapitalStructureBase constrObj in constrPerm.CapitalStructureBase)
            {
                constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.Стройка;
                try
                {
                    string constrPermInfo = String.Format("от {0} №{1}{2}", constrPerm.DocDate.ToShortDateString(), constrPerm.DocNo, temp);
                    if (constrObj.ConstrPermInfo == null || constrObj.ConstrPermInfo == "")
                        constrObj.ConstrPermInfo = constrPermInfo;
                    else
                        if (!constrObj.ConstrPermInfo.Contains(constrPermInfo))
                        constrObj.ConstrPermInfo += ", " + constrPermInfo;
                }
                catch { }
                // заполняем дату начала строительства, если это первое разрешение на РС
                if (constrObj.StartConstrDate == DateTime.MinValue)
                    constrObj.StartConstrDate = constrPerm.DocDate;
                // проставляем дату последнего РС в объекте строительства
                try
                {
                    constrObj.ConstrPermLastDate = constrPerm.DocDate;
                }
                catch { }
                // проставляем месяц и год планируемого ввода
                try
                {
                    constrObj.PlaningYearInUse = constrPerm.ValidDateTotal.Year;
                }
                catch { }
                try
                {
                    switch (constrPerm.ValidDateTotal.Month)
                    {
                        case 1:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Январь;
                            break;
                        case 2:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Февраль;
                            break;
                        case 3:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Март;
                            break;
                        case 4:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Апрель;
                            break;
                        case 5:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Май;
                            break;
                        case 6:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Июнь;
                            break;
                        case 7:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Июль;
                            break;
                        case 8:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Август;
                            break;
                        case 9:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Сентябрь;
                            break;
                        case 10:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Октябрь;
                            break;
                        case 11:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Ноябрь;
                            break;
                        case 12:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Декабрь;
                            break;

                    }
                }
                catch { }
                // заполняем данные из разрешения по ОКС (по застройщикам и виду работ), если еще не заполнили
                try
                {
                    foreach (GeneralSubject subject in constrPerm.DocSubjects)
                    {
                        constrObj.DocSubjects.Add(subject);
                    }
                }
                catch { }
                try { constrObj.WorkKind = constrPerm.WorkKind; }
                catch { }

                // заполняем данные по проектным характеристикам
                //SetConstrObjProjectInfo(constrObj, connect);
                constrObj.Save();
            }
            foreach (InLetter letter in constrPerm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            constrPerm.Save();
            os.CommitChanges();
            // создаем документ для регистрации в ИСОГД 
            try
            {

                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = constrPerm.FullDocNo; }
                catch { }

                try { idocDate = constrPerm.DocDate; }
                catch { }

                try { icadNo = constrPerm.LotCadNo; }
                catch { }

                try { iaddrInfo = constrPerm.ObjectAddress; }
                catch { }
                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in constrPerm.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }
                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "060600", "Разрешение на строительство", icadNo, iaddrInfo, attachs);
                // "060100", "Градостроительный план  земельного участка"
                // 

            }
            catch { }
            os.CommitChanges();
            RefreshAllWindows();
        }

        /// <summary>
        /// Продление разрешения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConstrPermProlongue(IObjectSpace os, Connect connect, ConstrPerm constrPerm, SingleChoiceActionExecuteEventArgs e)
        {
            if (constrPerm.EDocStatus != AISOGD.Enums.eDocStatus.Утвержден &&
                //constrPerm.EDocStatus != AISOGD.Enums.eDocStatus.Продление &&
                constrPerm.EDocStatus != AISOGD.Enums.eDocStatus.УтратилоДействиеПоСроку)
            {
                XtraMessageBox.Show("Продлить можно только документ со статусом 'Утвержден' или 'Утратил действие по сроку'!");
                return;
            }

            PermProlongue permProlongue = connect.CreateObject<PermProlongue>();
            permProlongue.ConstrPerm = constrPerm;
            permProlongue.DateProlongue = DateTime.Now.Date;
            constrPerm.PermKind = AISOGD.Enums.ePermKind.Продление;
            foreach (InLetter letter in constrPerm.InLetter)
            {
                if (letter.InLetterStatus == AISOGD.Enums.eInLetterStatus.Вработе)
                {
                    letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                    letter.Save();
                }
            }
            constrPerm.Save();
            permProlongue.Save();
            os.CommitChanges();
            RefreshAllWindows();
            // Открываем окно продления
            DetailView dv = Application.CreateDetailView(os, permProlongue);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;

            View.ObjectSpace.CommitChanges();

        }

        
        /// <summary>
        /// Создание разрешение взамен
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConstrPermInstead(IObjectSpace os, Connect connect, ConstrPerm constrPermOld, SingleChoiceActionExecuteEventArgs e)
        {
            ConstrPerm instead = connect.FindFirstObject<ConstrPerm>(x => x.ConstrPermInstead.Oid.ToString() == constrPermOld.Oid.ToString());
            if (instead != null)
            {
                XtraMessageBox.Show("У данного разрешения уже было подготовлено разрешение взамен (этого разрешения)! Подготовка еще одного разрешения взамен невозможна!");
                return;
            }

            if (constrPermOld.EDocStatus != AISOGD.Enums.eDocStatus.Утвержден &&
                constrPermOld.EDocStatus != AISOGD.Enums.eDocStatus.УтратилоДействиеПоСроку)
            {
                XtraMessageBox.Show("Подготовить разрешение взамен можно только у документа со статусом 'Утвержден' или 'Утратил действие по сроку'!");
                return;
            }

            ConstrPerm constrPermNew = connect.CreateObject<ConstrPerm>();
            try { constrPermNew.BuildingCategory = constrPermOld.BuildingCategory; }
            catch { }
            try { constrPermNew.MapNo = constrPermOld.MapNo; }
            catch { }
            try { constrPermNew.ConstructionType = constrPermOld.ConstructionType; }
            catch { }
            try
            {
                constrPermNew.BuildingSizeProject = constrPermOld.BuildingSizeProject;
            }
            catch { }
            try
            {
                constrPermNew.BuildingSizeUnderGroundPartProject = constrPermOld.BuildingSizeUnderGroundPartProject;
            }
            catch { }
            try
            {
                constrPermNew.BuildSquare = constrPermOld.BuildSquare;
            }
            catch { }
            try
            {
                constrPermNew.CadKvartalNo = constrPermOld.CadKvartalNo;
            }
            catch { }
            try
            {
                constrPermNew.Capacity = constrPermOld.Capacity;
            }
            catch { }
            try
            {
                foreach (CapitalStructureBase constrObj in constrPermOld.CapitalStructureBase)
                {
                    constrPermNew.CapitalStructureBase.Add(constrObj);
                }
            }
            catch { }
            try
            {
                constrPermNew.ConstrDeveloperString = constrPermOld.ConstrDeveloperString;
            }
            catch { }
            try
            {
                foreach (ConstrStage constrObj in constrPermOld.ConstrStages)
                {
                    constrPermNew.ConstrStages.Add(constrObj);
                    constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;
                    constrObj.Save();
                }
            }
            catch { }
            try
            {
                constrPermNew.ConstructiveElementsInfoProject = constrPermOld.ConstructiveElementsInfoProject;
            }
            catch { }

            //constrPermNew.DocKind = constrPermOld.DocKind;

            foreach (GeneralSubject constrSubj in constrPermOld.DocSubjects)
            {
                constrPermNew.DocSubjects.Add(constrSubj);
            }

            try
            {
                constrPermNew.ElectricLinesInfoProject = constrPermOld.ElectricLinesInfoProject;
            }
            catch { }
            try
            {
                constrPermNew.ExpertInfo = constrPermOld.ExpertInfo;
            }
            catch { }
            try
            {
                constrPermNew.FloorCount = constrPermOld.FloorCount;
            }
            catch { }
            try
            {
                constrPermNew.GPZU = constrPermOld.GPZU;
            }
            catch { }
            try
            {
                constrPermNew.Height = constrPermOld.Height;
            }
            catch { }
            try
            {
                constrPermNew.LengthProject = constrPermOld.LengthProject;
            }
            catch { }
            try
            {
                constrPermNew.LineObjectClass = constrPermOld.LineObjectClass;
            }
            catch { }
            try
            {
                constrPermNew.LotCadNo = constrPermOld.LotCadNo;
            }
            catch { }
            try
            {
                constrPermNew.LotSquare = constrPermOld.LotSquare;
            }
            catch { }

            try
            {
                constrPermNew.ObjChangeCadNo = constrPermOld.ObjChangeCadNo;
            }
            catch { }
            try
            {
                constrPermNew.ObjectAddress = constrPermOld.ObjectAddress;
            }
            catch { }
            //try
            //{
            //    foreach (ObjectConstractionInfo objInfo in constrPermOld.ObjectConstractionInfo)
            //    {
            //        constrPermNew.ObjectConstractionInfo.Add(objInfo);
            //    }
            //}
            //catch { }
            try
            {
                constrPermNew.ObjectEstateComplexName = constrPermOld.ObjectEstateComplexName;
            }
            catch { }

            try
            {
                constrPermNew.ObjectName = constrPermOld.ObjectName;
            }
            catch { }
            try
            {
                constrPermNew.OKSOtherIndicators = constrPermOld.OKSOtherIndicators;
            }
            catch { }
            try
            {
                constrPermNew.OrgExpertName = constrPermOld.OrgExpertName;
            }
            catch { }
            try
            {
                constrPermNew.OtherIndicatorsLineProject = constrPermOld.OtherIndicatorsLineProject;
            }
            catch { }

            try
            {
                foreach (Parcel p in constrPermOld.Parcels)
                {
                    constrPermNew.Parcels.Add(p);
                }
            }
            catch { }
            try
            {
                constrPermNew.PlanningDocInfo = constrPermOld.PlanningDocInfo;
            }
            catch { }
            try
            {
                constrPermNew.PowerLineProject = constrPermOld.PowerLineProject;
            }
            catch { }
            try
            {
                constrPermNew.ProjectDocInfo = constrPermOld.ProjectDocInfo;
            }
            catch { }
            try
            {
                constrPermNew.TotalBuildSquareProject = constrPermOld.TotalBuildSquareProject;
            }
            catch { }
            try
            {
                constrPermNew.TotalLivingSpaceProject = constrPermOld.TotalLivingSpaceProject;
            }
            catch { }
            try
            {
                constrPermNew.UnderGroundFloorCount = constrPermOld.UnderGroundFloorCount;
            }
            catch { }

            try
            {
                constrPermNew.WorkKind = constrPermOld.WorkKind;
            }
            catch { }
            constrPermNew.EDocStatus = AISOGD.Enums.eDocStatus.Подготовка;
            constrPermNew.PermKind = AISOGD.Enums.ePermKind.Взамен;
            constrPermNew.ConstrPermInstead = constrPermOld;
            constrPermNew.Save();
            constrPermOld.EDocStatus = AISOGD.Enums.eDocStatus.ОжиданиеУтвержденияВзамен;
            constrPermOld.Save();

            os.CommitChanges();
            RefreshAllWindows();
            // Открываем окно нового разрешения
            DetailView dv = Application.CreateDetailView(os, constrPermNew);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
        }


        /// <summary>
        /// Отказ по разрешению
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConstrPermRefuse(IObjectSpace os, Connect connect, ConstrPerm constrPerm)
        {
            SetPermRefuse(constrPerm, os, connect, "Отказ");
        }

        
        /// <summary>
        /// Прекращение делопроизводства
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopPermWork(IObjectSpace os, Connect connect, ConstrPerm constrPerm)
        {
            SetPermRefuse(constrPerm, os, connect, "Прекращение делопроизводства");
        }

        private void SetPermRefuse(ConstrPerm constrPerm, IObjectSpace os, Connect connect, string reason)
        {
            
            if (constrPerm.EDocStatus != AISOGD.Enums.eDocStatus.Подготовка)
            {
                XtraMessageBox.Show("Можно отменить только документ со статусом 'Подготовка'!");
                return;
            }
            if(reason == "Отказ")
                constrPerm.EDocStatus = AISOGD.Enums.eDocStatus.Отказ;
            if (reason == "Прекращение делопроизводства")
                constrPerm.EDocStatus = AISOGD.Enums.eDocStatus.ПрекращениеДелопроизводства;
            if (constrPerm.PermKind == AISOGD.Enums.ePermKind.Взамен)
            {
                try { constrPerm.ConstrPermInstead.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден; }
                catch { }
            }
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            foreach (ConstrStage stage in constrPerm.ConstrStages)
            {
                stage.ConstrStageState = AISOGD.Enums.eConstrStageState.Отмена;
                stage.Save();
                try
                {
                    SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", stage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                    m_MapInfo.Do("Select * From " + objLink.SpatialLayerId + " Where MI_PRINX = " + objLink.SpatialObjectId);
                    string mapNo = "";
                    if (stage.MapNo != null)
                        mapNo = stage.MapNo;
                    m_MapInfo.Do("Update Selection Set НОМЕР_НА_СХЕМЕ =  \"" + mapNo + "_отказ" + "\" Commit table " + objLink.SpatialLayerId);
                    try
                    {
                        m_MapInfo.Do("Update Selection Set СТАТУС =  \"" + stage.ConstrStageState + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                }
                catch { }
                //try
                //{
                // переносим связанные ГИС объекты в архивный слой
                //if (m_MapInfo.mapinfo != null)
                //{
                //    var spatial = new SpatialController<GisLayer>(connect);

                //    // получаем слой и MI_PRINX стадии
                //    foreach (SpatialRepository sprepo in spatial.GetSpatialLinks(stage.ClassInfo.FullName, stage.Oid.ToString()))
                //    {
                //        string layer = sprepo.SpatialLayerId;
                //        string mapObjId = sprepo.SpatialObjectId;

                //        TableInfo stageTableInfo = new TableInfo(m_MapInfo, layer);
                //        //получить слой для архивных объектов
                //        TableInfo archiveTableInfo = new TableInfo(m_MapInfo, ConstrStageHelper.GetArchiveLayerName(ObjectSpace));

                //        if (!archiveTableInfo.isTableExist)
                //        {
                //            XtraMessageBox.Show("Не обнаружена таблица " + archiveTableInfo.Name);
                //            return;
                //        }

                //        // помещаем не архивный объект стадии во временную таблицу, чтобы его потом удалить 
                //        TableInfoTemp startSelectObject = new TableInfoTemp(m_MapInfo, ETempTableType.Temp);
                //        m_MapInfo.Do(String.Format("Select * From {0} Where MI_PRINX = {1} into {2}", layer, mapObjId, startSelectObject.Name));
                //        //пространственный объект стадии
                //        SpatialObject spObject = new SpatialObject(m_MapInfo, layer, mapObjId);
                //        // клонируем его
                //        string newObjectRowID = spObject.CloneSpatialObject(archiveTableInfo.Name);
                //        // удаляем исходный объект
                //        m_MapInfo.Do(string.Format("Delete From {0} Where RowID = {1}", startSelectObject.Name, 0));
                //        stageTableInfo.CommitChanges();
                //    }
                //}
                //}
                //catch { }
            }
            foreach (CapitalStructureBase oks in constrPerm.CapitalStructureBase)
            {
                oks.ConstrStageState = AISOGD.Enums.eConstrStageState.Отмена;
            }

            foreach (InLetter letter in constrPerm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            constrPerm.Save();
            os.CommitChanges();
            RefreshAllWindows();
        }

        /// <summary>
        /// Заполнение проектных показателей у ОКС по этапам при утверждении РС (по всем этапам со статусом стройка или введено)
        /// </summary>
        /// <param name="constrObj"></param>
        /// <param name="ConstrStageList"></param>
        /// <param name="connect"></param>
        public void SetConstrObjProjectInfo(CapitalStructureBase constrObj , Connect connect)
        {
            #region Сначала обнуляем все показатели 
            constrObj.BuildingSizeProject = 0;
            constrObj.BuildingSizeOverGroundPartProject = 0;
            constrObj.BuildingSizeUnderGroundPartProject = 0;
            constrObj.TotalBuildSquareProject = 0;
            constrObj.BuildSquareProject = 0;
            constrObj.HeightProject = 0;
            constrObj.NotLivingSquareProject = 0;
            constrObj.OutBuildingSquareProject = 0;
            constrObj.BuildingCountProject = 0;
            constrObj.ElevatorsCountProject = 0;
            constrObj.EscalatorsCountProject = 0;
            constrObj.InvalidLiftsCountProject = 0;
            constrObj.OtherIndicatorsProject = "";

            constrObj.PlacesCountProject = 0;
            constrObj.RoomCountProject = 0;
            constrObj.CapacityProject = 0;

            constrObj.LivingSpaceProject = 0;
            constrObj.TotalLivingSpaceProject = 0;
            constrObj.NotLivingSpaceProject = 0;
            constrObj.SectionCountProject = 0;
            constrObj.PorchCountProject = 0;
            constrObj.AppartmentsCountProject = 0;
            constrObj.StudioCountProject = 0;
            constrObj.StudioSpaceProject = 0;
            constrObj.OneRoomFlatCountProject = 0;
            constrObj.OneRoomFlatSpaceProject = 0;
            constrObj.TwoRoomFlatCountProject = 0;
            constrObj.TwoRoomFlatSpaceProject = 0;
            constrObj.ThreeRoomFlatCountProject = 0;
            constrObj.ThreeRoomFlatSpaceProject = 0;
            constrObj.FourRoomFlatCountProject = 0;
            constrObj.FourRoomSpaceProject = 0;
            constrObj.MoreRoomFlatCountProject = 0;
            constrObj.MoreRoomSpaceProject = 0;
            constrObj.ElitAppartmentsCountProject = 0;
            constrObj.ElitAppartmentsSpaceProject = 0;

            constrObj.PowerProject = "";
            constrObj.ProductivityProject = "";

            constrObj.LengthProject = 0;
            constrObj.PowerLineProject = "";
            constrObj.PipesInfoProject = "";
            constrObj.ElectricLinesInfoProject = "";
            constrObj.ConstructiveElementsInfoProject = "";

            constrObj.HeatConsumptionProject = 0;
            constrObj.OutdoorIsolationMaterialProject = "";
            constrObj.SkylightsFillingProject = "";
            #endregion

            //List<ConstrStage> ConstrStageList = new List<ConstrStage>();
            var ConstrStageList = connect.FindObjects<ConstrStage>(x=> x.CapitalStructureBase == constrObj && (x.ConstrStageState == AISOGD.Enums.eConstrStageState.Стройка ||
            x.ConstrStageState == AISOGD.Enums.eConstrStageState.Введено));
            char[] seps = { '/', '\\'};
            
            foreach (ConstrStage constrStage in ConstrStageList)
            {
                
                #region Общие показатели
                try { constrObj.BuildingSizeProject += constrStage.BuildingSizeProject; }
                catch { }
                try { constrObj.BuildingSizeOverGroundPartProject += constrStage.BuildingSizeOverGroundPartProject; }
                catch { }
                try { constrObj.BuildingSizeUnderGroundPartProject += constrStage.BuildingSizeUnderGroundPartProject; }
                catch { }
                try { constrObj.TotalBuildSquareProject += constrStage.TotalBuildSquareProject; }
                catch { }
                try { constrObj.BuildSquareProject += constrStage.BuildSquare; }
                catch { }
                try { constrObj.HeightProject += constrStage.Height; }
                catch { }
                try { constrObj.NotLivingSquareProject += constrStage.NotLivingSquareProject; }
                catch { }
                try { constrObj.OutBuildingSquareProject += constrStage.OutBuildingSquareProject; }
                catch { }
                try { constrObj.BuildingCountProject += constrStage.BuildingCountProject; }
                catch { }
                #endregion

                #region Лифты, Эскалаторы, подъемники для ОКС
                try { constrObj.ElevatorsCountProject += constrStage.ElevatorsCountProject; }
                catch { }
                try { constrObj.EscalatorsCountProject += constrStage.EscalatorsCountProject; }
                catch { }
                try { constrObj.InvalidLiftsCountProject += constrStage.InvalidLiftsCountProject; }
                catch { }
                #endregion

                #region Материалы
                //try {
                //    if (constrObj.FundMaterialProject == null)
                //        constrObj.FundMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.FundMaterialProject.Oid);
                //}
                //catch { }
                //try
                //{
                //    if (constrObj.WallMaterialProject == null)
                //        constrObj.WallMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.WallMaterialProject.Oid); }
                //catch { }
                //try
                //{
                //    if (constrObj.BorderMaterialProject == null)
                //        constrObj.BorderMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.BorderMaterialProject.Oid); }
                //catch { }
                //try
                //{
                //    if (constrObj.RoofMaterialProject == null)
                //        constrObj.RoofMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.RoofMaterialProject.Oid); }
                //catch { }


                #endregion

                #region Сети и системы инженерно технического обслуживания
                try {
                    if(constrStage.ElectroProject == true)
                    constrObj.ElectroProject = true; }
                catch { }
                try
                {
                    if (constrStage.HeatProject == true)
                        constrObj.HeatProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.WaterProject == true)
                        constrObj.WaterProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.GazProject == true)
                        constrObj.GazProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.HouseSeverageProject == true)
                        constrObj.HouseSeverageProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.PhonesProject == true)
                        constrObj.PhonesProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.TVProject == true)
                        constrObj.TVProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.RadioProject == true)
                        constrObj.RadioProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.SeverageProject == true)
                        constrObj.SeverageProject = true;
                }
                catch { }
                #endregion

                #region Иные показатели
                try { constrObj.OtherIndicatorsProject += constrStage.OtherIndicatorsProject + Environment.NewLine;
                    constrObj.OtherIndicatorsProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region нежилые
                try { constrObj.PlacesCountProject += constrStage.PlacesCountProject; }
                catch { }
                try { constrObj.RoomCountProject += constrStage.RoomCountProject; }
                catch { }
                try { constrObj.CapacityProject += constrStage.CapacityProject; }
                catch { }
                #endregion

                #region жилые
                try { constrObj.LivingSpaceProject += constrStage.LivingSpaceProject; }
                catch { }
                try { constrObj.TotalLivingSpaceProject += constrStage.TotalLivingSpaceProject; }
                catch { }
                try { constrObj.NotLivingSpaceProject += constrStage.NotLivingSpaceProject; }
                catch { }
                try { constrObj.SectionCountProject += constrStage.SectionCountProject; }
                catch { }
                try { constrObj.PorchCountProject += constrStage.PorchCountProject; }
                catch { }
                try { constrObj.AppartmentsCountProject += constrStage.AppartmentsCountProject; }
                catch { }

                try { constrObj.StudioCountProject += constrStage.StudioCountProject; }
                catch { }
                try { constrObj.StudioSpaceProject += constrStage.StudioSpaceProject; }
                catch { }
                try { constrObj.OneRoomFlatCountProject += constrStage.OneRoomFlatCountProject; }
                catch { }
                try { constrObj.OneRoomFlatSpaceProject += constrStage.OneRoomFlatSpaceProject; }
                catch { }
                try { constrObj.TwoRoomFlatCountProject += constrStage.TwoRoomFlatCountProject; }
                catch { }
                try { constrObj.TwoRoomFlatSpaceProject += constrStage.TwoRoomFlatSpaceProject; }
                catch { }
                try { constrObj.ThreeRoomFlatCountProject += constrStage.ThreeRoomFlatCountProject; }
                catch { }
                try { constrObj.ThreeRoomFlatSpaceProject += constrStage.ThreeRoomFlatSpaceProject; }
                catch { }
                try { constrObj.FourRoomFlatCountProject += constrStage.FourRoomFlatCountProject; }
                catch { }
                try { constrObj.FourRoomSpaceProject += constrStage.FourRoomSpaceProject; }
                catch { }
                try { constrObj.MoreRoomFlatCountProject += constrStage.MoreRoomFlatCountProject; }
                catch { }
                try { constrObj.MoreRoomSpaceProject += constrStage.MoreRoomSpaceProject; }
                catch { }
                try { constrObj.ElitAppartmentsCountProject += constrStage.ElitAppartmentsCountProject; }
                catch { }
                try { constrObj.ElitAppartmentsSpaceProject += constrStage.ElitAppartmentsSpaceProject; }
                catch { }
                #endregion

                #region Объекты производственного назначения
                try
                {
                    if (constrObj.ProductPurposeObjectType == null)
                        constrObj.ProductPurposeObjectType = connect.FindFirstObject<ProductPurposeObjectType>(x => x.Oid == constrStage.ProductPurposeObjectType.Oid);
                }
                catch { }
                try
                {
                    //constrObj.PowerProject = "";
                    constrObj.PowerProject += constrStage.PowerProject + Environment.NewLine;
                    constrObj.PowerProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    constrObj.ProductivityProject += constrStage.ProductivityProject + Environment.NewLine;
                    constrObj.ProductivityProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region  Линейные объекты
                try {
                    if (constrObj.LineObjectClass == null)
                        constrObj.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid == constrStage.LineObjectClass.Oid); }
                catch { }
                try { constrObj.LengthProject += constrStage.LengthProject; }
                catch { }
                try
                {
                    
                    constrObj.PowerLineProject += constrStage.PowerLineProject + Environment.NewLine;
                    constrObj.PowerLineProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    
                    constrObj.PipesInfoProject += constrStage.PipesInfoProject + Environment.NewLine;
                    constrObj.PipesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    
                    constrObj.ElectricLinesInfoProject += constrStage.ElectricLinesInfoProject + Environment.NewLine;
                    constrObj.ElectricLinesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    
                    constrObj.ConstructiveElementsInfoProject += constrStage.ConstructiveElementsInfoProject + Environment.NewLine;
                    constrObj.ConstructiveElementsInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region Соответствие требованиям энергетической эффективности
                try
                {
                    if (constrObj.EnergyEfficiencyClassProject == null)
                        constrObj.EnergyEfficiencyClassProject = connect.FindFirstObject<EnergyEfficiencyClass>(x => x.Oid == constrStage.EnergyEfficiencyClassProject.Oid);
                }
                catch { }
                try
                {
                    if (constrObj.HeatUnit == null)
                        constrObj.HeatUnit = connect.FindFirstObject<dHeatUnit>(x => x.Oid == constrStage.HeatUnit.Oid);
                }
                catch { }

                try { constrObj.HeatConsumptionProject += constrStage.HeatConsumptionProject; }
                catch { }
                try
                {
                    
                    constrObj.OutdoorIsolationMaterialProject += constrStage.OutdoorIsolationMaterialProject + Environment.NewLine;
                    constrObj.OutdoorIsolationMaterialProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    
                    constrObj.SkylightsFillingProject += constrStage.SkylightsFillingProject + Environment.NewLine;
                    constrObj.SkylightsFillingProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion
            }
        }

        
    }
}
