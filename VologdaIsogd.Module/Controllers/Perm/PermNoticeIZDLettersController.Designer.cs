﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class PermNoticeIZDLettersController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PermNoticeIZDLettersAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PermNoticeIZDLettersAction
            // 
            this.PermNoticeIZDLettersAction.Caption = "Письма";
            this.PermNoticeIZDLettersAction.ConfirmationMessage = null;
            this.PermNoticeIZDLettersAction.Id = "PermNoticeIZDLettersAction";
            this.PermNoticeIZDLettersAction.ToolTip = "Формирование сопроводительных писем";
            this.PermNoticeIZDLettersAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PermNoticeIZDLettersAction_Execute);
            // 
            // PermNoticeIZDLettersController
            // 
            this.Actions.Add(this.PermNoticeIZDLettersAction);
            this.TargetObjectType = typeof(AISOGD.Perm.PermNoticeIZD);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PermNoticeIZDLettersAction;
    }
}
