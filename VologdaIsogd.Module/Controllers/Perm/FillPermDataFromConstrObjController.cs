﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Constr;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using AISOGD.Perm;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Win.Layout;
using DevExpress.ExpressApp.Win;
using AISOGD.Subject;
using AISOGD.Address;
using AISOGD.DocFlow;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FillPermDataFromConstrObjController : ViewController
    {
        public FillPermDataFromConstrObjController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        protected WinWindow FindWindowByView(DevExpress.ExpressApp.View view)
        {
            ViewShortcut viewShortcut = view.CreateShortcut();
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.View.CreateShortcut() == viewShortcut) {
                    return existingWindow;
                }
            }
            return null;
        }

        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }

        /// <summary>
        /// Заполнить данные по разрешению на строительство и этапам из объекта строительства
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FillPermDataFromConstrAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();


            CapitalStructureBase constrObject;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            constrObject = os.FindObject<CapitalStructureBase>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)constrObject.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            foreach(GeneralDocBase generalDocBase in constrObject.generalDocBase)
            {
                ConstrPerm constrPerm = null;

                if (generalDocBase.ClassInfo.FullName == "AISOGD.Perm.ConstrPerm")
                {
                    constrPerm = generalDocBase as ConstrPerm;
                    try { constrPerm.ConstructionType = constrObject.ConstructionType; }
                    catch { }
                    try { constrPerm.BuildingKind = constrObject.BuildingKind; }
                    catch { }
                    if (constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.Подготовка)
                    {
                        //try { constrPerm.ObjectAddress = constrObject.Address; }
                        //catch { }

                        try
                        {
                            if (constrObject.BuildingKind == AISOGD.Enums.eBuildingKind.Линейные)
                                constrPerm.BuildingCategory = AISOGD.Enums.eBuildingCategory.ЛинейныйОбъект;
                        }
                        catch { }
                        // заполняем данные из разрешения по ОКС (по застройщикам и виду работ)
                        try
                        {
                            foreach (GeneralSubject subject in constrPerm.DocSubjects)
                            {
                                constrObject.DocSubjects.Add(subject);
                            }
                        }
                        catch { }
                        try { constrObject.WorkKind = constrPerm.WorkKind; }
                        catch { }

                        //обновляем данные в родительском окне разрешения
                        //DetailView dv = Application.CreateDetailView(os, constrPerm);
                        ////dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                        ////e.ShowViewParameters.CreatedView = dv;

                        //FindWindowByView(dv).GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                    }
                    constrObject.Save();
                    constrPerm.Save();
                    
                }
                UsePerm usePerm = null;
                if (generalDocBase.ClassInfo.FullName == "AISOGD.Perm.UsePerm")
                {
                    usePerm = generalDocBase as UsePerm;
                    try { usePerm.BuildingKind = constrObject.BuildingKind; }
                    catch { }
                    usePerm.Save();
                }
                os.CommitChanges();
            }
            
            foreach (ConstrStage constrStage in constrObject.ConstrStages)
            {
                if (constrStage.ConstrStageState == AISOGD.Enums.eConstrStageState.ПодготовкаРС)
                {
                    //try { constrStage.Address = constrObject.Address; }
                    //catch { }
                    //try { constrStage.ConstrAddress = constrObject.ConstrAddress; }
                    //catch { }
                    try { constrStage.ConstructionType = constrObject.ConstructionType; }
                    catch { }
                    try { constrStage.BuildingKind = constrObject.BuildingKind; }
                    catch { }
                    //try { constrStage.FloorCount = constrObject.FloorCountProject; }
                    //catch { }
                    //try { constrStage.FloorCountAbove = constrObject.FloorCountAboveProject; }
                    //catch { }
                    //try { constrStage.UnderGroundFloorCount = constrObject.UnderGroundFloorCounProject; }
                    //catch { }
                    //try { constrStage.FloorCountDiff = constrObject.FloorCountDiffProject; }
                    //catch { }
                    //try { constrStage.FloorsNote = constrObject.FloorsNoteProject; }
                    //catch { }
                    //try { constrStage.ConstrMaterial = constrObject.ConstrMaterial; }
                    //catch { }
                    //try { constrStage.ConstrFireDangerClass = constrObject.ConstrFireDangerClass; }
                    //catch { }
                    //try { constrStage.ConstrFireShield = constrObject.ConstrFireShield; }
                    //catch { }
                    try { constrStage.City = connect.FindFirstObject<City>(x=> x.Oid == constrObject.City.Oid);}
                    catch { }
                    try { constrStage.PlaningYearInUse = constrObject.PlaningYearInUse; }
                    catch { }
                    
                    constrStage.Save();
                }
            }
            os.CommitChanges();
            RefreshAllWindows();
            //os.Refresh();
            //View.Close();
            //XtraMessageBox.Show("Данные заполнены");
        }


        private void FillConstrObjectDataFromStagesAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();


            CapitalStructureBase constrObject;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            constrObject = os.FindObject<CapitalStructureBase>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)constrObject.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            FillConstrObjectDataFromStages(constrObject, connect, os);

            //обновляем данные в родительском окне объекта строительства
            //DetailView dv = Application.CreateDetailView(os, constrObject);
            //dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            //e.ShowViewParameters.CreatedView = dv;

            

            //foreach(ConstrStage stage in constrObject.ConstrStages)
            //{
            //    DetailView dv1 = Application.CreateDetailView(os, stage);
            //    dv1.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            //    e.ShowViewParameters.CreatedView = dv1;
            //    FindWindowByView(dv1).GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
            //}
            //foreach (ConstrPerm perm in constrObject.generalDocBase)
            //{
            //    DetailView dv1 = Application.CreateDetailView(os, perm);
            //    FindWindowByView(dv1).GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
            //}
            
            //DetailView dv = View as DetailView;
            //FindWindowByView(dv).GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();

            RefreshAllWindows();
        }

        /// <summary>
        /// Заполнение данных в объекте строительства по его этапам
        /// </summary>
        public void FillConstrObjectDataFromStages(CapitalStructureBase constrObject, Connect connect, IObjectSpace os)
        {
            //View.ObjectSpace.CommitChanges();
            //IObjectSpace os = Application.CreateObjectSpace();


            //CapitalStructureBase constrObject;
            //var x_id = ((BaseObject)e.CurrentObject).Oid;

            //constrObject = os.FindObject<CapitalStructureBase>(new BinaryOperator("Oid", x_id));

            var ConstrStageList = connect.FindObjects<ConstrStage>(x => x.CapitalStructureBase == constrObject && (x.ConstrStageState != AISOGD.Enums.eConstrStageState.Отмена &&
            x.ConstrStageState != AISOGD.Enums.eConstrStageState.НеИзвестно));
            // сколько этапов
            bool isOneStage = false;
            if (constrObject.ConstrStages.Count == 1)
                isOneStage = true;


            // Заполнение проектного наименования, если оно не заполнено (если заполнено, то, возможно оно подправлено)
            if (constrObject.ProjectName == null || constrObject.ProjectName == "")
            {
                // для одного этапа просто вписываем его наименование в наименование объекта
                if (isOneStage)
                {
                    constrObject.ProjectName = constrObject.ConstrStages[0].Name?? "";
                    constrObject.Name = constrObject.ConstrStages[0].Name?? "";
                }
                else
                {
                    constrObject.ProjectName = constrObject.Name?? "";
                    SortProperty sort = new SortProperty("StageNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
                    constrObject.ConstrStages.Sorting.Add(sort);
                    foreach (ConstrStage constrStage in constrObject.ConstrStages)
                    {
                        if (constrStage.ConstrStageState != AISOGD.Enums.eConstrStageState.Отмена &&
            constrStage.ConstrStageState != AISOGD.Enums.eConstrStageState.НеИзвестно)
                        {
                            if (constrStage.StageNo != null && constrStage.StageNo != "")
                                constrObject.ProjectName += Environment.NewLine + constrStage.StageNo + " - " + constrStage.Name;
                            else
                                constrObject.ProjectName += Environment.NewLine + constrStage.Name;
                        }
                    }
                }
                constrObject.Save();
            }

            // Заполнение адреса, если он не заполнены (если заполнено, то, возможно, оно подправлено)
            if (constrObject.Address == null || constrObject.Address == "")
            {
                // для одного этапа просто вписываем его наименование в наименование объекта
                if (isOneStage)
                {
                    constrObject.Address = constrObject.ConstrStages[0].Address ?? "";
                }
                else
                {
                    SortProperty sort = new SortProperty("StageNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
                    constrObject.ConstrStages.Sorting.Add(sort);
                    foreach (ConstrStage constrStage in constrObject.ConstrStages)
                    {
                        if (constrStage.ConstrStageState != AISOGD.Enums.eConstrStageState.Отмена &&
            constrStage.ConstrStageState != AISOGD.Enums.eConstrStageState.НеИзвестно)
                        {
                            if (constrStage.StageNo != null && constrStage.StageNo != "")
                                constrObject.Address += constrStage.StageNo + " - " + constrStage.Address + Environment.NewLine;
                            else
                                constrObject.Address += constrStage.Address + Environment.NewLine;
                        }
                    }
                    constrObject.Address = constrObject.Address.TrimEnd(Environment.NewLine.ToCharArray());
                }
                constrObject.Save();
            }

            // заполнение проектных характеристик
            SetConstrObjProjectInfo(constrObject, connect);

            os.CommitChanges();
        }

        /// <summary>
        /// Заполнение проектных показателей у ОКС по этапам при утверждении РС (по всем этапам со статусом стройка или введено)
        /// </summary>
        /// <param name="constrObj"></param>
        /// <param name="ConstrStageList"></param>
        /// <param name="connect"></param>
        public void SetConstrObjProjectInfo(CapitalStructureBase constrObj, Connect connect)
        {
            #region Сначала обнуляем все показатели 
            constrObj.BuildingSizeProject = 0;
            constrObj.BuildingSizeOverGroundPartProject = 0;
            constrObj.BuildingSizeUnderGroundPartProject = 0;
            constrObj.TotalBuildSquareProject = 0;
            constrObj.BuildSquareProject = 0;
            constrObj.HeightProject = 0;
            constrObj.NotLivingSquareProject = 0;
            constrObj.OutBuildingSquareProject = 0;
            constrObj.BuildingCountProject = 0;
            constrObj.ElevatorsCountProject = 0;
            constrObj.EscalatorsCountProject = 0;
            constrObj.InvalidLiftsCountProject = 0;
            //constrObj.OtherIndicatorsProject = "";

            constrObj.PlacesCountProject = 0;
            constrObj.RoomCountProject = 0;
            //constrObj.CapacityProject = 0;

            constrObj.LivingSpaceProject = 0;
            constrObj.TotalLivingSpaceProject = 0;
            constrObj.NotLivingSpaceProject = 0;
            constrObj.SectionCountProject = 0;
            constrObj.PorchCountProject = 0;
            constrObj.AppartmentsCountProject = 0;
            constrObj.StudioCountProject = 0;
            constrObj.StudioSpaceProject = 0;
            constrObj.OneRoomFlatCountProject = 0;
            constrObj.OneRoomFlatSpaceProject = 0;
            constrObj.TwoRoomFlatCountProject = 0;
            constrObj.TwoRoomFlatSpaceProject = 0;
            constrObj.ThreeRoomFlatCountProject = 0;
            constrObj.ThreeRoomFlatSpaceProject = 0;
            constrObj.FourRoomFlatCountProject = 0;
            constrObj.FourRoomSpaceProject = 0;
            constrObj.MoreRoomFlatCountProject = 0;
            constrObj.MoreRoomSpaceProject = 0;
            constrObj.ElitAppartmentsCountProject = 0;
            constrObj.ElitAppartmentsSpaceProject = 0;

            constrObj.PowerProject = "";
            constrObj.ProductivityProject = "";

            constrObj.LengthProject = 0;
            constrObj.PowerLineProject = "";
            constrObj.PipesInfoProject = "";
            constrObj.ElectricLinesInfoProject = "";
            constrObj.ConstructiveElementsInfoProject = "";

            constrObj.HeatConsumptionProject = 0;
            constrObj.OutdoorIsolationMaterialProject = "";
            constrObj.SkylightsFillingProject = "";
            #endregion

            var ConstrStageList = connect.FindObjects<ConstrStage>(x => x.CapitalStructureBase == constrObj && (x.ConstrStageState != AISOGD.Enums.eConstrStageState.Отмена &&
            x.ConstrStageState != AISOGD.Enums.eConstrStageState.НеИзвестно));
            char[] seps = { '/', '\\' };

            // высота 
            double height = 0;
            foreach (ConstrStage constrStage in ConstrStageList)
            {

                #region Общие показатели
                try { constrObj.BuildingSizeProject += constrStage.BuildingSizeProject; }
                catch { }
                try { constrObj.BuildingSizeOverGroundPartProject += constrStage.BuildingSizeOverGroundPartProject; }
                catch { }
                try { constrObj.BuildingSizeUnderGroundPartProject += constrStage.BuildingSizeUnderGroundPartProject; }
                catch { }
                try { constrObj.TotalBuildSquareProject += constrStage.TotalBuildSquareProject; }
                catch { }
                try { constrObj.BuildSquareProject += constrStage.BuildSquare; }
                catch { }
                if (constrStage.Height > height)
                    height = constrStage.Height;
                //try { constrObj.HeightProject += constrStage.Height; }
                //catch { }
                try { constrObj.NotLivingSquareProject += constrStage.NotLivingSquareProject; }
                catch { }
                try { constrObj.OutBuildingSquareProject += constrStage.OutBuildingSquareProject; }
                catch { }
                try { constrObj.BuildingCountProject += constrStage.BuildingCountProject; }
                catch { }
                #endregion

                #region Лифты, Эскалаторы, подъемники для ОКС
                try { constrObj.ElevatorsCountProject += constrStage.ElevatorsCountProject; }
                catch { }
                try { constrObj.EscalatorsCountProject += constrStage.EscalatorsCountProject; }
                catch { }
                try { constrObj.InvalidLiftsCountProject += constrStage.InvalidLiftsCountProject; }
                catch { }
                #endregion

                #region Материалы
                //try {
                //    if (constrObj.FundMaterialProject == null)
                //        constrObj.FundMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.FundMaterialProject.Oid);
                //}
                //catch { }
                //try
                //{
                //    if (constrObj.WallMaterialProject == null)
                //        constrObj.WallMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.WallMaterialProject.Oid); }
                //catch { }
                //try
                //{
                //    if (constrObj.BorderMaterialProject == null)
                //        constrObj.BorderMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.BorderMaterialProject.Oid); }
                //catch { }
                //try
                //{
                //    if (constrObj.RoofMaterialProject == null)
                //        constrObj.RoofMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.RoofMaterialProject.Oid); }
                //catch { }


                #endregion

                #region Сети и системы инженерно технического обслуживания
                try
                {
                    if (constrStage.ElectroProject == true)
                        constrObj.ElectroProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.HeatProject == true)
                        constrObj.HeatProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.WaterProject == true)
                        constrObj.WaterProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.GazProject == true)
                        constrObj.GazProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.HouseSeverageProject == true)
                        constrObj.HouseSeverageProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.PhonesProject == true)
                        constrObj.PhonesProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.TVProject == true)
                        constrObj.TVProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.RadioProject == true)
                        constrObj.RadioProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.SeverageProject == true)
                        constrObj.SeverageProject = true;
                }
                catch { }
                #endregion

                //#region Иные показатели
                //try
                //{
                //    constrObj.OtherIndicatorsProject += constrStage.OtherIndicatorsProject + Environment.NewLine;
                //    constrObj.OtherIndicatorsProject.TrimEnd(Environment.NewLine.ToCharArray());
                //}
                //catch { }
                //#endregion

                #region нежилые
                try { constrObj.PlacesCountProject += constrStage.PlacesCountProject; }
                catch { }
                try { constrObj.RoomCountProject += constrStage.RoomCountProject; }
                catch { }
                //try { constrObj.CapacityProject += constrStage.CapacityProject; }
                //catch { }
                #endregion

                #region жилые
                try { constrObj.LivingSpaceProject += constrStage.LivingSpaceProject; }
                catch { }
                try { constrObj.TotalLivingSpaceProject += constrStage.TotalLivingSpaceProject; }
                catch { }
                try { constrObj.NotLivingSpaceProject += constrStage.NotLivingSpaceProject; }
                catch { }
                try { constrObj.SectionCountProject += constrStage.SectionCountProject; }
                catch { }
                try { constrObj.PorchCountProject += constrStage.PorchCountProject; }
                catch { }
                try { constrObj.AppartmentsCountProject += constrStage.AppartmentsCountProject; }
                catch { }

                try { constrObj.StudioCountProject += constrStage.StudioCountProject; }
                catch { }
                try { constrObj.StudioSpaceProject += constrStage.StudioSpaceProject; }
                catch { }
                try { constrObj.OneRoomFlatCountProject += constrStage.OneRoomFlatCountProject; }
                catch { }
                try { constrObj.OneRoomFlatSpaceProject += constrStage.OneRoomFlatSpaceProject; }
                catch { }
                try { constrObj.TwoRoomFlatCountProject += constrStage.TwoRoomFlatCountProject; }
                catch { }
                try { constrObj.TwoRoomFlatSpaceProject += constrStage.TwoRoomFlatSpaceProject; }
                catch { }
                try { constrObj.ThreeRoomFlatCountProject += constrStage.ThreeRoomFlatCountProject; }
                catch { }
                try { constrObj.ThreeRoomFlatSpaceProject += constrStage.ThreeRoomFlatSpaceProject; }
                catch { }
                try { constrObj.FourRoomFlatCountProject += constrStage.FourRoomFlatCountProject; }
                catch { }
                try { constrObj.FourRoomSpaceProject += constrStage.FourRoomSpaceProject; }
                catch { }
                try { constrObj.MoreRoomFlatCountProject += constrStage.MoreRoomFlatCountProject; }
                catch { }
                try { constrObj.MoreRoomSpaceProject += constrStage.MoreRoomSpaceProject; }
                catch { }
                try { constrObj.ElitAppartmentsCountProject += constrStage.ElitAppartmentsCountProject; }
                catch { }
                try { constrObj.ElitAppartmentsSpaceProject += constrStage.ElitAppartmentsSpaceProject; }
                catch { }
                #endregion

                #region Объекты производственного назначения
                try
                {
                    if (constrObj.ProductPurposeObjectType == null)
                        constrObj.ProductPurposeObjectType = connect.FindFirstObject<ProductPurposeObjectType>(x => x.Oid == constrStage.ProductPurposeObjectType.Oid);
                }
                catch { }
                try
                {
                    //constrObj.PowerProject = "";
                    constrObj.PowerProject += constrStage.PowerProject + Environment.NewLine;
                    constrObj.PowerProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    constrObj.ProductivityProject += constrStage.ProductivityProject + Environment.NewLine;
                    constrObj.ProductivityProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region  Линейные объекты
                try
                {
                    if (constrObj.LineObjectClass == null)
                        constrObj.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid == constrStage.LineObjectClass.Oid);
                }
                catch { }
                try { constrObj.LengthProject += constrStage.LengthProject; }
                catch { }
                try
                {

                    constrObj.PowerLineProject += constrStage.PowerLineProject + Environment.NewLine;
                    constrObj.PowerLineProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    constrObj.PipesInfoProject += constrStage.PipesInfoProject + Environment.NewLine;
                    constrObj.PipesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    constrObj.ElectricLinesInfoProject += constrStage.ElectricLinesInfoProject + Environment.NewLine;
                    constrObj.ElectricLinesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    constrObj.ConstructiveElementsInfoProject += constrStage.ConstructiveElementsInfoProject + Environment.NewLine;
                    constrObj.ConstructiveElementsInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region Соответствие требованиям энергетической эффективности
                try
                {
                    if (constrObj.EnergyEfficiencyClassProject == null)
                        constrObj.EnergyEfficiencyClassProject = connect.FindFirstObject<EnergyEfficiencyClass>(x => x.Oid == constrStage.EnergyEfficiencyClassProject.Oid);
                }
                catch { }
                try
                {
                    if (constrObj.HeatUnit == null)
                        constrObj.HeatUnit = connect.FindFirstObject<dHeatUnit>(x => x.Oid == constrStage.HeatUnit.Oid);
                }
                catch { }

                try { constrObj.HeatConsumptionProject += constrStage.HeatConsumptionProject; }
                catch { }
                try
                {

                    constrObj.OutdoorIsolationMaterialProject += constrStage.OutdoorIsolationMaterialProject + Environment.NewLine;
                    constrObj.OutdoorIsolationMaterialProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    constrObj.SkylightsFillingProject += constrStage.SkylightsFillingProject + Environment.NewLine;
                    constrObj.SkylightsFillingProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion
            }

            // Заполняем высоту (берем большую)
            try {
                if(height != 0)
                    constrObj.HeightProject = height;
            }
            catch { }

            // заполняем иные показатели у объекта строительства, если не заполнены
            #region // заполняем иные показатели у объекта строительства, если не заполнены
            if (constrObj.OtherIndicatorsProject == null || constrObj.OtherIndicatorsProject == "")
            {
                if (constrObj.BuildingKind != AISOGD.Enums.eBuildingKind.Линейные)
                {
                    // Иные показатели заполняются так:
                    //                        Количество квартир -        шт. ;
                    //                        Общая площадь квартир(с учетом балконов, лоджий, тамбуров) - кв.м;
                    //                        площадь квартир без учета (без учета  балконов, лоджий, тамбуров) -кв.м;
                    //                        площадь помещений 1 этажа - кв.м;
                    //                        площадь помещений подвального этажа -кв.м

                    List<OtherIndicatorTemp> OtherIndicatorTempList = new List<OtherIndicatorTemp>();
                    string[] OtherIndicatorsNames = { "количество квартир", "жилая площадь квартир", "площадь квартир с балконами", "площадь квартир без балконов" };
                    int i = 0;
                    string newLine = Environment.NewLine;
                    string OtherIndicators = "";

                    SortProperty sort = new SortProperty("StageNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
                    constrObj.ConstrStages.Sorting.Add(sort);
                    foreach (ConstrStage constrStage in constrObj.ConstrStages)
                    {
                        i++;
                        try
                        {
                            //ObjectConstractionInfo constrInfo = null;
                            //constrInfo = constrStage.ObjectConstractionInfo[0];
                            //string OtherIndicatorName = "";
                            bool isExist = false;

                            // кол-во квартир
                            //OtherIndicatorName = "количество квартир";
                            //string[] OtherIndicatorsNames = { "количество квартир", "общая площадь квартир", "жилая площадь квартир" };

                            foreach (string OtherIndicatorName in OtherIndicatorsNames)
                            {
                                string oValue = "";
                                try
                                {
                                    if (OtherIndicatorName == "количество квартир")
                                        oValue = constrStage.AppartmentsCountProject.ToString();
                                    if (OtherIndicatorName == "жилая площадь квартир")
                                        oValue = constrStage.AppartmentsSpaceProject.ToString();
                                    if (OtherIndicatorName == "площадь квартир с балконами")
                                        oValue = constrStage.TotalLivingSpaceProject.ToString();
                                    if (OtherIndicatorName == "площадь квартир без балконов")
                                        oValue = constrStage.LivingSpaceProject.ToString();

                                    foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                                    {
                                        if (t.IndicatorType == OtherIndicatorName)
                                        {
                                            StageValue stageValue = new StageValue();
                                            stageValue.StageName = constrStage.StageNo;
                                            stageValue.Value = oValue;
                                            t.StageValues.Add(stageValue);
                                            isExist = true;
                                            break;
                                        }
                                    }
                                    if (!isExist)
                                    {
                                        OtherIndicatorTemp t = new OtherIndicatorTemp();
                                        t.IndicatorType = OtherIndicatorName;
                                        StageValue stageValue = new StageValue();
                                        stageValue.StageName = constrStage.StageNo;
                                        stageValue.Value = oValue;
                                        t.StageValues.Add(stageValue);
                                        OtherIndicatorTempList.Add(t);
                                    }
                                    isExist = false;
                                }
                                catch { }
                            }

                        }
                        catch { }
                    }
                    foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                    {
                        string unit = "";
                        if (t.IndicatorType == "количество квартир")
                            unit = " шт.";
                        else
                            unit = " кв.м";
                        OtherIndicators += t.IndicatorType + " - ";
                        double totalvalue = 0;
                        foreach (StageValue v in t.StageValues)
                        {
                            try
                            {
                                totalvalue += Convert.ToDouble(v.Value);
                            }
                            catch { }
                        }
                        if (constrObj.ConstrStages.Count == 1)
                        {
                            OtherIndicators += totalvalue + unit + ";" + Environment.NewLine;
                        }
                        else
                        {
                            OtherIndicators += totalvalue + unit + ":" + Environment.NewLine;
                            foreach (StageValue v in t.StageValues)
                            {
                                try
                                {
                                    OtherIndicators += v.StageName + " - " + v.Value + unit + Environment.NewLine;
                                }
                                catch { }
                            }
                        }
                    }

                    //OtherIndicators = OtherIndicators.Trim();
                    //OtherIndicators = OtherIndicators.TrimEnd(',');
                    //OtherIndicators += ";" + Environment.NewLine;
                    OtherIndicators += @"площадь помещений 1 этажа  -     кв.м;
площадь помещений подвального этажа -     кв.м";
                    OtherIndicators.TrimEnd(Environment.NewLine.ToCharArray());
                    constrObj.OtherIndicatorsProject = OtherIndicators;
                }
            }
            #endregion

            #region // Этажность
            int floorCount = 0;
            int floorCountAbove = 0;
            int UnderGroundFloorCounProject = 0;
            List<int> FloorCountAboveList = new List<int>();
            string floorCountDiff = "";
            bool isDiff = false;
            int j = 0;
            foreach (ConstrStage constrStage in ConstrStageList)
            {
                if (constrStage.FloorCountDiff != null && constrStage.FloorCountDiff != "")
                {
                    if (!floorCountDiff.Contains(constrStage.FloorCountDiff))
                    {
                        floorCountDiff += constrStage.FloorCountDiff + "-";
                        isDiff = true;
                    }
                }
                else
                {
                    if (j == 0)
                    {
                        FloorCountAboveList.Add(constrStage.FloorCountAbove);
                        floorCount = constrStage.FloorCount;
                        UnderGroundFloorCounProject = constrStage.UnderGroundFloorCount;
                    }
                    else
                    {
                        floorCount = constrStage.FloorCount;
                        UnderGroundFloorCounProject = constrStage.UnderGroundFloorCount;
                        if (!FloorCountAboveList.Contains(constrStage.FloorCountAbove))
                        {
                            isDiff = true;
                            FloorCountAboveList.Add(constrStage.FloorCountAbove);
                        }
                        //else

                        //if (floorCountAbove != constrStage.FloorCountAbove)
                        //{
                        //    if(!floorCountDiff.Contains(floorCountAbove.ToString()))
                        //        floorCountDiff += floorCountAbove.ToString() + "-" + constrStage.FloorCountAbove.ToString() + "-";
                        //    isDiff = true;
                        //}
                    }
                    j++;
                }
            }


            if (!isDiff)
            {
                constrObj.FloorCountAboveProject = FloorCountAboveList[0];
                constrObj.FloorCountProject = floorCount;
                constrObj.UnderGroundFloorCounProject = UnderGroundFloorCounProject;
            }
            else
            {
                FloorCountAboveList.Sort();
                foreach (int f in FloorCountAboveList)
                {
                    if (!floorCountDiff.Contains(f.ToString()))
                        floorCountDiff += f.ToString() + "-";
                }
                floorCountDiff = floorCountDiff.TrimEnd('-');
                constrObj.FloorCountDiffProject = floorCountDiff;
            }
            #endregion

            constrObj.Save();
        }

        /// <summary>
        /// Заполнение фактических показателей у объекта строительства из РВ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FillConstrObjFaktDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();


            CapitalStructureBase constrObj;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            constrObj = os.FindObject<CapitalStructureBase>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)constrObj.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            #region Сначала обнуляем все показатели 
            constrObj.BuildingSizeFakt = 0;
            constrObj.BuildingSizeOverGroundPartFakt = 0;
            constrObj.BuildingSizeUnderGroundPartFakt = 0;
            constrObj.TotalBuildSquareFakt = 0;
            constrObj.BuildSquareFakt = 0;
            constrObj.HeightFakt = 0;
            constrObj.NotLivingSquareFakt = 0;
            constrObj.OutBuildingSquareFakt = 0;
            constrObj.BuildingCountFakt = 0;
            constrObj.ElevatorsCountFakt = 0;
            constrObj.EscalatorsCountFakt = 0;
            constrObj.InvalidLiftsCountFakt = 0;
            //constrObj.OtherIndicatorsProject = "";

            constrObj.PlacesCountFakt = 0;
            constrObj.RoomCountFakt = 0;
            constrObj.CapacityFakt = 0;

            constrObj.LivingSpaceFakt = 0;
            constrObj.TotalLivingSpaceFakt = 0;
            constrObj.NotLivingSpaceFakt = 0;
            constrObj.SectionCountFakt = 0;
            constrObj.PorchCountFakt = 0;
            constrObj.AppartmentsCountFakt = 0;
            constrObj.StudioCountFakt = 0;
            constrObj.StudioSpaceFakt = 0;
            constrObj.OneRoomFlatCountFakt = 0;
            constrObj.OneRoomFlatSpaceFakt = 0;
            constrObj.TwoRoomFlatCountFakt = 0;
            constrObj.TwoRoomFlatSpaceFakt = 0;
            constrObj.ThreeRoomFlatCountFakt = 0;
            constrObj.ThreeRoomFlatSpaceFakt = 0;
            constrObj.FourRoomFlatCountFakt = 0;
            constrObj.FourRoomSpaceFakt = 0;
            constrObj.MoreRoomFlatCountFakt = 0;
            constrObj.MoreRoomSpaceFakt = 0;
            constrObj.ElitAppartmentsCountFakt = 0;
            constrObj.ElitAppartmentsSpaceFakt = 0;

            constrObj.PowerFakt = "";
            constrObj.ProductivityFakt = "";

            constrObj.LengthFakt = 0;
            constrObj.PowerLineFakt = "";
            constrObj.PipesInfoFakt = "";
            constrObj.ElectricLinesInfoFakt = "";
            constrObj.ConstructiveElementsInfoFakt = "";

            constrObj.HeatConsumptionFakt = 0;
            constrObj.OutdoorIsolationMaterialFakt = "";
            constrObj.SkylightsFillingFakt = "";
            #endregion

            
            char[] seps = { '/', '\\' };
            // высота 
            double height = 0;
            // этажность
            int floorCount = 0;
            int floorCountAbove = 0;
            int UnderGroundFloorCounProject = 0;
            List<int> FloorCountAboveList = new List<int>();
            string floorCountDiff = "";
            bool isDiff = false;
            int j = 0;
            foreach (GeneralDocBase doc in constrObj.generalDocBase)
            {
                if (doc.ClassInfo.ClassType.FullName == "AISOGD.Perm.UsePerm")
                {
                    UsePerm usePerm = (UsePerm)doc;
                    if (usePerm.EDocStatus != AISOGD.Enums.eDocStatus.Отказ)
                    {
                        #region Общие показатели
                        try { constrObj.BuildingSizeFakt += usePerm.BuildingSizeFakt; }
                        catch { }
                        try { constrObj.BuildingSizeOverGroundPartFakt += usePerm.BuildingSizeOverGroundPartFakt; }
                        catch { }
                        try { constrObj.BuildingSizeUnderGroundPartFakt += usePerm.BuildingSizeUnderGroundPartFakt; }
                        catch { }
                        try { constrObj.TotalBuildSquareFakt += usePerm.TotalBuildSquareFakt; }
                        catch { }
                        try { constrObj.BuildSquareFakt += usePerm.BuildSquareFakt; }
                        catch { }
                        //try { constrObj.HeightFakt += usePerm.HeightFakt; }
                        //catch { }
                        if (usePerm.HeightFakt > height)
                            height = usePerm.HeightFakt;
                        try { constrObj.NotLivingSquareFakt += usePerm.NotLivingSquareFakt; }
                        catch { }
                        try { constrObj.OutBuildingSquareFakt += usePerm.OutBuildingSquareFakt; }
                        catch { }
                        try { constrObj.BuildingCountFakt += usePerm.BuildingCountFakt; }
                        catch { }
                        #endregion

                        #region Лифты, Эскалаторы, подъемники для ОКС
                        try { constrObj.ElevatorsCountFakt += usePerm.ElevatorsCountFakt; }
                        catch { }
                        try { constrObj.EscalatorsCountFakt += usePerm.EscalatorsCountFakt; }
                        catch { }
                        try { constrObj.InvalidLiftsCountFakt += usePerm.InvalidLiftsCountFakt; }
                        catch { }
                        #endregion

                        #region Сети и системы инженерно технического обслуживания
                        try
                        {
                            if (usePerm.ElectroFakt == true)
                                constrObj.ElectroFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.HeatFakt == true)
                                constrObj.HeatFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.WaterFakt == true)
                                constrObj.WaterFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.GazFakt == true)
                                constrObj.GazFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.HouseSeverageFakt == true)
                                constrObj.HouseSeverageFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.PhonesFakt == true)
                                constrObj.PhonesFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.TVFakt == true)
                                constrObj.TVFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.RadioFakt == true)
                                constrObj.RadioFakt = true;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.SeverageFakt == true)
                                constrObj.SeverageFakt = true;
                        }
                        catch { }
                        #endregion

                        #region Иные показатели
                        try
                        {
                            constrObj.OtherIndicatorsFakt += usePerm.OtherIndicatorsFakt + Environment.NewLine;
                            constrObj.OtherIndicatorsFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region нежилые
                        try { constrObj.PlacesCountFakt += usePerm.PlacesCountFakt; }
                        catch { }
                        try { constrObj.RoomCountFakt += usePerm.RoomCountFakt; }
                        catch { }
                        try { constrObj.CapacityFakt += usePerm.CapacityFakt; }
                        catch { }
                        #endregion

                        #region жилые
                        try { constrObj.LivingSpaceFakt += usePerm.LivingSpaceFakt; }
                        catch { }
                        try { constrObj.TotalLivingSpaceFakt += usePerm.TotalLivingSpaceFakt; }
                        catch { }
                        try { constrObj.NotLivingSpaceFakt += usePerm.NotLivingSpaceFakt; }
                        catch { }
                        try { constrObj.SectionCountFakt += usePerm.SectionCountFakt; }
                        catch { }
                        try { constrObj.PorchCountFakt += usePerm.PorchCountFakt; }
                        catch { }
                        try { constrObj.AppartmentsCountFakt += usePerm.AppartmentsCountFakt; }
                        catch { }

                        try { constrObj.StudioCountFakt += usePerm.StudioCountFakt; }
                        catch { }
                        try { constrObj.StudioSpaceFakt += usePerm.StudioSpaceFakt; }
                        catch { }
                        try { constrObj.OneRoomFlatCountFakt += usePerm.OneRoomFlatCountFakt; }
                        catch { }
                        try { constrObj.OneRoomFlatSpaceFakt += usePerm.OneRoomFlatSpaceFakt; }
                        catch { }
                        try { constrObj.TwoRoomFlatCountFakt += usePerm.TwoRoomFlatCountFakt; }
                        catch { }
                        try { constrObj.TwoRoomFlatSpaceFakt += usePerm.TwoRoomFlatSpaceFakt; }
                        catch { }
                        try { constrObj.ThreeRoomFlatCountFakt += usePerm.ThreeRoomFlatCountFakt; }
                        catch { }
                        try { constrObj.ThreeRoomFlatSpaceFakt += usePerm.ThreeRoomFlatSpaceFakt; }
                        catch { }
                        try { constrObj.FourRoomFlatCountFakt += usePerm.FourRoomFlatCountFakt; }
                        catch { }
                        try { constrObj.FourRoomSpaceFakt += usePerm.FourRoomSpaceFakt; }
                        catch { }
                        try { constrObj.MoreRoomFlatCountFakt += usePerm.MoreRoomFlatCountFakt; }
                        catch { }
                        try { constrObj.MoreRoomSpaceFakt += usePerm.MoreRoomSpaceFakt; }
                        catch { }
                        try { constrObj.ElitAppartmentsCountFakt += usePerm.ElitAppartmentsCountFakt; }
                        catch { }
                        try { constrObj.ElitAppartmentsSpaceFakt += usePerm.ElitAppartmentsSpaceFakt; }
                        catch { }
                        #endregion

                        #region Объекты производственного назначения
                        try
                        {
                            if (constrObj.ProductPurposeObjectType == null)
                                constrObj.ProductPurposeObjectType = connect.FindFirstObject<ProductPurposeObjectType>(x => x.Oid == usePerm.ProductPurposeObjectType.Oid);
                        }
                        catch { }
                        try
                        {
                            constrObj.PowerFakt = "";
                            constrObj.PowerFakt += usePerm.PowerFakt + Environment.NewLine;
                            constrObj.PowerFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            constrObj.ProductivityFakt = "";
                            constrObj.ProductivityFakt += usePerm.ProductivityFakt + Environment.NewLine;
                            constrObj.ProductivityFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region  Линейные объекты
                        try
                        {
                            if (constrObj.LineObjectClass == null)
                                constrObj.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid == usePerm.LineObjectClass.Oid);
                        }
                        catch { }
                        try { constrObj.LengthFakt += usePerm.LengthFakt; }
                        catch { }
                        try
                        {
                            constrObj.PowerLineFakt = "";
                            constrObj.PowerLineFakt += usePerm.PowerLineFakt + Environment.NewLine;
                            constrObj.PowerLineFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            constrObj.PipesInfoFakt = "";
                            constrObj.PipesInfoFakt += usePerm.PipesInfoFakt + Environment.NewLine;
                            constrObj.PipesInfoFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            constrObj.ElectricLinesInfoFakt = "";
                            constrObj.ElectricLinesInfoFakt += usePerm.ElectricLinesInfoFakt + Environment.NewLine;
                            constrObj.ElectricLinesInfoFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            constrObj.ConstructiveElementsInfoFakt = "";
                            constrObj.ConstructiveElementsInfoFakt += usePerm.ConstructiveElementsInfoFakt + Environment.NewLine;
                            constrObj.ConstructiveElementsInfoFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region Соответствие требованиям энергетической эффективности
                        try
                        {
                            if (constrObj.EnergyEfficiencyClassFakt == null)
                                constrObj.EnergyEfficiencyClassFakt = connect.FindFirstObject<EnergyEfficiencyClass>(x => x.Oid == usePerm.EnergyEfficiencyClassFakt.Oid);
                        }
                        catch { }

                        try { constrObj.HeatConsumptionFakt += usePerm.HeatConsumptionFakt; }
                        catch { }
                        try
                        {
                            constrObj.OutdoorIsolationMaterialFakt = "";
                            constrObj.OutdoorIsolationMaterialFakt += usePerm.OutdoorIsolationMaterialFakt + Environment.NewLine;
                            constrObj.OutdoorIsolationMaterialFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            constrObj.SkylightsFillingFakt = "";
                            constrObj.SkylightsFillingFakt += usePerm.SkylightsFillingFakt + Environment.NewLine;
                            constrObj.SkylightsFillingFakt.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region Этажность вычисление
                        if (usePerm.FloorCountDiffFakt != null && usePerm.FloorCountDiffFakt != "")
                        {
                            if (!floorCountDiff.Contains(usePerm.FloorCountDiffFakt))
                            {
                                floorCountDiff += usePerm.FloorCountDiffFakt + "-";
                                isDiff = true;
                            }
                        }
                        else
                        {
                            if (j == 0)
                            {
                                FloorCountAboveList.Add(usePerm.FloorCountAboveFakt);
                                floorCount = usePerm.FloorCountFakt;
                                UnderGroundFloorCounProject = usePerm.UnderGroundFloorCountFakt;
                            }
                            else
                            {
                                floorCount = usePerm.FloorCountFakt;
                                UnderGroundFloorCounProject = usePerm.UnderGroundFloorCountFakt;
                                if (!FloorCountAboveList.Contains(usePerm.FloorCountAboveFakt))
                                {
                                    isDiff = true;
                                    FloorCountAboveList.Add(usePerm.FloorCountAboveFakt);
                                }
                            }
                            j++;
                        }
                        #endregion
                    }
                }
            }

            #region Этажность заполнение
            if (!isDiff)
            {
                constrObj.FloorCountAboveProject = FloorCountAboveList[0];
                constrObj.FloorCountProject = floorCount;
                constrObj.UnderGroundFloorCounProject = UnderGroundFloorCounProject;
            }
            else
            {
                FloorCountAboveList.Sort();
                foreach (int f in FloorCountAboveList)
                {
                    if (!floorCountDiff.Contains(f.ToString()))
                        floorCountDiff += f.ToString() + "-";
                }
                floorCountDiff = floorCountDiff.TrimEnd('-');
                constrObj.FloorCountDiffProject = floorCountDiff;
            }
            #endregion
            constrObj.Save();
            os.CommitChanges();
            RefreshAllWindows();
        }


        /// <summary>
        /// Заполнить данные по ОКС из уведомления на строительство
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FillPermDataFromPermNoticeAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();


            CapitalStructureBase constrObj;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            constrObj = os.FindObject<CapitalStructureBase>(new BinaryOperator("Oid", x_id));
            Connect connect = Connect.FromObjectSpace(os);

            #region Сначала обнуляем все показатели 
            constrObj.HeightProject = 0;
            constrObj.FloorCountProject = 0;
            constrObj.BuildSquareProject = 0;
            #endregion

            foreach (GeneralDocBase doc in constrObj.generalDocBase)
            {
                if (doc.ClassInfo.ClassType.FullName == "AISOGD.Perm.PermNoticeIZD")
                {
                    PermNoticeIZD perm = (PermNoticeIZD)doc;
                    if (perm.EDocStatus != AISOGD.Enums.eDocStatus.Отказ)
                    {
                        try
                        {
                            constrObj.FloorCountProject = Convert.ToInt16(perm.FloorCount);
                        }
                        catch { }
                        try
                        {
                            constrObj.HeightProject = Convert.ToDouble(perm.Height);
                        }
                        catch { }
                        try
                        {
                            constrObj.BuildSquareProject = Convert.ToDouble(perm.BuildSquare);
                        }
                        catch { }

                        try
                        {
                            if (perm.IZDAim == AISOGD.Enums.eIZDAim.Строительство)
                                constrObj.WorkKind = AISOGD.Enums.eWorkKind.Строительство;
                            else
                                constrObj.WorkKind = AISOGD.Enums.eWorkKind.Реконструкция;
                        }
                        catch { }

                        try
                        {
                            constrObj.Address = perm.ObjectAddress ?? "";
                        }
                        catch { }

                        try
                        {
                            if (perm.IZDKind == AISOGD.Enums.eIZDKind.ИЖД)
                                constrObj.ConstructionType = connect.FindFirstObject<dConstructionType>(x => x.Name == "индивидуальный жилой дом");
                            else
                                constrObj.ConstructionType = connect.FindFirstObject<dConstructionType>(x => x.Name == "дачный дом");
                        }
                        catch { }
                        constrObj.Save();
                        os.CommitChanges();
                    }
                }
            }

            RefreshAllWindows();
        }
    }
}
