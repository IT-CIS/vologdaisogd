﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class UsePermNoticeIZDTemplateController : ViewController
    {
        public UsePermNoticeIZDTemplateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        /// <summary>
        /// Сформировать уведомление на ввод ИЖД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsePermNoticeIZDTemplateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            UsePermNoticeIZD perm = os.GetObjectByKey<UsePermNoticeIZD>(x_id);

            Connect connect = Connect.FromObjectSpace(os);

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePermNoticeIzd.rtf");
            richServer.LoadDocumentTemplate(templatePath);


            //Застройщик
            // делим на слова для вставки в строки
            string developer = "";
            developer = perm.ConstrDeveloperStringDat ?? "";


            try
            {
                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = developer.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 25)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterBook1 = richServer.Document.Bookmarks["RequesterBook1"];
                richServer.Document.Replace(RequesterBook1.Range, text1.TrimEnd());
                Bookmark RequesterBook2 = richServer.Document.Bookmarks["RequesterBook2"];
                richServer.Document.Replace(RequesterBook2.Range, text2.TrimEnd());
                Bookmark RequesterBook3 = richServer.Document.Bookmarks["RequesterBook3"];
                richServer.Document.Replace(RequesterBook3.Range, text3.TrimEnd());
            }
            catch { }


            // адрес застройщика
            try
            {
                string RequesterAddr = "";
                try
                {
                    RequesterAddr = perm.ConstrDeveloperContactInfo;
                    RequesterAddr = RequesterAddr.Replace("Адрес:", "");
                }
                catch { }
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RequesterAddr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterAddrBook1 = richServer.Document.Bookmarks["RequesterAddrBook1"];
                richServer.Document.Replace(RequesterAddrBook1.Range, text1);
                Bookmark RequesterAddrBook2 = richServer.Document.Bookmarks["RequesterAddrBook2"];
                richServer.Document.Replace(RequesterAddrBook2.Range, text2.Trim());

            }
            catch { }

            // email
            try
            {
                string Email = perm.ConstrDeveloperEmail ?? "";

                Bookmark EmailBook = richServer.Document.Bookmarks["EmailBook"];
                richServer.Document.Replace(EmailBook.Range, Email);
            }
            catch { }

            // дата заявки
            string LetterDate = "";
            try
            {


                if (perm.LetterDate != DateTime.MinValue)
                {
                    LetterDate = perm.LetterDate.Day.ToString();
                    if (LetterDate.Length == 1)
                        LetterDate = "0" + LetterDate;
                    LetterDate += $" {connect.GetMonth(perm.LetterDate)} {perm.LetterDate.Year} года";
                }

                Bookmark LetterDateBook = richServer.Document.Bookmarks["LetterDateBook"];
                richServer.Document.Replace(LetterDateBook.Range, LetterDate);
            }
            catch { }

            // реквизиты заявки
            try
            {
                string LetterInfo = "";

                LetterInfo = $"{LetterDate} № {perm.LetterNo}";

                Bookmark LetterInfoBook = richServer.Document.Bookmarks["LetterInfoBook"];
                richServer.Document.Replace(LetterInfoBook.Range, LetterInfo);
            }
            catch { }

            // вид работ (цель подачи)
            try
            {
                string WorkType = "построенного";
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Реконструкция)
                {
                    WorkType = "реконструированного";
                }
                Bookmark WorkTypeBook = richServer.Document.Bookmarks["WorkTypeBook"];
                richServer.Document.Replace(WorkTypeBook.Range, WorkType);
            }
            catch { }

            // тип объекта ИЖД
            try
            {
                string IZDKind = "объекта индивидуального жилищного строительства";
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    IZDKind = "садового дома";
                }

                Bookmark IZDKindBook = richServer.Document.Bookmarks["IZDKindBook"];
                richServer.Document.Replace(IZDKindBook.Range, IZDKind);
            }
            catch { }

            // Сведения о земельном участке
            try
            {
                string ParcelInfo = "";

                ParcelInfo = $"{perm.LotCadNo ?? ""} по адресу: {perm.ObjectAddress}";

                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = ParcelInfo.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 81)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 85)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }

                Bookmark LotAddrBook1 = richServer.Document.Bookmarks["LotAddrBook1"];
                richServer.Document.Replace(LotAddrBook1.Range, text1.Trim());
                Bookmark LotAddrBook2 = richServer.Document.Bookmarks["LotAddrBook2"];
                richServer.Document.Replace(LotAddrBook2.Range, text2.Trim());
                Bookmark LotAddrBook3 = richServer.Document.Bookmarks["LotAddrBook3"];
                richServer.Document.Replace(LotAddrBook3.Range, text3.Trim());

            }
            catch { }

            // Подписывающее лицо
            try
            {
                string SignerCapacity = "";
                try { SignerCapacity = perm.signer.capacity; }
                catch { }
                Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
            }
            catch { }
            try
            {
                string SignerName = "";
                try { SignerName = perm.signer.name; }
                catch { }
                Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                richServer.Document.Replace(SignerNameBook.Range, SignerName);
            }
            catch { }

            string permInfo = "";
            //try { permInfo = String.Format($"№ {perm.FullDocNo?? ""} от {perm.DocDate.ToShortDateString()}"); }
            //catch { }
            //string path = Path.GetTempPath() + @"\Уведомление на ввод_" + permInfo + "_" +
            //    DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            try
            {
                string letterNo = "";
                if ((perm.LetterNo ?? "").Contains("/"))
                    letterNo = (perm.LetterNo ?? "").Split('/')[1];
                permInfo = String.Format($"{letterNo}_{perm.ConstrDeveloperString.Split(' ')[0]}_{perm.ObjectAddress ?? ""}");
            }
            catch { }
            string path = Path.GetTempPath() + permInfo +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        /// <summary>
        /// Сформировать несоответствие на ввод
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsePermNoticeIZDRefuseTemplateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            UsePermNoticeIZD perm = os.GetObjectByKey<UsePermNoticeIZD>(x_id);

            Connect connect = Connect.FromObjectSpace(os);

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePermNoticeIzdRefuse.rtf");
            richServer.LoadDocumentTemplate(templatePath);


            //Застройщик
            // делим на слова для вставки в строки
            string developer = "";
            developer = perm.ConstrDeveloperStringDat ?? "";


            try
            {
                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = developer.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 25)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterBook1 = richServer.Document.Bookmarks["RequesterBook1"];
                richServer.Document.Replace(RequesterBook1.Range, text1.TrimEnd());
                Bookmark RequesterBook2 = richServer.Document.Bookmarks["RequesterBook2"];
                richServer.Document.Replace(RequesterBook2.Range, text2.TrimEnd());
                Bookmark RequesterBook3 = richServer.Document.Bookmarks["RequesterBook3"];
                richServer.Document.Replace(RequesterBook3.Range, text3.TrimEnd());
            }
            catch { }


            // адрес застройщика
            try
            {
                string RequesterAddr = "";
                try
                {
                    RequesterAddr = perm.ConstrDeveloperContactInfo;
                    RequesterAddr = RequesterAddr.Replace("Адрес:", "");
                }
                catch { }
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RequesterAddr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterAddrBook1 = richServer.Document.Bookmarks["RequesterAddrBook1"];
                richServer.Document.Replace(RequesterAddrBook1.Range, text1);
                Bookmark RequesterAddrBook2 = richServer.Document.Bookmarks["RequesterAddrBook2"];
                richServer.Document.Replace(RequesterAddrBook2.Range, text2.Trim());

            }
            catch { }

            // email
            try
            {
                string Email = perm.ConstrDeveloperEmail ?? "";

                Bookmark EmailBook = richServer.Document.Bookmarks["EmailBook"];
                richServer.Document.Replace(EmailBook.Range, Email);
            }
            catch { }

            // дата заявки
            string LetterDate = "";
            try
            {


                if (perm.LetterDate != DateTime.MinValue)
                {
                    LetterDate = perm.LetterDate.Day.ToString();
                    if (LetterDate.Length == 1)
                        LetterDate = "0" + LetterDate;
                    LetterDate += $" {connect.GetMonth(perm.LetterDate)} {perm.LetterDate.Year} года";
                }

                Bookmark LetterDateBook = richServer.Document.Bookmarks["LetterDateBook"];
                richServer.Document.Replace(LetterDateBook.Range, LetterDate);

                Bookmark LetterDateBook1 = richServer.Document.Bookmarks["LetterDateBook1"];
                richServer.Document.Replace(LetterDateBook1.Range, LetterDate);
            }
            catch { }

            // реквизиты заявки
            try
            {
                string LetterInfo = "";

                LetterInfo = $"{LetterDate} № {perm.LetterNo}";

                Bookmark LetterInfoBook = richServer.Document.Bookmarks["LetterInfoBook"];
                richServer.Document.Replace(LetterInfoBook.Range, LetterInfo);
            }
            catch { }

            // вид работ (цель подачи)
            try
            {
                string WorkType = "построенного";
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Реконструкция)
                {
                    WorkType = "реконструированного";
                }
                Bookmark WorkTypeBook = richServer.Document.Bookmarks["WorkTypeBook"];
                richServer.Document.Replace(WorkTypeBook.Range, WorkType);
            }
            catch { }

            // тип объекта ИЖД
            try
            {
                string IZDKind = "объекта индивидуального жилищного строительства";
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    IZDKind = "объекта садового дома";
                }

                Bookmark IZDKindBook = richServer.Document.Bookmarks["IZDKindBook"];
                richServer.Document.Replace(IZDKindBook.Range, IZDKind);
            }
            catch { }

            // Сведения о земельном участке
            try
            {
                string WorkType = "построенного";
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Реконструкция)
                {
                    WorkType = "реконструированного";
                }
                string IZDKind = "объекта индивидуального жилищного строительства";
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    IZDKind = "объекта садового дома";
                }

                string ParcelInfo = "";

                ParcelInfo = $"{WorkType} {IZDKind}, расположенного на участке с кадастровым номером {perm.LotCadNo ?? ""} по адресу: {perm.ObjectAddress}";

                string text1 = "";
                string text2 = "";
                string text3 = "";
                string text4 = "";
                string[] separators = { " " };
                string[] words = ParcelInfo.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 47)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 85)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text3 + words[i]).Length < 85)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text4 += words[i] + " ";
                        i++;
                    }
                }

                Bookmark LotAddrBook1 = richServer.Document.Bookmarks["LotAddrBook1"];
                richServer.Document.Replace(LotAddrBook1.Range, text1.Trim());
                Bookmark LotAddrBook2 = richServer.Document.Bookmarks["LotAddrBook2"];
                richServer.Document.Replace(LotAddrBook2.Range, text2.Trim());
                Bookmark LotAddrBook3 = richServer.Document.Bookmarks["LotAddrBook3"];
                richServer.Document.Replace(LotAddrBook3.Range, text3.Trim());
                Bookmark LotAddrBook4 = richServer.Document.Bookmarks["LotAddrBook4"];
                richServer.Document.Replace(LotAddrBook4.Range, text4.Trim());
            }
            catch { }


            // Причины отказа в согласовании
            try
            {
                string RefuseReasonParam = "";
                try { RefuseReasonParam = perm.RefuseReasonParam; }
                catch { }

                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RefuseReasonParam.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 82)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }

                Bookmark RefuseReasonParamBook1 = richServer.Document.Bookmarks["RefuseReasonParamBook1"];
                richServer.Document.Replace(RefuseReasonParamBook1.Range, text1.Trim());
                Bookmark RefuseReasonParamBook2 = richServer.Document.Bookmarks["RefuseReasonParamBook2"];
                richServer.Document.Replace(RefuseReasonParamBook2.Range, text2.Trim());
            }
            catch { }
            try
            {
                string RefuseReasonAppearance = "";
                try { RefuseReasonAppearance = perm.RefuseReasonAppearance; }
                catch { }

                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RefuseReasonAppearance.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 82)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }

                Bookmark RefuseReasonAppearanceBook1 = richServer.Document.Bookmarks["RefuseReasonAppearanceBook1"];
                richServer.Document.Replace(RefuseReasonAppearanceBook1.Range, text1.Trim());
                Bookmark RefuseReasonAppearanceBook2 = richServer.Document.Bookmarks["RefuseReasonAppearanceBook2"];
                richServer.Document.Replace(RefuseReasonAppearanceBook2.Range, text2.Trim());

            }
            catch { }
            try
            {
                string RefuseReasonUseKind = "";
                try { RefuseReasonUseKind = perm.RefuseReasonUseKind; }
                catch { }

                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RefuseReasonUseKind.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 82)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }

                Bookmark RefuseReasonUseKindBook1 = richServer.Document.Bookmarks["RefuseReasonUseKindBook1"];
                richServer.Document.Replace(RefuseReasonUseKindBook1.Range, text1.Trim());
                Bookmark RefuseReasonUseKindBook2 = richServer.Document.Bookmarks["RefuseReasonUseKindBook2"];
                richServer.Document.Replace(RefuseReasonUseKindBook2.Range, text2.Trim());
            }
            catch { }
            try
            {
                string RefuseReasonPlacement = "";
                try { RefuseReasonPlacement = perm.RefuseReasonPlacement; }
                catch { }

                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RefuseReasonPlacement.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 82)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }

                Bookmark RefuseReasonPlacementBook1 = richServer.Document.Bookmarks["RefuseReasonPlacementBook1"];
                richServer.Document.Replace(RefuseReasonPlacementBook1.Range, text1.Trim());
                Bookmark RefuseReasonPlacementBook2 = richServer.Document.Bookmarks["RefuseReasonPlacementBook2"];
                richServer.Document.Replace(RefuseReasonPlacementBook2.Range, text2.Trim());
            }
            catch { }

            // Подписывающее лицо
            try
            {
                string SignerCapacity = "";
                try { SignerCapacity = perm.signer.capacity; }
                catch { }
                Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
            }
            catch { }
            try
            {
                string SignerName = "";
                try { SignerName = perm.signer.name; }
                catch { }
                Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                richServer.Document.Replace(SignerNameBook.Range, SignerName);
            }
            catch { }

            string permInfo = "";
            try
            {
                string letterNo = "";
                if ((perm.LetterNo ?? "").Contains("/"))
                    letterNo = (perm.LetterNo ?? "").Split('/')[1];
                permInfo = String.Format($"{letterNo}_{perm.ConstrDeveloperString.Split(' ')[0]}_{perm.ObjectAddress ?? ""}");
            }
            catch { }
            string path = Path.GetTempPath() + permInfo + @"_отказ" +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }
    }
}
