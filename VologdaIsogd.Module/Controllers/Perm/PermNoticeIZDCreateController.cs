﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.Land;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PermNoticeIZDCreateController : ViewController
    {
        public PermNoticeIZDCreateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        // необходимо добавить различные проверки - 
        // на существование разрешения
        // на существование этапов
        public PermNoticeIZD CreatePermNoticeIZD(Connect connect)
        {
            PermNoticeIZD perm = null;
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();


            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return null;
            }

            string layer = "";
            // получаем название слоя выделения
            SelectionTable selectionTable = m_MapInfo.SelectionTable;// new SelectionTable(m_MapInfo);
            layer = selectionTable.Name;
            TableInfoTemp startSelectObjects = new TableInfoTemp(m_MapInfo, ETempTableType.Temp);
            m_MapInfo.Do(String.Format("Select * From Selection into {0}", startSelectObjects.Name));
            if (layer == "")
            {
                XtraMessageBox.Show("Не удалось получить название таблицы выделенных объектов. Пожалуйста, обратитесь к администратору Системы!", "Ошибка");
                return null;
            }

            if (layer != "")
            {
                List<string> mapObjInfos = new List<string>();
                int selObjCount = selectionTable.GetCount().Value; // Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                if (selObjCount > 1)
                {
                    XtraMessageBox.Show("Выделено более одного объекта. Уведомление можно создать только по одному объекту ИЖД!", "Ошибка");
                    return null;
                }
                int GISobjNo = 0;

                m_MapInfo.Fetch(EFetch.Rec, startSelectObjects.Name, 1);
                try
                {
                    string gisObjId = selectionTable.GetFirstID();//m_MapInfo.Eval("Selection.MI_PRINX");
                    if (gisObjId != "0")
                    {
                        mapObjInfos.Add(gisObjId);
                    }
                    else
                    {
                        XtraMessageBox.Show("Имеются несохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                        return null;
                    }
                }
                catch { }
                try
                {
                    GISobjNo = Convert.ToInt16(m_MapInfo.Eval(String.Format("{0}.НОМЕР_В_БАЗЕ", startSelectObjects.Name)));
                }
                catch { }

                // если номер не указан, то указываем его автоматически и записываем в семантику
                if (GISobjNo == 0)
                {
                    GISobjNo = DistributedIdGeneratorHelper.Generate(connect.GetSession().DataLayer, "ConstrPerm_GISObjNo", string.Empty);
                }

                if (GISobjNo == 0)
                {
                    XtraMessageBox.Show("У выделенных объектов не удалось определить родительский объект строительства. Пожалайста, обратитесь к администратору Системы!", "Ошибка");
                    return null;
                }

                if (mapObjInfos.Count > 0)
                {
                    // по Номеру на схеме ищем объект строительства. Если не находим - создаем
                    CapitalStructureBase constrObject = connect.FindFirstObject<CapitalStructureBase>(x => x.GISObjNo == GISobjNo);
                    if (constrObject == null)
                    {
                        constrObject = connect.CreateObject<CapitalStructureBase>();
                        constrObject.GISObjNo = GISobjNo;
                        try
                        {
                            constrObject.ConstructionType = connect.FindFirstObject<dConstructionType>(x => x.Name == "индивидуальный жилой дом");
                        }
                        catch { }
                        try {
                            constrObject.ConstructionCategory = connect.FindFirstObject<dConstructionType>(x => x.Name == "индивидуальные жилые дома");
                        }
                        catch { }
                        
                        constrObject.Save();
                    }
                    constrObject.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;
                    ConstrStage constrStage = null;
                    if (constrObject.ConstrStages.Count == 0)
                    {
                        constrStage = connect.CreateObject<ConstrStage>();

                        constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;

                        try { constrStage.ConstructionType = constrObject.ConstructionType; }
                        catch { }
                        try
                        {
                            constrStage.ConstructionCategory = constrObject.ConstructionCategory;
                        }
                        catch { }
                        
                        constrStage.Save();
                        connect.GetUnitOfWork().CommitChanges();
                        
                        constrObject.ConstrStages.Add(constrStage);
                    }
                    

                    // записываем в семантику номер ОКС
                    try
                    {
                        m_MapInfo.Do("Update Selection Set НОМЕР_В_БАЗЕ =  " + GISobjNo + " Commit table " + layer);
                    }
                    catch { }

                    // У объекта строительства ищем уведомление на строительство, если нет - создаем
                    bool isPermNoticeExist = false;
                    foreach(PermNoticeIZD d in constrObject.generalDocBase)
                    {
                        if(d.ClassInfo.FullName == "AISOGD.Perm.PermNoticeIZD")
                        {
                            if(d.EDocStatus != AISOGD.Enums.eDocStatus.Отказ && d.EDocStatus != AISOGD.Enums.eDocStatus.ПрекращениеДелопроизводства &&
                                d.EDocStatus != AISOGD.Enums.eDocStatus.УтратилоДействие && d.EDocStatus != AISOGD.Enums.eDocStatus.УтратилоДействиеПоСроку)
                            {
                                perm = (PermNoticeIZD)d;
                                isPermNoticeExist = true;
                                break;
                            }
                        }
                    }
                    if (!isPermNoticeExist)
                    {
                        perm = connect.CreateObject<PermNoticeIZD>();
                        perm.DocDate = DateTime.Now.Date;
                        perm.EDocStatus = AISOGD.Enums.eDocStatus.Подготовка;
                        perm.DocKind = connect.FindFirstObject<dDocKind>(x => x.Name == "Уведомление о строительстве объекта индивидуального жилищного строительства");
                        perm.Save();
                    }
                    if(!perm.CapitalStructureBase.Contains(constrObject))
                        perm.CapitalStructureBase.Add(constrObject);

                    try
                    {
                        if (!perm.ConstrStages.Contains(constrStage))
                            perm.ConstrStages.Add(constrStage);
                    }
                    catch { }
                    // записываем в семантику номер ОКС
                    try
                    {
                        m_MapInfo.Do("Update Selection Set НОМЕР_В_БАЗЕ =  " + GISobjNo + " Commit table " + layer);
                    }
                    catch { }

                    connect.GetUnitOfWork().CommitChanges();
                    try { spatial.AddSpatialLink(constrObject.ClassInfo.FullName, constrObject.Oid.ToString(), layer, mapObjInfos[0]); }
                    catch { }


                    // делаем пространственные запросы по ЗУ
                    try
                    {
                        //Сохранить выделенный объект в выборку
                        //TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
                        //m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));

                        m_MapInfo.Do("Select * From " + layer + " Where MI_PRINX = " + mapObjInfos[0] + " Into zObject");
                        // получаем слои для ЗУ
                        List<string> parcelLayersIds = spatial.GetListLinkedSpatialLayers("AISOGD.Land.Parcel");
                        foreach (string parcelLayer in parcelLayersIds)
                        {
                            // ищем пересечение с нашим объектом
                            m_MapInfo.Do("Select * from " + parcelLayer + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
                            int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                            for (int j = 1; j < selObjCount1 + 1; j++)
                            {
                                m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable");
                                // Id(MI_PRINX) пространственного объекта ЗУ
                                string parcelReqestObjID = m_MapInfo.Eval("tmpTable.MI_PRINX");
                                // находим ЗУ
                                //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
                                // объекта строительства, если нет - добавляем
                                // из таблицы связи получаем Ид реестрового объекта ЗУ
                                SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                    mc.SpatialLayerId == parcelLayer &&
                                    mc.SpatialObjectId == parcelReqestObjID &&
                                    mc.IsogdClassName == "AISOGD.Land.Parcel");
                                Parcel parcel = null;
                                if (objLink != null)
                                {
                                    // по ИД находим сам реестровый объект
                                    parcel = connect.FindFirstObject<Parcel>(mc => mc.Oid.ToString() == objLink.IsogdObjectId);

                                }
                                else
                                {
                                    try { parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber == m_MapInfo.Eval("tmpTable.CADASTRAL_NUMBER")); }
                                    catch { }
                                    if (parcel == null)
                                    {
                                        parcel = connect.CreateObject<Parcel>();
                                        try { parcel.CadastralNumber = m_MapInfo.Eval("tmpTable.CADASTRAL_NUMBER"); }
                                        catch { }
                                        parcel.Save();
                                    }
                                    spatial.AddSpatialLink(parcel.ClassInfo.FullName, parcel.Oid.ToString(), parcelLayer, parcelReqestObjID);
                                }
                                if (parcel != null)
                                {
                                    bool isParcelExist = false;

                                    if (parcel.Area == 0)
                                    {
                                        try
                                        {
                                            string area = m_MapInfo.Eval("tmpTable.AREA");
                                            if (area.Contains("+"))
                                                area = area.Split('+')[0];
                                            area = area.Trim();
                                            area = area.Replace(" ", "");
                                            parcel.Area = Convert.ToDouble(area);
                                        }
                                        catch { }
                                        try
                                        {
                                            parcel.Unit = connect.FindFirstObject<dUnit>(x => x.Name == "кв.м");
                                        }
                                        catch { }
                                        parcel.Save();
                                    }

                                    try {
                                        parcel.Adress = m_MapInfo.Eval("tmpTable.LOCATION");
                                        parcel.Save();
                                    }
                                    catch { }
                                    connect.GetUnitOfWork().CommitChanges();

                                    foreach (Parcel p in perm.Parcels)
                                    {
                                        if (parcel == p)
                                            isParcelExist = true;
                                    }
                                    if (!isParcelExist)
                                    {
                                        perm.Parcels.Add(parcel);
                                        perm.Save();
                                    }
                                    foreach (Parcel p in constrObject.Parcels)
                                    {
                                        if (parcel == p)
                                            isParcelExist = true;
                                    }
                                    if (!isParcelExist)
                                    {
                                        constrObject.Parcels.Add(parcel);
                                        constrObject.Save();
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                    perm.Save();
                    try
                    {
                        constrObject.Address = perm.ObjectAddress;
                        constrObject.Save();
                    }
                    catch { }
                    try {
                        constrStage.Address = perm.ObjectAddress;
                        constrStage.Save();
                    }
                    catch { }
                    connect.GetUnitOfWork().CommitChanges();
                }
            }
            return perm;
        }
    }
}
