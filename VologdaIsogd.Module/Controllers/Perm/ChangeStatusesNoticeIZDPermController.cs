﻿using System;
using System.Collections.Generic;
using System.Linq;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.General;
using AISOGD.Perm;
using AISOGD.Subject;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Win;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraEditors;
using VologdaIsogd.Module.Controllers.Delo;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ChangeStatusesNoticeIZDPermController : ViewController
    {
        private ChoiceActionItem noticeIZDPermAcceptAction;
        private ChoiceActionItem noticeIZDPermProlongueAction;
        private ChoiceActionItem noticeIZDPermRefuseAction;


        public ChangeStatusesNoticeIZDPermController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ChangeStatusesNoticeIZDPermChoiceAction.Items.Clear();
            noticeIZDPermAcceptAction =
               new ChoiceActionItem("Утвердить", null);
            ChangeStatusesNoticeIZDPermChoiceAction.Items.Add(noticeIZDPermAcceptAction);

            noticeIZDPermProlongueAction =
               new ChoiceActionItem("Продлить", null);
            ChangeStatusesNoticeIZDPermChoiceAction.Items.Add(noticeIZDPermProlongueAction);

            noticeIZDPermRefuseAction =
               new ChoiceActionItem("Отказ", null);
            ChangeStatusesNoticeIZDPermChoiceAction.Items.Add(noticeIZDPermRefuseAction);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ChangeStatusesNoticeIZDPermChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            PermNoticeIZD perm;
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            perm = os.FindObject<PermNoticeIZD>(new BinaryOperator("Oid", x_id));

            if(perm!= null)
            {
                if (e.SelectedChoiceActionItem == noticeIZDPermAcceptAction)
                {
                    NoticeIZDPermAcceptAction(os, connect, perm);
                }
                if (e.SelectedChoiceActionItem == noticeIZDPermProlongueAction)
                {
                    NoticeIZDPermProlongueAction(os, connect, perm, e);
                }
                if (e.SelectedChoiceActionItem == noticeIZDPermRefuseAction)
                {
                    NoticeIZDPermRefuseAction(os, connect, perm);
                }
            }
            
        }

        /// <summary>
        /// Утверждение уведомления
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void NoticeIZDPermAcceptAction(IObjectSpace os, Connect connect, PermNoticeIZD perm)
        {

            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            SynhronizeNoticeIZDDataController SynhronizeNoticeIZDDataController = new SynhronizeNoticeIZDDataController();
            if (perm.EDocStatus == AISOGD.Enums.eDocStatus.Отказ || perm.EDocStatus == AISOGD.Enums.eDocStatus.ОжиданиеУтвержденияВзамен ||
                perm.EDocStatus == AISOGD.Enums.eDocStatus.ПрекращениеДелопроизводства || perm.EDocStatus == AISOGD.Enums.eDocStatus.УтратилоДействие)
            {
                XtraMessageBox.Show("Документ с данным статусом нельзя утвердить!");
                return;
            }
            if (perm.DocDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Не указана дата документа");
                return;
            }
            if (String.IsNullOrWhiteSpace(perm.DocNo))
            {
                XtraMessageBox.Show("Необходимо указать корректный номер документа");
                return;
            }
            PermNoticeIZD anotherPerm = connect.FindFirstObject<PermNoticeIZD>(x => x.DocNo == perm.DocNo && x.DocDate == perm.DocDate);
            if (perm.Oid.ToString() != anotherPerm.Oid.ToString())
            {
                XtraMessageBox.Show("В Системе уже внесен другой документ с подобными реквизитам.");
                return;
            }

            perm.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден;

            try
            {
                perm.FullDocNo = perm.DocKind.Prefix + perm.DocNo + perm.DocKind.Suffix;
            }
            catch { }
            string temp = "";
            //if (perm.PermKind == AISOGD.Enums.ePermKind.Взамен)
            //{
            //    temp = " (взамен)";
            //    try { perm.ConstrPermInstead.EDocStatus = AISOGD.Enums.eDocStatus.УтратилоДействие; }
            //    catch { }
            //}
            string docNo = perm.DocNo;
            if (docNo.Length == 1)
                docNo = "00" + docNo;
            if (docNo.Length == 2)
                docNo = "0" + docNo;
            // проставляем номер на карте для объекта строительства
            if (perm.PermKind != AISOGD.Enums.ePermKind.Взамен)
            {
                try
                {
                    if (perm.CapitalStructureBase[0].MapNo == null || perm.CapitalStructureBase[0].MapNo == "")
                        perm.CapitalStructureBase[0].MapNo = $"{perm.DocDate.Year.ToString().Remove(0, 2)}{docNo}_у";//.Replace("35-32357000-", "").Split('-')[0];
                }
                catch { }
            }

            os.CommitChanges();
            // проставляем номер на карте для РС
            try
            {
                if (perm.MapNo == null || perm.MapNo == "")
                    perm.MapNo = perm.CapitalStructureBase[0].MapNo;
            }
            catch { }
            foreach (CapitalStructureBase constrObj in perm.CapitalStructureBase)
            {
                constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.Стройка;

                try
                {
                    constrObj.FloorCountProject = Convert.ToInt16(perm.FloorCount);
                }
                catch { }
                try
                {
                    constrObj.HeightProject = Convert.ToDouble(perm.Height);
                }
                catch { }
                try
                {
                    constrObj.BuildSquareProject = Convert.ToDouble(perm.BuildSquare);
                }
                catch { }
                try
                {
                    string constrPermInfo = String.Format("Уведомление на строительство от {0} №{1}{2}", perm.DocDate.ToShortDateString(), perm.DocNo, temp);
                    if (String.IsNullOrWhiteSpace(constrObj.ConstrPermInfo))
                        constrObj.ConstrPermInfo = constrPermInfo;
                    else
                        if (!constrObj.ConstrPermInfo.Contains(constrPermInfo))
                        constrObj.ConstrPermInfo += ", " + constrPermInfo;
                }
                catch { }
                // заполняем дату начала строительства, если это первое разрешение на РС
                if (constrObj.StartConstrDate == DateTime.MinValue)
                    constrObj.StartConstrDate = perm.DocDate;
                // проставляем дату последнего РС в объекте строительства
                try
                {
                    constrObj.ConstrPermLastDate = perm.DocDate;
                }
                catch { }
                // проставляем месяц и год планируемого ввода
                try
                {
                    constrObj.PlaningYearInUse = perm.ValidDateTotal.Year;
                }
                catch { }
                try
                {
                    switch (perm.ValidDateTotal.Month)
                    {
                        case 1:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Январь;
                            break;
                        case 2:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Февраль;
                            break;
                        case 3:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Март;
                            break;
                        case 4:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Апрель;
                            break;
                        case 5:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Май;
                            break;
                        case 6:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Июнь;
                            break;
                        case 7:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Июль;
                            break;
                        case 8:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Август;
                            break;
                        case 9:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Сентябрь;
                            break;
                        case 10:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Октябрь;
                            break;
                        case 11:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Ноябрь;
                            break;
                        case 12:
                            constrObj.eMonth = AISOGD.Enums.eMonths.Декабрь;
                            break;

                    }
                }
                catch { }
                // заполняем данные из разрешения по ОКС (по застройщикам и виду работ), если еще не заполнили
                try
                {
                    foreach (GeneralSubject subject in perm.DocSubjects)
                    {
                        if (!constrObj.DocSubjects.Contains(subject))
                            constrObj.DocSubjects.Add(subject);
                    }
                }
                catch { }
                try {
                    if (perm.IZDAim == AISOGD.Enums.eIZDAim.Строительство)
                        constrObj.WorkKind = AISOGD.Enums.eWorkKind.Строительство;
                    else
                        constrObj.WorkKind = AISOGD.Enums.eWorkKind.Реконструкция;
                }
                catch { }

                // данные по этапу
                try
                {
                    ConstrStage constrStage = constrObj.ConstrStages[0];
                    constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.Стройка;
                    try
                    {
                        constrStage.FloorCount = Convert.ToInt16(perm.FloorCount);
                    }
                    catch { }
                    try
                    {
                        constrStage.Height = Convert.ToDouble(perm.Height);
                    }
                    catch { }
                    try
                    {
                        constrStage.BuildSquare = Convert.ToDouble(perm.BuildSquare);
                    }
                    catch { }

                    constrStage.Address = constrObj.Address ?? "";

                    constrStage.BuildingKind = constrObj.BuildingKind;

                    constrStage.WorkKind = constrObj.WorkKind;

                    try
                    {
                        constrStage.ConstructionType = connect.FindFirstObject<dConstructionType>(x => x.Oid == constrObj.ConstructionType.Oid);
                    }
                    catch { }
                    try
                    {
                        constrStage.ConstructionCategory = connect.FindFirstObject<dConstructionType>(x => x.Oid == constrObj.ConstructionCategory.Oid);
                    }
                    catch { }
                    try
                    {
                        constrStage.ConstrPermInfo = constrObj.ConstrPermInfo;
                    }
                    catch { }
                    constrStage.MapNo = constrObj.MapNo?? "";
                    constrStage.Save();
                }
                catch { }
                // заполняем семантику
                // из таблицы связи получаем Ид реестрового объекта ЗУ
                SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.CapitalStructureBase", constrObj.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                if (objLink != null)
                {
                    try
                    {
                        SynhronizeNoticeIZDDataController.SetConstrObjSemantic(m_MapInfo, constrObj, perm, objLink.SpatialLayerId, objLink.SpatialObjectId);
                    }
                    catch { }
                }
                constrObj.Save();
            }
            foreach (InLetter letter in perm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            perm.Save();
            os.CommitChanges();

            // создаем документ для регистрации в ИСОГД 
            try
            {
                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = perm.FullDocNo; }
                catch { }

                try { idocDate = perm.DocDate; }
                catch { }

                try { icadNo = perm.LotCadNo; }
                catch { }

                try { iaddrInfo = perm.ObjectAddress; }
                catch { }
                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in perm.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }
                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "060605",
@"уведомление о соотв. ук-х в уведомлении о план-м стр-е параметров ИЖС или сад. дома устан-м параметрам и допустимости разм-я объекта ИЖС или сад. дома на ЗУ",
                    icadNo, iaddrInfo, attachs);
            }
            catch { }
            os.CommitChanges();
            RefreshAllWindows();
        }


        /// <summary>
        /// Продление уведомления
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void NoticeIZDPermProlongueAction(IObjectSpace os, Connect connect, PermNoticeIZD perm, SingleChoiceActionExecuteEventArgs e)
        {

            if (perm.EDocStatus != AISOGD.Enums.eDocStatus.Утвержден &&
                //constrPerm.EDocStatus != AISOGD.Enums.eDocStatus.Продление &&
                perm.EDocStatus != AISOGD.Enums.eDocStatus.УтратилоДействиеПоСроку)
            {
                XtraMessageBox.Show("Продлить можно только документ со статусом 'Утвержден' или 'Утратил действие по сроку'!");
                return;
            }

            PermProlongue permProlongue = connect.CreateObject<PermProlongue>();
            permProlongue.PermNoticeIZD = perm;
            permProlongue.DateProlongue = DateTime.Now.Date;
            perm.PermKind = AISOGD.Enums.ePermKind.Продление;
            foreach (InLetter letter in perm.InLetter)
            {
                if (letter.InLetterStatus == AISOGD.Enums.eInLetterStatus.Вработе)
                {
                    letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                    letter.Save();
                }
            }
            perm.Save();
            permProlongue.Save();
            os.CommitChanges();
            RefreshAllWindows();
            // Открываем окно продления
            DetailView dv = Application.CreateDetailView(os, permProlongue);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;

            View.ObjectSpace.CommitChanges();
        }

        /// <summary>
        /// Отказ уведомления
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void NoticeIZDPermRefuseAction(IObjectSpace os, Connect connect, PermNoticeIZD perm)
        {
            if (perm.EDocStatus != AISOGD.Enums.eDocStatus.Подготовка)
            {
                XtraMessageBox.Show("Можно отменить только документ со статусом 'Подготовка'!");
                return;
            }
            perm.EDocStatus = AISOGD.Enums.eDocStatus.Отказ;
            //if (reason == "Отказ")
            //    perm.EDocStatus = AISOGD.Enums.eDocStatus.Отказ;
            //if (reason == "Прекращение делопроизводства")
            //    constrPerm.EDocStatus = AISOGD.Enums.eDocStatus.ПрекращениеДелопроизводства;
            //if (constrPerm.PermKind == AISOGD.Enums.ePermKind.Взамен)
            //{
            //    try { constrPerm.ConstrPermInstead.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден; }
            //    catch { }
            //}
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            foreach (CapitalStructureBase oks in perm.CapitalStructureBase)
            {
                oks.ConstrStageState = AISOGD.Enums.eConstrStageState.Отмена;
                oks.Save();
                try
                {
                    SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.CapitalStructureBase", oks.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                    m_MapInfo.Do("Select * From " + objLink.SpatialLayerId + " Where MI_PRINX = " + objLink.SpatialObjectId);
                    string mapNo = "";
                    if (oks.MapNo != null)
                        mapNo = oks.MapNo;
                    m_MapInfo.Do("Update Selection Set НОМЕР_НА_СХЕМЕ =  \"" + mapNo + "_отказ" + "\" Commit table " + objLink.SpatialLayerId);
                    try
                    {
                        m_MapInfo.Do("Update Selection Set СТАТУС =  \"" + oks.ConstrStageState + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                }
                catch { }

                try {
                    ConstrStage constrStage = oks.ConstrStages[0];
                    constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.Отмена;
                    constrStage.Save();
                }
                catch { }
            }

            foreach (InLetter letter in perm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            perm.Save();
            // создаем документ для регистрации в ИСОГД 
            try
            {
                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = perm.FullDocNo; }
                catch { }

                try { idocDate = perm.DocDate; }
                catch { }

                try { icadNo = perm.LotCadNo; }
                catch { }

                try { iaddrInfo = perm.ObjectAddress; }
                catch { }
                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in perm.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }
                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "060605",
@"уведомление о НЕсоотв. ук-х в уведомлении о план-м стр-е параметров объекта ИЖС или сад. дома устан-м параметрам и (или) недопустимости разм-я объекта ИЖС или сад. дома на ЗУ",
                    icadNo, iaddrInfo, attachs);
            }
            catch { }
            os.CommitChanges();
            RefreshAllWindows();
        }

        // обновить все окна
        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }
    }
}
