﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Constr;
using AISOGD.Subject;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateUsePermDocController : ViewController
    {
        public CreateUsePermDocController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        public UsePerm CreateUsePermDoc(Connect connect)
        {
            UsePerm usePerm = null;
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();


            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return null;
            }

            string layer = "";
            // получаем название слоя выделения
            SelectionTable selectionTable = m_MapInfo.SelectionTable;// new SelectionTable(m_MapInfo);
            layer = selectionTable.Name;
            TableInfoTemp startSelectObjects = new TableInfoTemp(m_MapInfo, ETempTableType.Temp);
            m_MapInfo.Do(String.Format("Select * From Selection into {0}", startSelectObjects.Name));
            if (layer == "")
            {
                XtraMessageBox.Show("Не удалось получить название таблицы выделенных объектов. Пожалуйста, обратитесь к администратору Системы!", "Ошибка");
                return null;
            }
            if (layer != "")
            {
                List<ConstrStage> ConstrStagesToInUse = new List<ConstrStage>();
                int selObjCount = selectionTable.GetCount().Value; // Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                string objMapNumber = "";
                // перебираем выделенные на карте объекты этапов, ищем их связанные реестровые объекты и создаем у них разрешение на ввод
                for (int j = 1; j < selObjCount + 1; j++)
                {
                    m_MapInfo.Fetch(EFetch.Rec, startSelectObjects.Name, j);
                    //m_MapInfo.Do("Fetch Rec " + j.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = selectionTable.GetFirstID();//m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // ищем, есть ли у выделенного объекта созданный реестровый. Если нет - сообщаем об этом
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                XtraMessageBox.Show("У ГИС объекта MI_PRINX: " + gisObjId
                                    + " нет связанного реестрового объекта этапа! Разрешение на ввод можно создать только у зарегистрированных этапов!", "Ошибка");
                                return null;
                            }
                            // находим связанный реестровый объект этапа
                            foreach (SpatialRepository sprepo in spatial.GetIsogdLinks(layer, gisObjId))
                            {
                                ConstrStage constrStage = connect.FindFirstObject<ConstrStage>(x => x.Oid.ToString() == sprepo.IsogdObjectId);
                                if (constrStage == null)
                                {
                                    XtraMessageBox.Show("У ГИС объекта MI_PRINX: " + gisObjId
                                    + " нет связанного реестрового объекта этапа! Разрешение на ввод можно создать только у зарегистрированных этапов!", "Ошибка");
                                    return null;
                                }

                                // проверяем, введен ли уже этап
                                if (constrStage.ConstrStageState == AISOGD.Enums.eConstrStageState.Введено)
                                {
                                    XtraMessageBox.Show("Этап: " + constrStage.Name 
                                    + " уже введен! Проверьте правильность выбранных для введения этапов", "Ошибка");
                                    return null;
                                }
                                // вносим прошедший все проверки этап в лист для дальнейшей с ним работы
                                ConstrStagesToInUse.Add(constrStage);
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show("Имеются несохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                            return null;
                        }
                    }
                    catch { }
                }

                // если все нормально - создаем разрешение на ввод и связываем со всеми 
                // 1. этапами
                // 2. объектами строительства на этапах
                // 3. разрешениями на строительства на этапах

                usePerm = connect.CreateObject<UsePerm>();
                usePerm.EDocStatus = AISOGD.Enums.eDocStatus.Подготовка;
                try
                {
                    usePerm.BuildingKind = ConstrStagesToInUse[0].BuildingKind;
                }
                catch { }
                //ObjectConstractionInfo usePermConstrInfo = connect.CreateObject<ObjectConstractionInfo>();
                //usePerm.ObjectConstractionInfo.Add(usePermConstrInfo);
                //usePermConstrInfo.Save();
                foreach (ConstrStage constrStage in ConstrStagesToInUse)
                {
                    usePerm.ConstrStages.Add(constrStage);
                    try { CapitalStructureBase constrObj = constrStage.CapitalStructureBase;
                        usePerm.CapitalStructureBase.Add(constrObj);
                        try
                        {
                            usePerm.BuildingKind = constrObj.BuildingKind;
                            //usePermConstrInfo.BuildingKind = constrObj.BuildingKind;
                        }
                        catch { }
                        try
                        {
                            if (usePerm.MapNo == null || usePerm.MapNo == "")
                                usePerm.MapNo = constrObj.MapNo;
                        }
                        catch { }
                        try
                        {
                           usePerm.ConstructionType = constrObj.ConstructionType;
                        }
                        catch { }
                    }
                    catch { }
                    
                    foreach (ConstrPerm constrPerm in constrStage.ConstrPerms)
                    {
                        if(constrPerm.EDocStatus == AISOGD.Enums.eDocStatus.Утвержден)
                            usePerm.ConstrPerm.Add(constrPerm);
                        //foreach (GeneralSubject dev in )
                    }

                    // заполняем проектные характеристики РВ по проектным характеристикам стадий
                    
                        #region Общие показатели
                        try { usePerm.BuildingSizeProject += constrStage.BuildingSizeProject; }
                        catch { }
                        try { usePerm.BuildingSizeOverGroundPartProject += constrStage.BuildingSizeOverGroundPartProject; }
                        catch { }
                        try { usePerm.BuildingSizeUnderGroundPartProject += constrStage.BuildingSizeUnderGroundPartProject; }
                        catch { }
                        try { usePerm.TotalBuildSquareProject += constrStage.TotalBuildSquareProject; }
                        catch { }
                        try { usePerm.BuildSquareProject += constrStage.BuildSquare; }
                        catch { }
                        try { usePerm.HeightProject += constrStage.Height; }
                        catch { }
                        try { usePerm.NotLivingSquareProject += constrStage.NotLivingSquareProject; }
                        catch { }
                        try { usePerm.OutBuildingSquareProject += constrStage.OutBuildingSquareProject; }
                        catch { }
                        try { usePerm.BuildingCountProject += constrStage.BuildingCountProject; }
                        catch { }
                    #endregion

                    #region Этажность 
                    try { usePerm.FloorCountProject += constrStage.FloorCount; }
                    catch { }
                    try { usePerm.UnderGroundFloorCounProject += constrStage.UnderGroundFloorCount; }
                    catch { }
                    try { usePerm.FloorCountAboveProject += constrStage.FloorCountAbove; }
                    catch { }
                    try { usePerm.FloorCountAboveProject += constrStage.FloorCountAbove; }
                    catch { }
                    try { usePerm.FloorCountDiffProject += constrStage.FloorCountDiff + Environment.NewLine; }
                    catch { }
                    try { usePerm.FloorsNoteProject += constrStage.FloorsNote + Environment.NewLine; }
                    catch { }
                    #endregion


                    #region Лифты, Эскалаторы, подъемники для ОКС
                    try { usePerm.ElevatorsCountProject += constrStage.ElevatorsCountProject; }
                        catch { }
                        try { usePerm.EscalatorsCountProject += constrStage.EscalatorsCountProject; }
                        catch { }
                        try { usePerm.InvalidLiftsCountProject += constrStage.InvalidLiftsCountProject; }
                        catch { }
                        #endregion

                        #region Материалы
                        try
                        {
                            if (usePerm.FundMaterialProject == null)
                                usePerm.FundMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.FundMaterialProject.Oid);
                        }
                        catch { }
                        try
                        {
                            if (usePerm.WallMaterialProject == null)
                                usePerm.WallMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.WallMaterialProject.Oid);
                        }
                        catch { }
                        try
                        {
                            if (usePerm.BorderMaterialProject == null)
                                usePerm.BorderMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.BorderMaterialProject.Oid);
                        }
                        catch { }
                        try
                        {
                            if (usePerm.RoofMaterialProject == null)
                                usePerm.RoofMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.RoofMaterialProject.Oid);
                        }
                        catch { }


                        #endregion

                        #region Сети и системы инженерно технического обслуживания
                        try
                        {
                            if (constrStage.ElectroProject == true)
                                usePerm.ElectroProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.HeatProject == true)
                                usePerm.HeatProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.WaterProject == true)
                                usePerm.WaterProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.GazProject == true)
                                usePerm.GazProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.HouseSeverageProject == true)
                                usePerm.HouseSeverageProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.PhonesProject == true)
                                usePerm.PhonesProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.TVProject == true)
                                usePerm.TVProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.RadioProject == true)
                                usePerm.RadioProject = true;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.SeverageProject == true)
                                usePerm.SeverageProject = true;
                        }
                        catch { }
                        #endregion

                        #region Иные показатели
                        try
                        {
                            usePerm.OtherIndicatorsProject += constrStage.OtherIndicatorsProject + Environment.NewLine;
                            usePerm.OtherIndicatorsProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region нежилые
                        try { usePerm.PlacesCountProject += constrStage.PlacesCountProject; }
                        catch { }
                        try { usePerm.RoomCountProject += constrStage.RoomCountProject; }
                        catch { }
                        try { usePerm.CapacityProject += constrStage.CapacityProject; }
                        catch { }
                        #endregion

                        #region жилые
                        try { usePerm.LivingSpaceProject += constrStage.LivingSpaceProject; }
                        catch { }
                        try { usePerm.TotalLivingSpaceProject += constrStage.TotalLivingSpaceProject; }
                        catch { }
                        try { usePerm.NotLivingSpaceProject += constrStage.NotLivingSpaceProject; }
                        catch { }
                        try { usePerm.SectionCountProject += constrStage.SectionCountProject; }
                        catch { }
                        try { usePerm.PorchCountProject += constrStage.PorchCountProject; }
                        catch { }
                        try { usePerm.AppartmentsCountProject += constrStage.AppartmentsCountProject; }
                        catch { }

                        try { usePerm.StudioCountProject += constrStage.StudioCountProject; }
                        catch { }
                        try { usePerm.StudioSpaceProject += constrStage.StudioSpaceProject; }
                        catch { }
                        try { usePerm.OneRoomFlatCountProject += constrStage.OneRoomFlatCountProject; }
                        catch { }
                        try { usePerm.OneRoomFlatSpaceProject += constrStage.OneRoomFlatSpaceProject; }
                        catch { }
                        try { usePerm.TwoRoomFlatCountProject += constrStage.TwoRoomFlatCountProject; }
                        catch { }
                        try { usePerm.TwoRoomFlatSpaceProject += constrStage.TwoRoomFlatSpaceProject; }
                        catch { }
                        try { usePerm.ThreeRoomFlatCountProject += constrStage.ThreeRoomFlatCountProject; }
                        catch { }
                        try { usePerm.ThreeRoomFlatSpaceProject += constrStage.ThreeRoomFlatSpaceProject; }
                        catch { }
                        try { usePerm.FourRoomFlatCountProject += constrStage.FourRoomFlatCountProject; }
                        catch { }
                        try { usePerm.FourRoomSpaceProject += constrStage.FourRoomSpaceProject; }
                        catch { }
                        try { usePerm.MoreRoomFlatCountProject += constrStage.MoreRoomFlatCountProject; }
                        catch { }
                        try { usePerm.MoreRoomSpaceProject += constrStage.MoreRoomSpaceProject; }
                        catch { }
                        try { usePerm.ElitAppartmentsCountProject += constrStage.ElitAppartmentsCountProject; }
                        catch { }
                        try { usePerm.ElitAppartmentsSpaceProject += constrStage.ElitAppartmentsSpaceProject; }
                        catch { }
                        #endregion

                        #region Объекты производственного назначения
                        try
                        {
                            if (usePerm.ProductPurposeObjectType == null)
                                usePerm.ProductPurposeObjectType = connect.FindFirstObject<ProductPurposeObjectType>(x => x.Oid == constrStage.ProductPurposeObjectType.Oid);
                        }
                        catch { }
                        try
                        {
                            usePerm.PowerProject = "";
                            usePerm.PowerProject += constrStage.PowerProject + Environment.NewLine;
                            usePerm.PowerProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            usePerm.ProductivityProject = "";
                            usePerm.ProductivityProject += constrStage.ProductivityProject + Environment.NewLine;
                            usePerm.ProductivityProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region  Линейные объекты
                        try
                        {
                            if (usePerm.LineObjectClass == null)
                                usePerm.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid == constrStage.LineObjectClass.Oid);
                        }
                        catch { }
                        try { usePerm.LengthProject += constrStage.LengthProject; }
                        catch { }
                        try
                        {
                            usePerm.PowerLineProject = "";
                            usePerm.PowerLineProject += constrStage.PowerLineProject + Environment.NewLine;
                            usePerm.PowerLineProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            usePerm.PipesInfoProject = "";
                            usePerm.PipesInfoProject += constrStage.PipesInfoProject + Environment.NewLine;
                            usePerm.PipesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            usePerm.ElectricLinesInfoProject = "";
                            usePerm.ElectricLinesInfoProject += constrStage.ElectricLinesInfoProject + Environment.NewLine;
                            usePerm.ElectricLinesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            usePerm.ConstructiveElementsInfoProject = "";
                            usePerm.ConstructiveElementsInfoProject += constrStage.ConstructiveElementsInfoProject + Environment.NewLine;
                            usePerm.ConstructiveElementsInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion

                        #region Соответствие требованиям энергетической эффективности
                        try
                        {
                            if (usePerm.EnergyEfficiencyClassProject == null)
                                usePerm.EnergyEfficiencyClassProject = connect.FindFirstObject<EnergyEfficiencyClass>(x => x.Oid == constrStage.EnergyEfficiencyClassProject.Oid);
                        }
                        catch { }
                    try
                    {
                        if (usePerm.HeatUnit == null)
                            usePerm.HeatUnit = connect.FindFirstObject<dHeatUnit>(x => x.Oid == constrStage.HeatUnit.Oid);
                    }
                    catch { }
                    try { usePerm.HeatConsumptionProject += constrStage.HeatConsumptionProject; }
                        catch { }
                        try
                        {
                            usePerm.OutdoorIsolationMaterialProject = "";
                            usePerm.OutdoorIsolationMaterialProject += constrStage.OutdoorIsolationMaterialProject + Environment.NewLine;
                            usePerm.OutdoorIsolationMaterialProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        try
                        {
                            usePerm.SkylightsFillingProject = "";
                            usePerm.SkylightsFillingProject += constrStage.SkylightsFillingProject + Environment.NewLine;
                            usePerm.SkylightsFillingProject.TrimEnd(Environment.NewLine.ToCharArray());
                        }
                        catch { }
                        #endregion
                }
                usePerm.Save();
            }
            return usePerm;
        }
    }
}
