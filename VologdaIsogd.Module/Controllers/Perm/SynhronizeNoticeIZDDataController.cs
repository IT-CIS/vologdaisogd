﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AISOGD.Constr;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SynhronizeNoticeIZDDataController : ViewController
    {
        public SynhronizeNoticeIZDDataController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция заполнения семантики у стадии
        /// </summary>
        /// <param name="aMapInfo"></param>
        /// <param name="constrStage"></param>
        /// <param name="aLayer"></param>
        public void SetConstrObjSemantic(MapInfoApplication aMapInfo, CapitalStructureBase constrObj, PermNoticeIZD perm, string aLayer, string aMI_PRINX)
        {
            aMapInfo.Do("Select * From " + aLayer + " Where MI_PRINX = " + aMI_PRINX);// + " Into zObject");
            try
            {
                aMapInfo.Do("Update Selection Set НОМЕР_НА_СХЕМЕ =  \"" + constrObj.MapNo + "\" Commit table " + aLayer);
            }
            catch { }
            //try
            //{
            //    aMapInfo.Do("Update Selection Set ОЧЕРЕДЬ_СТР =  \"" + constrObj.StageNo + "\" Commit table " + aLayer);
            //}
            //catch { }

            try
            {
                aMapInfo.Do("Update Selection Set ОПИСАНИЕ =  \"" + constrObj.Name + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set СТРОИТ_АДРЕС =  \"" + constrObj.Address + "\" Commit table " + aLayer);
            }
            catch { }
            
            try
            {
                aMapInfo.Do("Update Selection Set СТАТУС =  \"" + constrObj.ConstrStageState + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set МЕСТОПОЛОЖЕНИЕ =  \"" + constrObj.Address.Split(',')[0] + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set ПРОЕКТ_НОМЕР_ДОМА =  \"" + constrObj.Address.Split(',')[constrObj.Address.LastIndexOf(',')] + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set КОД_ОБЪЕКТ =  \"" + constrObj.ConstructionType.Name + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                if (constrObj.FloorCountFakt != 0)
                    aMapInfo.Do("Update Selection Set МАКС_ЭТАЖН =  " + constrObj.FloorCountFakt + " Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set РАЗНОЭТАЖНОСТЬ =  \"" + constrObj.FloorCountDiffFakt + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                if (constrObj.AppartmentsCountProject != 0)
                    aMapInfo.Do("Update Selection Set КОЛ_КВАРТ =  " + constrObj.AppartmentsCountProject + " Commit table " + aLayer);
            }
            catch { }

            //try
            //{
            //    aMapInfo.Do("Update Selection Set СТР_МАТЕРИАЛ_СТЕН =  \"" + constrObj.WallMaterialProject.Name + "\" Commit table " + aLayer);
            //}
            //catch { }

            try
            {
                aMapInfo.Do("Update Selection Set ВИД_РАБОТ =  \"" + constrObj.WorkKind + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ЗАСТРОЙЩИК =  \"" + constrObj.ConstrDeveloperString.Replace("\"", "'").Replace("«", "'").Replace("»", "'") + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set РЕКВИЗИТЫ_ЗАСТРОЙЩИКА =  \"" + constrObj.ConstrDeveloperContactInfo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ДАТА_ВНЕС_ИНФ = \"" + DateTime.Now.Date.ToShortDateString() + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                //string p = "";
                //foreach (ConstrPerm perm in constrStage.ConstrPerms)
                //{
                //    try
                //    {
                //        string n = "";
                //        string d = "";

                //        try { n = " №" + constrPerm.DocNo; }
                //        catch { }
                //        try { d = "от " + constrPerm.DocDate.ToShortDateString(); }
                //        catch { }
                //        if (!p.Contains(d+ n))
                //            p += d + n + ", ";
                //    }
                //    catch { }
                //}
                //p = p.Trim();
                //p = p.TrimEnd(',');
                aMapInfo.Do("Update Selection Set УВЕДОМЛЕНИЕ_СТР = \"" + constrObj.ConstrPermInfo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ОЖИД_ГОД_ВВОД = " + constrObj.PlaningYearInUse + " Commit table " + aLayer);
            }
            catch { }
            try
            {
                //string validDate = "";
                //foreach (ConstrPerm perm in constrStage.ConstrPerms)
                //{
                //    try
                //    {
                //        if (!validDate.Contains(perm.ValidDate.ToShortDateString()))
                //            validDate += perm.ValidDate.ToShortDateString() + ", ";
                //    }
                //    catch { }

                //}
                //validDate = validDate.Trim();
                //validDate = validDate.TrimEnd(',');
                aMapInfo.Do("Update Selection Set СРОК_РАЗРЕШ = \"" + perm.ValidDate.ToShortDateString() + "\" Commit table " + aLayer);
            }
            catch { }
        }
    }
}
