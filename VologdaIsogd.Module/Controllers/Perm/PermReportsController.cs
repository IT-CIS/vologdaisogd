﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using AISOGD.Perm;
using DevExpress.XtraEditors;
using DevExpress.Spreadsheet;
using System.Reflection;
using System.IO;
using System.Drawing;
using DevExpress.Xpo;
using AISOGD.Enums;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.Windows.Forms;
using AISOGD.Subject;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PermReportsController : ViewController
    {
        public PermReportsController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            Frame.GetController<PermReportsController>().UsePermElitReportAction.Active.SetItemValue("myReason", false);
            Frame.GetController<PermReportsController>().CreateConstrPermReportParamAction.Active.SetItemValue("myReason", false);

            Frame.GetController<PermReportsController>().CreateUsePermElitReportAction.Active.SetItemValue("myReason", false);
            Frame.GetController<PermReportsController>().CreateConstrPermReportAction.Active.SetItemValue("myReason", false);
            Frame.GetController<PermReportsController>().CreateFNSPermReportAction.Active.SetItemValue("myReason", false);
            if (View is DevExpress.ExpressApp.ListView)
            {
                if (View.Id == "ConstrPerm_ListView" ||
                    View.Id == "UsePerm_ListView")
                {
                    Frame.GetController<PermReportsController>().CreateConstrPermReportAction.Active.SetItemValue("myReason", true);
                    if (View.Id == "UsePerm_ListView")
                    {
                        Frame.GetController<PermReportsController>().CreateUsePermElitReportAction.Active.SetItemValue("myReason", true); 
                        Frame.GetController<PermReportsController>().CreateFNSPermReportAction.Active.SetItemValue("myReason", true);
                    }
                }
                //else
                //{
                //    Frame.GetController<PermReportsController>().CreateConstrPermReportAction.Active.SetItemValue("myReason", false);
                //}
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Подготовка отчета в статистику
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateConstrPermReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            SetDateTimeAndCreateReports(eReportType.статистика);
        }

        /// <summary>
        /// Подготовка отчета по элитному жилью
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateUsePermElitReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            SetDateTimeAndCreateReports(eReportType.элитжилье);
        }

        /// <summary>
        /// Подготовка отчета в налоговую
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateFNSPermReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            SetDateTimeAndCreateReports(eReportType.налоговая);
        }

        /// <summary>
        /// Указать даты отчета
        /// </summary>
        /// <param name="reportType"></param>
        private void SetDateTimeAndCreateReports(eReportType reportType)
        {
            var form = new SetDatesForPermReportsForm()
            {
                StartPosition = FormStartPosition.CenterParent,
            };
            form.ShowDialog();
            var result = form.DialogResult;
            if (result == DialogResult.Cancel)
            {
                return;
            }

            if (result == DialogResult.OK)
            {
                switch (reportType)
                {
                    case eReportType.налоговая:
                        CreateFNSPermReport(form.startDateEdit.DateTime, form.finishDateEdit.DateTime);
                        break;
                    case eReportType.элитжилье:
                        CreateUsePermElitReport(form.startDateEdit.DateTime, form.finishDateEdit.DateTime);
                        break;
                    case eReportType.статистика:
                        FormPermReport(form.startDateEdit.DateTime, form.finishDateEdit.DateTime);
                        break;
                }
            }
        }

        
        /// <summary>
        /// Сформировать отчет в статистику
        /// </summary>
        private void FormPermReport(DateTime startReportDate, DateTime FinishReportDate)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(objectSpace);

            


            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\PermReport.xlsx");
            workbook.LoadDocument(templatePath, DevExpress.Spreadsheet.DocumentFormat.OpenXml);

            // первый лист 
            Worksheet worksheet = workbook.Worksheets[0];
            Cell cell;
            cell = worksheet.Cells["B9"];
            string month = "";
            switch (startReportDate.Month)
            {
                case 1:
                    month = "январь";
                    break;
                case 2:
                    month = "февраль";
                    break;
                case 3:
                    month = "март";
                    break;
                case 4:
                    month = "апрель";
                    break;
                case 5:
                    month = "май";
                    break;
                case 6:
                    month = "июнь";
                    break;
                case 7:
                    month = "июль";
                    break;
                case 8:
                    month = "август";
                    break;
                case 9:
                    month = "сентябрь";
                    break;
                case 10:
                    month = "октябрь";
                    break;
                case 11:
                    month = "ноябрь";
                    break;
                case 12:
                    month = "декабрь";
                    break;
                default:
                    month = DateTime.Now.Month.ToString();
                    break;
            }
            cell.Value = String.Format("за {0} {1} г.", month, startReportDate.Year.ToString());

            // второй лист с информацией по РС
            worksheet = workbook.Worksheets[1];
            //Cell cell;
            cell = worksheet.Cells["E2"];
            cell.Value = month;

            cell = worksheet.Cells["F2"];
            cell.Value = startReportDate.Year.ToString();

            int constrPermCount = 0;
            //IQueryable ConstrPermList1 = connect.FindObjects<ConstrPerm>(x => x.DocDate.Year == ReportDate.Year && x.DocDate.Month == ReportDate.Month);
            //XtraMessageBox.Show(ConstrPermList);
            List<SortProperty> sortprops = new List<SortProperty>();
            SortProperty sortprop = new SortProperty("DocNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
            sortprops.Add(sortprop);
            var ConstrPermList = objectSpace.GetObjects<ConstrPerm>(CriteriaOperator.Parse("DocDate >= ? And DocDate <= ? And EDocStatus=?",//(String.Format("DocDate >= {0} And DocDate <= {1} And EDocStatus={2}",
                startReportDate, FinishReportDate, 4), sortprops, false);
            //var ConstrPermList = objectSpace.GetObjects<ConstrPerm>(CriteriaOperator.Parse(String.Format("GetYear([DocDate]) = {0} And GetMonth([DocDate]) = {1} And EDocStatus={2}",
            //    ReportDate.Year, ReportDate.Month, 4)), sortprops, false);
            //new GroupOperator(GroupOperatorType.And, 
            //new BinaryOperator("DocDate.Year", ReportDate.Year), new BinaryOperator("DocDate.Month", ReportDate.Month)), sortprops, false);
            foreach (ConstrPerm constrPerm in ConstrPermList.OrderBy(x=>x.DocNo))
            {
                constrPermCount++;

                // добавляем строку и форматируем ее
                worksheet.Rows.Insert(constrPermCount + 6);
                Range range = worksheet.Range[String.Format("A{0}:J{0}", (constrPermCount + 6).ToString())];
                Formatting rangeFormatting = range.BeginUpdateFormatting();
                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                rangeFormatting.Font.Name = "Times New Roman";
                rangeFormatting.Font.Size = 13;
                //rangeFormatting.Alignment.ShrinkToFit = true;
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //range.AutoFitColumns();
                range.EndUpdateFormatting(rangeFormatting);

                // информация по застройщику
                string DeveloperInfo = "";
                cell = worksheet.Cells["A" + (constrPermCount + 6).ToString()];
                try {
                    DeveloperInfo = constrPerm.ConstrDeveloperString;
                }
                catch { }
                try
                {
                    DeveloperInfo += ", " + constrPerm.ConstrDeveloperContactInfo;
                }
                catch { }
                cell.Value = DeveloperInfo;

                // ИНН застройщика
                string DeveloperINN = "";
                cell = worksheet.Cells["B" + (constrPermCount + 6).ToString()];
                try
                {
                    DeveloperINN = constrPerm.ConstrDeveloperINN;
                }
                catch { }
                cell.Value = DeveloperINN;

                // п\п
                cell = worksheet.Cells["C" + (constrPermCount + 6).ToString()];
                cell.Value = constrPermCount.ToString();

                // информация по объекту + адрес
                string ObjectInfo = "";
                cell = worksheet.Cells["D" + (constrPermCount + 6).ToString()];
                try
                {
                    ObjectInfo = constrPerm.ObjectName;
                }
                catch { }
                try
                {
                    ObjectInfo += Environment.NewLine + constrPerm.ObjectAddress;
                }
                catch { }
                cell.Value = ObjectInfo;

                // код ОКТМО
                cell = worksheet.Cells["E" + (constrPermCount + 6).ToString()];
                cell.Value = "19701000";

                // информация по строительству
                string ConstrInfo = "";
                cell = worksheet.Cells["F" + (constrPermCount + 6).ToString()];
                try
                {
                    ConstrInfo = Enum.GetName(typeof(ePermKind), constrPerm.PermKind);// constrPerm.PermKind.ToString("D");
                    
                }
                catch { }
                try
                {
                    ConstrInfo += Environment.NewLine + Enum.GetName(typeof(eWorkKind), constrPerm.WorkKind); // constrPerm.WorkKind.ToString("D");
                }
                catch { }
                cell.Value = ConstrInfo;

                // информация по документу РС
                // номер
                string constrDocNo = "";
                cell = worksheet.Cells["G" + (constrPermCount + 6).ToString()];
                try
                {
                    constrDocNo = constrPerm.DocNo;
                }
                catch { }
                cell.Value = constrDocNo;

                // дата
                string constrDocDate = "";
                cell = worksheet.Cells["I" + (constrPermCount + 6).ToString()];
                try
                {
                    constrDocDate = constrPerm.DocDate.ToShortDateString();
                }
                catch { }
                cell.Value = constrDocDate;

                // ожидаемая дата ввода
                string ValidDateTotal = "";
                cell = worksheet.Cells["J" + (constrPermCount + 6).ToString()];
                try
                {
                    ValidDateTotal = connect.GetMonth(constrPerm.ValidDateTotal) + " "  + constrPerm.ValidDateTotal.Year.ToString();
                }
                catch { }
                cell.Value = ValidDateTotal;

            }

            // информация по разрешениям на ввод
            // третий лист с информацией по РВ
            worksheet = workbook.Worksheets[2];
            cell = worksheet.Cells["G2"];
            cell.Value = month;

            cell = worksheet.Cells["H2"];
            cell.Value = startReportDate.Year.ToString();

            int constrUsePermCount = 0;
            
            List<SortProperty> sortprops1 = new List<SortProperty>();
            //SortProperty sort = new SortProperty("StageNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
            //constrPerm.ConstrStages.Sorting.Add(sort);
            SortProperty sortprop1 = new SortProperty("DocNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
            sortprops1.Add(sortprop1);
            var ConstrUsePermList = objectSpace.GetObjects<UsePerm>(CriteriaOperator.Parse("DocDate >= ? And DocDate <= ? And EDocStatus=?",//(String.Format("DocDate >= {0} And DocDate <= {1} And EDocStatus={2}",
                startReportDate, FinishReportDate, 4), sortprops1, false);
            //var ConstrUsePermList = objectSpace.GetObjects<UsePerm>(CriteriaOperator.Parse(String.Format("GetYear([DocDate]) = {0} And GetMonth([DocDate]) = {1} And EDocStatus={2}",
            //    ReportDate.Year, ReportDate.Month, 4)), sortprops1, false);
            //new GroupOperator(GroupOperatorType.And, 
            //new BinaryOperator("DocDate.Year", ReportDate.Year), new BinaryOperator("DocDate.Month", ReportDate.Month)), sortprops, false);
            foreach (UsePerm usePerm in ConstrUsePermList.OrderBy(x=> x.DocNo))
            {
                constrUsePermCount++;

                // добавляем строку и форматируем ее
                worksheet.Rows.Insert(constrUsePermCount + 8);
                Range range = worksheet.Range[String.Format("A{0}:AA{0}", (constrUsePermCount + 8).ToString())];
                Formatting rangeFormatting = range.BeginUpdateFormatting();
                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                rangeFormatting.Font.Name = "Times New Roman";
                rangeFormatting.Font.Size = 13;
                //rangeFormatting.Alignment.ShrinkToFit = true;
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //range.AutoFitColumns();
                range.EndUpdateFormatting(rangeFormatting);

                // информация по застройщику
                string DeveloperInfo = "";
                cell = worksheet.Cells["A" + (constrUsePermCount + 8).ToString()];
                try
                {
                    DeveloperInfo = usePerm.ConstrDeveloperString;
                }
                catch { }
                try
                {
                    DeveloperInfo += ", " + usePerm.ConstrDeveloperContactInfo;
                }
                catch { }
                cell.Value = DeveloperInfo;

                // ИНН застройщика
                string DeveloperINN = "";
                cell = worksheet.Cells["B" + (constrUsePermCount + 8).ToString()];
                try
                {
                    DeveloperINN = usePerm.ConstrDeveloperINN;
                }
                catch { }
                cell.Value = DeveloperINN;

                // п\п
                cell = worksheet.Cells["C" + (constrUsePermCount + 8).ToString()];
                cell.Value = constrUsePermCount.ToString();

                // информация по объекту + адрес
                string ObjectInfo = "";
                cell = worksheet.Cells["D" + (constrUsePermCount + 8).ToString()];
                try
                {
                    ObjectInfo = usePerm.ObjectName;
                }
                catch { }
                try
                {
                    ObjectInfo += Environment.NewLine + usePerm.AddressObjectName;
                }
                catch { }
                cell.Value = ObjectInfo;

                // код ОКТМО
                cell = worksheet.Cells["E" + (constrUsePermCount + 8).ToString()];
                cell.Value = "19701000";

                // код ОКТМО
                cell = worksheet.Cells["F" + (constrUsePermCount + 8).ToString()];
                cell.Value = "площадь";

                // код ОКТМО
                cell = worksheet.Cells["G" + (constrUsePermCount + 8).ToString()];
                cell.Value = "кв.м";

                // информация по строительству
                string ConstrInfo = "";
                cell = worksheet.Cells["H" + (constrUsePermCount + 8).ToString()];
                try
                {
                    ConstrInfo = Enum.GetName(typeof(eWorkKind), usePerm.WorkKind);

                }
                catch { }
                //try
                //{
                //    ConstrInfo += Environment.NewLine + Enum.GetName(typeof(eWorkKind), usePerm.WorkKind); // constrPerm.WorkKind.ToString("D");
                //}
                //catch { }
                cell.Value = ConstrInfo;

                // Тип объекта (код)
                string ObjectCode = "";
                cell = worksheet.Cells["I" + (constrUsePermCount + 8).ToString()];
                try
                {
                    ObjectCode = (Enum.GetName(typeof(eBuildingKind), usePerm.CapitalStructureBase[0].BuildingKind)).Split(')')[0].Remove('(');
                }
                catch { }
                cell.Value = ObjectCode;

                // характеристики по вводу
                // мощность
                string Power = "0";
                cell = worksheet.Cells["J" + (constrUsePermCount + 8).ToString()];
                try
                {
                    Power = usePerm.PowerFakt;
                }
                catch { }
                cell.Value = Power;


                // количество зданий
                string BuildsCountFakt = "1";
                cell = worksheet.Cells["K" + (constrUsePermCount + 8).ToString()];
                try
                {
                    if(usePerm.BuildingCountFakt != 0)
                        BuildsCountFakt = usePerm.BuildingCountFakt.ToString();
                }
                catch { }
                cell.Value = BuildsCountFakt;

                // Строительный объем
                string BuildingSizeFakt = "";
                cell = worksheet.Cells["L" + (constrUsePermCount + 8).ToString()];
                try
                {
                    BuildingSizeFakt = usePerm.BuildingSizeFakt.ToString();
                }
                catch { }
                cell.Value = BuildingSizeFakt;

                // Общая площадь
                string TotalBuildSquareFakt = "";
                cell = worksheet.Cells["M" + (constrUsePermCount + 8).ToString()];
                try
                {
                    TotalBuildSquareFakt = usePerm.TotalBuildSquareFakt.ToString();
                }
                catch { }
                cell.Value = TotalBuildSquareFakt;

                // Количество квартир
                string FlatCount = "";
                cell = worksheet.Cells["N" + (constrUsePermCount + 8).ToString()];
                try
                {
                    FlatCount = usePerm.AppartmentsCountFakt.ToString();
                }
                catch { }
                cell.Value = FlatCount;

                // Общая площадь жилых помещений с учетом балконов
                string TotalLivingSpaceFakt = "";
                cell = worksheet.Cells["O" + (constrUsePermCount + 8).ToString()];
                try
                {
                    TotalLivingSpaceFakt = usePerm.TotalLivingSpaceFakt.ToString();
                }
                catch { }
                cell.Value = TotalLivingSpaceFakt;

                // Общая площадь жилых помещений без учета балконов
                string LivingSpaceFakt = "";
                cell = worksheet.Cells["P" + (constrUsePermCount + 8).ToString()];
                try
                {
                    LivingSpaceFakt = usePerm.LivingSpaceFakt.ToString();
                }
                catch { }
                cell.Value = LivingSpaceFakt;

                // Этажность
                string FloorCount = "";
                cell = worksheet.Cells["Q" + (constrUsePermCount + 8).ToString()];
                try
                {
                    FloorCount = usePerm.FloorCountFakt.ToString();
                }
                catch { }
                cell.Value = FloorCount;

                // Материалы стен (код)
                string ConstrMaterial = "";
                cell = worksheet.Cells["R" + (constrUsePermCount + 8).ToString()];
                try
                {
                    ConstrMaterial = usePerm.WallMaterialFakt.Code;
                }
                catch { }
                cell.Value = ConstrMaterial;

                // Стоимость строительства
                string ContsrCost = "";
                cell = worksheet.Cells["S" + (constrUsePermCount + 8).ToString()];
                try
                {
                    ContsrCost = usePerm.constructionCost.ToString();
                }
                catch { }
                cell.Value = ContsrCost;

                // информация по документу РС
                // номер
                string constrDocNo = "";
                cell = worksheet.Cells["T" + (constrUsePermCount + 8).ToString()];
                try
                {
                    constrDocNo = usePerm.DocNo;
                }
                catch { }
                cell.Value = constrDocNo;

                // дата
                string constrDocDate = "";
                cell = worksheet.Cells["U" + (constrUsePermCount + 8).ToString()];
                try
                {
                    constrDocDate = usePerm.DocDate.ToShortDateString();
                }
                catch { }
                cell.Value = constrDocDate;

                // Количество студий
                string StudioCountFakt = "";
                cell = worksheet.Cells["V" + (constrUsePermCount + 8).ToString()];
                try
                {
                    StudioCountFakt = usePerm.StudioCountFakt.ToString();
                }
                catch { }
                cell.Value = StudioCountFakt;

                // Количество 1 комнатных
                string OneRoomFlatCountFakt = "";
                cell = worksheet.Cells["W" + (constrUsePermCount + 8).ToString()];
                try
                {
                    OneRoomFlatCountFakt = usePerm.OneRoomFlatCountFakt.ToString();
                }
                catch { }
                cell.Value = OneRoomFlatCountFakt;

                // Количество 2 комнатных
                string TwoRoomFlatCountFakt = "";
                cell = worksheet.Cells["X" + (constrUsePermCount + 8).ToString()];
                try
                {
                    TwoRoomFlatCountFakt = usePerm.TwoRoomFlatCountFakt.ToString();
                }
                catch { }
                cell.Value = TwoRoomFlatCountFakt;

                // Количество 3 комнатных
                string ThreeRoomFlatCountFakt = "";
                cell = worksheet.Cells["Y" + (constrUsePermCount + 8).ToString()];
                try
                {
                    ThreeRoomFlatCountFakt = usePerm.ThreeRoomFlatCountFakt.ToString();
                }
                catch { }
                cell.Value = ThreeRoomFlatCountFakt;

                // Количество 4 комнатных
                string FourRoomFlatCountFakt = "";
                cell = worksheet.Cells["Z" + (constrUsePermCount + 8).ToString()];
                try
                {
                    FourRoomFlatCountFakt = usePerm.FourRoomFlatCountFakt.ToString();
                }
                catch { }
                cell.Value = FourRoomFlatCountFakt;

                // Количество >4 комнатных
                string MoreRoomFlatCountFakt = "";
                cell = worksheet.Cells["AA" + (constrUsePermCount + 8).ToString()];
                try
                {
                    MoreRoomFlatCountFakt = usePerm.MoreRoomFlatCountFakt.ToString();
                }
                catch { }
                cell.Value = MoreRoomFlatCountFakt;
            }

            string path = Path.GetTempPath() + @"\Отчет_по_стройке_за_" + month + "_" + startReportDate.Year.ToString() +
             "_" + DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xlsx";
            workbook.SaveDocument(path, DevExpress.Spreadsheet.DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        /// <summary>
        /// Не используется
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateConstrPermReportParamAction_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            DateTime ReportDate = DateTime.Now.Date;
            var paramValue = e.ParameterCurrentValue;
            if (paramValue != null)
                ReportDate = Convert.ToDateTime(paramValue);

            //FormPermReport(ReportDate);
        }

        /// <summary>
        /// Не используется!! Сформировать отчет по элитному жилью.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsePermElitReportAction_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {

            DateTime ReportDate = DateTime.Now.Date;
            var paramValue = e.ParameterCurrentValue;
            if (paramValue != null)
                ReportDate = Convert.ToDateTime(paramValue);

            // получаем объект и его сессию

            View.ObjectSpace.CommitChanges();
            IObjectSpace objectSpace = Application.CreateObjectSpace();

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePermElit.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            string month = "";
            switch (ReportDate.Month)
            {
                case 1:
                    month = "январь";
                    break;
                case 2:
                    month = "февраль";
                    break;
                case 3:
                    month = "март";
                    break;
                case 4:
                    month = "апрель";
                    break;
                case 5:
                    month = "май";
                    break;
                case 6:
                    month = "июнь";
                    break;
                case 7:
                    month = "июль";
                    break;
                case 8:
                    month = "август";
                    break;
                case 9:
                    month = "сентябрь";
                    break;
                case 10:
                    month = "октябрь";
                    break;
                case 11:
                    month = "ноябрь";
                    break;
                case 12:
                    month = "декабрь";
                    break;
                default:
                    month = DateTime.Now.Month.ToString();
                    break;
            }
            string year =  ReportDate.Year.ToString();

            Bookmark reportDateBook = richServer.Document.Bookmarks["reportDateBook"];
            richServer.Document.Replace(reportDateBook.Range, month + " " + year);

            Bookmark reportDateBook1 = richServer.Document.Bookmarks["reportDateBook1"];
            richServer.Document.Replace(reportDateBook1.Range, month + " " + year);


            int ElitAppartmentsCount = 0;
            double ElitAppartmentsSpace = 0;

            int ElitOwnHausesCount = 0;
            double ElitOwnHausesSpace = 0;

            List<SortProperty> sortprops = new List<SortProperty>();
            SortProperty sortprop = new SortProperty("DocDate", DevExpress.Xpo.DB.SortingDirection.Ascending);
            sortprops.Add(sortprop);
            var ConstrUsePermList = objectSpace.GetObjects<UsePerm>(CriteriaOperator.Parse(String.Format("GetYear([DocDate]) = {0} And GetMonth([DocDate]) = {1} And EDocStatus={2}",
                ReportDate.Year, ReportDate.Month, 4)), sortprops, false);
            foreach (UsePerm usePerm in ConstrUsePermList.OrderBy(x => x.DocNo))
            {
                ElitAppartmentsCount += usePerm.ElitAppartmentsCountFakt;
                ElitAppartmentsSpace+= usePerm.ElitAppartmentsSpaceFakt;

                try { if (usePerm.CapitalStructureBase[0].ConstructionType.Name == "индивидуальные жилые дома" ||
                        usePerm.CapitalStructureBase[0].ConstructionType.Name == "индивидуальный жилой дом")
                    {
                        if(usePerm.TotalBuildSquareFakt > 200)
                        {
                            ElitOwnHausesCount++;
                            ElitOwnHausesSpace += usePerm.TotalBuildSquareFakt;
                        }
                    }
                }
                catch { }
            }
            Bookmark ElitAppartmentsCountBook = richServer.Document.Bookmarks["ElitAppartmentsCountBook"];
            richServer.Document.Replace(ElitAppartmentsCountBook.Range, ElitAppartmentsCount.ToString());

            Bookmark ElitAppartmentsSpaceBook = richServer.Document.Bookmarks["ElitAppartmentsSpaceBook"];
            richServer.Document.Replace(ElitAppartmentsSpaceBook.Range, ElitAppartmentsSpace.ToString());

            Bookmark ElitOwnHausesCountBook = richServer.Document.Bookmarks["ElitOwnHausesCountBook"];
            richServer.Document.Replace(ElitOwnHausesCountBook.Range, ElitOwnHausesCount.ToString());

            Bookmark ElitOwnHausesSpaceBook = richServer.Document.Bookmarks["ElitOwnHausesSpaceBook"];
            richServer.Document.Replace(ElitOwnHausesSpaceBook.Range, ElitOwnHausesSpace.ToString());



            string path = Path.GetTempPath() + @"\Отчет_по_элит_жилью_за_" + month + "_" + ReportDate.Year.ToString() +
             "_" + DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";
            richServer.SaveDocument(path, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        
        /// <summary>
        /// Сформировать отчет для налоговой
        /// </summary>
        /// <param name="startReportDate"></param>
        /// <param name="FinishReportDate"></param>
        private void CreateFNSPermReport(DateTime startReportDate, DateTime FinishReportDate)
        {
            //XtraMessageBox.Show(String.Format($"{startReportDate.ToShortDateString()} - {FinishReportDate.ToShortDateString()}"));

            IObjectSpace objectSpace = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(objectSpace);

            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePermFNS.xlsx");
            workbook.LoadDocument(templatePath, DevExpress.Spreadsheet.DocumentFormat.OpenXml);

            // первый лист 
            //XtraMessageBox.Show(String.Format($"{workbook.Worksheets.Count.ToString()} - {workbook.Worksheets[0].Name}"));
            Worksheet worksheet = workbook.Worksheets[0];
            Cell cell;

            int constrUsePermCount = 0;
            int startIndex = 2;
            List<SortProperty> sortprops1 = new List<SortProperty>();
            SortProperty sortprop1 = new SortProperty("DocNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
            sortprops1.Add(sortprop1);
            var ConstrUsePermList = objectSpace.GetObjects<UsePerm>(CriteriaOperator.Parse("DocDate >= ? And DocDate <= ? And EDocStatus=?",//(String.Format("DocDate >= {0} And DocDate <= {1} And EDocStatus={2}",
                startReportDate, FinishReportDate, 4), sortprops1, false);
            //XtraMessageBox.Show(ConstrUsePermList.Count.ToString());
            
            foreach (UsePerm usePerm in ConstrUsePermList.OrderBy(x => x.DocNo))
            {
                constrUsePermCount++;

                // добавляем строку и форматируем ее
                //worksheet.Rows.Insert(constrUsePermCount + 2);
                Range range = worksheet.Range[String.Format("A{0}:O{0}", (constrUsePermCount + startIndex).ToString())];
                Formatting rangeFormatting = range.BeginUpdateFormatting();
                //rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.);
                rangeFormatting.Font.Name = "Times New Roman";
                rangeFormatting.Font.Size = 13;
                //rangeFormatting.Alignment.ShrinkToFit = true;
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //range.AutoFitColumns();
                range.EndUpdateFormatting(rangeFormatting);

                // п\п
                cell = worksheet.Cells["A" + (constrUsePermCount + startIndex).ToString()];
                cell.Value = constrUsePermCount.ToString();

                // информация по документу РВ
                // номер
                string constrDocNo = "";
                cell = worksheet.Cells["B" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    constrDocNo = usePerm.DocNo;
                }
                catch { }
                cell.Value = constrDocNo;

                // дата
                string constrDocDate = "";
                cell = worksheet.Cells["C" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    constrDocDate = usePerm.DocDate.ToShortDateString();
                }
                catch { }
                cell.Value = constrDocDate;


                // застройщик
                string DeveloperName = "";
                cell = worksheet.Cells["D" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    DeveloperName = usePerm.ConstrDeveloperString;
                }
                catch { }
                cell.Value = DeveloperName;

                // ИНН застройщика
                string DeveloperINN = "";
                cell = worksheet.Cells["E" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    foreach (GeneralSubject developer in usePerm.DocSubjects)
                    {
                        try
                        {
                            if (DeveloperINN == "")
                                DeveloperINN = developer.INN;
                            else
                                DeveloperINN += "; " + developer.INN;
                        }
                        catch { }
                    }
                }
                catch { }
                cell.Value = DeveloperINN;

                // реквизиты застройщика
                string DeveloperInfo = "";
                cell = worksheet.Cells["F" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    DeveloperInfo = usePerm.ConstrDeveloperContactInfo;
                }
                catch { }
                cell.Value = DeveloperInfo;


                // информация по документу РС
                // номер + дата
                string constrPermInfo = "";
                cell = worksheet.Cells["G" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    constrPermInfo = usePerm.ConstrPermInfo;
                }
                catch { }
                cell.Value = constrPermInfo;

                // наименование объекта
                string ObjectName= "";
                cell = worksheet.Cells["H" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    ObjectName = usePerm.ObjectName;
                }
                catch { }
                cell.Value = ObjectName;

                // адрес объекта
                string ObjectAddress = "";
                cell = worksheet.Cells["I" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    ObjectAddress = usePerm.AddressObjectName;
                }
                catch { }
                cell.Value = ObjectAddress;

                // кадастровый номер участка
                string LotCadNo = "";
                cell = worksheet.Cells["J" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    LotCadNo = usePerm.LotCadNo;
                }
                catch { }
                cell.Value = LotCadNo;



                // Вид объекта
                string ObjectCode = "";
                cell = worksheet.Cells["K" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    ObjectCode = Enum.GetName(typeof(eBuildingKind), usePerm.BuildingKind);
                    //ObjectCode = (Enum.GetName(typeof(eBuildingKind), usePerm.CapitalStructureBase[0].BuildingKind)).Split(')')[0].Remove('(');
                }
                catch { }
                cell.Value = ObjectCode;

                // Тип объекта (справочник)
                string ConstructionType = "";
                cell = worksheet.Cells["L" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    ConstructionType = usePerm.ConstructionType.Name;
                }
                catch { }
                cell.Value = ConstructionType;

                // Этажность
                string FloorCount = "";
                cell = worksheet.Cells["M" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    FloorCount = usePerm.FloorCountFakt.ToString();
                }
                catch { }
                cell.Value = FloorCount;

                // Количество квартир
                string FlatCount = "";
                cell = worksheet.Cells["O" + (constrUsePermCount + startIndex).ToString()];
                try
                {
                    FlatCount = usePerm.AppartmentsCountFakt.ToString();
                }
                catch { }
                cell.Value = FlatCount;
                
            }

            string path = Path.GetTempPath() + @"\Отчет_РВ_в_ФНС_" + 
             "_" + DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xlsx";
            workbook.SaveDocument(path, DevExpress.Spreadsheet.DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        /// <summary>
        /// Сформировать отчет по элитному жилью
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateUsePermElitReport(DateTime startReportDate, DateTime FinishReportDate)
        {
            // получаем объект и его сессию

            View.ObjectSpace.CommitChanges();
            IObjectSpace objectSpace = Application.CreateObjectSpace();

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePermElit.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            string month = "";
            switch (startReportDate.Month)
            {
                case 1:
                    month = "январь";
                    break;
                case 2:
                    month = "февраль";
                    break;
                case 3:
                    month = "март";
                    break;
                case 4:
                    month = "апрель";
                    break;
                case 5:
                    month = "май";
                    break;
                case 6:
                    month = "июнь";
                    break;
                case 7:
                    month = "июль";
                    break;
                case 8:
                    month = "август";
                    break;
                case 9:
                    month = "сентябрь";
                    break;
                case 10:
                    month = "октябрь";
                    break;
                case 11:
                    month = "ноябрь";
                    break;
                case 12:
                    month = "декабрь";
                    break;
                default:
                    month = DateTime.Now.Month.ToString();
                    break;
            }
            string year = startReportDate.Year.ToString();

            Bookmark reportDateBook = richServer.Document.Bookmarks["reportDateBook"];
            richServer.Document.Replace(reportDateBook.Range, month + " " + year);

            Bookmark reportDateBook1 = richServer.Document.Bookmarks["reportDateBook1"];
            richServer.Document.Replace(reportDateBook1.Range, month + " " + year);


            int ElitAppartmentsCount = 0;
            double ElitAppartmentsSpace = 0;

            int ElitOwnHausesCount = 0;
            double ElitOwnHausesSpace = 0;

            List<SortProperty> sortprops = new List<SortProperty>();
            SortProperty sortprop = new SortProperty("DocNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
            sortprops.Add(sortprop);
            var ConstrUsePermList = objectSpace.GetObjects<UsePerm>(CriteriaOperator.Parse("DocDate >= ? And DocDate <= ? And EDocStatus=?",//(String.Format("DocDate >= {0} And DocDate <= {1} And EDocStatus={2}",
                startReportDate, FinishReportDate, 4), sortprops, false);
            //var ConstrUsePermList = objectSpace.GetObjects<UsePerm>(CriteriaOperator.Parse(String.Format("GetYear([DocDate]) = {0} And GetMonth([DocDate]) = {1} And EDocStatus={2}",
            //    ReportDate.Year, ReportDate.Month, 4)), sortprops, false);
            foreach (UsePerm usePerm in ConstrUsePermList.OrderBy(x => x.DocDate))
            {
                ElitAppartmentsCount += usePerm.ElitAppartmentsCountFakt;
                ElitAppartmentsSpace += usePerm.ElitAppartmentsSpaceFakt;

                try
                {
                    if (usePerm.CapitalStructureBase[0].ConstructionType.Name == "индивидуальные жилые дома" ||
                      usePerm.CapitalStructureBase[0].ConstructionType.Name == "индивидуальный жилой дом")
                    {
                        if (usePerm.TotalBuildSquareFakt > 200)
                        {
                            ElitOwnHausesCount++;
                            ElitOwnHausesSpace += usePerm.TotalBuildSquareFakt;
                        }
                    }
                }
                catch { }
            }
            Bookmark ElitAppartmentsCountBook = richServer.Document.Bookmarks["ElitAppartmentsCountBook"];
            richServer.Document.Replace(ElitAppartmentsCountBook.Range, ElitAppartmentsCount.ToString());

            Bookmark ElitAppartmentsSpaceBook = richServer.Document.Bookmarks["ElitAppartmentsSpaceBook"];
            richServer.Document.Replace(ElitAppartmentsSpaceBook.Range, ElitAppartmentsSpace.ToString());

            Bookmark ElitOwnHausesCountBook = richServer.Document.Bookmarks["ElitOwnHausesCountBook"];
            richServer.Document.Replace(ElitOwnHausesCountBook.Range, ElitOwnHausesCount.ToString());

            Bookmark ElitOwnHausesSpaceBook = richServer.Document.Bookmarks["ElitOwnHausesSpaceBook"];
            richServer.Document.Replace(ElitOwnHausesSpaceBook.Range, ElitOwnHausesSpace.ToString());



            string path = Path.GetTempPath() + @"\Отчет_по_элит_жилью_за_" + month + "_" + year +
             "_" + DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";
            richServer.SaveDocument(path, DevExpress.XtraRichEdit.DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        
    }

    /// <summary>
    /// Тип отчета
    /// </summary>
    public enum eReportType
    {
        статистика = 0,
        элитжилье = 1,
        налоговая = 2
       
    }

}
