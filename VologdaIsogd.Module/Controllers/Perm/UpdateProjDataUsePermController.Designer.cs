﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class UpdateProjDataUsePermController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UpdateProjDataUsePermAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // UpdateProjDataUsePermAction
            // 
            this.UpdateProjDataUsePermAction.Caption = "Обновить проект. показатели";
            this.UpdateProjDataUsePermAction.ConfirmationMessage = null;
            this.UpdateProjDataUsePermAction.Id = "UpdateProjDataUsePermAction";
            this.UpdateProjDataUsePermAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.UpdateProjDataUsePermAction.TargetObjectType = typeof(AISOGD.Perm.UsePerm);
            this.UpdateProjDataUsePermAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.UpdateProjDataUsePermAction.ToolTip = "При нажатии на эту кнопку в карточке Разрешения на ввод пересчитаются проектные п" +
    "оказатели (из этапов) ";
            this.UpdateProjDataUsePermAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.UpdateProjDataUsePermAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UpdateProjDataUsePermAction_Execute);
            // 
            // UpdateProjDataUsePermController
            // 
            this.Actions.Add(this.UpdateProjDataUsePermAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction UpdateProjDataUsePermAction;
    }
}
