﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class ChangeStatusesNoticeIZDPermController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ChangeStatusesNoticeIZDPermChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ChangeStatusesNoticeIZDPermChoiceAction
            // 
            this.ChangeStatusesNoticeIZDPermChoiceAction.Caption = "Изменить статус";
            this.ChangeStatusesNoticeIZDPermChoiceAction.ConfirmationMessage = null;
            this.ChangeStatusesNoticeIZDPermChoiceAction.Id = "ChangeStatusesNoticeIZDPermChoiceAction";
            this.ChangeStatusesNoticeIZDPermChoiceAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ChangeStatusesNoticeIZDPermChoiceAction.ToolTip = "Утверждение документа, его продление или отказ";
            this.ChangeStatusesNoticeIZDPermChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ChangeStatusesNoticeIZDPermChoiceAction_Execute);
            // 
            // ChangeStatusesNoticeIZDPermController
            // 
            this.Actions.Add(this.ChangeStatusesNoticeIZDPermChoiceAction);
            this.TargetObjectType = typeof(AISOGD.Perm.PermNoticeIZD);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ChangeStatusesNoticeIZDPermChoiceAction;
    }
}
