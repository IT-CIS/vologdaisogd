﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class InfoListTemplateController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InfoListTemplateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InfoListTemplateAction
            // 
            this.InfoListTemplateAction.Caption = "Инфо. лист";
            this.InfoListTemplateAction.ConfirmationMessage = null;
            this.InfoListTemplateAction.Id = "InfoListTemplateAction";
            this.InfoListTemplateAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.InfoListTemplateAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.InfoListTemplateAction.ToolTip = "Формировние информационного листа для согласования документа";
            this.InfoListTemplateAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.InfoListTemplateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InfoListTemplateAction_Execute);
            // 
            // InfoListTemplateController
            // 
            this.Actions.Add(this.InfoListTemplateAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InfoListTemplateAction;
    }
}
