﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Perm;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using AISOGD.Constr;
using DevExpress.ExpressApp.Win;
using AISOGD.OrgStructure;
using AISOGD.Subject;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class UpdateProjDataUsePermController : ViewController
    {
        public UpdateProjDataUsePermController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Обновление проектных характеристик у Разрешения на ввод из его этапов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateProjDataUsePermAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace(); // Create IObjectSpace or use the existing one, e.g. View.ObjectSpace, if it is suitable for your scenario.
            var x_id = ((BaseObject)e.CurrentObject).Oid;


            UsePerm usePerm;
            usePerm = os.FindObject<UsePerm>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)usePerm.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            try
            {
                usePerm.ConstructionType = usePerm.CapitalStructureBase[0].ConstructionType;
            }
            catch { }
            try
            {
                usePerm.ConstructionCategory = usePerm.CapitalStructureBase[0].ConstructionCategory;
            }
            catch { }

            #region Сначала обнуляем все показатели 
            usePerm.BuildingSizeProject = 0;
            usePerm.BuildingSizeOverGroundPartProject = 0;
            usePerm.BuildingSizeUnderGroundPartProject = 0;
            usePerm.TotalBuildSquareProject = 0;
            usePerm.BuildSquareProject = 0;
            usePerm.HeightProject = 0;
            usePerm.NotLivingSquareProject = 0;
            usePerm.OutBuildingSquareProject = 0;
            usePerm.BuildingCountProject = 0;
            usePerm.ElevatorsCountProject = 0;
            usePerm.EscalatorsCountProject = 0;
            usePerm.InvalidLiftsCountProject = 0;
            usePerm.OtherIndicatorsProject = "";

            usePerm.PlacesCountProject = 0;
            usePerm.RoomCountProject = 0;
            usePerm.CapacityProject = 0;

            usePerm.LivingSpaceProject = 0;
            usePerm.TotalLivingSpaceProject = 0;
            usePerm.NotLivingSpaceProject = 0;
            usePerm.SectionCountProject = 0;
            usePerm.PorchCountProject = 0;
            usePerm.AppartmentsCountProject = 0;
            usePerm.StudioCountProject = 0;
            usePerm.StudioSpaceProject = 0;
            usePerm.OneRoomFlatCountProject = 0;
            usePerm.OneRoomFlatSpaceProject = 0;
            usePerm.TwoRoomFlatCountProject = 0;
            usePerm.TwoRoomFlatSpaceProject = 0;
            usePerm.ThreeRoomFlatCountProject = 0;
            usePerm.ThreeRoomFlatSpaceProject = 0;
            usePerm.FourRoomFlatCountProject = 0;
            usePerm.FourRoomSpaceProject = 0;
            usePerm.MoreRoomFlatCountProject = 0;
            usePerm.MoreRoomSpaceProject = 0;
            usePerm.ElitAppartmentsCountProject = 0;
            usePerm.ElitAppartmentsSpaceProject = 0;

            usePerm.PowerProject = "";
            usePerm.ProductivityProject = "";

            usePerm.LengthProject = 0;
            usePerm.PowerLineProject = "";
            usePerm.PipesInfoProject = "";
            usePerm.ElectricLinesInfoProject = "";
            usePerm.ConstructiveElementsInfoProject = "";

            usePerm.HeatConsumptionProject = 0;
            usePerm.OutdoorIsolationMaterialProject = "";
            usePerm.SkylightsFillingProject = "";
            #endregion

            foreach (ConstrStage constrStage in usePerm.ConstrStages)
            {

                #region Общие показатели
                try { usePerm.BuildingSizeProject += constrStage.BuildingSizeProject; }
                catch { }
                try { usePerm.BuildingSizeOverGroundPartProject += constrStage.BuildingSizeOverGroundPartProject; }
                catch { }
                try { usePerm.BuildingSizeUnderGroundPartProject += constrStage.BuildingSizeUnderGroundPartProject; }
                catch { }
                try { usePerm.TotalBuildSquareProject += constrStage.TotalBuildSquareProject; }
                catch { }
                try { usePerm.BuildSquareProject += constrStage.BuildSquare; }
                catch { }
                try { usePerm.HeightProject += constrStage.Height; }
                catch { }
                try { usePerm.NotLivingSquareProject += constrStage.NotLivingSquareProject; }
                catch { }
                try { usePerm.OutBuildingSquareProject += constrStage.OutBuildingSquareProject; }
                catch { }
                try { usePerm.BuildingCountProject += constrStage.BuildingCountProject; }
                catch { }
                #endregion

                #region Лифты, Эскалаторы, подъемники для ОКС
                try { usePerm.ElevatorsCountProject += constrStage.ElevatorsCountProject; }
                catch { }
                try { usePerm.EscalatorsCountProject += constrStage.EscalatorsCountProject; }
                catch { }
                try { usePerm.InvalidLiftsCountProject += constrStage.InvalidLiftsCountProject; }
                catch { }
                #endregion

                #region Материалы
                try
                {
                    if (usePerm.FundMaterialProject == null)
                        usePerm.FundMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.FundMaterialProject.Oid);
                }
                catch { }
                try
                {
                    if (usePerm.WallMaterialProject == null)
                        usePerm.WallMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.WallMaterialProject.Oid);
                }
                catch { }
                try
                {
                    if (usePerm.BorderMaterialProject == null)
                        usePerm.BorderMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.BorderMaterialProject.Oid);
                }
                catch { }
                try
                {
                    if (usePerm.RoofMaterialProject == null)
                        usePerm.RoofMaterialProject = connect.FindFirstObject<Material>(x => x.Oid == constrStage.RoofMaterialProject.Oid);
                }
                catch { }


                #endregion

                #region Сети и системы инженерно технического обслуживания
                try
                {
                    if (constrStage.ElectroProject == true)
                        usePerm.ElectroProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.HeatProject == true)
                        usePerm.HeatProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.WaterProject == true)
                        usePerm.WaterProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.GazProject == true)
                        usePerm.GazProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.HouseSeverageProject == true)
                        usePerm.HouseSeverageProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.PhonesProject == true)
                        usePerm.PhonesProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.TVProject == true)
                        usePerm.TVProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.RadioProject == true)
                        usePerm.RadioProject = true;
                }
                catch { }
                try
                {
                    if (constrStage.SeverageProject == true)
                        usePerm.SeverageProject = true;
                }
                catch { }
                #endregion

                #region Иные показатели
                try
                {
                    usePerm.OtherIndicatorsProject += constrStage.OtherIndicatorsProject + Environment.NewLine;
                    usePerm.OtherIndicatorsProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region нежилые
                try { usePerm.PlacesCountProject += constrStage.PlacesCountProject; }
                catch { }
                try { usePerm.RoomCountProject += constrStage.RoomCountProject; }
                catch { }
                try { usePerm.CapacityProject += constrStage.CapacityProject; }
                catch { }
                #endregion

                #region жилые
                try { usePerm.LivingSpaceProject += constrStage.LivingSpaceProject; }
                catch { }
                try { usePerm.TotalLivingSpaceProject += constrStage.TotalLivingSpaceProject; }
                catch { }
                try { usePerm.NotLivingSpaceProject += constrStage.NotLivingSpaceProject; }
                catch { }
                try { usePerm.SectionCountProject += constrStage.SectionCountProject; }
                catch { }
                try { usePerm.PorchCountProject += constrStage.PorchCountProject; }
                catch { }
                try { usePerm.AppartmentsCountProject += constrStage.AppartmentsCountProject; }
                catch { }

                try { usePerm.StudioCountProject += constrStage.StudioCountProject; }
                catch { }
                try { usePerm.StudioSpaceProject += constrStage.StudioSpaceProject; }
                catch { }
                try { usePerm.OneRoomFlatCountProject += constrStage.OneRoomFlatCountProject; }
                catch { }
                try { usePerm.OneRoomFlatSpaceProject += constrStage.OneRoomFlatSpaceProject; }
                catch { }
                try { usePerm.TwoRoomFlatCountProject += constrStage.TwoRoomFlatCountProject; }
                catch { }
                try { usePerm.TwoRoomFlatSpaceProject += constrStage.TwoRoomFlatSpaceProject; }
                catch { }
                try { usePerm.ThreeRoomFlatCountProject += constrStage.ThreeRoomFlatCountProject; }
                catch { }
                try { usePerm.ThreeRoomFlatSpaceProject += constrStage.ThreeRoomFlatSpaceProject; }
                catch { }
                try { usePerm.FourRoomFlatCountProject += constrStage.FourRoomFlatCountProject; }
                catch { }
                try { usePerm.FourRoomSpaceProject += constrStage.FourRoomSpaceProject; }
                catch { }
                try { usePerm.MoreRoomFlatCountProject += constrStage.MoreRoomFlatCountProject; }
                catch { }
                try { usePerm.MoreRoomSpaceProject += constrStage.MoreRoomSpaceProject; }
                catch { }
                try { usePerm.ElitAppartmentsCountProject += constrStage.ElitAppartmentsCountProject; }
                catch { }
                try { usePerm.ElitAppartmentsSpaceProject += constrStage.ElitAppartmentsSpaceProject; }
                catch { }
                #endregion

                #region Объекты производственного назначения
                try
                {
                    if (usePerm.ProductPurposeObjectType == null)
                        usePerm.ProductPurposeObjectType = connect.FindFirstObject<ProductPurposeObjectType>(x => x.Oid == constrStage.ProductPurposeObjectType.Oid);
                }
                catch { }
                try
                {
                    //usePerm.PowerProject = "";
                    usePerm.PowerProject += constrStage.PowerProject + Environment.NewLine;
                    usePerm.PowerProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {
                    usePerm.ProductivityProject += constrStage.ProductivityProject + Environment.NewLine;
                    usePerm.ProductivityProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region  Линейные объекты
                try
                {
                    if (usePerm.LineObjectClass == null)
                        usePerm.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid == constrStage.LineObjectClass.Oid);
                }
                catch { }
                try { usePerm.LengthProject += constrStage.LengthProject; }
                catch { }
                try
                {

                    usePerm.PowerLineProject += constrStage.PowerLineProject + Environment.NewLine;
                    usePerm.PowerLineProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    usePerm.PipesInfoProject += constrStage.PipesInfoProject + Environment.NewLine;
                    usePerm.PipesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    usePerm.ElectricLinesInfoProject += constrStage.ElectricLinesInfoProject + Environment.NewLine;
                    usePerm.ElectricLinesInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    usePerm.ConstructiveElementsInfoProject += constrStage.ConstructiveElementsInfoProject + Environment.NewLine;
                    usePerm.ConstructiveElementsInfoProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion

                #region Соответствие требованиям энергетической эффективности
                try
                {
                    if (usePerm.EnergyEfficiencyClassProject == null)
                        usePerm.EnergyEfficiencyClassProject = connect.FindFirstObject<EnergyEfficiencyClass>(x => x.Oid == constrStage.EnergyEfficiencyClassProject.Oid);
                }
                catch { }
                try
                {
                    if (usePerm.HeatUnit == null)
                        usePerm.HeatUnit = connect.FindFirstObject<dHeatUnit>(x => x.Oid == constrStage.HeatUnit.Oid);
                }
                catch { }

                try { usePerm.HeatConsumptionProject += constrStage.HeatConsumptionProject; }
                catch { }
                try
                {

                    usePerm.OutdoorIsolationMaterialProject += constrStage.OutdoorIsolationMaterialProject + Environment.NewLine;
                    usePerm.OutdoorIsolationMaterialProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                try
                {

                    usePerm.SkylightsFillingProject += constrStage.SkylightsFillingProject + Environment.NewLine;
                    usePerm.SkylightsFillingProject.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                #endregion
            }

            #region // заполняем данные по типу разрешения и заявке для согласования
            if (usePerm.LetterInfo == null || usePerm.LetterInfo == "")
            {
                string devs = "";
                foreach (GeneralSubject subj in usePerm.DocSubjects)
                {
                    if (subj.NameFrom != null && subj.NameFrom != "")
                        devs += subj.NameFrom + ", ";
                    else
                        devs += subj.Name + ", ";
                }
                devs = devs.TrimEnd().TrimEnd(',');
                usePerm.LetterInfo = String.Format("Заявление {0} от {1} г. № {2}", devs, usePerm.LetterDate.ToShortDateString(), usePerm.LetterNo);
            }

            if (usePerm.PermInfo == null || usePerm.PermInfo == "")
            {
                string permInfo = "Выдача разрешения на ввод в эксплуатацию";
                string permInfoR = "Отказ в выдаче разрешения на ввод в эксплуатацию";

                string t = "";
                if (usePerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ОбъектКапитальногоСтроительства)
                    t += " объекта капитального строительства";
                if (usePerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ЛинейныйОбъект)
                    t += " линейного объекта";
                if (usePerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ОбъектКультурногоНаследия)
                    t += " объекта культурного наследия";
                if (usePerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ОксВСоставеЛинейного)
                    t += " объекта капитального строительства в составе линейного";
                permInfo += t;
                permInfoR += t;



                usePerm.PermInfo = permInfo;
                usePerm.PermInfoR = permInfoR;
            }
            if (usePerm.Decision1 == null)
            {
                try { usePerm.Decision1 = connect.FindFirstObject<Employee>(x => x.LastName == "Воропанов"); }
                catch { }
            }
            if (usePerm.Decision2 == null)
            {
                try { usePerm.Decision2 = connect.FindFirstObject<Employee>(x => x.LastName == "Борисовский"); }
                catch { }
            }
            if (usePerm.AgreementEmploye1 == null)
            {
                try { usePerm.AgreementEmploye1 = connect.FindFirstObject<Employee>(x => x.LastName == "Шмонова"); }
                catch { }
            }
            if (usePerm.AgreementEmploye2 == null)
            {
                try { usePerm.AgreementEmploye2 = connect.FindFirstObject<Employee>(x => x.LastName == "Угарина"); }
                catch { }
            }
            if (usePerm.AgreementEmploye3 == null)
            {
                try { usePerm.AgreementEmploye3 = connect.FindFirstObject<Employee>(x => x.LastName == "Аникин"); }
                catch { }
            }
            #endregion
            usePerm.Save();
            os.CommitChanges();

            // обновляем окно
            FindWindowByView(View).GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
        }

        /// <summary>
        /// Найти окно по View
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        protected WinWindow FindWindowByView(DevExpress.ExpressApp.View view)
        {
            ViewShortcut viewShortcut = view.CreateShortcut();
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.View.CreateShortcut() == viewShortcut)
                {
                    return existingWindow;
                }
            }
            return null;
        }

    }
}
