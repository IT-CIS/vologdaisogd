﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using AISOGD.Perm;
using AISOGD.Enums;
using DevExpress.XtraEditors;
using AISOGD.Constr;
using AISOGD.Subject;
using AISOGD.Land;
using DevExpress.ExpressApp.Win;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CopyDataFromAnotherPermController : ViewController
    {
        public CopyDataFromAnotherPermController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        object constrPermNewId = "";
        /// <summary>
        /// Скопировать данные из выбранного разрешения на строительство
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyDataFromAnotherPermAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            constrPermNewId = ((BaseObjectXAF)View.CurrentObject).Oid;

            Connect connect = Connect.FromObjectSpace(os);

            CollectionSourceBase source = Application.CreateCollectionSource(os, typeof(ConstrPerm), "123List");
            //Application.FindListViewId(typeof(ConstrPerm)));// "123list");
            string ID = Application.FindListViewId(typeof(ConstrPerm));
            source.Criteria.Add("DocStatus", CriteriaOperator.Parse("EDocStatus!=?", eDocStatus.Утвержден));
            e.ShowViewParameters.CreatedView = Application.CreateListView(ID, source, false);
            DialogController contr = new DialogController();

            contr.AcceptAction.Caption = "Выбрать объект";
            contr.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(GetConstrPermFromListView);
            contr.CancelAction.Caption = "Отмена";
            e.ShowViewParameters.Controllers.Add(contr);
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
        }

        /// <summary>
        /// Логика при выборе из списка разрешения на строительство взависимости от темы обращения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetConstrPermFromListView(object sender, DialogControllerAcceptingEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            if (e.AcceptActionArgs.SelectedObjects.Count == 0)
            {
                XtraMessageBox.Show("Не выбрано ни одно разрешение.");
                return;
            }
            if (e.AcceptActionArgs.SelectedObjects.Count > 1)
            {
                XtraMessageBox.Show("Выбрано больше одного разрешения.");
                return;
            }
            Connect connect = Connect.FromObjectSpace(os);
            string constrPermOldId = ((BaseObjectXAF)e.AcceptActionArgs.SelectedObjects[0]).Oid.ToString();
            ConstrPerm constrPermNew = os.GetObjectByKey<ConstrPerm>(constrPermNewId);
            ConstrPerm constrPermOld = os.FindObject<ConstrPerm>(new BinaryOperator("Oid", constrPermOldId));
            CopyDataFromAnotherPerm(constrPermNew, constrPermOld, os);
        }

        private void CopyDataFromAnotherPerm(ConstrPerm constrPermNew, ConstrPerm constrPermOld, IObjectSpace os)
        {
            try { constrPermNew.BuildingCategory = constrPermOld.BuildingCategory; }
            catch { }
            try { constrPermNew.MapNo = constrPermOld.MapNo; }
            catch { }
            try { constrPermNew.ConstructionType = constrPermOld.ConstructionType; }
            catch { }
            try
            {
                constrPermNew.BuildingSizeProject = constrPermOld.BuildingSizeProject;
            }
            catch { }
            try
            {
                constrPermNew.BuildingSizeUnderGroundPartProject = constrPermOld.BuildingSizeUnderGroundPartProject;
            }
            catch { }
            try
            {
                constrPermNew.BuildSquare = constrPermOld.BuildSquare;
            }
            catch { }
            try
            {
                constrPermNew.CadKvartalNo = constrPermOld.CadKvartalNo;
            }
            catch { }
            try
            {
                constrPermNew.Capacity = constrPermOld.Capacity;
            }
            catch { }
            try
            {
                foreach (CapitalStructureBase constrObj in constrPermOld.CapitalStructureBase)
                {
                    constrPermNew.CapitalStructureBase.Add(constrObj);
                }
            }
            catch { }
            try
            {
                constrPermNew.ConstrDeveloperString = constrPermOld.ConstrDeveloperString;
            }
            catch { }
            try
            {
                foreach (ConstrStage constrObj in constrPermOld.ConstrStages)
                {
                    constrPermNew.ConstrStages.Add(constrObj);
                    constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;
                    constrObj.Save();
                }
            }
            catch { }
            try
            {
                constrPermNew.ConstructiveElementsInfoProject = constrPermOld.ConstructiveElementsInfoProject;
            }
            catch { }

            //constrPermNew.DocKind = constrPermOld.DocKind;

            foreach (GeneralSubject constrSubj in constrPermOld.DocSubjects)
            {
                constrPermNew.DocSubjects.Add(constrSubj);
            }

            try
            {
                constrPermNew.ElectricLinesInfoProject = constrPermOld.ElectricLinesInfoProject;
            }
            catch { }
            try
            {
                constrPermNew.ExpertInfo = constrPermOld.ExpertInfo;
            }
            catch { }
            try
            {
                constrPermNew.FloorCount = constrPermOld.FloorCount;
            }
            catch { }
            try
            {
                constrPermNew.GPZU = constrPermOld.GPZU;
            }
            catch { }
            try
            {
                constrPermNew.Height = constrPermOld.Height;
            }
            catch { }
            try
            {
                constrPermNew.LengthProject = constrPermOld.LengthProject;
            }
            catch { }
            try
            {
                constrPermNew.LineObjectClass = constrPermOld.LineObjectClass;
            }
            catch { }
            try
            {
                constrPermNew.LotCadNo = constrPermOld.LotCadNo;
            }
            catch { }
            try
            {
                constrPermNew.LotSquare = constrPermOld.LotSquare;
            }
            catch { }

            try
            {
                constrPermNew.ObjChangeCadNo = constrPermOld.ObjChangeCadNo;
            }
            catch { }
            try
            {
                constrPermNew.ObjectAddress = constrPermOld.ObjectAddress;
            }
            catch { }
            //try
            //{
            //    foreach (ObjectConstractionInfo objInfo in constrPermOld.ObjectConstractionInfo)
            //    {
            //        constrPermNew.ObjectConstractionInfo.Add(objInfo);
            //    }
            //}
            //catch { }
            try
            {
                constrPermNew.ObjectEstateComplexName = constrPermOld.ObjectEstateComplexName;
            }
            catch { }

            try
            {
                constrPermNew.ObjectName = constrPermOld.ObjectName;
            }
            catch { }
            try
            {
                constrPermNew.OKSOtherIndicators = constrPermOld.OKSOtherIndicators;
            }
            catch { }
            try
            {
                constrPermNew.OrgExpertName = constrPermOld.OrgExpertName;
            }
            catch { }
            try
            {
                constrPermNew.OtherIndicatorsLineProject = constrPermOld.OtherIndicatorsLineProject;
            }
            catch { }

            try
            {
                foreach (Parcel p in constrPermOld.Parcels)
                {
                    constrPermNew.Parcels.Add(p);
                }
            }
            catch { }
            try
            {
                constrPermNew.PlanningDocInfo = constrPermOld.PlanningDocInfo;
            }
            catch { }
            try
            {
                constrPermNew.PowerLineProject = constrPermOld.PowerLineProject;
            }
            catch { }
            try
            {
                constrPermNew.ProjectDocInfo = constrPermOld.ProjectDocInfo;
            }
            catch { }
            try
            {
                constrPermNew.TotalBuildSquareProject = constrPermOld.TotalBuildSquareProject;
            }
            catch { }
            try
            {
                constrPermNew.TotalLivingSpaceProject = constrPermOld.TotalLivingSpaceProject;
            }
            catch { }
            try
            {
                constrPermNew.UnderGroundFloorCount = constrPermOld.UnderGroundFloorCount;
            }
            catch { }

            try
            {
                constrPermNew.WorkKind = constrPermOld.WorkKind;
            }
            catch { }
            constrPermNew.EDocStatus = AISOGD.Enums.eDocStatus.Подготовка;
            constrPermNew.PermKind = AISOGD.Enums.ePermKind.Новое;
            constrPermNew.Save();
            os.CommitChanges();
            RefreshAllWindows();
        }

        // обновить все окна
        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }
    }
}
