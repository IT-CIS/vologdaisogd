﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class UsePermNoticeIZDLettersController : ViewController
    {
        public UsePermNoticeIZDLettersController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Сформировать сопроводительные письма при несоответствии ввода
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsePermNoticeIZDLettersAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            UsePermNoticeIZD perm = os.GetObjectByKey<UsePermNoticeIZD>(x_id);


            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatesPath = "";

            templatesPath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\UsePermNoticeIzdRefuseLetters");

            try
            {
                foreach (string templatePath in Directory.GetFiles(templatesPath, "*.rtf"))
                {
                    try
                    {
                        richServer.LoadDocumentTemplate(templatePath);

                        // Сведения о земельном участке
                        try
                        {
                            string ParcelInfo = "";

                            ParcelInfo = $"с кадастровым номером {perm.LotCadNo ?? "[Не указан]"} по адресу: {perm.ObjectAddress ?? "[Не указан]"}";

                            Bookmark ParcelInfoBook = richServer.Document.Bookmarks["ParcelInfoBook"];
                            richServer.Document.Replace(ParcelInfoBook.Range, ParcelInfo.Trim());
                        }
                        catch { }

                        // Подписывающее лицо
                        try
                        {
                            string SignerCapacity = "";
                            try { SignerCapacity = perm.signer.capacity; }
                            catch { }
                            Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                            richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
                        }
                        catch { }
                        try
                        {
                            string SignerName = "";
                            try { SignerName = perm.signer.name; }
                            catch { }
                            Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                            richServer.Document.Replace(SignerNameBook.Range, SignerName);
                        }
                        catch { }

                        string name = templatePath.Substring(templatePath.LastIndexOf(@"\"));
                        name = name.Remove(name.LastIndexOf("."));

                        string path = Path.GetTempPath() + name +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

                        richServer.SaveDocument(path, DocumentFormat.OpenXml);
                        try
                        {
                            System.Diagnostics.Process.Start(path);
                        }
                        catch { }

                    }
                    catch { }
                }
            }
            catch { }
        }
    }
}
