﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class UsePermNoticeIZDTemplateController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UsePermNoticeIZDTemplateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.UsePermNoticeIZDRefuseTemplateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // UsePermNoticeIZDTemplateAction
            // 
            this.UsePermNoticeIZDTemplateAction.Caption = "Соответствие";
            this.UsePermNoticeIZDTemplateAction.ConfirmationMessage = null;
            this.UsePermNoticeIZDTemplateAction.Id = "UsePermNoticeIZDTemplateAction";
            this.UsePermNoticeIZDTemplateAction.ToolTip = null;
            this.UsePermNoticeIZDTemplateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UsePermNoticeIZDTemplateAction_Execute);
            // 
            // UsePermNoticeIZDRefuseTemplateAction
            // 
            this.UsePermNoticeIZDRefuseTemplateAction.Caption = "Несоответствие";
            this.UsePermNoticeIZDRefuseTemplateAction.ConfirmationMessage = null;
            this.UsePermNoticeIZDRefuseTemplateAction.Id = "UsePermNoticeIZDRefuseTemplateAction";
            this.UsePermNoticeIZDRefuseTemplateAction.ToolTip = null;
            this.UsePermNoticeIZDRefuseTemplateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UsePermNoticeIZDRefuseTemplateAction_Execute);
            // 
            // UsePermNoticeIZDTemplateController
            // 
            this.Actions.Add(this.UsePermNoticeIZDTemplateAction);
            this.Actions.Add(this.UsePermNoticeIZDRefuseTemplateAction);
            this.TargetObjectType = typeof(AISOGD.Perm.UsePermNoticeIZD);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction UsePermNoticeIZDTemplateAction;
        private DevExpress.ExpressApp.Actions.SimpleAction UsePermNoticeIZDRefuseTemplateAction;
    }
}
