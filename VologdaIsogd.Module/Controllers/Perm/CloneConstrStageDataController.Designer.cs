﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class CloneConstrStageDataController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CloneConstrStageDataAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CloneConstrStageDataAction
            // 
            this.CloneConstrStageDataAction.Caption = "Скопировать данные";
            this.CloneConstrStageDataAction.ConfirmationMessage = null;
            this.CloneConstrStageDataAction.Id = "CloneConstrStageDataAction";
            this.CloneConstrStageDataAction.TargetObjectType = typeof(AISOGD.Constr.ConstrStage);
            this.CloneConstrStageDataAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CloneConstrStageDataAction.ToolTip = "По данной кнопке вы сможете скопировать данные выбранной стадии строительства.";
            this.CloneConstrStageDataAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CloneConstrStageDataAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CloneConstrStageDataAction_Execute);
            // 
            // CloneConstrStageDataController
            // 
            this.Actions.Add(this.CloneConstrStageDataAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CloneConstrStageDataAction;
    }
}
