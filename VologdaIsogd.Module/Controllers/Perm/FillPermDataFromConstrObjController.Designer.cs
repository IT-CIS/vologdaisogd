﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class FillPermDataFromConstrObjController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FillPermDataFromConstrAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FillConstrObjectDataFromStagesAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FillConstrObjFaktDataAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FillPermDataFromPermNoticeAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // FillPermDataFromConstrAction
            // 
            this.FillPermDataFromConstrAction.Caption = "Заполнить данные по этапам";
            this.FillPermDataFromConstrAction.ConfirmationMessage = null;
            this.FillPermDataFromConstrAction.Id = "FillPermDataFromConstrAction";
            this.FillPermDataFromConstrAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.FillPermDataFromConstrAction.TargetObjectType = typeof(AISOGD.Constr.CapitalStructureBase);
            this.FillPermDataFromConstrAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.FillPermDataFromConstrAction.ToolTip = "Заполняются общие данные по этапам из объекта строительства:\r\n- Населенный пункт\r" +
    "\n- Тип объекта (справочник)\r\n- Вид объекта\r\n- Вид работ";
            this.FillPermDataFromConstrAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.FillPermDataFromConstrAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FillPermDataFromConstrAction_Execute);
            // 
            // FillConstrObjectDataFromStagesAction
            // 
            this.FillConstrObjectDataFromStagesAction.Caption = "Заполнить данные из этапов";
            this.FillConstrObjectDataFromStagesAction.ConfirmationMessage = null;
            this.FillConstrObjectDataFromStagesAction.Id = "FillConstrObjectDataFromStagesAction";
            this.FillConstrObjectDataFromStagesAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.FillConstrObjectDataFromStagesAction.TargetObjectType = typeof(AISOGD.Constr.CapitalStructureBase);
            this.FillConstrObjectDataFromStagesAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.FillConstrObjectDataFromStagesAction.ToolTip = "Заполняются данные по объекту строительства из его этапов:\r\n- Наименование\r\n- Адр" +
    "ес\r\n- Проектные показатели (кроме вместимости)";
            this.FillConstrObjectDataFromStagesAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.FillConstrObjectDataFromStagesAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FillConstrObjectDataFromStagesAction_Execute);
            // 
            // FillConstrObjFaktDataAction
            // 
            this.FillConstrObjFaktDataAction.Caption = "Обновить факт. показатели";
            this.FillConstrObjFaktDataAction.ConfirmationMessage = null;
            this.FillConstrObjFaktDataAction.Id = "FillConstrObjFaktDataAction";
            this.FillConstrObjFaktDataAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.FillConstrObjFaktDataAction.TargetObjectType = typeof(AISOGD.Constr.CapitalStructureBase);
            this.FillConstrObjFaktDataAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.FillConstrObjFaktDataAction.ToolTip = "Заполняются фактические показатели у объекта строительства по показателям разреше" +
    "ний на ввод.";
            this.FillConstrObjFaktDataAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.FillConstrObjFaktDataAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FillConstrObjFaktDataAction_Execute);
            // 
            // FillPermDataFromPermNoticeAction
            // 
            this.FillPermDataFromPermNoticeAction.Caption = "Заполнить данные из Ув.Ст.";
            this.FillPermDataFromPermNoticeAction.ConfirmationMessage = null;
            this.FillPermDataFromPermNoticeAction.Id = "FillPermDataFromPermNoticeAction";
            this.FillPermDataFromPermNoticeAction.ToolTip = null;
            this.FillPermDataFromPermNoticeAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FillPermDataFromPermNoticeAction_Execute);
            // 
            // FillPermDataFromConstrObjController
            // 
            this.Actions.Add(this.FillPermDataFromConstrAction);
            this.Actions.Add(this.FillConstrObjectDataFromStagesAction);
            this.Actions.Add(this.FillConstrObjFaktDataAction);
            this.Actions.Add(this.FillPermDataFromPermNoticeAction);
            this.TargetObjectType = typeof(AISOGD.Constr.CapitalStructureBase);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FillPermDataFromConstrAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FillConstrObjectDataFromStagesAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FillConstrObjFaktDataAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FillPermDataFromPermNoticeAction;
    }
}
