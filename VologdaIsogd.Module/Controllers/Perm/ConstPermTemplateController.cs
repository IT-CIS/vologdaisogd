using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Perm;
using System.Reflection;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using AISOGD.Subject;
using DevExpress.Office;
using AISOGD.Constr;
using AISOGD.OrgStructure;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class ConstPermTemplateController : ViewController
    {
        public ConstPermTemplateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ConstrTemplateCreateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ������ � ��� ������

            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            ConstrPerm perm = os.GetObjectByKey<ConstrPerm>(x_id);
            UnitOfWork unitOfWork = (UnitOfWork)perm.Session;

            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\Perm.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            //����������
            // ������� �������� �������� ��� ������������ � ��������� ������, ����� ����� �� ����� ��� ������� � ������
            string developer = "";
            developer = perm.ConstrDeveloperStringDat?? "";
            //foreach(GeneralSubject docSubject in perm.DocSubjects)
            //{
            //    try {
            //        if (docSubject.NameDat != null && docSubject.NameDat != "")
            //            developer += docSubject.NameDat + ", ";
            //        else
            //            developer += docSubject.FullName + ", ";
            //    }
            //    catch { }
            //}
            //developer = developer.Trim();
            //developer = developer.TrimEnd(',');


            try
                {
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = developer.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                //do
                //{
                //    text1 += words[i] + " ";
                //    i++;
                //}
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterBook1 = richServer.Document.Bookmarks["RequesterBook1"];
                richServer.Document.Replace(RequesterBook1.Range, text1.TrimEnd());
                Bookmark RequesterBook2 = richServer.Document.Bookmarks["RequesterBook2"];
                richServer.Document.Replace(RequesterBook2.Range, text2.TrimEnd());
            }
            catch { }

            // ����� �����������
            try
            {
                string RequesterAddr = "";
                try { RequesterAddr = perm.ConstrDeveloperContactInfo;
                    RequesterAddr = RequesterAddr.Replace("�����:", "");
                }
                catch { }
                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = RequesterAddr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 25)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterAddrBook1 = richServer.Document.Bookmarks["RequesterAddrBook1"];
                richServer.Document.Replace(RequesterAddrBook1.Range, text1);
                Bookmark RequesterAddrBook2 = richServer.Document.Bookmarks["RequesterAddrBook2"];
                richServer.Document.Replace(RequesterAddrBook2.Range, text2);
                Bookmark RequesterAddrBook3 = richServer.Document.Bookmarks["RequesterAddrBook3"];
                richServer.Document.Replace(RequesterAddrBook3.Range, text3);
            }
            catch { }

            // perm.ConstrDeveloper.RequesterEmail; }

            // ���� ���������
            try
            {
                string DocDate = "";
                try { DocDate = perm.DocDate.ToShortDateString() + " �."; }
                catch { }
                Bookmark DocDateBook = richServer.Document.Bookmarks["DocDateBook"];
                richServer.Document.Replace(DocDateBook.Range, DocDate);
            }
            catch { }

            // ����� ���������
            try
            {
                string DocNo = "        ";
                //try { DocNo = perm.DocNo; }
                //catch { }
                Bookmark DocNoBook = richServer.Document.Bookmarks["DocNoBook"];
                richServer.Document.Replace(DocNoBook.Range, DocNo);
            }
            catch { }

            // ������� ���
            try
            {
                string YearDocDate = DateTime.Now.Year.ToString();
                if (perm.EDocStatus == AISOGD.Enums.eDocStatus.��������� || perm.EDocStatus == AISOGD.Enums.eDocStatus.����)
                    if(perm.DocDate != DateTime.MinValue)
                        YearDocDate = perm.DocDate.Year.ToString();
                Bookmark YearDocDateBook = richServer.Document.Bookmarks["YearDocDateBook"];
                richServer.Document.Replace(YearDocDateBook.Range, YearDocDate);
            }
            catch { }

            // ������
//            try
//            {
//                int i = 0;
//                Table table = richServer.Document.Tables[1];
//                if(perm.ConstrPermInstead != null)
//                {
//                    string instead = "";
//                    instead = String.Format(@"(������ ���������� �� �������������
//�� {0}  �  {1})", perm.ConstrPermInstead.DocDate.ToShortDateString(), perm.ConstrPermInstead.DocNo);

//                    //table.Rows.InsertAfter(0);
//                    //range = table.Rows[1].Cells[4].ContentRange;
//                    //richServer.Document.Replace(range, instead);

//                    range = table.Range;
//                    DocumentPosition pos = range.End;
//                    richServer.Document.InsertText(pos, instead);
//                    ////pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
//                    //richServer.Document.InsertText(pos, Characters.LineBreak.ToString());
//                    //pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
//                    //richServer.Document.GetText(range);
//                    //richServer.Document.InsertDocumentContent(pos, range);
//                    ////richServer.Document.CaretPosition = pos;
//                    ////richServer.Document.Paste();
//                    //range = table.Rows[0].Cells[1].ContentRange;
//                    //richServer.Document.Replace(range, PlaceNo);
//                }
//            }
//            catch { }
            try
            {
                string instead = "";
                if (perm.ConstrPermInstead != null)
                {
                    instead = String.Format(@"(������ � {1} �� {0} �.)", perm.ConstrPermInstead.DocDate.ToShortDateString(), perm.ConstrPermInstead.FullDocNo);
                }
                Bookmark PermInsteadBook = richServer.Document.Bookmarks["PermInsteadBook"];
                richServer.Document.InsertText(PermInsteadBook.Range.Start, instead);

                SubDocument doc = PermInsteadBook.Range.BeginUpdateDocument();
                DevExpress.XtraRichEdit.API.Native.ParagraphProperties pp = doc.BeginUpdateParagraphs(PermInsteadBook.Range);
                pp.SpacingAfter = 1.5f;
                pp.SpacingBefore = 1.5f;
                doc.EndUpdateParagraphs(pp);
            }
            catch { }

            // ��� �����
            try
            {
                String PermKind1 = "";
                String PermKind2 = "";
                String PermKind3 = "";
                String PermKind4 = "";
                String PermKind5 = "";
                if (perm.WorkKind == AISOGD.Enums.eWorkKind.�������������)
                {
                    if (perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.�������������������������������)
                    {
                        PermKind1 = "V";
                    }
                    if (perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.��������������)// || perm.BuildingKind == AISOGD.Enums.BuildingKind.��������������������)
                    {
                        PermKind4 = "V";
                    }
                }
                if (perm.WorkKind == AISOGD.Enums.eWorkKind.������������������)
                {
                    PermKind3 = "V";
                }
                if (perm.WorkKind == AISOGD.Enums.eWorkKind.�������������)
                {
                    if (perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.�������������������������������)
                    {
                        PermKind2 = "V";
                    }
                    if (perm.BuildingCategory == AISOGD.Enums.eBuildingCategory.��������������)// || perm.BuildingKind == AISOGD.Enums.BuildingKind.��������������������)
                    {
                        PermKind5 = "V";
                    }
                }
                Bookmark PermKindBook1 = richServer.Document.Bookmarks["PermKindBook1"];
                richServer.Document.Replace(PermKindBook1.Range, PermKind1);
                Bookmark PermKindBook2 = richServer.Document.Bookmarks["PermKindBook2"];
                richServer.Document.Replace(PermKindBook2.Range, PermKind2);
                Bookmark PermKindBook3 = richServer.Document.Bookmarks["PermKindBook3"];
                richServer.Document.Replace(PermKindBook3.Range, PermKind3);
                Bookmark PermKindBook4 = richServer.Document.Bookmarks["PermKindBook4"];
                richServer.Document.Replace(PermKindBook4.Range, PermKind4);
                Bookmark PermKindBook5 = richServer.Document.Bookmarks["PermKindBook5"];
                richServer.Document.Replace(PermKindBook5.Range, PermKind5);
            }
            catch { }

            // ������������ �������
            try
            {
                string ObjectName = "";
                try { ObjectName = perm.ObjectName; }
                catch { }
                Bookmark ObjectNameBook = richServer.Document.Bookmarks["ObjectNameBook"];
                richServer.Document.Replace(ObjectNameBook.Range, ObjectName);
            }
            catch { }

            // ������������ �����������, �������� ���������� ����������
            try
            {

                string OrgExpertName = "";
                try { OrgExpertName = perm.OrgExpertName; }
                catch { }
                Bookmark OrgExpertNameBook = richServer.Document.Bookmarks["OrgExpertNameBook"];
                richServer.Document.Replace(OrgExpertNameBook.Range, OrgExpertName.Trim());
            }
            catch { }

            // �������� � ���������� ���������� � ��������� ������� �� �����������
            try
            {
                string ExpertInfo = "";
                try { ExpertInfo = perm.ExpertInfo; }
                catch { }
                Bookmark ExpertInfoBook = richServer.Document.Bookmarks["ExpertInfoBook"];
                richServer.Document.Replace(ExpertInfoBook.Range, ExpertInfo.Trim());
            }
            catch { }

            // ����������� ����� ���������� �������(��)
            try
            {
                string LotCadNo = "";
                try
                {
                    foreach (string lcn in perm.LotCadNo.Split(','))
                    {
                        LotCadNo += lcn.Trim() + Environment.NewLine;
                    }
                    LotCadNo.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                Bookmark LotCadNoBook = richServer.Document.Bookmarks["LotCadNoBook"];
                richServer.Document.Replace(LotCadNoBook.Range, LotCadNo);
            }
            catch { }


            // ����� ������������ ��������
            try
            {
                string CadKvartalNo = "";
                try { CadKvartalNo = perm.CadKvartalNo; }
                catch { }
                Bookmark CadKvartalNoBook = richServer.Document.Bookmarks["CadKvartalNoBook"];
                richServer.Document.Replace(CadKvartalNoBook.Range, CadKvartalNo);
            }
            catch { }

            // ����������� ����� ������������������� ���
            try
            {
                string ObjChangeCadNo = "";
                try { ObjChangeCadNo = perm.ObjChangeCadNo; }
                catch { }
                Bookmark ObjChangeCadNoBook = richServer.Document.Bookmarks["ObjChangeCadNoBook"];
                richServer.Document.Replace(ObjChangeCadNoBook.Range, ObjChangeCadNo);
            }
            catch { }

            // �������� � ����������������� ����� ���������� �������
            try
            {
                string GPZU = "";
                try { GPZU = perm.GPZU; }
                catch { }
                Bookmark GPZUBook = richServer.Document.Bookmarks["GPZUBook"];
                richServer.Document.Replace(GPZUBook.Range, GPZU);
            }
            catch { }

            // �������� � ������� ���������� � ������� ���������
            try
            {
                string PlanningDocInfo = "";
                try { PlanningDocInfo = perm.PlanningDocInfo; }
                catch { }
                Bookmark PlanningDocInfoBook = richServer.Document.Bookmarks["PlanningDocInfoBook"];
                richServer.Document.Replace(PlanningDocInfoBook.Range, PlanningDocInfo);
            }
            catch { }

            // �������� � ��������� ������������
            try
            {
                string ProjectDocInfo = "";
                try { ProjectDocInfo = perm.ProjectDocInfo; }
                catch { }
                Bookmark ProjectDocInfoBook = richServer.Document.Bookmarks["ProjectDocInfoBook"];
                richServer.Document.Replace(ProjectDocInfoBook.Range, ProjectDocInfo);
            }
            catch { }

            // ������������ ���, ��������� � ������ �������������� ���������
            try
            {
                string ObjectEstateComplexName = "";
                try {
                    if (perm.ObjectEstateComplexName != null)
                        ObjectEstateComplexName = Environment.NewLine + perm.ObjectEstateComplexName; }
                catch { }
                Bookmark ObjectEstateComplexNameBook = richServer.Document.Bookmarks["ObjectEstateComplexNameBook"];
                richServer.Document.Replace(ObjectEstateComplexNameBook.Range, ObjectEstateComplexName);
            }
            catch { }

            // ������� ��������������
            try {
                string TotalBuildSquareProjectText = "����� ������� ������ ������ (��. �):";
                try
                {
                    if(perm.BuildingKind != AISOGD.Enums.eBuildingKind.����� )
                        TotalBuildSquareProjectText = "����� ������� ������ (��. �):";
                }
                catch { }
                Bookmark TotalBuildSquareProjectTextBook = richServer.Document.Bookmarks["TotalBuildSquareProjectTextBook"];
                richServer.Document.Replace(TotalBuildSquareProjectTextBook.Range, TotalBuildSquareProjectText);
            }
            catch { }
            try
            {
                string TotalBuildSquareProject = "";
                try
                {
                    TotalBuildSquareProject = perm.TotalBuildSquareProject;
                }
                catch { }
                Bookmark TotalBuildSquareProjectBook = richServer.Document.Bookmarks["TotalBuildSquareProjectBook"];
                richServer.Document.Replace(TotalBuildSquareProjectBook.Range, TotalBuildSquareProject);
            }
            catch { }
            try
            {
                string LotSquare = "";
                try
                {
                    LotSquare = perm.LotSquare.ToString();
                }
                catch { }
                Bookmark LotSquareBook = richServer.Document.Bookmarks["LotSquareBook"];
                richServer.Document.Replace(LotSquareBook.Range, LotSquare);
            }
            catch { }
            try
            {
                string BuildingSizeProject = "";
                try
                {
                    BuildingSizeProject = perm.BuildingSizeProject.ToString();
                }
                catch { }
                Bookmark BuildingSizeProjectBook = richServer.Document.Bookmarks["BuildingSizeProjectBook"];
                richServer.Document.Replace(BuildingSizeProjectBook.Range, BuildingSizeProject);
            }
            catch { }
            try
            {
                string BuildingSizeUnderGroundPartProject = "";
                try
                {
                    BuildingSizeUnderGroundPartProject = perm.BuildingSizeUnderGroundPartProject.ToString();
                }
                catch { }
                Bookmark BuildingSizeUnderGroundPartProjectBook = richServer.Document.Bookmarks["BuildingSizeUnderGroundPartProjectBook"];
                richServer.Document.Replace(BuildingSizeUnderGroundPartProjectBook.Range, BuildingSizeUnderGroundPartProject);
            }
            catch { }
            try
            {
                string FloorCount = "";
                try
                {
                    FloorCount = perm.FloorCount.ToString();
                }
                catch { }
                Bookmark FloorCountBook = richServer.Document.Bookmarks["FloorCountBook"];
                richServer.Document.Replace(FloorCountBook.Range, FloorCount);
            }
            catch { }
            try
            {
                string Height = "";
                // 
                
                try
                {
                    if (perm.ConstrStages.Count > 1)
                        Height = perm.Height.ToString();
                    else
                    {
                        if (perm.ConstrStages[0].Height != 0)
                        {
                            Height = perm.ConstrStages[0].Height.ToString();
                            if(perm.ConstrStages[0].HeightNote != null && perm.ConstrStages[0].HeightNote != "")
                            {
                                string HeightNote = perm.ConstrStages[0].HeightNote + " ";
                                Bookmark HeightNoteBook = richServer.Document.Bookmarks["HeightNoteBook"];
                                richServer.Document.Replace(HeightNoteBook.Range, HeightNote);
                            }
                        }
                        else
                            Height = "-";
                    }
                }
                catch { }
                Bookmark HeightBook = richServer.Document.Bookmarks["HeightBook"];
                richServer.Document.Replace(HeightBook.Range, Height);
            }
            catch { }
            try
            {
                string UnderGroundFloorCount = "";
                try {
                        UnderGroundFloorCount = perm.UnderGroundFloorCount.ToString(); }
                catch { }
                Bookmark UnderGroundFloorCountBook = richServer.Document.Bookmarks["UnderGroundFloorCountBook"];
                richServer.Document.Replace(UnderGroundFloorCountBook.Range, UnderGroundFloorCount);
            }
            catch { }
            try
            {
                string Capacity = "";
                try {
                        Capacity = perm.Capacity.ToString(); }
                catch { }
                Bookmark CapacityBook = richServer.Document.Bookmarks["CapacityBook"];
                richServer.Document.Replace(CapacityBook.Range, Capacity);
            }
            catch { }
            try
            {
                string BuildSquare = "";
                try {
                        BuildSquare = perm.BuildSquare.ToString(); }
                catch { }
                Bookmark BuildSquareBook = richServer.Document.Bookmarks["BuildSquareBook"];
                richServer.Document.Replace(BuildSquareBook.Range, BuildSquare);
            }
            catch { }
            try
            {
                string OKSOtherIndicators = "";
                try { OKSOtherIndicators = perm.OKSOtherIndicators.ToString(); }
                catch { }
                Bookmark OKSOtherIndicatorsBook = richServer.Document.Bookmarks["OKSOtherIndicatorsBook"];
                richServer.Document.Replace(OKSOtherIndicatorsBook.Range, OKSOtherIndicators);
            }
            catch { }

            //����� (��������������) �������
            try
            {
                string ObjectAddress = "";
                try { if (perm.ConstrStages.Count == 1)
                        ObjectAddress = "���������� ���������, ����������� �������, ������������� ����������� ������ �������, " + perm.ObjectAddress.ToString();
                    else
                    {
                        List<string> addrList = new List<string>();
                        SortProperty sort = new SortProperty("StageNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
                        perm.ConstrStages.Sorting.Add(sort);
                        foreach (ConstrStage stage in perm.ConstrStages)
                        {
                            string addr = "";
                            if(stage.StageNo != null && stage.StageNo != "")
                                addr += stage.StageNo + " - ���������� ���������, ����������� �������, ������������� ����������� ������ �������, " + stage.Address;
                            else
                                addr += "���������� ���������, ����������� �������, ������������� ����������� ������ �������, " + stage.Address;
                            if (!addrList.Contains(addr))
                                addrList.Add(addr);
                            //ObjectAddress += Environment.NewLine;
                        }
                        foreach (string a in addrList)
                        { ObjectAddress += a + Environment.NewLine; }
                    }
               
                    ObjectAddress = ObjectAddress.TrimEnd(Environment.NewLine.ToCharArray());
                }
                catch { }
                ObjectAddress.Replace("�����:","");
                Bookmark ObjectAddressBook = richServer.Document.Bookmarks["ObjectAddressBook"];
                richServer.Document.Replace(ObjectAddressBook.Range, ObjectAddress);
            }
            catch { }

            // ������� ��������� �������������� ��������� �������
            try
            {
                string LineObjectClass = "";
                try { LineObjectClass = perm.LineObjectClass.Name; }
                catch { }
                Bookmark LineObjectClassBook = richServer.Document.Bookmarks["LineObjectClassBook"];
                richServer.Document.Replace(LineObjectClassBook.Range, LineObjectClass);
            }
            catch { }

            try
            {
                string LengthProject = "";
                try {
                        LengthProject = perm.LengthProject.ToString(); }
                catch { }
                Bookmark LengthProjectBook = richServer.Document.Bookmarks["LengthProjectBook"];
                richServer.Document.Replace(LengthProjectBook.Range, LengthProject);
            }
            catch { }
            try
            {
                string PowerLineProject = "";
                try { 
                    PowerLineProject = perm.PowerLineProject.ToString(); }
                catch { }
                Bookmark PowerLineProjectBook = richServer.Document.Bookmarks["PowerLineProjectBook"];
                richServer.Document.Replace(PowerLineProjectBook.Range, PowerLineProject);
            }
            catch { }
            try
            {
                string ElectricLinesInfoProject = "";
                try { ElectricLinesInfoProject = perm.ElectricLinesInfoProject.ToString(); }
                catch { }
                Bookmark ElectricLinesInfoProjectBook = richServer.Document.Bookmarks["ElectricLinesInfoProjectBook"];
                richServer.Document.Replace(ElectricLinesInfoProjectBook.Range, ElectricLinesInfoProject);
            }
            catch { }
            try
            {
                string ConstructiveElementsInfoProject = "";
                try { ConstructiveElementsInfoProject = perm.ConstructiveElementsInfoProject.ToString(); }
                catch { }
                Bookmark ConstructiveElementsInfoProjectBook = richServer.Document.Bookmarks["ConstructiveElementsInfoProjectBook"];
                richServer.Document.Replace(ConstructiveElementsInfoProjectBook.Range, ConstructiveElementsInfoProject);
            }
            catch { }
            try
            {
                string OtherIndicatorsLineProject = "";
                try { OtherIndicatorsLineProject = perm.OtherIndicatorsLineProject.ToString(); }
                catch { }
                Bookmark OtherIndicatorsLineProjectBook = richServer.Document.Bookmarks["OtherIndicatorsLineProjectBook"];
                richServer.Document.Replace(OtherIndicatorsLineProjectBook.Range, OtherIndicatorsLineProject);
            }
            catch { }

            // ���� �������� ����������
            try
            {
                string ValidDayDate = "";
                try {
                    string d = perm.ValidDate.Day.ToString();
                    if (d.Length == 1)
                        d = "0" + d;
                    ValidDayDate =  d;}
                catch { }
                Bookmark ValidDayDateBook = richServer.Document.Bookmarks["ValidDayDateBook"];
                richServer.Document.Replace(ValidDayDateBook.Range, ValidDayDate);
            }
            catch { }
            try
            {
                string ValidMonthDate = "";
                try
                {
                    switch (perm.ValidDate.Month)
                    {
                        case 1:
                            ValidMonthDate = "������";
                            break;
                        case 2:
                            ValidMonthDate = "�������";
                            break;
                        case 3:
                            ValidMonthDate = "�����";
                            break;
                        case 4:
                            ValidMonthDate = "������";
                            break;
                        case 5:
                            ValidMonthDate = "���";
                            break;
                        case 6:
                            ValidMonthDate = "����";
                            break;
                        case 7:
                            ValidMonthDate = "����";
                            break;
                        case 8:
                            ValidMonthDate = "�������";
                            break;
                        case 9:
                            ValidMonthDate = "��������";
                            break;
                        case 10:
                            ValidMonthDate = "�������";
                            break;
                        case 11:
                            ValidMonthDate = "������";
                            break;
                        case 12:
                            ValidMonthDate = "�������";
                            break;
                        default:
                            ValidMonthDate = perm.ValidDate.Month.ToString();
                            break;
                    }
                }
                catch { }
                Bookmark ValidMonthDateBook = richServer.Document.Bookmarks["ValidMonthDateBook"];
                richServer.Document.Replace(ValidMonthDateBook.Range, ValidMonthDate);
            }
            catch { }

            try
            {
                string ValidYearDate = "";
                try { ValidYearDate = perm.ValidDate.Year.ToString().Substring(2); }
                catch { }
                Bookmark ValidYearDateBook = richServer.Document.Bookmarks["ValidYearDateBook"];
                richServer.Document.Replace(ValidYearDateBook.Range, ValidYearDate);
            }
            catch { }

           

            //���� �������� � ������������ � 
            // �������, ��� ������ ������ 54 ������� 
            //try
            //{
            //    //string text1 = "";
            //    //string text2 = "";
            //    //string[] separators = {" "};
            //    //string[] words = perm.ValidDateAccording.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            //    //int i = 0;
            //    //int wordscount = words.Length;
            //    ////do
            //    ////{
            //    ////    text1 += words[i] + " ";
            //    ////    i++;
            //    ////}
            //    //while (i < wordscount && (text1 + words[i]).Length < 54)
            //    //{
            //    //    text1 += words[i] + " ";
            //    //    i++;
            //    //}
            //    //if (i < wordscount)
            //    //{
            //    //    while (i < wordscount)
            //    //    {
            //    //        text2 += words[i] + " ";
            //    //        i++;
            //    //    }
            //    //}
            //    //string text = "";
            //    //if (perm.BuildingKind == AISOGD.Enums.BuildingKind.��������������)
            //    //    text = "5";//"�.19 ��.51 ������������������ ������� ���������� ��������� � 190-�� �� 29.12.2004 (� ����������� � ������������), �������� 5 ������� ����������� �������������";
            //    //else
            //    //    text = "6";//"�.19 ��.51 ������������������ ������� ���������� ��������� � 190-�� �� 29.12.2004 (� ����������� � ������������), �������� 6 ������� ����������� �������������";
            //    Bookmark ValidDateBook1 = richServer.Document.Bookmarks["ValidDateBook1"];
            //    richServer.Document.Replace(ValidDateBook1.Range, text.TrimEnd());
            //    //Bookmark ValidDateBook2 = richServer.Document.Bookmarks["ValidDateBook2"];
            //    //richServer.Document.Replace(ValidDateBook2.Range, text2.TrimEnd());
            //}
            //catch { }

            // ������������� ����
            try
            {
                string SignerCapacity = "";
                try { SignerCapacity = perm.signer.capacity; }
                catch { }
                Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
            }
            catch { }
            try
            {
                string SignerName = "";
                try { SignerName = perm.signer.name; }
                catch { }
                Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                richServer.Document.Replace(SignerNameBook.Range, SignerName);
            }
            catch { }

            // ����
            try
            {
                string DayDate = "";
                try
                {
                    string d = DateTime.Now.Day.ToString();
                    if (d.Length == 1)
                        d = "0" + d;
                    DayDate = d;
                }
                catch { }
                Bookmark DayDateBook = richServer.Document.Bookmarks["DayDateBook"];
                richServer.Document.Replace(DayDateBook.Range, DayDate);
            }
            catch { }

            try
            {
                string MonthDate = "";
                try
                {
                    switch (DateTime.Now.Month)
                    {
                        case 1:
                            MonthDate = "������";
                            break;
                        case 2:
                            MonthDate = "�������";
                            break;
                        case 3:
                            MonthDate = "�����";
                            break;
                        case 4:
                            MonthDate = "������";
                            break;
                        case 5:
                            MonthDate = "���";
                            break;
                        case 6:
                            MonthDate = "����";
                            break;
                        case 7:
                            MonthDate = "����";
                            break;
                        case 8:
                            MonthDate = "�������";
                            break;
                        case 9:
                            MonthDate = "��������";
                            break;
                        case 10:
                            MonthDate = "�������";
                            break;
                        case 11:
                            MonthDate = "������";
                            break;
                        case 12:
                            MonthDate = "�������";
                            break;
                        default:
                            MonthDate = DateTime.Now.Month.ToString();
                            break;
                    }
                }
                catch { }
                Bookmark MonthDateBook = richServer.Document.Bookmarks["MonthDateBook"];
                richServer.Document.Replace(MonthDateBook.Range, MonthDate);
            }
            catch { }
            try
            {
                string YearDate = "";
                try { YearDate = DateTime.Now.Year.ToString().Substring(2); }
                catch { }
                Bookmark YearDateBook = richServer.Document.Bookmarks["YearDateBook"];
                richServer.Document.Replace(YearDateBook.Range, YearDate);
            }
            catch { }

            string permInfo = "";
            try { permInfo = String.Format($"� {perm.FullDocNo} �� {perm.DocDate.ToShortDateString()}"); }
            catch { }
            string path = Path.GetTempPath() + @"\���������� �� �������������_" + permInfo + "_" +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }

        }
    }
}
