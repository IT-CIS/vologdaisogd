﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class PermReportsController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateConstrPermReportParamAction = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            this.UsePermElitReportAction = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            this.CreateFNSPermReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CreateUsePermElitReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CreateConstrPermReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CreateConstrPermReportParamAction
            // 
            this.CreateConstrPermReportParamAction.Caption = "Отчет";
            this.CreateConstrPermReportParamAction.ConfirmationMessage = null;
            this.CreateConstrPermReportParamAction.Id = "CreateConstrPermReportParamAction";
            this.CreateConstrPermReportParamAction.NullValuePrompt = null;
            this.CreateConstrPermReportParamAction.ShortCaption = null;
            this.CreateConstrPermReportParamAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.CreateConstrPermReportParamAction.ToolTip = null;
            this.CreateConstrPermReportParamAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.CreateConstrPermReportParamAction.ValueType = typeof(System.DateTime);
            this.CreateConstrPermReportParamAction.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.CreateConstrPermReportParamAction_Execute);
            // 
            // UsePermElitReportAction
            // 
            this.UsePermElitReportAction.Caption = "Элит. жилье";
            this.UsePermElitReportAction.ConfirmationMessage = null;
            this.UsePermElitReportAction.Id = "UsePermElitReportAction";
            this.UsePermElitReportAction.NullValuePrompt = null;
            this.UsePermElitReportAction.ShortCaption = null;
            this.UsePermElitReportAction.ToolTip = null;
            this.UsePermElitReportAction.ValueType = typeof(System.DateTime);
            this.UsePermElitReportAction.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.UsePermElitReportAction_Execute);
            // 
            // CreateFNSPermReportAction
            // 
            this.CreateFNSPermReportAction.Caption = "Отчет в налоговую";
            this.CreateFNSPermReportAction.ConfirmationMessage = null;
            this.CreateFNSPermReportAction.Id = "CreateFNSPermReportAction";
            this.CreateFNSPermReportAction.ToolTip = null;
            this.CreateFNSPermReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateFNSPermReportAction_Execute);
            // 
            // CreateUsePermElitReportAction
            // 
            this.CreateUsePermElitReportAction.Caption = "Отчет по элит. жилью";
            this.CreateUsePermElitReportAction.ConfirmationMessage = null;
            this.CreateUsePermElitReportAction.Id = "CreateUsePermElitReportAction";
            this.CreateUsePermElitReportAction.ToolTip = null;
            this.CreateUsePermElitReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateUsePermElitReportAction_Execute);
            // 
            // CreateConstrPermReportAction
            // 
            this.CreateConstrPermReportAction.Caption = "Отчет в статистику";
            this.CreateConstrPermReportAction.ConfirmationMessage = null;
            this.CreateConstrPermReportAction.Id = "CreateConstrPermReportAction";
            this.CreateConstrPermReportAction.ToolTip = null;
            this.CreateConstrPermReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateConstrPermReportAction_Execute);
            // 
            // PermReportsController
            // 
            this.Actions.Add(this.CreateConstrPermReportParamAction);
            this.Actions.Add(this.UsePermElitReportAction);
            this.Actions.Add(this.CreateFNSPermReportAction);
            this.Actions.Add(this.CreateUsePermElitReportAction);
            this.Actions.Add(this.CreateConstrPermReportAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction CreateConstrPermReportParamAction;
        private DevExpress.ExpressApp.Actions.ParametrizedAction UsePermElitReportAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreateFNSPermReportAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreateUsePermElitReportAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreateConstrPermReportAction;
    }
}
