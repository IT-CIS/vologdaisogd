﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraEditors;
using AISOGD.Perm;
using DevExpress.XtraRichEdit;
using System.Reflection;
using DevExpress.XtraRichEdit.API.Native;
using AISOGD.OrgStructure;
using System.IO;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InfoListTemplateController : ViewController
    {
        public InfoListTemplateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            //var currentObject = View.CurrentObject as XPBaseObject;
            Frame.GetController<InfoListTemplateController>().InfoListTemplateAction.Active.SetItemValue("myReason", false);
            if (View is DetailView)
            {
                //if(currentObject == )
                if (View.Id == "ConstrPerm_DetailView" ||
                    View.Id == "UsePerm_DetailView")
                {
                    Frame.GetController<InfoListTemplateController>().InfoListTemplateAction.Active.SetItemValue("myReason", true);
                }
                else
                {
                    Frame.GetController<InfoListTemplateController>().InfoListTemplateAction.Active.SetItemValue("myReason", false);
                }
            }
            else
            {
                Frame.GetController<InfoListTemplateController>().InfoListTemplateAction.Active.SetItemValue("myReason", false);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Формирование информационного листа согласования документа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InfoListTemplateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();

            string classT = ((BaseObject)e.CurrentObject).ClassInfo.ClassType.ToString();
            //XtraMessageBox.Show(classT);
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\PermInfoList.rtf");
            

            if (classT == "AISOGD.Perm.ConstrPerm")
            {
                ConstrPerm perm = os.GetObjectByKey<ConstrPerm>(x_id);
                FormListInfoConstrPermReport(os, perm, templatePath);
            }
            else
                if (classT == "AISOGD.Perm.UsePerm")
            {
                UsePerm perm = os.GetObjectByKey<UsePerm>(x_id);
                FormListInfoUserPermReport(os, perm, templatePath);
            }
        }

        private void FormListInfoConstrPermReport(IObjectSpace os, ConstrPerm perm, string templatePath)
        {
            UnitOfWork unitOfWork = (UnitOfWork)perm.Session;

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            // лист согласований (информационный лист)
            try
            {
                string LetterInfo = "";
                try { LetterInfo = perm.LetterInfo; }
                catch { }
                Bookmark LetterInfoBook = richServer.Document.Bookmarks["LetterInfoBook"];
                richServer.Document.Replace(LetterInfoBook.Range, LetterInfo);
            }
            catch { }
            try
            {
                string ObjectName1 = "";
                try { ObjectName1 = perm.ObjectName; }
                catch { }
                Bookmark ObjectNameBook1 = richServer.Document.Bookmarks["ObjectNameBook1"];
                richServer.Document.Replace(ObjectNameBook1.Range, ObjectName1);
            }
            catch { }
            try
            {
                string PermInfo = "";
                try { PermInfo = perm.PermInfo; }
                catch { }
                Bookmark PermInfoBook1 = richServer.Document.Bookmarks["PermInfoBook1"];
                richServer.Document.Replace(PermInfoBook1.Range, PermInfo);
                Bookmark PermInfoBook2 = richServer.Document.Bookmarks["PermInfoBook2"];
                richServer.Document.Replace(PermInfoBook2.Range, PermInfo);
            }
            catch { }

            try
            {
                string PermInfoR = "";
                try { PermInfoR = perm.PermInfoR; }
                catch { }
                Bookmark PermInfoRBook1 = richServer.Document.Bookmarks["PermInfoRBook1"];
                richServer.Document.Replace(PermInfoRBook1.Range, PermInfoR);
                Bookmark PermInfoRBook2 = richServer.Document.Bookmarks["PermInfoRBook2"];
                richServer.Document.Replace(PermInfoRBook2.Range, PermInfoR);
            }
            catch { }

            try
            {
                string Decision1Name = "";
                string Decision1Position = "";
                try { Decision1Name = perm.Decision1.BriefName; }
                catch { }
                try
                {
                    if (perm.Decision1.PositionLink.NameFrom != null && perm.Decision1.PositionLink.NameFrom != "")
                        Decision1Position = perm.Decision1.PositionLink.NameFrom;
                    else
                        Decision1Position = perm.Decision1.PositionLink.Name;
                }
                catch { }
                if (Decision1Name != "")
                {
                    Bookmark Decision1NameBook1 = richServer.Document.Bookmarks["Decision1NameBook1"];
                    richServer.Document.Replace(Decision1NameBook1.Range, Decision1Name);
                    Bookmark Decision1NameBook2 = richServer.Document.Bookmarks["Decision1NameBook2"];
                    richServer.Document.Replace(Decision1NameBook2.Range, Decision1Name);
                }
                if (Decision1Position != "")
                {
                    Bookmark Decision1PositionBook1 = richServer.Document.Bookmarks["Decision1PositionBook1"];
                    richServer.Document.Replace(Decision1PositionBook1.Range, Decision1Position);
                }
            }
            catch { }

            try
            {
                string Decision2Name = "";
                string Decision2Position = "";
                try { Decision2Name = perm.Decision2.BriefName; }
                catch { }
                if (Decision2Name != "")
                {
                    Bookmark Decision2NameBook1 = richServer.Document.Bookmarks["Decision2NameBook1"];
                    richServer.Document.Replace(Decision2NameBook1.Range, Decision2Name);
                    Bookmark Decision2NameBook2 = richServer.Document.Bookmarks["Decision2NameBook2"];
                    richServer.Document.Replace(Decision2NameBook2.Range, Decision2Name);
                }
                try
                {
                    if (perm.Decision2.PositionLink.NameFrom != null && perm.Decision2.PositionLink.NameFrom != "")
                        Decision2Position = perm.Decision2.PositionLink.NameFrom;
                    else
                        Decision2Position = perm.Decision2.PositionLink.Name;
                }
                catch { }
                if (Decision2Position != "")
                {
                    Bookmark Decision2PositionBook1 = richServer.Document.Bookmarks["Decision2PositionBook1"];
                    richServer.Document.Replace(Decision2PositionBook1.Range, Decision2Position);
                }
            }
            catch { }

            try
            {
                List<Employee> EmployeeList = new List<Employee>();
                if (perm.AgreementEmploye1 != null)
                    EmployeeList.Add(perm.AgreementEmploye1);
                if (perm.AgreementEmploye2 != null)
                    EmployeeList.Add(perm.AgreementEmploye2);
                if (perm.AgreementEmploye3 != null)
                    EmployeeList.Add(perm.AgreementEmploye3);
                if (perm.AgreementEmploye4 != null)
                    EmployeeList.Add(perm.AgreementEmploye4);
                if (perm.AgreementEmploye5 != null)
                    EmployeeList.Add(perm.AgreementEmploye5);
                if (perm.AgreementEmploye6 != null)
                    EmployeeList.Add(perm.AgreementEmploye6);
                if (perm.AgreementEmploye7 != null)
                    EmployeeList.Add(perm.AgreementEmploye7);


                Table table = richServer.Document.Tables[3];
                for (int i = 0; i < EmployeeList.Count; i++)
                {
                    string AgreementEmploye = "";
                    try
                    {
                        AgreementEmploye = EmployeeList[i].PositionLink.Name + Environment.NewLine;
                    }
                    catch { }
                    try
                    {
                        AgreementEmploye += EmployeeList[i].BriefName;
                    }
                    catch { }

                    range = table.Rows[i + 1].Cells[1].ContentRange;
                    richServer.Document.Replace(range, AgreementEmploye);

                    if (EmployeeList.Count - i > 1)
                        table.Rows.InsertAfter(i + 1);
                }

                //table.Rows.InsertAfter()
            }
            catch { }

            string path = Path.GetTempPath() + @"\PermInfoList_" + perm.Oid.ToString() + "_" +
               DateTime.Now.Date.ToLongDateString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        private void FormListInfoUserPermReport(IObjectSpace os, UsePerm perm, string templatePath)
        {
            UnitOfWork unitOfWork = (UnitOfWork)perm.Session;

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            // лист согласований (информационный лист)
            try
            {
                string LetterInfo = "";
                try { LetterInfo = perm.LetterInfo; }
                catch { }
                Bookmark LetterInfoBook = richServer.Document.Bookmarks["LetterInfoBook"];
                richServer.Document.Replace(LetterInfoBook.Range, LetterInfo);
            }
            catch { }
            try
            {
                string ObjectName1 = "";
                try { ObjectName1 = perm.ObjectName; }
                catch { }
                Bookmark ObjectNameBook1 = richServer.Document.Bookmarks["ObjectNameBook1"];
                richServer.Document.Replace(ObjectNameBook1.Range, ObjectName1);
            }
            catch { }
            try
            {
                string PermInfo = "";
                try { PermInfo = perm.PermInfo; }
                catch { }
                Bookmark PermInfoBook1 = richServer.Document.Bookmarks["PermInfoBook1"];
                richServer.Document.Replace(PermInfoBook1.Range, PermInfo);
                Bookmark PermInfoBook2 = richServer.Document.Bookmarks["PermInfoBook2"];
                richServer.Document.Replace(PermInfoBook2.Range, PermInfo);
            }
            catch { }

            try
            {
                string PermInfoR = "";
                try { PermInfoR = perm.PermInfoR; }
                catch { }
                Bookmark PermInfoRBook1 = richServer.Document.Bookmarks["PermInfoRBook1"];
                richServer.Document.Replace(PermInfoRBook1.Range, PermInfoR);
                Bookmark PermInfoRBook2 = richServer.Document.Bookmarks["PermInfoRBook2"];
                richServer.Document.Replace(PermInfoRBook2.Range, PermInfoR);
            }
            catch { }

            try
            {
                string Decision1Name = "";
                string Decision1Position = "";
                try { Decision1Name = perm.Decision1.BriefName; }
                catch { }
                try
                {
                    if (perm.Decision1.PositionLink.NameFrom != null && perm.Decision1.PositionLink.NameFrom != "")
                        Decision1Position = perm.Decision1.PositionLink.NameFrom;
                    else
                        Decision1Position = perm.Decision1.PositionLink.Name;
                }
                catch { }
                if (Decision1Name != "")
                {
                    Bookmark Decision1NameBook1 = richServer.Document.Bookmarks["Decision1NameBook1"];
                    richServer.Document.Replace(Decision1NameBook1.Range, Decision1Name);
                    Bookmark Decision1NameBook2 = richServer.Document.Bookmarks["Decision1NameBook2"];
                    richServer.Document.Replace(Decision1NameBook2.Range, Decision1Name);
                }
                if (Decision1Position != "")
                {
                    Bookmark Decision1PositionBook1 = richServer.Document.Bookmarks["Decision1PositionBook1"];
                    richServer.Document.Replace(Decision1PositionBook1.Range, Decision1Position);
                }
            }
            catch { }

            try
            {
                string Decision2Name = "";
                string Decision2Position = "";
                try { Decision2Name = perm.Decision2.BriefName; }
                catch { }
                if (Decision2Name != "")
                {
                    Bookmark Decision2NameBook1 = richServer.Document.Bookmarks["Decision2NameBook1"];
                    richServer.Document.Replace(Decision2NameBook1.Range, Decision2Name);
                    Bookmark Decision2NameBook2 = richServer.Document.Bookmarks["Decision2NameBook2"];
                    richServer.Document.Replace(Decision2NameBook2.Range, Decision2Name);
                }
                try
                {
                    if (perm.Decision2.PositionLink.NameFrom != null && perm.Decision2.PositionLink.NameFrom != "")
                        Decision2Position = perm.Decision2.PositionLink.NameFrom;
                    else
                        Decision2Position = perm.Decision2.PositionLink.Name;
                }
                catch { }
                if (Decision2Position != "")
                {
                    Bookmark Decision2PositionBook1 = richServer.Document.Bookmarks["Decision2PositionBook1"];
                    richServer.Document.Replace(Decision2PositionBook1.Range, Decision2Position);
                }
            }
            catch { }

            try
            {
                List<Employee> EmployeeList = new List<Employee>();
                if (perm.AgreementEmploye1 != null)
                    EmployeeList.Add(perm.AgreementEmploye1);
                if (perm.AgreementEmploye2 != null)
                    EmployeeList.Add(perm.AgreementEmploye2);
                if (perm.AgreementEmploye3 != null)
                    EmployeeList.Add(perm.AgreementEmploye3);
                if (perm.AgreementEmploye4 != null)
                    EmployeeList.Add(perm.AgreementEmploye4);
                if (perm.AgreementEmploye5 != null)
                    EmployeeList.Add(perm.AgreementEmploye5);
                if (perm.AgreementEmploye6 != null)
                    EmployeeList.Add(perm.AgreementEmploye6);
                if (perm.AgreementEmploye7 != null)
                    EmployeeList.Add(perm.AgreementEmploye7);


                Table table = richServer.Document.Tables[3];
                for (int i = 0; i < EmployeeList.Count; i++)
                {
                    string AgreementEmploye = "";
                    try
                    {
                        AgreementEmploye = EmployeeList[i].PositionLink.Name + Environment.NewLine;
                    }
                    catch { }
                    try
                    {
                        AgreementEmploye += EmployeeList[i].BriefName;
                    }
                    catch { }

                    range = table.Rows[i + 1].Cells[1].ContentRange;
                    richServer.Document.Replace(range, AgreementEmploye);

                    if (EmployeeList.Count - i > 1)
                        table.Rows.InsertAfter(i + 1);
                }

                //table.Rows.InsertAfter()
            }
            catch { }

            string path = Path.GetTempPath() + @"\PermInfoList_" + perm.Oid.ToString() + "_" +
               DateTime.Now.Date.ToLongDateString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

    }
}
