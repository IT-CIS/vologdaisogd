﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class UsePermNoticeIZDLettersController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UsePermNoticeIZDLettersAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // UsePermNoticeIZDLettersAction
            // 
            this.UsePermNoticeIZDLettersAction.Caption = "Письма";
            this.UsePermNoticeIZDLettersAction.ConfirmationMessage = null;
            this.UsePermNoticeIZDLettersAction.Id = "UsePermNoticeIZDLettersAction";
            this.UsePermNoticeIZDLettersAction.ToolTip = "Формирование сопроводительных писем";
            this.UsePermNoticeIZDLettersAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UsePermNoticeIZDLettersAction_Execute);
            // 
            // UsePermNoticeIZDLettersController
            // 
            this.Actions.Add(this.UsePermNoticeIZDLettersAction);
            this.TargetObjectType = typeof(AISOGD.Perm.UsePermNoticeIZD);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction UsePermNoticeIZDLettersAction;
    }
}
