﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class SetDatesForPermReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.finishDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.formReportButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelReportButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.startDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishDateEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // startDateEdit
            // 
            this.startDateEdit.EditValue = null;
            this.startDateEdit.Location = new System.Drawing.Point(35, 41);
            this.startDateEdit.Name = "startDateEdit";
            this.startDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateEdit.Size = new System.Drawing.Size(136, 20);
            this.startDateEdit.TabIndex = 1;
            // 
            // finishDateEdit
            // 
            this.finishDateEdit.EditValue = null;
            this.finishDateEdit.Location = new System.Drawing.Point(238, 41);
            this.finishDateEdit.Name = "finishDateEdit";
            this.finishDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.finishDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.finishDateEdit.Size = new System.Drawing.Size(136, 20);
            this.finishDateEdit.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(35, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(122, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Начальная дата отчета";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(238, 22);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(116, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Конечная дата отчета";
            // 
            // formReportButton
            // 
            this.formReportButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.formReportButton.Location = new System.Drawing.Point(35, 79);
            this.formReportButton.Name = "formReportButton";
            this.formReportButton.Size = new System.Drawing.Size(136, 23);
            this.formReportButton.TabIndex = 4;
            this.formReportButton.Text = "Сформировать отчет";
            // 
            // cancelReportButton
            // 
            this.cancelReportButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelReportButton.Location = new System.Drawing.Point(238, 79);
            this.cancelReportButton.Name = "cancelReportButton";
            this.cancelReportButton.Size = new System.Drawing.Size(136, 23);
            this.cancelReportButton.TabIndex = 4;
            this.cancelReportButton.Text = "Отмена";
            // 
            // SetDatesForPermReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 121);
            this.Controls.Add(this.cancelReportButton);
            this.Controls.Add(this.formReportButton);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.finishDateEdit);
            this.Controls.Add(this.startDateEdit);
            this.Name = "SetDatesForPermReportsForm";
            this.Text = "Укажите даты формируемого отчета";
            ((System.ComponentModel.ISupportInitialize)(this.startDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishDateEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton formReportButton;
        private DevExpress.XtraEditors.SimpleButton cancelReportButton;
        public DevExpress.XtraEditors.DateEdit startDateEdit;
        public DevExpress.XtraEditors.DateEdit finishDateEdit;
    }
}