﻿using System;
using System.Collections.Generic;
using System.Linq;
using AISOGD.Constr;
using AISOGD.DocFlow;
using AISOGD.General;
using AISOGD.Perm;
using AISOGD.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Win;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraEditors;
using VologdaIsogd.Module.Controllers.Delo;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ChangeStatusesUseNoticeIZDPermController : ViewController
    {

        private ChoiceActionItem noticeIZDPermAcceptAction;
        private ChoiceActionItem noticeIZDPermRefuseAction;

        public ChangeStatusesUseNoticeIZDPermController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            ChangeStatusesUseNoticeIZDPermChoiceAction.Items.Clear();
            noticeIZDPermAcceptAction =
               new ChoiceActionItem("Утвердить", null);
            ChangeStatusesUseNoticeIZDPermChoiceAction.Items.Add(noticeIZDPermAcceptAction);

            noticeIZDPermRefuseAction =
               new ChoiceActionItem("Отказ", null);
            ChangeStatusesUseNoticeIZDPermChoiceAction.Items.Add(noticeIZDPermRefuseAction);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ChangeStatusesUseNoticeIZDPermChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            UsePermNoticeIZD perm;
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            perm = os.FindObject<UsePermNoticeIZD>(new BinaryOperator("Oid", x_id));

            if (perm != null)
            {
                if (e.SelectedChoiceActionItem == noticeIZDPermAcceptAction)
                {
                    UseNoticeIZDPermAcceptAction(os, connect, perm);
                }
                if (e.SelectedChoiceActionItem == noticeIZDPermRefuseAction)
                {
                    UseNoticeIZDPermRefuseAction(os, connect, perm);
                }
            }
        }

        /// <summary>
        /// Утверждение уведомления
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void UseNoticeIZDPermAcceptAction(IObjectSpace os, Connect connect, UsePermNoticeIZD perm)
        {
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            SynhronizeNoticeIZDDataController SynhronizeNoticeIZDDataController = new SynhronizeNoticeIZDDataController();
            if (perm.EDocStatus == AISOGD.Enums.eDocStatus.Отказ || perm.EDocStatus == AISOGD.Enums.eDocStatus.ОжиданиеУтвержденияВзамен ||
                perm.EDocStatus == AISOGD.Enums.eDocStatus.ПрекращениеДелопроизводства || perm.EDocStatus == AISOGD.Enums.eDocStatus.УтратилоДействие)
            {
                XtraMessageBox.Show("Документ с данным статусом нельзя утвердить!");
                return;
            }
            if (perm.DocDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Не указана дата документа");
                return;
            }
            if (String.IsNullOrWhiteSpace(perm.DocNo))
            {
                XtraMessageBox.Show("Необходимо указать корректный номер документа");
                return;
            }
            UsePermNoticeIZD anotherPerm = connect.FindFirstObject<UsePermNoticeIZD>(x => x.DocNo == perm.DocNo && x.DocDate == perm.DocDate);
            if (perm.Oid.ToString() != anotherPerm.Oid.ToString())
            {
                XtraMessageBox.Show("В Системе уже внесен другой документ с подобными реквизитам.");
                return;
            }

            perm.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден;

            try
            {
                perm.FullDocNo = perm.DocKind.Prefix + perm.DocNo + perm.DocKind.Suffix;
            }
            catch { }
            foreach (CapitalStructureBase constrObj in perm.CapitalStructureBase)
            {
                constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.Введено;
                constrObj.DocInUseDate = perm.DocDate;
                try { constrObj.ConstrCountDays = ((perm.DocDate - constrObj.StartConstrDate).Days + 1).ToString(); }
                catch { }

                try
                {
                    constrObj.FloorCountFakt = Convert.ToInt16(perm.FloorCount);
                }
                catch { }
                try
                {
                    constrObj.HeightFakt = Convert.ToDouble(perm.Height);
                }
                catch { }
                try
                {
                    constrObj.BuildSquareFakt = Convert.ToDouble(perm.BuildSquare);
                }
                catch { }
                try
                {
                    string usePermInfo = $"Уведомление на ввод от {perm.DocDate.ToShortDateString()} №{perm.DocNo}";
                    if (String.IsNullOrWhiteSpace(constrObj.UsePermInfo))
                        constrObj.UsePermInfo = usePermInfo;
                    else
                        if (!constrObj.UsePermInfo.Contains(usePermInfo))
                        constrObj.UsePermInfo += ", " + usePermInfo;
                }
                catch { }


                // заполняем семантику
                // из таблицы связи получаем Ид реестрового объекта ЗУ
                SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.CapitalStructureBase", constrObj.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                if (objLink != null)
                {
                    try
                    {
                        m_MapInfo.Do("Update Selection Set СТАТУС =  \"" + constrObj.ConstrStageState + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                    try
                    {
                        m_MapInfo.Do("Update Selection Set УВЕДОМЛЕНИЕ_ВВОД =  \"" + constrObj.UsePermInfo + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                    try
                    {
                        m_MapInfo.Do("Update Selection Set ГОД_ВВОДА =  " + perm.DocDate.Year + " Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                }

                constrObj.Save();

                try
                {
                    ConstrStage constrStage = constrObj.ConstrStages[0];
                    constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.Введено;
                    
                    constrStage.UsePermInfo = constrObj.UsePermInfo?? "";

                    constrStage.Save();
                }
                catch { }
            }
            foreach (InLetter letter in perm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }

            perm.Save();
            os.CommitChanges();

            // создаем документ для регистрации в ИСОГД 
            try
            {
                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = perm.FullDocNo; }
                catch { }

                try { idocDate = perm.DocDate; }
                catch { }

                try { icadNo = perm.LotCadNo; }
                catch { }

                try { iaddrInfo = perm.ObjectAddress; }
                catch { }

                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in perm.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }

                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "061101", 
                    "уведомление о соотв. постр-х или рек-х объекта ИЖС или сад. дома треб. закон-ва о град. деят-и", 
                    icadNo, iaddrInfo, attachs);
            }
            catch { }
            RefreshAllWindows();
        }

        /// <summary>
        /// Отказ по уведомлению
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UseNoticeIZDPermRefuseAction(IObjectSpace os, Connect connect, UsePermNoticeIZD perm)
        {
            if (perm.EDocStatus != AISOGD.Enums.eDocStatus.Подготовка)
            {
                XtraMessageBox.Show("Можно отменить только документ со статусом 'Подготовка'!");
                return;
            }
            perm.EDocStatus = AISOGD.Enums.eDocStatus.Отказ;
            
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            foreach (CapitalStructureBase oks in perm.CapitalStructureBase)
            {
                oks.ConstrStageState = AISOGD.Enums.eConstrStageState.Отмена;
                oks.Save();
                try
                {
                    SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.CapitalStructureBase", oks.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                    m_MapInfo.Do("Select * From " + objLink.SpatialLayerId + " Where MI_PRINX = " + objLink.SpatialObjectId);
                    //string mapNo = "";
                    //if (oks.MapNo != null)
                    //    mapNo = oks.MapNo;
                    //m_MapInfo.Do("Update Selection Set НОМЕР_НА_СХЕМЕ =  \"" + mapNo + "_отказ" + "\" Commit table " + objLink.SpatialLayerId);
                    try
                    {
                        m_MapInfo.Do("Update Selection Set СТАТУС =  \"" + oks.ConstrStageState + "\" Commit table " + objLink.SpatialLayerId);
                    }
                    catch { }
                }
                catch { }
                try
                {
                    ConstrStage constrStage = oks.ConstrStages[0];
                    constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.Отмена;
                    constrStage.Save();
                }
                catch { }
            }

            foreach (InLetter letter in perm.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            perm.Save();
            // создаем документ для регистрации в ИСОГД 
            try
            {
                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = perm.FullDocNo; }
                catch { }

                try { idocDate = perm.DocDate; }
                catch { }

                try { icadNo = perm.LotCadNo; }
                catch { }

                try { iaddrInfo = perm.ObjectAddress; }
                catch { }

                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in perm.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }

                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "061101",
                    "уведомление о НЕсоотв. постр-х или рек-х объекта ИЖС или сад. дома треб. закон-ва о град. деят-и",
                    icadNo, iaddrInfo, attachs);
            }
            catch { }
            os.CommitChanges();
            RefreshAllWindows();
        }

        // обновить все окна
        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }
    }
}
