﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using AISOGD.Perm;
using AISOGD.SystemDir;
using AISOGD.Constr;
using AISOGD.Land;
using DevExpress.ExpressApp.Win;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Subject;
using AISOGD.OrgStructure;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SynhronizePermDataController : ViewController
    {
        public SynhronizePermDataController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        // обновить все окна
        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }

        /// <summary>
        /// синхронизировать данные по разрешению на строительство:
        /// заполняем данные в разрешении на строительство из этапов
        /// заполняем семантику у этапов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SynhronizePermDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();


            ConstrPerm constrPerm;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            constrPerm = os.FindObject<ConstrPerm>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)constrPerm.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);


            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            
            //foreach (ConstrStage stagePerm in constrPerm.ConstrStages)
            //{
            //    foreach (ConstrStage stageObj in obj.ConstrStages)
            //    {

                //    }
                //}

                // Иные показатели заполняются так:
                //                        "Количество квартир -        шт. ;
                //                        Общая площадь квартир(с учетом балконов, лоджий, тамбуров) - кв.м;
                //                        площадь квартир без учета (без учета  балконов, лоджий, тамбуров) -кв.м;
                //                        площадь помещений 1 этажа - кв.м;
                //                        площадь помещений подвального этажа -кв.м


            List<OtherIndicatorTemp> OtherIndicatorTempList = new List<OtherIndicatorTemp>();
            string[] OtherIndicatorsNames = { "количество квартир", "жилая площадь квартир", "площадь квартир с балконами", "площадь квартир без балконов" };
            int i = 0;
            string newLine = Environment.NewLine;
            string OtherIndicators = "";

            constrPerm.TotalBuildSquareProject = "";
            constrPerm.BuildingSizeProject = "";
            constrPerm.BuildingSizeUnderGroundPartProject = "";
            constrPerm.FloorCount = "";
            constrPerm.UnderGroundFloorCount = "";
            constrPerm.Height = "";
            constrPerm.Capacity = "";
            constrPerm.BuildSquare = "";
            //constrPerm.OKSOtherIndicators = "";
            constrPerm.LotSquare = "";
            constrPerm.TotalLivingSpaceProject = "";

            constrPerm.LineObjectClass = null;
            constrPerm.LengthProject = "";
            constrPerm.PowerLineProject = "";
            constrPerm.ElectricLinesInfoProject = "";
            constrPerm.ConstructiveElementsInfoProject = "";
            //constrPerm.OtherIndicatorsLineProject = "";

            // проверяем сначала, все ли этапы РС совпадают с этапами Объекта
            // если да, то копируем наименование, Адрес и иные показатели из Объекта
            // если нет, то формируем их из этапов
            bool isAllStagesCoinside = true;
            CapitalStructureBase obj = constrPerm.CapitalStructureBase[0];
            try
            {
                constrPerm.ConstructionType = obj.ConstructionType;
            }
            catch { }
            try
            {
                constrPerm.ConstructionCategory = obj.ConstructionCategory;
            }
            catch { }
            int stageObjCount = connect.FindObjects<ConstrStage>(x => x.CapitalStructureBase == obj && (x.ConstrStageState != AISOGD.Enums.eConstrStageState.Отмена &&
          x.ConstrStageState != AISOGD.Enums.eConstrStageState.НеИзвестно)).Count();

            if (constrPerm.ConstrStages.Count() != stageObjCount)
                isAllStagesCoinside = false;

            if (isAllStagesCoinside)
            {
                if (constrPerm.ObjectName == null || constrPerm.ObjectName == "")
                    constrPerm.ObjectName = obj.ProjectName;
                if (constrPerm.ObjectAddress == null || constrPerm.ObjectAddress == "")
                    constrPerm.ObjectAddress = obj.Address;
                if (constrPerm.OKSOtherIndicators == null || constrPerm.OKSOtherIndicators == "")
                    if(obj.OtherIndicatorsProject != null && obj.OtherIndicatorsProject != "")
                        constrPerm.OKSOtherIndicators = obj.OtherIndicatorsProject;
            }
            if(obj.CapacityProject != 0)
                constrPerm.Capacity = obj.CapacityProject.ToString();
            if (constrPerm.ConstrStages.Count == 1)
            {
                ConstrStage constrStage = constrPerm.ConstrStages[0];
                //if (constrStage.ConstrStageState == AISOGD.Enums.eConstrStageState.ПодготовкаРС)
                //{
                if (!isAllStagesCoinside)
                {
                    if (constrPerm.ObjectName == null || constrPerm.ObjectName == "")
                    {
                        constrPerm.ObjectName = constrStage.Name;
                    }
                    if (constrPerm.ObjectAddress == null || constrPerm.ObjectAddress == "")
                    {
                        constrPerm.ObjectAddress = constrStage.Address;
                    }
                }
                if (constrStage.BuildingKind != AISOGD.Enums.eBuildingKind.Линейные)
                {
                    try
                    {
                        if (constrStage.TotalBuildSquareProject != 0)
                            constrPerm.TotalBuildSquareProject = constrStage.TotalBuildSquareProject.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.BuildingSizeProject != 0)
                            constrPerm.BuildingSizeProject = constrStage.BuildingSizeProject.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.BuildingSizeUnderGroundPartProject != 0)
                            constrPerm.BuildingSizeUnderGroundPartProject = constrStage.BuildingSizeUnderGroundPartProject.ToString();
                    }
                    catch { }
                    try
                    {
                        string FloorCount = "";
                        string FloorNote = "";
                        if (constrStage.FloorCount != 0)
                        {
                            FloorCount = constrStage.FloorCount.ToString();
                            if (constrStage.FloorsNote != null && constrStage.FloorsNote != "")
                            {
                                //FloorNote = constrStage.FloorsNote + Environment.NewLine;
                                //if (FloorCount != "")
                                FloorNote = Environment.NewLine + constrStage.FloorsNote;
                            }
                        }
                        constrPerm.FloorCount += String.Format("{0}{1}", FloorCount, FloorNote);
                    }
                    catch { }
                    try
                    {
                        if (constrStage.UnderGroundFloorCount != 0)
                            constrPerm.UnderGroundFloorCount = constrStage.UnderGroundFloorCount.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.Height != 0)
                        {
                            constrPerm.Height = constrStage.Height.ToString();
                            if (constrStage.HeightNote != null && constrStage.HeightNote != "")
                                constrPerm.Height += String.Format(" ({0})", constrStage.HeightNote);
                        }
                    }
                    catch { }
                    //try
                    //{
                    //    if (constrStage.CapacityProject != 0)
                    //        constrPerm.Capacity = constrStage.CapacityProject.ToString();
                    //}
                    //catch { }
                    try
                    {
                        if (constrStage.BuildSquare != 0)
                            constrPerm.BuildSquare = constrStage.BuildSquare.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.TotalLivingSpaceProject != 0)
                            constrPerm.TotalLivingSpaceProject = constrStage.TotalLivingSpaceProject.ToString();
                    }
                    catch { }
                    constrPerm.Save();

                    try
                    {
                        //ObjectConstractionInfo constrInfo = null;
                        //constrInfo = constrStage.ObjectConstractionInfo[0];
                        //string OtherIndicatorName = "";
                        bool isExist = false;

                        // кол-во квартир
                        //OtherIndicatorName = "количество квартир";
                        //string[] OtherIndicatorsNames = { "количество квартир", "площадь квартир с балконами", "площадь квартир без балконов" };
                        //                        "Количество квартир -        шт. ;
                        //                        жилая площадь квартир - кв.м;
                        //                        Общая площадь квартир(с учетом балконов, лоджий, тамбуров) - кв.м;
                        //                        площадь квартир без учета (без учета  балконов, лоджий, тамбуров) -кв.м;
                        //                        площадь помещений 1 этажа - кв.м;
                        //                        площадь помещений подвального этажа -кв.м
                        char[] seps = { '\\', '/' };

                        foreach (string OtherIndicatorName in OtherIndicatorsNames)
                        {
                            string oValue = "";
                            try
                            {
                                if (OtherIndicatorName == "количество квартир")
                                    oValue = constrStage.AppartmentsCountProject.ToString();
                                if (OtherIndicatorName == "жилая площадь квартир")
                                    oValue = constrStage.AppartmentsSpaceProject.ToString();
                                if (OtherIndicatorName == "площадь квартир с балконами")
                                    oValue = constrStage.TotalLivingSpaceProject.ToString();
                                if (OtherIndicatorName == "площадь квартир без балконов")
                                    oValue = constrStage.LivingSpaceProject.ToString();

                                foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                                {
                                    if (t.IndicatorType == OtherIndicatorName)
                                    {
                                        StageValue stageValue = new StageValue();
                                        stageValue.StageName = constrStage.StageNo;
                                        stageValue.Value = oValue;
                                        t.StageValues.Add(stageValue);
                                        isExist = true;
                                        break;
                                    }
                                }
                                if (!isExist)
                                {
                                    OtherIndicatorTemp t = new OtherIndicatorTemp();
                                    t.IndicatorType = OtherIndicatorName;
                                    StageValue stageValue = new StageValue();
                                    stageValue.StageName = constrStage.StageNo;
                                    stageValue.Value = oValue;
                                    t.StageValues.Add(stageValue);
                                    OtherIndicatorTempList.Add(t);
                                }
                                isExist = false;
                            }
                            catch { }
                        }

                    }
                    catch { }
                }
                #region // Линейный объект 
                if (constrStage.BuildingKind == AISOGD.Enums.eBuildingKind.Линейные)
                {
                    try
                    {
                        if (constrStage.LineObjectClass != null)
                            constrPerm.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid.ToString() == constrStage.LineObjectClass.Oid.ToString());
                    }
                    catch { }
                    try
                    {
                        if (constrStage.LengthProject != 0)
                            constrPerm.LengthProject = constrStage.LengthProject.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.PowerLineProject != null)
                            constrPerm.PowerLineProject = constrStage.PowerLineProject.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.ElectricLinesInfoProject != null)
                            constrPerm.ElectricLinesInfoProject = constrStage.ElectricLinesInfoProject.ToString();
                    }
                    catch { }
                    try
                    {
                        if (constrStage.ConstructiveElementsInfoProject != null)
                            constrPerm.ConstructiveElementsInfoProject = constrStage.ConstructiveElementsInfoProject.ToString();
                    }
                    catch { }
                    //try
                    //{
                    //    constrPerm.OtherIndicatorsLineProject = constrStage.OtherIndicatorsProject;
                    //}
                    //catch { }
                }
                #endregion
                #region ///
                //unitOfWork.CommitChanges();
                //if (constrStage.BuildingKind != AISOGD.Enums.eBuildingKind.Линейные)
                //{
                //    // заполняем данные по иным показателям - берем их из проектных и фактических показателях для каждого этапа, потом аккамулирем (ниже) для вставки в РС
                //    try
                //    {
                //        //ObjectConstractionInfo constrInfo = null;
                //        //constrInfo = constrStage.ObjectConstractionInfo[0];
                //        //string OtherIndicatorName = "";
                //        bool isExist = false;

                //        // кол-во квартир
                //        //OtherIndicatorName = "количество квартир";
                //        //string[] OtherIndicatorsNames = { "количество квартир", "площадь квартир с балконами", "площадь квартир без балконов" };
                //        //                        "Количество квартир -        шт. ;
                //        //                        Общая площадь квартир(с учетом балконов, лоджий, тамбуров) - кв.м;
                //        //                        площадь квартир без учета (без учета  балконов, лоджий, тамбуров) -кв.м;
                //        //                        площадь помещений 1 этажа - кв.м;
                //        //                        площадь помещений подвального этажа -кв.м
                //        char[] seps = { '\\', '/' };

                //        foreach (string OtherIndicatorName in OtherIndicatorsNames)
                //        {
                //            string oValue = "";
                //            try
                //            {
                //                if (OtherIndicatorName == "количество квартир")
                //                    oValue = constrStage.AppartmentsCountProject.ToString();
                //                if (OtherIndicatorName == "площадь квартир с балконами")
                //                    oValue = constrStage.TotalLivingSpaceProject.ToString();
                //                if (OtherIndicatorName == "площадь квартир без балконов")
                //                    oValue = constrStage.LivingSpaceProject.ToString();

                //                foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                //                {
                //                    if (t.IndicatorType == OtherIndicatorName)
                //                    {
                //                        StageValue stageValue = new StageValue();
                //                        stageValue.StageName = constrStage.StageNo;
                //                        stageValue.Value = oValue;
                //                        t.StageValues.Add(stageValue);
                //                        isExist = true;
                //                        break;
                //                    }
                //                }
                //                if (!isExist)
                //                {
                //                    OtherIndicatorTemp t = new OtherIndicatorTemp();
                //                    t.IndicatorType = OtherIndicatorName;
                //                    StageValue stageValue = new StageValue();
                //                    stageValue.StageName = constrStage.StageNo;
                //                    stageValue.Value = oValue;
                //                    t.StageValues.Add(stageValue);
                //                    OtherIndicatorTempList.Add(t);
                //                }
                //                isExist = false;
                //            }
                //            catch { }
                //        }

                //    }
                //    catch { }
                //}
                #endregion

                constrPerm.Save();

                // заполняем семантику
                // из таблицы связи получаем Ид реестрового объекта ЗУ
                SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", constrStage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                if (objLink != null)
                {
                    try
                    {
                        SetConstrStageSemantic(m_MapInfo, constrStage, constrPerm, objLink.SpatialLayerId, objLink.SpatialObjectId);
                    }
                    catch { }
                }
                //}
            }
            else
            {
                SortProperty sort = new SortProperty("StageNo", DevExpress.Xpo.DB.SortingDirection.Ascending);
                constrPerm.ConstrStages.Sorting.Add(sort);

                if (!isAllStagesCoinside)
                {
                    if (constrPerm.ObjectName == null || constrPerm.ObjectName == "")
                    {
                        constrPerm.ObjectName = obj.Name;
                        foreach (ConstrStage constrStage in constrPerm.ConstrStages)
                        {
                            try { constrPerm.ObjectName += Environment.NewLine +  String.Format("{0} - {1};", constrStage.StageNo, constrStage.Name); }
                            catch { }
                        }
                    }
                    if (constrPerm.ObjectAddress == null || constrPerm.ObjectAddress == "")
                    {
                        foreach (ConstrStage constrStage in constrPerm.ConstrStages)
                        {
                            try { constrPerm.ObjectAddress += String.Format("{0} - {1};", constrStage.StageNo, constrStage.Address) + Environment.NewLine; }
                            catch { }
                        }
                        constrPerm.ObjectAddress = constrPerm.ObjectAddress.TrimEnd(Environment.NewLine.ToCharArray());
                    }

                }
                double TotalBuildSquareProject = 0;
                double BuildingSizeProject = 0;
                double BuildingSizeUnderGroundPartProject = 0;
                double TotalLivingSpaceProject = 0;
                double BuildSquare = 0;

                foreach (ConstrStage constrStage in constrPerm.ConstrStages)
                {
                    i++;
                    if (i == constrPerm.ConstrStages.Count)
                        newLine = "";
                    //try { constrPerm.ObjectName += String.Format("{0} - {1};", constrStage.StageNo, constrStage.Name) + newLine; }
                    //catch { }
                    if (constrStage.BuildingKind != AISOGD.Enums.eBuildingKind.Линейные)
                    {

                        try
                        {
                            TotalBuildSquareProject += constrStage.TotalBuildSquareProject;
                            if (constrStage.TotalBuildSquareProject != 0)
                                constrPerm.TotalBuildSquareProject += String.Format("{0} - {1}", constrStage.StageNo, constrStage.TotalBuildSquareProject.ToString()) + newLine;
                            
                        }
                        catch { }
                        try
                        {
                            BuildingSizeProject += constrStage.BuildingSizeProject;
                            if (constrStage.BuildingSizeProject != 0)
                                constrPerm.BuildingSizeProject += String.Format("{0} - {1}", constrStage.StageNo, constrStage.BuildingSizeProject.ToString()) + newLine;
                        }
                        catch { }
                        try
                        {
                            BuildingSizeUnderGroundPartProject += constrStage.BuildingSizeUnderGroundPartProject;
                            if (constrStage.BuildingSizeUnderGroundPartProject != 0)
                                constrPerm.BuildingSizeUnderGroundPartProject += String.Format("{0} - {1}", constrStage.StageNo, constrStage.BuildingSizeUnderGroundPartProject.ToString()) + newLine;
                        }
                        catch { }
                        try
                        {
                            TotalLivingSpaceProject += constrStage.TotalLivingSpaceProject;
                            if (constrStage.TotalLivingSpaceProject != 0)
                                constrPerm.TotalLivingSpaceProject += String.Format("{0} - {1}", constrStage.StageNo, constrStage.TotalLivingSpaceProject.ToString()) + newLine;
                        }
                        catch { }
                        try
                        {
                            string FloorCount = "";
                            string FloorNote = "";
                            if (constrStage.FloorCount != 0)
                            {
                                FloorCount = constrStage.FloorCount.ToString();
                                if (constrStage.FloorsNote != null && constrStage.FloorsNote != "")
                                {
                                    //FloorNote = constrStage.FloorsNote + Environment.NewLine;
                                    //if (FloorCount != "")
                                        FloorNote = Environment.NewLine + constrStage.FloorsNote;
                                }
                                constrPerm.FloorCount += String.Format("{2} - {0}{1}", FloorCount, FloorNote, constrStage.StageNo) + newLine;
                            }
                        }
                        catch { }
                        try
                        {
                            if (constrStage.UnderGroundFloorCount != 0)
                                constrPerm.UnderGroundFloorCount += String.Format("{0} - {1}", constrStage.StageNo, constrStage.UnderGroundFloorCount.ToString()) + newLine;
                        }
                        catch { }
                        try
                        {
                            //if (constrStage.Height != 0)
                            //    constrPerm.Height += String.Format("{0} - {1}", constrStage.StageNo, constrStage.Height.ToString()) + newLine;

                            if (constrStage.Height != 0)
                            {
                                constrPerm.Height += String.Format("{0} - {1}", constrStage.StageNo, constrStage.Height.ToString());
                                if (constrStage.HeightNote != null && constrStage.HeightNote != "")
                                    constrPerm.Height += String.Format(" ({0})", constrStage.HeightNote);
                                constrPerm.Height += newLine;
                            }
                        }
                        catch { }
                        //try
                        //{
                        //    if (constrStage.CapacityProject != 0)
                        //        constrPerm.Capacity += String.Format("{0} - {1}", constrStage.StageNo, constrStage.CapacityProject.ToString()) + newLine;
                        //}
                        //catch { }
                        try
                        {
                            BuildSquare += constrStage.BuildSquare;
                            if (constrStage.BuildSquare != 0)
                                constrPerm.BuildSquare += String.Format("{0} - {1}", constrStage.StageNo, constrStage.BuildSquare.ToString()) + newLine;
                        }
                        catch { }
                        constrPerm.Save();
                        //unitOfWork.CommitChanges();

                        // заполняем данные по иным показателям - берем их из проектных показателях для каждого этапа, потом аккамулирем (ниже) для вставки в РС

                        try
                        {
                            //ObjectConstractionInfo constrInfo = null;
                            //constrInfo = constrStage.ObjectConstractionInfo[0];
                            //string OtherIndicatorName = "";
                            bool isExist = false;

                            // кол-во квартир
                            //OtherIndicatorName = "количество квартир";
                            //string[] OtherIndicatorsNames = { "количество квартир", "общая площадь квартир", "жилая площадь квартир" };
                            char[] seps = { '\\', '/' };

                            foreach (string OtherIndicatorName in OtherIndicatorsNames)
                            {
                                string oValue = "";
                                try
                                {
                                    if (OtherIndicatorName == "количество квартир")
                                        oValue = constrStage.AppartmentsCountProject.ToString();
                                    if (OtherIndicatorName == "жилая площадь квартир")
                                        oValue = constrStage.AppartmentsSpaceProject.ToString();
                                    if (OtherIndicatorName == "площадь квартир с балконами")
                                        oValue = constrStage.TotalLivingSpaceProject.ToString();
                                    if (OtherIndicatorName == "площадь квартир без балконов")
                                        oValue = constrStage.LivingSpaceProject.ToString();

                                    foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                                    {
                                        if (t.IndicatorType == OtherIndicatorName)
                                        {
                                            StageValue stageValue = new StageValue();
                                            stageValue.StageName = constrStage.StageNo;
                                            stageValue.Value = oValue;
                                            t.StageValues.Add(stageValue);
                                            isExist = true;
                                            break;
                                        }
                                    }
                                    if (!isExist)
                                    {
                                        OtherIndicatorTemp t = new OtherIndicatorTemp();
                                        t.IndicatorType = OtherIndicatorName;
                                        StageValue stageValue = new StageValue();
                                        stageValue.StageName = constrStage.StageNo;
                                        stageValue.Value = oValue;
                                        t.StageValues.Add(stageValue);
                                        OtherIndicatorTempList.Add(t);
                                    }
                                    isExist = false;
                                }
                                catch { }
                            }

                        }
                        catch { }
                    }
                    else
                    {
                        try
                        {
                            if (constrStage.LineObjectClass == null)
                                constrPerm.LineObjectClass = connect.FindFirstObject<LineObjectClass>(x => x.Oid.ToString() == constrStage.LineObjectClass.Oid.ToString());
                        }
                        catch { }
                        try
                        {
                            if (constrStage.LengthProject != 0)
                                constrPerm.LengthProject += String.Format("{0} - {1};", constrStage.StageNo, constrStage.LengthProject.ToString()) + newLine; //constrStage.LengthProject.ToString();
                        }
                        catch { }
                        try
                        {
                            if (constrStage.PowerLineProject != null)
                                constrPerm.PowerLineProject += String.Format("{0} - {1};", constrStage.StageNo, constrStage.PowerLineProject) + newLine;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.ElectricLinesInfoProject != null)
                                constrPerm.ElectricLinesInfoProject += String.Format("{0} - {1};", constrStage.StageNo, constrStage.ElectricLinesInfoProject) + newLine;
                        }
                        catch { }
                        try
                        {
                            if (constrStage.ConstructiveElementsInfoProject != null)
                                constrPerm.ConstructiveElementsInfoProject += String.Format("{0} - {1};", constrStage.StageNo, constrStage.ConstructiveElementsInfoProject) + newLine;
                        }
                        catch { }
                        //try
                        //{
                        //    constrPerm.OtherIndicatorsLineProject += String.Format("{0} - {1};", constrStage.StageNo, constrStage.OtherIndicatorsProject) + newLine;
                        //}
                        //catch { }
                    }

                    #region Раньше считалось по листу иных показателей у стадии
                    //foreach (OtherIndicators OtherIndicator in constrStage.OtherIndicators)
                    //{
                    //    try
                    //    {
                    //        bool isExist = false;
                    //        if (OtherIndicator.OtherIndicatorType != null)
                    //        {
                    //            if (OtherIndicator.OtherIndicatorType.Name != null && OtherIndicator.OtherIndicatorType.Name != "")
                    //            {
                    //                foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                    //                {
                    //                    if (t.IndicatorType == OtherIndicator.OtherIndicatorType.Name)
                    //                    {
                    //                        string v = "";
                    //                        if (OtherIndicator.OtherIndicatorValue != null && OtherIndicator.OtherIndicatorValue != "")
                    //                            v = OtherIndicator.OtherIndicatorValue;
                    //                        if (v != "")
                    //                            if (OtherIndicator.Unit != null)
                    //                                if (OtherIndicator.Unit.Name != null && OtherIndicator.Unit.Name != "")
                    //                                    v += " " + OtherIndicator.Unit.Name;
                    //                        if (v != "")
                    //                            t.Values.Add(String.Format("{0} - {1}", constrStage.StageNo, v));
                    //                        isExist = true;
                    //                        break;
                    //                    }
                    //                }
                    //                if (!isExist)
                    //                {
                    //                    OtherIndicatorTemp OtherIndicatorTemp = new OtherIndicatorTemp();
                    //                OtherIndicatorTemp.Values = new List<string>();
                    //                    OtherIndicatorTemp.IndicatorType = OtherIndicator.OtherIndicatorType.Name;
                    //                    string v = "";
                    //                    if (OtherIndicator.OtherIndicatorValue != null && OtherIndicator.OtherIndicatorValue != "")
                    //                        v = OtherIndicator.OtherIndicatorValue;
                    //                    if (v != "")
                    //                        if (OtherIndicator.Unit != null)
                    //                            if (OtherIndicator.Unit.Name != null && OtherIndicator.Unit.Name != "")
                    //                                v += " " + OtherIndicator.Unit.Name;
                    //                    if (v != "")
                    //                    {
                    //                        OtherIndicatorTemp.Values.Add(String.Format("{0} - {1}", constrStage.StageNo, v));
                    //                        OtherIndicatorTempList.Add(OtherIndicatorTemp);
                    //                    }
                    //                }

                    //            }
                    //        }
                    //    }
                    //    catch { }
                    //}
                    #endregion
                    //unitOfWork.CommitChanges();
                    constrPerm.Save();

                    // заполняем семантику
                    // из таблицы связи получаем Ид реестрового объекта ЗУ
                    SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.Constr.ConstrStage", constrStage.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                    if (objLink != null)
                    {
                        try
                        {
                            SetConstrStageSemantic(m_MapInfo, constrStage, constrPerm, objLink.SpatialLayerId, objLink.SpatialObjectId);
                        }
                        catch { }
                    }
                    //}
                }
                if (TotalBuildSquareProject != 0)
                    constrPerm.TotalBuildSquareProject += Environment.NewLine + String.Format("(всего {0})", TotalBuildSquareProject.ToString());
                if (BuildingSizeProject != 0)
                    constrPerm.BuildingSizeProject += Environment.NewLine + String.Format("(всего {0})", BuildingSizeProject.ToString());
                if (BuildingSizeUnderGroundPartProject != 0)
                    constrPerm.BuildingSizeUnderGroundPartProject += Environment.NewLine + String.Format("(всего {0})", BuildingSizeUnderGroundPartProject.ToString());
                if (TotalLivingSpaceProject != 0)
                    constrPerm.TotalLivingSpaceProject += Environment.NewLine + String.Format("(всего {0})", TotalLivingSpaceProject.ToString());
                if (BuildSquare != 0)
                    constrPerm.BuildSquare += Environment.NewLine + String.Format("(всего {0})", BuildSquare.ToString());
            }
            //foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
            //{
            //    OtherIndicators += t.IndicatorType + ":";
            //    foreach(string v in t.Values)
            //    { OtherIndicators += v + ", "; }
            //    OtherIndicators = OtherIndicators.Trim();
            //    OtherIndicators = OtherIndicators.TrimEnd(',');
            //    OtherIndicators += ";" + Environment.NewLine;
            //}
            if (constrPerm.BuildingCategory != AISOGD.Enums.eBuildingCategory.ЛинейныйОбъект)
            {
                

                foreach (OtherIndicatorTemp t in OtherIndicatorTempList)
                {
                    string unit = "";
                    if (t.IndicatorType == "количество квартир")
                        unit = " шт.";
                    else
                        unit = " кв.м";
                    OtherIndicators += t.IndicatorType + " - ";
                    double totalvalue = 0;
                    foreach (StageValue v in t.StageValues)
                    {
                        try
                        {
                            totalvalue += Convert.ToDouble(v.Value);
                        }
                        catch { }
                    }
                    if (constrPerm.ConstrStages.Count == 1)
                    {
                        OtherIndicators += totalvalue + unit + ";" + Environment.NewLine;
                    }
                    else
                    {
                        OtherIndicators += totalvalue + unit + ":" + Environment.NewLine;
                        foreach (StageValue v in t.StageValues)
                        {
                            try
                            {
                                OtherIndicators += v.StageName + " - " + v.Value + unit + Environment.NewLine;
                            }
                            catch { }
                        }
                    }
                }

                //OtherIndicators = OtherIndicators.Trim();
                //OtherIndicators = OtherIndicators.TrimEnd(',');
                //OtherIndicators += ";" + Environment.NewLine;
                OtherIndicators += @"площадь помещений 1 этажа  -     кв.м;
площадь помещений подвального этажа -     кв.м";
                OtherIndicators.TrimEnd(Environment.NewLine.ToCharArray());
                
                if (constrPerm.OKSOtherIndicators == null || constrPerm.OKSOtherIndicators == "")
                    constrPerm.OKSOtherIndicators = OtherIndicators;
                


                foreach (Parcel parcel in constrPerm.Parcels)
                {
                    try { constrPerm.LotSquare += String.Format("{0}", parcel.Area) + Environment.NewLine; }
                    catch { }
                }
                constrPerm.LotSquare.TrimEnd(Environment.NewLine.ToCharArray());
            }
            
            if (constrPerm.TotalBuildSquareProject == "")
                constrPerm.TotalBuildSquareProject = "-";
            if (constrPerm.BuildingSizeProject == "")
                constrPerm.BuildingSizeProject = "-";
            if (constrPerm.BuildingSizeUnderGroundPartProject == "")
                constrPerm.BuildingSizeUnderGroundPartProject = "-";
            if (constrPerm.FloorCount == "")
                constrPerm.FloorCount = "-";
            if (constrPerm.UnderGroundFloorCount == "")
                constrPerm.UnderGroundFloorCount = "-";
            if (constrPerm.Height == "")
                constrPerm.Height = "-";
            if (constrPerm.Capacity == "")
                constrPerm.Capacity = "-";
            if (constrPerm.BuildSquare == "")
                constrPerm.BuildSquare = "-";
            if (constrPerm.OKSOtherIndicators == "")
                constrPerm.OKSOtherIndicators = "-";
            if (constrPerm.LotSquare == "")
                constrPerm.LotSquare = "-";
            if (constrPerm.TotalLivingSpaceProject == "")
                constrPerm.TotalLivingSpaceProject = "-";

            if (constrPerm.LotSquare == "")
                constrPerm.LotSquare = "-";
            if (constrPerm.LengthProject == "")
                constrPerm.LengthProject = "-";
            if (constrPerm.PowerLineProject == "")
                constrPerm.PowerLineProject = "-";
            if (constrPerm.ElectricLinesInfoProject == "")
                constrPerm.ElectricLinesInfoProject = "-";
            if (constrPerm.ConstructiveElementsInfoProject == "")
                constrPerm.ConstructiveElementsInfoProject = "-";

            #region // заполняем данные по типу разрешения и заявке для согласования
            if (constrPerm.LetterInfo == null || constrPerm.LetterInfo == "")
            {
                string devs = "";
                foreach(GeneralSubject subj in constrPerm.DocSubjects)
                {
                    if (subj.NameFrom != null && subj.NameFrom != "")
                        devs += subj.NameFrom + ", ";
                    else
                        devs += subj.Name + ", ";
                }
                devs = devs.TrimEnd().TrimEnd(',');
                constrPerm.LetterInfo = String.Format("Заявление {0} от {1} г. № {2}", devs, constrPerm.LetterDate.ToShortDateString(), constrPerm.LetterNo);
            }

            if (constrPerm.PermInfo == null || constrPerm.PermInfo == "")
            {
                string permInfo = "";
                string permInfoR = "";
                if (constrPerm.PermProlongues.Count != 0)
                {
                    permInfo = "Продление разрешения на";
                    permInfoR = "Отказ в продлении разрешения на";
                }
                else
                {
                    permInfo = "Выдача разрешения на";
                    permInfoR = "Отказ в выдаче разрешения на";
                }

                string t = "";
                if (constrPerm.WorkKind == AISOGD.Enums.eWorkKind.Строительство)
                    t += " cтроительство";
                if (constrPerm.WorkKind == AISOGD.Enums.eWorkKind.Реконструкция)
                    t += " реконструкцию";
                if (constrPerm.WorkKind == AISOGD.Enums.eWorkKind.РаботыПоСохранению)
                    t += " работы по сохранению";
                permInfo += t;
                permInfoR += t;

                t = "";
                if (constrPerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ОбъектКапитальногоСтроительства)
                    t += " объекта капитального строительства";
                if (constrPerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ЛинейныйОбъект)
                    t += " линейного объекта";
                if (constrPerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ОбъектКультурногоНаследия)
                    t += " объекта культурного наследия";
                if (constrPerm.BuildingCategory == AISOGD.Enums.eBuildingCategory.ОксВСоставеЛинейного)
                    t += " объекта капитального строительства в составе линейного";
                permInfo += t;
                permInfoR += t;

                if (constrPerm.ConstrPermInstead != null && constrPerm.PermProlongues.Count == 0)
                {
                    permInfo += " взамен предыдущего";
                    permInfoR += " взамен предыдущего";
                }


                constrPerm.PermInfo = permInfo;
                constrPerm.PermInfoR = permInfoR;
            }
            if(constrPerm.Decision1 == null)
            {
                try { constrPerm.Decision1 = connect.FindFirstObject<Employee>(x=> x.LastName == "Воропанов"); }
                catch { }
            }
            if (constrPerm.Decision2 == null)
            {
                try { constrPerm.Decision2 = connect.FindFirstObject<Employee>(x => x.LastName == "Борисовский"); }
                catch { }
            }
            if (constrPerm.AgreementEmploye1 == null)
            {
                try { constrPerm.AgreementEmploye1 = connect.FindFirstObject<Employee>(x => x.LastName == "Шмонова"); }
                catch { }
            }
            if (constrPerm.AgreementEmploye2 == null)
            {
                try { constrPerm.AgreementEmploye2 = connect.FindFirstObject<Employee>(x => x.LastName == "Угарина"); }
                catch { }
            }
            if (constrPerm.AgreementEmploye3 == null)
            {
                try { constrPerm.AgreementEmploye3 = connect.FindFirstObject<Employee>(x => x.LastName == "Аникин"); }
                catch { }
            }
            #endregion
            constrPerm.Save();
            os.CommitChanges();

            RefreshAllWindows();
            //
        }

        /// <summary>
        /// Функция заполнения семантики у стадии
        /// </summary>
        /// <param name="aMapInfo"></param>
        /// <param name="constrStage"></param>
        /// <param name="aLayer"></param>
        public void SetConstrStageSemantic(MapInfoApplication aMapInfo, ConstrStage constrStage, ConstrPerm constrPerm, string aLayer, string aMI_PRINX)
        {
            aMapInfo.Do("Select * From " + aLayer + " Where MI_PRINX = " + aMI_PRINX);// + " Into zObject");
            try
            {
                aMapInfo.Do("Update Selection Set НОМЕР_НА_СХЕМЕ =  \"" + constrStage.MapNo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ОЧЕРЕДЬ_СТР =  \"" + constrStage.StageNo + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set ОПИСАНИЕ =  \"" + constrStage.Name + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set СТРОИТ_АДРЕС =  \"" + constrStage.Address + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ПРОЕКТ_НОМЕР_ДОМА =  \"" + constrStage.ProjectHouseNo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set СТАТУС =  \"" + constrStage.ConstrStageState + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set МЕСТОПОЛОЖЕНИЕ =  \"" + constrStage.Street.FullName + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set ПРОЕКТ_НОМЕР_ДОМА =  \"" + constrStage.Address.Split(',')[constrStage.Address.LastIndexOf(',')] + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set КОД_ОБЪЕКТ =  \"" + constrStage.ConstructionType.Name + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                if(constrStage.FloorCount != 0)
                    aMapInfo.Do("Update Selection Set МАКС_ЭТАЖН =  " + constrStage.FloorCount + " Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set РАЗНОЭТАЖНОСТЬ =  \"" + constrStage.FloorCountDiff + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                if (constrStage.AppartmentsCountProject != 0)
                    aMapInfo.Do("Update Selection Set КОЛ_КВАРТ =  " + constrStage.AppartmentsCountProject + " Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set СТР_МАТЕРИАЛ_СТЕН =  \"" + constrStage.WallMaterialProject.Name + "\" Commit table " + aLayer);
            }
            catch { }

            try
            {
                aMapInfo.Do("Update Selection Set ВИД_РАБОТ =  \"" + constrStage.WorkKind + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ЗАСТРОЙЩИК =  \"" + constrPerm.ConstrDeveloperString.Replace("\"","'").Replace("«", "'").Replace("»", "'") + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set РЕКВИЗИТЫ_ЗАСТРОЙЩИКА =  \"" + constrPerm.ConstrDeveloperContactInfo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ДАТА_ВНЕС_ИНФ = \"" + DateTime.Now.Date.ToShortDateString() + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                //string p = "";
                //foreach (ConstrPerm perm in constrStage.ConstrPerms)
                //{
                //    try
                //    {
                //        string n = "";
                //        string d = "";

                //        try { n = " №" + constrPerm.DocNo; }
                //        catch { }
                //        try { d = "от " + constrPerm.DocDate.ToShortDateString(); }
                //        catch { }
                //        if (!p.Contains(d+ n))
                //            p += d + n + ", ";
                //    }
                //    catch { }
                //}
                //p = p.Trim();
                //p = p.TrimEnd(',');
                aMapInfo.Do("Update Selection Set РАЗРЕШ_СТР = \"" + constrStage.ConstrPermInfo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set ОЖИД_ГОД_ВВОД = " + constrStage.PlaningYearInUse + " Commit table " + aLayer);
            }
            catch { }
            try
            {
                string validDate = "";
                foreach (ConstrPerm perm in constrStage.ConstrPerms)
                {
                    try
                    {
                        if (!validDate.Contains(perm.ValidDate.ToShortDateString()))
                            validDate += perm.ValidDate.ToShortDateString() + ", " ;
                    }
                    catch { }

                }
                validDate = validDate.Trim();
                validDate = validDate.TrimEnd(',');
                aMapInfo.Do("Update Selection Set СРОК_РАЗРЕШ = \"" + validDate + "\" Commit table " + aLayer);
            }
            catch { }
        }
    }
    //public class OtherIndicatorTemp
    //{
    //    public string IndicatorType;
    //    public List<string> Values;
    //}

    public class OtherIndicatorTemp
    {
        public string IndicatorType;
        public List<StageValue> StageValues = new List<StageValue>();
    }
    public class StageValue
    {
        public string StageName;
        public string Value;
    }
}
