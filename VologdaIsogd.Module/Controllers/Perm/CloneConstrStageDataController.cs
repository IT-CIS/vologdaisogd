﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using AISOGD.Perm;
using AISOGD.Constr;
using DevExpress.XtraEditors;
using DevExpress.ExpressApp.Win;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CloneConstrStageDataController : ViewController
    {
        public CloneConstrStageDataController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        object constrCurrentStageId = String.Empty;
        /// <summary>
        /// Клонировать данные из выбранной стадии строительства.
        /// 1. Открываем список стадий строительства (по умолчанию отфильтрованных по объекту строительства)
        /// 2. При выборе стадии копируем из нее все данные в текущую стадию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloneConstrStageDataAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            constrCurrentStageId = ((BaseObjectXAF)View.CurrentObject).Oid;

            Connect connect = Connect.FromObjectSpace(os);
            ConstrStage currentStage = os.GetObjectByKey<ConstrStage>(constrCurrentStageId);
            CollectionSourceBase source = Application.CreateCollectionSource(os, typeof(ConstrStage), "123List");
            //Application.FindListViewId(typeof(ConstrPerm)));// "123list");
            string ID = Application.FindListViewId(typeof(ConstrStage));
            try
            {
                source.Criteria.Add("CapitalStructureBase", CriteriaOperator.Parse("CapitalStructureBase=?", currentStage.CapitalStructureBase));
            }
            catch { }
            e.ShowViewParameters.CreatedView = Application.CreateListView(ID, source, false);
            DialogController contr = new DialogController();

            contr.AcceptAction.Caption = "Выбрать объект";
            contr.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(GetConstrStageFromListView);
            contr.CancelAction.Caption = "Отмена";
            e.ShowViewParameters.Controllers.Add(contr);
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
        }

        /// <summary>
        /// Логика при выборе из списка стадий строительства
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetConstrStageFromListView(object sender, DialogControllerAcceptingEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            if (e.AcceptActionArgs.SelectedObjects.Count == 0)
            {
                XtraMessageBox.Show("Не выбрана ни одна стадия.");
                return;
            }
            if (e.AcceptActionArgs.SelectedObjects.Count > 1)
            {
                XtraMessageBox.Show("Выбрано больше одной стадии.");
                return;
            }
            Connect connect = Connect.FromObjectSpace(os);
            string constrOtherStageId = ((BaseObjectXAF)e.AcceptActionArgs.SelectedObjects[0]).Oid.ToString();
            ConstrStage constrCurrentStage = os.GetObjectByKey<ConstrStage>(constrCurrentStageId);
            ConstrStage constrOtherStage = os.FindObject<ConstrStage>(new BinaryOperator("Oid", constrOtherStageId));
            CopyDataFromAnotherConstrStage(constrCurrentStage, constrOtherStage, os);
        }

        private void CopyDataFromAnotherConstrStage(ConstrStage constrCurrentStage, ConstrStage constrOtherStage, IObjectSpace os)
        {
            try { constrCurrentStage.Name = constrOtherStage.Name; }
            catch { }
            try { constrCurrentStage.City = constrOtherStage.City; }
            catch { }
            try { constrCurrentStage.Street = constrOtherStage.Street; }
            catch { }
            try
            {
                constrCurrentStage.ProjectHouseNo = constrOtherStage.ProjectHouseNo;
            }
            catch { }
            try
            {
                constrCurrentStage.Address = constrOtherStage.Address;
            }
            catch { }
            try
            {
                constrCurrentStage.WorkKind = constrOtherStage.WorkKind;
            }
            catch { }
            try
            {
                constrCurrentStage.ConstructionType = constrOtherStage.ConstructionType;
            }
            catch { }
            try
            {
                constrCurrentStage.ConstructionCategory = constrOtherStage.ConstructionCategory;
            }
            catch { }
            
            try
            {
                constrCurrentStage.PlaningYearInUse = constrOtherStage.PlaningYearInUse;
            }
            catch { }
            //try
            //{
            //    foreach (ConstrStage constrObj in constrOtherStage.ConstrStages)
            //    {
            //        constrCurrentStage.ConstrStages.Add(constrObj);
            //        constrObj.ConstrStageState = AISOGD.Enums.eConstrStageState.ПодготовкаРС;
            //        constrObj.Save();
            //    }
            //}
            //catch { }
            try
            {
                constrCurrentStage.ConstrMaterial = constrOtherStage.ConstrMaterial;
            }
            catch { }

            try
            {
                constrCurrentStage.ConstrFireDangerClass = constrOtherStage.ConstrFireDangerClass;
            }
            catch { }
            try
            {
                constrCurrentStage.ConstrFireShield = constrOtherStage.ConstrFireShield;
            }
            catch { }
            try
            {
                constrCurrentStage.Note = constrOtherStage.Note;
            }
            catch { }
            try
            {
                constrCurrentStage.BuildingKind = constrOtherStage.BuildingKind;
            }
            catch { }
            // показатели объекта
            try
            {
                constrCurrentStage.BuildingSizeProject = constrOtherStage.BuildingSizeProject;
            }
            catch { }
            try
            {
                constrCurrentStage.BuildingSizeOverGroundPartProject = constrOtherStage.BuildingSizeOverGroundPartProject;
            }
            catch { }
            try
            {
                constrCurrentStage.BuildingSizeUnderGroundPartProject = constrOtherStage.BuildingSizeUnderGroundPartProject;
            }
            catch { }
            try
            {
                constrCurrentStage.TotalBuildSquareProject = constrOtherStage.TotalBuildSquareProject;
            }
            catch { }
            try
            {
                constrCurrentStage.BuildSquare = constrOtherStage.BuildSquare;
            }
            catch { }

            try
            {
                constrCurrentStage.Height = constrOtherStage.Height;
            }
            catch { }
            try
            {
                constrCurrentStage.HeightNote = constrOtherStage.HeightNote;
            }
            catch { }
            try
            {
                constrCurrentStage.NotLivingSquareProject = constrOtherStage.NotLivingSquareProject;
            }
            catch { }

            try
            {
                constrCurrentStage.OutBuildingSquareProject = constrOtherStage.OutBuildingSquareProject;
            }
            catch { }
            try
            {
                constrCurrentStage.BuildingCountProject = constrOtherStage.BuildingCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.FloorCount = constrOtherStage.FloorCount;
            }
            catch { }
            try
            {
                constrCurrentStage.UnderGroundFloorCount = constrOtherStage.UnderGroundFloorCount;
            }
            catch { }

            try
            {
                constrCurrentStage.FloorCountAbove = constrOtherStage.FloorCountAbove;
            }
            catch { }
            try
            {
                constrCurrentStage.FloorCountDiff = constrOtherStage.FloorCountDiff;
            }
            catch { }
            try
            {
                constrCurrentStage.FloorsNote = constrOtherStage.FloorsNote;
            }
            catch { }
            try
            {
                constrCurrentStage.ElevatorsCountProject = constrOtherStage.ElevatorsCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.EscalatorsCountProject = constrOtherStage.EscalatorsCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.InvalidLiftsCountProject = constrOtherStage.InvalidLiftsCountProject;
            }
            catch { }

            try
            {
                constrCurrentStage.FundMaterialProject = constrOtherStage.FundMaterialProject;
            }
            catch { }
            ///
            try
            {
                constrCurrentStage.WallMaterialProject = constrOtherStage.WallMaterialProject;
            }
            catch { }
            try
            {
                constrCurrentStage.BorderMaterialProject = constrOtherStage.BorderMaterialProject;
            }
            catch { }
            try
            {
                constrCurrentStage.RoofMaterialProject = constrOtherStage.RoofMaterialProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ElectroProject = constrOtherStage.ElectroProject;
            }
            catch { }
            try
            {
                constrCurrentStage.HeatProject = constrOtherStage.HeatProject;
            }
            catch { }
            try
            {
                constrCurrentStage.WaterProject = constrOtherStage.WaterProject;
            }
            catch { }
            try
            {
                constrCurrentStage.GazProject = constrOtherStage.GazProject;
            }
            catch { }
            try
            {
                constrCurrentStage.HouseSeverageProject = constrOtherStage.HouseSeverageProject;
            }
            catch { }
            try
            {
                constrCurrentStage.PhonesProject = constrOtherStage.PhonesProject;
            }
            catch { }
            try
            {
                constrCurrentStage.TVProject = constrOtherStage.TVProject;
            }
            catch { }
            try
            {
                constrCurrentStage.RadioProject = constrOtherStage.RadioProject;
            }
            catch { }
            try
            {
                constrCurrentStage.SeverageProject = constrOtherStage.SeverageProject;
            }
            catch { }
            ///
            try
            {
                constrCurrentStage.OtherIndicatorsProject = constrOtherStage.OtherIndicatorsProject;
            }
            catch { }
            try
            {
                constrCurrentStage.PlacesCountProject = constrOtherStage.PlacesCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.RoomCountProject = constrOtherStage.RoomCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.CapacityProject = constrOtherStage.CapacityProject;
            }
            catch { }
            try
            {
                constrCurrentStage.LivingSpaceProject = constrOtherStage.LivingSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.TotalLivingSpaceProject = constrOtherStage.TotalLivingSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.NotLivingSpaceProject = constrOtherStage.NotLivingSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.SectionCountProject = constrOtherStage.SectionCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.PorchCountProject = constrOtherStage.PorchCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.AppartmentsCountProject = constrOtherStage.AppartmentsCountProject;
            }
            catch { }
            ///
            try
            {
                constrCurrentStage.AppartmentsSpaceProject = constrOtherStage.AppartmentsSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.StudioCountProject = constrOtherStage.StudioCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.StudioSpaceProject = constrOtherStage.StudioSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.OneRoomFlatCountProject = constrOtherStage.OneRoomFlatCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.OneRoomFlatSpaceProject = constrOtherStage.OneRoomFlatSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.TwoRoomFlatCountProject = constrOtherStage.TwoRoomFlatCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.TwoRoomFlatSpaceProject = constrOtherStage.TwoRoomFlatSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ThreeRoomFlatCountProject = constrOtherStage.ThreeRoomFlatCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ThreeRoomFlatSpaceProject = constrOtherStage.ThreeRoomFlatSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.FourRoomFlatCountProject = constrOtherStage.FourRoomFlatCountProject;
            }
            catch { }
            ///
            try
            {
                constrCurrentStage.FourRoomSpaceProject = constrOtherStage.FourRoomSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.MoreRoomFlatCountProject = constrOtherStage.MoreRoomFlatCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.MoreRoomSpaceProject = constrOtherStage.MoreRoomSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ElitAppartmentsCountProject = constrOtherStage.ElitAppartmentsCountProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ElitAppartmentsSpaceProject = constrOtherStage.ElitAppartmentsSpaceProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ProductPurposeObjectType = constrOtherStage.ProductPurposeObjectType;
            }
            catch { }
            try
            {
                constrCurrentStage.PowerProject = constrOtherStage.PowerProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ProductivityProject = constrOtherStage.ProductivityProject;
            }
            catch { }
            try
            {
                constrCurrentStage.LineObjectClass = constrOtherStage.LineObjectClass;
            }
            catch { }
            try
            {
                constrCurrentStage.LengthProject = constrOtherStage.LengthProject;
            }
            catch { }
            try
            {
                constrCurrentStage.PowerLineProject = constrOtherStage.PowerLineProject;
            }
            catch { }
            try
            {
                constrCurrentStage.PipesInfoProject = constrOtherStage.PipesInfoProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ElectricLinesInfoProject = constrOtherStage.ElectricLinesInfoProject;
            }
            catch { }
            try
            {
                constrCurrentStage.ConstructiveElementsInfoProject = constrOtherStage.ConstructiveElementsInfoProject;
            }
            catch { }
            try
            {
                constrCurrentStage.EnergyEfficiencyClassProject = constrOtherStage.EnergyEfficiencyClassProject;
            }
            catch { }
            ///
            try
            {
                constrCurrentStage.HeatUnit = constrOtherStage.HeatUnit;
            }
            catch { }
            try
            {
                constrCurrentStage.HeatConsumptionProject = constrOtherStage.HeatConsumptionProject;
            }
            catch { }
            try
            {
                constrCurrentStage.OutdoorIsolationMaterialProject = constrOtherStage.OutdoorIsolationMaterialProject;
            }
            catch { }
            try
            {
                constrCurrentStage.SkylightsFillingProject = constrOtherStage.SkylightsFillingProject;
            }
            catch { }
            
            constrCurrentStage.Save();
            os.CommitChanges();
            RefreshAllWindows();
        }

        // обновить все окна
        protected void RefreshAllWindows()
        {
            foreach (WinWindow existingWindow in ((WinShowViewStrategyBase)Application.ShowViewStrategy).Windows)
            {
                if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
                {
                    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
                }
            }
        }
    }
}
