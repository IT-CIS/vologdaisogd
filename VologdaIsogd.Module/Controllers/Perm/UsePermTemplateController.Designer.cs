namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class UsePermTemplateController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UsePermTemplateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // UsePermTemplateAction
            // 
            this.UsePermTemplateAction.Caption = "������������";
            this.UsePermTemplateAction.ConfirmationMessage = null;
            this.UsePermTemplateAction.Id = "UsePermTemplateAction";
            this.UsePermTemplateAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.UsePermTemplateAction.TargetObjectType = typeof(AISOGD.Perm.UsePerm);
            this.UsePermTemplateAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.UsePermTemplateAction.ToolTip = null;
            this.UsePermTemplateAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.UsePermTemplateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UsePermTemplateAction_Execute);
            // 
            // UsePermTemplateController
            // 
            this.Actions.Add(this.UsePermTemplateAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction UsePermTemplateAction;
    }
}
