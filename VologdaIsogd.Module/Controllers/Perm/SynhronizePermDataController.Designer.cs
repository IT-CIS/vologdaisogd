﻿namespace VologdaIsogd.Module.Controllers.Perm
{
    partial class SynhronizePermDataController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SynhronizePermDataAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SynhronizePermDataAction
            // 
            this.SynhronizePermDataAction.Caption = "Подготовка данных";
            this.SynhronizePermDataAction.ConfirmationMessage = null;
            this.SynhronizePermDataAction.Id = "SynhronizePermDataAction";
            this.SynhronizePermDataAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.SynhronizePermDataAction.TargetObjectType = typeof(AISOGD.Perm.ConstrPerm);
            this.SynhronizePermDataAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.SynhronizePermDataAction.ToolTip = "Заполняются данные в разрешении на строительство:\r\n - Наименование\r\n - Адрес\r\n - " +
    "Краткие характеристики";
            this.SynhronizePermDataAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.SynhronizePermDataAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SynhronizePermDataAction_Execute);
            // 
            // SynhronizePermDataController
            // 
            this.Actions.Add(this.SynhronizePermDataAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SynhronizePermDataAction;
    }
}
