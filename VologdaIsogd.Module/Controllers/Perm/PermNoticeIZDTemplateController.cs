﻿using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using AISOGD.SystemDir;
using DevExpress.Persistent.BaseImpl;
using AISOGD.Perm;
using System.Reflection;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System.IO;

namespace VologdaIsogd.Module.Controllers.Perm
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PermNoticeIZDTemplateController : ViewController
    {
        public PermNoticeIZDTemplateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Сформировать уведомление о соответствии строительства
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PermNoticeIZDTemplateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            PermNoticeIZD perm = os.GetObjectByKey<PermNoticeIZD>(x_id);

            Connect connect = Connect.FromObjectSpace(os);

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\PermNoticeIzd.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            //Застройщик
            // делим на слова для вставки в строки
            string developer = "";
            developer = perm.ConstrDeveloperStringDat ?? "";


            try
            {
                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = developer.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 25)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterBook1 = richServer.Document.Bookmarks["RequesterBook1"];
                richServer.Document.Replace(RequesterBook1.Range, text1.TrimEnd());
                Bookmark RequesterBook2 = richServer.Document.Bookmarks["RequesterBook2"];
                richServer.Document.Replace(RequesterBook2.Range, text2.TrimEnd());
                Bookmark RequesterBook3 = richServer.Document.Bookmarks["RequesterBook3"];
                richServer.Document.Replace(RequesterBook3.Range, text3.TrimEnd());
            }
            catch { }


            // адрес застройщика
            try
            {
                string RequesterAddr = "";
                try
                {
                    RequesterAddr = perm.ConstrDeveloperContactInfo;
                    RequesterAddr = RequesterAddr.Replace("Адрес:", "");
                }
                catch { }
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RequesterAddr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterAddrBook1 = richServer.Document.Bookmarks["RequesterAddrBook1"];
                richServer.Document.Replace(RequesterAddrBook1.Range, text1);
                Bookmark RequesterAddrBook2 = richServer.Document.Bookmarks["RequesterAddrBook2"];
                richServer.Document.Replace(RequesterAddrBook2.Range, text2.Trim());
                
            }
            catch { }

            // email
            try
            {
                string Email = perm.ConstrDeveloperEmail?? "";
                
                Bookmark EmailBook = richServer.Document.Bookmarks["EmailBook"];
                richServer.Document.Replace(EmailBook.Range, Email);
            }
            catch { }

            // вид работ (цель подачи)
            try {
                Bookmark WorkTypeBook = richServer.Document.Bookmarks["ConstractionWorkBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(WorkTypeBook.Range);
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Строительство)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);

            }
            catch { }
            try
            {
                Bookmark WorkTypeBook = richServer.Document.Bookmarks["ReConstractionWorkBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(WorkTypeBook.Range);
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Реконструкция)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);
            }
            catch { }

            // тип объекта ИЖД
            try
            {
                Bookmark IZDBuildKindkBook = richServer.Document.Bookmarks["IZDBuildKindkBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(IZDBuildKindkBook.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.ИЖД)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);

            }
            catch { }
            try
            {
                Bookmark IZDBuildKindkBook1 = richServer.Document.Bookmarks["IZDBuildKindkBook1"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(IZDBuildKindkBook1.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.ИЖД)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);

            }
            catch { }
            try
            {
                Bookmark HouseBuildKindBook = richServer.Document.Bookmarks["HouseBuildKindBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(HouseBuildKindBook.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);
            }
            catch { }

            try
            {
                Bookmark HouseBuildKindBook1 = richServer.Document.Bookmarks["HouseBuildKindBook1"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(HouseBuildKindBook1.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);
            }
            catch { }


            // дата заявки
            string LetterDate = "";
            try
            {
               

                if(perm.LetterDate != DateTime.MinValue)
                {
                    LetterDate = perm.LetterDate.Day.ToString();
                    if (LetterDate.Length == 1)
                        LetterDate = "0" + LetterDate;
                    LetterDate += $" {connect.GetMonth(perm.LetterDate)} {perm.LetterDate.Year} года";
                }

                Bookmark LetterDateBook = richServer.Document.Bookmarks["LetterDateBook"];
                richServer.Document.Replace(LetterDateBook.Range, LetterDate);
            }
            catch { }

            // реквизиты заявки
            try
            {
                string LetterInfo = "";

                LetterInfo = $"{LetterDate} № {perm.LetterNo}";

                Bookmark LetterInfoBook = richServer.Document.Bookmarks["LetterInfoBook"];
                richServer.Document.Replace(LetterInfoBook.Range, LetterInfo);
            }
            catch { }

            // Сведения о земельном участке
            try
            {
                string ParcelInfo = "";

                ParcelInfo = $"с кадастровым номером {perm.LotCadNo?? ""} по адресу: {perm.ObjectAddress}";

                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = ParcelInfo.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 70)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark LotAddrBook1 = richServer.Document.Bookmarks["LotAddrBook1"];
                richServer.Document.Replace(LotAddrBook1.Range, text1.Trim());
                Bookmark LotAddrBook2 = richServer.Document.Bookmarks["LotAddrBook2"];
                richServer.Document.Replace(LotAddrBook2.Range, text2.Trim());

            }
            catch { }

            // Подписывающее лицо
            try
            {
                string SignerCapacity = "";
                try { SignerCapacity = perm.signer.capacity; }
                catch { }
                Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
            }
            catch { }
            try
            {
                string SignerName = "";
                try { SignerName = perm.signer.name; }
                catch { }
                Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                richServer.Document.Replace(SignerNameBook.Range, SignerName);
            }
            catch { }



            string permInfo = "";
            //try { permInfo = String.Format($"№ {perm.FullDocNo?? ""} от {perm.DocDate.ToShortDateString()}"); }
            //catch { }
            //string path = Path.GetTempPath() + @"\Уведомление на строительство_" + permInfo + "_" +
            //    DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            try
            {
                string letterNo = "";
                if ((perm.LetterNo ?? "").Contains("/"))
                    letterNo = (perm.LetterNo ?? "").Split('/')[1];
                permInfo = String.Format($"{letterNo}_{perm.ConstrDeveloperString.Split(' ')[0]}_{perm.ObjectAddress ?? ""}");
            }
            catch { }
            string path = Path.GetTempPath() + permInfo +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }

        /// <summary>
        /// Сформирвоать уведомление о несоответствии параметров строительства ИЖД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PermNoticeIZDRefuseTemplateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            var x_id = ((BaseObject)e.CurrentObject).Oid;
            PermNoticeIZD perm = os.GetObjectByKey<PermNoticeIZD>(x_id);

            Connect connect = Connect.FromObjectSpace(os);

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("VologdaIsogd.Module.DLL", @"\DocTemplates\Perm\PermNoticeIzdRefuse.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            //Застройщик
            // делим на слова для вставки в строки
            string developer = "";
            developer = perm.ConstrDeveloperStringDat ?? "";


            try
            {
                string text1 = "";
                string text2 = "";
                string text3 = "";
                string[] separators = { " " };
                string[] words = developer.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount && (text2 + words[i]).Length < 25)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text3 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterBook1 = richServer.Document.Bookmarks["RequesterBook1"];
                richServer.Document.Replace(RequesterBook1.Range, text1.TrimEnd());
                Bookmark RequesterBook2 = richServer.Document.Bookmarks["RequesterBook2"];
                richServer.Document.Replace(RequesterBook2.Range, text2.TrimEnd());
                Bookmark RequesterBook3 = richServer.Document.Bookmarks["RequesterBook3"];
                richServer.Document.Replace(RequesterBook3.Range, text3.TrimEnd());
            }
            catch { }


            // адрес застройщика
            try
            {
                string RequesterAddr = "";
                try
                {
                    RequesterAddr = perm.ConstrDeveloperContactInfo;
                    RequesterAddr = RequesterAddr.Replace("Адрес:", "");
                }
                catch { }
                string text1 = "";
                string text2 = "";
                string[] separators = { " " };
                string[] words = RequesterAddr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                int wordscount = words.Length;
                while (i < wordscount && (text1 + words[i]).Length < 25)
                {
                    text1 += words[i] + " ";
                    i++;
                }
                if (i < wordscount)
                {
                    while (i < wordscount)
                    {
                        text2 += words[i] + " ";
                        i++;
                    }
                }
                Bookmark RequesterAddrBook1 = richServer.Document.Bookmarks["RequesterAddrBook1"];
                richServer.Document.Replace(RequesterAddrBook1.Range, text1);
                Bookmark RequesterAddrBook2 = richServer.Document.Bookmarks["RequesterAddrBook2"];
                richServer.Document.Replace(RequesterAddrBook2.Range, text2.Trim());

            }
            catch { }

            // email
            try
            {
                string Email = perm.ConstrDeveloperEmail ?? "";

                Bookmark EmailBook = richServer.Document.Bookmarks["EmailBook"];
                richServer.Document.Replace(EmailBook.Range, Email);
            }
            catch { }

            // вид работ (цель подачи)
            try
            {
                Bookmark WorkTypeBook = richServer.Document.Bookmarks["ConstractionWorkBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(WorkTypeBook.Range);
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Строительство)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);

            }
            catch { }
            try
            {
                Bookmark WorkTypeBook = richServer.Document.Bookmarks["ReConstractionWorkBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(WorkTypeBook.Range);
                if (perm.IZDAim == AISOGD.Enums.eIZDAim.Реконструкция)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);
            }
            catch { }

            // тип объекта ИЖД
            try
            {
                Bookmark IZDBuildKindkBook = richServer.Document.Bookmarks["IZDBuildKindkBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(IZDBuildKindkBook.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.ИЖД)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);

            }
            catch { }
            try
            {
                Bookmark IZDBuildKindkBook1 = richServer.Document.Bookmarks["IZDBuildKindkBook1"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(IZDBuildKindkBook1.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.ИЖД)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);

            }
            catch { }
            try
            {
                Bookmark HouseBuildKindBook = richServer.Document.Bookmarks["HouseBuildKindBook"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(HouseBuildKindBook.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);
            }
            catch { }

            try
            {
                Bookmark HouseBuildKindBook1 = richServer.Document.Bookmarks["HouseBuildKindBook1"];
                CharacterProperties cp = richServer.Document.BeginUpdateCharacters(HouseBuildKindBook1.Range);
                if (perm.IZDKind == AISOGD.Enums.eIZDKind.СадовыйДом)
                {
                    cp.Bold = true;
                }
                richServer.Document.EndUpdateCharacters(cp);
            }
            catch { }


            // дата заявки
            string LetterDate = "";
            try
            {


                if (perm.LetterDate != DateTime.MinValue)
                {
                    LetterDate = perm.LetterDate.Day.ToString();
                    if (LetterDate.Length == 1)
                        LetterDate = "0" + LetterDate;
                    LetterDate += $" {connect.GetMonth(perm.LetterDate)} {perm.LetterDate.Year} года";
                }

                Bookmark LetterDateBook = richServer.Document.Bookmarks["LetterDateBook"];
                richServer.Document.Replace(LetterDateBook.Range, LetterDate);
            }
            catch { }

            // реквизиты заявки
            try
            {
                string LetterInfo = "";

                LetterInfo = $"{LetterDate} № {perm.LetterNo}";

                Bookmark LetterInfoBook = richServer.Document.Bookmarks["LetterInfoBook"];
                richServer.Document.Replace(LetterInfoBook.Range, LetterInfo);

                Bookmark LetterInfoBook1 = richServer.Document.Bookmarks["LetterInfoBook1"];
                richServer.Document.Replace(LetterInfoBook1.Range, LetterInfo);
            }
            catch { }

            // Причины отказа в согласовании
            try
            {
                string RefuseReasonParam = "";
                try { RefuseReasonParam = perm.RefuseReasonParam; }
                catch { }
                Bookmark RefuseReasonParamBook = richServer.Document.Bookmarks["RefuseReasonParamBook"];
                richServer.Document.Replace(RefuseReasonParamBook.Range, RefuseReasonParam);
            }
            catch { }
            try
            {
                string RefuseReasonPlacement = "";
                try { RefuseReasonPlacement = perm.RefuseReasonPlacement; }
                catch { }
                Bookmark RefuseReasonPlacementBook = richServer.Document.Bookmarks["RefuseReasonPlacementBook"];
                richServer.Document.Replace(RefuseReasonPlacementBook.Range, RefuseReasonPlacement);
            }
            catch { }
            try
            {
                string RefuseReasonRights = "";
                try { RefuseReasonRights = perm.RefuseReasonRights; }
                catch { }
                Bookmark RefuseReasonRightsBook = richServer.Document.Bookmarks["RefuseReasonRightsBook"];
                richServer.Document.Replace(RefuseReasonRightsBook.Range, RefuseReasonRights);
            }
            catch { }
            try
            {
                string RefuseReasonAppearance = "";
                try { RefuseReasonAppearance = perm.RefuseReasonAppearance; }
                catch { }
                Bookmark RefuseReasonAppearanceBook = richServer.Document.Bookmarks["RefuseReasonAppearanceBook"];
                richServer.Document.Replace(RefuseReasonAppearanceBook.Range, RefuseReasonAppearance);
            }
            catch { }

            // Подписывающее лицо
            try
            {
                string SignerCapacity = "";
                try { SignerCapacity = perm.signer.capacity; }
                catch { }
                Bookmark SignerCapacityBook = richServer.Document.Bookmarks["SignerCapacityBook"];
                richServer.Document.Replace(SignerCapacityBook.Range, SignerCapacity);
            }
            catch { }
            try
            {
                string SignerName = "";
                try { SignerName = perm.signer.name; }
                catch { }
                Bookmark SignerNameBook = richServer.Document.Bookmarks["SignerNameBook"];
                richServer.Document.Replace(SignerNameBook.Range, SignerName);
            }
            catch { }



            string permInfo = "";
            try {
                string letterNo = "";
                if ((perm.LetterNo ?? "").Contains("/"))
                    letterNo = (perm.LetterNo ?? "").Split('/')[1];
                permInfo = String.Format($"{letterNo}_{perm.ConstrDeveloperString.Split(' ')[0]}_{perm.ObjectAddress?? ""}"); }
            catch { }
            string path = Path.GetTempPath() + permInfo + @"_отказ" +
                DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }
    }
}
