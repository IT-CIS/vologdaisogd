using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.Xpo;
using System.Collections;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.Persistent.BaseImpl;
using System.Runtime.InteropServices;
using MapInfoActiveX;
//using MapInfo;
//using MapInfoActiveX;

namespace VologdaIsogd.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class MapController : ViewController
    {
        public MapController()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var currentObject = View.CurrentObject as XPBaseObject;
            IList list = ObjectSpace.GetObjects(typeof(SpatialConfig));
            if (View is DetailView)
            {
                if (currentObject != null)
                {
                    if (list.Cast<SpatialConfig>().FirstOrDefault(mc => mc.IsogdClassName == currentObject.ClassInfo.ClassType) != null)// View.ObjectTypeInfo.Type) != null)
                    {
                        Frame.GetController<MapController>().SetInGeoLinkAction.Active.SetItemValue("myReason", true);
                        if (currentObject != null && Connect.FromObjectSpace(ObjectSpace)
                            .IsExist<SpatialRepository>(mc => mc.IsogdObjectId == currentObject.ClassInfo.KeyProperty.GetValue(currentObject).ToString()))
                        {
                            Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", true);
                            Frame.GetController<MapController>().ViewMapAction.Caption = "�������� �� �����";
                        }
                        else
                        { Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", false); }
                    }
                    else
                    {
                        Frame.GetController<MapController>().SetInGeoLinkAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", false);
                    }
                }
            }
            else
            {
                Frame.GetController<MapController>().SetInGeoLinkAction.Active.SetItemValue("myReason", false);
                Frame.GetController<MapController>().ViewMapAction.Active.SetItemValue("myReason", false);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private MapInfoComModule m_MapInfoCom = null;
        private AISOGD.SystemDir.MapInfoApplication m_MapInfo = null;

        /// <summary>
        /// ����� ���������� � ���������������� �������� ������ � �������� MapInfo (� ��������). � �������� �������� ������ ��������� ��� �����.
        /// �������� 1 � 1.
        /// 1. �������� ������� �� � ����� ����������� �������
        /// 2. �������� �������� ������� � ���� (�������) ����������� ������� �� �����
        /// 3. ���������, ����� �� ������� ���������� ������ �� ����� � ������� ���������� (SpatialConfig)
        /// 4. ���������, ���� �� � �������� ����������� ��� ��������� ���������������� ������� (����� ���������, �� �������� �� ���������� ������ �������)
        /// 5. �����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetInGeoLinkAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // 1. �������� id, ����� ����������� ������� � UnitOfWork
            if (e.CurrentObject == null) return;
            var id = ((BaseObject)e.CurrentObject).Oid;
            var isogdClass = ((BaseObject)e.CurrentObject).ClassInfo;
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);
            bool isOk = true;
            // 2. �������� �������� �������
            //m_MapInfoCom = new MapInfoActiveX.MapInfoComModule();
            //m_MapInfo = m_MapInfoCom.GetActiveMapInfo();
            //var layer = m_MapInfo.Eval("SelectionInfo(1)");
            //XtraMessageBox.Show("layer: " + layer);
            m_MapInfo = new MapInfoApplication();

            //Type mapinfotype = Type.GetTypeFromProgID("Mapinfo.Application");
            //m_MapInfo = (MapInfoApplication)Activator.CreateInstance(mapinfotype);

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo �� ��������", "����");
                return;
            }
            // ���������� ��������� ������� � �������
            if (Convert.ToInt32(m_MapInfo.Eval("SelectionInfo(3)")) == 0)
            {
                XtraMessageBox.Show(
                    "�� ������� �� ������ ����������������� �������.", "����");
                return;
            }
            
            // 2. �������� ������� (����) ���������
            var layer = m_MapInfo.Eval("SelectionInfo(1)");
            //XtraMessageBox.Show("layer: " + layer);
            // 3. ���������, ����� �� ������� ������� ���������� ������ � ���������� �� �����
            if (!spatial.IsLayerValid(isogdClass.FullName, layer))
            {
                XtraMessageBox.Show(
                    "������ ����� ������ ������� � ���������� ��� ��������. ��������� ��������� �����.", "����");
                return;
            }
            // �������� ���������� ���������� �������� �������
            int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));

            // ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository 
            DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
            var xtraMessageBoxForm = new XtraMessageBoxForm(); ;
            // ���� �������� ������ ������ ����������������� ������� �� ����� - ������� ��������� �� ����
            if (selObjCount > 1)
            {
                //xtraMessageBoxForm = new XtraMessageBoxForm();
                if (
                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                        String.Format("�������� {0} �������� ������� {1}. ����������?",
                            selObjCount.ToString(), layer),
                        "��������!", dialogResults, null, 0)) == DialogResult.No)
                { return; }
            }
                // ���� � ����������� ������� ��� ����� � �����������������
                if (!spatial.IsObjectLinked(isogdClass.FullName, id.ToString()))
            {
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) ����������� ������� �������
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                            {
                                xtraMessageBoxForm = new XtraMessageBoxForm();
                                if (
                                    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                        String.Format("������ ��� ������ [MI_PRINX: {0}] ��� ������ � ����������. �����������?",
                                            gisObjId),
                                        "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                                {
                                    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
                                        layer,
                                        gisObjId);
                                }
                            }
                            else
                                spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
                                    layer,
                                    gisObjId);
                        }
                        else
                        {
                            MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                    }
                }
            }
            //���� ����, �������� �� ����, � ����������, ������� ��� � ����� ��� �������������
            // ���� ��������� - ���������� ���������� � ��������� - ����� ���������� ��� ������ � ����� �� ����������
            else
            {
                //DialogResult[] dialogResult = { DialogResult.OK, DialogResult.Yes, DialogResult.Cancel };
                var form = new DialogForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    label1 =
                    {
                        Text = String.Format(
                            "������ ���������� ������ [{0}] ��� ������ � �����������������. ������� �� ������ " +
                            " '��' ���� ������ �����������, �� ������ '��������', ���� ������ �������� �����, �� ������ '������' ��� ������",
                            CaptionHelper.GetClassCaption(isogdClass.FullName))
                    }
                };
                form.ShowDialog();
                
                //XtraMessageBoxForm xtraMessageBoxForm;
                var result = form.DialogResult;
                if (result == DialogResult.Cancel)
                {
                    return;
                }

                if (result == DialogResult.OK)
                {
                    spatial.DeleteSpatialLinks(isogdClass.FullName, id.ToString());
                    ObjectSpace.CommitChanges();
                    for (int i = 1; i < selObjCount + 1; i++)
                    {
                        m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                        try
                        {
                            // Id (MI_PRINX) ����������� ������� �������
                            string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                            if (gisObjId != "0")
                            {
                                if (!spatial.IsObjectLinkExist(isogdClass.FullName, id.ToString(), layer, gisObjId))
                                {
                                    if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                                    {
                                        xtraMessageBoxForm = new XtraMessageBoxForm();
                                        if (
                                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                                String.Format("������ ��� ������ [MI_PRINX: {0}] ��� ������ � ����������. �����������?",
                                                    gisObjId),
                                                "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                                        {
                                            spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
                                                layer,
                                                gisObjId);
                                        }
                                    }
                                    else
                                        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
                                            layer,
                                            gisObjId);
                                }
                            }
                            else
                            {
                                MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
                                return;
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                        }
                    }
                }
                if (result == DialogResult.Ignore)
                {
                    for (int i = 1; i < selObjCount + 1; i++)
                    {
                        m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                        // Id (MI_PRINX) ����������� ������� �������
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            if (!spatial.IsObjectLinkExist(isogdClass.FullName, id.ToString(), layer, gisObjId))
                            {
                                if (spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                                {
                                    xtraMessageBoxForm = new XtraMessageBoxForm();
                                    if (
                                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                            String.Format("������ ��� ������ [MI_PRINX: {0}] ��� ������ � ����������. �����������?",
                                                gisObjId),
                                            "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                                    {
                                        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
                                            layer,
                                            gisObjId);
                                    }
                                }
                                else
                                    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(),
                                        layer,
                                        gisObjId);
                            }
                        }
                        else
                        {
                            MessageBox.Show("����� ������� ���������� ��������� ���������������� ������");
                            return;
                        }
                    }
                }
            }

            #region /// ������� ����� ����� ������� ������� - ����� ������������ � ���� (XafId, XafClass) ������
            // 4. ���������, ������ �� ������� ���������� ������ � ����� ������ ����������������
            
            ////XtraMessageBox.Show("������ �� ������� ���������� ������ � ����� ������ ����������������: " + spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()).ToString());
            //if (!spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()))
            //{
            //    // ���������� �� ������
            //    // ���������� � ���������������� ������ ������ � �����
            //    isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //    // ���������� � SpatialRepository ����������, ��� �������
            //    string gisObjId = "1";
            //    try
            //    {
            //        m_MapInfo.Do("Select MI_PRINX from Selection Into tmpTable");
            //        //    selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
            //        //    for (int i = 1; i < selObjCount + 1; i++)
            //        //    {
            //        m_MapInfo.Do("Fetch Rec first From tmpTable");
            //        // Id ����������� ������� ��������� �����
            //        gisObjId = m_MapInfo.Eval("tmpTable.Col1");
            //    }
            //    catch { }
            //    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, gisObjId);
            //}
            //else
            //{
            //    // ���������� ��� ������ � ����� �� ����������������
            //    //var xtraMessageBoxForm = new XtraMessageBoxForm();
            //    //if (
            //    //    xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
            //    //        String.Format("������ ���������� ������ [{0}] ��� ������ � ����������������. �����������?",
            //    //            CaptionHelper.GetClassCaption(isogdClass.FullName)),
            //    //        "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
            //    //{
            //    //    spatial.DelLinkMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //    //    isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //    //    // ���������� � SpatialRepository ����������, ��� �������
            //    //    spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
            //    //}

            //    // ����������, �������� ��������, ������� ��� � ���� ��� �������������
            //    var form = new DialogForm
            //    {
            //        StartPosition = FormStartPosition.CenterParent,
            //        label1 =
            //        {
            //            Text = String.Format(
            //                "������ ���������� ������ [{0}] ��� ������ � ����������������. ������� �� ������ " +
            //                " '��' ���� ������ �����������, �� ������ '��������', ���� ������ �������� �����, �� ������ '������' ��� ������",
            //                CaptionHelper.GetClassCaption(isogdClass.FullName))
            //        }
            //    };
            //    form.ShowDialog();
            //    XtraMessageBoxForm xtraMessageBoxForm;
            //    var result = form.DialogResult;
            //    // ������
            //    if (result == DialogResult.Cancel)
            //    {
            //        return;
            //    }
            //    // �����������
            //    if (result == DialogResult.OK)
            //    {
            //        spatial.DelLinkMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //        isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //        // ���������� � SpatialRepository ����������, ��� �������
            //        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
            //    }
            //    // �������� �����
            //    if (result == DialogResult.Ignore)
            //    {
            //        isOk = spatial.SetLinkToMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //        // ���������� � SpatialRepository ����������, ��� �������
            //        spatial.AddSpatialLink(isogdClass.FullName, id.ToString(), layer, "1");
            //    }
            //}
            //if (isOk)
            //    XtraMessageBox.Show("�������� ���������", "����");
            //else
            //    XtraMessageBox.Show("��� �� ����� �� ���", "����");
            #endregion

            XtraMessageBox.Show("�������� ���������", "����");
        }

        /// <summary>
        /// �������� ��������� ���������������� ������ �� �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewMapAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // 1. �������� id, ����� ����������� ������� � UnitOfWork
            if (e.CurrentObject == null) return;
            var id = ((BaseObject)e.CurrentObject).Oid;
            var isogdClass = ((BaseObject)e.CurrentObject).ClassInfo;
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);
            bool isOk = false;
            // 2. �������� �������� �������
            //m_MapInfoCom = new MapInfoActiveX.MapInfoComModule();
            //m_MapInfo = m_MapInfoCom.GetActiveMapInfo();

            m_MapInfo = new MapInfoApplication();
            //Type mapinfotype = Type.GetTypeFromProgID("Mapinfo.Application");
            //m_MapInfo = (MapInfoApplication)Activator.CreateInstance(mapinfotype);

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo �� ��������", "����");
                return;
            }
            // ������� ������ ����� ������� Xaf - ����� ������������ � SpatialRepository 
            var objs = spatial.GetSpatialLinks(isogdClass.FullName, id.ToString());
            foreach (var spatialRepository in objs)
            {
                try
                {
                    string gisObjId = spatialRepository.SpatialObjectId;
                    string layer = spatialRepository.SpatialLayerId;
                    //m_MapInfo.Do("Select * From \"��������_�����\" Where MI_PRINX = 1 Into ZoomObject");
                    m_MapInfo.Do("Select * From " + layer + " Where MI_PRINX = " + gisObjId + " Into ZoomObject");
                    //m_MapInfo.Do("Select * From Isogddoc Where MI_PRINX = 7792 Into ZoomObject");
                    m_MapInfo.Do("Add Map Layer ZoomObject Fetch first From ZoomObject Set Map Center (CentroidX(ZoomObject.obj),CentroidY(ZoomObject.obj)) " +
                    "Add Map Layer ZoomObject Set Map Zoom Entire Layer ZoomObject");
                    m_MapInfo.Do("Drop Table ZoomObject");
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(String.Format("InnerException: {0}; Message: {1}; , TargetSite: {2}", ex.InnerException, ex.Message, ex.TargetSite));
                }

            }
            

            /////������� ����� ����� ������� ������� - ����� ������������ � ���� (XafId, XafClass) ������
            //// �� ������� �������� ������ �����, � ������� ����� ������
            //foreach (string layer in spatial.GetListLinkedSpatialLayers(isogdClass.FullName))
            //{
            //    if (spatial.isMapInfoObjectLinked(m_MapInfo, layer, isogdClass.FullName, id.ToString()))
            //    {
            //        spatial.GetLinkedMapInfoObject(m_MapInfo, layer, isogdClass.FullName, id.ToString());
            //        break;
            //    }
            //}

            System.Diagnostics.Process[] p = System.Diagnostics.Process.GetProcessesByName("MapInfow");
            if (p.Length > 0)
            {
                ShowWindow(p[0].MainWindowHandle, 10);
                ShowWindow(p[0].MainWindowHandle, 5);
                SetForegroundWindow(p[0].MainWindowHandle);
            }
        }

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr handle, int cmdShow);
        [DllImport("user32.dll")]
        private static extern int SetForegroundWindow(IntPtr handle);
    }
}
