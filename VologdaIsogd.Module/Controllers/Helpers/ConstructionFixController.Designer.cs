﻿namespace VologdaIsogd.Module.Controllers.Helpers
{
    partial class ConstructionFixController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConstructionFixController));
            this.FixAppartCountUsePermAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FixConstactionChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // FixAppartCountUsePermAction
            // 
            this.FixAppartCountUsePermAction.Caption = "Проставить кол-во квартир";
            this.FixAppartCountUsePermAction.ConfirmationMessage = null;
            this.FixAppartCountUsePermAction.Id = "FixAppartCountUsePermAction";
            this.FixAppartCountUsePermAction.TargetObjectType = typeof(AISOGD.SystemDir.SpatialConfig);
            this.FixAppartCountUsePermAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.FixAppartCountUsePermAction.ToolTip = null;
            this.FixAppartCountUsePermAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.FixAppartCountUsePermAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FixAppartCountUsePermAction_Execute);
            // 
            // FixConstactionChoiceAction
            // 
            this.FixConstactionChoiceAction.Caption = "Стройка";
            this.FixConstactionChoiceAction.ConfirmationMessage = null;
            this.FixConstactionChoiceAction.Id = "FixConstactionChoiceAction";
            this.FixConstactionChoiceAction.ToolTip = resources.GetString("FixConstactionChoiceAction.ToolTip");
            this.FixConstactionChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FixConstactionChoiceAction_Execute);
            // 
            // ConstructionFixController
            // 
            this.Actions.Add(this.FixAppartCountUsePermAction);
            this.Actions.Add(this.FixConstactionChoiceAction);
            this.TargetObjectType = typeof(AISOGD.SystemDir.SpatialConfig);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FixAppartCountUsePermAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FixConstactionChoiceAction;
    }
}
