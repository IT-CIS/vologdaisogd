﻿namespace VologdaIsogd.Module.Controllers.Helpers
{
    partial class FixEmployessInISOGDController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FixEmployessInISOGDAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // FixEmployessInISOGDAction
            // 
            this.FixEmployessInISOGDAction.Caption = "Исправить сотрудников в ИСОГД";
            this.FixEmployessInISOGDAction.ConfirmationMessage = null;
            this.FixEmployessInISOGDAction.Id = "FixEmployessInISOGDAction";
            this.FixEmployessInISOGDAction.TargetObjectType = typeof(AISOGD.SystemDir.SpatialConfig);
            this.FixEmployessInISOGDAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.FixEmployessInISOGDAction.ToolTip = null;
            this.FixEmployessInISOGDAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.FixEmployessInISOGDAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FixEmployessInISOGDAction_Execute);
            // 
            // FixEmployessInISOGDController
            // 
            this.Actions.Add(this.FixEmployessInISOGDAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FixEmployessInISOGDAction;
    }
}
