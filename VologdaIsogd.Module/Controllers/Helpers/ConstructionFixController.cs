﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using AISOGD.Perm;
using AISOGD.Constr;
using DevExpress.Xpo;
using AISOGD.DocFlow;
using DevExpress.XtraEditors;

namespace VologdaIsogd.Module.Controllers.Helpers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ConstructionFixController : ViewController
    {
        private ChoiceActionItem setConstrLastDateAction;
        private ChoiceActionItem fixBuildingKindAction;
        private ChoiceActionItem setConstrDocsDataFromConstrObjAction;
        private ChoiceActionItem setConstrDocInfoInConstrUseDocAction;
        private ChoiceActionItem setConstrConstructionCategoryAction;
        private ChoiceActionItem setConstrStagesSubjectsAction;

        public ConstructionFixController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            FixConstactionChoiceAction.Items.Clear();
            setConstrLastDateAction =
               new ChoiceActionItem("Проставить дату последнего РС у ОКС и этапов", null);
            FixConstactionChoiceAction.Items.Add(setConstrLastDateAction);

            fixBuildingKindAction =
               new ChoiceActionItem("Исправить типы объектов", null);
            FixConstactionChoiceAction.Items.Add(fixBuildingKindAction);

            setConstrDocsDataFromConstrObjAction =
               new ChoiceActionItem("Заполнить данные в РС и РВ из ОКС", null);
            FixConstactionChoiceAction.Items.Add(setConstrDocsDataFromConstrObjAction);

            setConstrDocInfoInConstrUseDocAction =
               new ChoiceActionItem("Заполнить реквизиты РС в карточке РВ", null);
            FixConstactionChoiceAction.Items.Add(setConstrDocInfoInConstrUseDocAction);

            setConstrConstructionCategoryAction =
               new ChoiceActionItem("Заполнить категории по стройке", null);
            FixConstactionChoiceAction.Items.Add(setConstrConstructionCategoryAction);

            setConstrStagesSubjectsAction =
               new ChoiceActionItem("Заполнить застройщиков по этапам", null);
            FixConstactionChoiceAction.Items.Add(setConstrStagesSubjectsAction);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// функция проставления количества квартир в РВ и ОКС по их этапам
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FixAppartCountUsePermAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            int i = 0;
            int d = 0;
            int s = 0;
            Connect connect = Connect.FromObjectSpace(os);

            var UsePermList = connect.FindObjects<UsePerm>(x => true);
            foreach (UsePerm usePerm in UsePermList)
            {
                usePerm.AppartmentsCountFakt = 0;
                try
                {
                    foreach (ConstrStage constrStage in usePerm.ConstrStages)
                    {
                        try { usePerm.AppartmentsCountFakt += constrStage.AppartmentsCountProject; }
                        catch { }
                    }
                    usePerm.Save();
                    os.CommitChanges();
                }
                catch { }
            }
            var ConstrObjList = connect.FindObjects<CapitalStructureBase>(x => true);
            foreach (CapitalStructureBase constrObj in ConstrObjList)
            {
                constrObj.AppartmentsCountFakt = 0;
                try
                {
                    foreach (ConstrStage constrStage in constrObj.ConstrStages)
                    {
                        try { constrObj.AppartmentsCountFakt += constrStage.AppartmentsCountProject; }
                        catch { }
                    }
                    constrObj.Save();
                    os.CommitChanges();
                }
                catch { }
            }
        }


        /// <summary>
        /// Действия по данному ChoiceAction:
        /// 1. Заполняет дату последнего РС у объектов и этапов
        /// 2. Исправляет виды объектов(жилой/не жилой) у объектов строительства и этапов по их типу(справочнику)
        /// 3. Заполняет данные в РС и РВ из объекта строительства:
        ///  3.1. Тип объекта(справочник)
        ///  3.2. Номер на карте
        /// 4. Заполняет реквизиты РС в карточке РВ
        private void FixConstactionChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            if (e.SelectedChoiceActionItem == setConstrLastDateAction)
            {
                SetConstrLastDate(os, connect);
            }
            if (e.SelectedChoiceActionItem == fixBuildingKindAction)
            {
                FixBuildingKind(os, connect);
            }
            if (e.SelectedChoiceActionItem == setConstrDocsDataFromConstrObjAction)
            {
                SetConstrDocsDataFromConstrObj(os, connect);
            }
            if (e.SelectedChoiceActionItem == setConstrDocInfoInConstrUseDocAction)
            {
                SetConstrDocInfoInConstrUseDoc(os, connect);
            }
            if (e.SelectedChoiceActionItem == setConstrConstructionCategoryAction)
            {
                SetConstrConstructionCategory(os, connect);
            }
            if (e.SelectedChoiceActionItem == setConstrStagesSubjectsAction)
            {
                SetConstrStagesSubjects(os, connect);
            }
        }

        /// <summary>
        /// Заполняет дату последнего РС у объектов и этапов
        /// </summary>
        /// <param name="connect"></param>
        private void SetConstrLastDate(IObjectSpace os, Connect connect)
        {
            // 1.
            var constrObjectsList = connect.FindObjects<CapitalStructureBase>(x => true);
            foreach (CapitalStructureBase constrObj in constrObjectsList)
            {
                SortProperty sort = new SortProperty("DocDate", DevExpress.Xpo.DB.SortingDirection.Descending);
                constrObj.generalDocBase.Sorting.Add(sort);
                foreach (GeneralDocBase doc in constrObj.generalDocBase)
                {
                    if (doc.ClassInfo.FullName == "AISOGD.Perm.ConstrPerm")
                    {
                        ConstrPerm perm = (ConstrPerm)doc;
                        if (perm.EDocStatus == AISOGD.Enums.eDocStatus.Утвержден)
                        {
                            constrObj.ConstrPermLastDate = perm.DocDate;
                            break;
                        }
                    }
                }
                constrObj.Save();
            }
            var constrStagesList = connect.FindObjects<ConstrStage>(x => true);
            foreach (ConstrStage constrStage in constrStagesList)
            {
                SortProperty sort = new SortProperty("DocDate", DevExpress.Xpo.DB.SortingDirection.Descending);
                constrStage.ConstrPerms.Sorting.Add(sort);
                foreach (ConstrPerm perm in constrStage.ConstrPerms)
                {
                    if (perm.EDocStatus == AISOGD.Enums.eDocStatus.Утвержден)
                    {
                        constrStage.ConstrPermLastDate = perm.DocDate;
                        break;
                    }
                }
                constrStage.Save();
            }
            os.CommitChanges();
            XtraMessageBox.Show("Заполнение дат последнего РС у объектов строительства и этапов завершено");
        }

        /// <summary>
        /// Исправить типы объектов
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void FixBuildingKind(IObjectSpace os, Connect connect)
        {
            var constrObjectsList = connect.FindObjects<CapitalStructureBase>(x => true);
            foreach (CapitalStructureBase constrObj in constrObjectsList)
            {
                //if (constrObj.BuildingKind == AISOGD.Enums.eBuildingKind.Жилой)
                //{
                try
                {
                    if (constrObj.ConstructionType.Name.ToLower() != "многоквартирные жилые дома" && constrObj.ConstructionType.Name.ToLower() != "многоквартирный жилой дом"
                        && constrObj.ConstructionType.Name.ToLower() != "индивидуальные жилые дома" && constrObj.ConstructionType.Name.ToLower() != "индивидуальный жилой дом"
                        && constrObj.ConstructionType.Name.ToLower() != "жилые комплексы" && constrObj.ConstructionType.Name.ToLower() != "жилой квартал"
                        && constrObj.ConstructionType.Name.ToLower() != "квартал индивидуальной жилой застройки" && constrObj.ConstructionType.Name.ToLower() != "комплекс жилой застройки с многофункциональным центром")
                    {
                        if (constrObj.ConstructionType.Name.ToLower().Contains("производст"))
                            constrObj.BuildingKind = AISOGD.Enums.eBuildingKind.ПроизводственногоНазначения;
                        else
                            constrObj.BuildingKind = AISOGD.Enums.eBuildingKind.Нежилой;
                    }
                    else
                        constrObj.BuildingKind = AISOGD.Enums.eBuildingKind.Жилой;
                    constrObj.Save();
                    os.CommitChanges();
                }
                catch { }
                //}
            }
            os.CommitChanges();
            var constrStagesList = connect.FindObjects<ConstrStage>(x => true);
            foreach (ConstrStage constrStage in constrStagesList)
            {
                //if (constrStage.BuildingKind == AISOGD.Enums.eBuildingKind.Жилой)
                //{
                    try
                    {
                        if (constrStage.ConstructionType.Name.ToLower() != "многоквартирные жилые дома" && constrStage.ConstructionType.Name.ToLower() != "многоквартирный жилой дом"
                            && constrStage.ConstructionType.Name.ToLower() != "индивидуальные жилые дома" && constrStage.ConstructionType.Name.ToLower() != "индивидуальный жилой дом"
                            && constrStage.ConstructionType.Name.ToLower() != "жилые комплексы" && constrStage.ConstructionType.Name.ToLower() != "жилой квартал"
                            && constrStage.ConstructionType.Name.ToLower() != "квартал индивидуальной жилой застройки" && constrStage.ConstructionType.Name.ToLower() != "комплекс жилой застройки с многофункциональным центром")
                        {
                            if (constrStage.ConstructionType.Name.ToLower().Contains("производст"))
                                constrStage.BuildingKind = AISOGD.Enums.eBuildingKind.ПроизводственногоНазначения;
                            else
                                constrStage.BuildingKind = AISOGD.Enums.eBuildingKind.Нежилой;
                        }
                        else
                            constrStage.BuildingKind = AISOGD.Enums.eBuildingKind.Жилой;
                    constrStage.Save();
                    os.CommitChanges();
                    }
                    catch { }
                //}
            }
            os.CommitChanges();

            var constrPermList = connect.FindObjects<ConstrPerm>(x => true);
            foreach (ConstrPerm constrPerm in constrPermList)
            {
                //if (constrStage.BuildingKind == AISOGD.Enums.eBuildingKind.Жилой)
                //{
                try
                {
                    if (constrPerm.ConstructionType.Name.ToLower() != "многоквартирные жилые дома" && constrPerm.ConstructionType.Name.ToLower() != "многоквартирный жилой дом"
                        && constrPerm.ConstructionType.Name.ToLower() != "индивидуальные жилые дома" && constrPerm.ConstructionType.Name.ToLower() != "индивидуальный жилой дом"
                        && constrPerm.ConstructionType.Name.ToLower() != "жилые комплексы" && constrPerm.ConstructionType.Name.ToLower() != "жилой квартал"
                        && constrPerm.ConstructionType.Name.ToLower() != "квартал индивидуальной жилой застройки" && constrPerm.ConstructionType.Name.ToLower() != "комплекс жилой застройки с многофункциональным центром")
                    {
                        if (constrPerm.ConstructionType.Name.ToLower().Contains("производст"))
                            constrPerm.BuildingKind = AISOGD.Enums.eBuildingKind.ПроизводственногоНазначения;
                        else
                            constrPerm.BuildingKind = AISOGD.Enums.eBuildingKind.Нежилой;
                    }
                    else
                        constrPerm.BuildingKind = AISOGD.Enums.eBuildingKind.Жилой;
                    constrPerm.Save();
                    os.CommitChanges();
                }
                catch { }
                //}
            }
            os.CommitChanges();
            var usePermList = connect.FindObjects<UsePerm>(x => true);
            foreach (UsePerm usePerm in usePermList)
            {
                //if (constrStage.BuildingKind == AISOGD.Enums.eBuildingKind.Жилой)
                //{
                try
                {
                    if (usePerm.ConstructionType.Name.ToLower() != "многоквартирные жилые дома" && usePerm.ConstructionType.Name.ToLower() != "многоквартирный жилой дом"
                        && usePerm.ConstructionType.Name.ToLower() != "индивидуальные жилые дома" && usePerm.ConstructionType.Name.ToLower() != "индивидуальный жилой дом"
                        && usePerm.ConstructionType.Name.ToLower() != "жилые комплексы" && usePerm.ConstructionType.Name.ToLower() != "жилой квартал"
                        && usePerm.ConstructionType.Name.ToLower() != "квартал индивидуальной жилой застройки" && usePerm.ConstructionType.Name.ToLower() != "комплекс жилой застройки с многофункциональным центром")
                    {
                        if (usePerm.ConstructionType.Name.ToLower().Contains("производст"))
                            usePerm.BuildingKind = AISOGD.Enums.eBuildingKind.ПроизводственногоНазначения;
                        else
                            usePerm.BuildingKind = AISOGD.Enums.eBuildingKind.Нежилой;
                    }
                    else
                        usePerm.BuildingKind = AISOGD.Enums.eBuildingKind.Жилой;
                    usePerm.Save();
                    os.CommitChanges();
                }
                catch { }
                //}
            }
            os.CommitChanges();
            XtraMessageBox.Show("Исправления типов объектов строительства и этапов завершено");
        }

        /// <summary>
        ///  3. Заполняет данные в РС и РВ из объекта строительства:
        ///  3.1. Тип объекта(справочник)
        ///  3.2. Номер на карте
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void SetConstrDocsDataFromConstrObj(IObjectSpace os, Connect connect)
        {
            var constrObjectsList = connect.FindObjects<CapitalStructureBase>(x => true);
            foreach (CapitalStructureBase constrObj in constrObjectsList)
            {
                foreach (GeneralDocBase doc in constrObj.generalDocBase)
                {
                    if (doc.ClassInfo.FullName == "AISOGD.Perm.ConstrPerm")
                    {
                        ConstrPerm perm = (ConstrPerm)doc;
                        perm.ConstructionType = constrObj.ConstructionType;
                        perm.MapNo = constrObj.MapNo;
                        perm.Save();
                    }
                    if (doc.ClassInfo.FullName == "AISOGD.Perm.UsePerm")
                    {
                        UsePerm perm = (UsePerm)doc;
                        perm.ConstructionType = constrObj.ConstructionType;
                        perm.MapNo = constrObj.MapNo;
                        perm.Save();
                    }
                }
            }
            os.CommitChanges();
            XtraMessageBox.Show("Заполнение данных в РС и РВ из объекта строительства завершено");
        }

        /// <summary>
        /// 4. Заполняет реквизиты РС в карточке РВ
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void SetConstrDocInfoInConstrUseDoc(IObjectSpace os, Connect connect)
        {
            var usePermList = connect.FindObjects<UsePerm>(x => true);
            foreach (UsePerm usePerm in usePermList)
            {
                foreach (ConstrPerm constrPerm in usePerm.ConstrPerm)
                {
                    if (usePerm.ConstrPermInfo == null)
                        usePerm.ConstrPermInfo = "";
                    string docInfo = "";
                    docInfo = String.Format($"от {constrPerm.DocDate.ToShortDateString()} № {constrPerm.DocNo}");
                    if (!usePerm.ConstrPermInfo.Contains(docInfo))
                        usePerm.ConstrPermInfo += docInfo + "; ";
                    usePerm.ConstrPermInfo = usePerm.ConstrPermInfo.Trim();
                    usePerm.ConstrPermInfo = usePerm.ConstrPermInfo.TrimEnd(';');
                    usePerm.Save();
                }
            }
            os.CommitChanges();
            XtraMessageBox.Show("Заполнение реквизитов РС в карточке РВ завершено");
        }

        
        /// <summary>
        /// Проставить категории у объектов, этапов, РС и РВ
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void SetConstrConstructionCategory(IObjectSpace os, Connect connect)
        {
            var constrObjectsList = connect.FindObjects<CapitalStructureBase>(x => true);
            foreach (CapitalStructureBase constrObj in constrObjectsList)
            {
                try
                {
                    if (constrObj.ConstructionType.ParentConstructionUsing != null)
                    {
                        if (constrObj.ConstructionType.ConstructionTypes.Count == 0)
                            constrObj.ConstructionCategory = constrObj.ConstructionType.ParentConstructionUsing;
                        else
                            constrObj.ConstructionCategory = constrObj.ConstructionType;
                        constrObj.Save();
                        os.CommitChanges();
                    }
                }
                catch { }
            }
            os.CommitChanges();
            var constrStagesList = connect.FindObjects<ConstrStage>(x => true);
            foreach (ConstrStage constrStage in constrStagesList)
            {
                try
                {
                    if (constrStage.ConstructionType.ParentConstructionUsing != null)
                    {
                        if (constrStage.ConstructionType.ConstructionTypes.Count == 0)
                            constrStage.ConstructionCategory = constrStage.ConstructionType.ParentConstructionUsing;
                        else
                            constrStage.ConstructionCategory = constrStage.ConstructionType;
                        constrStage.Save();
                        os.CommitChanges();
                    }
                }
                catch { }
            }
            os.CommitChanges();

            var constrPermList = connect.FindObjects<ConstrPerm>(x => true);
            foreach (ConstrPerm constrPerm in constrPermList)
            {
                try
                {
                    if (constrPerm.ConstructionType.ParentConstructionUsing != null)
                    {
                        if (constrPerm.ConstructionType.ConstructionTypes.Count == 0)
                            constrPerm.ConstructionCategory = constrPerm.ConstructionType.ParentConstructionUsing;
                        else
                            constrPerm.ConstructionCategory = constrPerm.ConstructionType;
                        constrPerm.Save();
                        os.CommitChanges();
                    }
                }
                catch { }
            }
            os.CommitChanges();
            var usePermList = connect.FindObjects<UsePerm>(x => true);
            foreach (UsePerm usePerm in usePermList)
            {
                try
                {
                    if (usePerm.ConstructionType.ParentConstructionUsing != null)
                    {
                        if (usePerm.ConstructionType.ConstructionTypes.Count == 0)
                            usePerm.ConstructionCategory = usePerm.ConstructionType.ParentConstructionUsing;
                        else
                            usePerm.ConstructionCategory = usePerm.ConstructionType;
                        usePerm.Save();
                        os.CommitChanges();
                    }
                }
                catch { }
            }
            os.CommitChanges();
            XtraMessageBox.Show("Заполнение категорий объектов строительства, этапов, РС и РВ завершено");
        }


        /// <summary>
        /// Заполнение полей застройщика в этапах из XPCollection
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        private void SetConstrStagesSubjects(IObjectSpace os, Connect connect)
        {
            var constrObjectsList = connect.FindObjects<CapitalStructureBase>(x => true);
            foreach (CapitalStructureBase constrObj in constrObjectsList)
            {
                try
                {
                    constrObj.SetDevelopers();
                    constrObj.Save();
                    os.CommitChanges();
                }
                catch { }
            }

                var constrStagesList = connect.FindObjects<ConstrStage>(x => true);
            foreach (ConstrStage constrStage in constrStagesList)
            {
                try
                {
                    constrStage.SetDevelopers();
                    constrStage.Save();
                    os.CommitChanges();
                }
                catch { }
            }
            os.CommitChanges();
            XtraMessageBox.Show("Заполнение застройщиков по объектам строительства и этапам завершено");
        }
    }
}
