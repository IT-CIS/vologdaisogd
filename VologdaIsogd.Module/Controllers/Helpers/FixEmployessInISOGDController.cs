﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using AISOGD.Isogd;
using AISOGD.OrgStructure;
using DevExpress.XtraEditors;

namespace VologdaIsogd.Module.Controllers.Helpers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FixEmployessInISOGDController : ViewController
    {
        public FixEmployessInISOGDController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        /// <summary>
        /// Исправление сотрудников реестров ИСОГД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FixEmployessInISOGDAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            int i = 0;
            int d = 0;
            int s = 0;
            Connect connect = Connect.FromObjectSpace(os);

            Employee empl = connect.FindFirstObject<Employee>(x => x.UserName == "Кузнецова Н.Г.");
            //var IsogdPhysStorageBooks = connect.FindObjects<IsogdPhysStorageBook>(x => true).
            //    Select<IsogdPhysStorageBook>(mc => new int { bookNo = Convert.ToInt16(mc.BookNo) });// >= 14000 && Convert.ToInt16(x.BookNo) <=14500);

            var IsogdPhysStorageBooks = os.GetObjects<IsogdPhysStorageBook>(CriteriaOperator.Parse("BookNo >= '14000' and BookNo <='14500'"));
            foreach (IsogdPhysStorageBook IsogdPhysStorageBook in IsogdPhysStorageBooks)
            {
                i++;
                IsogdPhysStorageBook.Empl = empl;
                IsogdPhysStorageBook.Save();
                foreach (IsogdDocument IsogdDocument in IsogdPhysStorageBook.IsogdDocuments)
                {
                    d++;
                    IsogdDocument.Empl = empl;
                    IsogdDocument.Save();
                    os.CommitChanges();
                }
                foreach (IsogdStorageBook IsogdStorageBook in IsogdPhysStorageBook.IsogdStorageBooks)
                {
                    s++;
                    IsogdStorageBook.Empl = empl;
                    IsogdStorageBook.Save();
                    os.CommitChanges();
                }
                os.CommitChanges();
            }

            var IsogdPhysStorageBooks1 = os.GetObjects<IsogdPhysStorageBook>(CriteriaOperator.Parse("BookNo >= '15000' and BookNo <='15500'"));
            foreach (IsogdPhysStorageBook IsogdPhysStorageBook in IsogdPhysStorageBooks1)
            {
                i++;
                IsogdPhysStorageBook.Empl = empl;
                IsogdPhysStorageBook.Save();
                foreach (IsogdDocument IsogdDocument in IsogdPhysStorageBook.IsogdDocuments)
                {
                    d++;
                    IsogdDocument.Empl = empl;
                    IsogdDocument.Save();
                    os.CommitChanges();
                }
                foreach (IsogdStorageBook IsogdStorageBook in IsogdPhysStorageBook.IsogdStorageBooks)
                {
                    s++;
                    IsogdStorageBook.Empl = empl;
                    IsogdStorageBook.Save();
                    os.CommitChanges();
                }
                os.CommitChanges();
            }

            var IsogdPhysStorageBooks2 = os.GetObjects<IsogdPhysStorageBook>(CriteriaOperator.Parse("BookNo >= '16000' and BookNo <='16500'"));
            foreach (IsogdPhysStorageBook IsogdPhysStorageBook in IsogdPhysStorageBooks2)
            {
                i++;
                IsogdPhysStorageBook.Empl = empl;
                IsogdPhysStorageBook.Save();
                foreach (IsogdDocument IsogdDocument in IsogdPhysStorageBook.IsogdDocuments)
                {
                    d++;
                    IsogdDocument.Empl = empl;
                    IsogdDocument.Save();
                    os.CommitChanges();
                }
                foreach (IsogdStorageBook IsogdStorageBook in IsogdPhysStorageBook.IsogdStorageBooks)
                {
                    s++;
                    IsogdStorageBook.Empl = empl;
                    IsogdStorageBook.Save();
                    os.CommitChanges();
                }
                os.CommitChanges();
            }

            #region старые исправления
            /*
            Employee empl = connect.FindFirstObject<Employee>(x => x.UserName == "Лапина Н.А.");
            if (empl != null)
            {
                IQueryable IsogdDocuments = connect.FindObjects<IsogdDocument>(x => x.PlacementDate.Year < 2012 &&  x.Empl.UserName == "Себякина Е.А.");
                foreach (IsogdDocument IsogdDocument in IsogdDocuments)
                {
                    i++;
                    IsogdDocument.Empl = empl;
                    IsogdDocument.Save();
                    os.CommitChanges();
                }
                os.CommitChanges();
                IQueryable IsogdStorageBooks = connect.FindObjects<IsogdStorageBook>(x => x.OpenenigBookDate.Year < 2012 && x.Empl.UserName == "Себякина Е.А.");
                foreach (IsogdStorageBook IsogdStorageBook in IsogdStorageBooks)
                {
                    i++;
                    IsogdStorageBook.Empl = empl;
                    IsogdStorageBook.Save();
                    os.CommitChanges();
                }
                IQueryable IsogdStorageBooks1 = connect.FindObjects<IsogdStorageBook>(x => x.OpenenigBookDate.Year < 2012 && x.Empl == null);
                foreach (IsogdStorageBook IsogdStorageBook in IsogdStorageBooks1)
                {
                    i++;
                    IsogdStorageBook.Empl = empl;
                    IsogdStorageBook.Save();
                    os.CommitChanges();
                }
                os.CommitChanges();
                IQueryable IsogdPhysStorageBooks = connect.FindObjects<IsogdPhysStorageBook>(x => x.OpenenigBookDate.Year < 2012 && x.Empl.UserName == "Себякина Е.А.");
                foreach (IsogdPhysStorageBook IsogdPhysStorageBook in IsogdPhysStorageBooks)
                {
                    i++;
                    IsogdPhysStorageBook.Empl = empl;
                    IsogdPhysStorageBook.Save();
                    os.CommitChanges();
                }
                IQueryable IsogdPhysStorageBooks1 = connect.FindObjects<IsogdPhysStorageBook>(x => x.OpenenigBookDate.Year < 2012 && x.Empl == null);
                foreach (IsogdPhysStorageBook IsogdPhysStorageBook in IsogdPhysStorageBooks)
                {
                    i++;
                    IsogdPhysStorageBook.Empl = empl;
                    IsogdPhysStorageBook.Save();
                    os.CommitChanges();
                }
                os.CommitChanges();
            }
            */
            #endregion
            XtraMessageBox.Show(String.Format("Обработка завершена. Изменено {0} ДЗУ, {1} Документов, {2} КХ.", i.ToString(), d.ToString(), s.ToString()));
            os.CommitChanges();
        }
    }
}
