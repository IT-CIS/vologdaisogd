﻿namespace VologdaIsogd.Module.Controllers.Helpers
{
    partial class FillReestrFromGISController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FillReestrFromGISAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CreateParcelsFromGISAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.LinkGISConstrStagesAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.GISInteractionChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // FillReestrFromGISAction
            // 
            this.FillReestrFromGISAction.Caption = "Заполнить реестры";
            this.FillReestrFromGISAction.ConfirmationMessage = null;
            this.FillReestrFromGISAction.Id = "FillReestrFromGISAction";
            this.FillReestrFromGISAction.TargetObjectType = typeof(AISOGD.SystemDir.SpatialConfig);
            this.FillReestrFromGISAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.FillReestrFromGISAction.ToolTip = null;
            this.FillReestrFromGISAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.FillReestrFromGISAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FillReestrFromGISAction_Execute);
            // 
            // CreateParcelsFromGISAction
            // 
            this.CreateParcelsFromGISAction.Caption = "Создать участки из ГИС";
            this.CreateParcelsFromGISAction.ConfirmationMessage = null;
            this.CreateParcelsFromGISAction.Id = "CreateParcelsFromGISAction";
            this.CreateParcelsFromGISAction.ToolTip = null;
            this.CreateParcelsFromGISAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateParcelsFromGISAction_Execute);
            // 
            // LinkGISConstrStagesAction
            // 
            this.LinkGISConstrStagesAction.Caption = "Связать объекты строительства";
            this.LinkGISConstrStagesAction.ConfirmationMessage = null;
            this.LinkGISConstrStagesAction.Id = "LinkGISConstrStagesAction";
            this.LinkGISConstrStagesAction.ToolTip = null;
            this.LinkGISConstrStagesAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.LinkGISConstrStagesAction_Execute);
            // 
            // GISInteractionChoiceAction
            // 
            this.GISInteractionChoiceAction.Caption = "Связь с картой";
            this.GISInteractionChoiceAction.ConfirmationMessage = null;
            this.GISInteractionChoiceAction.Id = "GISInteractionChoiceAction";
            this.GISInteractionChoiceAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.GISInteractionChoiceAction.ToolTip = null;
            this.GISInteractionChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.GISInteractionChoiceAction_Execute);
            // 
            // FillReestrFromGISController
            // 
            this.Actions.Add(this.FillReestrFromGISAction);
            this.Actions.Add(this.CreateParcelsFromGISAction);
            this.Actions.Add(this.LinkGISConstrStagesAction);
            this.Actions.Add(this.GISInteractionChoiceAction);
            this.TargetObjectType = typeof(AISOGD.SystemDir.SpatialConfig);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FillReestrFromGISAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreateParcelsFromGISAction;
        private DevExpress.ExpressApp.Actions.SimpleAction LinkGISConstrStagesAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction GISInteractionChoiceAction;
    }
}
