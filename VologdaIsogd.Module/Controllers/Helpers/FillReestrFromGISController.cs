﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Reglament;
using AISOGD.OrgStructure;
using AISOGD.Land;
using AISOGD.Constr;
using AISOGD.Isogd;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.Helpers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FillReestrFromGISController : ViewController
    {
        private ChoiceActionItem setIsogdStorageBooksMapInfoLinksAction;
        private ChoiceActionItem createTerrZonesMapObjectsAction;
        private ChoiceActionItem createSpecialZonesMapObjectsAction;

        public FillReestrFromGISController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            GISInteractionChoiceAction.Items.Clear();
            setIsogdStorageBooksMapInfoLinksAction =
               new ChoiceActionItem("Связать КХ с картой", null);
            GISInteractionChoiceAction.Items.Add(setIsogdStorageBooksMapInfoLinksAction);

            createTerrZonesMapObjectsAction =
               new ChoiceActionItem("Создать терр.зоны", null);
            GISInteractionChoiceAction.Items.Add(createTerrZonesMapObjectsAction);

            createSpecialZonesMapObjectsAction =
              new ChoiceActionItem("Создать спец.зоны", null);
            GISInteractionChoiceAction.Items.Add(createSpecialZonesMapObjectsAction);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Действия по данному ChoiceAction:
        /// 1. Связывает реестровые и ГИС данные по всем книгам хранения (сначала удаляя связки)
        /// 2. 
        /// 3. 
        /// 4. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GISInteractionChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo = new MapInfoApplication();
            if (e.SelectedChoiceActionItem == setIsogdStorageBooksMapInfoLinksAction)
            {
                setIsogdStorageBooksMapInfoLinks(m_MapInfo, connect, os);
            }
            if (e.SelectedChoiceActionItem == createTerrZonesMapObjectsAction)
            {
                CreateTerrZones(m_MapInfo, connect, os);
            }
            if (e.SelectedChoiceActionItem == createSpecialZonesMapObjectsAction)
            {
                CreateSpecialsZones(m_MapInfo, connect, os);
            }
        }

        /// <summary>
        /// Связывает реестровые и ГИС данные по всем книгам хранения (сначала удаляя связки)
        /// </summary>
        /// <param name="m_MapInfo"></param>
        /// <param name="connect"></param>
        /// <param name="os"></param>
        private void setIsogdStorageBooksMapInfoLinks(MapInfoApplication m_MapInfo, Connect connect, IObjectSpace os)
        {
            if (m_MapInfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено");
                return;
            }

            string xafClassName = "AISOGD.Isogd.IsogdStorageBook";
            int i = 0;
            var spatial = new SpatialController<GisLayer>(connect);
            SpatialConfig spatialConfig = null;
            int j = 0;
            int d = 0;
            string spatialLayer = "";

            // 1. Удаляем все существующие связки реестров с ГИС объектами
            List<SpatialRepository> SpatialRepositoryList = connect.FindObjects<SpatialRepository>(x => x.IsogdClassName == xafClassName).ToList<SpatialRepository>();
            foreach(SpatialRepository sr in SpatialRepositoryList)
            {

                //SpatialRepositoryList.Remove(sr);
                sr.Delete();
                
                d++;
            }
            os.CommitChanges();
            // 2. Создаем связки
            // 2.1 Получаем настройки связи для указанного класса 
            spatialConfig = connect.FindFirstObject<SpatialConfig>(mc => mc.IsogdClassName == connect.GetClassInfo(xafClassName).ClassType);
            if (spatialConfig != null)
            {
                // 2.1 Получаем ГИС слои из настроек связи
                foreach (SpatialLayerItem spatialLayerItem in spatialConfig.Layers)
                {
                    if (spatialLayerItem != null)
                        if (spatialLayerItem.SpatialLayer != null)
                            if (spatialLayerItem.SpatialLayer.LayerId != null && spatialLayerItem.SpatialLayer.LayerId != String.Empty)
                                spatialLayer = spatialLayerItem.SpatialLayer.LayerId;
                    if (spatialLayer != "")
                    {
                        
                        // 2.2 Перебираем книги хранения и смотрим у них кад номер
                        foreach (IsogdStorageBook isogdObj in connect.FindObjects<IsogdStorageBook>(x => !String.IsNullOrEmpty(x.Code)))
                        {
                            j++;
                            //CopySurveyFromExcelConsole.SystemConsole.drawTextProgressBar(j, totalcount);
                            try
                            {
                                string qw = "";
                                // 2.3  Ищем 
                                // если книга хранения ППТ, то ищем по ид импорта из Инметы
                                if (spatialLayer.Contains("ППТ"))
                                {
                                    if(!String.IsNullOrEmpty(isogdObj.ImportCode))
                                        qw = "Select MI_PRINX From " + spatialLayer + " Where INMETANO = \"" + isogdObj.ImportCode + "\" Into tmpTable";
                                    //else
                                    //    qw = "Select MI_PRINX From " + spatialLayer + " Where КНИГА_ХРАНЕНИЯ_ППТ = \"" + isogdObj.Code + "\" Into tmpTable";
                                }
                                else
                                    qw = "Select MI_PRINX From " + spatialLayer + " Where КАДАСТРОВЫЙ_НОМЕР = \"" + isogdObj.Code + "\" Into tmpTable";
                                m_MapInfo.Do(qw);
                                string gisObjId = m_MapInfo.Eval("tmpTable.MI_PRINX");
                                if (gisObjId != null && gisObjId != String.Empty && gisObjId != "0")
                                {
                                    if (!spatial.IsSpatialObjectLinkExist(spatialLayer, gisObjId))
                                    {
                                        spatial.AddSpatialLink(xafClassName,
                                            isogdObj.Oid.ToString(),
                                                spatialLayer,
                                                gisObjId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI);
                                        i++;
                                        
                                    }

                                }
                                m_MapInfo.Do("Drop Table tmpTable");
                            }
                            catch { }
                        }
                    }
                    spatialLayer = "";
                }
            }
            os.CommitChanges();
            XtraMessageBox.Show(String.Format($"Удалено {d} связей, создано {i}, не создано {j-i}"));
        }

        private void FillReestrFromGISAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo = new MapInfoApplication();
            SetSurveysMapInfoLinks(m_MapInfo, connect);
            //CreateTerrZones(m_MapInfo, connect);
            //CreateSpecialsZones(m_MapInfo, connect);
        }

        /// <summary>
        ///  связываем изыскания с картой
        /// </summary>
        /// <param name="connect"></param>
        private void SetSurveysMapInfoLinks(MapInfoApplication m_MapInfo, Connect connect)
        {

            if (m_MapInfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено");
                return;
            }

            int i = 0;
            var spatial = new SpatialController<GisLayer>(connect);
            SpatialConfig spatialConfig = null;
            int j = 0;
            string xafClassName = "";
            string spatialLayer = "";
            // 1. Перебираем все настройки связи SpatialConfig для классов Survey
            // 1.1
            xafClassName = "AISOGD.Surveys.Survey";
            spatialConfig = connect.FindFirstObject<SpatialConfig>(mc => mc.IsogdClassName == connect.GetClassInfo(xafClassName).ClassType);
            if (spatialConfig != null)
            {
                foreach (SpatialLayerItem spatialLayerItem in spatialConfig.Layers)
                {
                    if (spatialLayerItem != null)
                        if (spatialLayerItem.SpatialLayer != null)
                            if (spatialLayerItem.SpatialLayer.LayerId != null && spatialLayerItem.SpatialLayer.LayerId != String.Empty)
                                spatialLayer = spatialLayerItem.SpatialLayer.LayerId;
                    if (spatialLayer != "")
                    {
                        Console.WriteLine("Перебираем объекты слоя: " + spatialLayer);
                        Console.ReadLine();
                        j = 0;
                        //totalcount = connect.FindObjects<Survey>(mc => mc.ArhNo != null && mc.ArhNo != "").Count<Survey>();
                        foreach (AISOGD.Surveys.Survey isogdObj in connect.FindObjects<AISOGD.Surveys.Survey>(mc => true))
                        {
                            j++;
                            //CopySurveyFromExcelConsole.SystemConsole.drawTextProgressBar(j, totalcount);
                            try
                            {
                                m_MapInfo.Do("Select MI_PRINX From " + spatialLayer + " Where АРХ_НОМЕР = " + isogdObj.ArhNo + " Into tmpTable");
                                //Console.WriteLine(m_MapInfo.Eval("SelectionInfo(3)"));
                                string gisObjId = m_MapInfo.Eval("tmpTable.MI_PRINX");
                                //Console.WriteLine(String.Format("gisObjId = {0}, АРХ_НОМЕР = {1}", gisObjId, isogdObj.ArhNo));
                                if (gisObjId != null && gisObjId != String.Empty && gisObjId != "0")
                                {
                                    if (!spatial.IsSpatialObjectLinkExist(spatialLayer, gisObjId))
                                    {
                                        spatial.AddSpatialLink(xafClassName,
                                            isogdObj.Oid.ToString(),
                                                spatialLayer,
                                                gisObjId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI);
                                        i++;
                                        //Console.WriteLine(String.Format("Обработали {0} объект класса {1} в слое {2}. MI_PRINX: {3}", i.ToString(), xafClassName, spatialLayer, gisObjId));
                                    }

                                }
                                m_MapInfo.Do("Drop Table tmpTable");
                            }
                            catch { }
                            //Console.WriteLine(String.Format("j: {0}", j.ToString()));
                        }
                        //connect.GetUnitOfWork().CommitChanges();
                    }
                    spatialLayer = "";
                    //Console.WriteLine(String.Format("Создано связей {0}, не создано {1}", i.ToString(), (j - i).ToString()));
                }
            }

            //connect.GetUnitOfWork().CommitChanges();
            //Console.WriteLine(String.Format("Создано связей {0}, не создано {1}", i.ToString(), (j - i).ToString()));
            XtraMessageBox.Show(String.Format("Создано связей {0}, не создано {1}", i.ToString(), (j - i).ToString()));
        }

        public void CreateTerrZones(MapInfoApplication m_MapInfo, Connect connect, IObjectSpace os)
        {

            var spatial = new SpatialController<GisLayer>(connect);

            //Type mapinfotype = Type.GetTypeFromProgID("Mapinfo.Application");
            //m_MapInfo = (MapInfoApplication)Activator.CreateInstance(mapinfotype);

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено");
                return;
            }
            var layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.TerrZone");
            int totalCount = 0;
            int j = 0;
            foreach (string layer in layers)
            {
                //Console.WriteLine(String.Format("Слой: {0}", layer));
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    totalCount++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneName = m_MapInfo.Eval("Selection.ИНТЕГРАЛЬНАЯ_ЗОНА");
                                if (zoneName != null || zoneName != "")
                                    if(zoneName.Length < 255)
                                {
                                    // создаем террзону 
                                    TerrZone terrZone = connect.CreateObject<TerrZone>();
                                    terrZone.NameSmall = zoneName;
                                    TerrZoneKind terrZoneKind = connect.FindFirstObject<TerrZoneKind>(mc => mc.NameSmall == zoneName);
                                    if (terrZoneKind != null)
                                    {
                                        terrZone.TerrZoneKind = terrZoneKind;
                                        terrZone.Name = terrZoneKind.Name;
                                    }
                                    try { terrZone.ZoneNo = m_MapInfo.Eval("Selection.НОМЕР"); }
                                    catch { }
                                    try { terrZone.YearChange = Convert.ToInt16(m_MapInfo.Eval("Selection.ГОД_ИЗМ")); }
                                    catch { }
                                    try { terrZone.OldZone = m_MapInfo.Eval("Selection.БЫВШАЯ_ЗОНА"); }
                                    catch { }
                                    try { terrZone.TerrZoneDoc = m_MapInfo.Eval("Selection.ДОКУМЕНТ"); }
                                    catch { }
                                    terrZone.Save();
                                    spatial.AddSpatialLink(terrZone.ClassInfo.FullName, terrZone.Oid.ToString(), layer, gisObjId);
                                    //Console.WriteLine(String.Format("Создана {0} территориальная зона: {1}", i.ToString(), zoneName));
                                    j++;
                                }
                            }
                        }
                    }
                    catch { }
                }
                os.CommitChanges();
            }
            XtraMessageBox.Show(String.Format($"Создано {j.ToString()} территориальных зон, не создано {(totalCount-j)}"));
        }

        public static void CreateSpecialsZones(MapInfoApplication m_MapInfo, Connect connect, IObjectSpace os)
        {

            var spatial = new SpatialController<GisLayer>(connect);

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено");
                return;
            }
            var layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.MonumentZone");
            int m = 0;
            int toatalCountM = 0;
            foreach (string layer in layers)
            {
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    toatalCountM++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneCode = m_MapInfo.Eval("Selection.КОД_ЗОНА");
                                if (zoneCode != null || zoneCode != "")
                                {
                                    // создаем террзону 
                                    MonumentZone zone = connect.CreateObject<MonumentZone>();
                                    zone.ZoneCode = zoneCode;
                                    MonumentZoneKind zoneKind = connect.FindFirstObject<MonumentZoneKind>(mc => mc.NameSmall == zoneCode);
                                    if (zoneKind != null)
                                    {
                                        zone.MonumentZoneKind = zoneKind;
                                        zone.Name = zoneKind.Name;
                                    }
                                    try { zone.ZoneNo = m_MapInfo.Eval("Selection.MI_PRINX"); }
                                    catch { }
                                    try { zone.MonumentNumbers = m_MapInfo.Eval("Selection.Номера_ПАМ"); }
                                    catch { }
                                    try { zone.Kvartal = m_MapInfo.Eval("Selection.Квартал"); }
                                    catch { }
                                    try { zone.Square = m_MapInfo.Eval("Selection.Площадь_факт"); }
                                    catch { }
                                    try { zone.Note = m_MapInfo.Eval("Selection.примечание"); }
                                    catch { }
                                    zone.Save();
                                    spatial.AddSpatialLink(zone.ClassInfo.FullName, zone.Oid.ToString(), layer, gisObjId, WinAisogdMapInfo.Module.SystemDir.Spatial.Application.GISApplicationType.MI);
                                    //Console.WriteLine(String.Format("Создана {0} зона ОКН: {1}", i.ToString(), zoneCode));
                                    m++;
                                }
                            }
                        }
                    }
                    catch { }
                }
            }

            int e = 0;
            int toatalCountE = 0;
            layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.EngeneeringZone");
            foreach (string layer in layers)
            {
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    toatalCountE++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsSpatialObjectLinkExist(layer, gisObjId))
                            {
                                // создаем террзону 
                                EngeneeringZone zone = connect.CreateObject<EngeneeringZone>();
                                SpecialZoneKind zoneKind = null;
                                try
                                {
                                    zone.Name = layer;//m_MapInfo.Eval("Selection.НОМЕР_РЕЕСТР");
                                }
                                catch { }
                                try
                                {
                                    zone.ZoneNo = m_MapInfo.Eval("Selection.Номер_ИС");
                                }
                                catch { }
                                e++;
                            }
                        }
                    }
                    catch { }
                }
            }
            int a = 0;
            int toatalCountA = 0;
            layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.ArtWellZone");
            foreach (string layer in layers)
            {
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    toatalCountA++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneNo = "";
                                // создаем террзону 
                                ArtWellZone zone = connect.CreateObject<ArtWellZone>();
                                SpecialZoneKind zoneKind = null;
                                try
                                {
                                    zone.WellZoneNo = m_MapInfo.Eval("Selection.ЗСО");
                                    zoneNo = m_MapInfo.Eval("Selection.ЗСО");
                                }
                                catch { }
                                if (zoneNo != "")
                                {
                                    foreach (string z in zoneNo.Split(','))
                                    {
                                        if (zoneNo == "1")
                                            zoneKind = connect.FindFirstObject<SpecialZoneKind>(mc => mc.Name ==
                                            "Зона санитарной охраны источников питьевого и хозяйственно-бытового водоснабжения - Пояс I");
                                        if (zoneNo == "2")
                                            zoneKind = connect.FindFirstObject<SpecialZoneKind>(mc => mc.Name ==
                                            "Зона санитарной охраны источников питьевого и хозяйственно-бытового водоснабжения - Пояс II");
                                        if (zoneNo == "3")
                                            zoneKind = connect.FindFirstObject<SpecialZoneKind>(mc => mc.Name ==
                                            "Зона санитарной охраны источников питьевого и хозяйственно-бытового водоснабжения - Пояс III");
                                        zone.SpecialZoneKinds.Add(zoneKind);
                                    }
                                }

                                //if (zoneKind != null)
                                //{
                                //    zone.SpecialZoneKind = zoneKind;
                                //    zone.Name = zoneKind.Name;

                                //}
                                //try { zone.WellZoneNo = m_MapInfo.Eval("Selection.АРТСКВАЖИНА"); }
                                //catch { }
                                try { zone.ArtWell = m_MapInfo.Eval("Selection.АРТСКВАЖИНА"); }
                                catch { }
                                try { zone.Name = m_MapInfo.Eval("Selection.НАЗНАЧЕНИЕ_ЗСО"); }
                                catch { }
                                try { zone.ZSOParam = m_MapInfo.Eval("Selection.ПАРАМЕТРЫ_ЗСО"); }
                                catch { }
                                try { zone.InfoSource = m_MapInfo.Eval("Selection.ИСТОЧНИК_ИНФ"); }
                                catch { }
                                try { zone.Developer = m_MapInfo.Eval("Selection.РАЗРАБОТЧИК_ПРОЕКТА"); }
                                catch { }
                                try { zone.ZSODoc = m_MapInfo.Eval("Selection.ДОКУМЕНТ"); }
                                catch { }
                                try
                                {
                                    string date = m_MapInfo.Eval("Selection.ДАТА_ВНЕС");
                                    date = date.Insert(4, ".").Insert(7, ".");
                                    DateTime insertdate;
                                    DateTime.TryParse((string)date, out insertdate);
                                    zone.RegDate = insertdate;
                                }
                                catch { }
                                try
                                {
                                    if (m_MapInfo.Eval("Selection.ВНЕС_ИНФОРМ") != "")
                                    {
                                        string em = "";
                                        if (m_MapInfo.Eval("Selection.ВНЕС_ИНФОРМ") == "КОА")
                                            em = "Колобова О.А.";
                                        if ((m_MapInfo.Eval("Selection.ВНЕС_ИНФОРМ")).Contains("Румакова"))
                                            em = "Румакова Е.В.";
                                        if (m_MapInfo.Eval("Selection.ВНЕС_ИНФОРМ") == "Пикалева" || m_MapInfo.Eval("Selection.ВНЕС_ИНФОРМ") == "ПМН")
                                            em = "Пикалева М.Н.";
                                        if (em != "")
                                        {
                                            Employee empl = connect.FindFirstObject<Employee>(x => x.BriefName == em);
                                            if (empl == null)
                                            {
                                                empl = connect.CreateObject<Employee>();
                                                empl.BriefName = em;
                                                empl.UserName = em;
                                                empl.Save();
                                            }
                                            zone.Employee = empl;
                                        }
                                    }
                                }
                                catch { }
                                try { zone.Notes = m_MapInfo.Eval("Selection.ПРИМЕЧАНИЕ"); }
                                catch { }

                                zone.Save();
                                spatial.AddSpatialLink(zone.ClassInfo.FullName, zone.Oid.ToString(), layer, gisObjId);
                                //Console.WriteLine(String.Format("Создана {0} зона арт скважины: {1}", i.ToString(), zoneNo));
                                a++;
                            }
                        }
                    }
                    catch { }
                }
            }
            int an = 0;
            int toatalCountAn = 0;
            layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.AntennZone");
            foreach (string layer in layers)
            {
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    toatalCountAn++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneNo = "";
                                // создаем террзону 
                                AntennZone zone = connect.CreateObject<AntennZone>();
                                SpecialZoneKind zoneKind = null;
                                try
                                {
                                    zone.ZoneNo = m_MapInfo.Eval("Selection.НОМЕР_РЕЕСТР");
                                }
                                catch { }

                                try { zone.Address = m_MapInfo.Eval("Selection.МЕСТОПОЛОЖЕНИЕ"); }
                                catch { }
                                try { zone.InfluenceRadius = Convert.ToDouble(m_MapInfo.Eval("Selection.РАДИУС_ВЛИЯНИЯ")); }
                                catch { }
                                try { zone.MaxBuildHight = Convert.ToDouble(m_MapInfo.Eval("Selection.ЗАСТР_НЕ_ВЫШЕ_М")); }
                                catch { }
                                try { zone.InfluenceAngle = Convert.ToDouble(m_MapInfo.Eval("Selection.УГОЛ_ВЛИЯНИЯ_ГРАД")); }
                                catch { }
                                try { zone.ZSODoc = m_MapInfo.Eval("Selection.ДОКУМЕНТЫ"); }
                                catch { }
                                try { zone.SEZYear = Convert.ToInt16(m_MapInfo.Eval("Selection.ГОД_СЭЗ")); }
                                catch { }
                                try
                                {
                                    string date = m_MapInfo.Eval("Selection.ДАТА_ВНЕСЕНИЯ");
                                    date = date.Insert(4, ".").Insert(7, ".");
                                    DateTime insertdate;
                                    DateTime.TryParse((string)date, out insertdate);
                                    zone.RegDate = insertdate;
                                }
                                catch { }

                                try
                                {
                                    if (m_MapInfo.Eval("Selection.ПРИМЕЧАНИЕ") != "")
                                    {
                                        string em = "";
                                        if ((m_MapInfo.Eval("Selection.ПРИМЕЧАНИЕ")).Contains("КОА"))
                                            em = "Колобова О.А.";
                                        if ((m_MapInfo.Eval("Selection.ПРИМЕЧАНИЕ")).Contains("Румакова"))
                                            em = "Румакова Е.В.";
                                        if (em != "")
                                        {
                                            Employee empl = connect.FindFirstObject<Employee>(x => x.BriefName == em);
                                            if (empl == null)
                                            {
                                                empl = connect.CreateObject<Employee>();
                                                empl.BriefName = em;
                                                empl.UserName = em;
                                                empl.Save();
                                            }
                                            zone.Employee = empl;
                                        }
                                    }
                                }
                                catch { }
                                try { zone.Notes = m_MapInfo.Eval("Selection.ПРИМЕЧАНИЕ"); }
                                catch { }
                                try { zone.Operators = m_MapInfo.Eval("Selection.ОПЕРАТОРЫ"); }
                                catch { }
                                try { zone.DocStorage = m_MapInfo.Eval("Selection.НОМЕР_ПАПКИ_ХРАНЕНИЯ"); }
                                catch { }
                                zone.Save();
                                spatial.AddSpatialLink(zone.ClassInfo.FullName, zone.Oid.ToString(), layer, gisObjId);
                                //Console.WriteLine(String.Format("Создана {0} антенн связи: {1}", i.ToString(), zone.Address));
                                an++;
                            }
                        }
                    }
                    catch { }
                }
            }
            int s = 0;
            int toatalCountS = 0;
            layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.SZZone");
            foreach (string layer in layers)
            {
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    toatalCountS++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneNo = "";
                                // создаем террзону 
                                SZZone zone = connect.CreateObject<SZZone>();
                                SpecialZoneKind zoneKind = connect.FindFirstObject<SpecialZoneKind>(mc => mc.Name == "Санитарно-защитная зона");
                                if (zoneKind != null)
                                {
                                    zone.SpecialZoneKind = zoneKind;
                                    zone.Name = zoneKind.Name;
                                }
                                try
                                {
                                    zone.ZoneNo = m_MapInfo.Eval("Selection.РЕЕСТР_НОМЕР");
                                    zoneNo = m_MapInfo.Eval("Selection.РЕЕСТР_НОМЕР");
                                }
                                catch { }
                                try { zone.Polluter = m_MapInfo.Eval("Selection.ИСТ_ЗАГРЯЗН"); }
                                catch { }
                                try { zone.Address = m_MapInfo.Eval("Selection.АДРЕС"); }
                                catch { }
                                try { zone.DangerClass = m_MapInfo.Eval("Selection.КЛАСС_ОПАСНОСТИ"); }
                                catch { }
                                try { zone.ZoneMark = m_MapInfo.Eval("Selection.ОРИЕНТ_СЗЗ"); }
                                catch { }
                                try { zone.CalcParam = m_MapInfo.Eval("Selection.ПАРАМ_РАСЧ_СЗ"); }
                                catch { }
                                try { zone.Finished = m_MapInfo.Eval("Selection.УСТ_ОКОНЧ"); }
                                catch { }
                                try { zone.SanitaryConclusion = m_MapInfo.Eval("Selection.САН_ЭПИД_ЗАКЛ"); }
                                catch { }
                                try { zone.GSVSolution = m_MapInfo.Eval("Selection.РЕШ_ГСВ"); }
                                catch { }
                                try { zone.SZZYear = Convert.ToInt16(m_MapInfo.Eval("Selection.ГОД_УСТ_СЗЗ")); }
                                catch { }
                                try { zone.RegDate = Convert.ToDateTime(m_MapInfo.Eval("Selection.ДАТА_ВНЕС")); }
                                catch { }
                                try { zone.InfoSource = m_MapInfo.Eval("Selection.ИСТ_ИНФ"); }
                                catch { }
                                try { zone.DocStorage = m_MapInfo.Eval("Selection.ДЕЛО_ХРАН_ДОК"); }
                                catch { }
                                try { zone.SZZKindString = m_MapInfo.Eval("Selection.ВИД_СЗЗ"); }
                                catch { }
                                try { zone.Notes = m_MapInfo.Eval("Selection.ПРИМЕЧАНИЕ"); }
                                catch { }
                                try { zone.ZoneCode = m_MapInfo.Eval("Selection.КОД_ЗОНЫ"); }
                                catch { }
                                zone.Save();
                                spatial.AddSpatialLink(zone.ClassInfo.FullName, zone.Oid.ToString(), layer, gisObjId);
                                //Console.WriteLine(String.Format("Создана {0} антенн связи: {1}", i.ToString(), zone.Address));
                                s++;
                            }
                        }
                    }
                    catch { }
                }
            }
            int w = 0;
            int toatalCountW = 0;
            layers = spatial.GetListLinkedSpatialLayers("AISOGD.Reglament.WaterProtectionZone");
            foreach (string layer in layers)
            {
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    toatalCountW++;
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            // проверяем, есть ли связанный объект у зоны
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                string zoneName = "";
                                // создаем террзону 
                                WaterProtectionZone zone = connect.CreateObject<WaterProtectionZone>();
                                SpecialZoneKind zoneKind = null;
                                if (layer.ToLower() == "Водоохранные_зоны")
                                    zoneName = "Водоохранная зона";
                                if (layer == "Прибрежная_защитная_полоса")
                                    zoneName = "Прибрежная защитная полоса";
                                if (layer == "Береговые_полосы")
                                    zoneName = "Береговая полоса";
                                zoneKind = connect.FindFirstObject<SpecialZoneKind>(mc => mc.Name == zoneName);

                                if (zoneKind != null)
                                {
                                    zone.SpecialZoneKind = zoneKind;
                                    zone.Name = zoneKind.Name;

                                }
                                try { zone.Name = m_MapInfo.Eval("Selection.Название_реки"); }
                                catch { }
                                try { zone.ZoneWidth = Convert.ToDouble(m_MapInfo.Eval("Selection.Ширина_полосы_м")); }
                                catch { }
                                try { zone.RiverLength = Convert.ToDouble(m_MapInfo.Eval("Selection.Длина_реки_км")); }
                                catch { }
                                try { zone.ZoneLimit = m_MapInfo.Eval("Selection.Режим_ограничения"); }
                                catch { }
                                try { zone.Kind = m_MapInfo.Eval("Selection.Вид"); }
                                catch { }
                                try { zone.ZoneDocs = m_MapInfo.Eval("Selection.Документы"); }
                                catch { }
                                try
                                {
                                    string date = m_MapInfo.Eval("Selection.Дата_внесения");
                                    date = date.Insert(4, ".").Insert(7, ".");
                                    DateTime insertdate;
                                    DateTime.TryParse((string)date, out insertdate);
                                    zone.RegDate = insertdate;
                                }
                                catch { }
                                try { zone.Notes = m_MapInfo.Eval("Selection.Примечание"); }
                                catch { }
                                zone.Save();
                                spatial.AddSpatialLink(zone.ClassInfo.FullName, zone.Oid.ToString(), layer, gisObjId);
                                //Console.WriteLine(String.Format("Создана {0} антенн связи: {1}", i.ToString(), zone.Address));
                                w++;
                            }
                        }
                    }
                    catch { }
                }
            }
            XtraMessageBox.Show(String.Format(@"Окончено." +
                $"MonumentZone: Создано {m} зон, не создано {toatalCountM-m}. " +
                $"EngeneeringZone: Создано {e} зон, не создано {toatalCountE - e}. " +
                $"ArtWellZone: Создано {a} зон, не создано {toatalCountA -a}. " +
                $"AntennZone: Создано {an} зон, не создано {toatalCountAn - an}. " +
                $"SZZone: Создано {s} зон, не создано {toatalCountS - s}. " +
                $"WaterProtectionZone: Создано {w} зон, не создано {toatalCountW - w}. "
                ));
        }

        /// <summary>
        ///  Создать участки из ГИС объектов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateParcelsFromGISAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Connect connect = Connect.FromSettings();
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo = new MapInfoApplication();

            //Type mapinfotype = Type.GetTypeFromProgID("Mapinfo.Application");
            //m_MapInfo = (MapInfoApplication)Activator.CreateInstance(mapinfotype);

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено");
                return;
            }
            var layers = spatial.GetListLinkedSpatialLayers("AISOGD.Land.Parcel");
            int j = 0;
            foreach (string layer in layers)
            {
                //Console.WriteLine(String.Format("Слой: {0}", layer));
                m_MapInfo.Do("Select * From " + layer);
                // получаем количество выделенных объектов МапИнфо
                int selObjCount = Convert.ToInt32(m_MapInfo.Eval("SelectionInfo(3)"));
                for (int i = 1; i < selObjCount + 1; i++)
                {
                    m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
                    //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                    // Id (MI_PRINX) выделенного объекта мапинфо
                    try
                    {
                        string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                        if (gisObjId != "0")
                        {
                            string cadNo = m_MapInfo.Eval("Selection.CADASTRAL_NUMBER").Trim();
                            // проверяем, есть ли связанный объект у участка
                            if (!spatial.IsXafObjectLinkedSpatialExist(layer, gisObjId))
                            {
                                
                                if (cadNo != null || cadNo != "")
                                {
                                    // ищем земельный участок с таким кад номером
                                    Parcel parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber == cadNo);
                                    // создаем участок
                                    if (parcel == null)
                                    {
                                        parcel = connect.CreateObject<Parcel>();
                                        parcel.CadastralNumber = cadNo;
                                        try { parcel.Adress = m_MapInfo.Eval("Selection.LOCATION"); }
                                        catch { }
                                        try { parcel.ByDoc = m_MapInfo.Eval("Selection.UTILIZATION"); }
                                        catch { }
                                        try
                                        {
                                            parcel.Area = Convert.ToDouble(m_MapInfo.Eval("Selection.AREA").Split('+')[0].Trim());//.Trim().Replace("+", "").Replace("-", ""));
                                            try
                                            {
                                                parcel.Unit = connect.FindFirstObject<dUnit>(x => x.Name == "кв.м");
                                            }
                                            catch { }
                                        }
                                        catch { }

                                        parcel.Save();
                                    }
                                    spatial.AddSpatialLink(parcel.ClassInfo.FullName, parcel.Oid.ToString(), layer, gisObjId);
                                    //Console.WriteLine(String.Format("Создана {0} территориальная зона: {1}", i.ToString(), zoneName));
                                    j++;
                                }
                            }
                            if (cadNo != null || cadNo != "")
                            {
                                // ищем земельный участок с таким кад номером
                                Parcel parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber == cadNo);
                                try
                                {
                                    parcel.Area = Convert.ToDouble(m_MapInfo.Eval("Selection.AREA").Trim().Replace("+", "").Replace("-", ""));
                                    try
                                    {
                                        parcel.Unit = connect.FindFirstObject<dUnit>(x => x.Name == "кв.м");
                                    }
                                    catch { }
                                }
                                catch { }
                            }
                        }
                    }
                    catch { }
                }
            }
            XtraMessageBox.Show(String.Format("Создано {0} земельный участков", j.ToString()));
        }

        /// <summary>
        /// Функция связки объектов строительства (ГИС с реестрами)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LinkGISConstrStagesAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Connect connect = Connect.FromSettings();
            //string message = "Начало связки документов ИСОГД с пространственными данными";
            //Console.WriteLine(message);

            MapInfoApplication m_MapInfo = null;
            // 2. Получаем активную МапИнфо
            //m_MapInfoCom = new MapInfoActiveX.MapInfoComModule();
            m_MapInfo = new MapInfoApplication();//m_MapInfoCom.GetActiveMapInfo();
            int selObjCount = 0;
            string ReqestObjID = "";
            if (m_MapInfo == null)
            {
                Console.WriteLine("MapInfo не запущено", "Инфо");
                Console.ReadLine();
                return;
            }

            var spatial = new SpatialController<GisLayer>(connect);
            SpatialConfig spatialConfig = null;
            int i = 0;
            int j = 0;
            string xafClassName = "";
            string spatialLayer = "";
            int totalcount = 0;
            // 1. Перебираем все настройки связи SpatialConfig для классов IsogdPhysStorageBook, IsogdStorageBook, Lot, IsogdRequestCard
            // 1.1
            xafClassName = "AISOGD.Constr.ConstrStage";
            spatialConfig = connect.FindFirstObject<SpatialConfig>(mc => mc.IsogdClassName == connect.GetClassInfo(xafClassName).ClassType);
            if (spatialConfig != null)
            {
                Console.WriteLine("Перебираем объекты класса: " + xafClassName);

                foreach (SpatialLayerItem spatialLayerItem in spatialConfig.Layers)
                {

                    if (spatialLayerItem != null)
                        if (spatialLayerItem.SpatialLayer != null)
                            if (spatialLayerItem.SpatialLayer.LayerId != null && spatialLayerItem.SpatialLayer.LayerId != String.Empty)
                                spatialLayer = spatialLayerItem.SpatialLayer.LayerId;
                    if (spatialLayer != "")
                    {
                        Console.WriteLine("Перебираем объекты слоя: " + spatialLayer);
                        totalcount = connect.FindObjects<ConstrStage>(mc => mc.MapNo != null && mc.MapNo != "").Count<ConstrStage>();
                        foreach (ConstrStage isogdObj in connect.FindObjects<ConstrStage>(mc => mc.MapNo != null && mc.MapNo != ""))
                        {
                            j++;
                            //ImportConstractionsConsole.SystemConsole.drawTextProgressBar(j, totalcount);
                            m_MapInfo.Do("Select MI_PRINX From " + spatialLayer + " Where НОМЕР_НА_СХЕМЕ = \"" + isogdObj.MapNo + "\"");
                            string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                            if (gisObjId != null && gisObjId != String.Empty && gisObjId != "0")
                            {
                                if (!spatial.IsSpatialObjectLinkExist(spatialLayer, gisObjId))
                                {
                                    spatial.AddSpatialLink(xafClassName,
                                        isogdObj.Oid.ToString(),
                                            spatialLayer,
                                            gisObjId);
                                    i++;
                                    //Console.WriteLine(String.Format("Обработали {0} объект класса {1} в слое {2}. MI_PRINX: {3}", i.ToString(), xafClassName, spatialLayer, gisObjId));
                                }

                            }
                            //m_MapInfo.Do("Drop Table tmpTable");
                            //Console.WriteLine(String.Format("Обработали {0} объект класса {1} в слое {2}. MI_PRINX: {3}", i.ToString(), xafClassName, spatialLayer, gisObjId));
                        }
                        connect.GetUnitOfWork().CommitChanges();
                    }
                    spatialLayer = "";
                }
            }
        }

        
    }
}
