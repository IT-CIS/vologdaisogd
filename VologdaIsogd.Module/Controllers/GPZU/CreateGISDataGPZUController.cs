﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.GPZU;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateGISDataGPZUController : ViewController
    {
        public CreateGISDataGPZUController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Различные пространственные построения в ГПЗУ
        /// 1. Вырезка зон ограничений по участку
        /// 2. Отрисовка начального места застройки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateGISDataGPZUAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            GradPlan gradPlan;
            gradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)gradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();

            SpatialObject GradPlanSpatialObject = null;

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return;
            }

            // ищем контур градплана
            //try
            //{
                string gradPlanLayerId = spatial.GetSpatialLinks("AISOGD.GPZU.GradPlan", gradPlan.Oid.ToString()).First<SpatialRepository>().SpatialLayerId;

                string gradPlanObjId = spatial.GetSpatialLinks("AISOGD.GPZU.GradPlan", gradPlan.Oid.ToString()).First<SpatialRepository>().SpatialObjectId;

                GradPlanSpatialObject = new SpatialObject(m_MapInfo, gradPlanLayerId, gradPlanObjId);
                //Сохранить ГИС объект градплана в выборку
                m_MapInfo.Do(string.Format("Select * from {0} Where MI_PRINX = {1}", gradPlanLayerId, gradPlanObjId));
                TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
                m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));
                m_MapInfo.SelectionTable.Close();

            //string zoneOriginalTableName = "Зоны_охраны_ОКН";
            //string zoneToCopyTableName = "ГПЗУ_СЗЗ";
            // объявляем переменные МапИфно для стилей и объекта
            try
            {
                m_MapInfo.Do(@"Dim myBrush as Brush");
            }
            catch { }
            try
            {
                m_MapInfo.Do(@"Dim myObj as Object");
            }
            catch { }
            GPZUHelper GPZUHelper = connect.FindFirstObject<GPZUHelper>(x=> x.Name == "Соответствие зон ГПЗУ");
            foreach (ZoneLayerAccording ZoneLayerAccording in GPZUHelper.ZoneLayerAccordings)
            {
                string zoneOriginalTableName = "";
                string zoneToCopyTableName = "";
                zoneOriginalTableName = ZoneLayerAccording.SpatialLayerIn.LayerId;
                zoneToCopyTableName = ZoneLayerAccording.SpatialLayerOut.LayerId;
                //выбрать пересекающиеся объекты
                TableInfo intersectsTempTable = new TableInfo(m_MapInfo, "intersectsTempTable");
                m_MapInfo.Do(string.Format("Select * From {0} Where obj Intersects (Select obj From {1} Where MI_PRINX = {2}) Into {3}",
                        zoneOriginalTableName, gradPlanLayerId, gradPlanObjId, intersectsTempTable.Name));

                //XtraMessageBox.Show("intersectsTempTable=" + intersectsTempTable.Name);
                //Колличество пересекающихся объектов
                long rowCount = intersectsTempTable.GetRowCount() ?? 0;

                //XtraMessageBox.Show("Колличество пересекающихся объектов=" + rowCount.ToString());
                for (int i = 1; i <= rowCount; i++)
                {
                    /// сначала клонируем пересекающийся объект в слой рабочего набора
                    //Выделить пересекающийся объект
                    m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1}", intersectsTempTable.Name, i));
                    //m_MapInfo.Do(string.Format("Fetch Rec {0} From {1}", i.ToString(), intersectsTempTable.Name));
                    // Id(MI_PRINX) пространственного объекта ЗУ
                    string zoneReqestObjID = m_MapInfo.Eval("Selection.MI_PRINX");
                    // получаем стиль (оформление выделенного объекта)
                    try { m_MapInfo.Do(@"myBrush  = ObjectInfo(selection.obj, 3)"); }
                    catch { }
                    SpatialObject zoneOriginalSpatialObject = null;
                    zoneOriginalSpatialObject = new SpatialObject(m_MapInfo, zoneOriginalTableName, zoneReqestObjID);
                    // Возвращает RowID созданного объекта зоны в рабочем наборе
                    string rowzoneToCopyID = zoneOriginalSpatialObject.CloneSpatialObject(zoneToCopyTableName, false);
                    //Снять выделение
                    m_MapInfo.SelectionTable.Close();

                    // теперь выбираем нашу скопированную в рабочий набор зону, назначем ей стиль(оформление) и режем ее
                    m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1}", zoneToCopyTableName, rowzoneToCopyID));
                    // назначаем скопированный стиль
                    try
                    {
                        if (zoneToCopyTableName != "ОхрЗоны_СЕТЕЙ")// для зон сетей оформление единообразно
                        {
                            m_MapInfo.Do(@"myObj = selection.obj");
                            // присваеваем новый стиль
                            m_MapInfo.Do(string.Format(@"Alter Object myObj Info 3, myBrush"));
                            m_MapInfo.Do(string.Format("Update selection Set obj = myobj Where RowID = 1"));
                        }
                    }
                    catch { }
                    //Добавить его в target
                    m_MapInfo.Target.SetTargetOn();

                    //Снять выделение
                    m_MapInfo.SelectionTable.Close();

                    //Выделить исходный объект градплана
                    m_MapInfo.Do(string.Format("Select * From {0} Where MI_PRINX = {1}  Into Selection", gradPlanLayerId, gradPlanObjId));
                    //XtraMessageBox.Show("SplitObjects(true) - начало");
                    //Разрезать и скопировать семантику/ true - копировать все поля
                    m_MapInfo.SplitObjects(false);

                    /// определяем, какие объекты разрезанных зон не лежат в границах участка - их удаляем
                    //поместить результат разрезания во временную таблицу
                    TableInfo resultSplitTempTable = new TableInfo(m_MapInfo, "resultSplitTempTable");
                    m_MapInfo.Do(string.Format("Select * From Selection Into {0}", resultSplitTempTable.Name));
                    //XtraMessageBox.Show("resultSplitTempTable:" + resultSplitTempTable.Name);
                    //Снять выделение
                    m_MapInfo.SelectionTable.Close();

                    //колличество разрезанных объектов
                    long resultRowCount = resultSplitTempTable.GetRowCount() ?? 0;

                    //выбрать объекты из массива разрезанных, которые не попали под площадь участка (ГПЗУ)
                    TableInfoTemp containsObjectTempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                    //XtraMessageBox.Show("containsObjectTempTable:" + containsObjectTempTable.Name);
                    for (int j = 1; j <= resultRowCount; j++)
                    {
                        m_MapInfo.SelectionTable.Close();
                        TableInfoTemp tempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                        m_MapInfo.Do(string.Format("Select * From {0} Where RowID = {1} Into {2}",
                            resultSplitTempTable.Name, j, tempTable.Name));
                        m_MapInfo.SelectionTable.Close();
                        //XtraMessageBox.Show("tempTable2:");
                        TableInfoTemp tempTable2 = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                        m_MapInfo.Do(string.Format("Select * From {0} Where obj Within (Select obj from {1} Into {2}) Into {3}",
                            tempTable.Name, selectedObjectsTempTable.Name, tempTable2.Name, containsObjectTempTable.Name));
                        tempTable.Dispose();
                        tempTable2.Dispose();

                        //XtraMessageBox.Show("111111:");
                        long resultIntersectsRowCount = containsObjectTempTable.GetRowCount() ?? 0;
                        if (resultIntersectsRowCount == 0)
                        {
                            // удаляем лишний объект
                            m_MapInfo.Do(string.Format("Delete From {0} Where RowID = {1} Commit table {0}",
                            resultSplitTempTable.Name, j));
                        }
                    }
                }
            }
            

            // 2. Создаем пятно застройки 
            // to do - создание номера пятна застройки 
            try
            {
                // считываем настройки
                string BuildingSpotLayerName = GPZUHelper.BuildingSpotLayerName;
                double BufferDistance = GPZUHelper.BufferDistance;

                try { m_MapInfo.Do(@"Dim buffObj as Object"); }
                catch { }
                m_MapInfo.Do(string.Format("Select * from {0} Where MI_PRINX = {1}", gradPlanLayerId, gradPlanObjId));
                m_MapInfo.Do(string.Format("Create Object As Buffer From selection Into Variable buffObj Width {1} Units \"{2}\"",
                                gradPlanLayerId, BufferDistance, "m"));
                m_MapInfo.Do(string.Format("Insert Into {0} (Obj) Values (buffObj)",
                            BuildingSpotLayerName));
                m_MapInfo.Do(string.Format("Commit table {0}",
                           BuildingSpotLayerName));

            }
            catch { }
            //}
            //catch { }

            // 3. Номер пятна застройки
            try {
                string NumberLayerName = GPZUHelper.NumberObjectLayerName;
                try
                {
                    m_MapInfo.Do(@"Dim x as Float, y as Float");
                }
                catch { }
                m_MapInfo.Do(string.Format("Select * from {0} Where MI_PRINX = {1}", gradPlanLayerId, gradPlanObjId));
                // получаем координаты центра выделенного объека
                m_MapInfo.Do(string.Format("x = CentroidX(selection.obj)"));
                m_MapInfo.Do(string.Format("y = CentroidY(selection.obj)"));
                m_MapInfo.Do(string.Format("Insert Into {0} (Obj) Values (CreatePoint(x, y))",
                            NumberLayerName));
                m_MapInfo.Do(string.Format("Select * From {0} Where RowId = {1}", NumberLayerName,
                            m_MapInfo.Eval((string.Format("TableInfo(\"{0}\",  8)", NumberLayerName)))));
                //try
                //{
                int i = 1;
                m_MapInfo.Do("Update selection Set Номер =  " + i + " Commit table " + NumberLayerName);
            }
            catch { }

            // 4. Копируем контур ГПЗУ и точки ЗУ в отдельные слои
            //try
            //{
                CopyGpadPlanAndPointsToParcel(m_MapInfo, gradPlanObjId, gradPlan.LotCadNo);
            //}
            //catch { }
            // 5. Формируем текст иных показателей

            CreateGPZUAnotherInfoController CreateGPZUAnotherInfoController = new CreateGPZUAnotherInfoController();
            //try {
            CreateGPZUAnotherInfoController.CreateTopoInfoText(gradPlan, connect, m_MapInfo);
            //}
            //catch { }
            //try {
            CreateGPZUAnotherInfoController.CreateParcelPlaningInfoText(gradPlan, connect, m_MapInfo);
            CreateGPZUAnotherInfoController.CreateArchSolutionsInfoText(gradPlan, connect, m_MapInfo);
            CreateGPZUAnotherInfoController.CreateAnotherInfoText(gradPlan, connect, m_MapInfo);
            //}
            //catch { }
            //Закрыть все слои запросов
            int NumTables = Convert.ToInt16(m_MapInfo.Eval("NumTables()"));
            List<string> queryTablesList = new List<string>();
            for (int k = 1; k <= NumTables; k++)
            {
                TableInfo TableToClose = new TableInfo(m_MapInfo, k);
                if (TableToClose.Name.IndexOf("Query") != -1)
                    queryTablesList.Add(TableToClose.Name);
            }
            foreach (string queryTable in queryTablesList)
            {
                try
                {
                    m_MapInfo.TableInfo(queryTable).Dispose();
                }
                catch { }
            }
            
        }


        public void CopyGpadPlanAndPointsToParcel(MapInfoApplication m_MapInfo, string gradPlanObjId, string cadNo)
        {
            // объявляем в МапИнфо переменные для копирования стиля закраски
            try
            {
                m_MapInfo.Do(@"Dim myBrush as Brush");
            }
            catch { }
            try
            {
                m_MapInfo.Do(@"Dim mySymbol as Symbol");
            }
            catch { }
            try
            {
                m_MapInfo.Do(@"Dim myNewObj as Object");
            }
            catch { }

            string[] Layers = { "Поворотные_точки;ГПЗУ_точки_ЗУ", "ГПЗУ;ЗУ_ГПЗУ" };
            foreach (string l in Layers)
            {
                string originalTableName = l.Split(';')[0];
                string toCopyTableName = l.Split(';')[1];

                TableInfo originalTableInfo = new TableInfo(m_MapInfo, originalTableName);
                TableInfoTemp sourceTempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);

                //таблица, куда поместятся точки по участку (точки находим по кадастровому номеру ЗУ)
                TableInfo intersectsTempTable = new TableInfo(m_MapInfo, "intersectsTempTable");
                
                //try
                //{
                    if (originalTableName == "ГПЗУ")
                    {
                        m_MapInfo.Do(string.Format("Select * From {0} Where MI_PRINX={1} Into {2}",
                               originalTableName,  gradPlanObjId, intersectsTempTable.Name));
                    }
                    //если Поворотные_точки - ищем именно те, которые относятся к нашему ЗУ
                    else if (originalTableName == "Поворотные_точки")
                    {
                        m_MapInfo.Do(string.Format("Select * From {0} Where Кадастровый_номер = \"{1}\" Into {2}",
                                originalTableName, cadNo, intersectsTempTable.Name));
                    }
                //}
                //catch { }

                //Колличество объектов пересечения
                long rowCount = intersectsTempTable.GetRowCount() ?? 0;

                for (int i = 1; i <= rowCount; i++)
                {
                    //Выделить пересекающийся объект и поместить его в темп таблицу sourceTempTable
                    m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1} Into {2}", intersectsTempTable.Name, i, sourceTempTable.Name));

                    //получить стиль площадного объекта
                    try
                    {
                        m_MapInfo.Do(string.Format("myBrush  = ObjectInfo({0}.obj, 3)", sourceTempTable.Name)); //
                    }
                    catch { }
                    //получить стиль точечного объекта
                    //try
                    //{
                    //    m_MapInfo.Do(string.Format("mySymbol  = ObjectInfo({0}.obj, 2)", sourceTempTable.Name)); //
                    //}
                    //catch { }

                    // клонировать выделенный объект в слой назначения 
                    m_MapInfo.Do(string.Format("Insert Into {0} (Obj) Select Obj From {1}",
                toCopyTableName, sourceTempTable.Name));



                    //получить RowID нового объекта
                    m_MapInfo.Do("Fetch Last From " + toCopyTableName);
                    string newRowID = m_MapInfo.Eval(toCopyTableName + ".RowID");

                    // клонируем семантику
                    if (originalTableName == "Поворотные_точки")
                    {
                        int? columnCount = originalTableInfo.GetColumnCount();
                        if (columnCount != null)
                        {
                            for (int j = 1; j <= columnCount; j++)
                            {
                                //try
                                //{
                                if (originalTableInfo.ColumnInfo(j).Name != "MI_PRINX")
                                {
                                    //string colName = "";
                                    m_MapInfo.Do(string.Format("Fetch First From {0}", sourceTempTable.Name));
                                    string value = m_MapInfo.Eval(string.Format("{0}.{1}", sourceTempTable.Name, originalTableInfo.ColumnInfo(j).Name));
                                    //switch (originalTableInfo.ColumnInfo(j).Name)
                                    //{
                                    //    case "Номер_точки":
                                    //        colName = "номер";
                                    //        break;
                                    //    case "x_msk35":
                                    //        colName = "Х_МСК35";
                                    //        break;
                                    //    case "y_msk35":
                                    //        colName = "У_МСК35";
                                    //        break;
                                    //}
                                    //if (colName != "")
                                        if (!string.IsNullOrWhiteSpace(value))
                                    {
                                        m_MapInfo.Do(string.Format("Update {0} Set {1} = \"{2}\" Where RowID = {3}",
                                            toCopyTableName, originalTableInfo.ColumnInfo(j).Name, value, newRowID));

                                    }
                                }
                                //}
                                //catch (Exception ex) { }
                            }
                        }
                    }
                    try
                    {
                        // присваеваем новый стиль
                        ////Снять выделение
                        //m_MapInfo.SelectionTable.Close();

                        // теперь выбираем наш скопированный объект и назначем ему стиль(оформление)
                        m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1}", toCopyTableName, newRowID));
                        m_MapInfo.Do("myNewObj = selection.obj");
                        try
                        {
                            m_MapInfo.Do("Alter Object myNewObj Info 3, myBrush");
                        }
                        catch { }
                        //try
                        //{
                        //    m_MapInfo.Do("Alter Object myNewObj Info 2, mySymbol");
                        //}
                        //catch { }
                        // Сохраняем данные
                        m_MapInfo.Do(string.Format("Update {0} Set obj = myNewObj Where RowID = {1}", toCopyTableName, newRowID));
                    }
                    catch { }
                    // Сохраняем данные
                    m_MapInfo.Do(string.Format("Commit table {0}", toCopyTableName));
                }
                // Сохраняем данные
                m_MapInfo.Do(string.Format("Commit table {0}", toCopyTableName));
            }
        }
    }
}
