﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.GPZU;
using AISOGD.SystemDir;
using AISOGD.Reglament;
using DevExpress.XtraEditors;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateGPZUAnotherInfoController : ViewController
    {
        public CreateGPZUAnotherInfoController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Создаем текст по иным показателям ГПЗУ по п. 1. Инженерные изыскания
        /// </summary>
        /// <param name="gradplan"></param>
        /// <param name="connect"></param>
        /// <param name="m_MapInfo"></param>
        public void CreateTopoInfoText(GradPlan gradplan, Connect connect, MapInfoApplication m_MapInfo)
        {
            string topoInfo = "Перед началом проектных работ необходимо выполнить инженерные изыскания с корректурой топографической съемки М 1:500";
            bool isTopoExist = false;
            // ищем пересечение с СТИ
            // из таблицы связи получаем Ид реестрового объекта ГПЗУ
            SpatialRepository objParcelLink = connect.FindFirstObject<SpatialRepository>(mc =>
                mc.IsogdClassName == gradplan.ClassInfo.FullName && mc.IsogdObjectId == gradplan.Oid.ToString());
            m_MapInfo.Do("Select * From " + objParcelLink.SpatialLayerId + " Where MI_PRINX = " + objParcelLink.SpatialObjectId + " Into zObject");

            string[] zoneLayerIds = { "Схема_текущих_изменений" };
            foreach (string zoneLayerId in zoneLayerIds)
            {
                // ищем пересечение с нашим объектом (Partly Within, Intersects)
                m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
                int selObjCount = 0;
                try { selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)")); }
                catch
                { }
                if (selObjCount > 0)
                {
                    isTopoExist = true;
                    break;
                }
            }

            if (!isTopoExist)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                    topoInfo = "До подготовки схемы планировочной организации земельного участка с обозначением места размещения индивидуального жилого дома рекомендуем выполнить топографическую съемку в масштабе 1:500";
                else
                    topoInfo = "Перед началом проектных работ необходимо выполнить инженерные изыскания и топографическую съемку М 1:500";

            }
            else
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                    topoInfo = "До подготовки схемы планировочной организации земельного участка с обозначением места размещения индивидуального жилого дома рекомендуем выполнить корректуру топографической съемки в масштабе 1:500";
            }
            gradplan.TopoInfo = topoInfo;
            gradplan.Save();
            connect.GetUnitOfWork().CommitChanges();
        }

        /// <summary>
        /// Создаем текст по иным показателям ГПЗУ по п. 2. Планировочная организация земельного участка
        /// </summary>
        /// <param name="gradplan"></param>
        /// <param name="connect"></param>
        /// <param name="m_MapInfo"></param>
        public void CreateParcelPlaningInfoText(GradPlan gradplan, Connect connect, MapInfoApplication m_MapInfo)
        {
            string parcelInfo = "";


            // из таблицы связи получаем Ид реестрового объекта ГПЗУ
            SpatialRepository objParcelLink = connect.FindFirstObject<SpatialRepository>(mc =>
                mc.IsogdClassName == gradplan.ClassInfo.FullName && mc.IsogdObjectId == gradplan.Oid.ToString());
            m_MapInfo.Do("Select * From " + objParcelLink.SpatialLayerId + " Where MI_PRINX = " + objParcelLink.SpatialObjectId + " Into zObject");

            // находим, есть ли пересечения с Водой (из ГИСа)
            bool isWaterZoneExist = false;
            string[] zoneLayerIds = { "Территория_паводка", "Прибрежная_защитная_полоса", "Водоохранные_зоны", "Береговые_полосы" };
            foreach (string zoneLayerId in zoneLayerIds)
            {
                // ищем пересечение с нашим объектом (Partly Within, Intersects)
                m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0");// Into tmpTable");
                int selObjCount = 0;
                try { selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)")); }
                catch
                { }
                if (selObjCount > 0)
                {
                    isWaterZoneExist = true;
                    break;
                }
            }

            // находим, есть ли пересечения с зонами артскважин (из ГИСа)
            bool isArtZoneExist = false;
            string[] zoneArtLayerIds = { "Зоны_арт_скважин" };
            foreach (string zoneLayerId in zoneArtLayerIds)
            {
                // ищем пересечение с нашим объектом (Partly Within, Intersects)
                m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0");// Into tmpTable");
                int selObjCount = 0;
                //try {
                    selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                //}
                //catch
                //{ }
                if (selObjCount > 0)
                {
                    isArtZoneExist = true;
                    break;
                }
            }

            // есть ли на участке инженерные коммуникации
            bool isCommunicationExist = false;
            string CommLayerIds = "ОхрЗоны_СЕТЕЙ";
            // ищем пересечение с нашим объектом (Partly Within, Intersects)
            m_MapInfo.Do("Select * from " + CommLayerIds + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0");
            int commObjCount = 0;
            try { commObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)")); }
            catch
            { }
            if (commObjCount > 0)
            {
                isCommunicationExist = true;
            }

            // лежит ли участок в зонах ОКН
            bool isOKNZone = false;
            try
            {
                if (gradplan.MonumentZoneKind.Count > 0)
                    isOKNZone = true;
            }
            catch { }

            // лежит ли участок в зонах ОКН - (А-1 или А-2) (И-2 .. И-4) 
            bool isA12Zone = false;
            bool isI234Zone = false;
            try
            {
                if (gradplan.MonumentZoneKind.Count > 0)
                {
                    foreach (MonumentZoneKind monumentZoneKind in gradplan.MonumentZoneKind)
                    {
                        try
                        {
                            if (monumentZoneKind.NameSmall == "А-1" || monumentZoneKind.NameSmall == "А-2")
                            {
                                isA12Zone = true;
                            }
                            if (monumentZoneKind.NameSmall == "И-2" || monumentZoneKind.NameSmall == "И-3" || monumentZoneKind.NameSmall == "И-4")
                            {
                                isI234Zone = true;
                            }
                        }
                        catch { }
                    }
                }
            }
            catch { }

            // лежит ли участок в зонах ОКН - И-1 
            bool isI1Zone = false;
            try { isI1Zone = gradplan.I1OKNZoneFlag; }
            catch { }

            // собираем теперь весь текст 
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
            {
                parcelInfo = "2.1. Планировочную структуру территории участка сформировать с учетом существующей улично-дорожной сети и зеленых территорий общего пользования.";
                parcelInfo += Environment.NewLine;
                try
                {
                    parcelInfo += String.Format("2.2. Выполнить проектирование {0} и иных объектов, " +
                  "связанных с его эксплуатацией и обслуживанием, в том числе сетей инженерно-технического обеспечения, " +
                  "а также определить место допустимого размещения объекта", gradplan.ObjectNameRod.ToLower() ?? "многоквартирного дома");
                }
                catch { }
                //if (gradplan.GPZUWorkKind == AISOGD.Enums.eGPZUWorkKind.Реконструкция)
                //    parcelInfo += "Градостроительное освоение земельного участка";
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
            {
                parcelInfo = "2.1.  ";
                if (gradplan.GPZUWorkKind == AISOGD.Enums.eGPZUWorkKind.Строительство)
                    parcelInfo += String.Format("Строительство {0} с благоустройством земельного участка выполнить", gradplan.ObjectNameRod.ToLower() ?? "индивидуального жилого дома");
                else
                    parcelInfo += "Градостроительное освоение земельного участка выполнить";
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
            {
                parcelInfo = "2.1.  ";
                if (gradplan.GPZUWorkKind == AISOGD.Enums.eGPZUWorkKind.Строительство)
                    parcelInfo += String.Format("Проектирование и место допустимого размещения {0} с благоустройством земельного участка выполнить",
                        gradplan.ObjectNameRod.ToLower() ?? "___________");
                else
                    parcelInfo += "Градостроительное освоение земельного участка выполнить";
            }

            parcelInfo += " на основании нормативов градостроительного проектирования муниципального образования «Город Вологда», " +
                    "утвержденных решением Вологодской городской Думы от 31 мая 2010 года  № 357 (с последующими изменениями)";
            if (isOKNZone)
            {
                parcelInfo += " и  требований градостроительных регламентов в границах зон охраны объектов культурного наследия, расположенных на территории г. Вологды, " +
                    "и режимов использования земель на территориях памятников и в границах зон охраны объектов культурного наследия, расположенных на территории г. Вологды, " +
                    "утвержденных постановлением Правительства Вологодской области от 28 декабря  2009 года № 2087 (с последующими изменениями)";
            }
            //XtraMessageBox.Show(isArtZoneExist.ToString());
            if (isWaterZoneExist || isArtZoneExist)
            {
                parcelInfo += ", а также в соответствии с водным законодательством  и законодательством в области охраны окружающей среды";
            }
            parcelInfo += "." + Environment.NewLine;
            int j = 5;
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
            {
                try
                {
                    parcelInfo += String.Format("2.3.  При проектировании {0} и сооружений, " +
                        "необходимых для его облуживания обеспечить соблюдение противопожарных и инсоляционных норм.", gradplan.ObjectNameRod.ToLower() ?? "многоквартирного дома");
                    parcelInfo += Environment.NewLine;
                }
                catch { }
                parcelInfo += String.Format("2.4. Проектной документацией предусмотреть нормативное комплексное благоустройство данного земельного участка " +
                    "с кадастровым номером {0} с выполнением озеленения (посадка новых экземпляров деревьев и декоративных кустарников, устройство и " +
                    "восстановление газонных поверхностей), освещения, оснащения игровыми комплексами и тренажерами детских и физкультурных площадок " +
                    "для занятий физкультурой разных возрастных групп в границах данного земельного участка с кадастровым номером {0}; " +
                    "размещение для проектируемого {1} контейнеров и урн для мусора с соблюдением требуемых санитарных норм; " +
                    "в перечень оборудования для физкультурных площадок, необходимых для обслуживания проектируемого {1}, " +
                    "включить тренажеры  для жителей разных возрастных категорий.",
                    gradplan.LotCadNo ?? "_________", gradplan.ObjectNameRod.ToLower() ?? "многоквартирного дома");
                parcelInfo += Environment.NewLine;

            }
            int i = 4;
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
            {
                parcelInfo += String.Format("2.2.  При размещении {0} обеспечить соблюдение противопожарных разрывов и " +
                    "требуемых норм инсоляции до существующих зданий и строений.", gradplan.ObjectNameRod.ToLower() ?? "индивидуального жилого дома");
                parcelInfo += Environment.NewLine;
                parcelInfo += String.Format("2.3. При разработке схемы планировочной организации земельного участка с обозначением места " +
                    "размещения {1} предусмотреть: комплексное благоустройство данного земельного участка с " +
                    "кадастровым номером {0}, степень огнестойкости проектируемых объектов и класс конструктивной пожарной опасности, " +
                    "указание линейных размеров планируемого к строительству {1}, а также от него до границ данного земельного участка " +
                    "с кадастровым номером {0}, зданий, строений и сооружений, расположенных на смежных земельных участках.",
                    gradplan.LotCadNo ?? "_________", gradplan.ObjectNameRod.ToLower() ?? "индивидуального жилого дома");
                parcelInfo += Environment.NewLine;
            }
            int n = 5;
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
            {
                parcelInfo += String.Format("2.2. Обеспечить соблюдение противопожарных разрывов и требуемых норм инсоляции до существующих зданий и строений.",
                    gradplan.ObjectNameRod ?? "___________");
                parcelInfo += Environment.NewLine;
                parcelInfo += "2.3. Вертикальную планировку земельного участка увязать с проектными отметками смежных земельных участков (при наличии проектных решений), " +
                     "также исключить подтопление смежных территорий." + Environment.NewLine;
                parcelInfo += "2.4. Нормативные разрывы от существующих инженерных сетей принять в соответствии со СП 42.13330.2011.";
                parcelInfo += Environment.NewLine;
                if (isCommunicationExist)
                {
                    n++;
                    parcelInfo += "2.5. В случае выноса инженерных сетей (в соответствии с техническими условиями) требования по соблюдению охранных зон, " +
                        "указанные на чертеже градостроительного плана земельного участка и линий градостроительного регулирования, не применять.";
                    parcelInfo += Environment.NewLine;
                    if (isA12Zone)
                    {
                        parcelInfo += String.Format("2.{0}. Подземная прокладка инженерных коммуникаций, кроме транзитных и магистральных сетей, " +
                            "с обязательным соблюдением требований об охране культурного слоя.", n.ToString());
                        parcelInfo += Environment.NewLine;
                        n++;
                    }
                }
                parcelInfo += String.Format("2.{0}. Проектной документацией предусмотреть нормативное комплексное благоустройство земельного участка с " +
                    "выполнением озеленения (посадка новых экземпляров деревьев и декоративных кустарников, устройство и восстановление газонных поверхностей)," +
                    "освещения; размещение в границах земельного участка контейнеров и урн для мусора с соблюдением требуемых санитарных норм.", n.ToString());
                parcelInfo += Environment.NewLine;
            }
            if (isI234Zone)
            {
                parcelInfo += "Благоустройство территории на основании заключения, выданного уполномоченным органом в сфере охраны объектов культурного наследия " +
                    "Вологодской области в соответствии с  градостроительными регламентами в границах зон охраны объектов культурного наследия, расположенных на " +
                    "территории г. Вологды, и режимов использования земель на территориях памятников и в границах зон охраны объектов культурного наследия, " +
                    "расположенных на территории г. Вологды, утвержденными постановлением Правительства Вологодской области от 28 декабря  2009 года № 2087  (с последующими изменениями).";
                parcelInfo += Environment.NewLine;
            }
            if (isI1Zone)
            {
                parcelInfo += "Благоустройство территории на основании согласованного проекта, выполненного на основании заключения, выданного уполномоченным " +
                    "органом в сфере охраны объектов культурного наследия Вологодской области в соответствии с  градостроительными регламентами в границах зон " +
                    "охраны объектов культурного наследия, расположенных на территории г. Вологды, и режимов использования земель на территориях памятников и в границах " +
                    "зон охраны объектов культурного наследия, расположенных на территории г. Вологды, утвержденными постановлением Правительства Вологодской области от " +
                    "28 декабря 2009 года № 2087  (с последующими изменениями).";
                parcelInfo += Environment.NewLine;
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое || gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                    {
                        parcelInfo += String.Format("2.{0}. ", j.ToString());
                        j++;
                    }
                    else
                    {
                        parcelInfo += String.Format("2.{0}.", i.ToString());
                        i++;
                    }
                    parcelInfo += "Проектирование и строительство объектов капитального строительства вести на основании разработанного и рекомендованного " +
                        "уполномоченным органом в сфере охраны объектов культурного наследия Вологодской области проекта регенерации (комплексной реконструкции) квартала. " +
                        "Проектирование и строительство ведется на основании задания, выданного уполномоченным органом в сфере охраны объектов культурного наследия " +
                        "Вологодской области (в соответствии с проектом регенерации квартала).";
                    parcelInfo += Environment.NewLine;
                }
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
            {
                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Рекомендуем при подаче заявления о выдаче разрешения на строительство дополнить раздел «Схема планировочной организации земельного участка»" +
                    "альбомом малых архитектурных форм в части разработки элементов благоустройства (игровые комплексы и тренажеры детских и физкультурных площадок " +
                    "для занятий физкультурой разных возрастных групп).";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += String.Format("Границы благоустройства принять до границ данного земельного участка с кадастровым номером {0}.", gradplan.LotCadNo);
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "За пределами земельного участка элементы благоустройства рекомендуется увязать с существующими транспортными и пешеходными путями " +
                    "смежных земельных участков и территорий общего пользования; при проектировании пешеходных путей обеспечить их максимальную непрерывность и " +
                    "возможность прямого прохода пешеходов.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Вертикальную планировку земельного участка увязать с проектными отметками смежных земельных участков (при наличии проектных решений)," +
                    "также исключить подтопление смежных территорий.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += String.Format("Нормативное количество автопарковочных мест и площадки, необходимые для обслуживания проектируемого {0}, " +
                    "разместить в границах данного земельного участка с кадастровым номером {1} в соответствии с нормативами градостроительного " +
                    "проектирования муниципального образования «Город Вологда», утвержденными решением Вологодской городской Думы от 31 мая 2010 года № 357 " +
                    "(с последующими изменениями); размер машиноместа: 2,5 м x 5,3 м, для инвалидов 3,6 x 6,0 м.",
                    gradplan.ObjectNameRod.ToLower() ?? "многоквартирного дома", gradplan.LotCadNo ?? "_________________");
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Расстояния до площадок и автопарковочных мест от окон жилых и общественных зданий, а также друг от друга принять " +
                    "в соответствии с требованиями СП 42.13330.2011 и табл. 7.1.1 СанПин 2.2.1/2.1.1.1200-03.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Нормативные разрывы от существующих инженерных сетей принять в соответствии со СП 42.13330.2011.";
                parcelInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
            {
                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Элементы благоустройства увязать с существующими транспортными и пешеходными путями смежных земельных участков и территорий " +
                    "общего пользования (при проектировании пешеходных путей обеспечить их максимальную непрерывность и возможность прямого прохода пешеходов).";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Нормативное количество автопарковочных мест и площадки,  необходимые для обслуживания проектируемых объектов, " +
                    "разместить в границах предоставленного земельного участка в соответствии с нормативами градостроительного проектирования муниципального " +
                    "образования «Город Вологда», утвержденными решением Вологодской городской Думы от 31 мая 2010 года № 357 (с последующими изменениями); " +
                    "размер машиноместа: 2,5 м x 5,3 м, для инвалидов 3,6 x 6,0 м.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Проектной документацией предусмотреть выделение не менее 10 процентов автопарковочных мест (но не менее одного места) " +
                    "для парковки специальных автотранспортных средств инвалидов, которые не должны занимать иные транспортные средства " +
                    "(согласно Федеральному закону от 24 ноября 1995 года № 181-ФЗ).";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Границы благоустройства принять в границах земельного участка.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Предусмотреть комплексное благоустройство проезжей части, тротуаров, газонов прилегающих к данному земельному участку.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Учесть требования СанПин 2.2.1/2.1.1.1200-03 «Санитарно-защитные зоны и санитарная классификация предприятий, сооружений " +
                    "и иных объектов». Предусмотреть необходимые разрывы до жилых и общественных зданий.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "Проектной документацией предусмотреть устройство подъездных путей к проектируемым объектам.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", n.ToString());
                n++;
                parcelInfo += "В проектной документации рекомендуется предусмотреть мероприятия, исключающие попадание атмосферных осадков с " +
                    "кровли проектируемых объектов на территорию смежных земельных участков. ";
                parcelInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
            {
                parcelInfo += String.Format("2.{0}. ", i.ToString());
                i++;
                parcelInfo += "Вертикальную планировку земельного участка увязать с проектными отметками смежных земельных участков " +
                    "(при наличии проектных решений), также исключить подтопление смежных территорий.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", i.ToString());
                i++;
                parcelInfo += "Нормативные разрывы от существующих инженерных сетей принять в соответствии  со СП 42.13330.2011.";
                parcelInfo += Environment.NewLine;
            }
            if (isCommunicationExist)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом || gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                    {
                        parcelInfo += String.Format("2.{0}. ", j.ToString());
                        j++;
                    }
                    else
                    {
                        parcelInfo += String.Format("2.{0}. ", i.ToString());
                        i++;
                    }
                    parcelInfo += "В случае выноса инженерных сетей (в соответствии с техническими условиями) требования по соблюдению охранных зон, " +
                        "указанные на чертеже градостроительного плана земельного участка и линий градостроительного регулирования, не применять.";
                    parcelInfo += Environment.NewLine;
                    if (isA12Zone)
                    {
                        if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                        {
                            parcelInfo += String.Format("2.{0}. ", j.ToString());
                            j++;
                        }
                        else
                        {
                            parcelInfo += String.Format("2.{0}. ", i.ToString());
                            i++;
                        }
                        parcelInfo += "Подземная прокладка инженерных коммуникаций, кроме транзитных и магистральных сетей, " +
                            "с обязательным соблюдением требований об охране культурного слоя.";
                        parcelInfo += Environment.NewLine;
                    }
                }
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
            {
                parcelInfo += String.Format("2.{0}. ", i.ToString());
                i++;
                parcelInfo += "Учесть требования СанПин 2.2.1/2.1.1.1200-03 «Санитарно-защитные зоны и санитарная классификация предприятий, сооружений и иных объектов».";
                parcelInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
            {
                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Проектной документацией предусмотреть выделение не менее 10 процентов автопарковочных мест (но не менее одного места) " +
                    "для парковки специальных автотранспортных средств инвалидов, которые не должны занимать иные транспортные средства " +
                    "(согласно Федеральному закону от 24 ноября 1995 года № 181-ФЗ).";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += String.Format("Проектной документацией предусмотреть устройство подъездных путей к проектируемому многоквартирному дому.",
                    gradplan.ObjectNameRod.ToLower() ?? "многоквартирному дому");
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Проектом предусмотреть стоянки для велосипедов, велосипедные дорожки (при необходимости).";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Учесть требования СанПин 2.2.1/2.1.1.1200-03 «Санитарно-защитные зоны и санитарная классификация предприятий, сооружений и иных объектов».";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Предусмотреть комплексное благоустройство проезжей части, тротуаров, газонов прилегающих к данному земельному участку.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "В проектной документации рекомендуется предусмотреть мероприятия, исключающие попадание атмосферных осадков " +
                    "с кровли проектируемого объекта на территорию смежных земельных участков.";
                parcelInfo += Environment.NewLine;

                parcelInfo += String.Format("2.{0}. ", j.ToString());
                j++;
                parcelInfo += "Предусмотреть выполнение норм посадки деревьев и кустарников на 1 га в соответствии с таблицей 6 Приказа " +
                    "Госстроя Российской Федерации от 15 декабря 1999 года N 153 \"Об утверждении Правил создания, охраны и содержания зеленых " +
                    "насаждений в городах Российской Федерации\": 80-100 штук деревья, 800-1000 штук кустарники для жилых территорий.";
                parcelInfo += Environment.NewLine;
            }
            if (isWaterZoneExist)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    parcelInfo += String.Format("2.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    parcelInfo += String.Format("2.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    parcelInfo += String.Format("2.{0}. ", n.ToString());
                    n++;
                }
                parcelInfo += "Учесть требования Водного кодекса Российской Федерации.";
                parcelInfo += Environment.NewLine;
            }
            if (isArtZoneExist)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    parcelInfo += String.Format("2.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    parcelInfo += String.Format("2.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    parcelInfo += String.Format("2.{0}. ", n.ToString());
                    n++;
                }
                parcelInfo += "Учесть требования СанПин 2.1.4.1110-02  «Зоны санитарной охраны источников водоснабжения и водопроводов питьевого назначения».";
                parcelInfo += Environment.NewLine;
            }


            gradplan.ParcelPlaningInfo = parcelInfo;
            gradplan.Save();
            connect.GetUnitOfWork().CommitChanges();
        }

        /// <summary>
        /// Создаем текст по иным показателям ГПЗУ по п. 3. Архитектурные решения
        /// </summary>
        /// <param name="gradplan"></param>
        /// <param name="connect"></param>
        /// <param name="m_MapInfo"></param>
        public void CreateArchSolutionsInfoText(GradPlan gradplan, Connect connect, MapInfoApplication m_MapInfo)
        {
            string archSolutionInfo = "";

            
            // из таблицы связи получаем Ид реестрового объекта ГПЗУ
            SpatialRepository objParcelLink = connect.FindFirstObject<SpatialRepository>(mc =>
                mc.IsogdClassName == gradplan.ClassInfo.FullName && mc.IsogdObjectId == gradplan.Oid.ToString());
            m_MapInfo.Do("Select * From " + objParcelLink.SpatialLayerId + " Where MI_PRINX = " + objParcelLink.SpatialObjectId + " Into zObject");

            // //Если земельный участок находится в водоохранной зоне реки Вологды (из ГИСа)
            bool isVologdaWaterZoneExist = false;
            string[] zoneLayerIds = { "Водоохранные_зоны" };
            foreach (string zoneLayerId in zoneLayerIds)
            {
                // ищем пересечение с нашим объектом (Partly Within, Intersects)
                m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0");// Into tmpTable");
                int selObjCount = 0;
                try { selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)")); }
                catch
                { }
                if (selObjCount > 0)
                {

                    try
                    {
                        if ((m_MapInfo.Eval("Selection.Название_реки")).Trim() == "Вологда")
                        {
                            isVologdaWaterZoneExist = true;
                            break;
                        }
                    }
                    catch { }
                   
                }
            }
            // лежит ли участок в зонах ОКН - (И-1 ... И-5), (И-2) или (И-3) 
            bool isI12345Zone = false;
            bool isI1Zone = false;
            bool isI2Zone = false;
            bool isI3Zone = false;
            bool isI5Zone = false;
            bool isI23Zone = false;
            bool isOKNZone = false;
            try
            {
                if (gradplan.MonumentZoneKind.Count > 0)
                {
                    isOKNZone = true;
                    foreach (MonumentZoneKind monumentZoneKind in gradplan.MonumentZoneKind)
                    {
                        try
                        {
                            if (monumentZoneKind.NameSmall == "И-1" || monumentZoneKind.NameSmall == "И-2" || monumentZoneKind.NameSmall == "И-3" ||
                                monumentZoneKind.NameSmall == "И-4" || monumentZoneKind.NameSmall == "И-5")
                            {
                                isI12345Zone = true;
                            }
                            if (monumentZoneKind.NameSmall == "И-1")
                            {
                                isI1Zone = true;
                            }
                            if (monumentZoneKind.NameSmall == "И-2")
                            {
                                isI2Zone = true;
                            }
                            if (monumentZoneKind.NameSmall == "И-3")
                            {
                                isI3Zone = true;
                            }
                            if (monumentZoneKind.NameSmall == "И-5")
                            {
                                isI5Zone = true;
                            }
                        }
                        catch { }
                    }
                }
            }
            catch { }
            if(isI2Zone && isI3Zone)
            {
                isI3Zone = false;
                isI23Zone = true;
            }

            // собираем теперь весь текст 
            int i = 1;
            int j = 1;
            int n = 1;

            if (isVologdaWaterZoneExist)
            {
                archSolutionInfo = "3.1. Учесть размещение объекта в непосредственной близости реки Вологды - одного из важных природных элементов формирующих общегородскую среду.";
                archSolutionInfo += Environment.NewLine;
                i++;
                j++;
                n++;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
            {
                archSolutionInfo += String.Format("3.{0}. Архитектурные и колористические решения {1}, а также благоустройство " + 
                    "земельного участка должны создавать гармоничное  и слаженное жизненное пространство.", 
                    j.ToString(), gradplan.ObjectNameRod.ToLower() ?? "многоквартирного дома");
                j++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. Облицовку фасадов и покрытие кровли выполнить из качественных отделочных материалов, " + 
                    "материал стен и кровли решить проектной документацией.",
                    j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. Проектной документацией предусмотреть обеспечение полноценного доступа маломобильных групп " + 
                    "населения в проектируемый многоквартирный дом, включающего в себя беспрепятственное перемещение как на территории земельного " + 
                    "участка с понижением бордюрного камня в местах пересечения тротуаров с проезжей частью, устройством пандусов,  так и  " + 
                    "внутри многоквартирного дома в соответствии со СП 59.13330.2012, Федеральным законом от 24 ноября 1995 года № 181 - ФЗ.",
                    j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
            {
                archSolutionInfo += String.Format("3.{0}. Архитектурные и колористические решения {1}, а также благоустройство " + 
                    "земельного участка должны создавать гармоничное и слаженное жизненное пространство.",
                    i.ToString(), gradplan.ObjectNameRod.ToLower() ?? "индивидуального жилого дома");
                i++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. Объёмно - планировочное решение, облицовку фасадов и покрытие кровли выполнить из качественных отделочных материалов.",
                    i.ToString());
                i++;
                archSolutionInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
            {
                archSolutionInfo += String.Format("3.{0}. Архитектурные и колористические решения, а также благоустройство " +
                    "земельного участка должны создавать гармоничное жизненное пространство.",
                    n.ToString());
                n++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. Облицовку фасадов и покрытие кровли выполнить из качественных отделочных материалов, " + 
                    "материал стен и кровли решить проектной документацией.",
                    n.ToString());
                n++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. При разработке объемно-планировочного решения рекомендуется использовать материалы и " + 
                    "конструктивные схемы, позволяющие повысить энергоэффективность проектируемого объекта.",
                   n.ToString());
                n++;
                archSolutionInfo += Environment.NewLine;
            }
            if (isI12345Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом || gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                    {
                        archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                        i++;
                    }
                    if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                    {
                        archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                        n++;
                    }
                    archSolutionInfo += "Объёмно-планировочное решение, облицовку фасадов и покрытие кровли выполнить с учетом " + 
                        "требований градостроительных регламентов в границах зон охраны объектов культурного наследия, расположенных на " + 
                        "территории г. Вологды, и режимов использования земель на территориях памятников и в границах зон охраны объектов " + 
                        "культурного наследия, расположенных на территории г. Вологды, утвержденных постановлением Правительства Вологодской " + 
                        "области от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                    archSolutionInfo += Environment.NewLine;
                }
            }

            if(isI2Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                { archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Проектной документацией предусмотреть строительство здания на основе историко-архивных материалов " + 
                    "и аналогов исторической застройки, в деревянном либо кирпичном (с обшивкой деревом, в отдельных случаях – без обшивки) " + 
                    "исполнении в один этаж с вальмовой кровлей (до 8,0 метров до конька кровли и до 4,7 метров до карниза от существующего " + 
                    "уровня поверхности земли), либо строительство зданий на основе историко-архивных материалов в деревянном либо кирпичном " + 
                    "(с обшивкой деревом, в отдельных случаях - без обшивки) исполнении в два этажа с вальмовой кровлей, в исключительных случаях " + 
                    "(до 12.0 м до конька кровли и до 8.0 м до карниза от существующего уровня поверхности земли)  с обязательным рассмотрением и " + 
                    "получением рекомендаций в уполномоченном органе в сфере охраны объектов культурного наследия Вологодской области в соответствии " + 
                    "с градостроительными регламентами в границах зон охраны объектов культурного наследия, расположенных на территории г. Вологды, и " + 
                    "режимами использования земель на территориях памятников и в границах зон охраны объектов культурного наследия, расположенных на " + 
                    "территории г. Вологды, утвержденными постановлением Правительства Вологодской области от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;
            }
            if (isI3Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Проектной документацией предусмотреть строительство здания на основе историко-архивных материалов " +
                    "и аналогов исторической застройки, в деревянном либо кирпичном (с обшивкой деревом, в отдельных случаях – без обшивки) " +
                    "исполнении в один этаж с вальмовой кровлей (до 8,0 метров до конька кровли и до 4,7 метров до карниза от существующего " +
                    "уровня поверхности земли), либо строительство зданий на основе историко-архивных материалов в деревянном либо кирпичном " +
                    "(с обшивкой деревом, в отдельных случаях - без обшивки) исполнении в два этажа с вальмовой кровлей, в исключительных случаях " +
                    "(до 12.0 м до конька кровли и до 8.0 м до карниза от существующего уровня поверхности земли)  с обязательным рассмотрением и " +
                    "получением рекомендаций в уполномоченном органе в сфере охраны объектов культурного наследия Вологодской области в соответствии " +
                    "с градостроительными регламентами в границах зон охраны объектов культурного наследия, расположенных на территории г. Вологды, и " +
                    "режимами использования земель на территориях памятников и в границах зон охраны объектов культурного наследия, расположенных на " +
                    "территории г. Вологды, утвержденными постановлением Правительства Вологодской области от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
            {
                if (!isOKNZone)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                    archSolutionInfo += "При проектировании фасадов в случае использования традиционных отделочных материалов: кирпича, штукатурки использовать " +
                        "облицовочный керамический кирпич, штукатурку применять композиционно для поэтажного или фрагментарного покрытия; также для наружной " +
                        "отделки могут применятся плитные фасадные элементы с целью создания выразительного архитектурно-художественного облика с условием полного закрытия фасадов.";
                    archSolutionInfo += Environment.NewLine;
                }
                archSolutionInfo += String.Format("3.{0}. Рисунок заполнения оконных проемов, витражей принять в соответствии с выбранным " + 
                    "архитектурно-художественным и стилевым решением.", j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;

                if (!isOKNZone)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                    archSolutionInfo += "Проектом предусмотреть места установки кондиционеров, спутниковых тарелок, флагштоков, указателей, " + 
                        "рекламных конструкций и вывесок (при их необходимости).";
                    archSolutionInfo += Environment.NewLine;
                }
                else
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                    archSolutionInfo += "Проектом предусмотреть места установки кондиционеров, спутниковых тарелок, флагштоков, указателей, " + 
                        "рекламных конструкций и вывесок (при условии соответствия градостроительным регламентам в границах зон охраны объектов " + 
                        "культурного наследия, расположенных на территории г. Вологды, и режимам использования земель на территориях памятников и " + 
                        "в границах зон охраны объектов культурного наследия, расположенных на территории г. Вологды, утвержденным постановлением " + 
                        "Правительства Вологодской области от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                    archSolutionInfo += Environment.NewLine;
                }

                archSolutionInfo += String.Format("3.{0}. При проектировании предусмотреть остекление лоджий (балконов)с учетом Федерального закона от " + 
                    "22 июля 2008 года № 123 - ФЗ «Технический регламент о требованиях пожарной безопасности».", j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. В первых этажах рекомендуется предусмотреть размещение магазинов розничной торговли продуктами питания по социально низким ценам.", j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. Предлагаем разместить в жилых зданиях встроенные или пристроенные помещения для детских садов.", j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;

                archSolutionInfo += String.Format("3.{0}. При разработке объемно - планировочного решения рекомендуется использовать материалы и " + 
                    "конструктивные схемы, позволяющие повысить энергоэффективность проектируемого объекта.", j.ToString());
                j++;
                archSolutionInfo += Environment.NewLine;
            }
            if (isI2Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Размещение воздушных кабельных линий, мачтовых конструкций, спутниковых устройств и кондиционеров " + 
                    "на главных фасадах и кровлях зданий запрещено в соответствии с градостроительными регламентами в границах зон охраны " + 
                    "объектов культурного наследия, расположенных на территории  г. Вологды,  и режимами использования земель на территориях " + 
                    "памятников и в границах зон   охраны объектов культурного наследия, расположенных на территории г. Вологды, утвержденными " + 
                    "постановлением Правительства Вологодской области от 28 декабря 2009 года № 2087   (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;
            }
            if (isI3Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Строительство зданий с плоской кровлей, размещение воздушных кабельных линий, мачтовых конструкций, " + 
                    "спутниковых устройств и кондиционеров на главных фасадах и кровлях зданий запрещено в соответствии с градостроительными регламентами " + 
                    "в границах зон охраны объектов культурного наследия, расположенных на территории  г. Вологды,  и режимами использования земель на " + 
                    "территориях памятников и в границах зон   охраны объектов культурного наследия, расположенных на территории г. Вологды, утвержденными " + 
                    "постановлением Правительства Вологодской области от 28 декабря 2009 года № 2087   (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;
            }
            if (isI23Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Запрещено строительство гаражей и хозяйственных построек, кроме случаев, когда объект выполняется на основании " + 
                    "заключения, выданного уполномоченным органом в сфере охраны объектов культурного наследия Вологодской области.";
                archSolutionInfo += Environment.NewLine;
            }
            if (isI1Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Прокладка транзитных коммуникаций, размещение воздушных кабельных линий, мачтовых конструкций, " + 
                    "спутниковых устройств и кондиционеров на главных фасадах и кровлях зданий, применение металлопластиковой профилированной " + 
                    "черепицы, пластикового сайдинга, в том числе винилового, зеркального остекления фасадов, тонированного стекла, глянцевых фасадных " + 
                    "покрытий, за исключением натурального камня запрещены в соответствии с градостроительными регламентами в границах зон охраны объектов " + 
                    "культурного наследия, расположенных на территории  г. Вологды,  и режимами использования земель на территориях памятников и в границах " + 
                    "зон охраны объектов культурного наследия, расположенных на территории г. Вологды, утвержденными постановлением Правительства " + 
                    "Вологодской области от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;

                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Запрещена окраска фасадов зданий и  сооружений в яркие и контрастные цвета в соответствии с градостроительными " + 
                    "регламентами в границах зон охраны объектов культурного наследия, расположенных на территории  г. Вологды,  и режимами использования " + 
                    "земель на территориях памятников и в границах зон   охраны объектов культурного наследия, расположенных на территории г. Вологды, " + 
                    "утвержденными постановлением Правительства Вологодской области от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;
            }
            if (isI5Zone)
            {
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", j.ToString());
                    j++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
                {
                    archSolutionInfo += String.Format("3.{0}. ", i.ToString());
                    i++;
                }
                if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
                {
                    archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                    n++;
                }
                archSolutionInfo += "Запрещено размещение мачтовых конструкций, спутниковых устройств и кондиционеров на главных фасадах и кровлях " + 
                    "зданий в соответствии с градостроительными регламентами в границах зон охраны объектов культурного наследия, расположенных на " + 
                    "территории  г. Вологды,  и режимами использования земель на территориях памятников и в границах зон   охраны объектов культурного " + 
                    "наследия, расположенных на территории г. Вологды, утвержденными постановлением Правительства Вологодской области " + 
                    "от 28 декабря 2009 года № 2087 (с последующими изменениями).";
                archSolutionInfo += Environment.NewLine;
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
            {
                archSolutionInfo += String.Format("3.{0}. ", n.ToString());
                n++;
                archSolutionInfo += "Проектной документацией предусмотреть обеспечение полноценного доступа маломобильных групп населения в " + 
                    "проектируемое здание, включающего в себя беспрепятственное перемещение как на территории земельного участка с понижением " + 
                    "бордюрного камня в местах пересечения тротуаров с проезжей частью, устройством пандусов,  так и  внутри административного " + 
                    "здания в соответствии со СП 59.13330.2012, Федеральным законом от 24 ноября 1995 года № 181-ФЗ (при обеспечении рабочими местами маломобильных групп населения).";
                archSolutionInfo += Environment.NewLine;
            }
            archSolutionInfo = archSolutionInfo.TrimEnd(Environment.NewLine.ToCharArray());
            gradplan.ArchSolutionsInfo = archSolutionInfo;
            gradplan.Save();
            connect.GetUnitOfWork().CommitChanges();
        }

        /// <summary>
        /// Создаем текст по иным показателям ГПЗУ по п. 4 - ... 
        /// </summary>
        /// <param name="gradplan"></param>
        /// <param name="connect"></param>
        /// <param name="m_MapInfo"></param>
        public void CreateAnotherInfoText(GradPlan gradplan, Connect connect, MapInfoApplication m_MapInfo)
        {
            string anotherInfo = "";
            // лежит ли участок в зонах ОКН - (И-1 ... И-5) 
            bool isI12345Zone = false;
            try
            {
                if (gradplan.MonumentZoneKind.Count > 0)
                {
                    foreach (MonumentZoneKind monumentZoneKind in gradplan.MonumentZoneKind)
                    {
                        try
                        {
                            if (monumentZoneKind.NameSmall == "И-1" || monumentZoneKind.NameSmall == "И-2" || monumentZoneKind.NameSmall == "И-3" ||
                                monumentZoneKind.NameSmall == "И-4" || monumentZoneKind.NameSmall == "И-5")
                            {
                                isI12345Zone = true;
                            }
                        }
                        catch { }
                    }
                }
            }
            catch { }

            // собираем теперь весь текст 
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое || gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСНеЖилое)
            {
                anotherInfo += "4. Проектом организации строительства предусмотреть:";
                anotherInfo += Environment.NewLine;
                anotherInfo += Environment.NewLine;

                anotherInfo += @"- устройство подъездов с твердым покрытием, в местах выезда транспорта со стройплощадки предусмотреть очистку (мойку) колес;
- установку информационного щита (размером не менее чем 3х4 м) с указанием застройщика(заказчика), подрядчика, их контактный телефон, изображением будущего объекта строительства и указанием сроков окончания строительства;
- установку по периметру земельного участка временного ограждения из оцинкованного (окрашенного)профилированного металлического листа с обязательной установкой козырьков в местах, организованных для проходов(высота прохода в чистоте не менее 2 метров);
- мероприятия, обеспечивающие выполнение нормативных требований охраны труда и охраны окружающей среды в период строительства, а также исключение доступа посторонних лиц на территорию строительной площадки;
- после завершения строительства восстановить нарушенное благоустройство  смежных земельных участков;
-  в случае повреждения проездов, использовавшихся в качестве подъездных путей, восстановить дорожное покрытие.

5. Проектную документацию подготовить в соответствии с Положением о составе разделов проектной документации и требованиях к их содержанию, утвержденным постановлением Правительства Российской Федерации от 16 февраля 2008 года № 87 (с последующими изменениями), в соответствии с ГОСТ Р 21.1101-2013 и ГОСТ 21 508-93.                
";
                anotherInfo += Environment.NewLine;
                anotherInfo += Environment.NewLine;
                int i = 6;
                if (isI12345Zone)
                {
                    if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ОКСЖилое)
                    {
                        anotherInfo += "6. В соответствии со статьей 51 Градостроительного кодекса Российской Федерации необходимо получить заключение " +
                              "уполномоченного органа в сфере охраны объектов культурного наследия Вологодской области о соответствии предмету охраны " +
                              "исторического поселения и требованиям к архитектурным решениям объектов капитального строительства, установленным " +
                              "градостроительным регламентом применительно к территориальной зоне, расположенной в границах территории исторического поселения.";
                        i++;
                        anotherInfo += Environment.NewLine;
                        anotherInfo += Environment.NewLine;
                    }
                    anotherInfo += i.ToString() + ". Проектную документацию рекомендуется направить на рассмотрение в Департамент градостроительства " + 
                        "Администрации города Вологды за 30 дней до подачи заявления о выдаче разрешения на строительство, " + 
                        "а также в соответствии с постановлением Правительства Вологодской области от 28 декабря 2009 года № 2087 (с последующими изменениями)" + 
                        "в уполномоченный орган в сфере охраны объектов культурного наследия Вологодской области.";
                }
                else
                {
                    anotherInfo += "6. Проектную документацию рекомендуется направить на рассмотрение в Департамент градостроительства Администрации города Вологды за 30 дней до подачи заявления о выдаче разрешения на строительство.";
                }
            }
            if (gradplan.GPZUBuildingKind == AISOGD.Enums.eGPZUBuildingKind.ИндивидуальныйЖилойДом)
            {
                anotherInfo += @"4. При строительстве объекта предусмотреть мероприятия, обеспечивающие выполнение нормативных требований охраны труда и охраны окружающей среды, а также исключение доступа посторонних лиц на территорию строительной площадки.
После завершения строительства восстановить нарушенное благоустройство  смежных земельных участков; в случае повреждения проездов, использовавшихся в качестве подъездных путей, восстановить дорожное покрытие.

Предусмотреть согласование с Департаментом городского хозяйства Администрации города Вологды проездов к строительной площадке на период строительства.
";
            }
                
            gradplan.AnotherInfo = anotherInfo;
            gradplan.Save();
            connect.GetUnitOfWork().CommitChanges();
        }
    }
}
