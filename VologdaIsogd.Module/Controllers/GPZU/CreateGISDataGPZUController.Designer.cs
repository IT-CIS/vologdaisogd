﻿namespace VologdaIsogd.Module.Controllers.GPZU
{
    partial class CreateGISDataGPZUController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateGISDataGPZUAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CreateGISDataGPZUAction
            // 
            this.CreateGISDataGPZUAction.Caption = "Подготовка ГПЗУ";
            this.CreateGISDataGPZUAction.ConfirmationMessage = null;
            this.CreateGISDataGPZUAction.Id = "CreateGISDataGPZUAction";
            this.CreateGISDataGPZUAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.CreateGISDataGPZUAction.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.CreateGISDataGPZUAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CreateGISDataGPZUAction.ToolTip = null;
            this.CreateGISDataGPZUAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CreateGISDataGPZUAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateGISDataGPZUAction_Execute);
            // 
            // CreateGISDataGPZUController
            // 
            this.Actions.Add(this.CreateGISDataGPZUAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CreateGISDataGPZUAction;
    }
}
