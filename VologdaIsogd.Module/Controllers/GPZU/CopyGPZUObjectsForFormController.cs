﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.GPZU;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CopyGPZUObjectsForFormController : ViewController
    {
        public CopyGPZUObjectsForFormController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        public void CopyGPZUObjects(IObjectSpace os)
        {
            //IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();

            SpatialObject GradPlanSpatialObject = null;

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return;
            }
            // получаем выделение
            SelectionTable selectionTable = m_MapInfo.SelectionTable;// new SelectionTable(m_MapInfo);
            string gradPlanObjId = selectionTable.GetFirstID();
            string gradPlanLayerId = selectionTable.Name;
            string cadNo = "";
            // ищем связанный реестровый объект ГПЗУ
            string gpoid = connect.FindFirstObject<SpatialRepository>(x=> x.SpatialLayerId == gradPlanLayerId && x.SpatialObjectId == gradPlanObjId).IsogdObjectId;
            GradPlan gradPlan = connect.FindFirstObject<GradPlan>(x => x.Oid.ToString() == gpoid);
            if(gradPlan == null)
            {
                XtraMessageBox.Show("Не найдена реестровая карточка объекта ГПЗУ. Убедитесь, что ГИС объект связан с реестровой карточкой градпалан.");
                return;
            }
            try { cadNo = gradPlan.LotCadNo; }
            catch { }
            cadNo = cadNo.Trim();
            if (cadNo == "")
            {
                XtraMessageBox.Show("В реестровой карточке Градоплана не указан кадастровый номер участка. Укажите его и попробуйте снова!");
                return;
            }
            // объявляем в МапИнфо переменные для копирования стиля закраски
            try
            {
                m_MapInfo.Do(@"Dim myBrush as Brush");
            }
            catch { }
            try
            {
                m_MapInfo.Do(@"Dim myNewObj as Object");
            }
            catch { }

            string[] Layers = { "ГПЗУ_Береговая", "ГПЗУ_ВОДОохранная", "ГПЗУ_Зоны_АНТЕНН", "ГПЗУ_Зоны_АРТ_СКВАЖИН",
            "ГПЗУ_Зоны_Пунктов_Полигонометр", "ГПЗУ_Линия_застройки","ГПЗУ_Номера_объектов", "ГПЗУ_размеры","ГПЗУ_СЗЗ", "ГПЗУ_Территория_паводка",
            "ГПЗУ_Береговая", "Поворотные_точки", "ГПЗУ", "ОхрЗоны_СЕТЕЙ", "пятно_застройки", "ГПЗУ_Зоны_ОКН"};

            foreach (string l in Layers)
            {
                string originalTableName = l;
                string toCopyTableName = l + "_форм";
                if (l == "ГПЗУ_Зоны_Пунктов_Полигонометр")
                    toCopyTableName = "ГПЗУ_Зоны_Пунктов_форм";
                //if (l == "Поворотные_точки")
                //    toCopyTableName = "точки_тест";
                TableInfo originalTableInfo = new TableInfo(m_MapInfo, originalTableName);
                TableInfoTemp sourceTempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                //выбрать пересекающиеся объекты
                TableInfo intersectsTempTable = new TableInfo(m_MapInfo, "intersectsTempTable");
                //если контур ГПЗУ - выбираем именно наш контур
                if (originalTableName == "ГПЗУ")
                {
                    m_MapInfo.Do(string.Format("Select * From {0} Where MI_PRINX={2} Into {3}",
                           originalTableName, gradPlanLayerId, gradPlanObjId, intersectsTempTable.Name));
                }
                //если Поворотные_точки - ищем именно те, которые относятся к нашему ЗУ
                else if (originalTableName == "Поворотные_точки")
                {
                    m_MapInfo.Do(string.Format("Select * From {0} Where Кадастровый_номер = \"{1}\" Into {2}",
                            originalTableName, cadNo, intersectsTempTable.Name));
                }
                else
                {
                    m_MapInfo.Do(string.Format("Select * From {0} Where obj Intersects (Select obj From {1} Where MI_PRINX = {2}) Into {3}",
                            originalTableName, gradPlanLayerId, gradPlanObjId, intersectsTempTable.Name));
                }

                Console.WriteLine("originalTableName = " + originalTableName);
                //Колличество пересекающихся объектов
                long rowCount = intersectsTempTable.GetRowCount() ?? 0;
                Console.WriteLine("Колличество пересекающихся объектов = " + rowCount);

                for (int i = 1; i <= rowCount; i++)
                {


                    //Выделить пересекающийся объект и поместить его в темп таблицу sourceTempTable
                    m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1} Into {2}", intersectsTempTable.Name, i, sourceTempTable.Name));

                    // получить его стиль
                    try
                    {
                        m_MapInfo.Do(string.Format("myBrush  = ObjectInfo({0}.obj, 3)", sourceTempTable.Name)); //
                    }
                    catch { }

                    // клонировать выделенный объект в слой назначения 
                    m_MapInfo.Do(string.Format("Insert Into {0} (Obj) Select Obj From {1}",
                toCopyTableName, sourceTempTable.Name));



                    //получить RowID нового объекта
                    m_MapInfo.Do("Fetch Last From " + toCopyTableName);
                    string newRowID = m_MapInfo.Eval(toCopyTableName + ".RowID");

                    // для номера и точек ГПЗУ клонируем семантику
                    if (originalTableName == "ГПЗУ_Номера_объектов" || originalTableName == "Поворотные_точки")
                    {
                        int? columnCount = originalTableInfo.GetColumnCount();
                        if (columnCount != null)
                        {
                            for (int j = 1; j <= columnCount; j++)
                            {
                                //try
                                //{
                                if (originalTableInfo.ColumnInfo(j).Name != "MI_PRINX")
                                {
                                    m_MapInfo.Do(string.Format("Fetch First From {0}", sourceTempTable.Name));
                                    string value = m_MapInfo.Eval(string.Format("{0}.{1}", sourceTempTable.Name, originalTableInfo.ColumnInfo(j).Name));
                                    if (!string.IsNullOrWhiteSpace(value))
                                    {
                                        m_MapInfo.Do(string.Format("Update {0} Set {1} = \"{2}\" Where RowID = {3}",
                                            toCopyTableName, originalTableInfo.ColumnInfo(j).Name, value, newRowID));

                                    }
                                }
                                //}
                                //catch (Exception ex) { }
                            }
                        }
                    }
                    try
                    {
                        // присваеваем новый стиль
                        ////Снять выделение
                        //m_MapInfo.SelectionTable.Close();

                        // теперь выбираем нашу скопированную в рабочий набор зону, назначем ей стиль(оформление) и режем ее
                        m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1}", toCopyTableName, newRowID));
                        m_MapInfo.Do("myNewObj = selection.obj");
                        m_MapInfo.Do("Alter Object myNewObj Info 3, myBrush");
                        // Сохраняем данные
                        m_MapInfo.Do(string.Format("Update {0} Set obj = myNewObj Where RowID = {1}", toCopyTableName, newRowID));
                    }
                    catch { }
                    // Сохраняем данные
                    m_MapInfo.Do(string.Format("Commit table {0}", toCopyTableName));
                }
            }
            //Закрыть все слои запросов
            int NumTables = Convert.ToInt16(m_MapInfo.Eval("NumTables()"));
            List<string> queryTablesList = new List<string>();
            for (int k = 1; k <= NumTables; k++)
            {
                TableInfo TableToClose = new TableInfo(m_MapInfo, k);
                if (TableToClose.Name.IndexOf("Query") != -1 || TableToClose.Name.IndexOf("IntersectsTempTable") != -1)
                    queryTablesList.Add(TableToClose.Name);
            }
            foreach (string queryTable in queryTablesList)
            {
                try
                {
                    m_MapInfo.TableInfo(queryTable).Dispose();
                }
                catch { }
            }
        }

        /// <summary>
        /// удалить все темп объекты из слоев для печати схемы
        /// </summary>
        /// <param name="m_MapInfo"></param>
        public void DelGPZUTempObjects()
        {
            MapInfoApplication m_MapInfo = new MapInfoApplication();
            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return;
            }
            string[] Layers = { "ГПЗУ_Береговая_форм", "ГПЗУ_ВОДОохранная_форм", "ГПЗУ_Зоны_АНТЕНН_форм", "ГПЗУ_Зоны_АРТ_СКВАЖИН_форм",
            "ГПЗУ_Зоны_Пунктов_форм", "ГПЗУ_Линия_застройки_форм","ГПЗУ_Номера_объектов_форм", "ГПЗУ_размеры_форм","ГПЗУ_СЗЗ_форм", "ГПЗУ_Территория_паводка_форм",
            "ГПЗУ_Береговая_форм", "Поворотные_точки_форм", "ГПЗУ_форм", "ОхрЗоны_СЕТЕЙ_форм", "пятно_застройки_форм", "ГПЗУ_Зоны_ОКН_форм"};

            foreach (string l in Layers)
            {
                try
                {
                    // удаляем все объекты из таблиц
                    m_MapInfo.Do(string.Format("Delete From {0} Commit table {0} Pack table {0}",
                    l));
                }
                catch { }
            }
        }
    }
}
