namespace WinAisogdIngeo.Module.Controllers
{
    partial class GpzuReportController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GradPlanReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // GradPlanReportAction
            // 
            this.GradPlanReportAction.Caption = "������������";
            this.GradPlanReportAction.ConfirmationMessage = null;
            this.GradPlanReportAction.Id = "GradPlanReportAction";
            this.GradPlanReportAction.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.GradPlanReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.GradPlanReportAction.ToolTip = null;
            this.GradPlanReportAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.GradPlanReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GradPlanReportAction_Execute);
            // 
            // GpzuReportController
            // 
            this.Actions.Add(this.GradPlanReportAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction GradPlanReportAction;
    }
}
