﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.GPZU;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.Land;
using AISOGD.Reglament;
using AISOGD.Coordinates;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateGPZUController : ViewController
    {
        public CreateGPZUController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция создания реестровой карточки Градплана, отрисовки контура ГПЗУ (по ЗУ), определение зон 
        /// 1. Создаем реестровую карточку ГПЗУ, подцепляем к ней ЗУ
        /// 2. Отрисовываем контур ГПЗУ
        /// 3. Определяем зоны (терр, ОКН, ограничений), попавшие в контур
        /// 4. Определение красных линий
        /// </summary>
        /// <param name="connect"></param>
        /// <returns></returns>
        public GradPlan CreateGradPlanDoc(Connect connect)
        {
            GradPlan gradPlan = null;
            Parcel parcel = null;


            var spatial = new SpatialController<GisLayer>(connect);
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();

            SpatialObject ParcelSpatialObject = null;

            if (m_MapInfo.mapinfo == null)
            {
                XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                return null;
            }

            string parcelLayer = "";
            // получаем название слоя выделения
            SelectionTable selectionTable = m_MapInfo.SelectionTable;// new SelectionTable(m_MapInfo);
            parcelLayer = selectionTable.Name;
            TableInfoTemp startSelectObjects = new TableInfoTemp(m_MapInfo, ETempTableType.Temp);
            m_MapInfo.Do(String.Format("Select * From Selection into {0}", startSelectObjects.Name));
            if (parcelLayer == "")
            {
                XtraMessageBox.Show("Не удалось получить название таблицы выделенных объектов. Пожалуйста, обратитесь к администратору Системы!", "Ошибка");
                return null;
            }
            // получаем название пространственного слоя для ГПЗУ
            string gradplanLayerId = "";
            gradplanLayerId = spatial.GetListLinkedSpatialLayers("AISOGD.GPZU.GradPlan")[0];
            if (gradplanLayerId == "")
            {
                XtraMessageBox.Show("Не удалось получить пространственные настройки связи для ГПЗУ. Пожалуйста, обратитесь к администратору Системы!", "Ошибка");
                return null;
            }
            m_MapInfo.Fetch(EFetch.Rec, startSelectObjects.Name, 1);

            // определяем земельный участок
            try
            {
                string gisObjParcelId = selectionTable.GetFirstID();//m_MapInfo.Eval("Selection.MI_PRINX");
                if (gisObjParcelId == "0")
                {
                    XtraMessageBox.Show("Имеются несохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                    return null;
                }
                ParcelSpatialObject = new SpatialObject(m_MapInfo, parcelLayer, gisObjParcelId);
                // находим объект выделенного земельного участка
                //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
                // этапа, если нет - добавляем
                // из таблицы связи получаем Ид реестрового объекта ЗУ
                SpatialRepository objParcelLink = connect.FindFirstObject<SpatialRepository>(mc =>
                    mc.SpatialLayerId == parcelLayer &&
                    mc.SpatialObjectId == gisObjParcelId &&
                    mc.IsogdClassName == "AISOGD.Land.Parcel");

                if (objParcelLink != null)
                {
                    // по ИД находим сам реестровый объект
                    parcel = connect.FindFirstObject<Parcel>(mc => mc.Oid.ToString() == objParcelLink.IsogdObjectId);
                }
                else
                {
                    try { parcel = connect.FindFirstObject<Parcel>(mc => mc.CadastralNumber == m_MapInfo.Eval(startSelectObjects.Name + ".CADASTRAL_NUMBER")); }
                    catch { }
                    if (parcel == null)
                    {
                        parcel = connect.CreateObject<Parcel>();
                        try { parcel.CadastralNumber = m_MapInfo.Eval(startSelectObjects.Name + ".CADASTRAL_NUMBER"); }
                        catch { }
                        parcel.Save();
                    }
                    spatial.AddSpatialLink(parcel.ClassInfo.FullName, parcel.Oid.ToString(), parcelLayer, gisObjParcelId);
                }
                if (parcel.Area == 0)
                {
                    try
                    {
                        string area = (m_MapInfo.Eval(startSelectObjects.Name + ".AREA")).Split('+')[0];
                        area = area.Trim();
                        parcel.Area = Convert.ToDouble(area);
                    }
                    catch { }
                    try
                    {
                        parcel.Unit = connect.FindFirstObject<dUnit>(x => x.Name == "кв.м");
                    }
                    catch { }
                    parcel.Save();
                }
            }
            catch { }

            gradPlan = connect.CreateObject<GradPlan>();
            gradPlan.Parcels.Add(parcel);
            gradPlan.Save();
            //XtraMessageBox.Show("1");
            connect.GetUnitOfWork().CommitChanges();

            // добавляем координатные точки в ГПЗУ
            CreateCoordinates(m_MapInfo, gradPlan.LotCadNo, gradPlan, connect);
            try { }
            catch { }

            //XtraMessageBox.Show("2");
            if (ParcelSpatialObject != null)
            {
                // таблица ГПЗУ
                TableInfo gpzuTableInfo = new TableInfo(m_MapInfo, gradplanLayerId);
                // Возвращает RowID созданного объекта Градплана
                //XtraMessageBox.Show("3");
                string rowGradPlanID = ParcelSpatialObject.CloneSpatialObject(gradplanLayerId, false);
                // выбираем сам ГИС объект ГПЗУ и связываем его с реестровым
                //сохраняем изменения в слое ГПЗУ
                //XtraMessageBox.Show("4");
                m_MapInfo.Do("Commit table " + gradplanLayerId + " Automatic ApplyUpdates");
                //XtraMessageBox.Show("5");
                //Выбрать объект во временную таблицу
                TableInfoTemp tempGPZUTableInfo = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
                m_MapInfo.Select("*", gradplanLayerId, "RowID", rowGradPlanID, tempGPZUTableInfo.Name);
                // получить ID выделенного объекта
                string gisObjGPZUId = m_MapInfo.SelectionTable.GetFirstID();//m_MapInfo.Eval("Selection.MI_PRINX");
                if (gisObjGPZUId == "0")
                {
                    XtraMessageBox.Show("Имеются несохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                    return null;
                }

                // добавить связь с реестровым
                spatial.AddSpatialLink(gradPlan.ClassInfo.FullName, gradPlan.Oid.ToString(), gradplanLayerId, gisObjGPZUId);

                try
                {
                    tempGPZUTableInfo.Dispose();
                }
                catch { }
                m_MapInfo.Do("Drop Table Selection");
                // 3.1 ищем пересечение с террзонами
                // делаем пространственные запросы по террзонам
                try
                {
                    //Сохранить выделенный объект в выборку
                    //TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
                    //m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));

                    m_MapInfo.Do("Select * From " + gradplanLayerId + " Where MI_PRINX = " + gisObjGPZUId + " Into zObject");
                    // получаем слои для террзон
                    TerrZone terrZone;
                    string zoneClassName = "AISOGD.Reglament.TerrZone";
                    List<string> zoneLayerIds = spatial.GetListLinkedSpatialLayers(zoneClassName);
                    foreach (string zoneLayerId in zoneLayerIds)
                    {
                        // ищем пересечение с нашим объектом (Partly Within, Intersects)
                        m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
                        int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                        for (int j = 1; j < selObjCount1 + 1; j++)
                        {
                            m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable");
                            // Id(MI_PRINX) пространственного объекта ЗУ
                            string zoneReqestObjID = m_MapInfo.Eval("tmpTable.MI_PRINX");
                            // находим ЗУ
                            //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
                            // этапа, если нет - добавляем
                            // из таблицы связи получаем Ид реестрового объекта ЗУ
                            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                             mc.SpatialLayerId == zoneLayerId &&
                             mc.SpatialObjectId == zoneReqestObjID &&
                             mc.IsogdClassName == zoneClassName);
                            if (objLink != null)
                            {
                                // по ИД находим сам реестровый объект
                                terrZone = connect.FindFirstObject<TerrZone>(mc => mc.Oid.ToString() == objLink.IsogdObjectId);

                                if (terrZone != null)
                                {
                                    if (terrZone.TerrZoneKind != null)
                                    {
                                        bool isZoneExist = false;
                                        // перебираем дочерние виды зон у градплана и если такой нет - добавляем ссылку
                                        foreach (TerrZoneKind terrZoneKind in gradPlan.TerrZoneKind)
                                        {
                                            if (terrZone.TerrZoneKind == terrZoneKind)
                                                isZoneExist = true;
                                        }
                                        if (!isZoneExist)
                                        {
                                            TerrZoneKind terrZoneKind = terrZone.TerrZoneKind;
                                            gradPlan.TerrZoneKind.Add(terrZoneKind);
                                            gradPlan.Save();
                                            connect.GetUnitOfWork().CommitChanges();
                                        }
                                    }
                                }
                            }
                        }
                        try
                        {
                            gradPlan.TerrZoneKindLink = gradPlan.TerrZoneKind.First();
                        }
                        catch { }
                    }
                }
                catch { }

                // 3.2 ищем пересечение с ОКН
                try
                {
                    //Сохранить выделенный объект в выборку
                    //TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
                    //m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));

                    m_MapInfo.Do("Select * From " + gradplanLayerId + " Where MI_PRINX = " + gisObjGPZUId + " Into zObject");
                    // получаем слои для террзон
                    MonumentZone zone;
                    string zoneClassName = "AISOGD.Reglament.MonumentZone";
                    List<string> zoneLayerIds = spatial.GetListLinkedSpatialLayers(zoneClassName);
                    foreach (string zoneLayerId in zoneLayerIds)
                    {
                        // ищем пересечение с нашим объектом (Partly Within)
                        m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable1");
                        int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                        for (int j = 1; j < selObjCount1 + 1; j++)
                        {
                            m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable1");
                            // Id(MI_PRINX) пространственного объекта ЗУ
                            string zoneReqestObjID = m_MapInfo.Eval("tmpTable1.MI_PRINX");
                            // находим ЗУ
                            //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
                            // этапа, если нет - добавляем
                            // из таблицы связи получаем Ид реестрового объекта ЗУ
                            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                             mc.SpatialLayerId == zoneLayerId &&
                             mc.SpatialObjectId == zoneReqestObjID &&
                             mc.IsogdClassName == zoneClassName);
                            if (objLink != null)
                            {
                                // по ИД находим сам реестровый объект
                                zone = connect.FindFirstObject<MonumentZone>(mc => mc.Oid.ToString() == objLink.IsogdObjectId);

                                if (zone != null)
                                {
                                    if (zone.MonumentZoneKind != null)
                                    {
                                        bool isZoneExist = false;
                                        // перебираем дочерние виды зон у градплана и если такой нет - добавляем ссылку
                                        foreach (MonumentZoneKind monumentZoneKind in gradPlan.MonumentZoneKind)
                                        {
                                            if (zone.MonumentZoneKind == monumentZoneKind)
                                                isZoneExist = true;
                                        }
                                        if (!isZoneExist)
                                        {
                                            MonumentZoneKind zoneKind = zone.MonumentZoneKind;
                                            gradPlan.MonumentZoneKind.Add(zoneKind);
                                            gradPlan.Save();
                                            connect.GetUnitOfWork().CommitChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }
                // 3.3 ищем пересечение с остальными зонами ограничений
                try
                {
                    //Сохранить выделенный объект в выборку
                    //TableInfo selectedObjectsTempTable = new TableInfo(m_MapInfo, "selectedObjectsTempTable");
                    //m_MapInfo.Do(string.Format("Select * From Selection Into {0}", selectedObjectsTempTable.Name));

                    m_MapInfo.Do("Select * From " + gradplanLayerId + " Where MI_PRINX = " + gisObjGPZUId + " Into zObject");

                    int i = 1;
                    //string zoneClassName = "AISOGD.Reglament.MonumentZone";
                    string[] zoneClassNames = { "AISOGD.Reglament.SZZone", "AISOGD.Reglament.AntennZone", "AISOGD.Reglament.ArtWellZone", "AISOGD.Reglament.EngeneeringZone",
                         "AISOGD.Reglament.WaterProtectionZone"};

                    foreach (string zoneClassName in zoneClassNames)
                    {
                        try
                        {
                            i++;
                            //connect.GetClassInfo(zoneClassName)
                            List<string> zoneLayerIds = spatial.GetListLinkedSpatialLayers(zoneClassName);
                            foreach (string zoneLayerId in zoneLayerIds)
                            {
                                // ищем пересечение с нашим объектом
                                m_MapInfo.Do("Select * from " + zoneLayerId + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable" + i.ToString());
                                int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                                for (int j = 1; j < selObjCount1 + 1; j++)
                                {
                                    m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable" + i.ToString());
                                    // Id(MI_PRINX) пространственного объекта ЗУ
                                    string zoneReqestObjID = m_MapInfo.Eval("tmpTable" + i.ToString() + ".MI_PRINX");
                                    // находим ЗУ
                                    //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
                                    // этапа, если нет - добавляем
                                    // из таблицы связи получаем Ид реестрового объекта ЗУ
                                    SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                     mc.SpatialLayerId == zoneLayerId &&
                                     mc.SpatialObjectId == zoneReqestObjID &&
                                     mc.IsogdClassName == zoneClassName);
                                    if (objLink != null)
                                    {
                                        // по ИД находим сам реестровый объект
                                        //object idz = new object() { objLink.IsogdObjectId };
                                        Guid o1 = new Guid(objLink.IsogdObjectId);
                                        SpecialZone zone = (SpecialZone)connect.GetObjectByKey(connect.GetClassInfo(zoneClassName), o1);// new object[] { objLink.IsogdObjectId });
                                        //zone = connect.FindFirstObject<MonumentZone>(mc => mc.Oid.ToString() == objLink.IsogdObjectId);

                                        if (zone != null)
                                        {
                                            if (zone.SpecialZoneKind != null)
                                            {
                                                bool isZoneExist = false;
                                                // перебираем дочерние виды зон у градплана и если такой нет - добавляем ссылку
                                                foreach (SpecialZoneKind specialZoneKind in gradPlan.SpecialZoneKind)
                                                {
                                                    if (zone.SpecialZoneKind == specialZoneKind)
                                                        isZoneExist = true;
                                                }
                                                if (!isZoneExist)
                                                {
                                                    SpecialZoneKind zoneKind = zone.SpecialZoneKind;
                                                    gradPlan.SpecialZoneKind.Add(zoneKind);
                                                    gradPlan.Save();
                                                    connect.GetUnitOfWork().CommitChanges();
                                                }
                                            }
                                        }
                                    }
                                    // если зона - береговая охранная то проставляем соответствующую галку
                                    try
                                    {
                                        if (zoneLayerId == "Береговые_полосы")
                                            gradPlan.CommonUseZoneFlag = true;
                                    }
                                    catch { }
                                }
                            }
                        }
                        catch { }
                    }
                }
                catch { }
            }
            // дропаем темптаблицы
            try
            {
                startSelectObjects.Dispose();
            }
            catch { }
            //Закрыть все слои запросов
            int NumTables = Convert.ToInt16(m_MapInfo.Eval("NumTables()"));
            List<string> queryTablesList = new List<string>();
            for (int k = 1; k <= NumTables; k++)
            {
                TableInfo TableToClose = new TableInfo(m_MapInfo, k);
                if (TableToClose.Name.IndexOf("Query") != -1)
                    queryTablesList.Add(TableToClose.Name);
            }
            foreach (string queryTable in queryTablesList)
            {
                try
                {
                    m_MapInfo.TableInfo(queryTable).Dispose();
                }
                catch { }
            }

            gradPlan.Save();
            connect.GetUnitOfWork().CommitChanges();
            return gradPlan;
        }

        /// <summary>
        /// Создание координат к реестровому объекту ГПЗУ (для вставки в текстовую часть)
        /// </summary>
        /// <param name="m_MapInfo"></param>
        /// <param name="cadNo"></param>
        /// <param name="gradPlan"></param>
        /// <param name="connect"></param>
        private void CreateCoordinates(MapInfoApplication m_MapInfo, string cadNo, GradPlan gradPlan, Connect connect)
        {
            TableInfo coordinatesTempTable = new TableInfo(m_MapInfo, "coordinatesTempTable");
            m_MapInfo.Do(string.Format("Select * From {0} Where Кадастровый_номер = \"{1}\" Into {2}",
                            "Поворотные_точки", cadNo, coordinatesTempTable.Name));

            TableInfoTemp sourceTempTable = new TableInfoTemp(m_MapInfo, ETempTableType.Query);
            //Колличество точек
            long rowCount = coordinatesTempTable.GetRowCount() ?? 0;

            for (int i = 1; i <= rowCount; i++)
            {

                
                //Выделить пересекающийся объект и поместить его в темп таблицу sourceTempTable
                m_MapInfo.Do(string.Format("Select * from {0} Where RowID = {1} Into {2}", coordinatesTempTable.Name, i, sourceTempTable.Name));
                int coordinateNumber;
                double coordinateX = 0, coordinateY = 0;
                coordinateNumber = Convert.ToInt32(m_MapInfo.Eval(sourceTempTable.Name + ".Номер_точки"));

                //try
                //{
                string test = m_MapInfo.Eval(sourceTempTable.Name + ".x_msk35");
                    coordinateX = Convert.ToDouble((m_MapInfo.Eval(sourceTempTable.Name + ".x_msk35")).Replace('.',','));
                //}
                //catch { }
                try
                {
                    coordinateY = Convert.ToDouble((m_MapInfo.Eval(sourceTempTable.Name + ".y_msk35")).Replace('.', ','));
                }
                catch { }
                if (coordinateNumber != 0 && coordinateX != 0 && coordinateY != 0)
                {
                    Coordinate c = connect.FindFirstObject<Coordinate>(x => x.GPZU.Oid.ToString() == gradPlan.Oid.ToString() && x.CoordinateNo == coordinateNumber);
                    if (c == null)
                    {
                        c = connect.CreateObject<Coordinate>();
                        c.CoordinateNo = coordinateNumber;
                        c.X = coordinateX;
                        c.Y = coordinateY;
                        c.GPZU = gradPlan;
                        c.Save();
                    }
                }
            }
            connect.GetUnitOfWork().CommitChanges();
        }
    }
}
