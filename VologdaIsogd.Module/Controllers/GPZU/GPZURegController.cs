using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using AISOGD.GPZU;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using AISOGD.Land;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Ingeo;
using AISOGD.Reglament;
using System.IO;
using System.Drawing;

namespace WinAisogdIngeo.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class GPZURegController : ViewController
    {
        public GPZURegController()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// ���������� ������ �� ��������� ������� �� ����������� ��������� �������� ����������� �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FindLotsFromGPZUAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ���� � ��� ������ (UnitOfWork)
            GradPlan i_GradPlan;
            i_GradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_GradPlan.Session;

            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            Lot i_lot;

            DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
            // ��������� ������������� ������
            if (i_GradPlan.LotCadNo == null || i_GradPlan.LotCadNo == String.Empty)
            {
                XtraMessageBox.Show("�� ������� ����������� ������� ��������� ��������.");
                return;
            }

            // ������� �� ������������ ������� ��������
            char[] sep = { ',', ';' };
            string[] cadNoms = i_GradPlan.LotCadNo.Trim().Split(sep);

            foreach (string cadNo in cadNoms)
            {
                cadNo.Trim();
                if (cadNo != "")
                {
                    // ���� �� � ����� �������� � ��������� ����������� �������
                    // ���� �� ������� - ������� � ���������� �� ��������
                    // ���� ������� - ��������, ���� �� �� � ���������, ���� ��� - ����������
                    i_lot = connect.FindFirstObject<Lot>(mc => mc.CadastralNumber == cadNo);
                    if (i_lot != null)
                    {
                        i_GradPlan.Lots.Add(i_lot);
                        i_GradPlan.Save();
                    }
                    else
                    {
                        var xtraMessageBoxForm = new XtraMessageBoxForm();
                        if (
                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                "��������� ������� � ����������� ������� " + Environment.NewLine +
                                cadNo + Environment.NewLine +
                                "�� ������ � �������� ������! �������?",
                                "��������!", dialogResults, null, 0)) == DialogResult.Yes)
                        {
                            i_lot = new Lot(unitOfWork);
                            i_lot.CadastralNumber = cadNo;
                            i_GradPlan.Lots.Add(i_lot);
                            i_lot.Save();
                        }
                        else
                        {
                            return;
                        }
                    }
                    i_GradPlan.Save();
                    unitOfWork.CommitChanges();
                }
            }

        }

        /// <summary>
        /// ���������� �������� ����
        /// 1. ������� ������ ���� �� �������� ��, ���� ����. ���� ��� - ������ ��������� � �������������
        /// ������� ������ ����
        /// 2. ������� �������� �����
        /// 3. ���������� ���� ���� 
        ///  - ��������
        ///  - ���
        ///  - �������� ����
        /// 4. ���������� ������� ����
        ///  - ���
        ///  - ���
        /// 5. ������� ����������� ���������� �������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GPZUPrepareAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ���� � ��� ������ (UnitOfWork)
            GradPlan i_GradPlan;
            i_GradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_GradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            SpatialController<GisLayer> spatial = new SpatialController<GisLayer>(connect);
            SpatialRepository spatialRepoGradPlan;

            Lot i_lot;
            //bool isLotsSpatialExist; 
            IngeoSpatial ingeoSpatial = new IngeoSpatial();
            IIngeoApplication ingeoApp = ingeoSpatial.GetActiveIngeo();
            IIngeoMapObject LotmapObject = null;
            IIngeoMapObject GPZUmapObject = null;

            string gpzuLayerName = "����������������� �����";
            string gpzuLayerId = "";
            string gpzuStyleName = "����";
            string gpzuStyleLocalId = "";
            string GPZUmapObjectId = "";
            IIngeoMapObject[] mapobjectsintersects;
            if (ingeoApp == null)
            {
                XtraMessageBox.Show("InGeo �� ��������", "����");
                return;
            }

            //
            
            //1.1 ���������, ���� �� � ���� ������ � ���
            //1.2 ������� ������ ���� �� �������� ��, ���� ����. ���� ��� - ������ ��������� � �������������
            // ���� ��� ��������� �������� ������� �� ���������� �� ���������� ���� (FindLotsFromGPZUAction)
            if (!spatial.IsObjectLinked(i_GradPlan.ClassInfo.FullName,i_GradPlan.Oid.ToString()))
            {
                //XtraMessageBox.Show("1");
                if (i_GradPlan.Lots.Count == 0)
                {
                    this.FindLotsFromGPZUAction_Execute(sender, e);
                    if (i_GradPlan.Lots.Count == 0)
                    {
                        XtraMessageBox.Show("� ���� �� ���������� ��������� �������.");
                        return;
                    }
                }
                if (i_GradPlan.Lots.Count > 0)
                {
                    foreach (Lot lt in i_GradPlan.Lots)
                    {
                        if (spatial.IsObjectLinked(lt.ClassInfo.FullName, lt.Oid.ToString()))
                        {
                            SpatialRepository spatialRepoLot = connect.FindFirstObject<SpatialRepository>(mc =>
               mc.IsogdClassName == lt.ClassInfo.FullName &&
               mc.IsogdObjectId == lt.Oid.ToString());

                            // ������� ��������������� ������ ���������� �������
                            if (spatialRepoLot.SpatialObjectId != null &&
                                spatialRepoLot.SpatialObjectId != "")
                            {
                                LotmapObject = IngeoSpatial.GetOneMapObjectBySpatialObjectID(ingeoApp,
                        spatialRepoLot.SpatialObjectId);

                                // �������� � ������ ����
                                /// ! ���������� ��� ���������� ��������
                                /// 
                                //gpzuLayerId = spatial.GetListLinkedSpatialLayers(i_GradPlan.ClassInfo.FullName)[0];
                                gpzuStyleLocalId =IngeoSpatial.GlobalIDToLocalID(ingeoApp,
                                    spatial.GetStyleIdByName(gpzuLayerName, gpzuStyleName));
                                GPZUmapObject = IngeoSpatial.CloneMapObject(ingeoApp, LotmapObject, gpzuStyleLocalId);
                                GPZUmapObject.MapObjects.UpdateChanges();
                                object space, number;
                                ingeoApp.ActiveDb.LocalIDToGlobalID(GPZUmapObject.LayerID, out space,
                                    out number);
                                var gpzuLayerGlobalId = space.ToString() + number;
                                // ��������� � ���������� �������� ����
                                spatial.AddSpatialLink(i_GradPlan.ClassInfo.FullName, i_GradPlan.Oid.ToString(),
                                    gpzuLayerGlobalId, GPZUmapObject.ID);
                                
                                // ����� ����������� ���������� ������� �� ������������, ��� ��� � ��� ������� ������� � ����������� �� �������� � ���� ��������� (������������� ��� ����)
                                //foreach (IIngeoShape shape in GPZUmapObject.Shapes)
                                //{
                                //    if (shape.DefineGeometry)
                                //    {
                                //        IIngeoContour con = shape.Contour.BuildBufferZone(-3);
                                //        IIngeoMapObject bufobj = GPZUmapObject.MapObjects.AddObject(IngeoSpatial.GlobalIDToLocalID(ingeoApp, "{84CF3E93-C47E-4F83-9CF9-36A2421EEF42}1023"));
                                //        IIngeoShape bufshape = bufobj.Shapes.Insert(-1, IngeoSpatial.GlobalIDToLocalID(ingeoApp, "{84CF3E93-C47E-4F83-9CF9-36A2421EEF42}1054"));
                                //        bufshape.Contour.AddPartsFrom(con);
                                //        bufobj.MapObjects.UpdateChanges();
                                //        CutOffPartOfBuffer(ingeoApp, bufobj, spatial);
                                //        DrawSizeLine(ingeoApp, GPZUmapObject, bufobj);
                                //    }
                                //}
                                
                                
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show(String.Format("��������� ������� (ID:{0}) �� ����� ����� � ���"
                + " ��������. ��� ����������� ������� ������ ���� � ���������� ��������.", lt.Oid.ToString()),
                "��������!");
                            return;
                        }
                    }
                }

            }
            unitOfWork.CommitChanges();
            // 1.4 ������� ������ ����������� � ��� �������� ��������� � SpatialRepository � �� ��� ������� ����
            spatialRepoGradPlan = connect.FindFirstObject<SpatialRepository>(mc =>
               mc.IsogdClassName == i_GradPlan.ClassInfo.FullName &&
               mc.IsogdObjectId == i_GradPlan.Oid.ToString());
            if (spatialRepoGradPlan == null)
            {
                XtraMessageBox.Show("����������� ��� ������ ����. ��� �����������, ���������� ��� ���������� "
                + " � ������� � ��������� ���������.", "������!");
                return;
            }
            GPZUmapObjectId = spatialRepoGradPlan.SpatialObjectId;
            // 1.5 �������� ��� ������ ������� ����
            GPZUmapObject = IngeoSpatial.GetOneMapObjectBySpatialObjectID(ingeoApp, GPZUmapObjectId);
            //XtraMessageBox.Show("2");
            //2. ������� �������� �����
            try
            {
                IngeoSpatial.CreatePointsForContour(ingeoApp, GPZUmapObject, connect, spatial);
            }
            catch { }
            //XtraMessageBox.Show("3");
            //3. ���������� ���� ���� 
            // �������� Id ����� ���� �� ������
            // 3.1 ��������������� ����
            TerrZone terrZone;
            string zoneClassName = "AISOGD.Reglament.TerrZone";
            List<string> zoneLayerIds = spatial.GetListLinkedSpatialLayers(zoneClassName);
            foreach (string zoneLayerId in zoneLayerIds)
            {
                // �������� ������� ����������� ���� ���� � �������� ����
                mapobjectsintersects = IngeoSpatial.CrossingMapObjectsByLayer(
                    IngeoSpatial.GlobalIDToLocalID(ingeoApp, zoneLayerId), GPZUmapObject);
                if (mapobjectsintersects != null)
                {
                    foreach (IIngeoMapObject mapObjectZone in mapobjectsintersects)
                    {
                        //�������� ��������� ���������� ������� ���, �������, ���� �� ��� ����� �
                        // ����, ���� ��� - ���������
                        // �� ������� ����� �������� �� ����������� ������� ����
                        SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                            mc.SpatialLayerId == zoneLayerId &&
                            mc.SpatialObjectId == mapObjectZone.ID &&
                            mc.IsogdClassName == zoneClassName);
                        if (objLink != null)
                        {
                            // �� �� ������� ��� ���������� ������ ����
                            terrZone = unitOfWork.FindObject<TerrZone>(new BinaryOperator("Oid", objLink.IsogdObjectId));
                            if (terrZone != null)
                            {
                                if (terrZone.TerrZoneKind != null)
                                {
                                    bool isZoneExist = false;
                                    // ���������� �������� ���� ��� � ��������� � ���� ����� ��� - ��������� ������
                                    foreach (TerrZoneKind terrZoneKind in i_GradPlan.TerrZoneKind)
                                    {
                                        if (terrZone.TerrZoneKind == terrZoneKind)
                                            isZoneExist = true;
                                    }
                                    if (!isZoneExist)
                                    {
                                        TerrZoneKind terrZoneKind = terrZone.TerrZoneKind;
                                        i_GradPlan.TerrZoneKind.Add(terrZoneKind);
                                        unitOfWork.CommitChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //XtraMessageBox.Show("4");
            try
            {
                // 3.2 ���
                SpecialZone specialZone;
                zoneClassName = "AISOGD.Reglament.SpecialZone";
                zoneLayerIds = spatial.GetListLinkedSpatialLayers(zoneClassName);
                foreach (string zoneLayerId in zoneLayerIds)
                {
                    // �������� ������� ����������� ���� ���� � �������� ����
                    mapobjectsintersects = IngeoSpatial.CrossingMapObjectsByLayer(
                        IngeoSpatial.GlobalIDToLocalID(ingeoApp, zoneLayerId), GPZUmapObject);
                    if (mapobjectsintersects != null)
                    {
                        foreach (IIngeoMapObject mapObjectZone in mapobjectsintersects)
                        {
                            //�������� ��������� ���������� ������� ���, �������, ���� �� ��� ����� �
                            // ����, ���� ��� - ���������
                            // �� ������� ����� �������� �� ����������� ������� ����
                            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                mc.SpatialLayerId == zoneLayerId &&
                                mc.SpatialObjectId == mapObjectZone.ID &&
                                mc.IsogdClassName == zoneClassName);
                            if (objLink != null)
                            {
                                // �� �� ������� ��� ���������� ������ ����
                                specialZone = unitOfWork.FindObject<SpecialZone>(new BinaryOperator("Oid", objLink.IsogdObjectId));
                                if (specialZone != null)
                                {
                                    if (specialZone.SpecialZoneKind != null)
                                    {
                                        bool isZoneExist = false;
                                        // ���������� �������� ���� ��� � ��������� � ���� ����� ��� - ��������� ������
                                        foreach (SpecialZoneKind zoneKind in i_GradPlan.SpecialZoneKind)
                                        {
                                            if (specialZone.SpecialZoneKind == zoneKind)
                                                isZoneExist = true;
                                        }
                                        if (!isZoneExist)
                                        {
                                            SpecialZoneKind zoneKind = specialZone.SpecialZoneKind;
                                            i_GradPlan.SpecialZoneKind.Add(zoneKind);
                                            unitOfWork.CommitChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            //XtraMessageBox.Show("5");
            try
            {
                // 3.3 �������� ����
                MonumentZone monumentZone;
                zoneClassName = "AISOGD.Reglament.MonumentZone";
                zoneLayerIds = spatial.GetListLinkedSpatialLayers(zoneClassName);
                foreach (string zoneLayerId in zoneLayerIds)
                {
                    // �������� ������� ����������� ���� ���� � �������� ����
                    mapobjectsintersects = IngeoSpatial.CrossingMapObjectsByLayer(
                        IngeoSpatial.GlobalIDToLocalID(ingeoApp, zoneLayerId), GPZUmapObject);
                    if (mapobjectsintersects != null)
                    {
                        foreach (IIngeoMapObject mapObjectZone in mapobjectsintersects)
                        {
                            //�������� ��������� ���������� ������� ���, �������, ���� �� ��� ����� �
                            // ����, ���� ��� - ���������
                            // �� ������� ����� �������� �� ����������� ������� ����
                            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                mc.SpatialLayerId == zoneLayerId &&
                                mc.SpatialObjectId == mapObjectZone.ID &&
                                mc.IsogdClassName == zoneClassName);
                            if (objLink != null)
                            {
                                // �� �� ������� ��� ���������� ������ ����
                                monumentZone = unitOfWork.FindObject<MonumentZone>(new BinaryOperator("Oid",
                                    objLink.IsogdObjectId));
                                if (monumentZone != null)
                                {
                                    if (monumentZone.MonumentZoneKind != null)
                                    {
                                        bool isZoneExist = false;
                                        // ���������� �������� ���� ��� � ��������� � ���� ����� ��� - ��������� ������
                                        foreach (MonumentZoneKind zoneKind in i_GradPlan.MonumentZoneKind)
                                        {
                                            if (monumentZone.MonumentZoneKind == zoneKind)
                                                isZoneExist = true;
                                        }
                                        if (!isZoneExist)
                                        {
                                            MonumentZoneKind zoneKind = monumentZone.MonumentZoneKind;
                                            i_GradPlan.MonumentZoneKind.Add(zoneKind);
                                            unitOfWork.CommitChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            //XtraMessageBox.Show("6");
            try
            {
                // 3.3 ������
                AISOGD.Constr.CapitalStructureBase specialBuild;
                string buildClassName = "AISOGD.Constr.CapitalStructureBase";
                List<string> buidLayerIds = spatial.GetListLinkedSpatialLayers(buildClassName);
                PutNumber(ingeoSpatial.GetActiveIngeo(), ingeoSpatial.GetActiveIngeo().ActiveDb.MapObjects,
    (GPZUmapObject.X1 + GPZUmapObject.X2) / 2, (GPZUmapObject.Y1 + GPZUmapObject.Y2) / 2, "1");
                foreach (string buildLayerId in buidLayerIds)
                {
                    // �������� ������� ����������� ����  � �������� ����
                    mapobjectsintersects = IngeoSpatial.CrossingMapObjectsByLayer(
                        IngeoSpatial.GlobalIDToLocalID(ingeoApp, buildLayerId), GPZUmapObject);
                    if (mapobjectsintersects != null)
                    {
                        int i = 1;
                        foreach (IIngeoMapObject mapObjectBuild in mapobjectsintersects)
                        {
                            //�������� ��������� ���������� ������� , �������, ���� �� ��� ����� �
                            // ����, ���� ��� - ���������
                            // �� ������� ����� �������� �� ����������� ������� 
                            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
                                mc.SpatialLayerId == buildLayerId &&
                                mc.SpatialObjectId == mapObjectBuild.ID &&
                                mc.IsogdClassName == buildClassName);
                            if (objLink != null)
                            {
                                // �� �� ������� ��� ���������� ������ 
                                specialBuild = unitOfWork.FindObject<AISOGD.Constr.CapitalStructureBase>
                                    (new BinaryOperator("Oid", objLink.IsogdObjectId));
                                if (specialBuild != null)
                                {

                                    bool isBuildExist = false;
                                    // ���������� �������� ���� ��� � ��������� � ���� ����� ��� - ��������� ������
                                    foreach (AISOGD.Constr.CapitalStructureBase buildKind in i_GradPlan.CapitalStructureBase)
                                    {
                                        if (specialBuild == buildKind)
                                            isBuildExist = true;
                                    }
                                    if (!isBuildExist)
                                    {
                                        AISOGD.Constr.CapitalStructureBase buildKind = specialBuild;
                                        i_GradPlan.CapitalStructureBase.Add(buildKind);
                                        unitOfWork.CommitChanges();
                                    }
                                    i++;
                                    PutNumber(ingeoSpatial.GetActiveIngeo(), ingeoSpatial.GetActiveIngeo().ActiveDb.MapObjects,
                                        (mapObjectBuild.X1 + mapObjectBuild.X2) / 2, (mapObjectBuild.Y1 + mapObjectBuild.Y2) / 2, i.ToString());

                                }
                            }
                        }
                    }
                }
               // PutBuildingsNumbers(ingeoApp, ingeoApp.ActiveDb.MapObjects, GPZUmapObject);
            }
            catch { }
            unitOfWork.CommitChanges();
        }

        public string PutNumber(IIngeoApplication ingeoApp, IIngeoMapObjects mapObjects, double X, double Y, string Number)
        {
            // ��������� ����������������� ������ ���� --> ����������� �������� --> ����� ������
            string LayerId = ingeoApp.ActiveDb.GlobalIDToLocalID("{2B042FDA-43E5-4E5A-AEEB-833F2C1E3ACF}", 1202);
            string StyleId = ingeoApp.ActiveDb.GlobalIDToLocalID("{2B042FDA-43E5-4E5A-AEEB-833F2C1E3ACF}", 1205);

            IIngeoMapObject NewNumber = mapObjects.AddObject(LayerId);
            IIngeoContourPart contour_part = NewNumber.Shapes.Insert(0, StyleId).Contour.Insert(0);
            contour_part.InsertVertex(-1, X, Y, 0);
            NewNumber.SemData.SetValue("������", "Number", Number, 0);
            mapObjects.UpdateChanges();
            return NewNumber.ID;
        }

        public void CutOffPartOfBuffer(IIngeoApplication ingeoApp, IIngeoMapObject bufferObject, SpatialController<GisLayer> spatial)
        {
            //���������� ������ 

            List<string> zoneLayerIDs = spatial.GetLayersIdsFromLayerGroupName("���������� ��� �� ����� ����������� ���������� ����");
            foreach (string zoneLayerID in zoneLayerIDs)
            {
                string zoneLayerLocalID = IngeoSpatial.GlobalIDToLocalID(ingeoApp, zoneLayerID);
                IIngeoMapObject[] sanZoneMapObjs = IngeoSpatial.CrossingMapObjectsByLayer(zoneLayerLocalID, bufferObject);
                foreach (IIngeoMapObject sanZoneMapObj in sanZoneMapObjs)
                {
                    foreach (IIngeoShape shape in sanZoneMapObj.Shapes)
                    {
                        if (bufferObject.Shapes.Count > 0)
                        {
                            bufferObject.Shapes[0].Contour.Combine(TIngeoContourCombineOperation.inccSub, shape.Contour);
                            bufferObject.MapObjects.UpdateChanges();
                        }
                    }
                }
            }
            
        }


        private void CoordLinesEx(IIngeoApplication ingeoApp, IIngeoMapObjects mapObjects, IIngeoMapObject mapObjectOutData)
        {
            double x;
            double y;
            double c;
            double xO;
            double yO;
            double xN;
            double yN;


            double Direct;
            double xV, xNum;
            double yV, yNum;


            double xs = 0;
            double ys = 0;

            //��������� ����������������� ������ ����
            string MapID = ingeoApp.ActiveDb.GlobalIDToLocalID("{84CF3E93-C47E-4F83-9CF9-36A2421EEF42}", 1007);
            //����� �������
            string LayerID = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1418);
            //�����
            string StyleID = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1420);
            string StyleIDPoint = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1425);

            double[,] ArrayVertex;
            bool Direction = true;
            int pointCount = 0;

            string left = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1422);
            string right = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1423);
            string Point = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1426);
           // string Number = ingeoApp.ActiveDb.GlobalIDToLocalID("{BC1D983B-81CE-4871-AE35-35B291FD969B}", 24036762);
            string Line = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}1427", 24036762);

            IIngeoMapObject pointObject;
            IIngeoContourPart pointContourPart;

            foreach (IIngeoShape shp in mapObjectOutData.Shapes)
            {
                if (shp.DefineGeometry)
                {
                    //������� �������
                    //  shp.Contour.GetBounds(out DB, out LB, out UB, out RB);
                    foreach (IIngeoContourPart cnt in shp.Contour)
                    {
                        ArrayVertex = GeometryFunc.FillVertexArray(cnt);
                        //�������� ����������� ������ �������)
                        if (GeometryFunc.ContourDirection(cnt) == 1)
                        {
                            Direction = false;
                        }
                        else Direction = true;
                        for (int i = 0; i < (ArrayVertex.Length / 2); i++)
                        {
                            pointCount++;
                            //��. �������� �������
                            SetDirectPoints(ArrayVertex, i, out x, out y, out xO, out yO, out xN, out yN);
                            //�������� ���� ����������� �������� ���� (����������� �������)
                            Direct = DirectionAngle(xO, yO, x, y, xN, yN, Direction);
                            if (cnt.IsHole)
                            {
                                if (Direct < Math.PI)
                                    Direct = Direct + Math.PI;
                                else
                                    Direct = Direct - Math.PI;
                            }
                            //������� ���������� ����� ����� ������� ������� �������
                            ShiftPoint(Direct, x, y, 10, out xV, out yV);
                            //������� ���������� ����� ��� ������
                            ShiftPoint(Direct, x, y, 2, out xNum, out yNum);
                            // ������� ���������������� ������
                            pointObject = mapObjects.AddObject(LayerID);
                            pointContourPart =
                                pointObject.Shapes.Insert(0, Point).Contour.Insert(0);
                            pointContourPart.InsertVertex(-1, x, y, 0);
                            mapObjects.UpdateChanges();

                            IIngeoContourPart LineContourPart =
                                pointObject.Shapes.Insert(0, Line).Contour.Insert(0);
                            LineContourPart.InsertVertex(-1, x, y, 0);
                            LineContourPart.InsertVertex(-1, xV, yV, 0); //�������� ����� �������
                            string CoordStyleID;
                            if (Direct < Math.PI)
                                CoordStyleID = right;
                            else CoordStyleID = left;
                            IIngeoContourPart OutContourPart =
                                pointObject.Shapes.Insert(0, CoordStyleID).Contour.Insert(-1);
                            OutContourPart.InsertVertex(-1, xV, yV, 0); //�������� ����� ������� �������

                            //IIngeoContourPart NumberContourPart =
                            //    pointObject.Shapes.Insert(0, Number).Contour.Insert(0);
                            //NumberContourPart.InsertVertex(-1, xNum, yNum, 0); //�������� ������ �����

                            pointObject.MapObjects.UpdateChanges();
                            xs = Math.Round(x, 2);
                            ys = Math.Round(y, 2);
                            // ���������� ������ � ���������
                            pointObject.SemData.SetValue("���������� �����", "Npoint", i + 1, 0);
                            // pointObject.SemData.SetValue("Point", "X", xs, 0);
                            // pointObject.SemData.SetValue("Point", "Y", ys, 0);
                            mapObjects.UpdateChanges();
                            //  ActivityWebApp.LogUtils.WriteTXT("// ��������� �������������� �����");
                            // ��������� �������������� �����
                            pointObject.TopoLinks.Clear();
                            pointObject.TopoLinks.Add(mapObjectOutData.ID);
                            mapObjects.UpdateChanges();
                        }
                    }
                }
            }
        }

        private static void SetDirectPoints(double[,] ArrayVertex, int i, out double x, out double y, out double xO, out double yO, out double xN, out double yN)
        {
            x = 0;
            y = 0;
            xO = 0;
            yO = 0;
            xN = 0;
            yN = 0;
            if (i != 0)
            {
                if (i != (ArrayVertex.Length / 2 - 1))
                {
                    xO = ArrayVertex[0, i - 1];
                    yO = ArrayVertex[1, i - 1];
                    xN = ArrayVertex[0, i + 1];
                    yN = ArrayVertex[1, i + 1];
                    x = ArrayVertex[0, i];
                    y = ArrayVertex[1, i];
                }
                else
                {
                    xO = ArrayVertex[0, i - 1];
                    yO = ArrayVertex[1, i - 1];
                    xN = ArrayVertex[0, 0];
                    yN = ArrayVertex[1, 0];
                    x = ArrayVertex[0, i];
                    y = ArrayVertex[1, i];
                }
            }
            else
            {
                xO = ArrayVertex[0, ArrayVertex.Length / 2 - 1];
                yO = ArrayVertex[1, ArrayVertex.Length / 2 - 1];
                xN = ArrayVertex[0, 1];
                yN = ArrayVertex[1, 1];
                x = ArrayVertex[0, i];
                y = ArrayVertex[1, i];
            }
        }
        private static double DirectionAngle(double Xo, double Yo, double X, double Y, double Xn, double Yn, bool Direct)
        {
            double Rumb1, Rumb2, dX1, dY1, dX2, dY2, Angle;
            double Result = Math.PI;
            dX1 = X - Xo;
            dX2 = Xn - X;
            dY1 = Y - Yo;
            dY2 = Yn - Y;
            //���� �������� ������
            if (dX1 != 0)
            {
                Rumb1 = Math.Atan(Math.Abs(dY1 / dX1));
            }
            else Rumb1 = Math.PI / 2;

            if ((dX1 < 0) & (dY1 >= 0)) //��
                Rumb1 = Math.PI - Rumb1;
            if ((dX1 < 0) & (dY1 < 0)) //��
                Rumb1 = Math.PI + Rumb1;
            if ((dX1 >= 0) & (dY1 < 0)) //��
                Rumb1 = Math.PI * 2 - Rumb1;
            //���� ��������� �����
            if (dX2 != 0)
            {
                Rumb2 = Math.Atan(Math.Abs(dY2 / dX2));
            }
            else Rumb2 = Math.PI / 2;

            if ((dX2 < 0) & (dY2 >= 0)) //��
                Rumb2 = Math.PI - Rumb2;
            if ((dX2 < 0) & (dY2 < 0)) //��
                Rumb2 = Math.PI + Rumb2;
            if ((dX2 >= 0) & (dY2 < 0)) //��
                Rumb2 = Math.PI * 2 - Rumb2;

            if (Rumb1 < Math.PI)
            {
                if ((Rumb2 - Rumb1) > Math.PI)
                    Angle = (Rumb2 + Rumb1 + Math.PI) / 2;
                else
                    Angle = (Rumb2 + Rumb1 + (3 * Math.PI)) / 2;
            }
            else
            {
                if ((Rumb1 - Rumb2) < Math.PI)
                    Angle = (Rumb2 + Rumb1 - Math.PI) / 2;
                else Angle = (Rumb2 + Rumb1 + Math.PI) / 2;
            }
            if (Angle > (Math.PI * 2))
                Angle = Angle % (Math.PI * 2);
            if (!Direct)
            {
                if (Angle < Math.PI) //����� ������ �������
                    Angle = Angle + Math.PI;
                else Angle = Angle - Math.PI;
            }

            Result = Angle;

            return Result;

        }
        private static void ShiftPoint(double Direct, double x, double y, double B, out double xV, out double yV)
        {
            double V = Math.Abs(Math.Tan(Direct));
            double dx = B / Math.Sqrt(V * V + 1);
            double dy = dx * V;
            xV = x + B;
            yV = y + B;
            //������ ����
            if (Direct == Math.PI / 2)
            {
                yV = y + B;
                xV = x;
            }
            if (Direct == Math.PI)
            {
                yV = y;
                xV = x - B;
            }
            if (Direct == Math.PI * 3 / 2)
            {
                yV = y - B;
                xV = x;
            }
            if (Direct == Math.PI * 2 | Direct == 0)
            {
                yV = y;
                xV = x + B;
            }
            //������ ����� �����
            if (Direct < Math.PI / 2)
            {
                xV = x + dx;
                yV = y + dy;
            }
            if ((Math.PI / 2) < Direct & Direct < Math.PI)
            {
                xV = x - dx;
                yV = y + dy;
            }
            if (Math.PI < Direct & Direct < (Math.PI * 3 / 2))
            {
                xV = x - dx;
                yV = y - dy;
            }
            if ((Math.PI * 3 / 2) < Direct)
            {
                xV = x + dx;
                yV = y - dy;
            }
        }

        private static double DirectionAngleSizeLine(double Xo, double Yo, double X, double Y, double Xn, double Yn, bool Direct)
        {
            double Rumb1, Rumb2, dX1, dY1, dX2, dY2, Angle;
            double Result = Math.PI;
            dX1 = X - Xo;
            dY1 = Y - Yo;


            //���� �������� ������
            if (dX1 != 0)
            {
                Rumb1 = Math.Atan(Math.Abs(dY1 / dX1));
            }
            else Rumb1 = Math.PI / 2;

            if ((dX1 < 0) & (dY1 >= 0)) //�� ��
                Rumb1 = Math.PI / 2 + (Math.PI - Rumb1);

            else if ((dX1 < 0) & (dY1 < 0)) //��
                Rumb1 = Math.PI / 2 + (Math.PI + Rumb1);

            else if ((dX1 >= 0) & (dY1 < 0)) //��
                Rumb1 = Math.PI / 2 - (Math.PI * 2 - Rumb1);


            else if ((dX1 > 0) & (dY1 >= 0))
                Rumb1 = 3 * Math.PI / 2 - (Math.PI - Rumb1);

            else
                Rumb1 = (Math.PI * 2 - Rumb1) - (135 * Math.PI / 180);
            Result = Rumb1;
            return Result;

        }

        private static void ShiftPointSizeLine(double Direct, double x, double y, double B, out double xV, out double yV)
        {
            double V = Math.Abs(Math.Tan(Direct));
            double dx = B / Math.Sqrt(V * V + 1);
            double dy = dx * V;
            xV = x + B;
            yV = y + B;
            //������ ����
            if (Direct == Math.PI / 2)
            {
                yV = y - B;
                xV = x;
            }
            if (Direct == Math.PI)
            {
                yV = y;
                xV = x + B;
            }
            if (Direct == Math.PI * 3 / 2)
            {
                yV = y + B;
                xV = x;
            }
            if (Direct == Math.PI * 2 | Direct == 0)
            {
                yV = y;
                xV = x - B;
            }
            //������ ����� �����
            if (Direct < Math.PI / 2)
            {
                xV = x - dx;
                yV = y - dy;
            }
            if ((Math.PI / 2) < Direct & Direct < Math.PI)
            {
                xV = x + dx;
                yV = y - dy;
            }
            if (Math.PI < Direct & Direct < (Math.PI * 3 / 2))
            {
                xV = x + dx;
                yV = y + dy;
            }
            if ((Math.PI * 3 / 2) < Direct)
            {
                xV = x - dx;
                yV = y + dy;
            }
        }


        private void GPZUReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ���� � ��� ������ (UnitOfWork)
            GradPlan i_GradPlan;
            i_GradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_GradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            SpatialController<GisLayer> spatial = new SpatialController<GisLayer>(connect);
            SpatialRepository spatialRepoGradPlan;

            //bool isLotsSpatialExist; 
            IngeoSpatial ingeoSpatial = new IngeoSpatial();
            IIngeoApplication ingeoApp = ingeoSpatial.GetActiveIngeo();
            IIngeoMapObject LotmapObject = null;
            IIngeoMapObject GPZUmapObject = null;
            string GPZUmapObjectId = "";
            // ������������ �����������
            // ������� ��������������� ������ ��� ������������ �������� � ��������� ��� � ���� � ������� ���������
            // ������� ��������� ���������� �� �� ���������, ���������� � ��� ���� ��������, ����� ��������� �� � 
            // GradPlanImage � ������ ���� ����������
            string directoryTempName = Path.GetTempPath() + @"\WinIsogd\GradPlan\" + i_GradPlan.Oid.ToString();
            if (!Directory.Exists(directoryTempName))
                Directory.CreateDirectory(directoryTempName);

            // �������� ��������� ���������� ������� � ��� bbox
            spatialRepoGradPlan = connect.FindFirstObject<SpatialRepository>(mc =>
               mc.IsogdClassName == i_GradPlan.ClassInfo.FullName &&
               mc.IsogdObjectId == i_GradPlan.Oid.ToString());
            if (spatialRepoGradPlan == null)
            {
                XtraMessageBox.Show("����������� ��� ������ ����. ��� �����������, ���������� ��� ���������� "
                + " � ������� � ��������� ���������.", "������!");
                return;
            }
            GPZUmapObjectId = spatialRepoGradPlan.SpatialObjectId;
            // 1.5 �������� ��� ������ ������� ����
            GPZUmapObject = IngeoSpatial.GetOneMapObjectBySpatialObjectID(ingeoApp, GPZUmapObjectId);

        }

        private void DrawSizeLine(IIngeoApplication ingeoApp, IIngeoMapObject SourceObject, IIngeoMapObject BufferObject)
        {

            List<Segment> BufferSegments = null;
            List<PointD> BufPoints = null;
            foreach (IIngeoShape shp in BufferObject.Shapes)
            {
                if (shp.DefineGeometry)
                {
                    foreach (IIngeoContourPart cnt in shp.Contour)
                    {
                        BufferSegments = FillSegmentList(cnt, out BufPoints);
                    }
                }
            }
            List<Segment> ZUSegments = null;
            List<PointD> ZUPoints = null;
            foreach (IIngeoShape shp in SourceObject.Shapes)
            {
                if (shp.DefineGeometry)
                {
                    foreach (IIngeoContourPart cnt in shp.Contour)
                    {
                        ZUSegments = FillSegmentList(cnt, out ZUPoints);
                    }
                }
            }

            foreach (Segment bufSeg in BufferSegments)
            {
                if (bufSeg.P2.C != 0)
                    continue;
                PointD pointOnBufCenter = new PointD(bufSeg.P1.X - (bufSeg.P1.X - bufSeg.P2.X) / 2,
    bufSeg.P1.Y - (bufSeg.P1.Y - bufSeg.P2.Y) / 2, 0);
                List<DistanceToSegment> segmentListToCalc = new List<DistanceToSegment>();
                List<DistanceBetweenPoints> distanceBetweenPointsList = new List<DistanceBetweenPoints>();
                foreach (PointD zuPoint in ZUPoints)
                {
                    distanceBetweenPointsList.Add(new DistanceBetweenPoints(zuPoint, pointOnBufCenter));
                }
                distanceBetweenPointsList = distanceBetweenPointsList.OrderBy(x => x.distance).ToList();
                foreach (Segment zuSeg in ZUSegments)
                {
                    if (zuSeg.P1 == distanceBetweenPointsList[0].p1 || zuSeg.P2 == distanceBetweenPointsList[0].p1 ||
                        zuSeg.P1 == distanceBetweenPointsList[1].p1 || zuSeg.P2 == distanceBetweenPointsList[1].p1 ||
                        zuSeg.P1 == distanceBetweenPointsList[2].p1 || zuSeg.P2 == distanceBetweenPointsList[2].p1 )//||
                       // zuSeg.P1 == distanceBetweenPointsList[3].p1 || zuSeg.P2 == distanceBetweenPointsList[3].p1)
                    {
                        PointD PointOnZU;

                        double distanceToSegment = DistanceToSegment(zuSeg, pointOnBufCenter, out PointOnZU);
                        if (isPointOnSegment(zuSeg, PointOnZU))
                        {
                            PointD pointToRelations = new PointD((PointOnZU.X + (distanceToSegment - 1) * pointOnBufCenter.X) / distanceToSegment,
                                (PointOnZU.Y + (distanceToSegment - 1) * pointOnBufCenter.Y) / distanceToSegment, 0);
                            TIngeoContourRelation rel = BufferPointRalation(BufferObject, pointToRelations);
                            if (rel == 0)
                            {
                                segmentListToCalc.Add(new DistanceToSegment(bufSeg, zuSeg, pointOnBufCenter, PointOnZU, distanceToSegment));

                            }

                        }
                    }
                }
                if (segmentListToCalc.Count > 0)
                {
                    DistanceToSegment temp = segmentListToCalc.OrderBy(x => x.distance).First();
                    Create(ingeoApp, BufferObject, temp.pointOnBuffer, temp.pointOnZu);
                }


            }
        }

        public double DistanceFromPoint(PointD p1, PointD p2)
        {
            double result = 0;
            result = Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
            return result;
        }


        public void Create(IIngeoApplication ingeoApp, IIngeoMapObject BufferObject, PointD pointOnBuf, PointD pointOnZU)
        {
            string LayerID = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1429);
            string SizeLineStyleID = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1434);
            string TextStyleID = ingeoApp.ActiveDb.GlobalIDToLocalID("{5B09ECE1-94C4-4528-91FB-3CF2DCEC53E0}", 1433);

            IIngeoMapObject NewLine = BufferObject.MapObjects.AddObject(LayerID);
            IIngeoContourPart contour_part2 = NewLine.Shapes.Insert(0, SizeLineStyleID).Contour.Insert(0);

            if (pointOnBuf.Y < pointOnZU.Y)
            {
                contour_part2.InsertVertex(-1, pointOnBuf.X, pointOnBuf.Y, 0);
                contour_part2.InsertVertex(1, pointOnZU.X, pointOnZU.Y, 0);
            }
            else
            {
                contour_part2.InsertVertex(-1, pointOnZU.X, pointOnZU.Y, 0);
                contour_part2.InsertVertex(1, pointOnBuf.X, pointOnBuf.Y, 0);
            }

            BufferObject.MapObjects.UpdateChanges();
        }


        public static TIngeoContourRelation BufferPointRalation(IIngeoMapObject aSourceMapObject, PointD aPoint)
        {
            IIngeoMapObject tempObj = aSourceMapObject.MapObjects.AddObject(aSourceMapObject.LayerID);
            IIngeoShape tempSh = tempObj.Shapes.Insert(
    -1, aSourceMapObject.Shapes[0].StyleID);
            IIngeoContourPart contour = tempSh.Contour.Insert(0);
            contour.InsertVertex(-1, aPoint.X,
                aPoint.Y, 0);
            aSourceMapObject.MapObjects.UpdateChanges();
            TIngeoContourRelation rel = aSourceMapObject.Shapes[0].Contour.TestRelation(tempObj.Shapes[0].Contour);
            aSourceMapObject.MapObjects.DeleteObject(tempObj.ID);
            aSourceMapObject.MapObjects.UpdateChanges();
            return rel;
        }


        //���������� ����������� ����������
        private double DistanceToSegment(Segment seg, PointD pt, out PointD resPoint)
        {
            double xR, yR;
            double a = seg.P2.Y - seg.P1.Y;
            double b = seg.P2.X - seg.P1.X;
            b = -b;
            double c = seg.P2.X * seg.P1.Y - seg.P2.Y * seg.P1.X;
            xR = (b * (b * pt.X - a * pt.Y) - a * c) / (a * a + b * b);
            yR = (a * (-b * pt.X + a * pt.Y) - b * c) / (a * a + b * b);
            resPoint = new PointD(xR, yR, 0);
            double distance = Math.Sqrt((pt.X - resPoint.X) * (pt.X - resPoint.X) + (pt.Y - resPoint.Y) * (pt.Y - resPoint.Y));
            return distance;
        }


        //�������� �������������� ����� �������
        private bool isPointOnSegment(Segment seg, PointD pt)
        {
            double x1, x2, y1, y2;
            x1 = seg.P1.X;
            x2 = seg.P2.X;
            y1 = seg.P1.Y;
            y2 = seg.P2.Y;
            double segmentLenth = Math.Sqrt((seg.P1.X - seg.P2.X) * (seg.P1.X - seg.P2.X) + (seg.P1.Y - seg.P2.Y) * (seg.P1.Y - seg.P2.Y));
            double ptP1Lenth = Math.Sqrt((seg.P1.X - pt.X) * (seg.P1.X - pt.X) + (seg.P1.Y - pt.Y) * (seg.P1.Y - pt.Y));
            double ptP2Lenth = Math.Sqrt((seg.P2.X - pt.X) * (seg.P2.X - pt.X) + (seg.P2.Y - pt.Y) * (seg.P2.Y - pt.Y));

            if (Math.Round(segmentLenth, 4) == Math.Round(ptP1Lenth + ptP2Lenth, 4))
                return true;
            else return false;
        }

        ///// <summary>
        ///// �-���� ���������� ����������� ������������ ������ ������� ������
        ///// </summary>
        ///// <param name="aMapObject"></param>
        ///// <returns></returns>
        //public List<Segment> FillSegmentList(IIngeoContourPart cnt)
        //{
        //    double x;
        //    double y;
        //    double c;
        //    double[,] ArrayVertexFirst;
        //    double[,] ArrayVertex = null;

        //    if (cnt.Closed) //��������� ����� ��� ���������� ������� ��
        //    {
        //        //��������� ������(����� ���� � ������������ �������)
        //        ArrayVertexFirst = new double[3, cnt.VertexCount];
        //        for (int i = 0; i < cnt.VertexCount; i++)
        //        {
        //            // �������� ���������� � ����� �����
        //            cnt.GetVertex(i, out x, out y, out c);
        //            ArrayVertexFirst[0, i] = x;
        //            ArrayVertexFirst[1, i] = y;
        //            ArrayVertexFirst[2, i] = c;
        //        }
        //        //���� ��������� � ������ ����� ���������, �������������� ������ ��� ��������� �����
        //        if (ArrayVertexFirst[0, 0] == ArrayVertexFirst[0, cnt.VertexCount - 1] &
        //            ArrayVertexFirst[1, 0] == ArrayVertexFirst[1, cnt.VertexCount - 1])
        //        {
        //            //������� ������ (���������)
        //            ArrayVertex = new double[3, cnt.VertexCount - 1];
        //            for (int i = 0; i < cnt.VertexCount - 1; i++)
        //            {
        //                // �������� ���������� � ����� �����
        //                cnt.GetVertex(i, out x, out y, out c);
        //                ArrayVertex[0, i] = x;
        //                ArrayVertex[1, i] = y;
        //                ArrayVertex[2, i] = c;
        //            }
        //        }
        //        else
        //        {
        //            //������� ������ (������)
        //            ArrayVertex = new double[3, cnt.VertexCount];
        //            for (int i = 0; i < cnt.VertexCount; i++)
        //            {
        //                // �������� ���������� � ����� �����
        //                cnt.GetVertex(i, out x, out y, out c);
        //                ArrayVertex[0, i] = x;
        //                ArrayVertex[1, i] = y;
        //                ArrayVertex[2, i] = c;
        //            }
        //        }

        //    }
        //    List<Segment> segmentList = new List<Segment>();
        //    for (int i = 0; i < ArrayVertex.Length / 3 - 1; i++)
        //    {
        //        segmentList.Add(new Segment(new PointD(ArrayVertex[0, i], ArrayVertex[1, i], ArrayVertex[2, i]),
        //            new PointD(ArrayVertex[0, i + 1], ArrayVertex[1, i + 1], ArrayVertex[2, i + 1])));
        //        if (i + 1 == ArrayVertex.Length / 3 - 1)
        //            segmentList.Add(new Segment(new PointD(ArrayVertex[0, i + 1], ArrayVertex[1, i + 1], ArrayVertex[2, i + 1]),
        //                new PointD(ArrayVertex[0, 0], ArrayVertex[1, 0], ArrayVertex[2, 0])));
        //    }
        //    return segmentList;
        //}

        /// <summary>
        /// �-���� ���������� ����������� ������������ ������ ������� ������
        /// </summary>
        /// <param name="aMapObject"></param>
        /// <returns></returns>
        public List<Segment> FillSegmentList(IIngeoContourPart cnt, out List<PointD> ZUPoints)
        {
            ZUPoints = new List<PointD>();
            double x;
            double y;
            double c;
            double[,] ArrayVertexFirst;
            double[,] ArrayVertex = null;

            if (cnt.Closed) //��������� ����� ��� ���������� ������� ��
            {
                //��������� ������(����� ���� � ������������ �������)
                ArrayVertexFirst = new double[3, cnt.VertexCount];
                for (int i = 0; i < cnt.VertexCount; i++)
                {
                    // �������� ���������� � ����� �����
                    cnt.GetVertex(i, out x, out y, out c);
                    ArrayVertexFirst[0, i] = x;
                    ArrayVertexFirst[1, i] = y;
                }
                //���� ��������� � ������ ����� ���������, �������������� ������ ��� ��������� �����
                if (ArrayVertexFirst[0, 0] == ArrayVertexFirst[0, cnt.VertexCount - 1] &
                    ArrayVertexFirst[1, 0] == ArrayVertexFirst[1, cnt.VertexCount - 1])
                {
                    //������� ������ (���������)
                    ArrayVertex = new double[3, cnt.VertexCount - 1];
                    for (int i = 0; i < cnt.VertexCount - 1; i++)
                    {
                        // �������� ���������� � ����� �����
                        cnt.GetVertex(i, out x, out y, out c);
                        ArrayVertex[0, i] = x;
                        ArrayVertex[1, i] = y;
                        ArrayVertex[2, i] = c;
                    }
                }
                else
                {
                    //������� ������ (������)
                    ArrayVertex = new double[3, cnt.VertexCount];
                    for (int i = 0; i < cnt.VertexCount; i++)
                    {
                        // �������� ���������� � ����� �����
                        cnt.GetVertex(i, out x, out y, out c);
                        ArrayVertex[0, i] = x;
                        ArrayVertex[1, i] = y;
                        ArrayVertex[2, i] = c;
                        ZUPoints.Add(new PointD(x, y, c));
                    }
                }

            }
            List<Segment> segmentList = new List<Segment>();
            for (int i = 0; i < ArrayVertex.Length / 3 - 1; i++)
            {

                segmentList.Add(new Segment(new PointD(ArrayVertex[0, i], ArrayVertex[1, i], ArrayVertex[2, i]),
                    new PointD(ArrayVertex[0, i + 1], ArrayVertex[1, i + 1], ArrayVertex[2, i + 1])));
                if (i + 1 == ArrayVertex.Length / 3 - 1)
                    segmentList.Add(new Segment(new PointD(ArrayVertex[0, i + 1], ArrayVertex[1, i + 1], ArrayVertex[2, i + 1]),
                        new PointD(ArrayVertex[0, 0], ArrayVertex[1, 0], ArrayVertex[2, 0])));

            }
            return segmentList;
        }
    }
    public class DistanceBetweenPoints
    {
        public PointD p1, p2;
        public double distance;

        public DistanceBetweenPoints(PointD aP1, PointD aP2)
        {
            p1 = aP1;
            p2 = aP2;
            distance = Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

    }

    public class DistanceToSegment
    {
        public Segment bufferSegment;
        public Segment ZUSegment;
        public PointD pointOnZu;
        public PointD pointOnBuffer;
        public double distance;

        public DistanceToSegment(Segment aBufferSegment, Segment aZUSegment, PointD aPointOnBuffer, PointD aPointOnZu, double aDistance)
        {
            bufferSegment = aBufferSegment;
            ZUSegment = aZUSegment;
            pointOnZu = aPointOnZu;
            pointOnBuffer = aPointOnBuffer;
            distance = aDistance;
        }
    }


    public struct Segment
    {
        public PointD P1;
        public PointD P2;

        public Segment(PointD aP1, PointD aP2)
        {
            P1 = aP1;
            P2 = aP2;
        }
    }

    public struct PointD
    {
        public double X;
        public double Y;
        public double C;
        public PointD(double x, double y, double c)
        {
            X = x;
            Y = y;
            C = c;
        }

        public Point ToPoint()
        {
            return new Point((int)X, (int)Y);
        }

        public override bool Equals(object obj)
        {
            return obj is PointD && this == (PointD)obj;
        }
        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }
        public static bool operator ==(PointD a, PointD b)
        {
            return a.X == b.X && a.Y == b.Y;
        }
        public static bool operator !=(PointD a, PointD b)
        {
            return !(a == b);
        }
    }
}
