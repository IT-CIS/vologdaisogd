﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.GPZU;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using DevExpress.XtraEditors;
using AISOGD.General;
using VologdaIsogd.Module.Controllers.Delo;
using AISOGD.DocFlow;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;

namespace VologdaIsogd.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ChangeStatusesGradPlanController : ViewController
    {
        public ChangeStatusesGradPlanController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Утверждение градплана
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GradPlanSubmitAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            GradPlan gradPlan;
            gradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)gradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            if (gradPlan.EDocStatus == AISOGD.Enums.eDocStatus.Отказ)
            {
                XtraMessageBox.Show("Документ с данным статусом нельзя утвердить!");
                return;
            }
            if (gradPlan.DocDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Не указана дата документа");
                return;
            }
            if (gradPlan.DocNo == null || gradPlan.DocNo == "")
            {
                XtraMessageBox.Show("Необходимо указать корректный номер документа");
                return;
            }
            GradPlan anothergradPlan = connect.FindFirstObject<GradPlan>(x => x.DocNo == gradPlan.DocNo && x.DocDate == gradPlan.DocDate);
            if (gradPlan.Oid.ToString() != anothergradPlan.Oid.ToString())
            {
                XtraMessageBox.Show("В Системе уже внесен другой документ с подобными реквизитам.");
                return;
            }
            gradPlan.EDocStatus = AISOGD.Enums.eDocStatus.Утвержден;

            try
            {
                gradPlan.FullDocNo = gradPlan.DocKind.Prefix + gradPlan.DocNo + gradPlan.DocKind.Suffix; // + gradPlan.DocDate.Year.ToString();
            }
            catch { }

            gradPlan.Save();
            unitOfWork.CommitChanges();
            // заполняем семантику ГИС объекта
            // из таблицы связи получаем Ид реестрового объекта ЗУ
            SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.GPZU.GradPlan", gradPlan.Oid.ToString()).FirstOrDefault<SpatialRepository>();
            if (objLink != null)
            {
                try
                {
                    SeGradPlanSemantic(m_MapInfo, gradPlan, objLink.SpatialLayerId, objLink.SpatialObjectId);
                }
                catch { }
            }
            foreach (InLetter letter in gradPlan.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            // создаем документ для регистрации в ИСОГД 
            try
            {
                DocImportToIsogdController DocImportToIsogdController = new DocImportToIsogdController();
                string idocNo = "";
                DateTime idocDate = DateTime.Now.Date;
                string icadNo = "";
                string iaddrInfo = "";
                try { idocNo = gradPlan.FullDocNo; }
                catch { }

                try { idocDate = gradPlan.DocDate; }
                catch { }

                try { icadNo = gradPlan.LotCadNo; }
                catch { }

                try { iaddrInfo = gradPlan.ObjectAddress; }
                catch { }
                List<AttachmentFiles> attachs = new List<AttachmentFiles>();
                try
                {
                    foreach (AttachmentFiles att in gradPlan.AttachmentFiles)
                    {
                        attachs.Add(att);
                    }
                }
                catch { }
                DocImportToIsogdController.CreateDocImportToIsogd(connect, idocNo, idocDate, "060100", "Градостроительный план  земельного участка", icadNo, iaddrInfo, attachs);
            }
            catch { }
            unitOfWork.CommitChanges();
        }

        /// <summary>
        /// Функция заполнения семантики у градплана
        /// </summary>
        /// <param name="aMapInfo"></param>
        /// <param name="constrStage"></param>
        /// <param name="aLayer"></param>
        public void SeGradPlanSemantic(MapInfoApplication aMapInfo, GradPlan gradPlan, string aLayer, string aMI_PRINX)
        {
            aMapInfo.Do("Select * From " + aLayer + " Where MI_PRINX = " + aMI_PRINX);// + " Into zObject");
            try
            {
                aMapInfo.Do("Update Selection Set Номер_ГП =  \"" + gradPlan.DocNo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Дата_регистрации_ГП =  \"" + gradPlan.DocDate.ToShortDateString() + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Действующий =  \"" + "да" + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Регистрационный_номер_заявки =  \"" + gradPlan.LetterNo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Дата_регистрации_заявки =  \"" + gradPlan.LetterDate.ToShortDateString() + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Заявитель =  \"" + gradPlan.Subject + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Назнач_объекта_строительства =  \"" + gradPlan.ObjectName + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Назнач_объекта_строительства =  \"" + gradPlan.ObjectName + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Тип_объекта_строительства =  \"" + gradPlan.ObjectName + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                string submitInfo = "";
                submitInfo = String.Format("постановление Администрации города Вологды от {0} №{1}", 
                    gradPlan.GPZUSubmitDocDate.ToShortDateString(), gradPlan.GPZUSubmitDocNo);
                aMapInfo.Do("Update Selection Set Документы_ГП =  \"" + submitInfo + "\" Commit table " + aLayer);
            }
            catch { }
            try
            {
                aMapInfo.Do("Update Selection Set Ответственный_исполнитель =  \"" + gradPlan.Empl.BriefName + "\" Commit table " + aLayer);
            }
            catch { }

            //Закрыть все слои запросов
            int NumTables = Convert.ToInt16(aMapInfo.Eval("NumTables()"));
            List<string> queryTablesList = new List<string>();
            for (int k = 1; k <= NumTables; k++)
            {
                TableInfo TableToClose = new TableInfo(aMapInfo, k);
                if (TableToClose.Name.IndexOf("Query") != -1)
                    queryTablesList.Add(TableToClose.Name);
            }
            foreach (string queryTable in queryTablesList)
            {
                try
                {
                    aMapInfo.TableInfo(queryTable).Dispose();
                }
                catch { }
            }
        }

        /// <summary>
        /// Отказ 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GradPlanRefuseAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            GradPlan gradPlan;
            gradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)gradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            if (gradPlan.EDocStatus != AISOGD.Enums.eDocStatus.Подготовка)
            {
                XtraMessageBox.Show("Можно отменить только документ со статусом 'Подготовка'!");
                return;
            }
            gradPlan.EDocStatus = AISOGD.Enums.eDocStatus.Отказ;
           
            MapInfoApplication m_MapInfo;
            m_MapInfo = new MapInfoApplication();
            var spatial = new SpatialController<GisLayer>(connect);

            try
            {
                SpatialRepository objLink = spatial.GetSpatialLinks("AISOGD.GPZU.GradPlan", gradPlan.Oid.ToString()).FirstOrDefault<SpatialRepository>();
                m_MapInfo.Do("Select * From " + objLink.SpatialLayerId + " Where MI_PRINX = " + objLink.SpatialObjectId);
                
                m_MapInfo.Do("Update Selection Set Действующий =  \"" + "Нет" + "\" Commit table " + objLink.SpatialLayerId);
               
            }
            catch { }

            

            foreach (InLetter letter in gradPlan.InLetter)
            {
                letter.InLetterStatus = AISOGD.Enums.eInLetterStatus.Завершено;
                letter.Save();
            }
            gradPlan.Save();
            unitOfWork.CommitChanges();
        }
    }
}
