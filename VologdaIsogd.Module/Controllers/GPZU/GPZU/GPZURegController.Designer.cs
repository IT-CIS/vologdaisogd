namespace WinAisogdIngeo.Module.Controllers
{
    partial class GPZURegController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FindLotsFromGPZUAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.GPZUPrepareAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // FindLotsFromGPZUAction
            // 
            this.FindLotsFromGPZUAction.Caption = "���������� ��������� �������";
            this.FindLotsFromGPZUAction.ConfirmationMessage = null;
            this.FindLotsFromGPZUAction.Id = "FindLotsFromGPZUAction";
            this.FindLotsFromGPZUAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.FindLotsFromGPZUAction.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.FindLotsFromGPZUAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.FindLotsFromGPZUAction.ToolTip = null;
            this.FindLotsFromGPZUAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.FindLotsFromGPZUAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FindLotsFromGPZUAction_Execute);
            // 
            // GPZUPrepareAction
            // 
            this.GPZUPrepareAction.Caption = "���������� ����";
            this.GPZUPrepareAction.ConfirmationMessage = null;
            this.GPZUPrepareAction.Id = "GPZUPrepareAction";
            this.GPZUPrepareAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.GPZUPrepareAction.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.GPZUPrepareAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.GPZUPrepareAction.ToolTip = null;
            this.GPZUPrepareAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.GPZUPrepareAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GPZUPrepareAction_Execute);
            // 
            // GPZURegController
            // 
            this.Actions.Add(this.FindLotsFromGPZUAction);
            this.Actions.Add(this.GPZUPrepareAction);
            this.Tag = "����";
            this.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction GPZUPrepareAction;
        public DevExpress.ExpressApp.Actions.SimpleAction FindLotsFromGPZUAction;
    }
}
