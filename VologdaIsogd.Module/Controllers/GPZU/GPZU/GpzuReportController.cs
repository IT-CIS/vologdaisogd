using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using AISOGD.GPZU;
using DevExpress.Xpo;
using AISOGD.SystemDir;
using Ingeo;
using System.IO;
using DevExpress.XtraEditors;
using System.Drawing;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit;
using DevExpress.Office;
using AISOGD.Monuments;
using System.Reflection;
using AISOGD.Reglament;

namespace WinAisogdIngeo.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class GpzuReportController : ViewController
    {
        public GpzuReportController()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.GetController<GpzuReportController>().GpzuReportParamAction.Active.SetItemValue("myReason", false);
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void GpzuReportParamAction_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            // 

            string paramValue = e.ParameterCurrentValue as string;
            //XtraMessageBox.Show(paramValue);
            double scale = 2000;
            if (paramValue != null || paramValue != String.Empty || paramValue != "0")
                scale = Convert.ToDouble(paramValue);
            if (scale == 0)
                scale = 2000;
            //try
            //{
            //    if (paramValue != null || paramValue != String.Empty)
            //        scale = Convert.ToDouble(paramValue);
            //}
            //catch { }
            //XtraMessageBox.Show(scale.ToString());
            // �������� ���� � ��� ������ (UnitOfWork)
            GradPlan i_GradPlan;
            i_GradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_GradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            SpatialController<GisLayer> spatial = new SpatialController<GisLayer>(connect);
            SpatialRepository spatialRepoGradPlan;

            //bool isLotsSpatialExist; 
            IngeoSpatial ingeoSpatial = new IngeoSpatial();
            IIngeoApplication ingeoApp = ingeoSpatial.GetActiveIngeo();
            List<IIngeoMapObject> mapObjects = new List<IIngeoMapObject>();
            IIngeoMapObject GPZUmapObject = null;
            string GPZUmapObjectId = "";

            //if (ingeoApp == null)
            //{
            //    XtraMessageBox.Show("InGeo �� ��������", "����");
            //    return;
            //}
            if (ingeoApp != null)
            {
                // ������������ �����������
                // ������� ��������������� ������ ��� ������������ �������� � ��������� ��� � ���� � ������� ���������
                // ������� ��������� ���������� �� �� ���������, ���������� � ��� ���� ��������, ����� ��������� �� � 
                // GradPlanImage � ������ ���� ����������
                string directoryTempName = Path.GetTempPath() + @"\WinIsogd\GradPlan\" + i_GradPlan.Oid.ToString();
                if (!Directory.Exists(directoryTempName))
                    Directory.CreateDirectory(directoryTempName);
                //�������� � ������
                byte[] imagebyte;
                string imageString = "";
                string layerGroupName = "����� ������������������ �����";
                string[] layerIds;
                string[] rasterMapIds;
                // �������� ��������� ���������� �������
                spatialRepoGradPlan = connect.FindFirstObject<SpatialRepository>(mc =>
                   mc.IsogdClassName == i_GradPlan.ClassInfo.FullName &&
                   mc.IsogdObjectId == i_GradPlan.Oid.ToString());
                if (spatialRepoGradPlan == null)
                {
                    XtraMessageBox.Show("����������� ��� ������ ����. ��� �����������, ���������� ��� ���������� "
                    + " � ������� � ��������� ���������.", "������!");
                    return;
                }
                GPZUmapObjectId = spatialRepoGradPlan.SpatialObjectId;
                // 1. �������� ��� ������ ������� ����
                GPZUmapObject = IngeoSpatial.GetOneMapObjectBySpatialObjectID(ingeoApp, GPZUmapObjectId);
                if (GPZUmapObject == null)
                {
                    XtraMessageBox.Show("����������� ��� ������ ����. ��� �����������, ���������� ��� ���������� "
                    + " � ������� � ��������� ���������.", "������!");
                    return;
                }
                // 2. �������� bbox
                mapObjects.Add(GPZUmapObject);
                //IngeoSpatial.
                IngeoSpatial.Rect bbox = IngeoSpatial.GetBoundRect(mapObjects.ToArray());
                //mapObjects
                // 3. �������� ������� �� ����� � �������
                layerIds = spatial.GetLayersIdsFromLayerGroupName(layerGroupName).ToArray();
                rasterMapIds = spatial.GetMapsIdsFromLayerGroupName(layerGroupName).ToArray();
                imageString = IngeoSpatial.MakeImageBase64(ingeoApp, rasterMapIds, layerIds, mapObjects.ToArray(),
                    1 / scale, 192, TInImageDataType.inidJPEG, bbox);
                imagebyte = Convert.FromBase64String(imageString);
                AddHelpImage(i_GradPlan, layerGroupName, imagebyte, i_GradPlan.Session,
                    directoryTempName, null);
                unitOfWork.CommitChanges();
            }
            // ��������� ������������ ������ �������
            // ������� ��� ������ �������
            IReportDataV2 reportData = (IReportDataV2)ObjectSpace.FindObject(Application.Modules.FindModule<ReportsModuleV2>().ReportDataType, new BinaryOperator("DisplayName", "��������"));
            // ��������� ���� � ������ ��������� ��� ������ ������� (� �������� �������� ��� ��)
            string reportContainerHandle = ReportDataProvider.ReportsStorage.GetReportContainerHandle(reportData);
            Frame.GetController<ReportServiceController>().ShowPreview(reportContainerHandle, new BinaryOperator("Oid", i_GradPlan.Oid.ToString()));
        }

        /// <summary>
        /// ������� �������� ��������� GpzuImage � ��������� ��� ������������ ������������ �����������
        /// </summary>
        /// <param name="gradinfo">������ �����������</param>
        /// <param name="imageName">��� ��������, ������� ����� ����������� ���������. �� ���� ���������� �������� �� ������������ </param>
        /// <param name="imagebyte">�������� � ������</param>
        /// <param name="s">������</param>
        /// <param name="directoryTempName">���� ����������, ��� �������� �������� ��� ������� � GpzuImage</param>
        public void AddHelpImage(GradPlan gradplan, string imageName, byte[] imagebyte, Session s,
            string directoryTempName, string[] imageLegend)
        {
            bool isExist = false;

            //foreach (GpzuImage gpzuIm in gradplan.GpzuImage)
            //{
            //    //gradplan.GpzuImage.Remove(gpzuIm);
            //    gpzuIm.Delete();
            //}
            if (gradplan.GpzuImage.Count > 0)
            {
                gradplan.GpzuImage.First().Delete();
                gradplan.Save();
            }
            //if (!isExist)
            //{
            Guid g;
            g = Guid.NewGuid();
            string imagePath = directoryTempName + "\\" + g + ".jpg";
            GpzuImage gpzuImage = new GpzuImage(s);
            gpzuImage.Name = imageName;
            using (var ms = new MemoryStream(imagebyte))
            {
                Image image = Image.FromStream(ms);
                image.Save(imagePath);
                gpzuImage.Image = Image.FromFile(imagePath);
                gpzuImage.Save();
                ms.Dispose();
                image.Dispose();
            }
            //AddHelpImageLegend(gpzuImage, s, directoryTempName, imageLegend);
            gradplan.GpzuImage.Add(gpzuImage);
            gradplan.Save();

            //}
        }

        private void GradPlanReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ���� � ��� ������ (UnitOfWork)
            View.ObjectSpace.CommitChanges();
            GradPlan i_GradPlan;
            i_GradPlan = (GradPlan)e.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)i_GradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            // ��������� ������������ ������ �������
            CreateGradPlanTemplate(i_GradPlan, unitOfWork);
        }

        public void CreateGradPlanTemplate(GradPlan gradplan, UnitOfWork unitOfWork)
        {
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("WinAisogdIngeo.Module.DLL", @"\DocTemplates\GPZU\GradPlan.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);



            // �������� �������������� �������
            try
            {
                string ObjectName = gradplan.ObjectName;
                Bookmark ObjectNameBook = richServer.Document.Bookmarks["ObjectNameBook"];
                richServer.Document.Replace(ObjectNameBook.Range, ObjectName);
            }
            catch { }

            // ��������
            try
            {
                string Subject = gradplan.Subject;
                Bookmark SubjectBook = richServer.Document.Bookmarks["SubjectBook"];
                richServer.Document.Replace(SubjectBook.Range, Subject);
            }
            catch { }

            // ������� ���
            try
            {
                string ThisYear = DateTime.Now.Year.ToString();
                Bookmark ThisYearBook = richServer.Document.Bookmarks["ThisYearBook"];
                richServer.Document.Replace(ThisYearBook.Range, ThisYear);
            }
            catch { }

            // ������ � �������� 14, 15, 16 ����� ������ ���������
            string DocNo = "";
            try
            {
                DocNo = gradplan.DocNo.Substring(13, 1);
                Bookmark DocNoBook14 = richServer.Document.Bookmarks["DocNoBook14"];
                richServer.Document.Replace(DocNoBook14.Range, DocNo);
            }
            catch { }
            try
            {
                DocNo = gradplan.DocNo.Substring(14, 1);
                Bookmark DocNoBook15 = richServer.Document.Bookmarks["DocNoBook15"];
                richServer.Document.Replace(DocNoBook15.Range, DocNo);
            }
            catch { }
            try
            {
                DocNo = gradplan.DocNo.Substring(15, 1);
                Bookmark DocNoBook16 = richServer.Document.Bookmarks["DocNoBook16"];
                richServer.Document.Replace(DocNoBook16.Range, DocNo);
            }
            catch { }

            // ��������� ���������� ���������
            try
            {
                string GpzuReason = gradplan.GpzuReason;
                Bookmark DocReasonBook = richServer.Document.Bookmarks["DocReasonBook"];
                richServer.Document.Replace(DocReasonBook.Range, GpzuReason);
            }
            catch { }
            // �����
            try
            {
                string CityRegion = gradplan.CityRegion.FullName;
                Bookmark CityRegionBook = richServer.Document.Bookmarks["CityRegionBook"];
                richServer.Document.Replace(CityRegionBook.Range, CityRegion);
            }
            catch { }
            // �����
            try
            {
                string Street = gradplan.Street.FullName;
                Bookmark StreetBook = richServer.Document.Bookmarks["StreetBook"];
                richServer.Document.Replace(StreetBook.Range, Street);
            }
            catch { }
            // ����������� ����� ���������� �������
            try
            {
                string CadNo = "";
                try { CadNo = gradplan.LotCadNo.TrimEnd(','); }
                catch { }
                Bookmark CadNoBook = richServer.Document.Bookmarks["CadNoBook"];
                richServer.Document.Replace(CadNoBook.Range, CadNo);
            }
            catch { }
            // �������� �������������� ������ ���������� �������
            try
            {
                string LotAddrInfo = "";
                try { LotAddrInfo = gradplan.LotAddressInfo; }
                catch { }
                Bookmark LotAddrInfoBook = richServer.Document.Bookmarks["LotAddrInfoBook"];
                richServer.Document.Replace(LotAddrInfoBook.Range, LotAddrInfo);
            }
            catch { }

            // ������� ���������� �������
            try
            {
                string LotSquare = "";
                try { LotSquare = gradplan.LotSquare; }
                catch { }
                Bookmark LotSquareBook = richServer.Document.Bookmarks["LotSquareBook"];
                richServer.Document.Replace(LotSquareBook.Range, LotSquare);
            }
            catch { }

            // �������� �������������� �������������� �������
            try
            {
                string ObjectAddress = "";
                try { ObjectAddress = gradplan.ObjectAddress; }
                catch { }
                Bookmark ObjectAddressBook = richServer.Document.Bookmarks["ObjectAddressBook"];
                richServer.Document.Replace(ObjectAddressBook.Range, ObjectAddress);
            }
            catch { }

            // ���� ���������
            try
            {
                string DocDate = "";

                //string d = gradplan.DocDate.Day.ToString();

                //if (d.Length == 1)
                //    d = "0" + d;
                //DocDate = d + " ";
                //switch (gradplan.DocDate.Month)
                //{
                //    case 1:
                //        DocDate += "������";
                //        break;
                //    case 2:
                //        DocDate += "�������";
                //        break;
                //    case 3:
                //        DocDate += "�����";
                //        break;
                //    case 4:
                //        DocDate += "������";
                //        break;
                //    case 5:
                //        DocDate += "���";
                //        break;
                //    case 6:
                //        DocDate += "����";
                //        break;
                //    case 7:
                //        DocDate += "����";
                //        break;
                //    case 8:
                //        DocDate += "�������";
                //        break;
                //    case 9:
                //        DocDate += "��������";
                //        break;
                //    case 10:
                //        DocDate += "�������";
                //        break;
                //    case 11:
                //        DocDate += "������";
                //        break;
                //    case 12:
                //        DocDate += "�������";
                //        break;
                //    default:
                //        DocDate += gradplan.DocDate.Month.ToString();
                //        break;
                //}
                //DocDate += " " + gradplan.DocDate.Year.ToString();
                DocDate += " " + gradplan.DocDate.ToShortDateString();
                Bookmark DocDateBook1 = richServer.Document.Bookmarks["DocDateBook1"];
                richServer.Document.Replace(DocDateBook1.Range, DocDate);
                Bookmark DocDateBook2 = richServer.Document.Bookmarks["DocDateBook2"];
                richServer.Document.Replace(DocDateBook2.Range, DocDate);
            }
            catch { }

            // ������������ ��������������� ����
            try
            {
                string TerrZoneName = "";
                try { TerrZoneName = gradplan.TerrZoneKindLink.Name; }
                catch { }
                Bookmark TerrZoneNameBook = richServer.Document.Bookmarks["TerrZoneNameBook"];
                richServer.Document.Replace(TerrZoneNameBook.Range, TerrZoneName);
            }
            catch { }

            // ���� ������������ �������������
            // �������� ���� ������������ �������������
            //try
            //{
                string ZFUKCode = "";
                string ZFUKName = "";
                string ZFUKObjectType = "";
                    int ir = 0;
                    Table tabler = richServer.Document.Tables[6];

                    range = tabler.Rows[2].Cells[0].ContentRange;
                    richServer.Document.Replace(range, "");

                    range = tabler.Rows[2].Cells[1].ContentRange;
                    richServer.Document.Replace(range, "");

                    range = tabler.Rows[2].Cells[2].ContentRange;
                    richServer.Document.Replace(range, "");

                    SortProperty sort = new SortProperty("FunctionalUsingKind.Code", DevExpress.Xpo.DB.SortingDirection.Ascending);
                    gradplan.TerrZoneKindLink.MainZoneFunctionalUsingKind.Sorting.Add(sort);

                    foreach (MainZoneFunctionalUsingKind ZFUK in gradplan.TerrZoneKindLink.MainZoneFunctionalUsingKind)
                    {
                        try
                        {
                            ZFUKCode = ZFUK.FunctionalUsingKind.Code;
                        }
                        catch { }
                        try
                        {
                            ZFUKName = ZFUK.FunctionalUsingKind.Name;
                        }
                        catch { }
                        try
                        {
                            ZFUKObjectType = ZFUK.FunctionalUsingKind.ObjectTypes;
                        }
                        catch { }

                        // ������ ��� �� �����
                        if (ir == 0)
                        {
                            range = tabler.Rows[2].Cells[0].ContentRange;
                            richServer.Document.Replace(range, ZFUKName);

                            range = tabler.Rows[2].Cells[1].ContentRange;
                            richServer.Document.Replace(range, ZFUKObjectType);

                            range = tabler.Rows[2].Cells[2].ContentRange;
                            richServer.Document.Replace(range, ZFUKCode);

                        }
                        // ���� ��� ������ 1 �������� ������� � ��������� ����� ��������
                        else
                        {
                            tabler.Rows.InsertAfter(ir+1);

                            //range = tabler.Rows[ir+2].Range;
                            //DocumentPosition pos = range.End;
                            //richServer.Document.InsertText(pos, Characters.LineBreak.ToString());
                            ////pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
                            //richServer.Document.GetText(range);
                            //richServer.Document.InsertDocumentContent(pos, range);

                            range = tabler.Rows[ir + 2].Cells[0].ContentRange;
                            richServer.Document.Replace(range, ZFUKName);

                            range = tabler.Rows[ir + 2].Cells[1].ContentRange;
                            richServer.Document.Replace(range, ZFUKObjectType);

                            range = tabler.Rows[ir + 2].Cells[2].ContentRange;
                            richServer.Document.Replace(range, ZFUKCode);
                        }
                        ir++;
                    }

                    //// �������-����������� ���� �������������
                    //SortProperty sort = new SortProperty("FunctionalUsingKind.Code", DevExpress.Xpo.DB.SortingDirection.Ascending);
                    gradplan.TerrZoneKindLink.CondZoneFunctionalUsingKind.Sorting.Add(sort);
                    int c = 0;
                    foreach (CondZoneFunctionalUsingKind ZFUK in gradplan.TerrZoneKindLink.CondZoneFunctionalUsingKind)
                    {
                        c++;
                        try
                        {
                            ZFUKCode = ZFUK.FunctionalUsingKind.Code;
                        }
                        catch { }
                        try
                        {
                            ZFUKName = ZFUK.FunctionalUsingKind.Name;
                        }
                        catch { }
                        try
                        {
                            ZFUKObjectType = ZFUK.FunctionalUsingKind.ObjectTypes;
                        }
                        catch { }

                        // ������ ��� �� �����
                        if (ir == 0)
                        {
                            range = tabler.Rows[4].Cells[0].ContentRange;
                            richServer.Document.Replace(range, ZFUKName);

                            range = tabler.Rows[4].Cells[1].ContentRange;
                            richServer.Document.Replace(range, ZFUKObjectType);

                            range = tabler.Rows[4].Cells[2].ContentRange;
                            richServer.Document.Replace(range, ZFUKCode);

                        }
                        // ���� ��� ������ 1 �������� ������� � ��������� ����� ��������
                        else
                        {
                            range = tabler.Rows[ir + 3].Cells[0].ContentRange;
                            richServer.Document.Replace(range, ZFUKName);

                            range = tabler.Rows[ir + 3].Cells[1].ContentRange;
                            richServer.Document.Replace(range, ZFUKObjectType);

                            range = tabler.Rows[ir + 3].Cells[2].ContentRange;
                            richServer.Document.Replace(range, ZFUKCode);

                            if (c < gradplan.TerrZoneKindLink.CondZoneFunctionalUsingKind.Count)
                                tabler.Rows.InsertAfter(ir + 3);
                        }
                        ir++;
                    }
                    //// ��������������� ���� ������������ �������������
                    int a = 0;
                    foreach (AddZoneFunctionalUsingKind ZFUK in gradplan.TerrZoneKindLink.AddZoneFunctionalUsingKind)
                    {
                        a++;
                        try
                        {
                            ZFUKName = ZFUK.FunctionalUsingKind.Name;
                        }
                        catch { }

                        // ������ ��� �� �����
                        if (ir == 0)
                        {
                            range = tabler.Rows[4].Cells[0].ContentRange;
                            richServer.Document.Replace(range, ZFUKName);
                        }
                        // ���� ��� ������ 1 �������� ������� � ��������� ����� ��������
                        else
                        {
                            range = tabler.Rows[ir + 4].Cells[0].ContentRange;
                            richServer.Document.Replace(range, ZFUKName);
                            if (a< gradplan.TerrZoneKindLink.AddZoneFunctionalUsingKind.Count)
                                tabler.Rows.InsertAfter(ir + 4);
                        }
                        ir++;
                    }

            
            // ����������� �� ����� ���
            try
            {
                string SZZoneRestriction = "";
                List<string> bolds = new List<string>();
                foreach(SpecialZoneKind SZZone in gradplan.SpecialZoneKind)
                {
                    string bold = SZZone.GradRestrictionText.Split(':')[0] + ":";
                    bolds.Add(bold);
                    SZZoneRestriction += SZZone.GradRestrictionText + Environment.NewLine;
                }
                SZZoneRestriction.TrimEnd(Environment.NewLine.ToCharArray());
                Bookmark SZZoneRestrictionBook = richServer.Document.Bookmarks["SZZoneRestrictionBook"];
                richServer.Document.InsertText(SZZoneRestrictionBook.Range.Start, SZZoneRestriction);

                bolds.Add("�������������� ����������� � �������� ���������� �������� ����� (������ � 10 � ��� ���� ��������):");
                foreach (string b in bolds)
                {
                    DocumentRange[] ranges = richServer.Document.FindAll(b, DevExpress.XtraRichEdit.API.Native.SearchOptions.None, SZZoneRestrictionBook.Range);
                    SubDocument doc = ranges[0].BeginUpdateDocument();
                    DevExpress.XtraRichEdit.API.Native.CharacterProperties cp = doc.BeginUpdateCharacters(ranges[0]);
                    cp.Bold = true;
                    doc.EndUpdateCharacters(cp);
                }

            }
            catch { }

            // ���������� ������� ������������ �������������
            try
            {
                string PlaceNo = "";
                string PlaceName = "";
                int i = 0;
                Table table = richServer.Document.Tables[7];

                range = table.Rows[0].Cells[1].ContentRange;
                richServer.Document.Replace(range, "");

                range = table.Rows[0].Cells[3].ContentRange;
                richServer.Document.Replace(range, "");
                if (gradplan.underConstructionPlace.Count == 0)
                {
                    PlaceName = gradplan.ObjectName;
                    range = table.Rows[0].Cells[1].ContentRange;
                    richServer.Document.Replace(range, "1");

                    range = table.Rows[0].Cells[3].ContentRange;
                    richServer.Document.Replace(range, PlaceName);
                }
                else
                {
                    //sort = new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending);
                    //gradplan.underConstructionPlace.Sorting.Add(sort);
                    foreach (UnderConstructionPlace place in gradplan.underConstructionPlace)
                    {

                        try
                        {
                            PlaceNo = place.No;
                        }
                        catch { }
                        try
                        {
                            PlaceName = place.Name;
                        }
                        catch { }

                        // ������ ��� �� �����
                        if (i == 0)
                        {
                            range = table.Rows[0].Cells[1].ContentRange;
                            richServer.Document.Replace(range, PlaceNo);

                            range = table.Rows[0].Cells[3].ContentRange;
                            richServer.Document.Replace(range, PlaceName);

                        }
                        // ���� ��� ������ 1 �������� ������� � ��������� ����� ��������
                        else
                        {
                            range = table.Range;
                            DocumentPosition pos = range.End;
                            richServer.Document.InsertText(pos, Characters.LineBreak.ToString());
                            //pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
                            richServer.Document.GetText(range);
                            richServer.Document.InsertDocumentContent(pos, range);
                            range = table.Rows[0].Cells[1].ContentRange;
                            richServer.Document.Replace(range, PlaceNo);

                            range = table.Rows[0].Cells[3].ContentRange;
                            richServer.Document.Replace(range, PlaceName);
                        }
                        i++;
                    }
                }
            }
            catch { }

            #region // old version
            //try
            //{
            //    //DocumentRange range = richServer.Document.CreateRange(0, 0);
            //    string PlaceNo = "";
            //    string PlaceName = "";
            //    int i = 0;
            //    //try
            //    //{
            //        // ������� �������� ������ � ������ � ������� (�� ������, ���� ��� ���� ����������� ����������)
            //        range = richServer.Document.Tables[4].Rows[0].Cells[1].ContentRange;
            //        richServer.Document.Replace(range, "");

            //        range = richServer.Document.Tables[4].Rows[0].Cells[3].ContentRange;
            //        richServer.Document.Replace(range, "");

            //        foreach (UnderConstructionPlace place in gradplan.underConstructionPlace)
            //        {

            //            //try
            //            //{
            //                PlaceNo = place.No;
            //                PlaceName = place.Name;
            //                if (richServer.Document.Tables[4].Rows.Count < 2*i + 2)
            //                {
            //                    richServer.Document.Tables[4].Rows.InsertAfter(2*i-1);
            //                    richServer.Document.Tables[4].Rows.InsertAfter(2*i);
            //                    richServer.Document.Tables[4].Rows.InsertAfter(2 * i + 1); // ������ ������
            //                    richServer.Document.Tables[4].Rows[i * 2].Cells[1].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    richServer.Document.Tables[4].Rows[i * 2].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    richServer.Document.Tables[4].Rows[i * 2 - 1].Cells[1].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    richServer.Document.Tables[4].Rows[i * 2 - 1].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                }
            //                if (i == 0)
            //                {
            //                    range = richServer.Document.Tables[4].Rows[0].Cells[1].ContentRange;
            //                    richServer.Document.Replace(range, PlaceNo);

            //                    range = richServer.Document.Tables[4].Rows[0].Cells[3].ContentRange;
            //                    richServer.Document.Replace(range, PlaceName);
            //                }
            //                if (i > 0)
            //                {
            //                    //foreach(TableCell cell in richServer.Document.Tables[4].Rows[i * 2].Cells)
            //                    //{
            //                    //    cell.Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    //}
            //                    richServer.Document.Tables[4].Rows[i * 2 ].Cells[1].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    richServer.Document.Tables[4].Rows[i * 2 ].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    richServer.Document.Tables[4].Rows[i * 2-1].Cells[1].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    richServer.Document.Tables[4].Rows[i * 2-1].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.None;


            //                    richServer.Document.Delete(richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[0].Range);
            //                    range = richServer.Document.InsertText(richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[0].Range.Start, "�");

            //                    CharacterProperties cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 12;
            //                    richServer.Document.EndUpdateCharacters(cp);

            //                    //range = richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[0].ContentRange;
            //                    //richServer.Document.Replace(range, "�");
            //                    //richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[0].Style.FontSize = 12;

            //                    richServer.Document.Delete(richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[1].Range);
            //                    range = richServer.Document.InsertText(richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[1].Range.Start, PlaceNo);

            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 12;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[1].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;

            //                    //range = richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[1].ContentRange;
            //                    //richServer.Document.Replace(range, PlaceNo);

            //                    range = richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[2].ContentRange;
            //                    richServer.Document.Replace(range, ",");

            //                    //range = richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[3].ContentRange;
            //                    //richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;
            //                    //richServer.Document.Replace(range, PlaceName);

            //                    richServer.Document.Delete(richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[3].Range);
            //                    range = richServer.Document.InsertText(richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[3].Range.Start, PlaceName);

            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 12;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;
            //                    //richServer.Document.Tables[4].Rows[i * 2 + 1].Cells[3].Style.Italic= true;

            //                    range = richServer.Document.Tables[4].Rows[i * 2 + 2].Cells[1].ContentRange;
            //                    richServer.Document.Replace(range, "(�������� �������)");
            //                    richServer.Document.Tables[4].Rows[i * 2 + 2].Cells[1].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    range = richServer.Document.Tables[4].Rows[i * 2 + 2].Cells[3].ContentRange;
            //                    richServer.Document.Replace(range, "(���������� ������� ������������ �������������)");
            //                    richServer.Document.Tables[4].Rows[i * 2 + 2].Cells[3].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                }
            //            //}
            //            //catch { }
            //            i++;
            //        }
            //    //}
            //    //catch { }

            //}
            //catch { }
            #endregion

            #region// ������ � ������� 2.2.1
            try
            {
                //DocumentRange range = richServer.Document.CreateRange(0, 0);
                Table table1 = richServer.Document.Tables[8];
                // ������� �������� ������ � ������ � �������
                range = table1.Rows[2].Cells[0].ContentRange;
                richServer.Document.Replace(range, "-");
                try { richServer.Document.Replace(range, gradplan.LotCadNo.TrimEnd(',')); }
                catch { }

                range = table1.Rows[2].Cells[1].ContentRange;
                if (gradplan.Length != null && gradplan.Length != "")
                    richServer.Document.Replace(range, gradplan.Length);
                else
                    richServer.Document.Replace(range, "-");


                range = table1.Rows[2].Cells[2].ContentRange;
                if (gradplan.Width != null && gradplan.Width != "")
                    richServer.Document.Replace(range, gradplan.Width);
                else
                    richServer.Document.Replace(range, "-");


                range = table1.Rows[2].Cells[3].ContentRange;
                if (gradplan.SzzText != null && gradplan.SzzText != "")
                    richServer.Document.Replace(range, gradplan.SzzText);
                else
                    richServer.Document.Replace(range, "-");


                range = table1.Rows[2].Cells[4].ContentRange;
                if (gradplan.Roadway != null && gradplan.Roadway != "")
                    richServer.Document.Replace(range, gradplan.Roadway);
                else
                    richServer.Document.Replace(range, "-");


                range = table1.Rows[2].Cells[5].ContentRange;
                if (gradplan.LotSquare != null && gradplan.LotSquare != "")
                    richServer.Document.Replace(range, gradplan.LotSquare);
                else
                    richServer.Document.Replace(range, "-");


                range = table1.Rows[2].Cells[6].ContentRange;
                try
                {
                    string no = "";
                    try
                    {
                        sort = new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending);
                        gradplan.underConstructionPlace.Sorting.Add(sort);
                        foreach (UnderConstructionPlace place in gradplan.underConstructionPlace)
                        {
                            no += place.No + ", ";
                        }
                    }
                    catch { }
                    no = no.Trim();
                    no = no.TrimEnd(',');
                    if (no == "")
                        no = "1";
                    richServer.Document.Replace(range, no);
                }
                catch { }

                range = table1.Rows[2].Cells[7].ContentRange;
                if (gradplan.MaxSize != null && gradplan.MaxSize != "")
                    richServer.Document.Replace(range, gradplan.MaxSize);
                else
                    richServer.Document.Replace(range, "-");


                range = table1.Rows[2].Cells[8].ContentRange;
                if (gradplan.MinSize != null && gradplan.MinSize != "")
                    richServer.Document.Replace(range, gradplan.MinSize);
                else
                    richServer.Document.Replace(range, "-");

                range = table1.Rows[2].Cells[9].ContentRange;
                if (gradplan.ObjectSquare != null && gradplan.ObjectSquare != "")
                    richServer.Document.Replace(range, gradplan.ObjectSquare);
                else
                    richServer.Document.Replace(range, "-");
            }
            catch { }

            // ��������� ���������
            try
            {
                string ConstrParams = "";
                try { ConstrParams = gradplan.TerrZoneKindLink.ConstrParams; }
                catch { }
                Bookmark ConstrParamsBook = richServer.Document.Bookmarks["ConstrParamsBook"];
                richServer.Document.Replace(ConstrParamsBook.Range, ConstrParams);
            }
            catch { }

            #region Old_version
            // ���������� ���������� ������
            //try
            //{
            //    string MaxFloorCount = "";
            //    try { MaxFloorCount = gradplan.MaxFloorCount; }
            //    catch { }
            //    Bookmark MaxFloorCountBook = richServer.Document.Bookmarks["MaxFloorCountBook"];
            //    richServer.Document.Replace(MaxFloorCountBook.Range, MaxFloorCount);
            //}
            //catch { }

            ////���������� ������ ������
            //try
            //{
            //    string MaxHeightCount = "";
            //    try { MaxHeightCount = gradplan.MaxHeightCount; }
            //    catch { }
            //    Bookmark MaxHeightCountBook = richServer.Document.Bookmarks["MaxHeightCountBook"];
            //    richServer.Document.Replace(MaxHeightCountBook.Range, MaxHeightCount);
            //}
            //catch { }

            ////������������ ������� ���������
            //try
            //{
            //    string MaxProcentBuilding = "";
            //    try { MaxProcentBuilding = gradplan.MaxProcentBuilding; }
            //    catch { }
            //    Bookmark MaxProcentBuildingBook = richServer.Document.Bookmarks["MaxProcentBuildingBook"];
            //    richServer.Document.Replace(MaxProcentBuildingBook.Range, MaxProcentBuilding);
            //}
            //catch { }

            ////���� ����������
            //try
            //{
            //    string AnotherItems = "";
            //    try { AnotherItems = gradplan.AnotherItems; }
            //    catch { }
            //    Bookmark AnotherItemsBook = richServer.Document.Bookmarks["AnotherItemsBook"];
            //    richServer.Document.Replace(AnotherItemsBook.Range, AnotherItems);
            //}
            //catch { }
            #endregion
            #endregion

            

            #region // ������ �� �����������_old
            //try
            //{

            //    string PlaceNo = "";
            //    string PlaceName = "";
            //    int i = 0;
            //    Table table = richServer.Document.Tables[8];
            //    //try
            //    //{
            //    // ������� �������� ������ � ������ � ������� (�� ������, ���� ��� ���� ����������� ����������)
            //    range = table.Rows[0].Cells[1].ContentRange;
            //    richServer.Document.Replace(range, "");

            //    range = table.Rows[0].Cells[3].ContentRange;
            //    richServer.Document.Replace(range, "");

            //    foreach (UnderConstructionPlace place in gradplan.underConstructionPlace)
            //    {

            //        //try
            //        //{
            //        if (place.TechSpecifications.Count > 0)
            //        {
            //            PlaceNo = place.No;
            //            PlaceName = place.Name;

            //            // ������ ������ ����� �� �����
            //            if (i == 0)
            //            {
            //                range = table.Rows[0].Cells[1].ContentRange;
            //                richServer.Document.Replace(range, PlaceNo);

            //                range = table.Rows[0].Cells[3].ContentRange;
            //                richServer.Document.Replace(range, PlaceName);

            //                table.Rows.InsertAfter(table.LastRow.Index);
            //                table.MergeCells(table.LastRow.FirstCell, table.LastRow.LastCell);
            //                //table.Rows.Append();
            //                range = table.LastRow.Cells[0].ContentRange;
            //                richServer.Document.Replace(range, "�����������  ������� ����������� ������� � ����� ���������-������������ �����������:");

            //                table.Rows.InsertAfter(table.LastRow.Index);

            //                int j = 0;
            //                foreach(TechSpecifications techSpecifications in place.TechSpecifications)
            //                {
            //                    string text = "";
            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    //range = table.LastRow.Cells[0].ContentRange;
            //                    try { text = techSpecifications.EngineeringSupportType.Name; }
            //                    catch { }


            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, text);

            //                    CharacterProperties cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 12;
            //                    cp.Italic = true;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;
            //                    ParagraphProperties props = richServer.Document.BeginUpdateParagraphs(range);
            //                    props.Alignment = ParagraphAlignment.Center;
            //                    richServer.Document.EndUpdateParagraphs(props);
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Center;


            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    //range = table.LastRow.Cells[0].ContentRange;
            //                    //richServer.Document.Replace(range, "(��� ���������-������������ �����������)");
            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, "(��� ���������-������������ �����������)");
            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 10;
            //                    cp.Italic = false;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Center;


            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    //table.LastRow.Cells.InsertAfter(0);
            //                    //table.LastRow.Cells[0].PreferredWidth = 7;
            //                    //range = table.LastRow.Cells[0].ContentRange;
            //                    //text = "������ ";
            //                    //richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    //range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, text);
            //                    //cp = richServer.Document.BeginUpdateCharacters(range);
            //                    //cp.FontSize = 10;
            //                    //cp.Italic = false;
            //                    //richServer.Document.EndUpdateCharacters(cp);
            //                    //table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Left;


            //                    text = "������ ";
            //                    try { text += techSpecifications.TechSpecificationsDate.ToShortDateString(); }
            //                    catch { }
            //                    try { text += " �� � " + techSpecifications.TechSpecificationsNo; }
            //                    catch { }
            //                    try { text += " " + techSpecifications.TechSpecificationsOrg.Name; }
            //                    catch { }
            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, text);
            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 11;
            //                    cp.Italic = true;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Left;

            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, "(����, ������������ ������ (�����������), ��������� ����������� �������)");
            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 10;
            //                    cp.Italic = false;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Center;
            //                }
            //            }
            //             //��� �����������
            //            if (i > 0)
            //            {
            //                table.Rows.InsertAfter(table.LastRow.Index);
            //                table.Rows.InsertAfter(table.LastRow.Index);
            //                table.Rows.InsertAfter(table.LastRow.Index);

            //                range = table.LastRow.Cells[1].ContentRange;
            //                richServer.Document.Replace(range, PlaceNo);

            //                range = table.LastRow.Cells[3].ContentRange;
            //                richServer.Document.Replace(range, PlaceName);

            //                table.Rows.InsertAfter(table.LastRow.Index);
            //                table.MergeCells(table.LastRow.FirstCell, table.LastRow.LastCell);
            //                //table.Rows.Append();
            //                range = table.LastRow.Cells[0].ContentRange;
            //                richServer.Document.Replace(range, "�����������  ������� ����������� ������� � ����� ���������-������������ �����������:");

            //                table.Rows.InsertAfter(table.LastRow.Index);

            //                int j = 0;
            //                foreach (TechSpecifications techSpecifications in place.TechSpecifications)
            //                {
            //                    string text = "";
            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    try { text = techSpecifications.EngineeringSupportType.Name; }
            //                    catch { }


            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, text);

            //                    CharacterProperties cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 12;
            //                    cp.Italic = true;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;
            //                    ParagraphProperties props = richServer.Document.BeginUpdateParagraphs(range);
            //                    props.Alignment = ParagraphAlignment.Center;
            //                    richServer.Document.EndUpdateParagraphs(props);


            //                    table.Rows.InsertAfter(table.LastRow.Index);

            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, "(��� ���������-������������ �����������)");
            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 10;
            //                    cp.Italic = false;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.None;


            //                    table.Rows.InsertAfter(table.LastRow.Index);

            //                    text = "������ ";
            //                    try { text += techSpecifications.TechSpecificationsDate.ToShortDateString(); }
            //                    catch { }
            //                    try { text += " �� � " + techSpecifications.TechSpecificationsNo; }
            //                    catch { }
            //                    try { text += " " + techSpecifications.TechSpecificationsOrg.Name; }
            //                    catch { }
            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, text);
            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 11;
            //                    cp.Italic = true;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.Thick;
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Left;

            //                    table.Rows.InsertAfter(table.LastRow.Index);
            //                    richServer.Document.Delete(table.LastRow.Cells[0].Range);
            //                    range = richServer.Document.InsertText(table.LastRow.Cells[0].Range.Start, "(����, ������������ ������ (�����������), ��������� ����������� �������)");
            //                    cp = richServer.Document.BeginUpdateCharacters(range);
            //                    cp.FontSize = 10;
            //                    cp.Italic = false;
            //                    richServer.Document.EndUpdateCharacters(cp);
            //                    table.LastRow.Cells[0].Borders.Bottom.LineStyle = TableBorderLineStyle.None;
            //                    //table.LastRow.TableRowAlignment = TableRowAlignment.Center;
            //                }
            //            }
            //            //}
            //            //catch { }
            //            i++;
            //        }
            //    }
            //    //}
            //    //catch { }

            //}
            //catch { }
            #endregion

            #region// ������� ������������ �������������
            try
            {
                string PlaceNo = "";
                string PlaceName = "";
                string InventaryNo = "";
                string PassportDate = "";
                string OrgName = "";
                int i = 0;
                Table table = richServer.Document.Tables[10];
                Table table1 = richServer.Document.Tables[11];
                Table table2 = richServer.Document.Tables[12];

                if (gradplan.GpzuOKS.Count == 0)
                {
                    range = table.Rows[0].Cells[0].ContentRange;
                    richServer.Document.Replace(range, "");

                    range = table.Rows[0].Cells[1].ContentRange;
                    richServer.Document.Replace(range, "�����������");
                }
                //try
                //{
                sort = new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending);
                gradplan.GpzuOKS.Sorting.Add(sort);
                foreach (GpzuOKS gpzuOKS in gradplan.GpzuOKS)
                {
                    try
                    {
                        PlaceNo = gpzuOKS.No;
                    }
                    catch { }
                    try
                    {
                        PlaceName = gpzuOKS.Name;
                    }
                    catch { }
                    try
                    {
                        InventaryNo = gpzuOKS.CapitalStructureBase.techPassport.InventaryNo;
                    }
                    catch { }
                    try { PassportDate = gpzuOKS.CapitalStructureBase.techPassport.PassportDate.ToShortDateString(); }
                    catch { }
                    try { OrgName = gpzuOKS.CapitalStructureBase.techPassport.Org.FullName; }
                    catch { }

                    // ������ ��� �� �����
                    if (i == 0)
                    {
                        range = table.Rows[0].Cells[1].ContentRange;
                        richServer.Document.Replace(range, PlaceNo);

                        range = table.Rows[0].Cells[3].ContentRange;
                        richServer.Document.Replace(range, PlaceName);



                        //range = table.LastRow.Cells[3].ContentRange;
                        //Table table1 = richServer.Document.Tables.Add(range.Start, 1, 2, AutoFitBehaviorType.AutoFitToContents);

                        range = table1.LastRow.Cells[1].ContentRange;
                        richServer.Document.Replace(range, InventaryNo);

                        range = table2.FirstRow.Cells[1].ContentRange;
                        richServer.Document.Replace(range, PassportDate);

                        range = table.Rows[table.LastRow.Index - 1].Cells[0].ContentRange;
                        richServer.Document.Replace(range, OrgName);
                        //table.Rows.InsertAfter(table.LastRow.Index);

                    }
                    // ���� ��� ������ 1 �������� ������� � ��������� ����� ��������
                    else
                    {
                        //table.Rows.InsertAfter(table.LastRow.Index);
                        //table.Rows.InsertAfter(table.LastRow.Index);
                        range = table.Range;
                        DocumentPosition pos = range.End;
                        //pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
                        richServer.Document.InsertText(pos, Characters.LineBreak.ToString());
                        pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
                        richServer.Document.GetText(range);
                        richServer.Document.InsertDocumentContent(pos, range);
                        //richServer.Document.CaretPosition = pos;
                        //richServer.Document.Paste();
                        range = table.Rows[0].Cells[1].ContentRange;
                        richServer.Document.Replace(range, PlaceNo);

                        range = table.Rows[0].Cells[3].ContentRange;
                        richServer.Document.Replace(range, PlaceName);

                        range = table1.LastRow.Cells[1].ContentRange;
                        richServer.Document.Replace(range, InventaryNo);

                        range = table2.FirstRow.Cells[1].ContentRange;
                        richServer.Document.Replace(range, PassportDate);

                        range = table.Rows[table.LastRow.Index - 1].Cells[0].ContentRange;
                        richServer.Document.Replace(range, OrgName);
                        //table.Rows.InsertAfter(table.LastRow.Index);
                    }
                    i++;
                }
            }
            catch { }
            #endregion

            #region// ������� ����������� ��������
            try
            {
                string PlaceNo = "";
                string PlaceName = "";
                string OrgName = "";
                string RegNo = "";
                string RegDate = "";
                int i = 0;
                Table table = richServer.Document.Tables[13];
                Table table1 = richServer.Document.Tables[14];
                if (gradplan.Monuments.Count == 0)
                {
                    range = table.Rows[0].Cells[0].ContentRange;
                    richServer.Document.Replace(range, "");

                    range = table.Rows[0].Cells[1].ContentRange;
                    richServer.Document.Replace(range, "�����������");
                }
                //try
                //{
                sort = new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending);
                gradplan.GpzuOKS.Sorting.Add(sort);
                foreach (GpzuMonument monument in gradplan.Monuments)
                {
                    try
                    {
                        PlaceNo = monument.No;
                    }
                    catch { }
                    try
                    {
                        PlaceName = monument.Name;
                    }
                    catch { }

                    try { OrgName = monument.RegOrg; }
                    catch { }

                    try
                    {
                        RegNo = monument.RegNo;
                    }
                    catch { }

                    try { RegDate = monument.RegDate.ToShortDateString(); }
                    catch { }


                    // ������ ������ �� �����
                    if (i == 0)
                    {
                        range = table.Rows[0].Cells[1].ContentRange;
                        richServer.Document.Replace(range, PlaceNo);

                        range = table.Rows[0].Cells[3].ContentRange;
                        richServer.Document.Replace(range, PlaceName);

                        range = table.Rows[2].Cells[0].ContentRange;
                        richServer.Document.Replace(range, OrgName);


                        range = table1.FirstRow.Cells[1].ContentRange;
                        richServer.Document.Replace(range, RegNo);

                        range = table1.FirstRow.Cells[3].ContentRange;
                        richServer.Document.Replace(range, RegDate);

                        //table.Rows.InsertAfter(table.LastRow.Index);

                    }
                    // ���� �������� ������ 1 �������� ������� � ��������� ����� ��������
                    else
                    {
                        //table.Rows.InsertAfter(table.LastRow.Index);
                        //table.Rows.InsertAfter(table.LastRow.Index);
                        range = table.Range;
                        DocumentPosition pos = range.End;
                        //pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
                        richServer.Document.InsertText(pos, Characters.LineBreak.ToString());
                        pos = richServer.Document.CreatePosition(pos.ToInt() + 1);
                        richServer.Document.GetText(range);
                        richServer.Document.InsertDocumentContent(pos, range);

                        range = table.Rows[0].Cells[1].ContentRange;
                        richServer.Document.Replace(range, PlaceNo);

                        range = table.Rows[0].Cells[3].ContentRange;
                        richServer.Document.Replace(range, PlaceName);

                        range = table.Rows[2].Cells[0].ContentRange;
                        richServer.Document.Replace(range, OrgName);


                        range = table1.FirstRow.Cells[1].ContentRange;
                        richServer.Document.Replace(range, RegNo);

                        range = table1.FirstRow.Cells[3].ContentRange;
                        richServer.Document.Replace(range, RegDate);

                        //table.Rows.InsertAfter(table.LastRow.Index);
                    }
                    i++;
                }
            }
            catch { }
            #endregion



            // ���������� � ���������� ���������� �������
            try
            {
                string LotDivisionInfo = "�������� �����������";
                if (gradplan.LotDivisionInfo != null && gradplan.LotDivisionInfo != "")
                    LotDivisionInfo = gradplan.LotDivisionInfo;
                Bookmark LotDivisionInfoBook = richServer.Document.Bookmarks["LotDivisionInfo"];
                richServer.Document.Replace(LotDivisionInfoBook.Range, LotDivisionInfo);
            }
            catch { }

            #region // ������ �� �����������
            try
            {
                string TechSpecificationsString = "";
                int i = 0;
                //try
                //{
                foreach (TechSpecifications TechSpecification in gradplan.TechSpecifications)
                {
                    i++;
                    TechSpecificationsString += String.Format("{0}. ����������� �������", i.ToString());
                    try{TechSpecificationsString += String.Format(" {0}", TechSpecification.TechSpecificationsOrg.Name);}
                    catch{}
                    try{TechSpecificationsString += String.Format(" �� {0}", TechSpecification.TechSpecificationsDate.ToShortDateString());}
                    catch{}
                    try{TechSpecificationsString += String.Format(" �{0}", TechSpecification.TechSpecificationsNo);}
                    catch{}
                    try{TechSpecificationsString += String.Format(" �� {0}", TechSpecification.EngineeringSupportType.Name);}
                    catch{}
                    TechSpecificationsString += ";" + Environment.NewLine;
                }
                TechSpecificationsString = TechSpecificationsString.TrimEnd(Environment.NewLine.ToCharArray());
                TechSpecificationsString = TechSpecificationsString.TrimEnd(';');
                TechSpecificationsString += ".";
                if(TechSpecificationsString == ".")
                    TechSpecificationsString = "�����������";
                Bookmark TechSpecificationsBook = richServer.Document.Bookmarks["TechSpecificationsBook"];
                richServer.Document.Replace(TechSpecificationsBook.Range, TechSpecificationsString);
            }
            catch { }
            #endregion

            // ���������� � ������� ������ ���� ������������ ���������� �������� ������������ ������������� ��� ��������������� ��� ������������� ����
            try
            {
                string ZonesOKSStateNeedsInfo = "�����������";
                if (gradplan.ZonesOKSStateNeedsInfo != null && gradplan.ZonesOKSStateNeedsInfo != "")
                    ZonesOKSStateNeedsInfo = gradplan.ZonesOKSStateNeedsInfo;
                Bookmark ZonesOKSStateNeedsInfoBook = richServer.Document.Bookmarks["ZonesOKSStateNeedsInfoBook"];
                richServer.Document.Replace(ZonesOKSStateNeedsInfoBook.Range, ZonesOKSStateNeedsInfo);
            }
            catch { }
            // ���� ����������
            try
            {
                string AnotherInfo = "�����������";
                if (gradplan.AnotherInfo != null && gradplan.AnotherInfo != "")
                    AnotherInfo = gradplan.AnotherInfo;
                Bookmark AnotherInfoBook = richServer.Document.Bookmarks["AnotherInfoBook"];
                richServer.Document.Replace(AnotherInfoBook.Range, AnotherInfo);
            }
            catch { }

            // ����������� ����
            try
            {
                string SignerString = "";
                string SignerPositionString = "";
                if (gradplan.Empl != null)
                {
                    try { SignerString = gradplan.signer.name; }
                    catch { }
                    try { SignerPositionString = gradplan.signer.capacity; }
                    catch { }
                }
                Bookmark SignerBook = richServer.Document.Bookmarks["SignerBook"];
                richServer.Document.Replace(SignerBook.Range, SignerString);
                Bookmark SignerPositionBook = richServer.Document.Bookmarks["SignerPositionBook"];
                richServer.Document.Replace(SignerPositionBook.Range, SignerPositionString);
            }
            catch { }

            // �����������
            try
            {
                string EmployeString = "";
                string EmployePositionString = "";
                if (gradplan.Empl != null)
                {
                    try { EmployeString = gradplan.Empl.BriefName; }
                    catch { }
                    try { EmployePositionString = gradplan.Empl.Position; }
                    catch { }
                }
                Bookmark EmployeBook = richServer.Document.Bookmarks["EmployeBook"];
                richServer.Document.Replace(EmployeBook.Range, EmployeString);
                Bookmark EmployePositionBook = richServer.Document.Bookmarks["EmployePositionBook"];
                richServer.Document.Replace(EmployePositionBook.Range, EmployePositionString);
            }
            catch { }
            string path = Path.GetTempPath() + @"\GradPlan_" + gradplan.Oid.ToString() + "_" + DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".docx"; 
            // Save the document file under the specified name.
            richServer.SaveDocument(path, DocumentFormat.OpenXml);

            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }
    }
}
