using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using AISOGD.GPZU;
using AISOGD.SystemDir;
using DevExpress.Xpo;
using AISOGD.DocFlow;
using AISOGD.Land;

namespace WinAisogdIngeo.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class GPZUSubmitController : ViewController
    {
        public GPZUSubmitController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void GPZUSubmitAddAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace(); // Create IObjectSpace or use the existing one, e.g. View.ObjectSpace, if it is suitable for your scenario.
            var x_id = ((BaseObject)e.CurrentObject).Oid;


            GradPlan i_GradPlan;
            GPZUSubmit i_GPZUSubmit;
            i_GradPlan = os.FindObject<GradPlan>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)i_GradPlan.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            if (i_GradPlan.GPZUSubmit != null)
            {
                i_GPZUSubmit = i_GradPlan.GPZUSubmit;
            }
            else
            {
                i_GPZUSubmit = connect.CreateObject<GPZUSubmit>();
            }

            try { i_GPZUSubmit.CityRegion = i_GradPlan.CityRegion; }
            catch { }

            try { i_GPZUSubmit.DocDate = DateTime.Now.Date; }
            catch { }

            try { i_GPZUSubmit.GradPlan = i_GradPlan; }
            catch { }

            try { i_GPZUSubmit.GpzuReason = i_GradPlan.GpzuReason; }
            catch { }

            //try { i_GPZUSubmit.LetterNo = i_GradPlan.LetterNo; }
            //catch { }

            try
            {
                foreach (InLetter letter in i_GradPlan.InLetter)
                {
                    i_GPZUSubmit.InLetter.Add(letter);
                }
            }
            catch { }

            //try { i_GPZUSubmit.LetterSubject = i_GradPlan.Subject; }
            //catch { }

            try { i_GPZUSubmit.LotAddress = i_GradPlan.LotAddress; }
            catch { }

            try { i_GPZUSubmit.LotCadNo = i_GradPlan.LotCadNo; }
            catch { }

            try
            {
                foreach (Lot lot in i_GradPlan.Lots)
                {
                    i_GPZUSubmit.Lots.Add(lot);
                }
            }
            catch { }

            try { i_GPZUSubmit.Municipality = i_GradPlan.Municipality; }
            catch { }
            //try {
                i_GPZUSubmit.DocNo = i_GradPlan.DocNo.Split('-')[1].Trim(); 
        //}
        //    catch { }
            try { i_GradPlan.GPZUSubmit = i_GPZUSubmit; }
            catch { }
            i_GPZUSubmit.Save();
            os.CommitChanges();
            //2. ��������� ���� ���������
            DetailView dv = Application.CreateDetailView(os, i_GPZUSubmit);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
        }
    }
}
