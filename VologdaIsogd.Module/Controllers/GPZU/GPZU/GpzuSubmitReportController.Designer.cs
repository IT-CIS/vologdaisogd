namespace WinAisogdIngeo.Module.Controllers.GPZU
{
    partial class GpzuSubmitReportController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GpzuSubmitReportAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // GpzuSubmitReportAction
            // 
            this.GpzuSubmitReportAction.Caption = "������������";
            this.GpzuSubmitReportAction.ConfirmationMessage = null;
            this.GpzuSubmitReportAction.Id = "GpzuSubmitReportAction";
            this.GpzuSubmitReportAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.GpzuSubmitReportAction.TargetObjectType = typeof(AISOGD.GPZU.GPZUSubmit);
            this.GpzuSubmitReportAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.GpzuSubmitReportAction.ToolTip = null;
            this.GpzuSubmitReportAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.GpzuSubmitReportAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GpzuSubmitReportAction_Execute);
            // 
            // GpzuSubmitReportController
            // 
            this.Actions.Add(this.GpzuSubmitReportAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction GpzuSubmitReportAction;
    }
}
