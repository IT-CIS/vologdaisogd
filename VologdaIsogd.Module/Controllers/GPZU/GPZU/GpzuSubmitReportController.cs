using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using AISOGD.GPZU;
using DevExpress.XtraRichEdit;
using System.Reflection;
using DevExpress.XtraRichEdit.API.Native;
using System.IO;

namespace WinAisogdIngeo.Module.Controllers.GPZU
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class GpzuSubmitReportController : ViewController
    {
        public GpzuSubmitReportController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

       /// <summary>
       /// ������������ ������� ������� �� ����������� ����
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void GpzuSubmitReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ���� � ��� ������ (UnitOfWork)
            View.ObjectSpace.CommitChanges();
            GPZUSubmit gpzuSubmit;
            gpzuSubmit = (GPZUSubmit)e.CurrentObject;

            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = "";

            templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
            LocalPath.Replace("WinAisogdIngeo.Module.DLL", @"\DocTemplates\GPZU\GpzuSubmit.rtf");
            richServer.LoadDocumentTemplate(templatePath);

            DocumentRange range = richServer.Document.CreateRange(0, 0);

            try
            {
                string DocNo = gpzuSubmit.DocNo;
                Bookmark DocNoBook = richServer.Document.Bookmarks["DocNoBook"];
                richServer.Document.Replace(DocNoBook.Range, DocNo);
            }
            catch { }

            try
            {
                string DocDate = gpzuSubmit.DocDate.ToShortDateString();
                Bookmark DocDateBook = richServer.Document.Bookmarks["DocDateBook"];
                richServer.Document.Replace(DocDateBook.Range, DocDate);
            }
            catch { }

            try
            {
                string LotCadNo = gpzuSubmit.LotCadNo;
                Bookmark LotCadNoBook = richServer.Document.Bookmarks["LotCadNoBook"];
                richServer.Document.Replace(LotCadNoBook.Range, LotCadNo);
            }
            catch { }

            try
            {
                string GpzuReason = gpzuSubmit.GpzuReason;
                Bookmark GpzuReasonBook = richServer.Document.Bookmarks["GpzuReasonBook"];
                richServer.Document.Replace(GpzuReasonBook.Range, GpzuReason);
            }
            catch { }


            try
            {
                string LotAddress = gpzuSubmit.LotAddress;
                Bookmark LotAddressBook = richServer.Document.Bookmarks["LotAddressBook"];
                richServer.Document.Replace(LotAddressBook.Range, LotAddress);
            }
            catch { }

            try
            {
                string GradPlanNo = gpzuSubmit.GradPlanNo;
                Bookmark GradPlanNoBook = richServer.Document.Bookmarks["GradPlanNoBook"];
                richServer.Document.Replace(GradPlanNoBook.Range, GradPlanNo);
            }
            catch { }

            try
            {
                string LotCadNo = gpzuSubmit.LotCadNo;
                Bookmark LotCadNoBook = richServer.Document.Bookmarks["LotCadNoBook1"];
                richServer.Document.Replace(LotCadNoBook.Range, LotCadNo);
            }
            catch { }

            try
            {
                string LotAddress = gpzuSubmit.LotAddress;
                Bookmark LotAddressBook = richServer.Document.Bookmarks["LotAddressBook1"];
                richServer.Document.Replace(LotAddressBook.Range, LotAddress);
            }
            catch { }


            string path = Path.GetTempPath() + @"\GpzuSubmit_" + gpzuSubmit.Oid.ToString() + "_" + DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".docx";
            
            richServer.SaveDocument(path, DocumentFormat.OpenXml);
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch { }
        }
    }
}
