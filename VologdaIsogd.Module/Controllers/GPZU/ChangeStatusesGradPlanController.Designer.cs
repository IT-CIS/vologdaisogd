﻿namespace VologdaIsogd.Module.Controllers.GPZU
{
    partial class ChangeStatusesGradPlanController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GradPlanSubmitAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.GradPlanRefuseAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // GradPlanSubmitAction
            // 
            this.GradPlanSubmitAction.Caption = "Утвержен";
            this.GradPlanSubmitAction.ConfirmationMessage = null;
            this.GradPlanSubmitAction.Id = "GradPlanSubmitAction";
            this.GradPlanSubmitAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.GradPlanSubmitAction.ToolTip = null;
            this.GradPlanSubmitAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GradPlanSubmitAction_Execute);
            // 
            // GradPlanRefuseAction
            // 
            this.GradPlanRefuseAction.Caption = "Отказ";
            this.GradPlanRefuseAction.ConfirmationMessage = null;
            this.GradPlanRefuseAction.Id = "GradPlanRefuseAction";
            this.GradPlanRefuseAction.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.GradPlanRefuseAction.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.GradPlanRefuseAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.GradPlanRefuseAction.ToolTip = null;
            this.GradPlanRefuseAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.GradPlanRefuseAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GradPlanRefuseAction_Execute);
            // 
            // ChangeStatusesGradPlanController
            // 
            this.Actions.Add(this.GradPlanSubmitAction);
            this.Actions.Add(this.GradPlanRefuseAction);
            this.TargetObjectType = typeof(AISOGD.GPZU.GradPlan);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction GradPlanSubmitAction;
        private DevExpress.ExpressApp.Actions.SimpleAction GradPlanRefuseAction;
    }
}
