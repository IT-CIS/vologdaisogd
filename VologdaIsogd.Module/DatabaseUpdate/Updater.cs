﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using AISOGD.Isogd;
using AISOGD.SystemDir;
using AISOGD.DocFlow;
using AISOGD.Constr;
using AISOGD.GPZU;

namespace VologdaIsogd.Module.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            //string name = "MyName";
            //DomainObject1 theObject = ObjectSpace.FindObject<DomainObject1>(CriteriaOperator.Parse("Name=?", name));
            //if(theObject == null) {
            //    theObject = ObjectSpace.CreateObject<DomainObject1>();
            //    theObject.Name = name;
            //}
			CreateDefaultRole();
            // способ доставки документа ИСОГД
            string[] isogdReceivingMethod = { "1;Почтовая служба",
                                      "2;Курьерская почта",
                                      "3;В руки",
                                      "4;Инвентаризация архива"
                                    };
            foreach (string irm in isogdReceivingMethod)
                fillIsogdReceivingMethod(irm);

            // список ГИС слоев + классы для конфига
            string[] gisLayers = { "Геодезические_изыскания;AISOGD.Surveys.Survey",
                                      "АРХИВ_Геодезические_изыскания;AISOGD.Surveys.Survey",
                                      "Инженерно_геологические_изыскания;AISOGD.Surveys.Survey",
                                      "КНИГИ_ХРАНЕНИЯ_ИСОГД;AISOGD.Isogd.IsogdStorageBook",
                                      "КНИГИ_ХРАН_ППТ;AISOGD.Isogd.IsogdStorageBook",
                                      "ДЗУ_ИСОГД;AISOGD.Isogd.IsogdPhysStorageBook",
                                      "ГПЗУ;AISOGD.GPZU.GradPlan",
                                      "ПЗЗ;AISOGD.Reglament.TerrZone",
                                      "Прибрежная_защитная_полоса;AISOGD.Reglament.WaterProtectionZone",
                                      "водоохранные_зоны;AISOGD.Reglament.WaterProtectionZone",
                                      "ЗОНЫ_АНТЕНН;AISOGD.Reglament.AntennZone",
                                      "Зоны_арт_скважин;AISOGD.Reglament.ArtWellZone",
                                      "Зоны_охраны_ОКН;AISOGD.Reglament.MonumentZone",
                                      "СЗЗ;AISOGD.Reglament.SZZone",
                                      "Строящиеся_объекты;AISOGD.Constr.ConstrStage",
                                      "Земельные_участки_КП;AISOGD.Land.Parcel"
                                    };
            foreach (string l in gisLayers)
                fillGISLayers(l);

            // список групп гис слоев
            string[] gisLayerGroups = {"ППТ для стройки; КНИГИ_ХРАН_ППТ"
                                    };
            foreach (string l in gisLayerGroups)
                fillGISLayerGroups(l);

            // список видов документов
            string[] docKindName = { "Градостроительный план земельного участка", "Приказ об утверждении градостроительного плана",
                                       "Разрешение на строительство", "Разрешение на ввод объекта в эксплуатацию",
                                       "Уведомление о строительстве объекта индивидуального жилищного строительства",
                                       "Уведомление о вводе объекта индивидуального жилищного строительства",
                                       //"Уведомление о переводе (об отказе) нежилого помещения в жилое",
                                       //"Акт приемочной комиссии о завершении переустройства и (или) перепланировки жилого помещения",
                                       //"Распоряжение о присвоении (изменении) адреса объекту адресации",
                                       //"Распоряжение об аннулировании адреса объекту адресации"
                                       };
            foreach (string dk in docKindName)
                SetDocKind(dk);
            
            // список видов заявок
            string[] letterThemeName = { "Заявка на выдачу градоплана земельного участка (обр.гр. ДГ)",
                                        "Заявка на присвоение (изменение) адреса объекту (обр.гр, ДГ)",
                                        "Заявка о внесении измен. в разреш. на строит (обр.гр., ДГ)",
                                        "Заявка на продл. срока действ. разр.на строит (обр.гр., ДГ)",
                                        "Заявка на разрешение на строительство (обр. граждан, ДГ)",
                                        "Заявка на разрешение на ввод в эксплуат (обр. граждан, ДГ)",
                                        "Заявка вх. на выдачу градоплана земельного участка (ДГ)",
                                        "Заявка вх. на разрешение на строительство (ДГ)",
                                        "Заявка вх. о внесении изменений на разреш. на строит-во (ДГ)",
                                        "Заявка вх. на разрешение на ввод в эксплуатац. объекта (ДГ)",
                                        "Заявка вх. на продл.срока действия разреш. на строительсво (ДГ)",
                                        "Заявление на прекращение делопроизводства по разрешению на строительство",
                                        "Уведомление о планируемом строит./реконструц.объекта (обр.гр,ДГ)",
                                        "Уведомление о постр./реконстр. постр. объекте (обр.гр,ДГ)"
                                         };
            foreach (string ltn in letterThemeName)
                SetLetterTheme(ltn);


            //// список Степень огнестойкости
            //string[] constrFireShield = { "I степень огнестойкости", "II степень огнестойкости",
            //                            "III степень огнестойкости",
            //                            "IV степень огнестойкости",
            //                            "V степень огнестойкости"//,
            //                            //"Заявление на подготовку распоряжения о присвоении (изменении) адреса",
            //                            //"Заявление на подготовку распоряжения об аннулировании адреса"
            //                             };
            //foreach (string ltn in constrFireShield)
            //    SetLetterTheme(ltn);

            //// список Класс конструктивной пожарной опасности
            //string[] constrFireDangerClass = { "С0", "С1",
            //                            "С2",
            //                            "С3"
            //                            //"V степень огнестойкости"//,
            //                            //"Заявление на подготовку распоряжения о присвоении (изменении) адреса",
            //                            //"Заявление на подготовку распоряжения об аннулировании адреса"
            //                             };
            //foreach (string ltn in constrFireShield)
            //    SetLetterTheme(ltn);


            // справочник материалов
            string[] materials = { "1;кирпичные",
                                      "3;каменные",
                                      "4;деревянные",
                                      "7;панельные",
                                    "9;блочные",
                                    "10;монолитные",
                                    "5; другой материал"
            };
            foreach (string m in materials)
                FillMaterials(m);

            // настройки ГПЗУ
            GPZUHelper GPZUHelper = ObjectSpace.FindObject<GPZUHelper>(new BinaryOperator("Name", "Соответствие зон ГПЗУ"));
            if (GPZUHelper == null)
            {
                GPZUHelper = ObjectSpace.CreateObject<GPZUHelper>();
                GPZUHelper.Name = "Соответствие зон ГПЗУ";
                GPZUHelper.Save();
            }
            ObjectSpace.CommitChanges();
        }

        private void fillIsogdReceivingMethod(string codename)
        {
            string code = codename.Split(';')[0];
            string name = codename.Split(';')[1];
            dIsogdReceivingMethod IsogdReceivingMethod = ObjectSpace.FindObject<dIsogdReceivingMethod>(new BinaryOperator("Code", code));
            if (IsogdReceivingMethod == null)
            {
                IsogdReceivingMethod = ObjectSpace.CreateObject<dIsogdReceivingMethod>();
                IsogdReceivingMethod.Code = code;
                IsogdReceivingMethod.Name = name;
            }
            IsogdReceivingMethod.Save();
            ObjectSpace.CommitChanges();
            IsogdReceivingMethod = null;
        }
        private void fillGISLayers(string layerNames)
        {
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            string layerName = layerNames.Split(';')[0];
            string className = layerNames.Split(';')[1];
            GisLayer Layer = ObjectSpace.FindObject<GisLayer>(new BinaryOperator("LayerId", layerName));
            if (Layer == null)
            {
                Layer = ObjectSpace.CreateObject<GisLayer>();
                Layer.LayerId = layerName;
            }
            Layer.Save();
            ObjectSpace.CommitChanges();
            if (className != null && className != "")
            {
                SpatialConfig config = connect.FindFirstObject<SpatialConfig>(x => x.IsogdClassName == connect.GetClassInfo(className).ClassType);
                if (config == null)
                {
                    config = connect.CreateObject<SpatialConfig>();
                    config.IsogdClassName = connect.GetClassInfo(className).ClassType;// connect.FindFirstObject<Type>(x=> x.FullName == className);
                }
                SpatialLayerItem item = connect.FindFirstObject<SpatialLayerItem>(x => x.SpatialConfig == config && x.SpatialLayer == Layer);
                if (item == null)
                {
                    item = connect.CreateObject<SpatialLayerItem>();
                    item.SpatialLayer = Layer;
                    config.Layers.Add(item);
                    item.Save();
                }
                config.Save();
                ObjectSpace.CommitChanges();
                config = null;
            }
            Layer = null;
        }
        private void fillGISLayerGroups(string layerGroupInfo)
        {
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            string layerGroupName = layerGroupInfo.Split(';')[0];
            string layerNames = layerGroupInfo.Split(';')[1];
            GisLayerGroup LayerGroup = ObjectSpace.FindObject<GisLayerGroup>(new BinaryOperator("Name", layerGroupName));
            if (LayerGroup == null)
            {
                LayerGroup = ObjectSpace.CreateObject<GisLayerGroup>();
                LayerGroup.Name = layerGroupName;
            }
            LayerGroup.Save();
            ObjectSpace.CommitChanges();
            if (layerNames != null && layerNames != "")
            {
                foreach (string layerName in layerNames.Split(','))
                {
                    GisLayer Layer = ObjectSpace.FindObject<GisLayer>(new BinaryOperator("LayerId", layerName));
                    if (Layer == null)
                    {
                        Layer = ObjectSpace.CreateObject<GisLayer>();
                        Layer.LayerId = layerName;
                    }
                    Layer.Save();
                    SpatialLayerItem item = connect.FindFirstObject<SpatialLayerItem>(x => x.GisLayerGroup == LayerGroup && x.SpatialLayer == Layer);
                    if (item == null)
                    {
                        item = connect.CreateObject<SpatialLayerItem>();
                        item.SpatialLayer = Layer;
                        LayerGroup.Layers.Add(item);
                        item.Save();
                    }
                    ObjectSpace.CommitChanges();
                    Layer = null;
                    item = null;
                }
            }
            LayerGroup = null;
        }
        private void SetDocKind(string name)
        {
            dDocKind docKind = ObjectSpace.FindObject<dDocKind>(new BinaryOperator("Name", name));
            if (docKind == null)
            {
                docKind = ObjectSpace.CreateObject<dDocKind>();
                docKind.Name = name;
            }
            docKind.Save();
            ObjectSpace.CommitChanges();
            docKind = null;
        }
        private void SetLetterTheme(string name)
        {
            dLetterTheme letterTheme = ObjectSpace.FindObject<dLetterTheme>(new BinaryOperator("Name", name));
            if (letterTheme == null)
            {
                letterTheme = ObjectSpace.CreateObject<dLetterTheme>();
                letterTheme.Name = name;
            }
            letterTheme.Save();
            ObjectSpace.CommitChanges();
            letterTheme = null;
        }
        private void FillMaterials(string material)
        {
            string code = material.Split(';')[0];
            string name = material.Split(';')[1];
            Material Material = ObjectSpace.FindObject<Material>(new BinaryOperator("Code", code));
            if (Material == null)
            {
                Material = ObjectSpace.CreateObject<Material>();
                Material.Code = code;
                Material.Name = name;
            }
            Material.Save();
            ObjectSpace.CommitChanges();
            Material = null;
        }
        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }
        private PermissionPolicyRole CreateDefaultRole() {
            PermissionPolicyRole defaultRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Default"));
            if(defaultRole == null) {
                defaultRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                defaultRole.Name = "Default";

				defaultRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.ReadOnlyAccess, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
				defaultRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
				defaultRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<PermissionPolicyRole>(SecurityOperations.Read, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
				defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.Create, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.Create, SecurityPermissionState.Allow);                
            }
            return defaultRole;
        }
    }
}
