﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;
using AISOGD.SystemDir;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.ExpressApp.Utils;
using DevExpress.Xpo.Metadata;
using DevExpress.ExpressApp.Model;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Reflection;
using DevExpress.XtraGrid;

namespace LinkedObjects
{
    public partial class LinkedObjectsForm : DevExpress.XtraEditors.XtraForm
    {

        public DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit addButton;
        public DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit editButton;
        private List<GridListItem> gridContents = new List<GridListItem>();
        private List<string> IsogdClassNames;
        private string mapObjectID;
        private Connect connect;
        public GridView gv;

        public LinkedObjectsForm(string aClasses, string aMapObjectID, Connect aCоnnect)
        {
            InitializeComponent();
            IsogdClassNames = aClasses.Split(',').ToList();
            mapObjectID = aMapObjectID;
            connect = aCоnnect;
            Init();
            FillData();
            
            ClassGridView.CustomRowCellEdit += ClassGridView_CustomRowCellEdit;
            ClassGridView.OptionsView.ShowGroupPanel = false;
            ClassGridView.OptionsView.ColumnAutoWidth = true;
            ClassGridView.OptionsView.ShowColumnHeaders = false;
            ClassGridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
        }


        GridViewInfo GetViewInfo(GridView view)
        {
            PropertyInfo pi = view.GetType().GetProperty("ViewInfo", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
            return pi.GetValue(view, null) as GridViewInfo;
        }

        int GetRowHeight(GridView view, int rowHandle)
        {
            GridViewInfo viewInfo = GetViewInfo(view);
            return viewInfo.CalcRowHeight(CreateGraphics(), rowHandle, 0, 0);
        }

        int GetRowsHeight(GridView view)
        {
            int height = 0;
            for (int i = 0; i < view.RowCount; i++)
            {
                int rowHandle = view.GetRowHandle(i);
                height += GetRowHeight(view, rowHandle);
            }
            return height + 2;
        }

        void ClassGridView_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "Show")
            {
                gv = sender as GridView;
                string AddFlag = gv.GetRowCellValue(e.RowHandle, gv.Columns["isExist"]).ToString();
                switch (AddFlag)
                {
                    //case "False":
                    //    editButton.Buttons[0]..Enabled = false;
                    //    e.RepositoryItem = editButton;
                    //    break;
                    case "True":
                        editButton.Buttons[0].Enabled = true;
                        e.RepositoryItem = editButton;
                        break;
                }
            }

            if (e.Column.FieldName == "Create")
            {
                e.RepositoryItem = addButton;
            }
        }


        public void Init()
        {
            this.ClassGridView.OptionsView.ColumnAutoWidth = true;
            DevExpress.XtraGrid.Columns.GridColumn colHeader;
            DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textEdit;
            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit checkEdit;

            this.ClassGridView.OptionsMenu.EnableColumnMenu = false;
            this.ClassGridView.OptionsMenu.EnableColumnMenu = false;
            this.ClassGridView.OptionsMenu.EnableFooterMenu = false;

            colHeader = this.ClassGridView.Columns.Add();
            colHeader.Name = "ClassName";
            colHeader.Caption = "Наименование";
            colHeader.FieldName = "ClassName";
            textEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            colHeader.ColumnEdit = textEdit;
            colHeader.Width = 200;
            colHeader.Visible = true;
            colHeader.VisibleIndex = this.ClassGridView.Columns.Count;
            colHeader.Visible = false;

            colHeader = this.ClassGridView.Columns.Add();
            colHeader.Name = "ClassCaption";
            colHeader.Caption = "Описание";
            colHeader.FieldName = "ClassCaption";
            textEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            colHeader.ColumnEdit = textEdit;
            colHeader.Width = 200;
            colHeader.Visible = true;
            colHeader.VisibleIndex = this.ClassGridView.Columns.Count;
            colHeader.Visible = true;

            colHeader = this.ClassGridView.Columns.Add();
            colHeader.Name = "isExist";
            colHeader.Caption = "Флаг создания";
            colHeader.FieldName = "isExist";
            checkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            colHeader.ColumnEdit = checkEdit;
            colHeader.Visible = false;

            colHeader = this.ClassGridView.Columns.Add();
            colHeader.Name = "Show";
            colHeader.Caption = "Показать";
            colHeader.FieldName = "Show";
            colHeader.Visible = true;

            colHeader = this.ClassGridView.Columns.Add();
            colHeader.Name = "Create";
            colHeader.Caption = "Создать";
            colHeader.FieldName = "Create";
            colHeader.Visible = true;

            addButton = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            addButton.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            addButton.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
            addButton.Buttons[0].Caption = "Создать";
            addButton.Buttons[0].Width = colHeader.Width - 2;
            addButton.Buttons[0].Enabled = true;

            editButton = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            editButton.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            editButton.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
            editButton.Buttons[0].Caption = "Показать";
            editButton.Buttons[0].Width = colHeader.Width - 2;
            editButton.Buttons[0].Enabled = true;

            colHeader.Visible = true;
            colHeader.VisibleIndex = this.ClassGridView.Columns.Count;
            colHeader.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.ClassGridControl.RepositoryItems.Add(addButton);
            this.ClassGridControl.RepositoryItems.Add(editButton);
        }


        private void FillData()
        {
            var spatialRepository = connect.FindObjects<SpatialRepository>
                (mc => mc.SpatialObjectId == mapObjectID);

            foreach (string xafClass in IsogdClassNames)
            {
               // XtraMessageBox.Show(connect.GetClassCaption(xafClass));
                var rep = spatialRepository.Where(x => x.IsogdClassName == xafClass);

                if (rep.Count() == 0)
                {
                    this.gridContents.Add(new GridListItem(xafClass, false, connect.GetClassCaption(xafClass)));

                }
                if (rep.Count() >= 1)
                {
                    this.gridContents.Add(new GridListItem(xafClass, true, connect.GetClassCaption(xafClass)));
                }

            }
            this.ClassGridControl.DataSource = this.gridContents;
            this.ClassGridView.RefreshData();
            ClassGridControl.Height = GetRowsHeight(ClassGridView);
            ClientSize = ClassGridControl.Size;
        }

        public class GridListItem
        {
            public GridListItem()
            { }
            public GridListItem(string aClassName, bool aIsExist, string aClassCaption)
            {
                ClassName = aClassName;
                isExist = aIsExist;
                ClassCaption = aClassCaption;
            }
            public string ClassName { get; set; }
            public string ClassCaption { get; set; }
            public bool isExist { get; set; }

            public string ActionButton { get; set; }
        }


        private void ClassGridView_MouseWheel(object sender, MouseEventArgs e)
        {
            (e as DevExpress.Utils.DXMouseEventArgs).Handled = true;
        }
    }

}