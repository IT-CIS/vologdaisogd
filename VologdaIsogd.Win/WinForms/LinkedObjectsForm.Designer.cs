﻿namespace LinkedObjects
{
    partial class LinkedObjectsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkedObjectsForm));
            this.ClassGridControl = new DevExpress.XtraGrid.GridControl();
            this.ClassGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.ClassGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClassGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ClassGridControl
            // 
            this.ClassGridControl.Location = new System.Drawing.Point(-1, -3);
            this.ClassGridControl.MainView = this.ClassGridView;
            this.ClassGridControl.Name = "ClassGridControl";
            this.ClassGridControl.Size = new System.Drawing.Size(434, 114);
            this.ClassGridControl.TabIndex = 1;
            this.ClassGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ClassGridView});
            // 
            // ClassGridView
            // 
            this.ClassGridView.GridControl = this.ClassGridControl;
            this.ClassGridView.Name = "ClassGridView";
            this.ClassGridView.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.ClassGridView_MouseWheel);
            // 
            // LinkedObjectsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(436, 112);
            this.Controls.Add(this.ClassGridControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LinkedObjectsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Реестровые данные";
            ((System.ComponentModel.ISupportInitialize)(this.ClassGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClassGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl ClassGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView ClassGridView;
    }
}