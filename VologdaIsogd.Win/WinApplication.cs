﻿using System;
using System.ComponentModel;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Win;
using System.Collections.Generic;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using AISOGD.SystemDir;
using XDMessaging;
using System.Windows.Forms;
using HelperClassLibrary;
using System.Web.Script.Serialization;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Editors;
using LinkedObjects;
using AISOGD.Perm;
using AISOGD.Constr;
using AISOGD.Land;
using VologdaIsogd.Controllers.Reglament;
using VologdaIsogd.Module.Controllers.Perm;
using AISOGD.GPZU;
using VologdaIsogd.Module.Controllers.GPZU;
using PipeInteraction;
using WinAisogdMapInfo.Module.SystemDir.Spatial.Application.MI;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading.Tasks;

namespace VologdaIsogd.Win {

    public delegate void MessageEvent(string aMessage);

    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/DevExpressExpressAppWinWinApplicationMembersTopicAll.aspx
    public partial class VologdaIsogdWindowsFormsApplication : WinApplication {
        public VologdaIsogdWindowsFormsApplication() {
            InitializeComponent();
            Settings.Create();
            LinkNewObjectToParentImmediately = true;

            //XDClient xdClient = new XDClient(XDClientVoid);
            //xdClient();

            int port = 52370;
            AsyncSocketService service = new AsyncSocketService(port);
            Task.Factory.StartNew(service.Run);
            service.MessageEventHandler += Service_MessageEventHandler;

            pipeServer = new PipeServer();
            pipeServer.PipeMessage += new DelegateMessage(PipesMessageHandler);
            pipeServer.Listen("RsiIsogdLink");
        }


        private void Service_MessageEventHandler(string aMessage)
        {
            objectSpace = CreateObjectSpace();
            //XtraMessageBox.Show("Получено " + aMessage);
            if (!string.IsNullOrWhiteSpace(aMessage))
            {
                if (aMessage.IndexOf("&") != -1)
                {
                    var inputArr = aMessage.Split('&');

                    if (inputArr.Length == 2)
                    {

                        string layer = inputArr[0];
                        string mapObjId = inputArr[1].Replace(".0", "");
                        
                        if (layer.Length > 0 && mapObjId != "0")
                        {
                            objectSpace = CreateObjectSpace();
                            connect = Connect.FromObjectSpace(objectSpace);
                            spatial = new SpatialController<GisLayer>(connect);
                            //Получаем набор классов, которые можно связать с данной таблицей МапИнфо
                            List<string> IsogdClassNames = spatial.GetListLinkedIsogdClasses(layer);
                            //XtraMessageBox.Show("IsogdClassNames.Count  " + IsogdClassNames.Count.ToString());
                            if (IsogdClassNames.Count == 0)
                            {
                                XtraMessageBox.Show(@"Для ГИС объекта данного слоя нет настроек связи с реестровыми объектами! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }

                            Type xafObjectType = null;
                            //XtraMessageBox.Show("spatialRepository.IsogdClassName: " + spatialRepository.IsogdClassName);
                            foreach (var type in objectSpace.TypesInfo.PersistentTypes.Where(type => type.FullName == IsogdClassNames[0]))
                            {
                                xafObjectType = type.Type;
                            }
                            if (xafObjectType == null)
                            {
                                XtraMessageBox.Show("Класс не найден", "Ошибка");
                                return;
                            }

                            //TODO сделать для множественных связей
                            var spatialRepository = connect.FindFirstObject<SpatialRepository>
                                (mc => mc.SpatialObjectId == mapObjId && mc.IsogdClassName == IsogdClassNames[0]);

                            //XtraMessageBox.Show("xafObjectType: " + xafObjectType.FullName);
                            object xafObject = null;
                            if (xafObjectType != null)
                            {
                                xafObject = objectSpace.FindObject(xafObjectType, new BinaryOperator("Oid", spatialRepository.IsogdObjectId));
                            }
                            if (xafObject == null)
                            {
                                XtraMessageBox.Show("Объект не найден или удален");
                                return;
                            }
                            //XtraMessageBox.Show("xafObject: " + xafObject.ToString());
                            DetailView detailView = CreateDetailView(objectSpace, xafObject);
                            detailView.ViewEditMode = ViewEditMode.Edit;
                            var svp = new ShowViewParameters
                            {
                                CreateAllControllers = true,
                                CreatedView = detailView,
                                Context = TemplateContext.View,
                                TargetWindow = TargetWindow.NewWindow
                            };
                            if (frm == null)
                            {
                                frm = (Form)this.MainWindow.Template;
                            }
                            if (ctlInvoke == null)
                            {
                                foreach (Control ctrl in frm.Controls)
                                {
                                    ctlInvoke = ctrl;
                                    break;
                                }
                            }
                            showViewDelegate = ShowView;
                            //Invoke на любом контроле основной формы
                            ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
                        }
                        else
                        {
                            XtraMessageBox.Show("Объект не найден");
                            return;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show("Объект не найден");
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show("Объект не найден");
                    return;
                }
            }
            else
            {
                XtraMessageBox.Show("Объект не найден");
                return;
            }
        }

        #region Связь с приложением АИСОГД из прикладной ГИС через Pipe
        public delegate void NewMessageDelegate(string NewMessage);
        private PipeServer pipeServer;
        private void PipesMessageHandler(string message)
        {
            //try
            //{
                frm = (Form)this.MainWindow.Template;
                if (ctlInvoke == null)
                {
                    foreach (Control ctrl in frm.Controls)
                    {
                        ctlInvoke = ctrl;
                        break;
                    }
                }
                showViewDelegate = ShowView;
                if (frm.InvokeRequired)
                {
                    frm.Invoke(new NewMessageDelegate(PipesMessageHandler), message);
                }
                else
                {
                    string mess = message;
                    mess = mess.Replace("\0", "");
                    m_MapInfo = new MapInfoApplication();
                    if (m_MapInfo.mapinfo == null)
                    {
                        XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                        return;
                    }
                    objectSpace = CreateObjectSpace();
                    connect = Connect.FromObjectSpace(objectSpace);
                    spatial = new SpatialController<GisLayer>(connect);
                    // синхронизация террзон
                    if (mess == "CheckTerrZones")
                    {
                        CheckTerrZonesController CheckTerrZonesController = new CheckTerrZonesController();
                        CheckTerrZonesController.CheckTerrZonesLinks(connect);
                        return;
                    }
                    if (mess == "delGpzuObjectsToForm")
                    {
                        DelGpzuObjectsToForm();
                        return;
                    }

                    

                    // получаем количество выделенных объектов МапИнфо
                    int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                    if (selObjCount == 0)
                    {
                        XtraMessageBox.Show(
                            "Не выбрано ни одного пространственного объекта.", "Инфо");
                        return;
                    }
                    
                    // 2. Получаем таблицу (слой) выделения
                    layer = m_MapInfo.Eval("SelectionInfo(1)");
                    //XtraMessageBox.Show("layer: " + layer);

                    // 3. Получаем набор классов, которые можно связать с данной таблицей МапИнфо
                    List<string> IsogdClassNames = spatial.GetListLinkedIsogdClasses(layer);
                    if (IsogdClassNames.Count == 0)
                    {
                        XtraMessageBox.Show(@"Для ГИС объекта данного слоя нет настроек связи с реестровыми объектами! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                        return;
                    }

                    DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
                    var xtraMessageBoxForm = new XtraMessageBoxForm(); ;
                    // Если выделено больше одного пространственного объекта на карте - выводим сообщение об этом
                    if (selObjCount > 1)
                    {
                        //xtraMessageBoxForm = new XtraMessageBoxForm();
                        if (
                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                String.Format("Выделено {0} объектов таблицы {1}. Продолжить?",
                                    selObjCount.ToString(), layer),
                                "Внимание!", dialogResults, null, 0)) == DialogResult.No)
                        { return; }
                    }
                    #region Открываем или создаем реестровый объект
                    if (mess == "default")
                    {
                        // получаем класс реестрового объекта
                        if (IsogdClassNames.Count == 1)
                        {
                            className = IsogdClassNames[0];
                        }
                        if (IsogdClassNames.Count > 1)
                        {

                            string classes = string.Join(",", IsogdClassNames.ToArray());
                            m_MapInfo.Do("Fetch Rec 1 From Selection");
                            string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                            linkedObjectsForm = new LinkedObjectsForm(classes, gisObjId, connect)
                            {
                                StartPosition = FormStartPosition.CenterParent,
                            };
                            linkedObjectsForm.addButton.ButtonClick += addButton_ButtonClick;
                            linkedObjectsForm.editButton.ButtonClick += editButton_ButtonClick;
                            linkedObjectsForm.ShowDialog();
                        }
                        mapObjInfos = new List<string>();
                        for (int j = 1; j < selObjCount + 1; j++)
                        {
                            m_MapInfo.Do("Fetch Rec " + j.ToString() + " From Selection");
                            //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                            // Id (MI_PRINX) выделенного объекта мапинфо
                            try
                            {
                                string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                                if (gisObjId != "0")
                                {
                                    mapObjInfos.Add(gisObjId);
                                }
                                else
                                {
                                    XtraMessageBox.Show("Имеются не сохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                                    return;
                                }
                            }
                            catch { }
                        }
                        //// XtraMessageBox.Show(className, "Инфо");
                        int i = 0;
                        // Проверяем, есть ли у нас уже связи у выделенных ГИС объектов
                        // если связь есть - добавляем +1 в i
                        foreach (string gisObjId in mapObjInfos)
                        {

                            var spatialRepository = connect.FindFirstObject<SpatialRepository>
                                (mc => mc.SpatialObjectId == gisObjId && mc.IsogdClassName == className);
                            if (spatialRepository != null)
                            {
                                i++;
                            }
                        }
                        // если связи были
                        if (i > 0)
                        {
                            // если связей столько же, сколько количество выделенных объектов - пробуем их все открыть
                            if (i == mapObjInfos.Count)
                            {
                                foreach (string gisObjId in mapObjInfos)
                                {
                                    var spatialRepository =
                                   connect.FindFirstObject<SpatialRepository>(mc => mc.SpatialObjectId ==
                                       gisObjId && mc.IsogdClassName == className);
                                    Type xafObjectType = null;
                                    foreach (var type in objectSpace.TypesInfo.PersistentTypes.Where(type => type.FullName == className))
                                    {
                                        xafObjectType = type.Type;
                                    }
                                    if (xafObjectType == null)
                                    {
                                        XtraMessageBox.Show("Класс не найден", "Ошибка");
                                        return;
                                    }
                                    object xafObject = null;
                                    if (xafObjectType != null)
                                    {
                                        xafObject =
                                            objectSpace.FindObject(xafObjectType, new BinaryOperator("Oid",
                                                spatialRepository.IsogdObjectId));
                                    }
                                    if (xafObject == null)
                                    {
                                        XtraMessageBox.Show("Объект не найден или удален", "Инфо");
                                        return;
                                    }
                                    DetailView detailView = CreateDetailView(objectSpace, xafObject);
                                    detailView.ViewEditMode = ViewEditMode.Edit;
                                    var svp = new ShowViewParameters
                                    {
                                        CreateAllControllers = true,
                                        CreatedView = detailView,
                                        Context = TemplateContext.View,
                                        TargetWindow = TargetWindow.NewWindow//.NewModalWindow
                                    };

                                    //Invoke на любом контроле основной формы
                                    ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
                                }
                            }
                            // иначе - сообщаем, что у части есть связь, у части - нет
                            else
                            {
                                XtraMessageBox.Show(@"У части выделенных ГИС объектов есть связанные реестровые, а у части нет! 
                            Пожалуйста, сделайте другую выборку или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                        }
                        else
                        {
                            CreateXAFObjectByLayerID();
                        }

                        //XtraMessageBox.Show(spatiallayerId);
                        //XtraMessageBox.Show(spatialobjectId);
                    }
                    #endregion
                    else
                    {
                        //                        XtraMessageBox.Show(@"Данная версия ИСОГД не позволяет сделать запрашиваемую Вами операцию. 
                        //В текущей версии доступна только связь реестровых объектов!", "Ошибка.");
                        //                        return;
                        // создаем по выделенным объектам разрешение на строительство и этапы строительства
                        if (mess == "createConstrPermDoc")
                        {
                            // Проверям, что мы выделяем объекты слоя строящихся объектов
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.Constr.ConstrStage").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя строящихся объектов! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CreateConstrPermDoc();
                        }
                        if (mess == "createUsePermDoc")
                        {
                            // Проверям, что мы выделяем объекты слоя строящихся объектов
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.Constr.ConstrStage").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя строящихся объектов! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CreateUsePermDoc();
                        }
                        if (mess == "createNoticeIZDPermDoc")
                        {
                            // Проверям, что мы выделяем объекты слоя строящихся объектов
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.Constr.CapitalStructureBase").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя строящихся объектов! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            if (selObjCount > 1)
                            {
                                XtraMessageBox.Show(@"Уведомление на строительство ИЖД можно создать только по одному объекту! 
                            Пожалуйста, выделите только один объект слоя 'Строящиеся объекты' или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CreateNoticeIZDDoc();
                        }
                        if (mess == "createUseNoticeIZDPermDoc")
                        {
                            // Проверям, что мы выделяем объекты слоя строящихся объектов
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.Constr.CapitalStructureBase").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя строящихся объектов! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            if (selObjCount > 1)
                            {
                                XtraMessageBox.Show(@"Уведомление на строительство ИЖД можно создать только по одному объекту! 
                            Пожалуйста, выделите только один объект слоя 'Строящиеся объекты' или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CreateUseNoticeIZDDoc();
                        }
                        if (mess == "createGradPlanDoc")
                        {

                            // Проверям, что мы выделяем объекты слоя ЗУ
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.Land.Parcel").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя земельных участков! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            if (selObjCount > 1)
                            {
                                XtraMessageBox.Show(@"ГПЗУ можно создать только по одному земельному участку! 
                            Пожалуйста, выделите только один объект участка или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CreateGradPlanDoc();
                        }
                        if (mess == "copyGpzuObjectsToForm")
                        {

                            // Проверям, что мы выделяем объекты слоя ГПЗУ
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.GPZU.GradPlan").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя ГПЗУ! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            if (selObjCount > 1)
                            {
                                XtraMessageBox.Show(@"Необходимо выделить только один объект! 
                            Пожалуйста, выделите только один объект участка или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CopyGpzuObjectsToForm();
                        }
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    XtraMessageBox.Show(String.Format($"Ошибка связи с АИСОГД - {ex.Message}"));
            //    //Debug.WriteLine(ex.Message);
            //}

        }

        #endregion

        #region Связь с приложением АИСОГД из прикладной ГИС (в нашем случае - MapInfo)  
        XDMessagingClient client;
        private delegate void XDClient();
        private delegate void ShowViewDelegate(ShowViewParameters svp);

        void XDClientVoid()
        {
            client = new XDMessaging.XDMessagingClient();
            XDMessaging.IXDListener listener;
            listener = client.Listeners.GetListenerForMode(XDMessaging.XDTransportMode.Compatibility);
            listener.RegisterChannel("XAfAisogdCommands");
            listener.MessageReceived += listener_MessageReceived;
        }

        Form frm = null;
        ShowViewDelegate showViewDelegate = null;
        private Control ctlInvoke;
        IObjectSpace objectSpace;
        Connect connect;
        string aInput;
        //string spatiallayerId;
        //string spatialobjectId;
        //string classData;
        string className;
        bool existFlag;
        string layer;
        List<string> mapObjInfos = new List<string>();
        SpatialController<GisLayer> spatial;
        TransportIngeoHelper tranportHelper = new TransportIngeoHelper();
        private MapInfoApplication m_MapInfo = null;
        LinkedObjectsForm linkedObjectsForm;

        void listener_MessageReceived(object o, XDMessageEventArgs e)
        {
            if (e.DataGram.Channel == "XAfAisogdCommands")
            {
                if (!string.IsNullOrEmpty(e.DataGram.Message))
                {
                    frm = (Form)this.MainWindow.Template;

                    if (ctlInvoke == null)
                    {
                        foreach (Control ctrl in frm.Controls)
                        {
                            ctlInvoke = ctrl;
                            break;
                        }
                    }
                    showViewDelegate = ShowView;

                    // мессадж приходит в виде:
                    // aInput (информация, что делать с переданной информацией: default - просто связка или отображение объекта);
                    // ClassName; 
                    // Лист выделенных пространственых объектов: List[GlobalLayerId, LocalObjectId]
                    objectSpace = CreateObjectSpace();
                    connect = Connect.FromObjectSpace(objectSpace);
                    spatial = new SpatialController<GisLayer>(connect);
                    string mess = e.DataGram.Message;

                    m_MapInfo = new MapInfoApplication();
                    if (m_MapInfo.mapinfo == null)
                    {
                        XtraMessageBox.Show("MapInfo не запущено", "Инфо");
                        return;
                    }
                    // синхронизация террзон
                    if(mess == "CheckTerrZones")
                    {
                        CheckTerrZonesController CheckTerrZonesController = new CheckTerrZonesController();
                        CheckTerrZonesController.CheckTerrZonesLinks(connect);
                        return;
                    }
                    if (mess == "delGpzuObjectsToForm")
                    {
                        DelGpzuObjectsToForm();
                        return;
                    }
                    // получаем количество выделенных объектов МапИнфо
                    int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
                    if (selObjCount == 0)
                    {
                        XtraMessageBox.Show(
                            "Не выбрано ни одного пространственного объекта.", "Инфо");
                        return;
                    }

                    // 2. Получаем таблицу (слой) выделения
                    layer = m_MapInfo.Eval("SelectionInfo(1)");
                    //XtraMessageBox.Show("layer: " + layer);

                    // 3. Получаем набор классов, которые можно связать с данной таблицей МапИнфо
                    List<string> IsogdClassNames;
                    IsogdClassNames = spatial.GetListLinkedIsogdClasses(layer);
                    if (IsogdClassNames.Count == 0)
                    {
                        XtraMessageBox.Show(@"Для ГИС объекта данного слоя нет настроек связи с реестровыми объектами! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                        return;
                    }

                    DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
                    var xtraMessageBoxForm = new XtraMessageBoxForm(); ;
                    // Если выделено больше одного пространственного объекта на карте - выводим сообщение об этом
                    if (selObjCount > 1)
                    {
                        //xtraMessageBoxForm = new XtraMessageBoxForm();
                        if (
                            xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                String.Format("Выделено {0} объектов таблицы {1}. Продолжить?",
                                    selObjCount.ToString(), layer),
                                "Внимание!", dialogResults, null, 0)) == DialogResult.No)
                        { return; }
                    }


                    #region Открываем или создаем реестровый объект
                    if (mess == "default")
                    {
                        // получаем класс реестрового объекта
                        if (IsogdClassNames.Count == 1)
                        {
                            className = IsogdClassNames[0];
                        }
                        if (IsogdClassNames.Count > 1)
                        {

                            string classes = string.Join(",", IsogdClassNames.ToArray());
                            m_MapInfo.Do("Fetch Rec 1 From Selection");
                            string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                            linkedObjectsForm = new LinkedObjectsForm(classes, gisObjId, connect)
                            {
                                StartPosition = FormStartPosition.CenterParent,
                            };
                            linkedObjectsForm.addButton.ButtonClick += addButton_ButtonClick;
                            linkedObjectsForm.editButton.ButtonClick += editButton_ButtonClick;
                            linkedObjectsForm.ShowDialog();
                        }
                        mapObjInfos = new List<string>();
                        for (int j = 1; j < selObjCount + 1; j++)
                        {
                            m_MapInfo.Do("Fetch Rec " + j.ToString() + " From Selection");
                            //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
                            // Id (MI_PRINX) выделенного объекта мапинфо
                            try
                            {
                                string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
                                if (gisObjId != "0")
                                {
                                    mapObjInfos.Add(gisObjId);
                                }
                                else
                                {
                                    XtraMessageBox.Show("Имеются не сохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
                                    return;
                                }
                            }
                            catch { }
                        }
                        //// XtraMessageBox.Show(className, "Инфо");
                        int i = 0;
                        // Проверяем, есть ли у нас уже связи у выделенных ГИС объектов
                        // если связь есть - добавляем +1 в i
                        foreach (string gisObjId in mapObjInfos)
                        {

                            var spatialRepository = connect.FindFirstObject<SpatialRepository>
                                (mc => mc.SpatialObjectId == gisObjId && mc.IsogdClassName == className);
                            if (spatialRepository != null)
                            {
                                i++;
                            }
                        }
                        // если связи были
                        if (i > 0)
                        {
                            // если связей столько же, сколько количество выделенных объектов - пробуем их все открыть
                            if (i == mapObjInfos.Count)
                            {
                                foreach (string gisObjId in mapObjInfos)
                                {
                                    var spatialRepository =
                                   connect.FindFirstObject<SpatialRepository>(mc => mc.SpatialObjectId ==
                                       gisObjId && mc.IsogdClassName == className);
                                    Type xafObjectType = null;
                                    foreach (var type in objectSpace.TypesInfo.PersistentTypes.Where(type => type.FullName == className))
                                    {
                                        xafObjectType = type.Type;
                                    }
                                    if (xafObjectType == null)
                                    {
                                        XtraMessageBox.Show("Класс не найден", "Ошибка");
                                        return;
                                    }
                                    object xafObject = null;
                                    if (xafObjectType != null)
                                    {
                                        xafObject =
                                            objectSpace.FindObject(xafObjectType, new BinaryOperator("Oid",
                                                spatialRepository.IsogdObjectId));
                                    }
                                    if (xafObject == null)
                                    {
                                        XtraMessageBox.Show("Объект не найден или удален", "Инфо");
                                        return;
                                    }
                                    DetailView detailView = CreateDetailView(objectSpace, xafObject);
                                    detailView.ViewEditMode = ViewEditMode.Edit;
                                    var svp = new ShowViewParameters
                                    {
                                        CreateAllControllers = true,
                                        CreatedView = detailView,
                                        Context = TemplateContext.View,
                                        TargetWindow = TargetWindow.NewWindow//.NewModalWindow
                                    };

                                    //Invoke на любом контроле основной формы
                                    ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
                                }
                            }
                            // иначе - сообщаем, что у части есть связь, у части - нет
                            else
                            {
                                XtraMessageBox.Show(@"У части выделенных ГИС объектов есть связанные реестровые, а у части нет! 
                            Пожалуйста, сделайте другую выборку или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                        }
                        else
                        {
                            CreateXAFObjectByLayerID();
                        }

                        //XtraMessageBox.Show(spatiallayerId);
                        //XtraMessageBox.Show(spatialobjectId);
                    }
                    #endregion
                    else
                    {
                        // создаем по выделенным объектам разрешение на строительство и этапы строительства
                        if (mess == "createConstrPermDoc")
                        {
                            CreateConstrPermDoc();
                        }
                        if (mess == "createUsePermDoc")
                        {
                            CreateUsePermDoc();
                        }
                        if(mess == "createGradPlanDoc")
                        {
                            
                            // Проверям, что мы выделяем объекты слоя ЗУ
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.Land.Parcel").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя земельных участков! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            if (selObjCount > 1)
                            {
                                XtraMessageBox.Show(@"ГПЗУ можно создать только по одному земельному участку! 
                            Пожалуйста, выделите только один объект участка или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CreateGradPlanDoc();
                        }
                        if (mess == "copyGpzuObjectsToForm")
                        {

                            // Проверям, что мы выделяем объекты слоя ГПЗУ
                            if (!spatial.GetListLinkedSpatialLayers("AISOGD.GPZU.GradPlan").Contains(layer))
                            {
                                XtraMessageBox.Show(@"Необходимо выделить ГИС объект слоя ГПЗУ! 
                            Пожалуйста, обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            if (selObjCount > 1)
                            {
                                XtraMessageBox.Show(@"Необходимо выделить только один объект! 
                            Пожалуйста, выделите только один объект участка или обратитесь к администратору Системы!", "Ошибка.");
                                return;
                            }
                            CopyGpzuObjectsToForm();
                        }
                        
                    }
                }
            }
        }

        // необходимо добавить различные проверки - 
        // на существование разрешения
        // на существование этапов
        void CreateConstrPermDoc()
        {
            ConstrPerm constrPerm = null;
            CreatePermDocController CreatePermDocController = new CreatePermDocController();
            constrPerm = CreatePermDocController.CreateConstrPermDoc(connect);

            if (constrPerm != null)
            {
                DetailView detailView = CreateDetailView(objectSpace, constrPerm);
                detailView.ViewEditMode = ViewEditMode.Edit;
                var svp = new ShowViewParameters
                {
                    CreateAllControllers = true,
                    CreatedView = detailView,
                    Context = TemplateContext.View,
                    TargetWindow = TargetWindow.NewWindow
                };

                //Invoke на любом контроле основной формы
                ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            }
            #region // логика перенесена в контроллер
            //if (layer != "" && mapObjInfos != null)
            //{
            //    mapObjInfos = new List<string>();
            //    int selObjCount = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
            //    for (int j = 1; j < selObjCount + 1; j++)
            //    {
            //        m_MapInfo.Do("Fetch Rec " + j.ToString() + " From Selection");
            //        //XtraMessageBox.Show(m_MapInfo.Eval("Selection.MI_PRINX"));
            //        // Id (MI_PRINX) выделенного объекта мапинфо
            //        try
            //        {
            //            string gisObjId = m_MapInfo.Eval("Selection.MI_PRINX");
            //            if (gisObjId != "0")
            //            {
            //                mapObjInfos.Add(gisObjId);
            //            }
            //            else
            //            {
            //                XtraMessageBox.Show("Имеются не сохраненные ГИС данные. Сохраните таблицу ГИС, и попробуйте заново!", "Ошибка");
            //                return;
            //            }
            //        }
            //        catch { }
            //    }
            //    if (mapObjInfos.Count > 0)
            //    {
            //        ConstrPerm constrPerm = connect.CreateObject<ConstrPerm>();
            //        constrPerm.DocDate = DateTime.Now.Date;
            //        constrPerm.PermKind = AISOGD.Enums.ePermKind.Новое;
            //        for (int i = 1; i < mapObjInfos.Count + 1; i++)
            //        {
            //            ConstrStage constrStage = connect.CreateObject<ConstrStage>();

            //            constrStage.ConstrStageState = AISOGD.Enums.eConstrStageState.Стройка;
            //            m_MapInfo.Do("Fetch Rec " + i.ToString() + " From Selection");
            //            try
            //            {
            //                constrStage.MapNo = m_MapInfo.Eval("Selection.НОМЕР_НА_СХЕМЕ");
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.StageNo = m_MapInfo.Eval("Selection.ОЧЕРЕДЬ_СТР");
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.Name = m_MapInfo.Eval("Selection.ОПИСАНИЕ");
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.ConstrAddress = m_MapInfo.Eval("Selection.СТРОИТ_АДРЕС");
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.Address = m_MapInfo.Eval("Selection.МЕСТОПОЛОЖЕНИЕ") + ", " + m_MapInfo.Eval("Selection.ПРОЕКТ_НОМЕР_ДОМА");
            //            }
            //            catch { }
            //            try
            //            {
            //                 dConstructionType constructionType = connect.FindFirstObject<dConstructionType>(mc=> 
            //                mc.Name == m_MapInfo.Eval("Selection.КОД_ОБЪЕКТ"));
            //                if (constructionType == null)
            //                {
            //                    constructionType = connect.CreateObject<dConstructionType>();
            //                    constructionType.Name = m_MapInfo.Eval("Selection.КОД_ОБЪЕКТ");
            //                    constructionType.Save();
            //                }
            //                constrStage.ConstructionType = constructionType;
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.FloorCount = m_MapInfo.Eval("Selection.МАКС_ЭТАЖН");
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.FloorCountDiff = m_MapInfo.Eval("Selection.РАЗНОЭТАЖНОСТЬ");
            //            }
            //            catch { }
            //            try
            //            {
            //                constrStage.FlatCount = Convert.ToInt16(m_MapInfo.Eval("Selection.КОЛ_КВАРТИР"));
            //            }
            //            catch { }
            //            try
            //            {
            //                Material constrMaterial = connect.FindFirstObject<Material>(mc =>
            //                mc.Name == m_MapInfo.Eval("Selection.СТР_МАТЕРИАЛ_СТЕН"));
            //                if (constrMaterial == null)
            //                {
            //                    constrMaterial = connect.CreateObject<Material>();
            //                    constrMaterial.Name = m_MapInfo.Eval("Selection.СТР_МАТЕРИАЛ_СТЕН");
            //                    constrMaterial.Save();
            //                }
            //                constrStage.ConstrMaterial = constrMaterial;
            //            }
            //            catch { }
            //            try
            //            {
            //                string wkind = m_MapInfo.Eval("Selection.ВИД_РАБОТ");
            //                switch (wkind.ToLower())
            //                {
            //                    case "стр":
            //                        constrStage.WorkKind = AISOGD.Enums.eWorkKind.Строительство;
            //                        break;
            //                    case "строительство":
            //                        constrStage.WorkKind = AISOGD.Enums.eWorkKind.Строительство;
            //                        break;
            //                    default:
            //                        constrStage.WorkKind = AISOGD.Enums.eWorkKind.Строительство;
            //                        break;
            //                }
            //            }
            //            catch { }
            //            constrStage.Save();
            //            spatial.AddSpatialLink(constrStage.ClassInfo.FullName, constrStage.Oid.ToString(), layer, mapObjInfos[i-1]);
            //            // делаем пространственные запросы по ЗУ
            //            //try
            //            //{
            //            //    m_MapInfo.Do("Select * From " + layer + " Where MI_PRINX = " + mapObjInfos[i] + " Into zObject");
            //            //    // получаем слои для ЗУ
            //            //    List<string> parcelLayersIds = spatial.GetListLinkedSpatialLayers("AISOGD.Land.Parcel");
            //            //    foreach (string parcelLayer in parcelLayersIds)
            //            //    {
            //            //        // ищем пересечение с нашим объектом
            //            //        m_MapInfo.Do("Select MI_PRINX from " + parcelLayer + " Where obj Partly Within (Select obj from zObject) AND MI_PRINX <> 0 Into tmpTable");
            //            //        int selObjCount1 = Convert.ToInt16(m_MapInfo.Eval("SelectionInfo(3)"));
            //            //        for (int j = 1; j < selObjCount1 + 1; j++)
            //            //        {
            //            //            m_MapInfo.Do("Fetch Rec " + j.ToString() + " From tmpTable");
            //            //            // Id(MI_PRINX) пространственного объекта ЗУ
            //            //            string parcelReqestObjID = m_MapInfo.Eval("tmpTable.Col1");
            //            //            // находим ЗУ
            //            //            //Получаем связанные реестровые объекты документов ЗУ, смотрим, есть ли уже такие у
            //            //            // этапа, если нет - добавляем
            //            //            // из таблицы связи получаем Ид реестрового объекта ЗУ
            //            //            SpatialRepository objLink = connect.FindFirstObject<SpatialRepository>(mc =>
            //            //                mc.SpatialLayerId == layer &&
            //            //                mc.SpatialObjectId == parcelReqestObjID &&
            //            //                mc.IsogdClassName == "AISOGD.Land.Parcel");
            //            //            if (objLink != null)
            //            //            {
            //            //                // по ИД находим сам реестровый объект документа ИСОГД
            //            //                Parcel parcel = connect.FindFirstObject<Parcel>(mc=> mc.Oid.ToString() == objLink.IsogdObjectId);
            //            //                if (parcel != null)
            //            //                {
            //            //                    bool isParcelExist = false;

            //            //                    foreach (Parcel p in constrStage.Parcels)
            //            //                    {
            //            //                        if (parcel == p)
            //            //                            isParcelExist = true;
            //            //                    }
            //            //                    if (!isParcelExist)
            //            //                    {
            //            //                        constrStage.Parcels.Add(parcel);
            //            //                    }
            //            //                }
            //            //            }
            //            //        }
            //            //        }
            //            //    }
            //            //catch { }
            //            constrPerm.ConstrStages.Add(constrStage);
            //            constrPerm.Save();
            //            constrStage.Save();
            //            connect.GetUnitOfWork().CommitChanges();
            //        }
            //        if (constrPerm.ConstrStages.Count > 0)
            //        {
            //            try
            //            {
            //                constrPerm.WorkKind = constrPerm.ConstrStages[0].WorkKind;
            //            }
            //            catch { }
            //            foreach (ConstrStage constrStage in constrPerm.ConstrStages)
            //            {
            //                try
            //                {
            //                    constrPerm.ObjectName += constrStage.Name + Environment.NewLine;
            //                }
            //                catch { }
            //                try
            //                {
            //                    foreach (Parcel p in constrStage.Parcels)
            //                    {
            //                        constrPerm.Parcels.Add(p);
            //                        if (p.CadastralNumber != null && p.CadastralNumber != "")
            //                            constrPerm.LotCadNo += p.CadastralNumber + "; ";
            //                    }
            //                }
            //                catch { }
            //            }
            //            try
            //            {
            //                constrPerm.LotCadNo.Trim().TrimEnd(';');
            //            }
            //            catch { }
            //        }
            //        constrPerm.Save();
            //        connect.GetUnitOfWork().CommitChanges();
            //        DetailView detailView = CreateDetailView(objectSpace, constrPerm);
            //        detailView.ViewEditMode = ViewEditMode.Edit;
            //        var svp = new ShowViewParameters
            //        {
            //            CreateAllControllers = true,
            //            CreatedView = detailView,
            //            Context = TemplateContext.View,
            //            TargetWindow = TargetWindow.NewWindow
            //        };

            //        //Invoke на любом контроле основной формы
            //        ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            //    } 
            //}
            #endregion
        }
        void CreateUsePermDoc()
        {
            UsePerm usePerm = null;
            CreateUsePermDocController CreatePermDocController = new CreateUsePermDocController();
            usePerm = CreatePermDocController.CreateUsePermDoc(connect);

            if (usePerm != null)
            {
                DetailView detailView = CreateDetailView(objectSpace, usePerm);
                detailView.ViewEditMode = ViewEditMode.Edit;
                var svp = new ShowViewParameters
                {
                    CreateAllControllers = true,
                    CreatedView = detailView,
                    Context = TemplateContext.View,
                    TargetWindow = TargetWindow.NewWindow
                };

                //Invoke на любом контроле основной формы
                ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            }
        }
        void CreateNoticeIZDDoc()
        {
            PermNoticeIZD perm = null;
           PermNoticeIZDCreateController CreatePermDocController = new PermNoticeIZDCreateController();
            perm = CreatePermDocController.CreatePermNoticeIZD(connect);

            if (perm != null)
            {
                DetailView detailView = CreateDetailView(objectSpace, perm);
                detailView.ViewEditMode = ViewEditMode.Edit;
                var svp = new ShowViewParameters
                {
                    CreateAllControllers = true,
                    CreatedView = detailView,
                    Context = TemplateContext.View,
                    TargetWindow = TargetWindow.NewWindow
                };

                //Invoke на любом контроле основной формы
                ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            }
        }

        void CreateUseNoticeIZDDoc()
        {
            UsePermNoticeIZD perm = null;
            UsePermNoticeIZDCreateController CreatePermDocController = new UsePermNoticeIZDCreateController();
            perm = CreatePermDocController.CreateUsePermNoticeIZD(connect);

            if (perm != null)
            {
                DetailView detailView = CreateDetailView(objectSpace, perm);
                detailView.ViewEditMode = ViewEditMode.Edit;
                var svp = new ShowViewParameters
                {
                    CreateAllControllers = true,
                    CreatedView = detailView,
                    Context = TemplateContext.View,
                    TargetWindow = TargetWindow.NewWindow
                };

                //Invoke на любом контроле основной формы
                ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            }
        }

        void CreateGradPlanDoc()
        {
            GradPlan gradPlan = null;
            CreateGPZUController CreateGPZUController = new CreateGPZUController();
            gradPlan = CreateGPZUController.CreateGradPlanDoc(connect);

            if (gradPlan != null)
            {
                DetailView detailView = CreateDetailView(objectSpace, gradPlan);
                detailView.ViewEditMode = ViewEditMode.Edit;
                var svp = new ShowViewParameters
                {
                    CreateAllControllers = true,
                    CreatedView = detailView,
                    Context = TemplateContext.View,
                    TargetWindow = TargetWindow.NewWindow
                };

                //Invoke на любом контроле основной формы
                ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            }
        }
        void CopyGpzuObjectsToForm()
        {
            CopyGPZUObjectsForFormController CopyGPZUObjectsForFormController = new CopyGPZUObjectsForFormController();
            CopyGPZUObjectsForFormController.CopyGPZUObjects(objectSpace);
        }
        // удалить все темп объекты из слоев для подготовки схемы для печати
        void DelGpzuObjectsToForm()
        {
            CopyGPZUObjectsForFormController CopyGPZUObjectsForFormController = new CopyGPZUObjectsForFormController();
            CopyGPZUObjectsForFormController.DelGPZUTempObjects();
        }
        void editButton_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //string AddFlag = gv.GetRowCellValue(e.Button., gv.Columns["AddFlag"]).ToString();
            className = linkedObjectsForm.gv.GetRowCellValue(linkedObjectsForm.gv.FocusedRowHandle, "ClassName").ToString();
            existFlag = true;
            linkedObjectsForm.Close();
        }


        void addButton_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //string AddFlag = gv.GetRowCellValue(e.Button., gv.Columns["AddFlag"]).ToString();
            className = linkedObjectsForm.gv.GetRowCellValue(linkedObjectsForm.gv.FocusedRowHandle, "ClassName").ToString();
            existFlag = false;
            linkedObjectsForm.Close();
        }

        public void CreateXAFObjectByLayerID()
        {
            if (layer != "" && mapObjInfos != null)
            {
                spatial = new SpatialController<GisLayer>(connect);

                var newObj = connect.CreateObject(className);
                connect.SaveObject(newObj);
                connect.GetUnitOfWork().CommitChanges();
                var id = newObj.ClassInfo.KeyProperty.GetValue(newObj);
                if (mapObjInfos.Count > 0)
                {
                    foreach (string gisObjId in mapObjInfos)
                    {
                        if(gisObjId!= "" && gisObjId != "0")
                            spatial.AddSpatialLink(className, id.ToString(), layer, gisObjId);
                    }
                }
                DetailView detailView = CreateDetailView(objectSpace, newObj);
                detailView.ViewEditMode = ViewEditMode.Edit;
                var svp = new ShowViewParameters
                {
                    CreateAllControllers = true,
                    CreatedView = detailView,
                    Context = TemplateContext.View,
                    TargetWindow = TargetWindow.NewWindow
                };
                //Invoke на любом контроле основной формы
                ctlInvoke.Invoke(showViewDelegate, new object[] { svp });
            }
        }

        private void ShowView(ShowViewParameters svp)
        {
            frm.Activate();
            ShowViewStrategy.ShowView(svp, new ShowViewSource(null, null));
        }
        #endregion

        protected override void CreateDefaultObjectSpaceProvider(CreateCustomObjectSpaceProviderEventArgs args) {
            args.ObjectSpaceProviders.Add(new SecuredObjectSpaceProvider((SecurityStrategyComplex)Security, args.ConnectionString, args.Connection, false));
            args.ObjectSpaceProviders.Add(new NonPersistentObjectSpaceProvider(TypesInfo, null));
        }
        private void VologdaIsogdWindowsFormsApplication_CustomizeLanguagesList(object sender, CustomizeLanguagesListEventArgs e) {
            string userLanguageName = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
            if(userLanguageName != "en-US" && e.Languages.IndexOf(userLanguageName) == -1) {
                e.Languages.Add(userLanguageName);
            }
        }
        private void VologdaIsogdWindowsFormsApplication_DatabaseVersionMismatch(object sender, DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs e) {
#if EASYTEST
            e.Updater.Update();
            e.Handled = true;
#else
            //if(System.Diagnostics.Debugger.IsAttached) {
                e.Updater.Update();
                e.Handled = true;
            //}
    //        else {
				//string message = "The application cannot connect to the specified database, " +
				//	"because the database doesn't exist, its version is older " +
				//	"than that of the application or its schema does not match " +
				//	"the ORM data model structure. To avoid this error, use one " +
				//	"of the solutions from the https://www.devexpress.com/kb=T367835 KB Article.";

				//if(e.CompatibilityError != null && e.CompatibilityError.Exception != null) {
				//	message += "\r\n\r\nInner exception: " + e.CompatibilityError.Exception.Message;
				//}
				//throw new InvalidOperationException(message);
    //        }
#endif
        }
    }


    public class AsyncSocketService
    {
        public event MessageEvent MessageEventHandler = null;

        public AsyncSocketService(int port)
        {
            this._port = port;
        }
        private int _port;
        public async void Run()
        {
            IPAddress add = IPAddress.Parse("127.0.0.1");
            TcpListener listener = new TcpListener(add, _port);
            listener.Start();
            // Console.WriteLine("port " + this.port);
            //Console.WriteLine("Hit <enter> to stop service\n");
            while (true)
            {
                try
                {
                    TcpClient tcpClient = await listener.AcceptTcpClientAsync();
                    Task t = Process(tcpClient);
                    await t;
                }
                catch (Exception ex)
                {
                    //  Console.WriteLine(ex.Message);
                }
            }
        }
        private async Task Process(TcpClient tcpClient)
        {
            string clientEndPoint =
              tcpClient.Client.RemoteEndPoint.ToString();
            //Console.WriteLine("Received connection request from "
            //  + clientEndPoint);
            try
            {
                NetworkStream networkStream = tcpClient.GetStream();
                StreamReader reader = new StreamReader(networkStream);
                StreamWriter writer = new StreamWriter(networkStream);
                writer.AutoFlush = true;
                while (true)
                {
                    string request = await reader.ReadLineAsync();
                    if (request != null)
                    {
                        //Console.WriteLine("Received service request: " + request);
                        string response = Response(request);
                        // Console.WriteLine("Computed response is: " + response + "\n");
                        await writer.WriteLineAsync(response);
                        if (MessageEventHandler != null)
                            MessageEventHandler.Invoke(request);
                    }
                    else
                        break; // Client closed connection
                }
                tcpClient.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (tcpClient.Connected)
                    tcpClient.Close();
            }
        }
        private static string Response(string request)
        {
            return "Ok";//response;
        }
    }
}
