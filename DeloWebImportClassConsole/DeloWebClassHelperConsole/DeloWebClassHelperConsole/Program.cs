﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DeloWebClassHelperConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // объект для сериализации
            InLetterInfo inLetterInfo = new InLetterInfo();
            inLetterInfo.InLetterNo = "Номер входящего обращения";
            inLetterInfo.InLetterDate = DateTime.Now.Date;
            inLetterInfo.InLetterType = "Тип обращения (Заявление на подготовку разрешения на строительство)";
            inLetterInfo.DeloRKLink = @"ссылка на РК обращения из Дело-Веб (http://delo/RK_id_test)";
            inLetterInfo.DeloRKId = "Id регистрационной карточки заявки из СЭД Дело";
            inLetterInfo.AnswerKind = "Отказ(если отказ)";
            

            InLetterSubject inLetterSubject = new InLetterSubject();
            inLetterSubject.Name = "Заявитель1";
            inLetterSubject.Address = "Заявитель1 адрес";
            inLetterSubject.Phone = "Заявитель1 телефон";
            inLetterSubject.Email = "Заявитель1 емаил";
            inLetterInfo.InLetterSubjects.Add(inLetterSubject);

            inLetterSubject = new InLetterSubject();
            inLetterSubject.Name = "Заявитель2";
            inLetterSubject.Address = "Заявитель2 адрес";
            inLetterSubject.Phone = "Заявитель2 телефон";
            inLetterSubject.Email = "Заявитель2 емаил";
            inLetterInfo.InLetterSubjects.Add(inLetterSubject);

            DocumentFile documentFile = new DocumentFile();
            documentFile.Name = "Отказ или утвержденный документ)";
            documentFile.File = File.ReadAllBytes(@"d:\19143660-19143699.doc");
            inLetterInfo.DocumentFiles.Add(documentFile);

            Console.WriteLine("Объект создан");

            // передаем в конструктор тип класса
            XmlSerializer formatter = new XmlSerializer(typeof(InLetterInfo));

            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream(@"d:\InLetterInfo.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, inLetterInfo);

                Console.WriteLine("Объект сериализован");
            }

            DocumentInfo documentInfo = new DocumentInfo();
            documentInfo.DocumentNo = "Номер документа для размещения в ИСОГД";
            documentInfo.DocumentDate = DateTime.Now.Date;
            documentInfo.DocumentType = "Тип(класс) документа для размещения в ИСОГД";
            documentInfo.DocumentName = "Наименование документа";
            documentInfo.DocumentDeveloper = "Разработчик документа";
            documentInfo.DocumentStatementOrg = "Утвердитель документа";

            documentFile = new DocumentFile();
            documentFile.Name = "Наименование файла1";
            documentFile.File = File.ReadAllBytes(@"d:\InLetterInfo.xml");
            documentInfo.DocumentFiles.Add(documentFile);

            documentFile = new DocumentFile();
            documentFile.Name = "Наименование файла2";
            documentFile.File = File.ReadAllBytes(@"d:\InLetterInfo.xml");
            documentInfo.DocumentFiles.Add(documentFile);

            // передаем в конструктор тип класса
            XmlSerializer formatter2 = new XmlSerializer(typeof(DocumentInfo));
            using (FileStream fs2 = new FileStream(@"d:\DocumentInfo.xml", FileMode.OpenOrCreate))
            {
                formatter2.Serialize(fs2, documentInfo);

                Console.WriteLine("Объект сериализован");
            }

            ISOGDMessage ISOGDMessage = new ISOGDMessage();
            ISOGDMessage.Message = "Заявка с № Номер от Дата взята на обработку Сотрудником.";
            // передаем в конструктор тип класса
            XmlSerializer formatter3 = new XmlSerializer(typeof(ISOGDMessage));
            using (FileStream fs3 = new FileStream(@"d:\MessageInfo.xml", FileMode.OpenOrCreate))
            {
                formatter3.Serialize(fs3, ISOGDMessage);

                Console.WriteLine("Объект сериализован");
            }
        }
    }

    [Serializable]
    public class InLetterInfo
    {
        //public Guid InLetterID;
        //public string Version;
        public string InLetterNo;
        public DateTime InLetterDate;
        public string InLetterType;
        public List<InLetterSubject> InLetterSubjects = new List<InLetterSubject>();
        public List<DocumentFile> DocumentFiles = new List<DocumentFile>();
        public string DeloRKId;
        public string AnswerKind;
        public string DeloRKLink;
    }
    [Serializable]
    public class InLetterSubject
    {
        public string Name;
        public string Address;
        public string Phone;
        public string Email;
    }

    //    2. xml для Документов для регистрации в ИСОГД(и для проставления реквизитов в документах подготовки)
    //- номер документа
    //- дата документа
    //- тип документа(класс)
    //- разработчик документа
    //- утвердитель докмента
    //- ссылка на саму копию документа(на файл, должен быть также в папке)
    [Serializable]
    public class DocumentInfo
    {
        public string DocumentNo;
        public DateTime DocumentDate;
        public string DocumentType;
        public string DocumentName;
        public string DocumentDeveloper;
        public string DocumentStatementOrg;
        public List<DocumentFile> DocumentFiles = new List<DocumentFile>();
    }
    [Serializable]
    public class DocumentFile
    {
        public string Name;
        public byte[] File;
    }

    // 3. xml для ответа из ИСОГД о статусе заявки (о ее занесении в ИСОГД, о начале обработки)
    [Serializable]
    public class ISOGDMessage
    {
        public string Message;
    }
}
